//
//  NotificationViewController.swift
//  NotificationContent
//
//  Created by vang on 6/11/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import AVKit
import AVFoundation
import Alamofire

public enum CONTENT_TYPE: String {
    case IMAGE_PNG = "attachment-image"
    case IMAGE_GIF = "attachment-image-gif"
    case AUDIO = "attachment-audio"
    case VIDEO = "attachment-video"
}

class NotificationViewController: UIViewController, UNNotificationContentExtension {
    
    var bestAttemptContent: UNMutableNotificationContent?
    
   
    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
        //https://mirrors.standaloneinstaller.com/video-sample/small.mp4
    }
    
   
    
    private func setTextLable(_ text: String?) {
        let attributedString = NSMutableAttributedString(string: text ?? "")
    
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
  
        self.lblBody.attributedText = attributedString
    }


    
    func didReceive(_ notification: UNNotification) {
        self.bestAttemptContent = (notification.request.content.mutableCopy() as? UNMutableNotificationContent)
     
        if let content = self.bestAttemptContent {
            
            self.setTextLable(content.body)
            
            if let attachmentImageURL = content.userInfo["attachment-image"] as? String {
              
            }
            if let attachmentImageURL = content.userInfo["attachment-image-gif"] as? String {
                self.imageView.image = UIImage.gifImageWithURL(content.userInfo["attachment-image-gif"] as? String ?? "")
            }
            if let attachmentVideoURL = content.userInfo["attachment-audio"] as? String {
              
            }
            if let attachmentVideoURL = content.userInfo["attachment-video"] as? String {
              
            }
                
            
            
            if let carouselStr = content.userInfo["images"] as? String {
                
                let list = carouselStr.components(separatedBy: ",")
               // self.carouselImages = list
                
                DispatchQueue.main.async {
//                    self.collectionView.reloadData()
                }
                
            } else {
                //handle non localytics rich push
            }
        }
    }
    
    
    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        if response.actionIdentifier == "deal.takeALook" {
            completion(UNNotificationContentExtensionResponseOption.dismissAndForwardAction)
        } else if response.actionIdentifier == "deal.noThanks" {
            completion(UNNotificationContentExtensionResponseOption.dismissAndForwardAction)
        } else if response.actionIdentifier == "deal.redmindMeLater" {
            completion(UNNotificationContentExtensionResponseOption.dismissAndForwardAction)
        }
    }

}


extension UIImage {
    public class func gifImageWithData(_ data: Data) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
            //print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source)
    }
    
    public class func gifImageWithURL(_ gifUrl:String) -> UIImage? {
        guard let bundleURL:URL = URL(string: gifUrl)
            else {
                //print("image named \"\(gifUrl)\" doesn't exist")
                return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            //print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    public class func gifImageWithName(_ name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                //print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }
        guard let imageData = try? Data(contentsOf: bundleURL) else {
            //print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(imageData)
    }
    
    class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(
            CFDictionaryGetValue(cfProperties,
                                 Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
            to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                                 Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                                                             Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a! < b! {
            let c = a
            a = b
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b
                b = rest
            }
        }
    }
    
    class func gcdForArray(_ array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                                                            source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames,
                                              duration: Double(duration) / 1000.0)
        
        return animation
    }
}
