//
//  GLOBAL.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

let FONT_DEFAULT = UIFont.systemFont(ofSize: 13)

public enum SORT_OPTIONS: Int {
    case NONE = -1
    case EXPIRATION_DATE_ASC = 111
    case EXPIRATION_DATE_DESC = 222
    case DISTANCE_ASC = 333
    case DISTANCE_DESC = 444
    case NAME_ASC = 555
    case NAME_DESC = 666
    
//order_by ( expired_asc | expired_desc | distance_asc | distance_desc | name_asc | name_desc )
    
    var sortSwitch : String {
        switch self {
        case .NONE : return ""
        case .EXPIRATION_DATE_ASC : return "expired_asc"
        case .EXPIRATION_DATE_DESC : return "expired_desc"
        case .DISTANCE_ASC : return "distance_asc"
        case .DISTANCE_DESC : return "distance_desc"
        case .NAME_ASC : return "name_asc"
        case .NAME_DESC : return "name_desc"
        }
    }
}

public enum LANGUAGE: String {
    case VIETNAM = "vi"
    case ENGLISH = "en"
    case CHINESE_SIMP = "zh-Hans"
    case CHINESE_TRAD = "zh-Hant"
}

public enum TODO_TYPE: String {
    case TEO = "tell_everyone"
}

public enum TEO_STATUS: String {
    case NEW = "new"
    case SENT = "sent"
    case PROCESSING = "processing"
    case COMPLETED = "completed"
}

public enum REPORT_TYPE: String {
    case ISSUE = "issue"
    case ERROR = "error"
    case HELP = "help"
}

public enum VERIFY_CARD_STATUS: String {
    case VALID_NOT_USE = "valid"
    case VALID_IS_USED = "is_used"
    case INVALID = "invalid"
}

public enum SWEEPSTAKE_PRIZE: String {
    case NONE = ""
    case NO_WIN = "no_win"
    case FREE_CARD = "free_card"
    case PRIZE1 = "prize1"
    case PRIZE2 = "prize2"
    case PRIZE3 = "prize3"
    case PRIZE4 = "prize4"
    
    var prizeKey : String {
        switch self {
        case .NONE : return ""
        case .NO_WIN : return "NoWin"
        case .FREE_CARD : return "FreeCard"
        case .PRIZE1 : return "Prize1"
        case .PRIZE2 : return "Prize2"
        case .PRIZE3 : return "Prize3"
        case .PRIZE4 : return "Prize4"
        }
    }
    
    var prizeIndex : Int {
        switch self {
        case .NONE : return -1
        case .PRIZE1 : return 0
        case .PRIZE2 : return 1
        case .PRIZE3 : return 2
        case .PRIZE4 : return 3
        case .FREE_CARD : return 4
        case .NO_WIN : return 5
       
        }
    }
}

public enum STORE_TYPE: String {
    case NONE = ""
    case NEARBY_DEALS = "nearby_deals"
    case GLOBAL_SPECIAL_DEALS = "specially_for_you"
}

public enum LEVEL: Int {
    case NONE = -1
    case GOLD = 111
    case SILVER = 222
    case BLACK = 333
    
    var levelSwitch : String {
        switch self {
        case .NONE : return ""
        case .GOLD : return "gold"
        case .SILVER : return "silver"
        case .BLACK : return "black"
        }
    }
}

public enum AVAILABLE_TIME: Int {
    case ALL = -11
    case TODAY = 11
    case TOMORROW = 22
    case THIS_WEEK = 33
    case NEXT_WEEK = 44
    
    var timeSwitch : String {
        switch self {
            case .ALL : return ""
            case .TOMORROW : return "tomorrow"
            case .THIS_WEEK : return "this_week"
            case .TODAY : return "today"
            case .NEXT_WEEK : return "next_week"
        }
    }
}

public enum OSWALD_FONT: String {
    case SEMI_BOLD = "Oswald-SemiBold"
    case REGULAR = "Oswald-Regular"
    case MEDIUM = "Oswald-Medium"
    case LIGHT = "Oswald-Light"
    case EXTRA_LIGHT = "Oswald-ExtraLight"
    case BOLD = "Oswald-Bold"
}

public enum ROBOTO_FONT: String {
    case LIGHT = "Roboto-Light"
    case REGULAR = "Roboto-Regular"
    case MEDIUM = "Roboto-Medium"
    case BOLD = "Roboto-Bold"
    case BLACK = "Roboto-Black"
}

public enum MONTSERRAT_FONT: String {
    case BLACK = "Montserrat-Black"
    case MEDIUM_ITALIC = "Montserrat-MediumItalic"
    case MEDIUM = "Montserrat-Medium"
    case BOLD = "Montserrat-Bold"
    case REGULAR = "Montserrat-Regular"
}

public enum ROBOTO_SLAB_FONT: String {
    case REGULAR = "RobotoSlab-Regular"
    case BOLD = "RobotoSlab-Bold"
}

public enum ULTRA_FONT: String {
    case REGULAR = "Ultra-Regular"
}

public enum LOBSTER_FONT : String {
    case REGULAR = "Lobster-Regular"
}

public enum SUGGEST_TYPE: String {
    case EVENT = "event"
    case OFFER = "offer"
    case DEAL = "deal"
}

public enum MASK_CARD: Int {
    case NONE = 0
    case EXPIRED = 1
    case CLAIMED = 2
}

public enum VENDOR_INIT_TAB: Int {
    case ABOUT = 0
    case BASIC_DEAL = 1
    case DEALS = 2
    case EVENTS = 3
    
    var tabSwitch : String {
        switch self {
        case .ABOUT : return "about"
        case .BASIC_DEAL : return "basic_deal"
        case .DEALS : return "deals"
        case .EVENTS : return "events"
        }
    }
}

public enum PUSH_TYPE: String {
    case DEAL_DAILY_NOTIFICATION = "deal_daily_notification"
    case NEW_CHAT_MESSAGE = "new_chat_message"
    case REMIND_BUY_CARD = "remind_member_buy_card_notification"
    case TELL_EVERYONE_ASSIGN_TO_MEMBER = "tell_everyone_assign_to_member"
    case TELL_EVERYONE_INVITEE_COMPLETED = "tell_everyone_invitee_completed"
    case SUGGEST_UPGRADE_SALESFORCE = "notification_upgrade_salesforce"

    case DAILY_SWIPES_TOOL = "daily_swipes_pns_tool"
    case SPLASH_PNS_TOOL = "splash_pns_tool"
    case WALLET_CAROUSEL_PNS_TOOL = "wallet_carousel_pns_tool"
    case WALLET_DEAL_PNS_TOOL = "wallet_deal_pns_tool"
    case DEAL_DETAIL_PNS_TOOL = "deal_detail_pns_tool"
    case REMINDER_TO_PAY_TOOL = "reminder_to_pay_tool"
    case NEW_LOCATION_SHAKE_PNS_TOOL = "new_location_shake_pns_tool"
}

public enum DEAL_TYPE: String {
    case NONE = ""
    case BASIC = "basic"
    case BLACK = "black"
    case SILVER = "silver"
    case GOLD = "gold"
    case TEO = "tell_everyone"
    case LOYALTY = "loyalty"
    case SWEEPSTAKE = "sweepstake"
    case UNSWIPED_DAILY = "unswiped"
    case SPECIAL_CARD = "special_card"
    
}

public enum DEAL_PROGRAM: String {
    case NONE = ""
    case BASIC = "basic"
    case SWIPE = "swipe"
    case TEO = "tell_everyone"
    case LOYALTY = "loyalty"
    case SWEEPSTAKE = "sweepstake"
    case UNSWIPED_DAILY = "unswiped_daily"
}

public enum TEMPLATE_SPLASH_SCREEN: String {
    case ONE = "Splashscreen1"
    case TWO = "Splashscreen2"
    case THREE = "Splashscreen3"
    case FOUR = "Splashscreen4"

    
    var templateIndex : Int {
        switch self {
            case .ONE : return 0
            case .TWO : return 1
            case .THREE : return 2
            case .FOUR : return 3
        }
    }
}

public enum TEMPLATE_CARD_DEAL: Int {
    case ONE = 1
    case TWO = 2
    case THREE = 3
    case FOUR = 4
    case FIVE = 5
    
    var templateIndex : Int {
        switch self {
            case .ONE : return 0
            case .TWO : return 1
            case .THREE : return 2
            case .FOUR : return 3
            case .FIVE : return 4
        }
    }
}


public enum UPGRADE_PLAN: String {
    case FREE = "Free"
    case BASIC = "Basic"
    case SILVER = "Silver"
    case GOLD = "Gold"
    
    var viewIndex : Int {
        switch self {
            case .FREE : return 0
            case .BASIC : return 1
            case .SILVER : return 2
            case .GOLD : return 3
        }
    }
    
    var txtName : String {
        switch self {
            case .FREE : return ""
            case .BASIC : return LOCALIZED.lifetime.translate
            case .SILVER : return LOCALIZED.local.translate
            case .GOLD : return LOCALIZED.global.translate
        }
    }
}

public enum STORAGE_KEYS: String {
    case USER_INFO
    case APP_TOKEN
    case CLICKED_ON_WELCOME
    case DEVICE_ID
    case APP_LANGUAGE
    case TIME_INTERVAL_GET_SPLASH_DEAL
    case SWEEPSTAKE_RESULT
    case EMAILS_TE
    case QUICK_SIGN_UP
    case HARD_LOCATION
    case HARD_LOCATION_LAT
    case HARD_LOCATION_LNG
}

public enum KIND_PAYMENT_METHOD: String {
    case SUBSCRIPTION = "subscription"
}

class UpgradePackageModel {
    var canUpNextStep: Bool = false
    var canUpToNextStep: UPGRADE_PLAN = .BASIC
    var iconUpgradeNextStep: String = ""
    
    required convenience init(_ data : [String: Any?]?) {
        self.init()
    
        if let tData = data {
            self.canUpNextStep = tData["CanUpNextStep"] as? Bool ?? false
            
            if self.canUpNextStep {
                self.canUpToNextStep = UPGRADE_PLAN(rawValue: tData["CanUpToNextStep"] as? String ?? "") ?? .BASIC
            } else {
                self.canUpToNextStep = UPGRADE_PLAN.FREE
            }
            
            self.iconUpgradeNextStep = tData["IconUpgradeNextStep"] as? String ?? ""
        }
    }
}

class QuickSignUpModel {
    var name: String = ""
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    
    required convenience init(_ data : [String: Any?]? = nil) {
        self.init()
    
        if let tData = data {
            self.name = tData["name"] as? String ?? ""
            self.email = tData["email"] as? String ?? ""
            self.password = tData["password"] as? String ?? ""
            self.confirmPassword = tData["confirmPassword"] as? String ?? ""
        }
    }
}


class SysConfigModel {
    var timePushStarter: Int = 20
    var isHardCodeLocation: Bool = false
    var hardLocationLat: Double = 51.502920
    var hardLocationLng: Double = -0.105724
    
    required convenience init(_ data : [String: Any?]?) {
        self.init()
    
        if let tData = data {
            self.timePushStarter = tData["TimePushStarter"] as? Int ?? 20
            self.isHardCodeLocation = tData["IsHardCodeLocation"] as? Bool ?? false
            self.hardLocationLat = tData["hard_code_location_lat"] as? Double ?? 51.502920
            self.hardLocationLng = tData["hard_code_location_lng"] as? Double ?? -0.105724
        }
    }
}

class GLOBAL {
    
    static let GlobalVariables = GLOBAL()
    
    init(){}
    var fcmToken: String = ""
    var userInfo: [String: Any?]?
    var appToken: String?

    var selectedSearchText: String = ""
    var selectedDeal: [String: Any?]? = nil
    var selectedCategory: [String: Any?]? = nil
    
    var listDealsOnMap: [[String: Any?]] = []
    var listCarouselsOnMap: [[String: Any?]] = []
    var listCategoriesOnMap: [[String: Any?]] = []
    
    var currentLanguage: LANGUAGE = .ENGLISH
    var numberChatReady: Int = 0
    var heightKeyboard: CGFloat = 260
    var shouldShowDailyDeals: Bool = false
    var isFirstCheckBranch: Bool = true
    var isHomeScreenReady: Bool = false
    var isCheckScreenReady: Bool = false
    var referralInfo: [String: Any?]?
    var appDelegate: AppDelegate?
    var sourceInstance: UIViewController?
    
    var rootVC: ExSlideMenuController? 
    var pullQuickInstance: PullQuickViewController?
    var discoveryInstance: DiscoveryMainController?
    var verifyCodeInstance: VerifyCodeViewController?
    var freeAccountInstance: FreeAccountViewController?
    var dealDetailInstance: DealDetailViewController?
    
    var todoWaitingId: String?
    
    var isShowNetworkAlert: Bool = false
    var customAlertTags: [Int] = []
    var claimAlert: CustomAlertView?
    
    var countriesSystem: [[String: Any?]] = []
    var walletCategories:[[String: Any?]] = []
    var homeCategories:[[String: Any?]] = []
    
    var purposeCreateTrip : [String: Any?]? = nil
    var countriesCreateTrip : [[String: Any?]] = []

    var selectedLevelWallet:LEVEL = LEVEL.NONE
    var selectedLocationWallet:Float = Constants.DEFAULT_LOCATION
    var latestLat: CLLocationDegrees = 0
    var latestLng: CLLocationDegrees = 0
    
    // For filter home screen
    var selectedAvailableFilterHome: AVAILABLE_TIME = AVAILABLE_TIME.ALL
    var selectedCategoryFilterHome: [String: Any?]? = nil
    var keywordFilterHome: String = ""
    
    // For filter wallet screen
    var selectedCategoryFilterWallet: [String: Any?]? = nil
    
    var cellHeightsWalletCarousel = [Int: [CGFloat]]()
    var cellHeightsHomeWalletCarousel = [Int: [CGFloat]]()
    
    var pagesWalletCarousel = [Int]()
    
    var growedCell: [String: Any]?
    
    var isOnNotification: Bool = true
    
    var isFirstInitApp: Bool = true
    
    var sysConfigModel: SysConfigModel = SysConfigModel(nil)
    var quickSignUpModel: QuickSignUpModel = QuickSignUpModel(nil)
    
    var getConfigReady: Bool = false
    
    var upgradePackageModel: UpgradePackageModel?
    
    var isHomeAppear: Bool = false
    var allowFetchLocation: Bool = false
    var numberCustomAlertAvailable: Int = 0
    var isShowAlertRequestLocation: Bool = false
    var connectionStatus: NETWORK_STATUS = .UNDEFINED
    var isShowLoyaltyFirst: Bool = true
    
    var tutorialConfig: [[String: Any?]] = []
    
}

extension Notification.Name {
    static let ON_CLICK_BACK_HOME = Notification.Name("ON_CLICK_BACK_HOME")
    static let ON_LIST_EVENT_SHOW = Notification.Name("ON_LIST_EVENT_SHOW")
    static let ON_CLICK_HEADER_WALLET = Notification.Name("ON_CLICK_HEADER_WALLET")
    static let ON_HOME_APPLY_FILTER = Notification.Name("ON_HOME_APPLY_FILTER")
    static let ON_WALLET_APPLY_FILTER = Notification.Name("ON_WALLET_APPLY_FILTER")
    static let FORCE_SIGN_IN = Notification.Name("FORCE_SIGN_IN")
    static let TOOGLE_BOOKMARK_EVENT = Notification.Name("TOOGLE_BOOKMARK_EVENT")
    static let TOOGLE_LIKE_DEAL = Notification.Name("TOOGLE_LIKE_OFFER")
    static let TOOGLE_FAVORITE_DEAL = Notification.Name("TOOGLE_FAVORITE_DEAL")
    static let UPDATED_WALLET_SUGGESTED_AMAZING = Notification.Name("UPDATED_WALLET_SUGGESTED_AMAZING")
    static let UPDATE_USE_DEAL = Notification.Name("UPDATE_USE_DEAL")
    static let FIXED_ITEM_ON_HORIZONTAL = Notification.Name("FIXED_ITEM_ON_HORIZONTAL")
    static let ON_CHANGE_MASK_REMOVE_DEAL = Notification.Name("ON_CHANGE_MASK_REMOVE_DEAL")
    static let ON_CHANGE_MASK_CHECK_DEAL = Notification.Name("ON_CHANGE_MASK_CHECK_DEAL")
    static let ON_CHANGE_MASK_DISLIKE_DEAL = Notification.Name("ON_CHANGE_MASK_DISLIKE_DEAL")
    static let ON_CHANGE_MASK_LIKE_DEAL = Notification.Name("ON_CHANGE_MASK_LIKE_DEAL")
    static let ON_RESET_MASK_DEAL = Notification.Name("ON_RESET_MASK_DEAL")
    static let REFRESH_MENU_VIEW = Notification.Name("REFRESH_MENU_VIEW")
    static let REFRESH_HOME_PAGE = Notification.Name("REFRESH_HOME_PAGE")
    static let SELECTED_CARD_UPGRADE = Notification.Name("SELECTED_CARD_UPGRADE")
    static let CHECK_CHAT_EXIST = Notification.Name("CHECK_CHAT_EXIST")
    static let UPDATED_SUBSCRIPTION = Notification.Name("UPDATED_SUBSCRIPTION")
    static let REFRESH_VENDOR_DETAIL = Notification.Name("REFRESH_VENDOR_DETAIL")
    static let SELECTED_CARD_PAYMENT = Notification.Name("SELECTED_CARD_PAYMENT")
    static let CHECK_BRANCH_COMPLETED = Notification.Name("CHECK_BRANCH_COMPLETED")
    static let FINISHED_SWEEPTAKES = Notification.Name("FINISHED_SWEEPTAKES")
    static let ANIMATION_GROW_CELL = Notification.Name("ANIMATION_GROW_CELL")
    static let DOWN_HEIGHT_FOR_CAROUSEL_IN_WALLET = Notification.Name("DOWN_HEIGHT_FOR_CAROUSEL_IN_WALLET")
    static let RELOAD_GROUP_GROW_CARD = Notification.Name("RELOAD_GROUP_GROW_CARD")
    
    static let REFRESH_THEN_SCROLL_TO_ACTION_AND_EXPAND = Notification.Name("REFRESH_THEN_SCROLL_TO_ACTION_AND_EXPAND")
    static let REFRESH_TEO_IN_LIST = Notification.Name("REFRESH_TEO_IN_LIST")
    
    static let SCROLL_TO_GROW_CARD_THEN_OPEN = Notification.Name("SCROLL_TO_GROW_CARD_THEN_OPEN")
    static let HIGHLIGH_CAROUSEL = Notification.Name("HIGHLIGH_CAROUSEL")
    
    static let UPDATE_LANGUAGES_IMMEDIATELY = Notification.Name("UPDATE_LANGUAGES_IMMEDIATELY")
    static let SELECTED_COUNTRY_IN_TRIP = Notification.Name("SELECTED_COUNTRY_IN_TRIP")
    static let FORCE_UNDO_ONE_SWIPE_CARD = Notification.Name("FORCE_UNDO_ONE_SWIPE_CARD")
    static let FORCE_SUBMIT_SWIPE_CARD = Notification.Name("FORCE_SUBMIT_SWIPE_CARD")
    
    static let SHOW_SPLASH_PNS = Notification.Name("SHOW_SPLASH_PNS")
 
    static let ON_SELECTED_CATEGORY_SEARCH = Notification.Name("ON_SELECTED_CATEGORY_SEARCH")
    static let ON_SELECTED_DEAL_ON_CAROUSEL = Notification.Name("ON_SELECTED_DEAL_ON_CAROUSEL")
    static let ON_DISSMIS_STORE_CAROUSEL_VIEW = Notification.Name("ON_DISSMIS_STORE_CAROUSEL_VIEW")
    static let UN_FOCUS_DEAL_ON_MAP = Notification.Name("UN_FOCUS_DEAL_ON_MAP")
    static let ON_UPDATE_STORE_CAROUSEL_VIEW = Notification.Name("ON_UPDATE_STORE_CAROUSEL_VIEW_DATA")
    static let ON_UPDATE_CATEGORY_VIEW = Notification.Name("ON_UPDATE_CATEGORY_VIEW")
    static let ON_SELECTED_DEAL_ON_MAP = Notification.Name("ON_SELECTED_DEAL_ON_MAP")
    static let ON_HIGHLIGHT_DEAL_ON_CAROUSEL = Notification.Name("ON_HIGHLIGHT_DEAL_ON_CAROUSEL")
    static let ON_DISABLE_HIGHLIGHT_DEAL_ON_CAROUSEL = Notification.Name("ON_DISABLE_HIGHLIGHT_DEAL_ON_CAROUSEL")
    static let FILTER_WITH_TEXT_ON_CAROUSEL = Notification.Name("FILTER_WITH_TEXT_ON_CAROUSEL")
    static let REFRESH_LIST_CAROUSEL = Notification.Name("REFRESH_LIST_CAROUSEL")
    static let BEGIN_CHECK_SWIPE_DEALS = Notification.Name("BEGIN_CHECK_SWIPE_DEALS")
    static let REFRESH_HOME_MAP_AFTER_AUTHORIZED = Notification.Name("REFRESH_HOME_MAP_AFTER_AUTHORIZED")
    static let ON_CLICKED_MORE_STORE_ON_CAROUSEL = Notification.Name("ON_CLICKED_MORE_STORE_ON_CAROUSEL")
    static let UPDATE_MORE_STORED_AGAIN = Notification.Name("UPDATE_MORE_STORED_AGAIN")
    static let FORCE_ALLOW_PERMISSION_LOCATION = Notification.Name("FORCE_ALLOW_PERMISSION_LOCATION")
    
    static let UPDATED_POSITION_PICKER_CENTER = Notification.Name("UPDATED_POSITION_PICKER_CENTER")
    static let UPDATED_POSITION_LOCATION = Notification.Name("UPDATED_POSITION_LOCATION")
    static let UPDATED_SECONDE_HIEGHT_PULL = Notification.Name("UPDATED_SECONDE_HIEGHT_PULL")
    
    static let REFRESH_DEAL_DETAIL = Notification.Name("REFRESH_DEAL_DETAIL")
}
