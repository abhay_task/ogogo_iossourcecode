//
//  VendorSummaryView.swift
//  TGCMember
//
//  Created by vang on 5/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class StoreCarouselView: UIViewController {
    
    @IBOutlet weak var tbDeals: UITableView!
    
    private var data:[String: Any?]?
    private var summaryData:[String: Any?]?
    var list:[[String: Any?]] = []
    
    var scrollItemCallback: DataCallback?
    var onClickedItemCallback: DataCallback?
    var onClickedMoreStoreCallback: DataCallback?
    
    private let heightCategoryView: CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tbDeals.register(UINib(nibName: "CarouselDealCell", bundle: nil), forCellReuseIdentifier: "CarouselDealCell")
        self.tbDeals.register(UINib(nibName: "SearchNotFoundCell", bundle: nil), forCellReuseIdentifier: "SearchNotFoundCell")
        self.tbDeals.register(UINib(nibName: "CategoryViewCell", bundle: nil), forCellReuseIdentifier: "CategoryViewCell")
    
    }

    
    func config(_ data: [String: Any?]?, scrollItemCallback _scrollItemCallback: DataCallback? = nil, onClickedItemCallback _onClickedItemCallback: DataCallback? = nil, onClickedMoreStoreCallback _onClickedMoreStoreCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.scrollItemCallback = _scrollItemCallback
        self.onClickedItemCallback = _onClickedItemCallback
        self.onClickedMoreStoreCallback = _onClickedMoreStoreCallback
    }
    
    
    func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        print("Passing all touches to the next view (if any), in the view stack.")
        return false
    }
    
    public func attach(_ pullVC: PullUpController) {
        self.tbDeals?.attach(to: pullVC)
    }
    
    public func updateData() {
        self.list = GLOBAL.GlobalVariables.listCarouselsOnMap
        
        self.tbDeals.reloadData()
    }
    
    private func openViewAddTrip() {
        let content = CreateTripView().loadView()
        content.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        content.layoutIfNeeded()
        content.config(nil, onClickedCancelCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
        }, onClickedAddTripCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tParams = data {
                self.handleCreateNewTrip(tParams)
            }
            
        })
        
        Utils.showAlertWithCustomView(content, bgColor: UIColor(47, 72, 88, 0.8), animation: false)
    }
    
    private func openUpgrade(_ data: [String: Any?]?) {
        if let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController(), let tData = data {
            let toPlan = tData["CanUpTo"] as? String ?? ""
            
            let selectedIndex = (UPGRADE_PLAN(rawValue: toPlan) ?? UPGRADE_PLAN.FREE).viewIndex
            
            let newUpgradeVC = NewUpgradeViewController()
            newUpgradeVC.fromCarousel = true
            newUpgradeVC.initPlanIndex = selectedIndex
            
            topVC.navigationController?.pushViewController(newUpgradeVC, animated: true)
        }
        
    }
    
    private func handleCreateNewTrip(_ params: [String: Any?]) {
        mPrint("handleCreateNewTrip params --> ", params)
        Utils.showLoading()
        
        APICommonServices.createNewTrip(params as [String : Any]) { (resp) in
            mPrint("handleCreateNewTrip --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                   
                } else {
                    Utils.dismissLoading()
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
        }
    }
    
}


extension StoreCarouselView: UITableViewDelegate, UITableViewDataSource {
    
    private func needIncreaseHeight(_ indexPath: IndexPath) -> [String: Any?] {
        
        var willNotify: Bool = false
        var willIncrease: Bool = false
        
        var sectionFirst =  [String: Any?]()
        for section in list {
            let isNotFound = section["IsNotFound"] as? Bool ?? false
            
            if !isNotFound {
                sectionFirst = section
                break
            }
        }
        
        let sectionInfo = list[indexPath.section]

        if sectionInfo["StoreType"] as? String == sectionFirst["StoreType"] as? String && (sectionInfo["StoreType"] as? String ?? "") != "" && (sectionFirst["StoreType"] as? String ?? "") != "" {
            if indexPath.row == 0 {
                willNotify = true
            }
            
            if let carousels = sectionInfo["List"] as? [[String: Any?]], carousels.count > 0, indexPath.row == 0 {
                let data = carousels[0]
                
                if Utils.isCarouselDeal(data) && Utils.isHaveItemMore(data) {
                    willIncrease = true
                }
            }
        }
         
        return ["willNotify": willNotify, "willIncrease": willIncrease]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = list[section]
        
        if let isNotFound = sectionInfo["IsNotFound"] as? Bool, isNotFound ==  true {
            return 1
        }
        
        if let isCategoryCell = sectionInfo["IsCategoryCell"] as? Bool, isCategoryCell ==  true {
            return 1
        }
        
        let subList = sectionInfo["List"] as? [[String: Any?]] ?? []
        
        return subList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionInfo = list[indexPath.section]
        
        if let isCategoryCell = sectionInfo["IsCategoryCell"] as? Bool, isCategoryCell ==  true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryViewCell") as? CategoryViewCell
            cell?.reloadData()
            
            return cell!
        }
        
        if let isNotFound = sectionInfo["IsNotFound"] as? Bool, isNotFound ==  true {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchNotFoundCell") as? SearchNotFoundCell
            
            return cell!
        }
        
        let carousels = sectionInfo["List"] as? [[String: Any?]] ?? []
        
        if carousels.count <= 0 || indexPath.row > carousels.count {
            return UITableViewCell()
        }
        
        var data = carousels[indexPath.row]
        
        
        if Utils.isCarouselDeal(data) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarouselDealCell") as? CarouselDealCell
            
            let backgroundView = UIView()
            backgroundView.frame = cell?.frame ?? .zero
            backgroundView.backgroundColor = .clear
            cell?.backgroundView = backgroundView
            cell?.backgroundColor = .clear
            
            data["indexTableCell"] = indexPath.row
            data["totalDeals"] = carousels
            data["haveItemMore"] = Utils.isHaveItemMore(data)
            data["sectionInfo"] = indexPath.row == 0 ? sectionInfo : nil
            
            cell?.config(data, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                    
                self.onClickedItemCallback?(data)
            }, onClickedMoreStoreCallback: { [weak self] (data) in
                guard let self = self else {return}
                    
                self.onClickedMoreStoreCallback?(data)
            }, onClickedUpgradeCallback: { [weak self] (data) in
                guard let self = self else {return}
                   
                self.openUpgrade(data)
             })
            
            cell?.layoutIfNeeded()
            
            return cell!
        }
        
        return UITableViewCell()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionInfo = list[indexPath.section]
        
        if let isCategoryCell = sectionInfo["IsCategoryCell"] as? Bool, isCategoryCell ==  true {
            return 100
        }
        
        if let isNotFound = sectionInfo["IsNotFound"] as? Bool, isNotFound ==  true {
            return 400
        }
        
        let carousels = sectionInfo["List"] as? [[String: Any?]] ?? []
        
        if carousels.count <= 0 || indexPath.row >= carousels.count {
            return 0
        }
        
        var tHeightRow: CGFloat = 210.0
        let data = carousels[indexPath.row]
        
        if Utils.isCarouselDeal(data) {
            if Utils.isHaveItemMore(data) {
                tHeightRow = 240.0
            }
            
            if indexPath.row == 0 {
                tHeightRow += 50
            }
            
            if indexPath.section != 0 {
                tHeightRow += 20
            }
        }
        
        let checkInfo = self.needIncreaseHeight(indexPath)
        
        let isNotify = checkInfo["willNotify"] as? Bool ?? false
        let willIncrease = checkInfo["willIncrease"] as? Bool ?? false
       
        if isNotify {
//            print("section -> ", indexPath.section)
//            print("row -> ", indexPath.row)
            
            NotificationCenter.default.post(name: .UPDATED_SECONDE_HIEGHT_PULL, object: ["isMore": willIncrease])
        }
    
        
        return tHeightRow
        
    }

    
    
}

