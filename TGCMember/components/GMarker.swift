//
//  VendorInfoWindow.swift
//  TGCMember
//
//  Created by vang on 4/24/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class GMarker: UIView {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var gIcon: UIImageView!
    @IBOutlet weak var gBigIcon: UIImageView!
    @IBOutlet weak var badgeImage: UIImageView!
    @IBOutlet weak var lblBadge: UILabel!
    
    @IBOutlet weak var dealName: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    
    @IBOutlet weak var coverBadge: UIView!
    @IBOutlet weak var coverCate: UIView!
    @IBOutlet weak var cateImage: UIImageView!
    
    private var data: [String: Any]?
    private var onClickedVendorDetailsCallback: DataCallback?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var lblTitle: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func decorate() {
        coverCate?.layer.cornerRadius = 22.0
        coverCate?.layer.masksToBounds = true
        
        thumbnail.layer.cornerRadius = 19.5
        thumbnail.layer.masksToBounds = true
    }

    
    func loadSelectedMarker() -> GMarker {
        let aView = Bundle.main.loadNibNamed("GMarker", owner: self, options: nil)?[0] as! GMarker
        
        return aView
    }
    
    func loadDefaultMarker() -> GMarker {
        let aView = Bundle.main.loadNibNamed("GMarker", owner: self, options: nil)?[1] as! GMarker
        
        return aView
    }
    
    func loadCateMarker() -> GMarker {
        let aView = Bundle.main.loadNibNamed("GMarker", owner: self, options: nil)?[2] as! GMarker
        
        return aView
    }
    
    @IBAction func onClickedVendorDetails(_ sender: Any) {
        
        if let callback = self.onClickedVendorDetailsCallback {
            callback(self.data)
        }
    }
    
     func config(_ data: [String: Any]?, onClickedVendorDetailsCallback _onClickedVendorDetailsCallback: DataCallback?) -> Void {
        
        
        
        self.data = data
        self.onClickedVendorDetailsCallback = _onClickedVendorDetailsCallback
        
        decorate()
    }
    
}
