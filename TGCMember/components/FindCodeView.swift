//
//  FindCodeView.swift
//  TGCMember
//
//  Created by vang on 10/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class FindCodeView: UIView {
    private var data: [String: Any?]?
    
    private var onClickedCloseCallback: DataCallback?
    @IBOutlet weak var widthImgConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImgConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingImg: UIActivityIndicatorView!
    @IBOutlet weak var cardImg: UIImageView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var btnOKGotIt: UIButton!
    @IBOutlet weak var lblPrompt: UILabel!
    
    override func awakeFromNib() {
       super.awakeFromNib()
       // Initialization code
       
       self.decorate()
    }

    private func decorate() {
        coverView?.layer.cornerRadius = 3.0
        coverView?.layer.masksToBounds = true
        self.btnOKGotIt.setTitle(LOCALIZED.ok_i_got_it.translate, for: .normal)
        
        self.loadingImg.startAnimating()
        self.cardImg.sd_setImage(with:  URL(string: Constants.OGOGO_CARD_LINK), completed: { [weak self]  (image, error, _, _) in
            guard let self = self else {return}
            
            if let tImage = image {
                let ratio = tImage.size.width / tImage.size.height
                
                self.widthImgConstraint.constant = self.heightImgConstraint.constant * ratio
            }
            
            
            self.loadingImg.stopAnimating()
        })
        
        self.lblPrompt?.setAttribute(LOCALIZED.txt_get_the_card_scratch_the_code.translate, color: UIColor(39, 39, 39), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 4)
        self.lblPrompt?.textAlignment = .center
        
        self.btnOKGotIt?.layer.cornerRadius = 3.0
        self.btnOKGotIt?.layer.applySketchShadow()
    
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
         self.onClickedCloseCallback?(self.data)
     }
    
    @IBAction func onClickedOkGotIt(_ sender: Any) {
        self.onClickedCloseCallback?(self.data)
    }

     override init(frame: CGRect) {
        super.init(frame: frame)
     }

     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
     }


     func loadView() -> FindCodeView {
        let aView = Bundle.main.loadNibNamed("FindCodeView", owner: self, options: nil)?[0] as! FindCodeView
        
        return aView
     }

     private func fillData() {
         if let tData = self.data {
            
         }
     }

     func config(_ data: [String: Any?]?, onClickedCloseCallback _onClickedCloseCallback: DataCallback? = nil) -> Void {
         
         self.data = data
        
         self.onClickedCloseCallback = _onClickedCloseCallback
        
         self.fillData()
     }

}
