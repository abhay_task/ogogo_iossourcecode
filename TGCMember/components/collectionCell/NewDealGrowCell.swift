//
//  NewDealGrowCell.swift
//  TGCMember
//
//  Created by vang on 8/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NewDealGrowCell: UICollectionViewCell {
    
    @IBOutlet weak var content: UIView!
    
    private var complexData: [String: Any?]?
    private var data: [String: Any?]?
    private var carouselType: STORE_TYPE = .NONE
    
    private var onClickedCellCallback: DataCallback?
    private var indexTableCell: Int = 0
    private var indexCollectionCell: Int = 0
    private var subview: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(animationGrowCell(_:)), name: .ANIMATION_GROW_CELL, object: nil)
    }
    
    @objc func animationGrowCell(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            let tIndexTableCell = uData["indexTableCell"] as? Int ?? 0
            let tIndexCollectionCell = uData["indexCollectionCell"] as? Int ?? 0
            let willGrow = uData["willGrow"] as? Bool ?? false
            
            if tIndexTableCell == self.indexTableCell {
                if  tIndexCollectionCell == self.indexCollectionCell {
                    var growedCell = [String: Int]()
                    growedCell["indexTableCell"] = tIndexTableCell
                    growedCell["indexCollectionCell"] = tIndexCollectionCell
                    
                    var tY:CGFloat = 0
                    if willGrow {
                        GLOBAL.GlobalVariables.growedCell = growedCell
                    } else {
                        tY = NEW_HEIGHT_GROW_VIEW.OPEN - NEW_HEIGHT_GROW_VIEW.CLOSE
                    }
                    
                    UIView.animate(withDuration: 0.3, delay: 0, options: .transitionFlipFromTop, animations: { () -> Void in
                        self.subview?.frame = CGRect(x: 0, y: tY, width: 124, height: 192)
                    }, completion: { (finised) in
                        self.subview?.frame = CGRect(x: 0, y: tY, width: 124, height: 192)
                        //self.layoutSubviews()
                    })
                }
                
            } else if !willGrow {
                //                self.subview?.frame = CGRect(x: 0, y: 0, width: 124, height: 192)
                //                self.layoutSubviews()
                //
                //                self.layoutIfNeeded()
            }
            
        }
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    private func renderUI() {
        if let tComplexData = self.complexData, let tData = tComplexData["data"] as? [String: Any?]  {
            self.data = tData
            
            self.indexTableCell = tData["indexTableCell"] as? Int ?? 0
            self.indexCollectionCell = tData["indexCollectionCell"] as? Int ?? 0
            let willDownCell = tData["willDownCell"] as? Bool ?? false
            
            if let tSubview  = self.subview {
                //print("NewDealGrowCell IS EXIST")
                tSubview.removeFromSuperview()
            }
            
            let dealShowType = Utils.getDealType(tData)
            
           if dealShowType == DEAL_TYPE.SWEEPSTAKE {
                let smallSweepstakeView = SmallSweepstakeCard().loadView()
                
                smallSweepstakeView.config(tData, onCompletedCallback: { (data) in
                    self.onClickedCellCallback?(data)
                }, onClickedCardCallback: { (data) in
                    self.onClickedCellCallback?(data)
                })
                
                self.subview = smallSweepstakeView
            } else {
                let typeTpl = Utils.getTemplateDeal(tData)
                
                let smallDealView = SmallDealCard().loadTheme(typeTpl)
                smallDealView.config(tData) { (data) in
                    self.onClickedCellCallback?(data)
                }
                
                self.subview = smallDealView
            }
            
            self.content.addSubview(self.subview!)
            
            var tY: CGFloat = 0
            
            if let growedCell = GLOBAL.GlobalVariables.growedCell {
                let tIndexTableCell = growedCell["indexTableCell"] as? Int ?? 0
                let tIndexCollectionCell = growedCell["indexCollectionCell"] as? Int ?? 0
                
                if tIndexTableCell == self.indexTableCell && tIndexCollectionCell == self.indexCollectionCell {
                    
                } else if willDownCell {
                    tY = NEW_HEIGHT_GROW_VIEW.OPEN - NEW_HEIGHT_GROW_VIEW.CLOSE
                }
            } else if willDownCell {
                tY = NEW_HEIGHT_GROW_VIEW.OPEN - NEW_HEIGHT_GROW_VIEW.CLOSE
            }
            
            self.subview?.frame = CGRect(x: 0, y: tY, width: 124, height: 192)
            self.subview?.layoutIfNeeded()
            
        }
    }
    
    public func config(_ complexData: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.complexData = complexData
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.renderUI()
    }
    
    
    
}
