//
//  CustomKolodaView.swift
//  Koloda
//
//  Created by Eugene Andreyev on 7/11/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit

public class OneDealSwipe: KolodaView {
    var isFirstLoad: Bool = true
    var paddingBottomItem: CGFloat = 0.0
    var paddingTopItem: CGFloat = 0.0
    
    var heightCover: CGFloat = 0.0
    
    var animationDelegate: SwipeAppearanceDelegate?
    private var draggableCardViews: [DraggableCardView] = []
    
    private var onRunOutOfCardsCallback: DataCallback?
    
    private var onRemoveCardCallback: DataCallback?
    private var onCheckCardCallback: DataCallback?
    private var onDislikeCardCallback: DataCallback?
    private var onLikeCardCallback: DataCallback?
    private var onReadyCallback: DataCallback?
    private var onNeedUpdateHeaderCallback: DataCallback?
    private var onSubmitDealIfHaveCallback: DataCallback?

    private var timerUndo: Timer?
    private var timerHoldCard: Timer?
    private var data: [String: Any]?
    
    private var dealPending: [String: Any?]?
    private var statusPending: MASK_DEAL_STATUS = .NONE
    private var isHolding: Bool = false
    
    private var headerView: HeaderSwipeCard?

    
    private var deals: [[String: Any?]] = [] {
        didSet {
           
            self.extDelegate = self
            self.delegate = self
            self.dataSource = self
            
            self.visibleCardsDirection = .top
            //self.backgroundCardsTopMargin = 50
            self.alphaValueTransparent = 1.0
            self.alphaValueSemiTransparent = 1.0
            
            
            NotificationCenter.default.addObserver(self, selector: #selector(forceUndo(_:)), name: .FORCE_UNDO_ONE_SWIPE_CARD, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(forceSubmit(_:)), name: .FORCE_SUBMIT_SWIPE_CARD, object: nil)
            //self.decorateHeader()
        }
    }
    
    @objc func forceUndo(_ notification: Notification) {
         if let uData = notification.object as? [String: Any], let tData = self.dealPending {
            let uId = uData["ID"] as? String ?? ""
            let tId = tData["ID"] as? String ?? ""
                
            if uId == tId {
                self.onClickedUndo()
            }
        }
       
    }
    
    @objc func forceSubmit(_ notification: Notification) {
         if let uData = notification.object as? [String: Any], let tData = self.dealPending {
            let uId = uData["ID"] as? String ?? ""
            let tId = tData["ID"] as? String ?? ""

            if uId == tId {
                self.submitDataIfHave()
            }
        }
       
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .FORCE_UNDO_ONE_SWIPE_CARD, object: nil)
    }

    override public func frameForCard(at index: Int) -> CGRect {
     
        let heightView = heightCover == 0 ? self.bounds.height : heightCover
        self.heightCover = heightView
        
        let widthView = self.bounds.width
        
        let heightItem: CGFloat = min((heightView - paddingBottomItem - paddingTopItem - (CGFloat(countOfCards - 1) * spaceItem)), maxHeightItem)
        let widthItem:CGFloat = heightItem * ratio
    
        let centerOffset: CGFloat = (heightView - heightItem - CGFloat(countOfCards - 1) * spaceItem)/2
         // top offset for LATEST ITEM
        //let spaceBottom = (heightView - heightItem) / 2
        
        var topOffset: CGFloat = 0
        if centerOffset <= paddingBottomItem {
            topOffset = centerOffset - abs(centerOffset - paddingBottomItem)
        } else {
            topOffset = centerOffset - paddingBottomItem
        }
       
        //topOffset += 50
        
        let xOffset: CGFloat = (widthView - widthItem) / 2
        let width: CGFloat = widthItem
        let height: CGFloat = heightItem
        let yOffset: CGFloat = topOffset + CGFloat(countOfCards - 1 - index) * spaceItem
        let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
        
        return frame
        
    }
    
    private func onClickedUndo() {
        if let tTimer = self.timerUndo {
            tTimer.invalidate()
        }
        
        self.headerView?.isHidden = true
        self.onNeedUpdateHeaderCallback?(["isHidden": true , "onlyShowHide": true])
        if let tDateRemove = self.dealPending {
            self.deals.insert(tDateRemove, at: 0)
            self.reloadData(forceAnimation: false)
            
            self.dealPending = nil
        }
        
        
        //print("onClickedUndo")
    }
    
    
    
    private func decorateHeader() {
        guard let _ = self.headerView else {
       
            self.headerView = HeaderSwipeCard().loadView()
            self.headerView?.config(onClickedUndoCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.onClickedUndo()
            })
            
            self.addSubview(self.headerView!)
            
            
            let heightHeader = Utils.isIPhoneNotch() ? 110 : 100
            self.headerView?.snp.makeConstraints({ (make) in
                make.top.equalTo(self).offset(0)
                make.right.equalTo(self).offset(0)
                make.width.equalTo(SCREEN_WIDTH)
                make.height.equalTo(heightHeader)
            })

            return
        }
    }

    private func updateHeaderView(currentStatus: MASK_DEAL_STATUS = .NONE, pendingStatus: MASK_DEAL_STATUS = .NONE, isHideUndo: Bool = true) {
        var textPending: String = ""
        var textCurrent: String = ""
        let isHideCurrentStatus = !isHideUndo
        let isHideHeader = (currentStatus == .NONE && pendingStatus == .NONE) ? true : false
        
        
        if pendingStatus == .REMOVE {
            textPending = LOCALIZED.refused_offer.translate.uppercased()
        } else if pendingStatus == .CHECK {
            textPending = LOCALIZED.added_to_wallet.translate.uppercased()
        } else if pendingStatus == .DISLIKE {
            textPending = LOCALIZED.offer_blocked.translate.uppercased()
        } else if pendingStatus == .LIKE {
            textPending = LOCALIZED.added_to_favorites.translate.uppercased()
        }
        
        if currentStatus == .REMOVE {
            textCurrent = LOCALIZED.refusing_offer.translate.uppercased()
        } else if currentStatus == .CHECK {
            textCurrent = LOCALIZED.adding_to_wallet.translate.uppercased()
        } else if currentStatus == .DISLIKE {
            textCurrent = LOCALIZED.blocking_offer.translate.uppercased()
        } else if currentStatus == .LIKE {
            textCurrent = LOCALIZED.adding_to_favorites.translate.uppercased()
        }
       
        var tData = [String: Any]()
        tData["isHideUndo"] = isHideUndo
        tData["isHideCurrentStatus"] = isHideCurrentStatus
        tData["textPending"] = textPending
        tData["textCurrent"] = textCurrent
        tData["isHideHeader"] = isHideHeader
        tData["isHidePrompt"] = true
        
        self.headerView?.updateView(tData)
    }
    
    public func updateData(_ data: [String: Any]?) {
        self.data = data
        
        if let tData = self.data {
            self.deals = tData["deals"] as? [[String: Any?]] ?? []
        }
    }
    
 
    
    public func config(_ data: [String: Any]?, onRemoveCardCallback _onRemoveCardCallback: DataCallback? = nil, onCheckCardCallback _onCheckCardCallback: DataCallback? = nil, onDislikeCardCallback _onDislikeCardCallback: DataCallback? = nil, onLikeCardCallback _onLikeCardCallback: DataCallback? = nil, onRunOutOfCardsCallback _onRunOutOfCardsCallback: DataCallback? = nil, onReadyCallback _onReadyCallback: DataCallback? = nil, onNeedUpdateHeaderCallback _onNeedUpdateHeaderCallback: DataCallback? = nil, onSubmitDealIfHaveCallback _onSubmitDealIfHaveCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onRemoveCardCallback = _onRemoveCardCallback
        self.onCheckCardCallback = _onCheckCardCallback
        self.onDislikeCardCallback = _onDislikeCardCallback
        self.onLikeCardCallback = _onLikeCardCallback
        self.onNeedUpdateHeaderCallback = _onNeedUpdateHeaderCallback
        self.onSubmitDealIfHaveCallback = _onSubmitDealIfHaveCallback
        self.onRunOutOfCardsCallback = _onRunOutOfCardsCallback
        self.onReadyCallback = _onReadyCallback
        
        if let tData = self.data {
            self.deals = tData["deals"] as? [[String: Any?]] ?? []
        }
    }
    
   
    
}


extension OneDealSwipe: KolodaViewExtDelegate {
    public func kolodaLoaded(_ koloda: KolodaView, card: DraggableCardView, atIndex: Int) {

        //card.alpha = isFirstLoad ? 0 : 1
        
        self.draggableCardViews.append(card)
        
        if atIndex == Int(countOfCards - 1) {
            setTimeout({ [weak self] in
                guard let self = self else {return}
                
                self.onReadyCallback?(self.data)
                
                if let header = self.headerView {
                    self.bringSubviewToFront(header)
                }
           }, 500)
        }
        
    }
}

extension OneDealSwipe: KolodaViewDelegate {
    
    public func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        // drag all card
        //self.onRunOutOfCardsCallback?(self.data)
    }
    
    public func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let data = self.deals[index]
    
    }
    
    public func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, in direction: SwipeResultDirection) {
        let index = koloda.currentCardIndex
        let data = deals[index]
        
        if finishPercentage == 100 {
            
            self.timerForHoldCard(card: data, direction: direction)
            
            if direction == .left || direction == .bottomLeft || direction == .topLeft{
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_REMOVE_DEAL, object: data)
                
                self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.REMOVE, "pendingStatus": self.statusPending, "isHideUndo": true])
                
                self.updateHeaderView(currentStatus: .REMOVE, pendingStatus: self.statusPending, isHideUndo: true)
            } else {
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_CHECK_DEAL, object: data)
                
                self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.CHECK, "pendingStatus": self.statusPending, "isHideUndo": true])
                
                self.updateHeaderView(currentStatus: .CHECK, pendingStatus: self.statusPending, isHideUndo: true)
            }
        } else {
            
            NotificationCenter.default.post(name: .ON_RESET_MASK_DEAL, object: data)
            self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.NONE, "pendingStatus": MASK_DEAL_STATUS.NONE, "isHideUndo": true])
            self.updateHeaderView(currentStatus: .NONE, pendingStatus: .NONE, isHideUndo: true)
            self.stopTimerHoldCard()
        }
        
    }
    
    private func stopTimerHoldCard() {
        self.isHolding = false
        
        if let tTimer = self.timerHoldCard {
            tTimer.invalidate()
        }
    }
    
    public func kolodaDidResetCard(_ koloda: KolodaView) {
        self.stopTimerHoldCard()
        
        let index = koloda.currentCardIndex
        let data = deals[index]

        
        NotificationCenter.default.post(name: .ON_RESET_MASK_DEAL, object: data)
        self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.NONE, "pendingStatus": MASK_DEAL_STATUS.NONE, "isHideUndo": true])
        self.updateHeaderView(currentStatus: .NONE, pendingStatus: .NONE, isHideUndo: true)
    }
    
    public func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
       
        self.onSubmitDealIfHaveCallback?(self.dealPending)
        
        //self.submitDataIfHave()
        
        
        if direction == .left || direction == .bottomLeft || direction == .topLeft {
            self.statusPending = self.isHolding ? .DISLIKE : .REMOVE
        } else  {
            self.statusPending = self.isHolding ? .LIKE : .CHECK
        }
        
        self.onNeedUpdateHeaderCallback?(["dealPending": self.deals[index], "currentStatus": MASK_DEAL_STATUS.NONE, "pendingStatus": self.statusPending, "isHideUndo": false])
        self.updateHeaderView(currentStatus: .NONE, pendingStatus: self.statusPending, isHideUndo: false)
        
        //print("One_DEAL statusPending \(self.statusPending) --> \(self.deals[index]["ID"] as? String ?? "")" )
        
        self.stopTimerHoldCard()
        self.reinitView(index)

    }
    
    
    private func timerForUndo(_ timeInterval: TimeInterval = Constants.TIME_INTERVAL_UNDO_CARD) {
        self.headerView?.isHidden = false
        self.onNeedUpdateHeaderCallback?(["isHidden": false , "onlyShowHide": true])
        if let tTimer = self.timerUndo {
            tTimer.invalidate()
        }
        
        self.timerUndo = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { timer in
            self.headerView?.isHidden = true
            self.onNeedUpdateHeaderCallback?(["isHidden": true , "onlyShowHide": true])
            self.submitDataIfHave()
        }
    }
    
    private func timerForHoldCard(_ timeInterval: TimeInterval = Constants.TIME_INTERVAL_HOLDING_CARD, card:[String: Any?], direction: SwipeResultDirection) {
     
        self.stopTimerHoldCard()
        
        self.timerHoldCard = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { timer in
            self.isHolding = true
            if direction == .left || direction == .bottomLeft || direction == .topLeft{
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_DISLIKE_DEAL, object: card)
                self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.DISLIKE, "pendingStatus": self.statusPending, "isHideUndo": true])
                self.updateHeaderView(currentStatus: .DISLIKE, pendingStatus: self.statusPending, isHideUndo: true)
            } else {
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_LIKE_DEAL, object: card)
                self.onNeedUpdateHeaderCallback?(["currentStatus": MASK_DEAL_STATUS.LIKE, "pendingStatus": self.statusPending, "isHideUndo": true])
                self.updateHeaderView(currentStatus: .LIKE, pendingStatus: self.statusPending, isHideUndo: true)
            }
        }
    }
    
    private func submitDataIfHave() {
        if let dataPending = self.dealPending {
            
            if statusPending == .REMOVE {
                self.onRemoveCardCallback?(dataPending)
            } else if statusPending == .CHECK {
                self.onCheckCardCallback?(dataPending)
            } else if statusPending == .DISLIKE {
                self.onDislikeCardCallback?(dataPending)
            } else if statusPending == .LIKE {
                self.onLikeCardCallback?(dataPending)
            }
            
        }
       
        
        self.dealPending = nil
        self.statusPending = .NONE
    }
    
    private func reinitView(_ idx: Int) {
        self.timerForUndo()
  
        self.draggableCardViews.removeAll()
        
        self.dealPending = self.deals[idx]
        
        self.deals.remove(at: idx)
        self.clearAtIndex(idx)
        
        self.reloadData(forceAnimation: false)
    }
}

extension OneDealSwipe: KolodaViewDataSource {
    
    public func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return deals.count
    }
    
    public func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    public func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        if index < 0 || index >= self.deals.count {
            return UIView()
        }
        
        var data = deals[index]
        data["index"] = index
        
        let swipeDeal = SwipeDeal().loadView()
        swipeDeal.defaultAngle = self.getAngleFromIndex(index)
        swipeDeal.config(data) { (info) in
            
        }
        
        return swipeDeal
    }
    
    private func getAngleFromIndex(_ index: Int) -> CGFloat {
        var angle: CGFloat = 0
        if index == 0 {
            angle = 0
        } else if index == 1 {
            angle = -0.10
        } else if index == 2 {
            angle = 0.05
        }
        
        return angle
    }

}
