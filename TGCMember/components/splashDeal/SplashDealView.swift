//
//  SplashDealView.swift
//  TGCMember
//
//  Created by vang on 8/12/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class SplashDealView: UIView {
    
    @IBOutlet weak var widthPhotoConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightPhotoConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnMoreDetails: UIButton!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var subBgImage: UIImageView!
    
    @IBOutlet weak var lblSaleOff: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblTitleDescription: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var photoView: UIImageView!
    
    private var data: [String: Any?]?
    
    private var onClickedDetailCallback: DataCallback?
    private var onClickedCloseCallback: DataCallback?
    
    func loadTheme(_ theme: TEMPLATE_SPLASH_SCREEN = .ONE) -> SplashDealView {
        let splashDealView = Bundle.main.loadNibNamed("SplashDealView", owner: self, options: nil)?[theme.templateIndex] as! SplashDealView
        
        return splashDealView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        decorate()
    }
    
    private func decorate() {
        self.contentView?.layer.cornerRadius = 4.0
        self.contentView?.layer.masksToBounds = true
        
        //self.btnMoreDetails?.setTitleUnderline(MLocalized("MORE DETAILS"), color: .white, font: UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: 16))
    }
    
    @IBAction func onClickedMoreDetails(_ sender: Any) {
        self.closeWithAnimation { (finished) in
            self.onClickedDetailCallback?(self.data)
        }
        
    }
    
    @IBAction func onClcikedClose(_ sender: Any) {
    
        self.closeWithAnimation { (finished) in
            self.onClickedCloseCallback?(self.data)
        }
    }
    
    private func fillData() {
        if let tData = self.data, let template = tData["Template"] as? [String: Any?], let details = tData["DealShow"] as? [String: Any?] {
            
            var bgURL = ""
            var subBgURL = ""
            let logoURL = details["Logo"] as? String ?? ""
            let photoURL = details["Photo"] as? String ?? ""
            
            let typeTpl = Utils.getTemplateSplash(template)
            
            if typeTpl == .ONE || typeTpl == .THREE {
                bgURL = template["BackgroundImageUrl"] as? String ?? ""
                subBgURL = template["SubBackgroundImageUrl"] as? String ?? ""
            } else {
                subBgURL = template["BackgroundImageUrl"] as? String ?? ""
            }
      
            self.bgImage?.sd_setImage(with: URL(string: bgURL), completed: { (_, _, _, _) in
               
            })
            
            self.subBgImage?.sd_setImage(with: URL(string: subBgURL), completed: { (_, _, _, _) in
                
            })
            
            self.logoImage?.sd_setImage(with: URL(string: logoURL), completed: { (_, _, _, _) in
                 
            })
            
            self.photoView?.sd_setImage(with: URL(string: photoURL), completed: { (image, error, _, _) in
                if error == nil && image != nil {
                    let ratio: CGFloat = image!.size.width / image!.size.height
                    
                    let tHeight = min(image!.size.height/3, 138)
                    
                    self.heightPhotoConstraint?.constant = tHeight
                    self.widthPhotoConstraint?.constant = tHeight * ratio
                } else {
                    
                }
            })
            
            self.btnMoreDetails?.sd_setImage(with: URL(string: template["ButtonImageUrl"] as? String ?? ""), for: .normal, completed: { (image, error, _, _) in
                if error == nil && image != nil {

                } else {

                }
            })
            
            if typeTpl == .ONE  {
                self.lblDescription?.setAttribute(details["Description"] as? String ?? "", color: .black, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13.0), spacing: 2)
            } else if typeTpl == .THREE  {
                self.lblDescription?.setAttribute(details["Description"] as? String ?? "", color: .black, font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 12.0), spacing: 2)
            } else if typeTpl == .FOUR {
                self.lblDescription?.setAttribute(details["Description"] as? String ?? "", color: .black, font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 11.0), spacing: 4)
            }
            
            self.lblSaleOff?.text = details["SmallTitle"] as? String ?? ""
            self.lblDiscount?.text = details["BigTitle"] as? String ?? ""
            self.lblTitleDescription?.text = details["TitleDescription"] as? String ?? ""
        
            if typeTpl == .THREE {
                self.lblDiscount?.textColor = .white
                self.lblTime?.text = "\(Utils.stringFromStringDate(details["StartDateAt"] as? String, toFormat: "MMM dd"))-\(Utils.stringFromStringDate(details["EndDateAt"] as? String, toFormat: "MMM dd"))"
            } else {
                var strTime = LOCALIZED.from_to_format.translate
                strTime = strTime.replacingOccurrences(of: "[%__TAG_START_DATE__%] ", with: Utils.stringFromStringDate(details["StartDateAt"] as? String, toFormat: "MMM dd"))
                strTime = strTime.replacingOccurrences(of: "[%__TAG_END_DATE__%] ", with: Utils.stringFromStringDate(details["EndDateAt"] as? String, toFormat: "MMM dd"))
                
                self.lblTime?.text = strTime
            }
            
            if typeTpl == .ONE {
                self.logoImage?.layer.cornerRadius = 36.0
            } else if typeTpl == .TWO {
                self.logoImage?.layer.cornerRadius = 11.0
            } else if typeTpl == .THREE {
                self.logoImage?.layer.cornerRadius = 10.6
            } else if typeTpl == .FOUR {
                self.logoImage?.layer.cornerRadius = 8.8
            }
            
            self.logoImage?.layer.masksToBounds = true
            self.logoImage?.layer.applySketchShadow(color: UIColor(31, 30, 61), alpha: 0.32, x: 0, y: 2, blur: 10, spread: 0)
        
        }
    }
    
    private func decorate(_ typeTpl: TEMPLATE_SPLASH_SCREEN = .ONE) {
        if typeTpl == .ONE {
            
        } else if typeTpl == .TWO {
            
        } else if typeTpl == .THREE {
            
        } else if typeTpl == .FOUR {
            
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedDetailCallback _onClickedDetailCallback: DataCallback?, onClickedCloseCallback _onClickedCloseCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedDetailCallback = _onClickedDetailCallback
        self.onClickedCloseCallback = _onClickedCloseCallback
        
        self.fillData()
    }
    
    public func showInView(_ vc: UIViewController) {
        self.alpha = 0
        self.frame = vc.view.bounds
        
        vc.view.addSubview(self)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1
            
        }, completion: { (finished: Bool) in
           self.alpha = 1
        })

    }

    private func closeWithAnimation(_ completed:((Bool) -> Void)? = nil) {

        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.alpha = 0
        }, completion: { (finished: Bool) in
            self.alpha = 0
            
            completed?(finished)
        
            self.removeFromSuperview()
        })

    }
    

}
