//
//  ResultUpgradeAlert.swift
//  TGCMember
//
//  Created by vang on 1/10/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class NewResultAlertView: UIView {
    
    private var onClickedActionCallback: DataCallback?
    
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var btnAction: UIButton!
    
    private var data: [String: Any?]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    private func decorate() {
        coverView?.layer.cornerRadius = 5.0
        coverView?.layer.masksToBounds = true
        
 
        self.btnAction?.layer.cornerRadius = 3.0
        self.btnAction?.layer.applySketchShadow()
        
    }
    
    @IBAction func onClickedAction(_ sender: Any) {
        self.onClickedActionCallback?(self.data)
    }

   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> NewResultAlertView {
        let aView = Bundle.main.loadNibNamed("NewResultAlertView", owner: self, options: nil)?[0] as! NewResultAlertView
        
        return aView
    }
    
    private func fillData() {
        if let _data = self.data {
            let success = _data["success"] as? Bool ?? false
            let tTitle = _data["title"] as? String ?? ""
            let tMessage = _data["message"] as? String ?? ""
            
            self.btnAction?.setTitle(success ? LOCALIZED.text_continue.translate : LOCALIZED.ok_i_got_it.translate, for: .normal)
            self.imageAlert.image = UIImage(named: success ? "report_message_ic" : "error_image")
            
            self.lblTitle?.text = tTitle
            self.lblMessage?.setAttribute(tMessage, color: UIColor("rgb 39 39 39"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 3.0)
            self.lblMessage?.textAlignment = .center
            
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedActionCallback _onClickedActionCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedActionCallback = _onClickedActionCallback
        
        self.fillData()
    }
}
