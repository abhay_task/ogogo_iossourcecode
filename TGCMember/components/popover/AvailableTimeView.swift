//
//  AvailableTimeView.swift
//  TGCMember
//
//  Created by vang on 5/30/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class AvailableTimeView: UIViewController {

    @IBOutlet weak var tbContent: UITableView!
    
    var availableTimes: [[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.tbContent.register(UINib(nibName: "AvailableTimeCell", bundle: nil), forCellReuseIdentifier: "AvailableTimeCell")
    }
    
    public func updateData() {
        self.tbContent.reloadData()
    }


}


extension AvailableTimeView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return availableTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = availableTimes[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableTimeCell") as? AvailableTimeCell
        
        cell?.config(data)
        
        return cell!
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    
    
}
