//
//  AvailableTimeCell.swift
//  TGCMember
//
//  Created by vang on 5/30/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class SortWalletCell: UITableViewCell {

    private var data: [String: Any?]?
    private var onClickedCell: DataCallback?
    
    @IBOutlet weak var imageSort: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCell {
            callback(self.data)
        }
    }
    
    private func fillData() {
        if let tData = self.data {
           self.lblText.text = "\(tData["text"] as? String ?? "")"
           self.imageSort.image = UIImage(named: tData["image"] as? String ?? "")
        } else {
            self.lblText.text = ""
        }
        
    }
    
    public func config(_ data: [String: Any?]?, onClickedCell _onClickedCell: DataCallback?) -> Void {
        self.data = data
        self.onClickedCell = _onClickedCell
        
        self.fillData()
    }
}
