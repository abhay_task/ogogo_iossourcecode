//
//  ResultUpgradeAlert.swift
//  TGCMember
//
//  Created by vang on 1/10/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class CustomAlert: UIView {
    
    private var onClickedConfirmCallback: DataCallback?
    private var onClickedCancelCallback: DataCallback?
    
    @IBOutlet weak var iconAlert: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    private var data: [String: Any?]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    private func decorate() {
        coverView?.layer.cornerRadius = 5.0
        coverView?.layer.masksToBounds = true
        
        self.btnConfirm?.layer.cornerRadius = 3.0
        self.btnConfirm?.layer.applySketchShadow()
    
    }
    
    @IBAction func onClickedConfirm(_ sender: Any) {
        Utils.closeCustomAlert(forceTop: true)
        
        self.onClickedConfirmCallback?(self.data)
    }

    @IBAction func onClickedCancel(_ sender: Any) {
        Utils.closeCustomAlert(forceTop: true)
        
        self.onClickedCancelCallback?(self.data)
    }
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadOneButtonView() -> CustomAlert {
        let aView = Bundle.main.loadNibNamed("CustomAlert", owner: self, options: nil)?[0] as! CustomAlert
        
        return aView
    }
    
    func loadTwoButtonView() -> CustomAlert {
        let aView = Bundle.main.loadNibNamed("CustomAlert", owner: self, options: nil)?[1] as! CustomAlert
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
            let success = tData["success"] as? Bool ?? false
            let tMessage = tData["message"] as? String ?? ""
            
            var titleBtnConfirm = tData["titleBtnConfirm"] as? String ?? ""
            if titleBtnConfirm == "" {
                titleBtnConfirm = LOCALIZED.txt_ok_title.translate
            }
            
            var titleBtnCancel = tData["titleBtnCancel"] as? String ?? ""
            if titleBtnCancel == "" {
                titleBtnCancel = LOCALIZED.txt_cancel_title.translate
            }
            
            self.btnConfirm?.setTitle(titleBtnConfirm, for: .normal)
            
            self.btnCancel?.setTitleUnderline(titleBtnCancel, color: UIColor("rgb 228 109 87"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13))
            
            if let _ = tData["success"] as? Bool {
                self.iconAlert?.image = UIImage(named: success ? "report_message_ic" : "error_image")
            } else {
                self.iconAlert?.image = UIImage(named: "info_ic")
            }
            
            self.lblMessage?.setAttribute(tMessage, color: UIColor("rgb 39 39 39"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 3.0)
            self.lblMessage?.textAlignment = .center
            
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedConfirmCallback _onClickedConfirmCallback: DataCallback? = nil, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedConfirmCallback = _onClickedConfirmCallback
        self.onClickedCancelCallback = _onClickedCancelCallback
        
        self.fillData()
    }
}
