//
//  PaymentCardView.swift
//  TGCMember
//
//  Created by vang on 7/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class PaymentCardView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imageBrand: UIImageView!
    @IBOutlet weak var lblNumCard: UILabel!
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var imageCheck: UIImageView!
    
    @IBOutlet weak var loadingBrand: UIActivityIndicatorView!
    
    @IBAction func onClickedCard(_ sender: Any) {
        if var tData = self.data {
            tData["isSelected"] = !isSelected
            self.onClickedCardCallback?(tData)
        }
    }
    
    private var isSelected: Bool = false
    private var data: [String: Any?]?
    private var onClickedCardCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateCard(_:)), name: .SELECTED_CARD_PAYMENT, object: nil)
        
        self.imageBrand.contentMode = .scaleAspectFit
        
        updateSelect(false)
    }
    
    
    @objc func onUpdateCard(_ notification: Notification) {
        self.updateSelect(false)
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                isSelected = !isSelected
                self.updateSelect(isSelected)
            } else {
                isSelected = false
            }
        } else {
            isSelected = false
        }
    }
 
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func updateSelect(_ isSelect: Bool) {
        contentView?.layer.cornerRadius = 2.0
        contentView?.layer.masksToBounds = true
        contentView?.layer.borderWidth = 0.5
        contentView?.layer.borderColor = isSelect ? UIColor(255, 193, 6).cgColor : UIColor(205, 207, 214).cgColor
        imageCheck.image = isSelect ? UIImage(named: "check_circle_yellow") : UIImage(named: "check_circle_empty")
    }
    
    
    func loadView() -> PaymentCardView {
        let paymentCardView = Bundle.main.loadNibNamed("PaymentCardView", owner: self, options: nil)?[0] as! PaymentCardView
        
        return paymentCardView
    }
    
    private func fillData() {
        if let tData = self.data {
            self.loadingBrand.startAnimating()
            self.imageBrand.sd_setImage(with:  URL(string: tData["Icon"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingBrand.stopAnimating()
            })
           
            self.lblNumCard.text = "**** **** **** \(tData["Last4"] as? String ?? "")"
            self.lblBrand.text = "\((tData["Brand"] as? String ?? "").upperCaseFirstLetter()) \((tData["Funding"] as? String ?? "").upperCaseFirstLetter())"
            
            self.imageCheck.image = UIImage(named: "check_circle_empty")
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback?) -> Void {
        
        self.data = data
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .SELECTED_CARD_PAYMENT, object: nil)
    }
}
