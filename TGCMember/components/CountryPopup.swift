//
//  PurposePopup.swift
//  TGCMember
//
//  Created by vang on 10/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CountryPopup: UIViewController {

    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var coverBtnCancel: UIView!
    @IBOutlet weak var coverInput: UIView!
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var coverBtnConfirm: UIView!
    
    @IBOutlet weak var tblCountry: UITableView!
    private var listCountries: [[String: Any?]] = []
    
    
    var selectedCountry: [String: Any?]? = nil

    var callback: DataCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tblCountry.register(UINib(nibName: "CountryTripCell", bundle: nil), forCellReuseIdentifier: "CountryTripCell")
        decorate()
        
        tfCountry.becomeFirstResponder()
        
        fillData()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            GLOBAL.GlobalVariables.heightKeyboard = keyboardSize.height
            self.heightContentConstraint.constant = SCREEN_HEIGHT - (Utils.isIPhoneNotch() ? 40 : 15)  - GLOBAL.GlobalVariables.heightKeyboard - 10
        }
    }

    @objc func keyboardWillHide(notification: Notification) {

    }

    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @IBAction func onClickedConfirm(_ sender: Any) {
    
        self.callback?(self.selectedCountry)
        
        Utils.dismissKeyboard()
        self.dismissFromSuperview()
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        Utils.dismissKeyboard()
        self.dismissFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func filterCountry(_ keyword: String = "") -> [[String: Any?]] {
        if keyword == "" {
            return GLOBAL.GlobalVariables.countriesCreateTrip
        }
        
        let tCountries = GLOBAL.GlobalVariables.countriesCreateTrip.filter { (item) -> Bool in
            if let name = item["countryName"] as? String {
                if name.lowercased().contains(keyword.lowercased()) {
                    return true
                }
                
                return false
            }
            
            return false
        }
        
        return tCountries
    }
    
    
    private func fillData() {
        self.lblTitle.text = LOCALIZED.where_will_you_plan_to_go.translate
        
        self.listCountries = GLOBAL.GlobalVariables.countriesCreateTrip
        
        self.tblCountry.reloadData()
    }
    
    private func decorate() {
        self.heightContentConstraint.constant = SCREEN_HEIGHT - (Utils.isIPhoneNotch() ? 40 : 15)  - GLOBAL.GlobalVariables.heightKeyboard - 10
        self.topConstraint.constant = Utils.isIPhoneNotch() ? 40 : 15
        
        self.coverContent.layer.cornerRadius = 5.0
        self.coverContent.layer.masksToBounds = true
        
        self.coverInput.layer.cornerRadius = 3.0
        self.coverInput.layer.masksToBounds = true
        self.coverInput.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInput.layer.borderWidth = 0.5
        
        self.coverBtnCancel.backgroundColor = .clear
        self.coverBtnCancel.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverBtnCancel.layer.borderWidth = 1
        self.coverBtnCancel.layer.cornerRadius = 5.0

        self.btnCancel.setTitle(LOCALIZED.txt_cancel_title.translate, for: .normal)
        self.btnConfirm.setTitle(LOCALIZED.txt_confirm_title.translate, for: .normal)
        self.tfCountry.placeholder = LOCALIZED.eg_tokyo_japan.translate
        
        _ = self.validateForm()
    }
    
  
    
    private func validateForm() -> Bool {
        let isValid = selectedCountry == nil ? false : true
        
        self.coverBtnConfirm.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.coverBtnConfirm.layer.cornerRadius = 5.0
        self.coverBtnConfirm.isUserInteractionEnabled = isValid
        
        if isValid {
            self.coverBtnConfirm.layer.applySketchShadow()
        } else {
            self.coverBtnConfirm.layer.removeSketchShadow()
        }
        
        return isValid
    }

    private func onSelectedCountry(_ data: [String: Any?]?) {
        self.selectedCountry = data
    
        _ = validateForm()
    }
}

extension CountryPopup : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listCountries.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tCountry = self.listCountries[indexPath.row]
        
        if let temp = self.selectedCountry {
            tCountry["_currentCountryCode"] = temp["countryCode"] as? Int ?? 0
            tCountry["_currentCountryName"] = temp["countryName"] as? String ?? ""
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTripCell") as? CountryTripCell
        
        cell?.config(tCountry, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            self.onSelectedCountry(data)
        })
        
        return cell!
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 35.0
      }

}

extension CountryPopup: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            self.listCountries = self.filterCountry(updatedText)
            
            self.tblCountry.reloadData()
        }
        
        return true
    }

}
