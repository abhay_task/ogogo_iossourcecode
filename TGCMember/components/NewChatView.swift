//
//  NewChatView.swift
//  TGCMember
//
//  Created by vang on 7/18/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NewChatView: UIView {

    @IBOutlet weak var loadingAvatar: UIActivityIndicatorView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    private var data: [String: Any?]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        avatar.layer.cornerRadius = 25.0
        avatar.layer.masksToBounds = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> NewChatView {
        let newChatView = Bundle.main.loadNibNamed("NewChatView", owner: self, options: nil)?[0] as! NewChatView
        
        return newChatView
    }
    
    public func config(_ data: [String: Any?]?) -> Void {
        self.data = data
        
        if let tData = self.data {
            self.lblTitle.text = tData["title"] as? String
            self.lblMessage.text = tData["body"] as? String
            
            self.loadingAvatar.startAnimating()
            self.avatar.sd_setImage(with:  URL(string: tData["avatar_url"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingAvatar.stopAnimating()
            })
        }
        
    }
}
