//
//  NavigationBarViewController.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NavigationBarViewController: UIViewController {
    
    var _leftClicked: DataCallback?
    var _title: String?
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.lblTitle.text = self._title
    }
    
    @IBAction func leftClicked(_ sender: Any) {
        self._leftClicked!(nil)
    }
    
    
    open func initBarWith(title: String?, leftClicked: DataCallback?) -> Void {
        self._leftClicked = leftClicked
        self._title = title
        
    }
    
    
}
