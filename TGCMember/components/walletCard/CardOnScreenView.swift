//
//  CardOnScreenView.swift
//  TGCMember
//
//  Created by vang on 8/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CardOnScreenView: BaseView {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var lblSuggestUpgrade: UILabel!
    @IBOutlet weak var maskWarter: UIView!
    @IBOutlet weak var coverWartermask: UIView!
    @IBOutlet weak var coverAction: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var contentCard: UIView!
    @IBOutlet weak var coverChat: HighlightableView!
    @IBOutlet weak var coverUseDeal: HighlightableView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblChat: UILabel!
    @IBOutlet weak var lblUseDeal: UILabel!
    @IBOutlet weak var coverCard: UIView!
    
    
    @IBOutlet weak var coverChatCenter: HighlightableView!
    @IBOutlet weak var lblChatCenter: UILabel!
    
    @IBOutlet weak var topActionConstraint: NSLayoutConstraint!
    @IBOutlet weak var topTrashConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var bottomActionConstraint: NSLayoutConstraint!
    private var data: [String: Any?]?
    
    private var onClickedCloseCallback: DataCallback?
    private var onClickedUseDealCallback: DataCallback?
    private var onClickedDeleteCallback: DataCallback?
    private var onClickedChatCallback: DataCallback?
    private var onClickedUpgradeCallback: DataCallback?
    private var onClickedCardCallback: DataCallback?
    
    private var subview: UIView?
    
    
    func loadView() -> CardOnScreenView {
        let aView = Bundle.main.loadNibNamed("CardOnScreenView", owner: self, options: nil)?[0] as! CardOnScreenView
        
        return aView
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        Utils.closeCustomAlert()
        
        if let tData = self.data, let cardData = tData["cardData"] as? [String: Any?] {
            self.onClickedUpgradeCallback?(cardData)
        }
        
    }
    
    @IBAction func onClickedUseDeal(_ sender: Any) {
        if let tData = self.data, let cardData = tData["cardData"] as? [String: Any?] {
            self.onClickedUseDealCallback?(cardData)
        }
    }
    
    @IBAction func onClickedChat(_ sender: Any) {
        if let tData = self.data, let cardData = tData["cardData"] as? [String: Any?] {
            self.onClickedChatCallback?(cardData)
        }
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
        self.onClickedCloseCallback?(self.data)
        
    }
    
    @IBAction func onClickedDelete(_ sender: Any) {
        self.showConfirmDeleteAlert()
        
        
    }
    
    private func showConfirmDeleteAlert() {
        let content = ConfirmDeleteDealView().loadView()
        
        content.config(self.data, onClickedYesCallback: { [weak self] (data) in
            guard let self = self else {return}
            
                Utils.closeCustomAlert()
                
                if let tData = data, let cardData = tData["cardData"] as? [String: Any?] {
                    self.onClickedDeleteCallback?(cardData)
                }
            }, onClickedNoCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
                
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        decorate()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.alpha = 0
        
    }
    
    private func decorate() {
        self.btnDelete.setTitleUnderline(" \(LOCALIZED.move_to_trash.translate)", color: UIColor("rgb 228 109 87"), font: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 18))
        self.lblUseDeal.text = LOCALIZED.use_deal.translate
        
        self.coverWartermask?.layer.cornerRadius = 22
        self.maskWarter?.layer.cornerRadius = 22
        self.coverChat?.layer.cornerRadius = 3.0
        self.coverChat?.layer.borderWidth = 1.0
        self.coverChat?.layer.borderColor = UIColor("rgb 0 202 234").cgColor
        self.coverChat?.layer.masksToBounds = true
        
        self.coverChatCenter?.layer.cornerRadius = 3.0
        self.coverChatCenter?.layer.borderWidth = 1.0
        self.coverChatCenter?.layer.borderColor = UIColor("rgb 0 202 234").cgColor
        self.coverChatCenter?.layer.masksToBounds = true
        
        self.coverUseDeal?.layer.cornerRadius = 3.0
        self.coverUseDeal?.layer.borderWidth = 1.0
        self.coverUseDeal?.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        self.coverUseDeal?.layer.masksToBounds = true
        
        self.topActionConstraint?.constant = SCREEN_HEIGHT <= 667 ? 20 : 50
        self.topTrashConstraint?.constant = SCREEN_HEIGHT <= 667 ? 13 : 30
        
        var fullText = LOCALIZED.you_are_now_browsing_as_a_free_member.translate
        fullText = fullText.replacingOccurrences(of: "[%__TAG_UPGRADE_TO_USE_DEALS__%]", with: LOCALIZED.upgrade_to_use_deals.translate)
        
        self.lblSuggestUpgrade?.setTextWithUnderline(fullText, .white, UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), highlightText: [LOCALIZED.upgrade_to_use_deals.translate], highlightColor: [.white], highlightFont: [UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13)])
        
    }
    
    private func fillData() {
        self.contentCard.alpha = 0
        self.coverAction?.alpha = 0
        self.btnClose?.alpha = 0
        
        if let tData = self.data {
            let cardPoint = tData["cardPoint"] as? CGPoint ?? .zero
            let cardData = tData["cardData"] as? [String: Any?]
        
            var isAllowUseDeal: Bool = false
            var isAllowMoveToTrash: Bool = true
            
            if let _cardData = cardData {
                isAllowUseDeal = _cardData["YouCanUseThisDeal"] as? Bool ?? false
            }
            
            if let tSubview = self.subview {
                tSubview.removeFromSuperview()
            }
            
            let dealType = Utils.getDealType(cardData)
            
            if dealType == .BASIC || dealType == .LOYALTY {
                isAllowMoveToTrash = false
            }
            
            if dealType == .LOYALTY {
                isAllowUseDeal = false
            }
            
            if dealType == DEAL_TYPE.SWEEPSTAKE {
                let tSubview = BigSweepstakeCard().loadView()
                tSubview.config(cardData, onCompletedCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    }, onClickedChatCallback: { [weak self] (data) in
                        guard let self = self else {return}
                        
                        if let tData = data, let summary = tData["VendorSummary"] as? [String: Any?] {
                            self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
                        }
                })
                
                self.subview = tSubview
            } else {
                let tSubview = FlipDealView().loadView()
                tSubview.config(cardData, onClickedCardCallback: { [weak self] (data) in
                    guard let self = self else { return }
                    
                    self.onClickedCardCallback?(data)
                })
                
                self.subview = tSubview
            }
            
            bottomActionConstraint?.constant = (!isAllowUseDeal && SCREEN_HEIGHT <= 667) ? -30 : 0
            self.coverWartermask?.isHidden = isAllowUseDeal || dealType == .LOYALTY
            self.coverChat?.isHidden = !isAllowUseDeal
            self.coverUseDeal?.isHidden = !isAllowUseDeal
            self.coverChatCenter?.isHidden = isAllowUseDeal
            
            self.btnDelete?.isHidden = !isAllowMoveToTrash
            
            self.contentCard.frame = CGRect(x: 0, y: 0, width: 318, height: 480)
            self.subview?.frame = self.contentCard.bounds
            
            self.contentCard.addSubview(self.subview!)
            
            self.appearFromPoint(cardPoint)
            
        }
    }
    
    public func addAnimationClaim(_ completed: FinishedCallback? = nil) {
        
        // FORCE SHOW FRONT VIEW
        if let tSubview = self.subview {
            if tSubview is FlipDealView {
                let flipDeal = tSubview as! FlipDealView
                
                flipDeal.flip(fromScreen: .BACK, toScreen: .FRONT) {
                    flipDeal.addAnimationClaim(forFront: true, completed: completed)
                }
                
            } else if tSubview is BigSweepstakeCard {
                let sweepCard = tSubview as! BigSweepstakeCard
                sweepCard.addAnimationClaim(completed)
            }
        }
        
    }
    
    private func appearFromPoint(_ point: CGPoint = .zero) {
        self.alpha = 1
        let orginalFrame = self.contentCard.frame
        
        self.contentCard.frame = CGRect(x: 0, y: 0, width: orginalFrame.size.width, height: orginalFrame.size.height)
        self.contentCard.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.2,
                       animations: {
                        self.alpha = 1
                        self.contentCard.transform = CGAffineTransform(scaleX: 1, y: 1)
                        self.contentCard.alpha = 1
                        self.coverAction.alpha = 1
                        self.btnClose?.alpha = 1
                        self.contentCard.frame = orginalFrame
        },
                       completion: { [weak self] _ in
                        guard let self = self else {return}
                        self.coverAction.alpha = 1
                        self.btnClose?.alpha = 1
                        self.alpha = 1
                        self.contentCard.frame = orginalFrame
                        self.contentCard.transform = CGAffineTransform.identity
        })
    }
    
    public func config(_ data: [String: Any?]?, onClickedCloseCallback _onClickedCloseCallback: DataCallback? = nil, onClickedUseDealCallback  _onClickedUseDealCallback: DataCallback? = nil, onClickedDeleteCallback  _onClickedDeleteCallback: DataCallback? = nil, onClickedChatCallback  _onClickedChatCallback: DataCallback? = nil, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onClickedCloseCallback = _onClickedCloseCallback
        self.onClickedUseDealCallback = _onClickedUseDealCallback
        self.onClickedDeleteCallback = _onClickedDeleteCallback
        self.onClickedChatCallback = _onClickedChatCallback
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }
}
