//
//  DealCardMiddle.swift
//  TGCMember
//
//  Created by vang on 8/9/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import WebKit

class DealCardMiddle: BaseCardMiddle {
    
    private var data: [String: Any?]?
    
    private var onClickedBackCallback: DataCallback?
    private var onClickedNextCallback: DataCallback?
    private var onClickedChatCallback: DataCallback?
    
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var coverCate: UIView!
    @IBOutlet weak var imageRating: UIImageView!
    @IBOutlet weak var lblTitleLine1: UILabel!
    @IBOutlet weak var lblTitleLine2: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var lblLongDescription: UILabel!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var contentView: UIView!

    
    @IBOutlet weak var wkWebview: WKWebView!

    func loadView() -> DealCardMiddle {
        let aView = Bundle.main.loadNibNamed("DealCardMiddle", owner: self, options: nil)?[0] as! DealCardMiddle
        
        return aView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.wkWebview?.uiDelegate = self
        self.wkWebview?.navigationDelegate = self
        self.wkWebview?.scrollView.showsVerticalScrollIndicator = false
        self.wkWebview?.scrollView.showsHorizontalScrollIndicator = false
        
        decorate()
    }
    
    @IBAction func onClickedChat(_ sender: Any) {
        self.onClickedChatCallback?(self.data)
    }
    
    private func decorate() {
        self.coverCate?.isHidden = true
        
        bodyView?.layer.cornerRadius = 10.0
        bodyView?.layer.masksToBounds = true
        
        contentView?.layer.cornerRadius = 25.0
        contentView?.layer.applySketchShadow(color: UIColor(92, 92, 92), alpha: 0.5, x: 2, y: 2, blur: 4, spread: 0)
        self.bgImage?.layer.cornerRadius = 25.0
        
        self.coverCate?.layer.cornerRadius = 20.0
        self.coverCate?.layer.borderColor =  UIColor.white.cgColor
        self.coverCate?.layer.borderWidth = 1.0
        self.coverCate?.layer.masksToBounds = true
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.onClickedBackCallback?(self.data)
    }
    
    @IBAction func onClickedNext(_ sender: Any) {
        self.onClickedNextCallback?(self.data)
    }
    
    private func fillData() {
        
        if let tData = self.data {
            let dealShowType = Utils.getDealType(tData)
            
            if dealShowType == .BLACK {
                self.bgImage?.image = UIImage(named: "black_deal_bg")
            } else if dealShowType == .SILVER {
                self.bgImage?.image = UIImage(named: "silver_deal_bg")
            } else if dealShowType == .GOLD {
                self.bgImage?.image = UIImage(named: "gold_deal_bg")
            } else if dealShowType == .BASIC {
                self.bgImage?.image = UIImage(named: "basic_deal_bg")
            } else if dealShowType == .TEO {
                self.bgImage?.image = UIImage(named: "teo_deal_bg")
            } else if dealShowType == .LOYALTY {
                self.bgImage?.image = UIImage(named: "loyalty_deal_bg")
            }
            
            let content = tData["ContentDetail"] as? String ?? ""
            
            let urlPath = Bundle.main.url(forResource: "\(Constants.DEFAULT_FONT)", withExtension: "ttf")
            
            let htmlCode = String(format: Constants.CONTENT_WEB_FORMAT, content)
            self.wkWebview?.loadHTMLString(htmlCode, baseURL: urlPath)
            
            let summary = tData["VendorSummary"] as? [String: Any?] ??  [String: Any?]()
            let category = summary["Category"] as? [String: Any?] ??  [String: Any?]()
            
            var title1 = tData["TitleLine1"] as? String ?? ""
            var title2 = tData["TitleLine2"] as? String ?? ""
            title1 = title1.capitalized
            title2 = title2.capitalized
            
            self.lblTitleLine1.text = title1 == "" ? " " : title1
            self.lblTitleLine2.text = title2 == "" ? " " : title2
            //self.lblCategoryName.text = category["Name"] as? String ?? " "
         
            let cateURL = category["ImageUrlSelected"] as? String ?? ""
            self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
                
            })

            self.lblLongDescription.setAttribute(tData["LongDescription"] as? String ?? " ", color: .black, font: UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 14), spacing: 3)
            

        }
        
    }
    
    
    override func config(_ data: [String: Any?]?, onClickedBackCallback _onClickedBackCallback: DataCallback? = nil, onClickedNextCallback _onClickedNextCallback: DataCallback? = nil, onClickedChatCallback _onClickedChatCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedBackCallback = _onClickedBackCallback
        self.onClickedNextCallback = _onClickedNextCallback
        self.onClickedChatCallback = _onClickedChatCallback
        
        self.fillData()
    }
    
    
}

extension DealCardMiddle:  WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    // self.heightWebviewConstraint.constant = height as! CGFloat
                    
                    self.contentView.isHidden = false
                    
                    Utils.dismissLoading()
                })
            }
            
        })
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            let tLink = navigationAction.request.url?.absoluteString
            
            if let link = tLink {
                Utils.openURL(link)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    
    
}
