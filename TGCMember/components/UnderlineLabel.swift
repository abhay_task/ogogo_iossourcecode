//
//  UnderlineLabel.swift
//  TGCMember
//
//  Created by Vang Doan on 5/12/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class UnderlineLabel: UILabel {
    
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}
