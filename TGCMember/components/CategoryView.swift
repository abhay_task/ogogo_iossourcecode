//
//  CategoryView.swift
//  TGCMember
//
//  Created by vang on 11/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CategoryView: UIView {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    private var listCategories:[[String: Any?]] = []
    
    private var data:[String: Any?]?
    private var onSelectedCategoryCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        
        self.decorate()
    }
    
    private func decorate() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> CategoryView {
        let aView = Bundle.main.loadNibNamed("CategoryView", owner: self, options: nil)?[0] as! CategoryView
        
        return aView
    }
    
    private func fillData() {
        self.getListCategories()
        
    }
    
    public func updateData() {
        self.listCategories = GLOBAL.GlobalVariables.listCategoriesOnMap
        
        self.collectionView.reloadData()
    }
    
    public func updateSelectedCategory(isReload: Bool = true) {
        if isReload {
            self.collectionView.reloadData()
        }
        
    }
    
    private func getListCategories() {
        self.loading.stopAnimating()
        self.listCategories = GLOBAL.GlobalVariables.listCategoriesOnMap
    }
    
    func config(_ data: [String: Any?]?, onSelectedCategoryCallback _onSelectedCategoryCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onSelectedCategoryCallback = _onSelectedCategoryCallback
        self.fillData()
    }
    
    private func sizeWithText(_ pText: String = "", _ pFont: UIFont?) -> CGSize {
        if let font = pFont {
            let fontAttributes = [NSAttributedString.Key.font: font]
            
            let size = (pText as NSString).size(withAttributes: fontAttributes)
            
            return size
        }
        
        return CGSize(width: 0, height: 0)
    }
    
}


// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension CategoryView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data = listCategories[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        cell.config(data, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            self.onSelectedCategoryCallback?(data)
            
        })
        
        return cell
        
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tData = listCategories[indexPath.row]
        
        let tName = tData["Name"] as? String ?? ""
       
        let tSize = self.sizeWithText(tName, UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 10))
        
        let tWidth = tSize.width + (indexPath.row == 0 ? 100 : 70)
        
        
        return CGSize(width: tWidth, height: 50)
    }

    
}
