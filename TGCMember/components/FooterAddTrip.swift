//
//  FooterAddTrip.swift
//  TGCMember
//
//  Created by vang on 1/7/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class FooterAddTrip: UIView {
    
    private var data:[String: Any?]?
    private var onClickedAddTripCallback: DataCallback?!
    
    @IBOutlet weak var lblAddTrip: UILabel!
    @IBOutlet weak var coverAdd: HighlightableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.decorate()
    }
    
    @IBAction func onClickedAdd(_ sender: Any) {
        self.onClickedAddTripCallback?(self.data)
    }
    
    private func decorate() {

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> FooterAddTrip {
        let aView = Bundle.main.loadNibNamed("FooterAddTrip", owner: self, options: nil)?[0] as! FooterAddTrip
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
            //            self.lblTitle.text = tData["Title"] as? String ?? ""
            //
            //            let canUpgrade = tData["CanUp"] as? Bool ?? false
            //            self.coverUpgrade.isHidden = !canUpgrade
            //
            //            self.imageUpgrade.sd_setImage(with:  URL(string: tData["IconUpgrade"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
            //                guard let self = self else {return}
            //            })
        }
    }
    
    
    func config(_ data: [String: Any?]?, onClickedAddTripCallback _onClickedAddTripCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedAddTripCallback = _onClickedAddTripCallback
        
        self.fillData()
    }
    
    
    
}
