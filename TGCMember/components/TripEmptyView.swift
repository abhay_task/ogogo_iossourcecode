//
//  TripEmptyView.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TripEmptyView: UIView {

    private var data: [String: Any?]?
    private var onClickedExploreCallback: DataCallback?
    @IBOutlet weak var heightImgConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTripEmpty: UILabel!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var btnExplorer: HighlightableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        decorate()
    }
    
    private func decorate() {
        if SCREEN_HEIGHT <= 667 {
            self.heightImgConstraint.constant = 230
        } else {
            self.heightImgConstraint.constant = 274
        }
        
        self.lblTripEmpty.text = LOCALIZED.this_trip_is_empty.translate
        self.lblPrompt?.setAttribute(LOCALIZED.new_deals_offers_and_events.translate, color: UIColor(30, 38, 71), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 4)
        self.lblPrompt?.textAlignment = .center
        
        self.btnExplorer.setTitle(LOCALIZED.explore_the_area.translate, for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func onClickedExplore(_ sender: Any) {
        self.onClickedExploreCallback?(self.data)
    }
    
    func loadView() -> TripEmptyView {
        let aView = Bundle.main.loadNibNamed("TripEmptyView", owner: self, options: nil)?[0] as! TripEmptyView
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
         
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedExploreCallback _onClickedExploreCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedExploreCallback = _onClickedExploreCallback
        
        self.fillData()
    }
}
