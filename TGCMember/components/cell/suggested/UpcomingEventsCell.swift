//
//  UpcomingEventsCell.swift
//  TGCMember
//
//  Created by vang on 6/7/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class UpcomingEventsCell: UITableViewCell {

    @IBOutlet weak var upcomingCollectionView: UICollectionView!
    
    @IBOutlet weak var heightCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblUpcomingEvents: UILabel!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var coverDotView: UIView!
    var owner: UIViewController?
    private var data: [String: Any?]?
    private var upcomingEvents: [[String: Any?]] = []

    private var beginScroll: Bool = false
    
    var currentEventIndex: Int = 0
    private var onClickedSeeAllCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(fixedItemCell(_:)), name: .FIXED_ITEM_ON_HORIZONTAL, object: nil)
        
        self.lblUpcomingEvents.text = LOCALIZED.upcoming_events_near_by.translate
        self.btnSeeAll.setTitleUnderline("  \(LOCALIZED.see_all_underline.translate)", color: UIColor(hexFromString: "#E46D57"))

        self.upcomingCollectionView.delegate = self
        self.upcomingCollectionView.dataSource = self
        
        self.upcomingCollectionView.register(UINib(nibName: "EventSuggestedCell", bundle: nil), forCellWithReuseIdentifier: "EventSuggestedCell")
        self.upcomingCollectionView.panGestureRecognizer.addTarget(self, action: #selector(_handleScrollViewGestureRecognizer(_:)))
        self.upcomingCollectionView.addObserver(self, forKeyPath:"contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
        
    }

    @objc func fixedItemCell(_ notification: Notification) {
        setTimeout({
            self.upcomingCollectionView.scrollToItem(at: IndexPath(row: self.currentEventIndex, section: 0), at: .left, animated: true)
        }, 1000)
    }
    
    @IBAction func onClickedSeeAll(_ sender: Any) {
        if let callback = self.onClickedSeeAllCallback {
            callback(self.data)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func config(_ owner: UIViewController?, _ data: [String: Any?]?, onClickedSeeAllCallback _onClickedSeeAllCallback: DataCallback?) -> Void {
        self.owner = owner
        self.data = data
        self.onClickedSeeAllCallback = _onClickedSeeAllCallback
        
        self.fillData()
    }
    
    private func renderDot() {
        for subview in self.coverDotView.subviews {
            subview.removeFromSuperview()
        }
        
        let widthDot: CGFloat = 6
        var leftConstraint:CGFloat = 0
        for index in 0..<upcomingEvents.count {
            let dotView = UIView()
            dotView.backgroundColor = index == currentEventIndex ? UIColor(255, 193, 6) : UIColor(215, 216, 229)
            dotView.layer.cornerRadius = widthDot/2
            dotView.layer.masksToBounds = true
            
            self.coverDotView.addSubview(dotView)
            
            dotView.snp.makeConstraints { (make) in
                make.left.equalTo(self.coverDotView).offset(leftConstraint)
                make.centerY.equalTo(self.coverDotView)
                make.width.equalTo(widthDot)
                make.height.equalTo(widthDot)
            }
            
            leftConstraint += 15.0
        }
        
    }
    
    @objc private func _handleScrollViewGestureRecognizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            beginScroll = true
        case .ended:
            beginScroll = false
            
            setTimeout({
                self.upcomingCollectionView.scrollToItem(at: IndexPath(row: self.currentEventIndex, section: 0), at: .left, animated: true)
            }, 200)
        default:
            break
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if !beginScroll {
            if let dictChange = change, let newPoint: CGPoint = dictChange[NSKeyValueChangeKey.newKey] as? CGPoint {
                //print("CHANGE OBSERVED: \(newPoint.x)")
                
                let index = newPoint.x / (UIScreen.main.bounds.width - 50)
                let roundIndex = round(index)
                
                if (currentEventIndex != Int(max(roundIndex, 0))) {
                    currentEventIndex = Int(max(roundIndex, 0))
                    
                    self.upcomingCollectionView.scrollToItem(at: IndexPath(row: currentEventIndex, section: 0), at: .left, animated: true)
                    
                    self.renderDot()
                }
                
            }
        }
        
    }
 
    private func fillData() {
        if let tData = self.data {
            self.upcomingEvents = tData["list"] as! [[String : Any?]]
            self.renderDot()
            self.upcomingCollectionView.reloadData()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .FIXED_ITEM_ON_HORIZONTAL, object: nil)
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension UpcomingEventsCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return upcomingEvents.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data = self.upcomingEvents[indexPath.row]
        let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventSuggestedCell", for: indexPath) as! EventSuggestedCell
        
        eventCell.config(data, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            setTimeout({
                self.upcomingCollectionView.scrollToItem(at: IndexPath(row: self.currentEventIndex, section: 0), at: .left, animated: true)
            }, 1000)
            
            let eventDetailVC = EventDetailViewController()
            eventDetailVC.eventInfo = data
            
            self.owner?.navigationController?.pushViewController(eventDetailVC, animated: true)
        })
        
        return eventCell

    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width: CGFloat = (UIScreen.main.bounds.width - 60 )/2
        let height = width * (99/158)
        
        return CGSize(width: UIScreen.main.bounds.width - 50, height: height)
    }
    
  
}
