//
//  AmazingTwinCell.swift
//  TGCMember
//
//  Created by vang on 6/7/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class AmazingTwinCell: UITableViewCell {

    private var data1: [String: Any?]?
    private var data2: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var cover1: UIView!
    @IBOutlet weak var loadingImage1: UIActivityIndicatorView!
    @IBOutlet weak var imageStatus1: UIImageView!
    @IBOutlet weak var thumbnail1: UIImageView!
    
    @IBOutlet weak var cover2: UIView!
    @IBOutlet weak var loadingImage2: UIActivityIndicatorView!
    @IBOutlet weak var imageStatus2: UIImageView!
    @IBOutlet weak var thumbnail2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateFavorite(_:)), name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUseDeal(_:)), name: .UPDATE_USE_DEAL, object: nil)
        
        self.cover1.layer.cornerRadius = 3.0
        self.cover1.layer.masksToBounds = true
        
        self.cover2.layer.cornerRadius = 3.0
        self.cover2.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickedItem1(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data1)
        }
    }
    
    @IBAction func onClickedItem2(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data2)
        }
    }
    
    @objc func updateUseDeal(_ notification: Notification) {
        var isUpdate = false
        
        if let uData = notification.object as? [String: Any], var tData = self.data1 {
            
            let uID = uData["DealID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                tData["IsUseDeal"] = true
                self.data1 = tData
            }
        }
        
        if !isUpdate {
            if let uData = notification.object as? [String: Any], var tData = self.data2 {
                
                let uID = uData["DealID"] as? String ?? ""
                let mID = tData["ID"] as? String ?? ""
                
                if uID == mID {
                    tData["IsUseDeal"] = true
                    self.data2 = tData
                }
            }
        }
    }
    
    @objc func updateFavorite(_ notification: Notification) {
        
        var isUpdate = false
        
        if let uData = notification.object as? [String: Any], var tData = self.data1 {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let isWallet = tData["IsWallet"] as? Bool ?? false
                let isFavorite = uData["Favorite"] as? Bool ?? false
                
                if isWallet {
                    tData["IsFavorite"] = isFavorite
                }
                
                isUpdate = true
                self.data1 = tData
                
            }
        }
        
        if !isUpdate {
            if let uData = notification.object as? [String: Any], var tData = self.data2 {
                
                let uID = uData["ID"] as? String ?? ""
                let mID = tData["ID"] as? String ?? ""
                
                if uID == mID {
                    let isWallet = tData["IsWallet"] as? Bool ?? false
                    let isFavorite = uData["Favorite"] as? Bool ?? false
                    
                    if isWallet {
                        tData["IsFavorite"] = isFavorite
                    }
                    
                    self.data2 = tData
                    
                }
            }
        }
    }
    
    private func fillData(_ array: [[String: Any?]]?) {
        
        if let tArr = array, tArr.count > 0 {
            self.cover1.isHidden = false
            self.cover2.isHidden = false
            
            if tArr.count == 1 {
                self.data1 = tArr[0]
                self.cover2.isHidden = true
            } else {
                self.data1 = tArr[0]
                self.data2 = tArr[1]
            }
        } else {
            self.cover1.isHidden = true
            self.cover2.isHidden = true
        }
        
        self.fillDataItem1()
        self.fillDataItem2()
        
    }
    
    private func fillDataItem2() {
        if let tData = self.data2 {
            self.loadingImage2.startAnimating()
            self.thumbnail2.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage2.stopAnimating()
            })
            
            let type = tData["Type"] as? String
            
            if type == SUGGEST_TYPE.DEAL.rawValue {
                let isWallet = tData["IsWallet"] as? Bool ?? false
                let isFavorite = tData["IsFavorite"] as? Bool ?? false
                if isWallet {
                    self.imageStatus2.isHidden = false
                    self.imageStatus2.image = UIImage(named: "wallet_small_ic")
                } else if isFavorite {
                    self.imageStatus2.isHidden = false
                    self.imageStatus2.image = UIImage(named: "favourite_big_ic")
                } else {
                    self.imageStatus2.isHidden = true
                }
            } else if type == SUGGEST_TYPE.OFFER.rawValue {
                let isLike = tData["IsLike"] as? Bool ?? false
                
                if isLike {
                    self.imageStatus2.isHidden = false
                    self.imageStatus2.image = UIImage(named: "like_big_ic")
                } else {
                    self.imageStatus2.isHidden = true
                }
            } else {
                self.imageStatus2.isHidden = true
            }
            
        }
    }
    
    private func fillDataItem1() {
        if let tData = self.data1 {
            self.loadingImage1.startAnimating()
            self.thumbnail1.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage1.stopAnimating()
            })
            
            let type = tData["Type"] as? String
            
            if type == SUGGEST_TYPE.DEAL.rawValue {
                let isWallet = tData["IsWallet"] as? Bool ?? false
                let isFavorite = tData["IsFavorite"] as? Bool ?? false
                if isWallet {
                    self.imageStatus1.isHidden = false
                    self.imageStatus1.image = UIImage(named: "wallet_small_ic")
                } else if isFavorite {
                    self.imageStatus1.isHidden = false
                    self.imageStatus1.image = UIImage(named: "favourite_big_ic")
                } else {
                    self.imageStatus1.isHidden = true
                }
            } else if type == SUGGEST_TYPE.OFFER.rawValue {
                let isLike = tData["IsLike"] as? Bool ?? false
                
                if isLike {
                    self.imageStatus1.isHidden = false
                    self.imageStatus1.image = UIImage(named: "like_big_ic")
                } else {
                    self.imageStatus1.isHidden = true
                }
            } else {
                self.imageStatus1.isHidden = true
            }
            
        }
    }
    
    public func config(_ data: [[String: Any?]]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData(data)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATE_USE_DEAL, object: nil)
    }
    
}
