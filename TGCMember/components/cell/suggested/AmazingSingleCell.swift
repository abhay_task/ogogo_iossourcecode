//
//  AmazingSingleCell.swift
//  TGCMember
//
//  Created by vang on 6/7/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class AmazingSingleCell: UITableViewCell {

    @IBOutlet weak var coverVendorName: UIView!
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateUseDeal(_:)), name: .UPDATE_USE_DEAL, object: nil)
        
        
        self.coverContent.layer.cornerRadius = 3.0
        self.coverContent.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @objc func updateUseDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["DealID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                tData["IsUseDeal"] = true
                self.data = tData
            }
        }
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        if let tData = self.data {
            self.lblName.text = tData["Name"] as? String
            self.lblDate.text = tData["CurrentTime"] as? String
            
            self.loadingImage.startAnimating()
            self.thumbnail.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            if let summary =  tData["VendorSummary"] as? [String: Any?], let tVendorName = summary["Name"] as? String {
                coverVendorName.isHidden = false
                lblVendorName.text = tVendorName
            } else {
                coverVendorName.isHidden = true
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .UPDATE_USE_DEAL, object: nil)
    }
    
}
