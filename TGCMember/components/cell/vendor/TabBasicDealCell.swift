//
//  TabBasicDealCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import WebKit

class TabBasicDealCell: UITableViewCell {
    
    @IBOutlet weak var lblLiked: UILabel!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var btnUseOffer: UIButton!
  
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var coverLiked: UIView!
    @IBOutlet weak var coverFollowingItems: UIView!
    @IBOutlet weak var offerHeightConstraint: NSLayoutConstraint!
    
    private var isViewMore: Bool = false
    private let numberItemsVisible = 4
    private var data: [String: Any?]?
    private var onClickedUseCallback: DataCallback?
    private var onClickedLikeCallback: DataCallback?
    private var onHeightChanged: DataCallback?
    
    @IBOutlet weak var wkWebview: WKWebView!
    private var isFinishLoadWeb: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateLike(_:)), name: .TOOGLE_LIKE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUseBasicDeal(_:)), name: .UPDATE_USE_DEAL, object: nil)
        
        self.wkWebview.uiDelegate = self
        self.wkWebview.navigationDelegate = self
        self.wkWebview.scrollView.showsVerticalScrollIndicator = false
        self.wkWebview.scrollView.showsHorizontalScrollIndicator = false
        
        decorate()
    }
    
    private func decorate() {
        self.lblLiked.text = LOCALIZED.txt_liked_title.translate
        self.btnUseOffer.setTitle(LOCALIZED.txt_use_title.translate, for: .normal)
    }
    
    @objc func updateLike(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let isLike = uData["Like"] as? Bool ?? false
                tData["IsLike"] = isLike
                
                self.data = tData
                
                self.fillData()
            }
        }
    }

    
    @objc func updateUseBasicDeal(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["DealID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                tData["IsUseDeal"] = true
                self.data = tData
                
                self.fillData()
            }
        }
    }


    @IBAction func onClickedUseOffer(_ sender: Any) {
        self.onClickedUseCallback?(self.data)
    }
    
    @IBAction func onClickedLike(_ sender: Any) {
        self.onClickedLikeCallback?(self.data)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    func fillData() {
        if let tData = self.data {
            if !isFinishLoadWeb {
                let content = tData["ContentDetail"] as? String ?? ""
                
                let urlPath = Bundle.main.url(forResource: "\(Constants.DEFAULT_FONT)", withExtension: "ttf")
                let htmlCode = Utils.getHTMLFromContent(content: content, bgColor: "#f6f7fb")
                self.wkWebview.loadHTMLString(htmlCode, baseURL: urlPath)
            }
            
            
            let isLike = tData["IsLike"] as? Bool ?? false
            self.updateLikeView(isLike)
            
            let isUseDeal = tData["IsUseDeal"] as? Bool ?? false
            
            self.btnUseOffer.isUserInteractionEnabled = !isUseDeal
            self.btnUseOffer.alpha = isUseDeal ? 0.7 : 1.0
            
            //mPrint("fillData", tData)
            
        } else {
            self.offerHeightConstraint.constant = 0
            
            if let callback = self.onHeightChanged {
                callback(["contentHeight": CGFloat(160)])
            }
        }
    }
    
    private func updateLikeView(_ isLike: Bool = true) {
        self.coverLiked.layer.cornerRadius = 5.0
        self.coverLiked.layer.borderWidth = 1.0
        self.coverLiked.layer.borderColor = isLike ? UIColor(hexFromString: "#FFC106").cgColor : UIColor(0, 0, 0, 0.4).cgColor
        self.coverLiked.layer.masksToBounds =  true
        self.lblLiked.textColor = isLike ? UIColor(hexFromString: "#FFC106") : UIColor(hexFromString: "#6B6B6B")
        self.imageLike.image = isLike ? UIImage(named: "like_ic") : UIImage(named: "unlike_ic")
    }
    
    public func config(_ data: [String: Any?]?, onClickedUseCallback _onClickedUseCallback: DataCallback?, onClickedLikeCallback _onClickedLikeCallback: DataCallback?, onHeightChanged _onHeightChanged: DataCallback?) -> Void {
        self.data = data
        self.onClickedUseCallback = _onClickedUseCallback
        self.onClickedLikeCallback = _onClickedLikeCallback
        self.onHeightChanged = _onHeightChanged
        
        self.fillData()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_LIKE_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATE_USE_DEAL, object: nil)
    }
    
}


extension TabBasicDealCell: WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.offerHeightConstraint.constant = height as! CGFloat
                    
                    self.isFinishLoadWeb = true
                
                    self.contentView.isHidden = false
                    
                    if let callback = self.onHeightChanged {
                        callback(["contentHeight": CGFloat(150 + self.offerHeightConstraint.constant)])
                    }
                })
            }
            
        })
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            let tLink = navigationAction.request.url?.absoluteString
            
            if let link = tLink {
                Utils.openURL(link)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    
  
}
