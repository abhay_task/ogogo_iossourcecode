//
//  TabAboutCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class TabAboutCell: UITableViewCell {
    var parentVC: UIViewController?
    
    @IBOutlet weak var lblOpenNow: UILabel!
    @IBOutlet weak var lblOpenTime: UILabel!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnWebsite: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var lblAddress: UILabel!
    
   
    private var data: [String: Any?]?
    private var onClickedOpenNow: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        decorate()
    }
    
    private func decorate() {
        self.lblOpenNow.text = LOCALIZED.open_now.translate
    }
    
    @IBAction func onClickedFullMap(_ sender: Any) {
        let fullMapVC = FullMapViewController()
        fullMapVC.dealInfo = self.data
                      
        self.parentVC?.addSubviewAsPresent(fullMapVC)

    }
    
    @IBAction func onClickedNumberPhone(_ sender: Any) {
        if let tData = self.data, let number = tData["Phone"] as? String {
            Utils.callNumber(number)
        }
    }
    
    @IBAction func onClickedWebsite(_ sender: Any) {
        if let tData = self.data, let siteUrl = tData["SiteURL"] as? String {
            Utils.openURL(siteUrl)
        }
    }
    
    @IBAction func onClickedOpenNow(_ sender: Any) {
        if var tData = self.data, let callback = self.onClickedOpenNow {
            tData["sender"] = sender
            callback(tData)
        }
    }
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 13.0)
        mapView.animate(with: camera)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadView() -> TabAboutCell {
        let tabAboutCell = Bundle.main.loadNibNamed("TabAboutCell", owner: self, options: nil)?[0] as! TabAboutCell
        
        return tabAboutCell
    }
    
    public func config(_ data: [String: Any?]?, onClickedOpenNow _onClickedOpenNow: DataCallback?) -> Void {
        self.data = data
        self.onClickedOpenNow = _onClickedOpenNow
        
        if let tData = self.data {
            if let tLocation = tData["Location"] as? [String: Any] {
                let position = CLLocationCoordinate2D(latitude: tLocation["lat"] as! CLLocationDegrees, longitude: tLocation["lng"] as! CLLocationDegrees)
              
                let marker = GMSMarker()
                marker.position = position
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker.icon = UIImage(named: "point_marker")
                
                marker.map = mapView
                self.animateToPoint(position)
            }
            
            let number = "     \(tData["Phone"] as? String ?? "")"
            self.btnPhone.setTitle(number, for: .normal)
            
            let siteUrl = "     \(tData["SiteURL"] as? String ?? "")"
            self.btnWebsite.setTitle(siteUrl, for: .normal)
            
            self.lblAddress.text = Utils.getFullAddress(tData)
        }
        
    }
    
}
