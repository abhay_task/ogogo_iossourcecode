//
//  TabDealCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabDealCell: UITableViewCell {

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBOutlet weak var lblValidateTime: UILabel!
    @IBOutlet weak var descriptionStatus: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    private var data: [String: Any?]?
    private var onClickedCell: DataCallback?
    private var onClickedMore: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverView.layer.cornerRadius = 3.0
        coverView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCell {
            callback(self.data)
        }
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblName.text = tData["Name"] as? String
            
            var strTime = LOCALIZED.valid_until_format.translate
            strTime = strTime.replacingOccurrences(of: "[%__TAG_DATE__%]", with: Utils.stringFromStringDate(tData["EndAt"] as? String, toFormat: "MMM dd, yyyy"))
            self.lblValidateTime.text = strTime
            
            self.loadingImage.startAnimating()
            self.thumbnail.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            let dealShowType = Utils.getDealType(tData)
            
            if dealShowType == DEAL_TYPE.GOLD {
                self.imageStatus.image =  UIImage(named: "gold_ic")
            } else if dealShowType == DEAL_TYPE.SILVER {
                self.imageStatus.image = UIImage(named: "silver_ic")
            } else {
                self.imageStatus.image = nil
            }
          
            self.descriptionStatus.text = tData["SubscriptionName"] as? String

        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCell _onClickedCell: DataCallback?, onClickedMore _onClickedMore: DataCallback?) -> Void {
        self.data = data
        self.onClickedCell = _onClickedCell
        self.onClickedMore = _onClickedMore
        
        self.fillData()
    }
    
}
