//
//  PastTripCell.swift
//  TGCMember
//
//  Created by vang on 10/14/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class PastTripCell: UITableViewCell {
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    private var onClickedMoreCallback: DataCallback?
    
    @IBOutlet weak var lblNumEvent: UILabel!
    @IBOutlet weak var lblNumOffer: UILabel!
    @IBOutlet weak var lblNumDeal: UILabel!
    @IBOutlet weak var lblTripName: UILabel!
    @IBOutlet weak var lblTripDate: UILabel!
    @IBOutlet weak var coverView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        decorate()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
         self.onClickedCellCallback?(self.data)
     }
     
     @IBAction func onClickedMore(_ sender: Any) {
         self.onClickedMoreCallback?(self.data)
     }
    
    private func decorate() {
        self.coverView.layer.cornerRadius = 3.0
        self.coverView.layer.borderWidth = 1.0
        self.coverView.layer.borderColor = UIColor(217, 217, 217).cgColor
    }
    
    private func fillData() {
        if let tData = self.data {
            let strDate = tData["FromDateAt"] as? String ?? ""
            
            self.lblTripName.text = tData["Name"] as? String ?? ""
            self.lblTripDate.text = "\(tData["CountryName"] as? String ?? ""), \(Utils.stringFromStringDate(strDate, toFormat: "MMM dd, yyyy"))"
            
            self.lblNumDeal.text = "\(10) \(LOCALIZED.txt_deals_title.translate)"
            self.lblNumOffer.text = "\(3) \(LOCALIZED.txt_offers_title.translate)"
            self.lblNumEvent.text = "\(5) \(LOCALIZED.txt_events_title.translate)"
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil, onClickedMoreCallback _onClickedMoreCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        self.onClickedMoreCallback = _onClickedMoreCallback
        
        self.fillData()
    }
    
}
