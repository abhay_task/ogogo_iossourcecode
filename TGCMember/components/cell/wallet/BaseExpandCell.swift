//
//  BaseExpandCell.swift
//  TGCMember
//
//  Created by vang on 6/28/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class BaseExpandCell: UITableViewCell {

    var isExpanded: Bool = false 
    var data: [String: Any?]?
    var onClickedCellCallback: DataCallback?
    var onClickedCardCallback: DataCallback?
    
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var foregroundView: UIView!
    @IBOutlet weak var expandedView: UIView!
    
    public enum AnimationType : Int {
        case expanded
        case collapsed
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        coverView?.layer.cornerRadius = 3.0
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }

    open func fillData() {
       
    }

    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        self.onClickedCardCallback = _onClickedCardCallback

        self.fillData()

    }
    
    public func expand(_ value: Bool, animated: Bool = true, completion: (() -> Void)? = nil) {
        if animated {
            let openCallback = { ()-> Void in
                self.completedExpandCallback()
                completion?()
            }
            
            let closeCallback = { ()-> Void in
                self.completedCollpasedCallback()
                completion?()
            }
            
            value ? openAnimation(openCallback) : closeAnimation(closeCallback)
        } else {
            expandedView.alpha = value ? 1 : 0
        }
    }
    
    open var completedExpandCallback: FinishedCallback {
        return { () -> Void in }
        
        
    }
    
    open var completedCollpasedCallback: FinishedCallback {
        return { () -> Void in }
        
        
    }
    
    func openAnimation(_ completion: (() -> Void)? = nil) {
        isExpanded = true

        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.expandedView.alpha = 1

        },
                       completion: { [weak self] _ in
                        guard let self = self else {return}
                        completion?()
        })
    }

    func closeAnimation(_ completion: (() -> Void)? = nil) {
        isExpanded = false

        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.expandedView.alpha = 0

        },
                       completion: { [weak self] _ in
                        guard let self = self else {return}
                        completion?()
        })

    }

}
