//
//  AnswerCell.swift
//  TGCMember
//
//  Created by vang on 10/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {

    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    

    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var checkImg: UIImageView!

    override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code

        decorate()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    // Configure the view for the selected state
    }

    private func decorate() {

    }
   
    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblAnswer.text = tData["answer"] as? String ?? ""
        }
    }
   
   public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil) -> Void {
       self.data = data
       self.onClickedCellCallback = _onClickedCellCallback
      
       self.fillData()
   }
}
