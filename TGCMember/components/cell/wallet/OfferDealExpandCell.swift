//
//  TabDealCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class DealExpandCell: UITableViewCell {

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBOutlet weak var lblValidateTime: UILabel!
    @IBOutlet weak var descriptionStatus: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblUsedOn: UILabel!
    @IBOutlet weak var imageUsedOn: UIImageView!
    @IBOutlet weak var coverUsedOn: UIView!
    private var data: [String: Any?]?
    private var onClickedCell: ActionCallback?
    private var onClickedMore: ActionCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateFavorite(_:)), name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUseDeal(_:)), name: .UPDATE_USE_DEAL, object: nil)
        
        coverView.layer.cornerRadius = 3.0
        coverView.layer.masksToBounds = true
    }

    func loadView() -> DealExpandCell {
        let offerDealExpandCell = Bundle.main.loadNibNamed("OfferDealExpandCell", owner: self, options: nil)?[0] as! DealExpandCell
        
        return offerDealExpandCell
    }
    
    @objc func updateUseDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["DealID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                tData["IsUseDeal"] = true
                self.data = tData
            }
        }
    }
    
    @objc func updateFavorite(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let isFavorite = uData["Favorite"] as? Bool ?? false
                tData["IsFavorite"] = isFavorite
          
                self.data = tData
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClickedMore(_ sender: Any) {
        if let callback = self.onClickedMore {
            callback(self.data)
        }
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCell {
            callback(self.data)
        }
    }
    
    private func decorate() {
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.coverUsedOn.frame
        rectShape.position = self.coverUsedOn.center
        rectShape.path = UIBezierPath(roundedRect: self.coverUsedOn.bounds, byRoundingCorners: [.topRight , .bottomRight], cornerRadii: CGSize(width: 13, height: 13)).cgPath
        

        self.coverUsedOn.layer.mask = rectShape
    }
    
    private func fillData() {
        if let tData = self.data {
            self.coverUsedOn.isHidden = false
            
            self.lblName.text = tData["Name"] as? String
            self.lblValidateTime.text = "Valid until \(Utils.stringFromStringDate(tData["EndAt"] as? String, toFormat: "MMM dd, yyyy"))"
            
            self.loadingImage.startAnimating()
            self.thumbnail.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            let dealShowType = Utils.getDealShowType(tData)
   
            if dealShowType == DEAL_SHOW_TYPE.GOLD {
                self.imageStatus.image =  UIImage(named: "gold_ic")
            } else if dealShowType == DEAL_SHOW_TYPE.SILVER {
                self.imageStatus.image = UIImage(named: "silver_ic")
            } else {
                self.imageStatus.image = nil
            }

            self.descriptionStatus.text = tData["SubscriptionName"] as? String
            
            let usedOnText = tData["UsedOn"] as? String ?? ""
            
            let isUsedOn = usedOnText != ""
            let isAlertExpired = tData["AlertExpiredOn"] as? Bool ?? false
            
            if isUsedOn {
                self.imageUsedOn.image = UIImage(named: "used_on_ic")
                self.lblUsedOn.textColor = UIColor(0, 146, 89)
                self.lblUsedOn.text = "Used on \(Utils.stringFromStringDate(usedOnText, toFormat: "dd/MM"))"
            } else if isAlertExpired {
                self.imageUsedOn.image = UIImage(named: "expired_on_ic")
                self.lblUsedOn.textColor = UIColor(255, 22, 22)
                self.lblUsedOn.text = "Expired on \(Utils.stringFromStringDate(tData["EndAt"] as? String, toFormat: "dd/MM"))"
            } else {
                self.coverUsedOn.isHidden = true
            }

        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCell _onClickedCell: ActionCallback?, onClickedMore _onClickedMore: ActionCallback?) -> Void {
        self.data = data
        self.onClickedCell = _onClickedCell
        self.onClickedMore = _onClickedMore
       
        self.fillData()
        
        self.layoutIfNeeded()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.decorate()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATE_USE_DEAL, object: nil)
    }
    
}
