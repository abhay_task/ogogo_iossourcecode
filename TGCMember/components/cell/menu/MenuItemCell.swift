//
//  MenuItemCell.swift
//  TGCMember
//
//  Created by vang on 5/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCell: UIButton!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var switcher: UISwitch!
    
    @IBOutlet weak var titleRightConstraint: NSLayoutConstraint!
    private var index: Int = 0
    private var data: [String: Any?]?
    
    private var onClickedCellCallback: DataCallback?
    private var onChangedNotification: DataCallback?
    @IBOutlet weak var lblTitleBtn: UILabel!
    
    @IBOutlet weak var coverTypeNormal: HighlightableView!
    @IBOutlet weak var coverTypeBtn: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onSwitchChanged(_ sender: Any) {
        let tSwitch = sender as! UISwitch
        let isOn = tSwitch.isOn
        
        GLOBAL.GlobalVariables.isOnNotification = isOn
        
        self.updateThumbColor()
        
        self.onChangedNotification?(["isOn": isOn])
    }
    
    private func updateThumbColor() {
        if switcher.isOn {
            switcher.thumbTintColor = .white
            switcher.onTintColor = UIColor("rgb 255 193 6")
        } else {
            switcher.thumbTintColor = UIColor("rgb 197 199 203")
        }
    }
    
    private func fillData() {
        if let tData = self.data {
            let type = tData["type"] as? String ?? ""
            let enable = tData["enable"] as? Bool ?? false
            
            let isTypeBtn = type == "button"
           
            self.coverTypeBtn?.isHidden = !isTypeBtn
            self.coverTypeNormal?.isHidden = isTypeBtn
         
            if type == "switch" {
                switcher.isOn  = GLOBAL.GlobalVariables.isOnNotification
            }
            
            if type == "text" {
                self.coverTypeNormal?.alpha = enable ? 1 : 0.5
                self.coverTypeNormal?.isUserInteractionEnabled = enable
            }
            
            btnCell.isHidden = type == "switch" ? true : false
            switcher.isHidden = type == "switch" ? false : true

            titleRightConstraint.constant = type == "switch" ? 80 : 10
          
            let title = tData["title"] as? String ?? ""
            self.lblTitle.text = title
            self.lblTitleBtn.text = title
            
            self.icon.image = UIImage(named: tData["icon"] as? String ?? "")
            
            self.updateThumbColor()
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil, onChangedNotification _onChangedNotification: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        self.onChangedNotification = _onChangedNotification
        
        self.fillData()
        
    }
    
}
