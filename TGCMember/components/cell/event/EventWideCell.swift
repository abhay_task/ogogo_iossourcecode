//
//  EventWideCell.swift
//  TGCMember
//
//  Created by vang on 10/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class EventWideCell: UITableViewCell {

    @IBOutlet weak var coverContent: UIView!
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!

    @IBOutlet weak var coverCell: UIView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var eventImageView: UIImageView!

    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      
          coverCell.layer.cornerRadius = 3.0
          coverCell.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func loadView() -> EventWideCell {
          let aView = Bundle.main.loadNibNamed("EventWideCell", owner: self, options: nil)?[0] as! EventWideCell
          
          return aView
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    private func fillData() {
       if let tData = self.data {
           self.lblEventName.text = tData["Name"] as? String
           self.lblEventDate.text = tData["CurrentTime"] as? String
           
           let imageUrl = tData["HeaderImageUrl"] as? String ?? ""

           self.loadingImage.startAnimating()
           self.eventImageView.sd_setImage(with:  URL(string: imageUrl), completed: {  [weak self]  (_, _, _, _) in
               guard let self = self else {return}
               
               self.loadingImage.stopAnimating()
           })
       }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
}
