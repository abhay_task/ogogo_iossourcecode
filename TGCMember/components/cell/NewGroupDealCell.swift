//
//  NewGroupDealCell.swift
//  TGCMember
//
//  Created by vang on 8/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

public enum NEW_HEIGHT_GROW_VIEW {
    static let CLOSE: CGFloat = 95
    static let OPEN: CGFloat = 125
}


class NewGroupDealCell: UITableViewCell {

    @IBOutlet weak var maskHighlight: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var subContentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var items:[[String: Any?]] = []
    private var data: [String: Any?]?
    
    private var onClickedCardCallback: DataCallback?
    private var onClickedShowDetailCallback: DataCallback?
    private var onScrollToCarouselCallback: DataCallback?
    
    private let batchSizeCarousel: Int = 10
    private var indexTableCell: Int = 0
    private var carouselType: STORE_TYPE = .NONE
    private var pageNumber: Int = 1
    

    var parentHomeWallet: HomeWalletViewController?
    
    private var isWatingLoadMore: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        decorate()
        self.collectionView.register(UINib(nibName: "NewDealGrowCell", bundle: nil), forCellWithReuseIdentifier: "NewDealGrowCell")
       
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCollectionKeepState(_:)), name: .RELOAD_GROUP_GROW_CARD, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToCardAndOpen(_:)), name: .SCROLL_TO_GROW_CARD_THEN_OPEN, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(highlightCarousel(_:)), name: .HIGHLIGH_CAROUSEL, object: nil)
    }
    
    @objc func reloadCollectionKeepState(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            let tIndexTableCell = uData["indexTableCell"] as? Int ?? 0
            
            if tIndexTableCell == self.indexTableCell {
                UIView.performWithoutAnimation {
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    @objc func highlightCarousel(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], let sType = uData["key"] as? String {
            let tCarouselType = STORE_TYPE(rawValue: sType) ?? .NONE
            
            if self.carouselType == tCarouselType {
                
                self.onScrollToCarouselCallback?(self.data)
                
                setTimeout({
                    //print("highlightCarousel --> ", sType);
                    self.animateHighlight()
                }, 500)
                
            }
        }
    }
    
    private func animateHighlight() {
        self.animate(fromAlpha: 0, toAlpha: 0.5) {
            self.animate(fromAlpha: 0.5, toAlpha: 0)
        }
    }
     
    private func animate(fromAlpha: CGFloat = 0.0, toAlpha: CGFloat = 1.0, completed: FinishedCallback? = nil) {
        self.maskHighlight.alpha = fromAlpha
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionFlipFromTop, animations: { () -> Void in
            self.maskHighlight.alpha = toAlpha
        }, completion: { (finised) in
            self.maskHighlight.alpha = toAlpha
            completed?()
        })
    }
    
    @objc func scrollToCardAndOpen(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], let dealId = uData["DealID"] as? String {
            
            let fromTool = uData["fromTool"] as? Bool ?? false
            
            var typeToCheck = STORE_TYPE.NONE
            
            if fromTool {
                typeToCheck = self.getCarouselFirst(dealId)
            }
            
            if self.carouselType == typeToCheck {
                let indexCard = self.items.firstIndex{ $0["ID"] as? String == dealId }
                
                if let tIndexCard = indexCard {
                    let data = self.items[tIndexCard]
                    let indexPath = IndexPath(row: tIndexCard, section: 0)
                    
                    setTimeout({
                        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    }, 300)
                    
                    setTimeout({
                        self.handleWhenClickOnCard(data, indexPath)
                    }, 800)
                    
                }
            }
        }
    }
    //59c7b895-ac97-49e2-9d76-aa3059d7dfa4
    private func getCarouselFirst(_ dealId: String) -> STORE_TYPE {
        var tCarouselType = STORE_TYPE.NONE
        
        if let tData = self.data {
            let totalDeals = tData["totalDeals"] as? [[String: Any?]] ?? []
            
            for item in totalDeals {
                if Utils.isCarouselDeal(item) {
                    let listDeals = item["List"] as? [[String: Any?]] ?? []
                    
                    let fItem = listDeals.first{ $0["ID"] as? String == dealId }
                     
                    if fItem != nil {
                        let sType = item["StoreType"] as? String ?? ""
                        tCarouselType = STORE_TYPE(rawValue: sType) ?? .NONE
                        
                        break
                    }
                }
            }
        }
        
        return tCarouselType
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .RELOAD_GROUP_GROW_CARD, object: nil)
        NotificationCenter.default.removeObserver(self, name: .SCROLL_TO_GROW_CARD_THEN_OPEN, object: nil)
        NotificationCenter.default.removeObserver(self, name: .HIGHLIGH_CAROUSEL, object: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func decorate() {
      
    }
    
    
    private func fillData() {
        if let tData = self.data {
            
            let hexBgColor = tData["Color"] as? String ?? "#000000"
            let cTitle = tData["Title"] as? String ?? ""
            let carouselType = tData["StoreType"] as? String ?? ""
            
            self.carouselType = STORE_TYPE(rawValue: carouselType) ?? .NONE
            
            let listDeal = tData["List"] as? [[String: Any?]] ?? []
            
            self.subContentView.backgroundColor = .white
            self.lblTitle.text = cTitle.capitalized
            
            self.indexTableCell = tData["indexTableCell"] as? Int ?? 0
            
            if GLOBAL.GlobalVariables.pagesWalletCarousel.count > self.indexTableCell {
                self.pageNumber = GLOBAL.GlobalVariables.pagesWalletCarousel[indexTableCell]
            }
            
            self.items = listDeal
            
            self.collectionView.reloadData {
                self.isWatingLoadMore = false
            }
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil, onClickedShowDetailCallback _onClickedShowDetailCallback: DataCallback? = nil, onScrollToCarouselCallback _onScrollToCarouselCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onClickedCardCallback = _onClickedCardCallback
        self.onClickedShowDetailCallback = _onClickedShowDetailCallback
        self.onScrollToCarouselCallback = _onScrollToCarouselCallback
        
        self.fillData()
        
        //self.layoutIfNeeded()
    }
    
    private func isNeedUpdateHeightForCell(_ indexRow: Int) -> Bool {
        let collectionCellHeights = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[indexRow]
        
        let maxHeight = collectionCellHeights?.max() ?? NEW_HEIGHT_CELL_GROW.CLOSE
        
        if maxHeight == NEW_HEIGHT_CELL_GROW.OPEN {
            return false
        }
        
        return true
        
    }
    
    private func validToLoadMore() -> Bool {
        if self.items.count == batchSizeCarousel * self.pageNumber {
            return true
        }
        
        return false
    }
    
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension NewGroupDealCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // Note: ‘isWatingLoadMore’ is used for checking the paging status Is currency process or not.
        if indexPath.row == self.items.count - 2 && !self.isWatingLoadMore && self.validToLoadMore() {
            self.isWatingLoadMore = true
            
            self.pageNumber += 1
            
            GLOBAL.GlobalVariables.pagesWalletCarousel[self.indexTableCell] = self.pageNumber
            
            self.doLoadMoreItems()
        }
    }
    
    private func doLoadMoreItems() {
        // call the API in this block and after getting the response then
        
//        self.dataSoruce.append(newData);
//        self.tableView.reloadData()
//        self.isWating = false // it’s means paging is done and user can able request another page request by scrolling the tableview at the bottom.
        
        //print("doLoadMoreItems")
        
    }
    
    private func handleWhenClickOnCard(_ data: [String: Any?]?, _ indexPath: IndexPath) {
        if var tData = data {
            tData["isNeedUpdateHeightForCell"] = self.isNeedUpdateHeightForCell(self.indexTableCell)
  
            self.growCard(indexPath.row, tData, indexPath)
            self.onClickedCardCallback?(tData)
        }
        
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var complexData = [String: Any?]()
        
        var data = items[indexPath.row]
        data["indexTableCell"] = self.indexTableCell
        data["indexCollectionCell"] = indexPath.row
        data["willDownCell"] = self.willDownCell()
        data["maskCard"] = (self.carouselType == .NONE) ? MASK_CARD.CLAIMED : MASK_CARD.NONE
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewDealGrowCell", for: indexPath) as! NewDealGrowCell
        
        complexData["carouselType"] = self.carouselType
        complexData["data"] = data
        
        cell.config(complexData, onClickedCellCallback: { [weak self] (pData) in
            guard let self = self else {return}
            
//            var fData = pData
//            fData?["isNeedUpdateHeightForCell"] = self.isNeedUpdateHeightForCell(self.indexTableCell)
//
//            //self.parent?.configCell(true)
//            self.growCard(indexPath.row, fData, indexPath)
//            self.onClickedCardCallback?(fData)
            
            self.handleWhenClickOnCard(pData, indexPath)
        })
        
        
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 124, height: self.getHeightCardFromIndex(indexPath.row))
    }
    
    private func updateHeightOtherGroupIfNeed() {
        
        if let growedCell = GLOBAL.GlobalVariables.growedCell {
            let tIndexTableCell = growedCell["indexTableCell"] as? Int ?? 0
            
            if tIndexTableCell != self.indexTableCell {
                
                var tInfo = [String: Any]()
                tInfo["indexTableCell"] = tIndexTableCell
                
                setTimeout({
                    self.parentHomeWallet?.configHeightForEachCardOnCarousel(true, self.indexTableCell)
                    NotificationCenter.default.post(name: .DOWN_HEIGHT_FOR_CAROUSEL_IN_WALLET, object: tInfo)
                }, 700)
            }
        }
        
      
    }
    
    private func growCard(_ index: Int, _ pData: [String: Any?]?, _ indexPath: IndexPath) {
        let collectionCellHeights = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[indexTableCell]
        
        self.updateHeightOtherGroupIfNeed()
        
        // UPDATE HEIGHT FOR SELECT GROUP CELL
        if var heights = collectionCellHeights {
            heights[index] = (NEW_HEIGHT_GROW_VIEW.OPEN + NEW_HEIGHT_TITLE_CAROUSEL_WALLET)
            
            GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[indexTableCell] = heights
        }
        
        UIView.performWithoutAnimation {
           self.collectionView.reloadData()
        }

        setTimeout({
            var isGrowUp = true
            
            // GROW DOWN
            if var tDictDown =  GLOBAL.GlobalVariables.growedCell {
                tDictDown["willGrow"] = false
                let cIndexTable = tDictDown["indexTableCell"] as? Int ?? 0
                let cIndexColl = tDictDown["indexCollectionCell"] as? Int ?? 0
                
                if cIndexTable == self.indexTableCell && cIndexColl == index { // show DETAIL
                    var tData = [String: Any?]()
                    tData["cardData"] = pData
                    
                    let cell = self.collectionView.cellForItem(at: indexPath)
                    var point: CGPoint = .zero
                    
                    if let tCell = cell {
                        let tPoint = CGPoint(x: tCell.bounds.width/2, y: tCell.bounds.height/2)
                        point = tCell.convert(tPoint, to: GLOBAL.GlobalVariables.appDelegate?.window)
                    }
           
                    tData["cardPoint"] = point
                    tData["carouselType"] = self.carouselType
                    
                    self.onClickedShowDetailCallback?(tData)
                    isGrowUp = false
                } else {
                    NotificationCenter.default.post(name: .ANIMATION_GROW_CELL, object: tDictDown)
                }
            }
            
            if isGrowUp {
                var tDictUp = [String: Any]()
                tDictUp["indexTableCell"] = self.indexTableCell
                tDictUp["indexCollectionCell"] = index
                tDictUp["willGrow"] = true
                
                // GROW UP
                NotificationCenter.default.post(name: .ANIMATION_GROW_CELL, object: tDictUp)
            }
        }, 200)
        
    }
    
    private func willDownCell() -> Bool {
        let collectionCellHeights = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[indexTableCell]
        
        //let tHeight = collectionCellHeights?[index] ?? ( NEW_HEIGHT_GROW_VIEW.CLOSE + NEW_HEIGHT_TITLE_CAROUSEL_WALLET)
        let tHeight = collectionCellHeights?.max() ?? NEW_HEIGHT_CELL_GROW.CLOSE
        
        if tHeight == NEW_HEIGHT_CELL_GROW.CLOSE {
            return false
        }
        
        return true
        
    }

    private func getHeightCardFromIndex(_ index: Int) -> CGFloat {
        let collectionCellHeights = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[indexTableCell]
        
        //let tHeight = collectionCellHeights?[index] ?? ( NEW_HEIGHT_GROW_VIEW.CLOSE + NEW_HEIGHT_TITLE_CAROUSEL_WALLET)
        let tHeight = collectionCellHeights?.max() ?? NEW_HEIGHT_CELL_GROW.CLOSE
        
        return tHeight - NEW_HEIGHT_TITLE_CAROUSEL_WALLET
    }
}
