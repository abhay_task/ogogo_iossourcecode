//
//  CarouselDealCell.swift
//  TGCMember
//
//  Created by vang on 12/18/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CarouselDealCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var coverContent: UIView!
    
    private var data: [String: Any?]?
    private var items:[[String: Any?]] = []
    private var currentItems:[[String: Any?]] = []
    
    private var onClickedCardCallback: DataCallback?
    private var onClickedMoreStoreCallback: DataCallback?
    private var onClickedUpgradeCallback: DataCallback?
    
    private var indexTableCell: Int = 0
    
    @IBOutlet weak var coverHeader: UIView!
    private var storeType: String = ""
    private var pageNumber: Int = 1
    private var isWatingLoadMore: Bool = true
    private var haveItemMore: Bool = false
    
    @IBOutlet weak var heightCollectionConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        decorate()
        
        self.collectionView.register(UINib(nibName: "NewDealCell", bundle: nil), forCellWithReuseIdentifier: "NewDealCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToDeal), name: .ON_SELECTED_DEAL_ON_MAP, object: nil)
    }
    
    @objc func filterWithText(_ notification: Notification) {
        self.handleFilterWWithText()
    }
    
    private func handleFilterWWithText() {

        self.currentItems = self.items
        self.collectionView.reloadData()
    }
    
    @objc func scrollToDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let uID = uData["ID"] as? String ?? ""
            let storeType = uData["StoreType"] as? String
            
            if storeType == STORE_TYPE.NEARBY_DEALS.rawValue {
                let indexCell = self.currentItems.firstIndex{ $0["ID"] as? String == uID }
                
                if let tRow = indexCell {
                    NotificationCenter.default.post(name: .ON_HIGHLIGHT_DEAL_ON_CAROUSEL, object: uData)
                    GLOBAL.GlobalVariables.selectedDeal = uData
                    
                    self.collectionView.scrollToItem(at: IndexPath(row: tRow, section: 0), at: .centeredHorizontally, animated: true)
                }
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_DEAL_ON_MAP, object: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func decorate() {
        coverContent?.backgroundColor = .clear
    }
    
    
    private func fillData() {
        if let tData = self.data {
            self.haveItemMore = tData["haveItemMore"] as? Bool ?? false
            let heightCarousel = self.haveItemMore ? 195 : 165
            
            self.coverHeader?.removeSubviews()
            
            if let sectionInfo = tData["sectionInfo"] as? [String: Any?] {
               
                let headerView = HeaderCarousel().loadView()
              
                headerView.config(sectionInfo, onClickedUpgradeCallback:  { [weak self] (data) in
                    guard let self = self else {return}
        
                    self.onClickedUpgradeCallback?(data)
                })
                
                self.coverHeader?.addSubview(headerView)
                headerView.snp.makeConstraints { (maker) in
                    maker.left.equalToSuperview()
                    maker.right.equalToSuperview()
                    maker.bottom.equalToSuperview()
                    maker.height.equalTo(40)
                }
            }
            
            self.heightCollectionConstraint?.constant = CGFloat(heightCarousel)
            
            let cTitle = tData["Title"] as? String ?? ""
            
            self.storeType =  tData["StoreType"] as? String ?? ""
            
            let listDeal = tData["List"] as? [[String: Any?]] ?? []
            
            self.lblTitle.text = cTitle
            
            self.indexTableCell = tData["indexTableCell"] as? Int ?? 0
            
            if GLOBAL.GlobalVariables.pagesWalletCarousel.count > self.indexTableCell {
                self.pageNumber = GLOBAL.GlobalVariables.pagesWalletCarousel[indexTableCell]
            }
            
            self.items = listDeal
            self.handleFilterWWithText()
            
            self.collectionView.reloadData {
                self.isWatingLoadMore = false
            }
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil, onClickedMoreStoreCallback _onClickedMoreStoreCallback: DataCallback? = nil, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil ) -> Void {
        self.data = data
        
        self.onClickedCardCallback = _onClickedCardCallback
        self.onClickedMoreStoreCallback = _onClickedMoreStoreCallback
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
        
        //self.layoutIfNeeded()
    }
    
}


// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension CarouselDealCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    private func handleWhenClickOnCard(_ data: [String: Any?]?, _ indexPath: IndexPath) {
        //if var tData = data {
        self.onClickedCardCallback?(data)
        //}
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var data = currentItems[indexPath.row]
        data["StoreType"] = self.storeType
        data["haveItemMore"] = self.haveItemMore
        
        //data["DealProgram"] = "sweepstake"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewDealCell", for: indexPath) as! NewDealCell
        
        cell.config(data, onClickedCellCallback: { [weak self] (pData) in
            guard let self = self else {return}
            
            self.handleWhenClickOnCard(pData, indexPath)
        }, onClickedMoreStoreCallback: { [weak self] (pData) in
            guard let self = self else {return}
            
            self.onClickedMoreStoreCallback?(pData)
        })
        
        
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 150, height: self.haveItemMore ? 195 : 165) // vangdoan
    }
    
    
    
    
}
