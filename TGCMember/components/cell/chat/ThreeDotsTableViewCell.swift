//
//  ThreeDotsTableViewCell.swift
//  SpeedocNative
//
//  Created by Sang Huynh on 5/7/19.
//

import UIKit

class ThreeDotsTableViewCell: UITableViewCell {

    @IBOutlet weak var dots : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    func setupAnimating(){
        dots.stopAnimating()
        dots.image = nil
        var imgs : [UIImage] = []
        imgs.append(UIImage(named : "dot1")!)
        imgs.append(UIImage(named : "dot2")!)
        imgs.append(UIImage(named : "dot3")!)
        dots.image = UIImage.animatedImage(with: imgs, duration: 1)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        setupAnimating()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupAnimating()
    }
    
    
    deinit {
        dots.stopAnimating()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
