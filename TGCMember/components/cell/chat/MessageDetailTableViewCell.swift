//
//  MessageDetaiilTableViewCell.swift
//  SpeedocNative
//
//  Created by Sang Huynh on 5/6/19.
//

import UIKit
import GoogleMaps

let LEFT_MESSAGE_BG_COLOR = UIColor(235, 169, 9)
let RIGHT_MESSAGE_BG_COLOR = UIColor(243, 244, 247)
let LEFT_MESSAGE_TEXT_COLOR = UIColor(255, 255, 255)
let RIGHT_MESSAGE_TEXT_COLOR = UIColor(30, 38, 71)

class LeftCoverMessageView : UIView { // ignore topleft
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        //self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner, .layerMaxXMinYCorner]

//        let v = UIImageView()
//        //v.image = UIImage(named: "bg_message")
//        v.backgroundColor = LEFT_MESSAGE_BG_COLOR
//        v.frame = self.bounds
//        v.contentMode = .scaleAspectFill
//        self.addSubview(v)
//        self.sendSubviewToBack(v)
    }
}

class RightCoverMessageView : UIView { // ignore topright
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.clipsToBounds = true
        self.layer.cornerRadius = 10
        //self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
//        let v = UIImageView()
//        //v.image = UIImage(named: "bg_message")
//        v.backgroundColor = RIGHT_MESSAGE_BG_COLOR
//        v.frame = self.bounds
//        v.contentMode = .scaleAspectFill
//        self.addSubview(v)
//        self.sendSubviewToBack(v)
    }
}


class MessageDetailTableViewCell: UITableViewCell {
    var parentVC: ChatDetailViewController?
    
    var isFirst: Bool = true
    var isMiddle: Bool = true
    var isEnd: Bool = true
    var avatarUrl: String = ""
    
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var rightLocationView: RightCoverMessageView!
    @IBOutlet weak var leftLocationView: LeftCoverMessageView!
    @IBOutlet weak var rightImageView: RightCoverMessageView!
    @IBOutlet weak var leftImageView: LeftCoverMessageView!
    @IBOutlet weak var coverLeftView: LeftCoverMessageView!
    @IBOutlet weak var coverRightView: RightCoverMessageView!
    @IBOutlet  weak var lblMessage : UILabel?
    @IBOutlet  weak var lblTime : UILabel?
    @IBOutlet  weak var mImageView : UIImageView?
    @IBOutlet  weak var mImageViewHeightConstaint : NSLayoutConstraint?
    @IBOutlet  weak var mImageViewWidthConstaint : NSLayoutConstraint?
    @IBOutlet  weak var imReadStatus : UIImageView?
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationViewWidthConstraint: NSLayoutConstraint!
    
    weak var table : UITableView?
    var imgBGView : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageAvatar?.layer.cornerRadius = 24
        self.imageAvatar?.layer.masksToBounds = true
        
        self.mImageViewWidthConstaint?.constant = 204
        self.mImageViewHeightConstaint?.constant = 150
        
        self.locationViewWidthConstraint?.constant = 204
        self.locationViewHeightConstraint?.constant = 150
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblMessage?.text = ""
        lblTime?.text = ""
        mImageView?.image = nil
        table = nil
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    @IBAction func onClickedLocation(_ sender: Any) {
        if let vc = self.parentVC {
            let fullMapVC = FullMapViewController()
            vc.addSubviewAsPresent(fullMapVC)
        }
    }
    
    private func updateBubble() {
        self.imageAvatar?.isHidden = true
        
        if isFirst == isMiddle && isMiddle == isEnd {
            self.coverLeftView?.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            self.coverRightView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.leftImageView?.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            self.rightImageView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.leftLocationView?.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            self.rightLocationView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.imageAvatar?.isHidden = false
        } else if isFirst {
            self.coverLeftView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.coverRightView?.layer.maskedCorners =  [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            self.leftImageView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightImageView?.layer.maskedCorners =  [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            self.leftLocationView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightLocationView?.layer.maskedCorners =  [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
            
        } else if isMiddle {
            self.coverLeftView?.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.coverRightView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            self.leftImageView?.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightImageView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            self.leftLocationView?.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightLocationView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        } else if isEnd {
            self.coverLeftView?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.coverRightView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            self.leftImageView?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightImageView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            self.leftLocationView?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            self.rightLocationView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            self.imageAvatar?.isHidden = false
        }
        
        if let avatar = self.imageAvatar {
            if !avatar.isHidden {
                avatar.sd_setImage(with: URL(string: avatarUrl), completed:nil)
            }
            
        }

        self.layoutIfNeeded()
    }
    
    private func setImageThenAnimate(_ url : String){
        mImageView?.sd_setImage(with: URL(string: url), completed: {[weak self] (img, err, type, url) in
            guard let self = self else {return}
            if let img = img {
                DispatchQueue.main.async{[weak self] in
                    guard let `self` = self else {return}
                    // TODO : code goes here
                    self.mImageViewWidthConstaint?.constant = 207
                    self.mImageViewHeightConstaint?.constant = (img.size.height  * 207) / img.size.width
                    self.layoutIfNeeded()
                    if let v =  self.table { //ookupScrollView(self) as? UITableView {
                        v.beginUpdates()
                        v.endUpdates()
                    }
                    
                }
                
            }
        })
    }
    
    var data : MMessage? {
        didSet {
            if let message = data {
                
                imReadStatus?.image = UIImage(named: message.isRead ? "ic_read_msg" : "ic_unread_msg")
                
                let strTime = Utils.stringFromMilliseconds(message.createdAt, toFormat: "hh:mm a")
                lblTime?.text = strTime
                
                let tType = message.type
                
                if tType == MESSAGE_TYPE.IMAGE.rawValue, let urlString = message.image {
                    if urlString.contains("://") {
                        mImageView?.sd_setImage(with: URL(string: urlString), completed:nil)
                    } else {
                        mImageView?.image = Utils.getImageFromBase64(urlString)
                    }
                } else {
                    lblMessage?.text = message.text
                }
                
                
                
                self.updateBubble()
            }
        }
    }
   
}
   
