//
//  TabDealCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit


class NewDealWideCell: UITableViewCell {
    
    @IBOutlet weak var coverView: UIView!
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        decorate()
        
    }
    
    private func loadSubViews() {
        let subContent = NewDealWideView().loadView()
        subContent.config(self.data) { (data) in
            self.onClickedCellCallback?(data)
        }
        
        self.coverView.addSubview(subContent)
        
        subContent.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func decorate() {
        coverView.layer.cornerRadius = 10.0
        coverView.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.16, x: 0, y: 0, blur: 9, spread: 0)
        coverView.layer.borderWidth = 0.2
        coverView.layer.borderColor = UIColor(151, 151, 151).cgColor
    }
    
    private func fillData() {
        self.coverView.removeSubviews()
        
        self.loadSubViews()
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
        
        self.layoutIfNeeded()
    }
}
