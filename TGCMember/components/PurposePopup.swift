//
//  PurposePopup.swift
//  TGCMember
//
//  Created by vang on 10/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class PurposePopup: UIViewController {

    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfAnswer: UITextField!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var coverBtnCancel: UIView!
    @IBOutlet weak var coverInput: UIView!
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var coverBtnConfirm: UIView!
    
    @IBOutlet weak var tblAnswer: UITableView!
    private var listAnswers: [String] = []
    
    var textAnswer: String = ""
    var callback: DataCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tblAnswer.register(UINib(nibName: "AnswerCell", bundle: nil), forCellReuseIdentifier: "AnswerCell")
        decorate()
        
        tfAnswer.becomeFirstResponder()
        
        fillData()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            GLOBAL.GlobalVariables.heightKeyboard = keyboardSize.height
            self.heightContentConstraint.constant = SCREEN_HEIGHT - (Utils.isIPhoneNotch() ? 40 : 15)  - GLOBAL.GlobalVariables.heightKeyboard - 10
        }
    }

    @objc func keyboardWillHide(notification: Notification) {

    }

    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    @IBAction func onClickedConfirm(_ sender: Any) {
        self.callback?(["answer": self.tfAnswer.text?.trimmingCharacters(in: .whitespacesAndNewlines)])
        
        Utils.dismissKeyboard()
        self.dismissFromSuperview()
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        Utils.dismissKeyboard()
        self.dismissFromSuperview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    private func fillData() {
        self.tfAnswer.text = self.textAnswer
        
        if let tData = GLOBAL.GlobalVariables.purposeCreateTrip {
            self.lblTitle.text = tData["Question"] as? String ?? ""
            
            self.listAnswers = tData["ListAnswer"] as? [String] ?? []
        
        }
        
         _ = self.validateForm()
        
        self.tblAnswer.reloadData()
    }
    
    private func decorate() {
        self.heightContentConstraint.constant = SCREEN_HEIGHT - (Utils.isIPhoneNotch() ? 40 : 15)  - GLOBAL.GlobalVariables.heightKeyboard - 10
        self.topConstraint.constant = Utils.isIPhoneNotch() ? 40 : 15
        
        self.coverContent.layer.cornerRadius = 5.0
        self.coverContent.layer.masksToBounds = true
        
        self.coverInput.layer.cornerRadius = 3.0
        self.coverInput.layer.masksToBounds = true
        self.coverInput.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInput.layer.borderWidth = 0.5
        
        self.coverBtnCancel.backgroundColor = .clear
        self.coverBtnCancel.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverBtnCancel.layer.borderWidth = 1
        self.coverBtnCancel.layer.cornerRadius = 5.0
        
        self.btnCancel.setTitle(LOCALIZED.txt_cancel_title.translate, for: .normal)
        self.btnConfirm.setTitle(LOCALIZED.txt_confirm_title.translate, for: .normal)
    }
    

    
    private func validateForm(_ textCheck: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let tText =  withoutField == self.tfAnswer ? textCheck : self.tfAnswer.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if tText != "" {
            isValid = true
        }
        
        self.coverBtnConfirm.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.coverBtnConfirm.layer.cornerRadius = 5.0
        self.coverBtnConfirm.isUserInteractionEnabled = isValid
        
        if isValid {
            self.coverBtnConfirm.layer.applySketchShadow()
        } else {
            self.coverBtnConfirm.layer.removeSketchShadow()
        }
        
        return isValid
    }

    private func onSelectedAnswer(_ data: [String: Any?]?) {
        if let tData = data {
            self.tfAnswer.text = tData["answer"] as? String ?? ""
        }
        
        _ = validateForm()
    }
}

extension PurposePopup : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listAnswers.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let answer = self.listAnswers[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell") as? AnswerCell
        
        cell?.config(["answer": answer], onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            self.onSelectedAnswer(data)
        })
        
        return cell!
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 35.0
      }

}

extension PurposePopup: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateForm(updatedText, textField)
        }
        
        return true
    }

}
