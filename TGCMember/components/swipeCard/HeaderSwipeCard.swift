//
//  HeaderSwipeCard.swift
//  TGCMember
//
//  Created by vang on 8/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class HeaderSwipeCard: UIView {

    @IBOutlet weak var lblClickUndo: UILabel!
    @IBOutlet weak var coverUndoView: UIView!
    @IBOutlet weak var coverStatus: UIView!
    @IBOutlet weak var lblPendingStatus: UILabel!
    @IBOutlet weak var lblCurrentStatus: UILabel!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var coverPrompt: UIView!
    @IBOutlet weak var imageCurrentStatus: UIImageView!
    
    private var onClickedUndoCallback: DataCallback?
    
    
    func loadView() -> HeaderSwipeCard {
        let view = Bundle.main.loadNibNamed("HeaderSwipeCard", owner: self, options: nil)?[0] as! HeaderSwipeCard
        
        return view
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(resetMask(_:)), name: .ON_RESET_MASK_DEAL, object: nil)
        
        decorate()
    }
    
    @objc func resetMask(_ notification: Notification) {
         self.coverPrompt.isHidden = false
    }
    
    private func decorate() {
        self.coverUndoView.isHidden = true
        self.coverStatus.isHidden = true
        self.coverPrompt.isHidden = false
        
        self.coverPrompt?.layer.cornerRadius = 32.5
        self.coverPrompt?.layer.masksToBounds = true
        
        self.coverStatus?.layer.cornerRadius = 32.5
        self.coverStatus?.layer.masksToBounds = true
        
        self.lblPrompt.text = LOCALIZED.swipe_left_or_right_to_play_with_daily_deals.translate
        self.lblClickUndo.text = LOCALIZED.click_to_undo.translate
    }
    
    @IBAction func onClickedUndo(_ sender: Any) {
        self.onClickedUndoCallback?(nil)
    }
    
    public func config(onClickedUndoCallback _onClickedUndoCallback: DataCallback?) -> Void {
        self.onClickedUndoCallback = _onClickedUndoCallback
    }
    
    
    public func updateView(_ data:[String: Any?]?) {
        if let tData = data {
            let isHideCoverUndo = tData["isHideUndo"] as? Bool ?? true
            let isHideCurrentStatus = tData["isHideCurrentStatus"] as? Bool ?? true
            let textPending = tData["textPending"] as? String ?? ""
            let textCurrent = tData["textCurrent"] as? String ?? ""
            let isHideHeader = tData["isHideHeader"] as? Bool ?? false
            let isHidePrompt = tData["isHidePrompt"] as? Bool ?? false
            let imageCurrentStatus = tData["imageCurrentStatus"] as? String ?? ""
            
            self.lblCurrentStatus.text = textCurrent
            self.lblPendingStatus.text = textPending
            self.coverUndoView.isHidden = isHideCoverUndo
            self.coverStatus.isHidden = isHideCurrentStatus
            self.coverPrompt.isHidden = isHidePrompt
            self.imageCurrentStatus.image = UIImage(named: imageCurrentStatus)
            
            self.isHidden = isHideHeader
            
        }
    }
    
    deinit {
      
    }

}
