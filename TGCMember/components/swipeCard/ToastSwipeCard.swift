//
//  ToastSwipeCard.swift
//  TGCMember
//
//  Created by vang on 12/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ToastSwipeCard: UIView {
    
    @IBOutlet weak var coverToast: UIView!
    @IBOutlet weak var lblToast: UILabel!
    
    func loadView() -> ToastSwipeCard {
        let view = Bundle.main.loadNibNamed("ToastSwipeCard", owner: self, options: nil)?[0] as! ToastSwipeCard
        
        return view
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        decorate()
    }
    
    public func updateMessage(_ text: String) {
        self.lblToast.text = text
    }
    
    private func decorate() {
   
        self.coverToast?.layer.cornerRadius = 22
        self.coverToast?.layer.masksToBounds = true
    }
    
}
