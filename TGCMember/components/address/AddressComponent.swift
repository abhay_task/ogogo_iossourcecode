//
//  AddressComponent.swift
//  TGCMember
//
//  Created by vang on 10/4/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class AddressComponent: UIView, GMSMapViewDelegate {
    private var data:[String: Any?]?
    private var onChangeddCallback: DataCallback?
    
    @IBOutlet weak var mapView: GMSMapView!

    func loadView() -> AddressComponent {
        let aView = Bundle.main.loadNibNamed("AddressComponent", owner: self, options: nil)?[0] as! AddressComponent
        
        return aView
    }
    
   
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        decorate()
        
        self.mapView.delegate = self
        
    }
    
    private func decorate() {
  
    }
    
    private func fillData() {
        if let tData = self.data {
            
        }
    }
    
    public func config(_ data: [String: Any?]?, onChangeddCallback _onChangeddCallback: DataCallback? = nil) -> Void {
           self.data = data
           self.onChangeddCallback = _onChangeddCallback
           
           self.fillData()
       }
}
