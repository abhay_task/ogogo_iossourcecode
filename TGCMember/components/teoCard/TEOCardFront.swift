//
//  TEOCard.swift
//  TGCMember
//
//  Created by vang on 8/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum TEOCardSize: String {
    case BIG = "BigTEOCard"
    case SMALL = "SmallTEOCard"
    case SMALLEST = "SmallestTEOCard"
}

class TEOStyle {
    var bgImage: UIImage?
    var isHideDot: Bool = false
    var companyFont: UIFont?
    var companyColor: UIColor?
    
    var discountFont: UIFont?
    var discountColor: UIColor?
    var discountItemFont: UIFont?
    var discountItemColor: UIColor?
    
    var promptFont: UIFont?
    var promptColor: UIColor?
    
    var distanceFont: UIFont?
    var distanceColor: UIColor?
    
    var lineImage: UIImage?
    
    var bgCoverTextColor: UIColor?
    var bgFooterColor: UIColor?
    
    var expiredDateFont: UIFont?
    var expiredDateColor: UIColor?
    
    required convenience init(_ theme:  TEMPLATE_CARD_DEAL = .ONE,_ cardSize: TEOCardSize = .BIG) {
        self.init()
        
        self.isHideDot = true
        self.bgImage = UIImage(named: "teo_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 5 : 4))
            self.companyColor = .white
            
            self.discountFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size:  (cardSize == .BIG) ? 30 : (cardSize == .SMALL ? 12 : 11))
            self.discountColor = .white
            
            self.discountItemFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: (cardSize == .BIG) ? 27 : (cardSize == .SMALL ? 11 : 10))
            self.discountItemColor = .white
            
            self.promptFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
            self.promptColor = .black
            

            self.distanceFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.distanceColor = UIColor(143, 143, 143)
            
            self.bgCoverTextColor = UIColor(185, 185, 185)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
            
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.companyColor =  .white
            
            self.discountFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 28 : (cardSize == .SMALL ? 12 : 11))
            self.discountColor = UIColor(222, 230, 156)
            
            self.discountItemFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 26 : (cardSize == .SMALL ? 11 : 10))
            self.discountItemColor = UIColor(222, 230, 156)
            
            self.promptFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
            self.promptColor = UIColor(225, 225, 225)
            
            self.distanceFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.distanceColor = .white
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.companyColor = .white
            
            self.discountFont = UIFont(name: LOBSTER_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 49 : (cardSize == .SMALL ? 20 : 19))
            self.discountColor = .white
            
            self.discountItemFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: (cardSize == .BIG) ? 29 : (cardSize == .SMALL ? 12 : 11))
            self.discountItemColor = .white
            
            self.promptFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
            self.promptColor = .white
            
            self.distanceFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.distanceColor = .white
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size:(cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.companyColor = .white
            
            self.discountFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 28 : (cardSize == .SMALL ? 11 : 10))
            self.discountColor = .white
            
            self.discountItemFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 26 : (cardSize == .SMALL ? 10 : 9))
            self.discountItemColor = .white
            
            self.promptFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 13 : (cardSize == .SMALL ? 6 : 5))
            self.promptColor = UIColor(225, 225, 225)
            
            self.distanceFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.distanceColor = .white
            
            self.bgFooterColor = UIColor(51, 51, 51)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 13 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: (cardSize == .BIG) ? 13 : (cardSize == .SMALL ? 5 : 4))
            self.companyColor = .white
            
            self.discountFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 29 : (cardSize == .SMALL ? 12 : 11))
            self.discountColor = .white
            
            self.discountItemFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 31 : (cardSize == .SMALL ? 12 : 11))
            self.discountItemColor = .white
            
            self.promptFont = UIFont(name: MONTSERRAT_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
            self.promptColor = UIColor(225, 225, 225)
            
            self.distanceFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: (cardSize == .BIG) ? 12 : (cardSize == .SMALL ? 5 : 4))
            self.distanceColor = .white
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: MONTSERRAT_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        }
    }
    
}

class TEOCardFront: BaseCardFront {

    private var data: [String: Any?]?
    private var onClickedCardCallback: DataCallback?
    
    @IBOutlet weak var lblExpiredDate: UILabel!
    @IBOutlet weak var heightLine2Constraint: NSLayoutConstraint!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblItemDiscount: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var thumbnailDeal: UIImageView!
    @IBOutlet weak var coverTextView: UIView!
    
    @IBOutlet weak var bgDot: UIImageView!
    
    @IBOutlet weak var btnMoreHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnMoreWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnMoreDetail: UIButton!
    
    @IBOutlet weak var lineThumbnail1: UIImageView!
    @IBOutlet weak var lineThumbnail2: UIImageView!
    
    @IBOutlet weak var coverFooterView: UIView!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBAction func onClickedMoreDetail(_ sender: Any) {
        self.onClickedCardCallback?(self.data)
    }
    
    @IBAction func onClickedOnCard(_ sender: Any) {
        self.onClickedCardCallback?(self.data)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // self.detailView = self.loadSecondView()
    }
    
    func decorate(theme:  TEMPLATE_CARD_DEAL = .ONE, cardSize: TEOCardSize = TEOCardSize.BIG) {

        coverFooterView?.layer.cornerRadius = (cardSize == .BIG) ? 22.0 : (cardSize == .SMALL ? 9 : 9)
        coverFooterView?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]

        contentView?.layer.cornerRadius = (cardSize == .BIG) ? 22.0 : (cardSize == .SMALL ? 9 : 10)
        contentView?.layer.applySketchShadow(color: UIColor(92, 92, 92), alpha: 0.5, x: 2, y: 2, blur: 4, spread: 0)
        
        coverMask?.alpha = 0
        coverMask?.layer.cornerRadius = (cardSize == .BIG) ? 22.0 : (cardSize == .SMALL ? 9 : 9)
        coverMask?.layer.masksToBounds = true

        if theme == .FIVE {
            thumbnailDeal?.layer.cornerRadius = (cardSize == .BIG) ? 22.0 : (cardSize == .SMALL ? 9 : 9)
            thumbnailDeal?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

        let tStyle = TEOStyle(theme, cardSize)
        self.bgImage.image = tStyle.bgImage

        self.lblCompany.textColor = tStyle.companyColor
        self.lblCompany.font = tStyle.companyFont ?? FONT_DEFAULT
        
        self.lblExpiredDate.textColor = tStyle.expiredDateColor
        self.lblExpiredDate.font = tStyle.expiredDateFont ?? FONT_DEFAULT

        self.lblDiscount.textColor = tStyle.discountColor
        self.lblDiscount.font = tStyle.discountFont ?? FONT_DEFAULT

        self.lblItemDiscount.textColor = tStyle.discountItemColor
        self.lblItemDiscount.font = tStyle.discountItemFont ?? FONT_DEFAULT

        self.coverTextView?.backgroundColor = tStyle.bgCoverTextColor
        self.lblDescription.textColor = tStyle.promptColor
        self.lblDescription.font = tStyle.promptFont ?? FONT_DEFAULT

        self.lblDistance.textColor = tStyle.distanceColor
        self.lblDistance.font = tStyle.distanceFont ?? FONT_DEFAULT


        self.lineThumbnail1?.image = tStyle.lineImage
        self.lineThumbnail2?.image = tStyle.lineImage

        self.bgDot?.isHidden = tStyle.isHideDot

    }

    private func fillData() {
        if let tData = self.data {
            //mPrint("deal ->fillData", tData)
            let tCardSize = TEOCardSize(rawValue: String(describing: type(of: self))) ?? .BIG
            
            let dealShowType = Utils.getDealType(tData)
            let typeTpl = Utils.getTemplateDeal(tData)
            
            let tSummary = tData["VendorSummary"] as? [String: Any?] ?? [String: Any?]()
            let tCategory = tSummary["Category"] as? [String: Any?] ?? [String: Any?]()
            decorate(theme: typeTpl, cardSize: tCardSize)
            
            let maskCard = tData["maskCard"] as? MASK_CARD ?? MASK_CARD.NONE
            
            if maskCard == .CLAIMED {
                coverMask?.alpha = 1
            } else {
                coverMask?.alpha = 0
            }
            
            let cateURL = tCategory["ImageUrl"] as? String ?? ""
            self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
                
            })
            
            
            let thumbnailDeal = tData["Photo"] as? String ?? ""
            
            self.thumbnailDeal?.sd_setImage(with: URL(string: thumbnailDeal), completed: { (_, _, _, _) in
                
            })
            
            let title1 = tData["TitleLine1"] as? String ?? ""
            let title2 = tData["TitleLine2"] as? String ?? ""
            self.lblDiscount.text = title1 == "" ? " " : title1
            self.lblItemDiscount.text = title2 == "" ? " " : title2
            
            self.lblCompany.text = tSummary["Name"] as? String ?? ""
            self.lblDescription.text = tData["ShortDescription"] as? String ?? ""
            
            
            self.btnMoreDetail?.setImage(UIImage(named: "click_more_detail_gold_2"), for: .normal)
            
            if tCardSize == .BIG {
                self.btnMoreWidthConstraint?.constant = 146
                self.btnMoreHeightConstraint?.constant = 42
            } else {
                self.btnMoreWidthConstraint?.constant = 58
                self.btnMoreHeightConstraint?.constant = 17
            }
//            let tCardSize = TEOCardSize(rawValue: String(describing: type(of: self))) ?? .BIG
//
//            let typeTpl = Utils.getTemplateDeal(tData)
//            decorate(theme: typeTpl, cardSize: tCardSize)
//
//            let maskCard = tData["maskCard"] as? MASK_CARD ?? MASK_CARD.NONE
//
//            if maskCard == .CLAIMED {
//                coverMask?.alpha = 1
//            } else {
//                coverMask?.alpha = 0
//            }
//
//            let tInfo = tData["DealShow"] as? [String: Any?] ?? [String: Any?]()
//
//            let cateURL = tData["CategoryImageURL"] as? String ?? ""
//            self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
//
//            })
//
//
//            let thumbnailDeal = tInfo["Photo"] as? String ?? ""
//
//            self.thumbnailDeal?.sd_setImage(with: URL(string: thumbnailDeal), completed: { (_, _, _, _) in
//
//            })
//
//            self.lblDiscount.text = tInfo["BigTitle"] as? String ?? ""
//            self.lblItemDiscount.text = tInfo["SmallTitle"] as? String ?? ""
//            self.lblCompany.text = tInfo["Company"] as? String ?? ""
//            self.lblDescription.text = tData["Description"] as? String ?? ""
//
//            var strDate = LOCALIZED.expiry_date_format.translate
//            strDate = strDate.replacingOccurrences(of: "[%__TAG_END_DATE__%]", with: Utils.stringFromStringDate(tData["EndAt"] as? String, toFormat: "dd MMM yyyy"))
//            self.lblExpiredDate.text = strDate
//
//
//            self.btnMoreDetail?.sd_setImage(with: URL(string: tInfo["ButtonImageUrl"] as? String ?? ""), for: .normal, completed: { (image, error, _, _) in
//
//                if error == nil && image != nil {
//                    self.btnMoreWidthConstraint?.constant = image!.size.width/3 *  (tCardSize == .BIG ? 1 : (tCardSize == .SMALL ? 0.4 : 0.3))
//                    self.btnMoreHeightConstraint?.constant = image!.size.height/3 *  (tCardSize == .BIG ? 1 : (tCardSize == .SMALL ? 0.4 : 0.3))
//                } else {
//                    self.btnMoreWidthConstraint?.constant = 0
//                    self.btnMoreHeightConstraint?.constant = 0
//                }
//            })
            
        }

    }
    

    
    override func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }

}


class SmallestTEOCard: TEOCardFront {
    func loadTheme(_ theme: TEMPLATE_CARD_DEAL = .ONE) -> SmallestTEOCard {
        let teoCard = Bundle.main.loadNibNamed("SmallestTEOCard", owner: self, options: nil)?[theme.templateIndex] as! SmallestTEOCard
        
        return teoCard
    }
}
