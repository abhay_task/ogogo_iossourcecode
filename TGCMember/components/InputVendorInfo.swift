//
//  InputVendorInfo.swift
//  TGCMember
//
//  Created by vang on 8/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class InputVendorInfo: UIView {
    private var data: [String: Any?]?
    private var onClickedCancelCallback: DataCallback?
    private var onClickedSubmitCallback: DataCallback?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var coverInputView: UIView!
    @IBOutlet weak var tfVendorName: UITextField!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    var parent: SweepstakeViewController?
    @IBOutlet weak var widthContentConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    @IBAction func onClickedSubmit(_ sender: Any) {
        self.dismissKeyboard()
        setTimeout({
            self.handleSubmit()
        }, 500)
        
    }
    
    private func handleSubmit() {
        if self.validateField(tfVendorName.text) {
            //self.onClickedSubmitCallback?(["code": self.tfVendorName.text?.trimmingCharacters(in: .whitespacesAndNewlines)])
            self.getConfig(self.tfVendorName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
    }
    
    private func getConfig(_ code: String) {
        Utils.showLoading()
        
        APICommonServices.getConfigSweepstake(code) { (resp) in
            mPrint("getConfigSweepstake", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.onClickedSubmitCallback?(resp)
                } else {
                    Utils.showAlertView("", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.dismissKeyboard()
        self.onClickedCancelCallback?(self.data)
    }
    
    private func dismissKeyboard() {
        self.tfVendorName.resignFirstResponder()
    }
    
    private func decorate() {
        widthContentConstraint.constant = UIScreen.main.bounds.width - 50
        
        contentView?.layer.cornerRadius = 3.0
        
        coverInputView?.backgroundColor = .clear
        coverInputView?.layer.cornerRadius = 3.0
        coverInputView?.layer.masksToBounds = true
        coverInputView?.layer.borderWidth = 0.5
        coverInputView?.layer.borderColor = UIColor(205, 207, 214).cgColor
        
        self.lblPrompt?.setAttribute(LOCALIZED.please_enter_the_vendor_name.translate, color: UIColor(39, 39, 39), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13.0), spacing: 4)
        self.lblPrompt?.textAlignment = .center
        
        self.tfVendorName?.placeholder = LOCALIZED.enter_vendor_name.translate
        
        self.btnSubmit?.setTitle(LOCALIZED.txt_submit_title.translate, for: .normal)
        self.btnSubmit?.layer.cornerRadius = 3.0
        _ = self.validateField(self.tfVendorName.text)
        
        self.btnCancel?.setTitleUnderline(LOCALIZED.txt_cancel_title.translate, color: UIColor(228, 109, 87), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
    }
    
    private func validateField(_ value: String? = "") -> Bool {
        var textCheck = ""
        if let tValue = value {
            textCheck = tValue.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        var isValid = false

        if textCheck != "" {
            isValid = true
        }
        
        self.btnSubmit.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSubmit.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnSubmit.layer.applySketchShadow()
        } else {
            self.btnSubmit.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    func loadView() -> InputVendorInfo {
        let view = Bundle.main.loadNibNamed("InputVendorInfo", owner: self, options: nil)?[0] as! InputVendorInfo
        
        return view
    }
    
    private func fillData() {
        
    }
    
    
    public func config(_ data: [String: Any?]?, onClickedSubmitCallback _onClickedSubmitCallback: DataCallback? = nil, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        self.onClickedCancelCallback = _onClickedCancelCallback
        self.onClickedSubmitCallback = _onClickedSubmitCallback
        
        self.fillData()
    }
}

extension InputVendorInfo: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.handleSubmit()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateField(updatedText)
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateField(textField.text)
    }
}
