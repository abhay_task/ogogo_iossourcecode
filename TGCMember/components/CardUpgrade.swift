//
//  CardUpgrade.swift
//  TGCMember
//
//  Created by vang on 7/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CardUpgrade: UIView {
    
    private var data: [String: Any?]?
    
    private var onClickedCardCallback: DataCallback?
    private var onClickedUpgradeCallback: DataCallback?
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var lblPlan: UILabel!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var upgradeView: HighlightableView!
    @IBOutlet weak var lblCurrentPlan: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var currentIndex: Int = -1
    var currentPlanIndex: Int = -1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        if var tData = self.data {
            tData["currentIndex"] = self.currentIndex
            self.onClickedUpgradeCallback?(tData)
        }
    }
    
    @IBAction func onClickedCard(_ sender: Any) {
        self.onClickedCardCallback?(self.data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadView() -> CardUpgrade {
        let cardUpgrade = Bundle.main.loadNibNamed("CardUpgrade", owner: self, options: nil)?[0] as! CardUpgrade
        
        
        return cardUpgrade
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateCard(_:)), name: .SELECTED_CARD_UPGRADE, object: nil)
    }
    
    @objc func onUpdateCard(_ notification: Notification) {
        self.styleSelected(false)
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            self.styleSelected(uID == mID)
        }
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblPrice.text =  "\(tData["Currency"] as? String ?? "$")\(tData["Price"] as? Int ?? 0 )"
            
            let tUnix = tData["SubscriptionUnix"] as? String ?? ""
            self.lblPlan.text = "/\(tUnix.uppercased())"
            
            let tName = tData["Name"] as? String ?? ""
            self.lblType.text = tName.uppercased()
            
            self.lblDescription.text = tData["SubDescription"] as? String
            
            let isCurrentPlan = tData["IsCurrentPlan"] as? Bool ?? false
            
            if isCurrentPlan {
                self.lblCurrentPlan.text = LOCALIZED.current_plan.translate
            } else  {
                self.lblCurrentPlan.text = currentIndex < currentPlanIndex ? LOCALIZED.txt_downgrade_title.translate : LOCALIZED.txt_upgrade_title.translate
            }
            
            self.upgradeView.isUserInteractionEnabled = !isCurrentPlan
            
            self.styleSelected(isCurrentPlan)
        }
    }
    
    
    private func styleSelected(_ isSelected: Bool = true) {
        self.bgImage.image = isSelected ? UIImage(named: "upgrade_yellow_bg") : UIImage(named: "upgrade_white_bg")
        self.lblPrice.textColor = isSelected ? .white : UIColor(hexFromString: "#FFC106")
        self.lblPlan.textColor = isSelected ? .white : UIColor(hexFromString: "#FFC106")
        self.lblType.textColor = isSelected ? .white : .black
        
        self.upgradeView.layer.cornerRadius = 3.0
        self.upgradeView.layer.masksToBounds = true
        self.upgradeView.backgroundColor = isSelected ? .white : UIColor(hexFromString: "#FFC106")
        
        self.lblCurrentPlan.textColor = isSelected ? UIColor(hexFromString: "#6B6B6B") : .white
        self.lblDescription.textColor = isSelected ? .white : UIColor(hexFromString: "#6B6B6B")
    }
    
   
    
    func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback?) -> Void {
    
        self.data = data
        
        self.onClickedCardCallback = _onClickedCardCallback
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .SELECTED_CARD_UPGRADE, object: nil)
    }

}
