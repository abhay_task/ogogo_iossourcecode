//
//  CreateTripView.swift
//  TGCMember
//
//  Created by vang on 10/11/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CreateTripView: UIView {
    
    @IBOutlet weak var lblDateTo: UILabel!
    @IBOutlet weak var lblDateFrom: UILabel!
    @IBOutlet weak var tfPurpose: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lblAboutTrip: UILabel!
    
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblWhere: UILabel!
    
    @IBOutlet weak var coverTo: UIView!
    @IBOutlet weak var coverFrom: UIView!
    //@IBOutlet weak var coverInputWhere: UIView!
    @IBOutlet weak var coverInputPurpose: UIView!
    @IBOutlet weak var coverInputName: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddTrip: UIButton!
    
    private var data: [String: Any?]?
    
    private var onClickedCancelCallback: DataCallback?
    private var onClickedAddTripCallback: DataCallback?
    
    var parent: UIViewController?
    private var selectedPurpose: [String: Any?]?
    private var selectedAnswer: String = ""
    private var selectedFromDate: String = ""
    private var selectedToDate: String = ""
    
    private var countriesList: [[String: Any?]] = []
    
    private var selectedCountry: [String: Any?]?
    
    
    func loadView() -> CreateTripView {
        let aView = Bundle.main.loadNibNamed("CreateTripView", owner: self, options: nil)?[0] as! CreateTripView
        
        return aView
    }
    
    private func dismissKeyboard() {
        //self.tf
    }
    
    @IBAction func onClickedDropDownManyMember(_ sender: Any) {
        
    }
    
    
    @IBAction func onClickedDateFrom(_ sender: Any) {
        if let app = GLOBAL.GlobalVariables.appDelegate, let vc = app.getTopViewController()  {
            let popup = DatePickerView()
            popup.maximumDate = self.selectedToDate
            popup.selectedDate = self.selectedFromDate
            
            popup.callback = { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data {
                    
                    self.selectedFromDate = tData["Date"] as? String ?? ""
                    self.lblDateFrom.text = self.selectedFromDate
                    
                    _ = self.validateForm()
                }
                
            }
            
            vc.addSubviewAsPresent(popup)
        }
    }
    
    @IBAction func onClickedDateTo(_ sender: Any) {
        if let app = GLOBAL.GlobalVariables.appDelegate, let vc = app.getTopViewController()  {
            let popup = DatePickerView()
            popup.minimumDate = self.selectedFromDate
            popup.selectedDate = self.selectedToDate
            
            popup.callback = { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data {
                    
                    self.selectedToDate = tData["Date"] as? String ?? ""
                    self.lblDateTo.text = self.selectedToDate
                    
                    _ = self.validateForm()
                }
            }
            
            vc.addSubviewAsPresent(popup)
        }
    }
    
    
    
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.onClickedCancelCallback?(self.data)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Utils.dismissKeyboard()
    }
    
    @IBAction func onClickedAddTrip(_ sender: Any) {
        let tName = self.tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let tAnswer = self.selectedAnswer
        var tPurposeId = ""
        if let sPurpose = self.selectedPurpose {
            tPurposeId = sPurpose["ID"] as? String ?? ""
        }
        
        var countryCode = 0
        if let sCountry = self.selectedCountry {
            countryCode = sCountry["countryCode"] as? Int ?? 0
        }
        
        var countryName = ""
        if let sCountry = self.selectedCountry {
            countryName = sCountry["countryName"] as? String ?? ""
        }
        
        var params = [String: Any]()
        params["name"] = tName
        params["purpose_id"] = tPurposeId
        params["country_code"] = countryCode
        params["country"] = countryName
        params["answer"] = tAnswer
        params["from_date"] = self.selectedFromDate
        params["to_date"] = self.selectedToDate
        
        self.onClickedAddTripCallback?(params)
        
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        dismissKeyboard()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.decorate()
    }
    
    
    private func decorate() {
        
        self.btnCancel?.setTitleUnderline(LOCALIZED.txt_cancel_title.translate, color: UIColor("rgb 228 109 87"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13))
        
        self.tfName?.placeholder = LOCALIZED.what_is_the_name_of_this_trip.translate
        self.tfPurpose.placeholder = LOCALIZED.what_is_the_purpose_of_the_trip.translate
        //self.tfWhere?.placeholder = LOCALIZED.eg_tokyo_japan.translate
        
        self.lblAboutTrip?.text = LOCALIZED.about_your_trip.translate
        self.lblWhere?.text = LOCALIZED.where_will_you_plan_to_go.translate
        self.lblTo?.text = LOCALIZED.txt_to_title.translate.upperCaseFirstLetter()
        self.lblFrom?.text = LOCALIZED.txt_from_title.translate.upperCaseFirstLetter()
        
        self.coverInputName?.backgroundColor = .white
        self.coverInputName?.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputName?.layer.borderWidth = 0.5
        self.coverInputName?.layer.cornerRadius = 3.0
        self.coverInputName?.layer.masksToBounds = true
        
        self.coverInputPurpose?.backgroundColor = .white
        self.coverInputPurpose?.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputPurpose?.layer.borderWidth = 0.5
        self.coverInputPurpose?.layer.cornerRadius = 3.0
        self.coverInputPurpose?.layer.masksToBounds = true
        
        
        self.coverFrom?.backgroundColor = .white
        self.coverFrom?.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverFrom?.layer.borderWidth = 0.5
        self.coverFrom?.layer.cornerRadius = 3.0
        self.coverFrom?.layer.masksToBounds = true
        
        
        self.coverTo?.backgroundColor = .white
        self.coverTo?.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverTo?.layer.borderWidth = 0.5
        self.coverTo?.layer.cornerRadius = 3.0
        self.coverTo?.layer.masksToBounds = true
        
        
        _ = self.validateForm()
    }
    
    
    
    private func validateForm(_ textCheck: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let tName =  withoutField == self.tfName ? textCheck : self.tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let tAnswer = self.selectedAnswer
        var tPurposeId = ""
        if let sPurpose = self.selectedPurpose {
            tPurposeId = sPurpose["ID"] as? String ?? ""
        }
        
        var countryCode = 0
        var countryName = ""
        if let sCountry = self.selectedCountry {
            countryCode = sCountry["countryCode"] as? Int ?? 0
            countryName = sCountry["countryName"] as? String ?? ""
        }
        
        
        if tName != nil && tName != "" && tAnswer != "" && tPurposeId != "" && (countryCode != 0 || countryName != "") && self.selectedFromDate != "" && selectedToDate != "" {
            isValid = true
        }
        
        self.btnAddTrip?.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnAddTrip?.layer.cornerRadius = 5.0
        self.btnAddTrip?.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnAddTrip.layer.applySketchShadow()
        } else {
            self.btnAddTrip.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    public func reset() {
        self.selectedAnswer =  ""
        self.selectedFromDate = ""
        self.selectedToDate = ""
        self.selectedCountry = nil
        self.selectedFromDate = ""
        self.selectedToDate = ""
        
        self.tfName.text = ""
        self.tfPurpose.text = ""
        //self.tfWhere.text = ""
        self.lblDateFrom.text = ""
        self.lblDateTo.text = ""
        
        _ = self.validateForm()
    }
    
    private func prepareData() {
        let isEdit = self.data == nil ? false : true
        
        self.btnAddTrip.setTitle(isEdit ? LOCALIZED.txt_edit_trip.translate : LOCALIZED.txt_add_trip.translate, for: .normal)
        
        if (GLOBAL.GlobalVariables.purposeCreateTrip == nil) {
            self.getPurpose {
                
                if isEdit {
                    self.remapData()
                } else {
                    self.mapData()
                }
            }
        } else {
            self.selectedPurpose = GLOBAL.GlobalVariables.purposeCreateTrip
            
            if isEdit {
                self.remapData()
            } else {
                self.mapData()
            }
        }
        
    }
    
    private func mapData() {
        if let tData = GLOBAL.GlobalVariables.purposeCreateTrip {
            self.tfPurpose.placeholder = tData["Question"] as? String ?? ""
        }
        
        _ = validateForm()
    }
    
    
    private func remapData() {
        if let tData = self.data {
            mPrint("remapData -> ", tData)
            self.tfName.text = tData["Name"] as? String ?? ""
            
            self.selectedAnswer = tData["Anwser"] as? String ?? ""
            self.tfPurpose.text = self.selectedAnswer
            self.selectedPurpose = ["ID": tData["TripPreferenceID"] as? String ?? ""]
            
            //self.tfWhere.text = tData["CountryName"] as? String ?? ""
            self.selectedCountry = ["countryName": tData["CountryName"] as? String ?? "", "countryCode":  tData["CountryCode"] as? String ?? ""]
            
            
            self.selectedFromDate = Utils.stringFromStringDate(tData["FromDateAt"] as? String ?? "", toFormat: "yyyy-MM-dd")
            self.selectedToDate =  Utils.stringFromStringDate(tData["ToDateAt"] as? String ?? "", toFormat: "yyyy-MM-dd")
            
            self.lblDateFrom.text = self.selectedFromDate
            self.lblDateTo.text = self.selectedToDate
        }
        
        _ = validateForm()
    }
    
    private func getPurpose(_ finished: FinishedCallback? = nil) {
        APICommonServices.getPurposeInCreateTrip { (resp) in
            mPrint("getPurposeInCreateTrip", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?], let list = data["list"] as? [[String: Any?]], list.count > 0, let firstData = list[0] as? [String: Any?] {
                    
                    var purpose = [String: Any?]()
                    
                    purpose["ID"] = firstData["ID"] as? String ?? ""
                    
                    if let objQuestion =  firstData["Question"] as? [String: Any?] {
                        purpose["Question"] = objQuestion["en"] as? String ?? ""
                    }
                    
                    var listAnswers:[String] = []
                    if let objAnswer =  firstData["ListAnswer"] as? [[String: Any?]] {
                        for answer in objAnswer {
                            listAnswers.append(answer["en"] as? String ?? "")
                        }
                    }
                    
                    purpose["ListAnswer"] = listAnswers
                    
                    purpose["IsDefault"] = firstData["IsDefault"] as? Bool ?? true
                    
                    GLOBAL.GlobalVariables.purposeCreateTrip = purpose
                    
                    self.selectedPurpose = GLOBAL.GlobalVariables.purposeCreateTrip
                    
                    //mPrint("getPurposeInCreateTrip after parse -> ",  self.selectedPurpose)
                }
            }
            
            finished?()
            
        }
    }
    
    
    public func config(_ data: [String: Any?]?, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil, onClickedAddTripCallback  _onClickedAddTripCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCancelCallback = _onClickedCancelCallback
        self.onClickedAddTripCallback = _onClickedAddTripCallback
        
        self.prepareData()
    }
}


extension CreateTripView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateForm(updatedText, textField)
        }
        
        return true
    }
    
}
