//
//  EventListViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class EventComponent: BaseViewController {
    
    @IBOutlet weak var eventCollectionView: UICollectionView!
    @IBOutlet weak var durationCollectionView: UICollectionView!
    
    var owner: UIViewController?
    var durations: [String] = []
    var selectedDuration: String = ""
    var onClickDuration: Bool = false
    var beginScroll: Bool = false
    
    var events: [String:[[String: Any?]]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.durationCollectionView.delegate = self
        self.durationCollectionView.dataSource = self
        
        self.eventCollectionView.delegate = self
        self.eventCollectionView.dataSource = self
        
        let alignedFlowLayout = eventCollectionView.collectionViewLayout as? AlignedCollectionViewFlowLayout
        alignedFlowLayout?.horizontalAlignment = .left
        
        self.durationCollectionView.register(UINib(nibName: "DurrationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DurrationCollectionViewCell")
        self.eventCollectionView.register(UINib(nibName: "NormalEventCell", bundle: nil), forCellWithReuseIdentifier: "NormalEventCell")
    
        self.eventCollectionView.panGestureRecognizer.addTarget(self, action: #selector(handleScrollViewGestureRecognizer(_:)))
        self.eventCollectionView.addObserver(self, forKeyPath:"contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
        
    }
    
    func updateLanguages() {
        
    }
    
    public func reloadContent() {
        self.durationCollectionView.reloadData()
        self.eventCollectionView.reloadData()
    }
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if beginScroll && onClickDuration == false {
            if let dictChange = change, let newPoint: CGPoint = dictChange[NSKeyValueChangeKey.newKey] as? CGPoint {
                // print("CHANGE OBSERVED: \(newPoint.y)")
                
                let section = self.getSectionAt(newPoint)
                
                if section >= 0 {
                    if (durations[section] != selectedDuration) {
                        selectedDuration = durations[section]
                        durationCollectionView.reloadData()
                    }
                }
                
            }
        }
    }
    
    private func getSectionAt(_ newPoint: CGPoint) -> Int {
        var tSection = -1
        
        for index in 0..<durations.count {
            let indexReversed = (durations.count - 1) - index
            
            var yHeader:CGFloat = 0.0
            
            if indexReversed != 0 {
                let headerView = self.eventCollectionView.layoutAttributesForItem(at: IndexPath(row: 0, section: indexReversed))
                yHeader = headerView?.frame.origin.y ?? 0.0
            }
            
            if newPoint.y >= yHeader {
                tSection = indexReversed
                break
            }
            
        }
        
        return tSection
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    @objc private func handleScrollViewGestureRecognizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            beginScroll = true
        default:
            break
        }
    }
    
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension EventComponent: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == durationCollectionView) {
            return durations.count
        }
        
        let key = durations[section]
        if let arrItems = events[key] {
            return arrItems.count
        }
        
        
        return 0
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == durationCollectionView) {
            let durationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DurrationCollectionViewCell", for: indexPath) as! DurrationCollectionViewCell
            
            let tDict = ["duration": durations[indexPath.row], "selected": selectedDuration]
            
            durationCell.config(tDict, onClickedCellCallback: { (data) in
                if let tData = data {
                    self.onClickDuration = true
                    
                    self.selectedDuration = tData["duration"] as! String
                    self.durationCollectionView.reloadData()
                    let sectionIndex = self.durations.firstIndex(where: {$0 == self.selectedDuration})
                    self.eventCollectionView.scrollToItem(at: IndexPath(row: 0, section: sectionIndex!), at: .top, animated: true)
                }
                
            })
            
            return durationCell
        } else if (collectionView == eventCollectionView) {
            let eventCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NormalEventCell", for: indexPath) as! NormalEventCell
            let key = durations[indexPath.section]
            
            var tDict = [String: Any?]()
            if let arrItems = events[key] {
                tDict = arrItems[indexPath.row]
                tDict["index"] = indexPath.row
            }
            
            eventCell.config(tDict, onClickedCellCallback: { (data) in
                let eventDetailVC = EventDetailViewController()
                eventDetailVC.eventInfo = data
                
                self.owner?.navigationController?.pushViewController(eventDetailVC, animated: true)
            })
            
            
            UIView.performWithoutAnimation {
                eventCell.layoutIfNeeded()
            }
            
            return eventCell
        }
        
        return UICollectionViewCell()
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView == durationCollectionView) {
            return 1
        }
        
        return durations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (collectionView == durationCollectionView) {
            
            //            if (durations.count <= 4) {
            //                let tWidth = UIScreen.main.bounds.width/CGFloat(self.durations.count)
            //
            //                return CGSize(width: tWidth, height: 35)
            //            } else {
            let text = durations[indexPath.row]
            
            let label = UILabel()
            label.numberOfLines = 1
            label.text = text
            label.font = UIFont.systemFont(ofSize: 15)
            
            let maximumLabelSize: CGSize = CGSize(width: 280, height: 9999)
            let expectedLabelSize: CGSize = label.sizeThatFits(maximumLabelSize)
            
            return CGSize(width: expectedLabelSize.width + 20 , height: 40)
            // }
        }
        
        let key = durations[indexPath.section]
        if let arrItems = events[key] {
//            let data = arrItems[indexPath.row]
            if (indexPath.row == 0) {
                let tWidth = UIScreen.main.bounds.width - 50
                let heightImage: CGFloat = tWidth * (150/325)
                
                return CGSize(width: tWidth , height: heightImage + 67)
            }
        }
        
        let tWidth = (UIScreen.main.bounds.width - 60 )/2
        let heightImage: CGFloat = tWidth * (99/158)
        
        return CGSize(width: tWidth, height: heightImage + 84)
    }
    
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self .perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
        
    }
    
    @objc internal func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget:self)
        
        onClickDuration = false
    }
}
