//
//  CustomKolodaView.swift
//  Koloda
//
//  Created by Eugene Andreyev on 7/11/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit

let defaultTopOffset: CGFloat = 20
let defaultHorizontalOffset: CGFloat = 10
let defaultHeightRatio: CGFloat = 1.25
let backgroundCardHorizontalMarginMultiplier: CGFloat = 0.25
let backgroundCardScalePercent: CGFloat = 1

class VerticalSwipeView: KolodaView {
    
    var numberItems: CGFloat = 3
    
    override func frameForCard(at index: Int) -> CGRect {
        if index == 0 {
            let topOffset: CGFloat = defaultTopOffset +  (numberItems - CGFloat(index + 1)) * 50
            let xOffset: CGFloat = defaultHorizontalOffset
            let width: CGFloat = 300
            let height: CGFloat = 200
            let yOffset: CGFloat = topOffset
            let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
            
            return frame
        } else {
//            let horizontalMargin = -self.bounds.width * backgroundCardHorizontalMarginMultiplier
//            let width = self.bounds.width * backgroundCardScalePercent
//            let height = width * defaultHeightRatio
//            return CGRect(x: horizontalMargin, y: 0, width: width, height: height)
            
            let topOffset: CGFloat = defaultTopOffset +  (numberItems - CGFloat(index + 1)) * 50
            let xOffset: CGFloat = defaultHorizontalOffset
            let width: CGFloat = 300
            let height: CGFloat = 200
            let yOffset: CGFloat = topOffset
            let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
            
            return frame
        }
        
    }

}
