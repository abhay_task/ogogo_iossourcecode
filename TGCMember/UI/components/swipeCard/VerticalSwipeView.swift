//
//  CustomKolodaView.swift
//  Koloda
//
//  Created by Eugene Andreyev on 7/11/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit

let spaceItem: CGFloat = 10
let ratio: CGFloat = 310/480 // w:h
let maxHeightItem:CGFloat = 480
let headerSwipe: CGFloat = 115

public class VerticalSwipeView: KolodaView {
    var isFirstLoad: Bool = true
    var paddingBottomItem: CGFloat = 0.0
    var paddingTopItem: CGFloat = 0.0
    
    var heightCover: CGFloat = 0.0
    
    var animationDelegate: SwipeAppearanceDelegate?
    private var draggableCardViews: [DraggableCardView] = []
    
    private var onRunOutOfCardsCallback: DataCallback?
    
    private var onRemoveCardCallback: DataCallback?
    private var onCheckCardCallback: DataCallback?
    private var onDislikeCardCallback: DataCallback?
    private var onLikeCardCallback: DataCallback?
    
    private var onReadyCallback: DataCallback?

    private var timerDisplayToast: Timer?
    private var timerUndo: Timer?
    private var timerHoldCard: Timer?
    private var data: [String: Any]?
    
    private var dealPending: [String: Any?]?
    private var statusPending: MASK_DEAL_STATUS = .NONE
    private var isHolding: Bool = false
    
    private var headerView: HeaderSwipeCard?
    private var toastView: ToastSwipeCard?

    private let maxNumberCardForShow: Int = 5
    
    private var totalDeals: [[String: Any?]] = []
    private var dealsForShow: [[String: Any?]] = []
    private var bufferDeals: [[String: Any?]] = []
    

    override public func frameForCard(at index: Int) -> CGRect {
     
        let heightView = heightCover == 0 ? SCREEN_HEIGHT : heightCover
        self.heightCover = heightView
         
        let widthView = SCREEN_WIDTH
        
        let heightItem: CGFloat = min((heightView - paddingBottomItem - paddingTopItem - (CGFloat(countOfCards - 1) * spaceItem)), maxHeightItem)
        let widthItem:CGFloat = heightItem * ratio
    
        let centerOffset: CGFloat = (heightView - heightItem - CGFloat(countOfCards - 1) * spaceItem)/2
         // top offset for LATEST ITEM
        //let spaceBottom = (heightView - heightItem) / 2
        
        var topOffset: CGFloat = 0
        if centerOffset <= paddingBottomItem {
            topOffset = centerOffset - abs(centerOffset - paddingBottomItem)
        } else {
            topOffset = centerOffset - paddingBottomItem
        }
       
        topOffset += 35
        
        let xOffset: CGFloat = (widthView - widthItem) / 2
        let width: CGFloat = widthItem
        let height: CGFloat = heightItem
        let yOffset: CGFloat = topOffset + CGFloat(countOfCards - 1 - index) * spaceItem
        let frame = CGRect(x: xOffset, y: yOffset, width: width, height: height)
        
        return frame
        
    }
    
    private func onClickedUndo() {
        if let tTimer = self.timerUndo {
            tTimer.invalidate()
        }
        
        self.headerView?.isHidden = true
        
        if let tDateRemove = self.dealPending {
            self.totalDeals.insert(tDateRemove, at: 0)
            self.reloadData(forceAnimation: false)
            
            self.dealPending = nil
        }
        
        
        print("onClickedUndo")
    }
    
    private func decorateToast() {
        guard let _ = self.toastView else {
       
            self.toastView = ToastSwipeCard().loadView()
            self.toastView?.alpha = 0
            
            self.addSubview(self.toastView!)
            
            self.toastView?.snp.makeConstraints({ (make) in
                make.bottom.equalTo(self).offset(Utils.isIPhoneNotch() ? -85 : -50)
                make.left.equalTo(self).offset(0)
                make.width.equalTo(SCREEN_WIDTH)
                make.height.equalTo(44)
            })

            return
        }
    }
    
    private func decorateHeader() {
        guard let _ = self.headerView else {
       
            self.headerView = HeaderSwipeCard().loadView()
            self.headerView?.config(onClickedUndoCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.onClickedUndo()
            })
            
            self.addSubview(self.headerView!)
            
            
            let heightHeader = Utils.isIPhoneNotch() ? 110 : 100
            self.headerView?.snp.makeConstraints({ (make) in
                make.top.equalTo(self).offset(0)
                make.right.equalTo(self).offset(0)
                make.width.equalTo(SCREEN_WIDTH)
                make.height.equalTo(heightHeader)
            })

            return
        }
    }

    private func updateHeaderView(currentStatus: MASK_DEAL_STATUS = .NONE, pendingStatus: MASK_DEAL_STATUS = .NONE, isHideUndo: Bool = true) {
        var textPending: String = ""
        var textCurrent: String = ""
        var imageCurrentStatus: String = "swipe_add_ic"
        
//        let isHideCurrentStatus = !isHideUndo
//        let isHideHeader = (currentStatus == .NONE && pendingStatus == .NONE) ? true : false
       
       
        
        if pendingStatus == .REMOVE {
            textPending = LOCALIZED.refused_offer.translate
        } else if pendingStatus == .CHECK {
            textPending = LOCALIZED.added_to_wallet.translate
        } else if pendingStatus == .DISLIKE {
            textPending = LOCALIZED.offer_blocked.translate
        } else if pendingStatus == .LIKE {
            textPending = LOCALIZED.added_to_favorites.translate
        }
        
        if currentStatus == .REMOVE {
            textCurrent = LOCALIZED.refusing_offer.translate
            imageCurrentStatus = "swipe_refusing_ic"
        } else if currentStatus == .CHECK {
            textCurrent = LOCALIZED.adding_to_wallet.translate
            imageCurrentStatus = "swipe_add_ic"
        } else if currentStatus == .DISLIKE {
            imageCurrentStatus = "swipe_refusing_ic"
            textCurrent = LOCALIZED.blocking_offer.translate
        } else if currentStatus == .LIKE {
            imageCurrentStatus = "swipe_add_ic"
            textCurrent = LOCALIZED.adding_to_favorites.translate
        }
        
        let isHideCurrentStatus = ((currentStatus == .NONE && pendingStatus == .NONE) || textCurrent == "") ? true : false
        let isHideHeader = false;
        
        var tData = [String: Any]()
        tData["isHideUndo"] = isHideUndo
        tData["isHideCurrentStatus"] = isHideCurrentStatus
        tData["textPending"] = textPending
        tData["textCurrent"] = textCurrent
        tData["imageCurrentStatus"] = imageCurrentStatus
        tData["isHideHeader"] = isHideHeader
        tData["isHidePrompt"] = !((isHideCurrentStatus || textCurrent == "") && self.dealsForShow.count > 0)
        
        
        //print("isHideCurrentStatus --> ", isHideCurrentStatus)
        self.headerView?.updateView(tData)
    }
    
//    public func updateData(_ data: [String: Any]?) {
//        self.data = data
//
//        if let tData = self.data {
//            self.deals = tData["deals"] as? [[String: Any?]] ?? []
//        }
//    }
    
 
    public func startAnimation() {
        
        self.displayCardWithAnimation(Int(countOfCards - 1), animation: isFirstLoad)
        
        isFirstLoad = false
    }
    
    public func stopAnimation() {
        for draggableCardView in self.draggableCardViews {
            draggableCardView.alpha = 0
        }
    }
    
    public func config(_ data: [String: Any]?, onRemoveCardCallback _onRemoveCardCallback: DataCallback?, onCheckCardCallback _onCheckCardCallback: DataCallback?, onDislikeCardCallback _onDislikeCardCallback: DataCallback?, onLikeCardCallback _onLikeCardCallback: DataCallback?, onRunOutOfCardsCallback _onRunOutOfCardsCallback: DataCallback?, onReadyCallback _onReadyCallback: DataCallback?) -> Void {
        self.data = data
        
        self.onRemoveCardCallback = _onRemoveCardCallback
        self.onCheckCardCallback = _onCheckCardCallback
        self.onDislikeCardCallback = _onDislikeCardCallback
        self.onLikeCardCallback = _onLikeCardCallback
        
        self.onRunOutOfCardsCallback = _onRunOutOfCardsCallback
        self.onReadyCallback = _onReadyCallback
        
        if let tData = self.data {
            self.totalDeals = tData["deals"] as? [[String: Any?]] ?? []
        }
        
        self.configDealsForShow()
    }
    
    private func addMoreDealForShowIfHave() {
        if self.bufferDeals.count > 0 {
            let firstDeal = self.bufferDeals[0]
            
            self.dealsForShow.append(firstDeal)
            
            self.bufferDeals.removeFirst()
        }
    }

    private func configDealsForShow() {
        self.decorateHeader()
        self.decorateToast()
        
        if self.totalDeals.count > self.maxNumberCardForShow {
            for index in 0..<self.totalDeals.count {
                let item = self.totalDeals[index]
                
                if index < self.maxNumberCardForShow {
                    self.dealsForShow.append(item)
                } else {
                    self.bufferDeals.append(item)
                }
            }
        } else {
            self.dealsForShow = self.totalDeals
            self.bufferDeals = []
        }
        
        self.extDelegate = self
        self.delegate = self
        self.dataSource = self
        
        self.visibleCardsDirection = .top
        self.backgroundCardsTopMargin = 50
        self.alphaValueTransparent = 1.0
        self.alphaValueSemiTransparent = 1.0
    }
    
    private func displayCardWithAnimation(_ atIndex: Int = 0, animation: Bool = true) {
        if atIndex < 0 || atIndex >= self.draggableCardViews.count {
            return
        }
        
        let dragCard = self.draggableCardViews[atIndex]
        
        self.animationDelegate?.prepareAnimation?(self, atIndex)
        
        if animation {
            dragCard.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.6,
                           animations: {
                            dragCard.transform = CGAffineTransform(scaleX: 1, y: 1)
                            dragCard.alpha = 1
                            
                            self.animationDelegate?.beginAnimation?(self, atIndex)
            },
                           completion: { [weak self] _ in
                            guard let self = self else {return}
                            dragCard.alpha = 1
                            
                            self.animationDelegate?.completionAnimation?(self, atIndex)
                            dragCard.transform = CGAffineTransform.identity
            })
            
            setTimeout({
                self.displayCardWithAnimation(atIndex - 1)
            }, 50)
        } else {
            dragCard.alpha = 1
            self.animationDelegate?.completionAnimation?(self, atIndex)
            
            self.displayCardWithAnimation(atIndex - 1, animation: animation)
        }
        
        
        
    }
    
}


@objc public protocol SwipeAppearanceDelegate {
    @objc optional func prepareAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int)
    @objc optional func beginAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int)
    @objc optional func completionAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int)
}


extension VerticalSwipeView: KolodaViewExtDelegate {
    public func kolodaLoaded(_ koloda: KolodaView, card: DraggableCardView, atIndex: Int) {

        card.alpha = isFirstLoad ? 0 : 1
        
        self.draggableCardViews.append(card)
        
        if atIndex == Int(countOfCards - 1) {
            setTimeout({ [weak self] in
                guard let self = self else {return}
                
                self.onReadyCallback?(self.data)
                
                if let header = self.headerView {
                    self.bringSubviewToFront(header)
                }
           }, 500)
        }
        
    }
}

extension VerticalSwipeView: KolodaViewDelegate {
    
    public func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        // drag all card
        //self.onRunOutOfCardsCallback?(self.data)
    }
    
    public func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let data = self.dealsForShow[index]
    
    }
    
    public func koloda(_ koloda: KolodaView, draggedCardWithPercentage finishPercentage: CGFloat, in direction: SwipeResultDirection) {
        let index = koloda.currentCardIndex
        let data = dealsForShow[index]
        
       
        if finishPercentage == 100 {
            
            self.timerForHoldCard(card: data, direction: direction)
            
            if direction == .left || direction == .bottomLeft || direction == .topLeft{
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_REMOVE_DEAL, object: data)
                self.updateHeaderView(currentStatus: .REMOVE, pendingStatus: self.statusPending, isHideUndo: true)
            } else {
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_CHECK_DEAL, object: data)
                self.updateHeaderView(currentStatus: .CHECK, pendingStatus: self.statusPending, isHideUndo: true)
            }
        } else {
            
            NotificationCenter.default.post(name: .ON_RESET_MASK_DEAL, object: data)
            self.updateHeaderView(currentStatus: .NONE, pendingStatus: .NONE, isHideUndo: true)
            self.stopTimerHoldCard()
        }
        
    }
    
    private func stopTimerHoldCard() {
        self.isHolding = false
        
        if let tTimer = self.timerHoldCard {
            tTimer.invalidate()
        }
    }
    
    public func kolodaDidResetCard(_ koloda: KolodaView) {
        self.stopTimerHoldCard()
        
        let index = koloda.currentCardIndex
        let data = dealsForShow[index]

        
        NotificationCenter.default.post(name: .ON_RESET_MASK_DEAL, object: data)
        self.updateHeaderView(currentStatus: .NONE, pendingStatus: .NONE, isHideUndo: true)
    }
    
    public func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
       
        self.submitDataIfHave()
        
        if direction == .left || direction == .bottomLeft || direction == .topLeft {
            self.statusPending = self.isHolding ? .DISLIKE : .REMOVE
        } else  {
            self.statusPending = self.isHolding ? .LIKE : .CHECK
        }
        
        self.updateHeaderView(currentStatus: .NONE, pendingStatus: self.statusPending, isHideUndo: Constants.TIME_INTERVAL_UNDO_CARD == 0) //vangdoan
        
        //print("statusPending \(self.statusPending)")
        
        self.stopTimerHoldCard()
        self.reinitView(index)

    }
    
    
    private func timerForUndo(_ timeInterval: TimeInterval = Constants.TIME_INTERVAL_UNDO_CARD) {
      
        //self.headerView?.isHidden = false

        if let tTimer = self.timerUndo {
            tTimer.invalidate()
        }
        
        self.timerUndo = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { timer in
            //self.headerView?.isHidden = true
            
            self.submitDataIfHave()
        }
    }
    
    private func timerForDisplayToast(_ status: MASK_DEAL_STATUS, _ timeInterval: TimeInterval = Constants.TIME_INTERVAL_TOAST_CARD) {
        var textToast = ""
        
        if status == .CHECK {
            textToast = LOCALIZED.you_just_added_1_deal_into_wallet.translate
        } else if status == .REMOVE {
            textToast = LOCALIZED.you_just_refused_1_deal.translate
        }
        
        if self.toastView != nil {
            self.toastView?.superview?.bringSubviewToFront(self.toastView!)
        }
        
        self.toastView?.updateMessage(textToast)
        self.toastView?.alpha = 1
        
        if let tTimer = self.timerDisplayToast {
            tTimer.invalidate()
        }
        
        self.timerDisplayToast = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { timer in
            self.toastView?.alpha = 0
            
            if self.dealsForShow.count == 0 {
                self.onRunOutOfCardsCallback?(nil)
            }
        }
    }
    
    private func timerForHoldCard(_ timeInterval: TimeInterval = Constants.TIME_INTERVAL_HOLDING_CARD, card:[String: Any?], direction: SwipeResultDirection) {
     
        self.stopTimerHoldCard()
        
        self.timerHoldCard = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { timer in
            self.isHolding = true
            if direction == .left || direction == .bottomLeft || direction == .topLeft{
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_DISLIKE_DEAL, object: card)
                self.updateHeaderView(currentStatus: .DISLIKE, pendingStatus: self.statusPending, isHideUndo: true)
            } else {
                NotificationCenter.default.post(name: .ON_CHANGE_MASK_LIKE_DEAL, object: card)
                self.updateHeaderView(currentStatus: .LIKE, pendingStatus: self.statusPending, isHideUndo: true)
            }
        }
    }
    
    private func submitDataIfHave() {
        if let dataPending = self.dealPending {
            
            if statusPending == .REMOVE {
                self.onRemoveCardCallback?(dataPending)
            } else if statusPending == .CHECK {
                self.onCheckCardCallback?(dataPending)
            } else if statusPending == .DISLIKE {
                self.onDislikeCardCallback?(dataPending)
            } else if statusPending == .LIKE {
                self.onLikeCardCallback?(dataPending)
            }
            
        }
        
        self.timerForDisplayToast(statusPending)
//        if self.deals.count == 0 {
//            self.onRunOutOfCardsCallback?(nil)
//        }
        
        self.dealPending = nil
        self.statusPending = .NONE
    }
    
    private func reinitView(_ idx: Int) {
        self.timerForUndo()
        
        
        self.draggableCardViews.removeAll()
        
        self.dealPending = self.dealsForShow[idx]
        
        self.dealsForShow.remove(at: idx)
        self.clearAtIndex(idx)
        
        self.addMoreDealForShowIfHave()
        
        self.reloadData(forceAnimation: false)
    }
}

extension VerticalSwipeView: KolodaViewDataSource {
    
    public func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return dealsForShow.count
    }
    
    public func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    public func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        if index < 0 || index >= self.dealsForShow.count {
            return UIView()
        }
        
        var data = dealsForShow[index]
        data["index"] = index
        
        let swipeDeal = SwipeDeal().loadView()
        swipeDeal.defaultAngle = self.getAngleFromIndex(index)
        swipeDeal.config(data) { (info) in
            
        }
        
        return swipeDeal
    }
    
    private func getAngleFromIndex(_ index: Int) -> CGFloat {
        var angle: CGFloat = 0
        if index == 0 {
            angle = 0
        } else if index%2 == 1 {
            angle = -0.10
        } else if index%2 == 0 {
            angle = 0.05
        }
        
        return angle
    }

}
