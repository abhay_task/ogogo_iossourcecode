//
//  ExampleOverlayView.swift
//  KolodaView
//
//  Created by Eugene Andreyev on 6/21/15.
//  Copyright (c) 2015 CocoaPods. All rights reserved.
//

import UIKit

@objc public enum PanDirection: Int {
    case Up
    case Down
    case Left
    case Right
    case None
    
    /// Returns true is the PanDirection is horizontal.
    public var isX: Bool { return self == .Left || self == .Right  }
    /// Returns true if the PanDirection is vertical.
    public var isY: Bool { return self == .Up || self == .Down }
}


public enum MASK_DEAL_STATUS: Int {
    case REMOVE
    case CHECK
    case DISLIKE
    case LIKE
    case NONE
}

class SwipeDeal: UIView {
    var defaultAngle: CGFloat = 0.15 {
        didSet {
         
        }
    }
    
    private var maskStatus: MASK_DEAL_STATUS = .NONE
    private var previousMaskStatus: MASK_DEAL_STATUS = .NONE
    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    


    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var flipCardView: UIView!
    @IBOutlet weak var removeView: UIView!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var dislikeView: UIView!
    @IBOutlet weak var likeView: UIView!

    @IBOutlet weak var contentView: UIView!
    
    func loadView() -> SwipeDeal {
        let swipeDeal = Bundle.main.loadNibNamed("SwipeDeal", owner: self, options: nil)?[0] as! SwipeDeal
        
        return swipeDeal
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        self.rotateView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMaskRemove(_:)), name: .ON_CHANGE_MASK_REMOVE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showMaskCheck(_:)), name: .ON_CHANGE_MASK_CHECK_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showMaskDislike(_:)), name: .ON_CHANGE_MASK_DISLIKE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showMaskLike(_:)), name: .ON_CHANGE_MASK_LIKE_DEAL, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(resetMask(_:)), name: .ON_RESET_MASK_DEAL, object: nil)
        
        decorate()
    }

    private func decorate() {
        self.removeView.layer.cornerRadius = 25
        self.removeView.layer.masksToBounds = true
        
        self.checkView.layer.cornerRadius = 25
        self.checkView.layer.masksToBounds = true
        
        self.dislikeView.layer.cornerRadius = 25
        self.dislikeView.layer.masksToBounds = true
        
        self.likeView.layer.cornerRadius = 25
        self.likeView.layer.masksToBounds = true
        
        self.overlayView.layer.cornerRadius = 25
        self.overlayView.layer.masksToBounds = true
    }
    

    private func getLatestMask() -> UIView? {
        var view: UIView? = nil
        if previousMaskStatus == .REMOVE {
            view = self.removeView
        } else if previousMaskStatus == .CHECK {
            view = self.checkView
        } else if previousMaskStatus == .DISLIKE {
            view = self.dislikeView
        } else if previousMaskStatus == .LIKE {
            view = self.likeView
        }
        
        return view
    }
    
    private func showMask() {
        let latestView = self.getLatestMask()
        
        if let preView = latestView {
            if preView.alpha == 1 {
                UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions() , animations: {
                    preView.alpha = 0
                    
                }, completion: { (finished: Bool) in
                    preView.alpha = 0
                })
            }
        }
        

        if self.maskStatus == .REMOVE {

            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.removeView.alpha = 1
                
            }, completion: { (finished: Bool) in
                self.removeView.alpha = 1
            })
            
        } else if self.maskStatus == .CHECK {
        
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.checkView.alpha = 1
                
            }, completion: { (finished: Bool) in
                self.checkView.alpha = 1
            })
        } else if self.maskStatus == .DISLIKE {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.dislikeView.alpha = 1
                
            }, completion: { (finished: Bool) in
                self.dislikeView.alpha = 1
            })
        } else if self.maskStatus == .LIKE {
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.likeView.alpha = 1
                
            }, completion: { (finished: Bool) in
                self.likeView.alpha = 1
            })
        }
    }
    
    private func hideMask() {
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
            self.checkView.alpha = 0
            self.removeView.alpha = 0
            
        }, completion: { (finished: Bool) in
            self.checkView.alpha = 0
            self.removeView.alpha = 0
        })
    }
    

    @objc func showMaskRemove(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID && self.maskStatus != .REMOVE {
                
                self.previousMaskStatus = self.maskStatus
                self.maskStatus = .REMOVE
                self.showMask()
            }
        }
    }
    
    
    @objc func showMaskCheck(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID && self.maskStatus != .CHECK {
                self.previousMaskStatus = self.maskStatus
                self.maskStatus = .CHECK
                self.showMask()
            }
        }
    }
    
    
    @objc func showMaskDislike(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID && self.maskStatus != .DISLIKE {
                self.previousMaskStatus = self.maskStatus
                self.maskStatus = .DISLIKE
                self.showMask()
            }
        }
    }
    
    
    @objc func showMaskLike(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID && self.maskStatus != .LIKE {
                self.previousMaskStatus = self.maskStatus
                self.maskStatus = .LIKE
                self.showMask()
            }
        }
    }
    
    @objc func resetMask(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID && self.maskStatus != .NONE {
                self.maskStatus = .NONE
                
                self.hideMask()
            }
        }
    }
    
    public func rotateView() {
        if defaultAngle != 0 {
            var transform = CATransform3DIdentity

            transform = CATransform3DRotate(transform, defaultAngle, 0, 0, 1)
            transform = CATransform3DTranslate(transform, 0, 0, 1)
            
            self.contentView.layer.transform = transform
            
            self.contentView.layer.shouldRasterize = true
        }

    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
    
    private func fillData() {
        
        if let tData = self.data  {
            let currentIdx = tData["index"] as? Int ?? 0
            
            self.overlayView.isHidden = (currentIdx == 0) ? true : false
            
            
            let flipView = FlipDealView().loadView()
            
            flipView.config(tData, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                
            })
            
            flipView.frame = CGRect(x: 0, y: 0, width: 310, height: 480)
            flipView.clipsToBounds = false
            print(self.flipCardView.bounds.width,self.flipCardView.bounds.height,"🥵")
            self.flipCardView.clipsToBounds = false
            self.flipCardView.addSubview(flipView)

        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_RESET_MASK_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_CHANGE_MASK_REMOVE_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_CHANGE_MASK_CHECK_DEAL, object: nil)
    }

}
