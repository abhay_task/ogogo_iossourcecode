//
//  DurrationCollectionViewCell.swift
//  TGCMember
//
//  Created by vang on 5/3/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class DurrationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var lblText: UILabel!
    private var data: [String: Any]?
    private var onClickedCellCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    private func updateColor(_ selected: Bool = false) {
        self.lineView.backgroundColor = selected ? UIColor(255, 193, 6) : UIColor.clear
        self.lblText.textColor = selected ? UIColor(255, 193, 6) : UIColor(142, 142, 147)
    }
    
    public func config(_ data: [String: Any]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        if let tData = self.data, let duration = tData["duration"] as? String {
            self.lblText.text = duration
        }
        
        if let tData = self.data, let selected = tData["selected"] as? String, let duration = tData["duration"] as? String  {
            updateColor(selected == duration)
        } else {
            updateColor(false)
        }
        
    }
}
