//
//  NormalEventCell.swift
//  TGCMember
//
//  Created by vang on 5/3/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NormalEventCell: UICollectionViewCell {

    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBOutlet weak var dateLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCell: UIView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var widthImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImageConstraint: NSLayoutConstraint!
    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverCell.layer.cornerRadius = 3.0
        coverCell.layer.masksToBounds = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadView() -> NormalEventCell {
        let normalEventCell = Bundle.main.loadNibNamed("NormalEventCell", owner: self, options: nil)?[0] as! NormalEventCell
        
        return normalEventCell
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        if let tData = self.data {
            self.lblEventName.text = tData["Name"] as? String
            self.lblEventDate.text = tData["CurrentTime"] as? String
            
            
            
            widthImageConstraint.constant = self.bounds.width
            
            var imageUrl = tData["ImageUrl"] as? String ?? ""
            if let index = tData["index"] as? Int {
                if (index == 0) {
                    imageUrl = tData["HeaderImageUrl"] as? String ?? ""
                    heightImageConstraint.constant = self.bounds.width * (150 / 325)
                    self.dateLeadingConstraint.constant = 15
                } else {
                    self.dateLeadingConstraint.constant = 8
                    heightImageConstraint.constant = self.bounds.width * (99/158)
                }
            } else {
                self.dateLeadingConstraint.constant = 8
                heightImageConstraint.constant = self.bounds.width * (99/158)
            }
            
            
            self.loadingImage.startAnimating()
            self.eventImageView.sd_setImage(with:  URL(string: imageUrl), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
        }
        
    }
}
