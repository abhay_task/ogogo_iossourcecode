//
//  TabControlCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabControlCell: UICollectionViewCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var numberView: UIView!
    
    private var data: [String: Any]?
    private var onClickedCellCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onClickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    private func updateColor(_ selected: Bool = false) {
        self.lineView.backgroundColor = selected ? UIColor(255, 193, 6) : UIColor.clear
        self.lblTitle.textColor = selected ? UIColor(255, 193, 6) : UIColor(142, 142, 147)
    }
    
    private func removeConstraintItem(_ item: UIView?) {
        if let view = item {
             view.snp.removeConstraints()
        }
       
    }
    
    private func decorate(showNumber: Bool = false) {
        if showNumber {
            self.numberView.isHidden = false
            removeConstraintItem(self.lblTitle)
            removeConstraintItem(self.numberView)
        
            if self.numberView != nil {
                self.numberView.snp.makeConstraints { (make) in
                    make.bottom.equalTo(self.lblTitle.snp.bottom).offset(-3)
                    make.right.equalTo(self.coverContent)
                }
            }
            
            
            if self.lblTitle != nil {
                self.lblTitle.snp.makeConstraints { (make) in
                    make.left.equalTo(self.coverContent)
                    make.top.equalTo(self.coverContent).offset(5)
                    make.bottom.equalTo(self.coverContent).offset(-5)
                    make.right.equalTo(self.numberView.snp.left).offset(-2)
                }
            }
            
        } else {
            self.numberView.isHidden = true
            
            removeConstraintItem(self.lblTitle)
            
            if self.lblTitle != nil {
                self.lblTitle.snp.makeConstraints { (make) in
                    make.left.equalTo(self.coverContent)
                    make.top.equalTo(self.coverContent).offset(5)
                    make.bottom.equalTo(self.coverContent).offset(-5)
                    make.right.equalTo(self.coverContent)
                }
            }

        }
    }
    
    
    public func config(_ data: [String: Any]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        if let tData = self.data {
            
            let title = tData["title"] as? String
            self.lblTitle.text = title
            
            if let tNumber = tData["number"] as? String {
                self.lblNumber.text = tNumber
                
                self.decorate(showNumber: true)
            } else {
                self.decorate(showNumber: false)
            }
        }
        
        
        if let tData = self.data  {
            let selectedIndex = tData["selectedIndex"] as! Int
            let tag = tData["tag"] as! Int
            
            self.updateColor(tag == selectedIndex)
        } else {
            self.updateColor(false)
        }
        
    }
}
