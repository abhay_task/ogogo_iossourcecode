//
//  TabEventCell.swift
//  TGCMember
//
//  Created by vang on 5/6/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabEventCell: UITableViewCell {

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var thumbnail: UIImageView!
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBOutlet weak var lblEventDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    private var data: [String: Any?]?
    
    private var onClickedCell: DataCallback?
    private var onClickedMore: DataCallback?
    
    @IBOutlet weak var imageFavorite: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateBookmark(_:)), name: .TOOGLE_BOOKMARK_EVENT, object: nil)
        
        coverView.layer.cornerRadius = 3.0
        coverView.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedMore(_ sender: Any) {
        if let callback = self.onClickedMore {
            callback(self.data)
        }
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        if let callback = self.onClickedCell {
            callback(self.data)
        }
    }
    
    @objc func updateBookmark(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let bookmark = uData["Bookmark"] as? Bool ?? false
                tData["IsBookmark"] = bookmark
                
                self.data = tData
                
                self.fillData()
            }
        }
    }
    
    private func fillData() {
        if let tData = self.data {
            //mPrint("event => ", tData)
            self.lblName.text = tData["Name"] as? String
            self.lblEventDate.text = tData["CurrentTime"] as? String
            
            self.loadingImage.startAnimating()
            self.thumbnail.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            let isBookmark = tData["IsBookmark"] as? Bool ?? false
            self.imageFavorite.isHidden = !isBookmark
            
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCell _onClickedCell: DataCallback?, onClickedMore _onClickedMore: DataCallback?) -> Void {
        self.data = data
        self.onClickedCell = _onClickedCell
        self.onClickedMore = _onClickedMore
        
        self.fillData()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_BOOKMARK_EVENT, object: nil)
    }
    
}
