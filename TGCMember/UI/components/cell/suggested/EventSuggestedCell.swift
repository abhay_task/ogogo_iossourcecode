//
//  EventCollectionViewCell.swift
//  TGCMember
//
//  Created by vang on 4/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class EventSuggestedCell: UICollectionViewCell {
    
    @IBOutlet weak var coverVendorName: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var lblEventTime: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var imageStatus: UIImageView!
    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    @IBAction func onClickedCell(_ sender: Any) {
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(updateBookmark(_:)), name: .TOOGLE_BOOKMARK_EVENT, object: nil)
       
        
        self.layer.cornerRadius = 3.0
        self.layer.masksToBounds = true
        
    }
    
    
    @objc func updateBookmark(_ notification: Notification) {

        if let uData = notification.object as? [String: Any], var tData = self.data {

            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let bookmark = uData["Bookmark"] as? Bool ?? false
                tData["IsBookmark"] = bookmark
                
                self.data = tData
                
                self.fillData()
            }
        }
    }
    
    
    private func fillData() {
        if let data = self.data {
            lblEventTime.text = data["CurrentTime"] as? String
            lblEventName.text = data["Name"] as? String
            
            self.loadingImage.startAnimating()
            self.thumbnail.sd_setImage(with:  URL(string: data["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            if let summary =  data["VendorSummary"] as? [String: Any?], let tVendorName = summary["Name"] as? String {
                coverVendorName.isHidden = false
                lblVendorName.text = tVendorName
            } else {
                coverVendorName.isHidden = true
            }

            let isBookmark = data["IsBookmark"] as? Bool ?? false
            imageStatus.isHidden = !isBookmark
    
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self, name: .TOOGLE_BOOKMARK_EVENT, object: nil)
    }
    
}
