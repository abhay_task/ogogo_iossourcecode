//
//  TEOCell.swift
//  TGCMember
//
//  Created by Vang Doan on 9/5/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TEOCell: BaseExpandCell {

    @IBOutlet weak var widthProgressConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTimeAgo: UILabel!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var cardView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func fillData() {
        if let tData = self.data, let dealData = tData["DealTellEveryone"] as? [String: Any?] {
            self.lblTimeAgo.text = tData["CreatedAgo"] as? String ?? ""
            self.lblPrompt.text = tData["TitleToDo"] as? String ?? ""
            
            self.updateProgress()
            
            let flipView = FlipDealView().loadView()
            
            flipView.config(dealData, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.onClickedCardCallback?(self.data)
                    
            })
            
            flipView.frame = self.cardView.bounds
            self.cardView.addSubview(flipView)
            
        }
    }
    
    private func updateProgress() {
        if let tData = self.data {
            let total = tData["TotalOfRequiredAssignee"] as? Int ?? 1
            let current = tData["NumberOfAssigneeCompleted"] as? Int ?? 0
            
            let totalWidth = SCREEN_WIDTH - 50
            
            let progressWidth: CGFloat = CGFloat(current) / CGFloat(total) * totalWidth
            self.widthProgressConstraint.constant = progressWidth
            
        }
    
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
}
