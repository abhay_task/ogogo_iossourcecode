//
//  InviteFriendsCell.swift
//  TGCMember
//
//  Created by vang on 5/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class InviteFriendsCell: UITableViewCell {

    @IBOutlet weak var coverBodyView: UIView!
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        coverBodyView.layer.borderColor = UIColor(227, 227, 227).cgColor
        coverBodyView.layer.borderWidth = 1.0
        coverBodyView.layer.cornerRadius = 3.0
        coverBodyView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback

    }
}
