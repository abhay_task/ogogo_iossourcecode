//
//  WellcomeDealCell.swift
//  TGCMember
//
//  Created by vang on 5/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class WelcomeDealCell: BaseExpandCell {

    @IBOutlet weak var paddingBottomSwipe: NSLayoutConstraint!
    @IBOutlet weak var paddingTopSwipe: NSLayoutConstraint!
    @IBOutlet weak var verticalSwipeView: VerticalSwipeView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imageAction: UIImageView!

    
    var deals: [[String: Any?]] = [["ID": "1", "ImageUrl" : "https://api.gcc.dev2.gkxim.com/data/uploads/deal/thumbnail/Cocktail.png?ver=20190620031909", "EndAt": "2019-06-07T05:15:21.789195Z", "SubscriptionKey": "gold", "SubscriptionName": "Gold", "Name": "50% discount to all coctails or signature drinks"],
                                   ["ID": "2", "EndAt": "2019-06-07T05:15:21.789195Z", "SubscriptionKey": "silver", "Name": "50% discount to all coctails or signature drinks", "ImageUrl" : "https://api.gcc.dev2.gkxim.com/data/uploads/deal/thumbnail/Cocktail.png?ver=20190620031909"],
                                   ["ID": "3", "EndAt": "2019-06-07T05:15:21.789195Z", "SubscriptionKey": "black", "Name": "50% discount to all coctails or signature drinks", "ImageUrl" : "https://api.gcc.dev2.gkxim.com/data/uploads/deal/thumbnail/Cocktail.png?ver=20190620031909"]]
    
//     var deals: [[String: Any?]] = [["ID": "1", "ImageUrl" : "https://api.gcc.dev2.gkxim.com/data/uploads/deal/thumbnail/Cocktail.png?ver=20190620031909", "EndAt": "2019-06-07T05:15:21.789195Z", "SubscriptionKey": "gold", "SubscriptionName": "Gold", "Name": "50% discount to all coctails or signature drinks"]]

    
    override var completedExpandCallback: FinishedCallback {
        let openCallback = { ()-> Void in
            self.verticalSwipeView.startAnimation()
        }
        
        return openCallback
    }
    
    override var completedCollpasedCallback: FinishedCallback {
        let closeCallback = { ()-> Void in
            self.verticalSwipeView.stopAnimation()
        }
        
        return closeCallback
    }
    
 
    
    override func fillData() {
        self.verticalSwipeView.paddingTopItem = 15
        self.verticalSwipeView.paddingBottomItem = 15
        
        let tDict = ["deals": self.deals]
        
        self.verticalSwipeView.config(tDict, onRemoveCardCallback: { [weak self] (cardData) in
            guard let self = self else {return}
                mPrint("call api  REMOVE", cardData)
            
            }, onCheckCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  CHECK", cardData)
                
            }, onDislikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  DISLIKE", cardData)
                
            }, onLikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  LIKE", cardData)
                
            }, onRunOutOfCardsCallback: { [weak self] (data) in
                guard let self = self else {return}
                

                
            }, onReadyCallback: { [weak self] (data) in
                guard let self = self else {return}
                //self.verticalSwipeView.startAnimation()
        })
        
    }
    
}
