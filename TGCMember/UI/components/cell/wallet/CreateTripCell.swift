//
//  CreateTripCell.swift
//  TGCMember
//
//  Created by vang on 10/14/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CreateTripCell: UITableViewCell {
    private var data: [String: Any?]?
    private var onClickedAddTrip: DataCallback?
    private var onClickedCancel: DataCallback?
    
    var subContent: CreateTripView?
    var parent: UIViewController?
    
    @IBOutlet weak var coverView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.loadSubViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func decorate() {
        
    }
    
    public func reset() {
        self.subContent?.reset()
    }
    
    private func loadSubViews() {
        self.subContent = CreateTripView().loadView()
        self.subContent?.parent = self.parent
        if let createTripView = self.subContent {
             self.coverView.addSubview(createTripView)
             
             createTripView.snp.makeConstraints { (make) in
                 make.left.equalToSuperview()
                 make.top.equalToSuperview()
                 make.right.equalToSuperview()
                 make.bottom.equalToSuperview()
             }
             
             createTripView.config(nil, onClickedCancelCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.resetFields()
                self.onClickedCancel?(data)
                
            }, onClickedAddTripCallback: { [weak self] (data) in
                 guard let self = self else {return}
                 self.onClickedAddTrip?(data)
            })
        }
        
    }
    
    private func resetFields() {
        if let createTripView = self.subContent {
            createTripView.reset()
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedAddTrip _onClickedAddTrip: DataCallback? = nil, onClickedCancel _onClickedCancel: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedAddTrip = _onClickedAddTrip
        self.onClickedCancel = _onClickedCancel
        
        self.resetFields()
    }
}
