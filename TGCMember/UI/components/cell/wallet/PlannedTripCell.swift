//
//  PlannedTripCell.swift
//  TGCMember
//
//  Created by vang on 10/14/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class PlannedTripCell: UITableViewCell {

    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    private var onClickedMoreCallback: DataCallback?
    
    @IBOutlet weak var lblNumEvent: UILabel!
    @IBOutlet weak var lblNumOffer: UILabel!
    @IBOutlet weak var lblNumDeal: UILabel!
    @IBOutlet weak var lblTripDate: UILabel!
    @IBOutlet weak var lblTripName: UILabel!
    @IBOutlet weak var lineImg: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        decorate()
    }
    
    private func decorate() {
        
    }
    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    @IBAction func onClickedMore(_ sender: Any) {
        self.onClickedMoreCallback?(self.data)
    }
    
    private func fillData() {
        if let tData = self.data {
            let currentIndex = tData["currentIndex"] as? Int ?? 0
            let numItem = tData["numItems"] as? Int ?? 1
            self.lineImg.isHidden = false
            
            if currentIndex == 0 {
                self.bgImage.image = UIImage(named: "top_tem_img")
            } else if currentIndex < numItem - 1 {
                self.bgImage.image = UIImage(named: "null_tem_img")
            } else {
               self.bgImage.image = UIImage(named: "bottom_tem_img")
               self.lineImg.isHidden = true
            }
            
            let strDate = tData["FromDateAt"] as? String ?? ""
            
            self.lblTripName.text = tData["Name"] as? String ?? ""
            self.lblTripDate.text = "\(tData["CountryName"] as? String ?? ""), \(Utils.stringFromStringDate(strDate, toFormat: "MMM dd, yyyy"))"
            
            self.lblNumDeal.text = "\(10) \(LOCALIZED.txt_deals_title.translate)"
            self.lblNumOffer.text = "\(3) \(LOCALIZED.txt_offers_title.translate)"
            self.lblNumEvent.text = "\(5) \(LOCALIZED.txt_events_title.translate)"
            
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil, onClickedMoreCallback _onClickedMoreCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        self.onClickedMoreCallback = _onClickedMoreCallback
        
        self.fillData()
    }
}
