//
//  AnswerCell.swift
//  TGCMember
//
//  Created by vang on 10/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CountryTripCell: UITableViewCell {

    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var checkImg: UIImageView!

    override func awakeFromNib() {
        
    super.awakeFromNib()
    // Initialization code

        decorate()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCheck(_:)), name: .SELECTED_COUNTRY_IN_TRIP, object: nil)
               
    }
    
    @objc func updateCheck(_ notification: Notification) {
         if let uData = notification.object as? [String: Any], let tData = self.data {
            
            let tCountryCode = tData["countryCode"] as? Int ?? 0
            let tCountryName = tData["countryName"] as? String ?? ""
            
            let uCountryCode = uData["countryCode"] as? Int ?? 0
            let uCountryName = uData["countryName"] as? String ?? ""
            
            if tCountryCode == uCountryCode && tCountryName == uCountryName {
                self.checkImg.isHidden = false
            } else {
                self.checkImg.isHidden = true
            }
        }
       
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .SELECTED_COUNTRY_IN_TRIP, object: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    private func decorate() {

    }
   
    @IBAction func onClickedCell(_ sender: Any) {
        NotificationCenter.default.post(name: .SELECTED_COUNTRY_IN_TRIP, object: self.data)
        self.onClickedCellCallback?(self.data)
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblCountry.text = tData["countryName"] as? String ?? ""
            
            let _currentCountryCode = tData["_currentCountryCode"] as? Int ?? 0
            let _currentCountryName = tData["_currentCountryName"] as? String ?? ""
            
            if _currentCountryCode == tData["countryCode"] as? Int ?? 0  && _currentCountryName == tData["countryName"] as? String ?? "" {
                self.checkImg.isHidden = false
            } else {
                self.checkImg.isHidden = true
            }

        }
    }
   
   public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil) -> Void {
       self.data = data
       self.onClickedCellCallback = _onClickedCellCallback
      
       self.fillData()
   }
}
