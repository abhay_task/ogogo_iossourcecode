//
//  CategoryCell.swift
//  TGCMember
//
//  Created by vang on 11/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CategoryWideCell: UITableViewCell {
    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var coverContent: UIView!
    
    @IBOutlet weak var thumbnailCate: UIImageView!
    @IBOutlet weak var cateIcon: UIImageView!
    @IBOutlet weak var lblCateName: UILabel!
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        decorate()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    private func fillData() {
        if let tData = self.data  {

            self.loadingImage.startAnimating()
            self.cateIcon.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            self.lblCateName.text = tData["Name"] as? String ?? ""
            
        }
    }
    
    private func decorate() {
        coverContent.layer.cornerRadius = 8.0
        coverContent.layer.masksToBounds = true
        
        self.cateIcon.contentMode = .scaleAspectFit
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
}
