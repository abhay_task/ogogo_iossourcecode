//
//  CountryCell.swift
//  TGCMember
//
//  Created by vang on 6/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var lblFlag: UILabel!
    
    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        
        if let callback = self.onClickedCellCallback {
            callback(self.data)
        }
    }
    
    private func fillData() {
        if let tData = self.data {
            
            let tCountry = CountryModel(tData)
            let selectedCountryCode = tData["selectedCountryCode"] as? String
            
            if selectedCountryCode == tCountry.countryCode {
                self.imageCheck.isHidden = false
            } else {
                self.imageCheck.isHidden = true
            }
            
            self.countryName.text = tCountry.name
            self.lblFlag.text = tCountry.flag
            self.countryCode.text = tCountry.phoneCode
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback?) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
}
