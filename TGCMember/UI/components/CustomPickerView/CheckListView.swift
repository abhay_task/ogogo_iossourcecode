//
//  CheckListView.swift
//  TGCMember
//
//  Created by jonas on 12/16/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class CheckListView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var listStackView: UIStackView!
    @IBOutlet weak var titleTextLabel: UILabel!
    
    var onChangeSelectedIndices: (([Int]) -> Void)?
    private var selectedIndices = [Int]() {
        didSet {
            onChangeSelectedIndices?(selectedIndices)
        }
    }
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(CheckListView.className(), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func show(title: String? = nil, items: [Wallet.Category], selectedIndices: [Int]) {
        titleTextLabel.text = title
        listStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        
        self.selectedIndices = selectedIndices
        items.enumerated().forEach({
            let itemView = CheckListItemView.loadFromNib()
            itemView.tag = $0
            itemView.nameLabel.text = $1.name
            itemView.setSelected(bool: selectedIndices.contains($0))
            itemView.onSelect = { [unowned self] int in
                self.selectedIndices.append(int)
            }
            
            itemView.onUnselect = { [unowned self] int in
                self.selectedIndices.removeAll(where: { $0 == int })
            }
            
            listStackView.addArrangedSubview(itemView)
        })
        getKeyWindow().addSubview(self)
        parentView.slideUpPresent()
    }
    
    func show(items: [String], selectedIndices: [Int]) {
        listStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        
        self.selectedIndices = selectedIndices
        items.enumerated().forEach({
            let itemView = CheckListItemView.loadFromNib()
            itemView.tag = $0
            itemView.nameLabel.text = $1
            itemView.setSelected(bool: selectedIndices.contains($0))
            itemView.onSelect = { [unowned self] int in
                self.selectedIndices.append(int)
            }
            
            itemView.onUnselect = { [unowned self] int in
                self.selectedIndices.removeAll(where: { $0 == int })
            }
            
            listStackView.addArrangedSubview(itemView)
        })
        getKeyWindow().addSubview(self)
        parentView.slideUpPresent()
    }
    
    func dismiss() {
        parentView.slideDownDismiss(duration: 0.4, { [unowned self] in
            removeFromSuperview()
        })
    }
    
    @IBAction func didTapOkButton(_ sender: Any) {
        dismiss()
    }
}
