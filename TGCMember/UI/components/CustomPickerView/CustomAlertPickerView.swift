//
//  CustomAlertPickerView.swift
//  TGCMember
//
//  Created by jonas on 12/23/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class CustomAlertPickerView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: CustomAlertPickerViewCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: CustomAlertPickerViewCell.className())
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
            tableView.separatorStyle = .none
            tableView.contentInset = UIEdgeInsets(top: tableView.frame.height * 0.4,
                                                  left: 0,
                                                  bottom: tableView.frame.height * 0.4,
                                                  right: 0)
            tableView.showsVerticalScrollIndicator = false
        }
    }
    @IBOutlet weak var parentView: CustomView!
    
    private var elements = [String]()
    var onSelectRow: ((Int) -> Void)?
    private var selectedRow: Int = 0
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(CustomAlertPickerView.className(), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func show(_ elements: [String], selectedRow: Int) {
        getKeyWindow().addSubview(self)
        parentView.zoomInAnimate()
        
        self.elements = elements
        
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.selectRow(at: IndexPath(row: selectedRow, section: 0),
                            animated: false,
                            scrollPosition: .middle)
    }
    
    func dismiss() {
        parentView.zoomOutAnimate(completion: { [weak self] in
            
            self?.removeFromSuperview()
        })
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        onSelectRow?(selectedRow)
        dismiss()
    }
}

extension CustomAlertPickerView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomAlertPickerViewCell.className(), for: indexPath) as! CustomAlertPickerViewCell
        cell.itemLabel.text = elements[safe: indexPath.row] ?? ""
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let centerIndexPath = tableView.indexPathForRow(at: tableView.bounds.center) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 0.9)) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 1.1)) {
            tableView.scrollToRow(at: centerIndexPath, at: .middle, animated: true)
            selectedRow = centerIndexPath.row
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else { return }
        
        if let centerIndexPath = tableView.indexPathForRow(at: tableView.bounds.center) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 0.9)) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 1.1)) {
            tableView.scrollToRow(at: centerIndexPath, at: .middle, animated: true)
            selectedRow = centerIndexPath.row
        }
    }
}
