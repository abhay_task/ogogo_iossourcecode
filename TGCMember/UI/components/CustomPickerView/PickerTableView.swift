//
//  PickerTableView.swift
//  TGCMember
//
//  Created by jonas on 12/15/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

enum PickerType {
    case gender([KeyPair<String>])
    case birthYear([Int])
    case marriageStatus([KeyPair<String>])
    case educationLevel([KeyPair<String>])
    case generic([String])
}

class PickerTableView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: PickerTableViewCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: PickerTableViewCell.className())
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView()
            tableView.separatorStyle = .none
            let multiplier: CGFloat = Device.iPhoneWithNotch ? 0.47 : 0.3
            tableView.contentInset = UIEdgeInsets(top: tableView.frame.height * multiplier,
                                                  left: 0,
                                                  bottom: tableView.frame.height * multiplier,
                                                  right: 0)
            tableView.showsVerticalScrollIndicator = false
        }
    }
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titleTextLabel: UILabel!
    
    private var elements = [String]()
    var onSelectRow: ((Int) -> Void)?
    private var selectedRow: Int = 0 {
        didSet {
            onSelectRow?(selectedRow)
        }
    }
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(PickerTableView.className(), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func show(_ type: PickerType, title: String? = nil, selectedRow: Int) {
        titleTextLabel.text = title
        getKeyWindow().addSubview(self)
        parentView.slideUpPresent()
        self.selectedRow = selectedRow
        switch type {
        case .birthYear(let ints):
            elements = ints.map { "\($0)" }
        case .educationLevel(let levels):
            elements = levels.map { $0.value }
        case .gender(let types):
            elements = types.map { $0.value }
        case .marriageStatus(let types):
            elements = types.map { $0.value }
        case .generic(let items):
            elements = items
        }
        
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.selectRow(at: IndexPath(row: selectedRow, section: 0),
                            animated: false,
                            scrollPosition: .middle)
    }
    
    func dismiss() {
        parentView.slideDownDismiss(duration: 0.4, { [unowned self] in
            removeFromSuperview()
        })
    }
    
    @IBAction func didTapOkButton(_ sender: Any) {
        onSelectRow?(selectedRow)
        dismiss()
    }
}

extension PickerTableView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PickerTableViewCell.className(), for: indexPath) as! PickerTableViewCell
        cell.rowtitleLabel.text = elements[safe: indexPath.row] ?? ""
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let centerIndexPath = tableView.indexPathForRow(at: tableView.bounds.center) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 0.9)) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 1.1)) {
            tableView.scrollToRow(at: centerIndexPath, at: .middle, animated: true)
            selectedRow = centerIndexPath.row
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else { return }
        
        if let centerIndexPath = tableView.indexPathForRow(at: tableView.bounds.center) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 0.9)) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 1.1)) {
            tableView.scrollToRow(at: centerIndexPath, at: .middle, animated: true)
            selectedRow = centerIndexPath.row
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let centerIndexPath = tableView.indexPathForRow(at: tableView.bounds.center) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 0.9)) ?? tableView.indexPathForRow(at: CGPoint(x: tableView.bounds.center.x, y: tableView.bounds.center.y * 1.1)) {
            
            (0..<tableView.numberOfRows(inSection: 0)).forEach {
                let cell = tableView.cellForRow(at: IndexPath(row: $0, section: 0)) as? PickerTableViewCell
                cell?.rowtitleLabel.textColor = .gray
                cell?.rowtitleLabel.font = UIFont(name: Font.RobotoRegular, size: 14)
            }
            
            let cell = tableView.cellForRow(at: centerIndexPath) as? PickerTableViewCell
            cell?.rowtitleLabel.textColor = .black
            cell?.rowtitleLabel.font = UIFont(name: Font.RobotoBold, size: 14)
        }
    }
}
