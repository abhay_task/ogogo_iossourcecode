//
//  CheckListItemView.swift
//  TGCMember
//
//  Created by jonas on 12/16/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class CheckListItemView: UIView {
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    
    var onSelect: ((Int) -> Void)?
    var onUnselect: ((Int) -> Void)?
    
    private var selected: Bool = false {
        didSet {
            checkButton.setImage(selected ? #imageLiteral(resourceName: "select_checked") : #imageLiteral(resourceName: "unchecked"), for: .normal)
            nameLabel.font = UIFont(name: selected ? Font.RobotoMedium : Font.RobotoRegular, size: nameLabel.font.pointSize)
            selected ? onSelect?(tag) : onUnselect?(tag)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupActions()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
     
        setupActions()
    }
    
    private func setupActions() {
        let tappableView = subviews.first as? InteractiveView
        tappableView?.onTap = { [unowned self] _ in
            selected.toggle()
        }
    }
    
    func setSelected(bool: Bool) {
        selected = bool
    }
}
