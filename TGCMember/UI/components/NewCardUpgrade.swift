//
//  NewCardUpgrade.swift
//  TGCMember
//
//  Created by vang on 1/7/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class NewCardUpgrade: UIView {
    
    var data: [String: Any?]?
    
    var onClickedUpgradeCallback: DataCallback?
    
    @IBOutlet weak var benefitView: UIView!
    
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var lblNameUpgrade: UILabel!
    @IBOutlet weak var lblTitleBenefit: UILabel!
    @IBOutlet weak var lblSubTitleBenefit: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblUnix: UILabel!
    @IBOutlet weak var coverButton: HighlightableView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var currentPlanView: UIView!
    @IBOutlet weak var lblCurrentPlan: UILabel!
    @IBOutlet weak var lblUprade: UILabel!
    
    var currentIndex: Int = 0
    var currentPlanIndex: Int = 0
    var isCurrentPlan: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        Utils.dismissKeyboard()
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        Utils.dismissKeyboard()
        
        self.onClickedUpgradeCallback?(self.data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func decorate() {
        self.lblCurrentPlan?.text = LOCALIZED.current_plan.translate
        
        coverContent?.layer.cornerRadius = 25.0
        coverContent?.layer.applySketchShadow(color: UIColor("rgba 120 121 147 1"), alpha: 0.26, x: 3, y: 4, blur: 20, spread: 0)
        
        coverButton?.layer.cornerRadius = 3.0
        coverButton?.layer.applySketchShadow()
        
        reDecorate()
    }
    
    open func reDecorate() {}
    open func reFillData() {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        decorate()
    }
    
    @objc func onUpdateCard(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
        }
    }
    
    func fillData() {
        if let tData = self.data {
            lblNameUpgrade?.text = tData["Name"] as? String ?? ""
            lblTitleBenefit.text = LOCALIZED.benefits_of_tag_name.translate.replacingOccurrences(of: tagNameString, with: "\(tData["Name"] as? String ?? "")")
            
            lblPrice?.text = (tData["SaleAmount"] as? String) ?? ("\(tData["Currency"] as? String ?? "$")\(tData["Price"] as? Int ?? 0 )")
            lblUnix?.text = "/\(tData["SubscriptionUnix"] as? String ?? "")"
                        
            self.isCurrentPlan = tData["IsCurrentPlan"] as? Bool ?? false
        
            renderBenefitView(tData)
            
            
            let tAction = currentIndex < currentPlanIndex ? LOCALIZED.txt_downgrade_title.translate : LOCALIZED.txt_upgrade_title.translate
            let type = UPGRADE_PLAN(rawValue: (tData["Type"] as? String ?? "")) ?? .FREE
            
            self.lblUprade?.text = "\(tAction) \(LOCALIZED.txt_to_title.translate) \(type.txtName)"
            
            if type == .FREE {
                self.coverButton?.alpha = 1
                self.coverButton?.isUserInteractionEnabled = true
            } else {
                self.coverButton?.alpha = self.isCurrentPlan ? 0 : (currentPlanIndex == 0 ? 0.5 : 1)
                self.coverButton?.isUserInteractionEnabled = !(currentPlanIndex == 0)
            }
            
            if let text = tData["TextButtonUpgradeFree"] as? String {
                guard let tType = tData["Type"] as? String, let plan = UPGRADE_PLAN(rawValue: tType) else { return }
                if plan.viewIndex > currentPlanIndex {
                    lblUprade.text = text
                } else {
                    lblUprade.text = LOCALIZED.downgrade_to_tag_name.translate.replacingOccurrences(of: tagNameString, with: plan.txtName)
                }
            }
            
            self.currentPlanView?.alpha = self.isCurrentPlan ? 1 : 0
            
        }
        

        reFillData()
    }
    
    private func renderBenefitView(_ data: [String: Any?]) {
        
        self.benefitView?.backgroundColor = .clear
        self.benefitView?.removeSubviews()
        
        if let benefits = data["Description"] as? [[String: Any?]] {
            var fBenefits: [[String: Any?]] = []
             benefits.forEach { item in
                let enable = item["enable"] as? Bool ?? false
                
                if enable {
                    fBenefits.append(item)
                }
            }
    
            Utils.buildBenefitDescription(fBenefits) { [weak self] (info) in
                guard let self = self else {return}
                
                if let tInfo = info {
                    let contentView = tInfo["contentView"] as? UIView ?? UIView()
                    
                    self.benefitView.addSubview(contentView)
                    
                    contentView.snp.makeConstraints({ (make) in
                        make.top.equalTo(self.benefitView)
                        make.left.equalTo(self.benefitView)
                        make.right.equalTo(self.benefitView)
                        make.bottom.equalTo(self.benefitView)
                    })
                }
            }
        }
    }
    
    open func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onActiveSuccessCallback _onActiveSuccessCallback: SuccessedCallback? = nil) -> Void {
        
    }

    
}


class FreeUpgrade: NewCardUpgrade {
    @IBOutlet weak var lblPromptFree: UILabel!
    @IBOutlet weak var lblUpgradeBenefit: UILabel!
    @IBOutlet weak var readyToClaimLabel: UILabel!
    @IBOutlet weak var itsTimeToPurchaseLabel: UILabel!

    func loadView() -> FreeUpgrade {
        let aView = Bundle.main.loadNibNamed("NewCardUpgrade", owner: self, options: nil)?[0] as! FreeUpgrade
        
        return aView
    }
    
    override func reDecorate() {
        lblUpgradeBenefit?.text = LOCALIZED.upgrade_to_get_more_benefits.translate
        readyToClaimLabel.text = LOCALIZED.ready_to_claim_your_deals_discounts_and_offers.translate
        itsTimeToPurchaseLabel.text = LOCALIZED.it_is_time_to_purchase_an_ogogo_activation_card.translate
    }
    
    override func reFillData() {
        if let tData = self.data {
            lblPromptFree?.setTextWithHighlight(LOCALIZED.you_are_already_a_tag_member.translate.replacingOccurrences(of: "[%_TAG_FREE_%]", with: LOCALIZED.free.translate), UIColor("rgb 160 167 171"), UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), highlightText: [LOCALIZED.free.translate], highlightColor: [UIColor("rgb 100 117 127")], highlightFont: [UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 14)])
            
            lblTitleBenefit?.text = LOCALIZED.your_tag_name_allows_you_to.translate.replacingOccurrences(of: tagNameString, with: "\(tData["Name"] as? String ?? "")")
            
        }
    }
    
    override func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onActiveSuccessCallback _onActiveSuccessCallback: SuccessedCallback? = nil) -> Void {
    
        self.data = data
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
    
}

class BasicUpgrade: NewCardUpgrade {
    private var onActiveSuccessCallback: SuccessedCallback?
    
    @IBOutlet weak var coverScratchCode: UIView!
    @IBOutlet weak var tfScratchCode: UITextField!
    @IBOutlet weak var coverBtnActive: UIView!
    @IBOutlet weak var lblActive: UILabel!
    @IBOutlet weak var lblPromptScratch: UILabel!
    
    @IBOutlet weak var bottomView2: UIView!
    @IBOutlet weak var bottomCenterViewConstraint: NSLayoutConstraint!
    
    @IBAction func onClickedActive(_ sender: Any) {
        doActivate()
    }
    
    func loadView() -> BasicUpgrade {
        let aView = Bundle.main.loadNibNamed("NewCardUpgrade", owner: self, options: nil)?[1] as! BasicUpgrade
        
        return aView
    }
    
    private func dismissKeyboard() {
        tfScratchCode.resignFirstResponder()
    }
    
    private func doActivate() {
        self.dismissKeyboard()
        
        if !validateCode() {
            return
        }
        
        Utils.showLoading()
        
        let tCode = self.tfScratchCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        APICommonServices.activateCard(tCode) { (resp) in
            mPrint("activateCard", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                let tMessage = resp["message"] as? String ?? ""
                if status {
                    let data = resp["data"] as? [String: Any?]
                    let userInfo = data != nil ? data!["user"] as? [String: Any?] : nil
                    Utils.setUserInfo(userInfo)
                    
                    NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                    NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: nil)
                }
                
                self.showResultAlert(status, hasTitle: false, tMessage)
                
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func showResultAlert(_ success: Bool = true, hasTitle: Bool = true, _ errorMessage: String = "") {
        let content = NewResultAlertView().loadView()
    
        let tTitle = success ? LOCALIZED.congratulation.translate : LOCALIZED.error.translate
        
        var successMessage = LOCALIZED.you_ve_successfully_upgraded_to.translate
        var namPlan = ""
        
        if let tData = self.data {
            namPlan = tData["Name"] as? String ?? ""
        }
        
        successMessage = successMessage.replacingOccurrences(of: "[%__TAG_NAME__%]", with: namPlan)
                    
        var tData = [String: Any?]()
        tData["success"] = success
        tData["message"] = success ? successMessage : errorMessage
        tData["title"] = hasTitle ? tTitle : nil
        
        content.config(tData, onClickedActionCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            self.onActiveSuccessCallback?(success)

        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    private func validateCode(_ pText: String? = nil) -> Bool {
        let checkText = pText != nil ? pText : self.tfScratchCode.text
        
        var isValid = false
        
        if checkText != "" {
            if let text = checkText, text.count >= 4 {
                isValid = true
            }
        }
        
        self.coverBtnActive.backgroundColor = isValid ? UIColor("rgb 255 193 6") : UIColor("rgb 255 225 133")
        self.lblActive.textColor = isValid ? UIColor.white : UIColor("rgb 227 208 149")
        
        self.coverBtnActive.isUserInteractionEnabled = isValid
        
        
        if isValid {
            self.coverBtnActive.layer.applySketchShadow()
        } else {
            self.coverBtnActive.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    override func reDecorate() {
        lblPromptScratch?.text = LOCALIZED.input_scratch_code_on_OGOGO_activation_card.translate
        coverScratchCode.backgroundColor = UIColor("rgb 205 207 214")
        coverBtnActive?.layer.cornerRadius = 3.0
        coverScratchCode?.layer.cornerRadius = 3.0
        
        _ = validateCode()
    }
    
    override func reFillData() {
        if isCurrentPlan {
            self.bottomView2?.alpha = 1
            self.currentPlanView?.alpha = 1
            self.coverButton?.alpha = 0
            self.bottomView?.alpha = 0
            
            self.bottomCenterViewConstraint?.constant  = -80
        } else if currentIndex < self.currentPlanIndex {
            self.bottomView?.alpha = 0
            self.coverButton?.alpha = 1
            self.currentPlanView?.alpha = 0
            
            self.bottomCenterViewConstraint?.constant  = -80
        } else {
            self.bottomView?.alpha = 1
            self.bottomView2?.alpha = 0
            
            self.bottomCenterViewConstraint?.constant  = 0
        }
        
    }
    
    override func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onActiveSuccessCallback _onActiveSuccessCallback: SuccessedCallback? = nil) -> Void {
        self.data = data
        self.onActiveSuccessCallback = _onActiveSuccessCallback
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
}

class SilverUpgrade: NewCardUpgrade {

    
    func loadView() -> SilverUpgrade {
        let aView = Bundle.main.loadNibNamed("NewCardUpgrade", owner: self, options: nil)?[2] as! SilverUpgrade
        
        return aView
    }
    
    override func reDecorate() {
        
    }
    
    override func reFillData() {
        if let tData = self.data {
            self.lblSubTitleBenefit?.text = LOCALIZED.tag_name_is_defined_as_within_your_city_region.translate.replacingOccurrences(of: tagNameString, with: "\(tData["Name"] as? String ?? "")")
        }
    }
    
    override func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onActiveSuccessCallback _onActiveSuccessCallback: SuccessedCallback? = nil) -> Void {
        self.data = data
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
}

class GoldUpgrade: NewCardUpgrade {

    func loadView() -> GoldUpgrade {
        let aView = Bundle.main.loadNibNamed("NewCardUpgrade", owner: self, options: nil)?[3] as! GoldUpgrade
        
        return aView
    }
    
    override func reDecorate() {
        
    }
    
    override func reFillData() {
        if let tData = self.data {
            self.lblSubTitleBenefit?.text = LOCALIZED.tag_name_is_defined_as_anywhere_around_the_world.translate.replacingOccurrences(of: tagNameString, with: "\(tData["Name"] as? String ?? "")")
        }
    }
    
    override func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil, onActiveSuccessCallback _onActiveSuccessCallback: SuccessedCallback? = nil) -> Void {
        self.data = data
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
}


extension BasicUpgrade: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.doActivate()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateCode(updatedText)
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateCode()
    }
}
