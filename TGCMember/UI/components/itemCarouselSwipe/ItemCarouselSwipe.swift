//
//  ItemCarouselSwipe.swift
//  TGCMember
//
//  Created by vang on 10/30/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ItemCarouselSwipe: UIView {
    private var onRunOutOfCardsCallback: DataCallback?
    private var onRemoveCardCallback: DataCallback?
    private var onCheckCardCallback: DataCallback?
    private var onDislikeCardCallback: DataCallback?
    private var onLikeCardCallback: DataCallback?
    private var onNeedUpdateHeaderCallback: DataCallback?
    private var onSubmitDealIfHaveCallback: DataCallback?
    @IBOutlet weak var verticalSwipeView: OneDealSwipe!

    private var data: [String: Any?]?
       
    private func configSwipeView(_ cardData: [String: Any?]) {
        //self.verticalSwipeView.animationDelegate = self
        let tDict = ["deals": [cardData]]
        
        self.verticalSwipeView.config(tDict, onRemoveCardCallback: {  [weak self] (cardData) in
            guard let self = self else {return}
            
            self.onRemoveCardCallback?(cardData)
        }, onCheckCardCallback: {  [weak self] (cardData) in
            guard let self = self else {return}

             self.onCheckCardCallback?(cardData)
        }, onDislikeCardCallback: { [weak self] (cardData) in
            guard let self = self else {return}

            self.onDislikeCardCallback?(cardData)
        }, onLikeCardCallback: { [weak self] (cardData) in
            guard let self = self else {return}

            self.onLikeCardCallback?(cardData)
        }, onRunOutOfCardsCallback: { [weak self] (data) in
            guard let self = self else {return}
            
          //  self.dismissSelf()

        }, onReadyCallback: { [weak self] (data) in
            guard let self = self else {return}
           // self.verticalSwipeView.startAnimation()
        }, onNeedUpdateHeaderCallback: { [weak self] (data) in
            guard let self = self else {return}
            self.onNeedUpdateHeaderCallback?(data)
        }, onSubmitDealIfHaveCallback: { [weak self] (data) in
            guard let self = self else {return}
            self.onSubmitDealIfHaveCallback?(data)
        })
    }
    
    private func getAngleFromIndex(_ index: Int) -> CGFloat {
        var angle: CGFloat = 0
        if index == 0 {
            angle = 0
        } else if index == 1 {
            angle = -0.10
        } else if index == 2 {
            angle = 0.05
        }
        
        return angle
    }
    

    func loadView() -> ItemCarouselSwipe {
       let aView = Bundle.main.loadNibNamed("ItemCarouselSwipe", owner: self, options: nil)?[0] as! ItemCarouselSwipe
       
       return aView
    }

    private func fillData() {
        if let tData = self.data {
            self.configSwipeView(tData)
        }
    }

    
    
    public func config(_ data: [String: Any?]?, onRemoveCardCallback _onRemoveCardCallback: DataCallback?, onCheckCardCallback _onCheckCardCallback: DataCallback?, onDislikeCardCallback _onDislikeCardCallback: DataCallback?, onLikeCardCallback _onLikeCardCallback: DataCallback?, onNeedUpdateHeaderCallback _onNeedUpdateHeaderCallback: DataCallback? = nil, onSubmitDealIfHaveCallback _onSubmitDealIfHaveCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onRemoveCardCallback = _onRemoveCardCallback
        self.onCheckCardCallback = _onCheckCardCallback
        self.onDislikeCardCallback = _onDislikeCardCallback
        self.onLikeCardCallback = _onLikeCardCallback
        self.onNeedUpdateHeaderCallback = _onNeedUpdateHeaderCallback
        self.onSubmitDealIfHaveCallback = _onSubmitDealIfHaveCallback
        
        self.fillData()
    }
}
