//
//  TEOAlertView.swift
//  TGCMember
//
//  Created by vang on 8/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TEOAlertView: UIView {
    
    private var data: [String: Any?]?
    private var onClickedCancelCallback: DataCallback?
    private var onClickedAcceptCallback: DataCallback?
   
    @IBOutlet weak var coverTEOCard: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var teoCardImage: UIImageView!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDontWant: UIButton!
    
    private var dealInfo: [String: Any?]?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    private func decorate() {
        contentView?.layer.cornerRadius = 3.0
        contentView?.layer.masksToBounds = true
        
        self.btnAccept?.setTitle(LOCALIZED.sure_i_accept_take_me_to_the_process.translate, for: .normal)
        self.btnAccept?.titleLabel?.textAlignment = .center
        self.btnAccept?.layer.cornerRadius = 3.0
        self.btnAccept?.layer.applySketchShadow()
        
        self.btnDontWant?.setTitleUnderline(LOCALIZED.no_i_dont_want.translate, color: UIColor(228, 109, 87), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 16))
        
        self.lblTitle.text = LOCALIZED.you_are_assigned.translate
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.onClickedCancelCallback?(self.dealInfo)
    }
    
    @IBAction func onClickedAccept(_ sender: Any) {
        self.onClickedAcceptCallback?(self.dealInfo)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> TEOAlertView {
        let aView = Bundle.main.loadNibNamed("TEOAlertView", owner: self, options: nil)?[0] as! TEOAlertView
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.dealInfo {
            mPrint("handleGetInfo", tData)
            
            self.updateMessagePrompt(tData)
            
            let typeTpl = Utils.getTemplateDeal(tData)
            let smallestTeoCard = SmallestTEOCard().loadTheme(typeTpl)
            smallestTeoCard.config(tData, onClickedCardCallback:  { [weak self] (data) in
                guard let self = self else {return}
                
            })
            
            smallestTeoCard.frame = self.coverTEOCard.bounds
            
            self.coverTEOCard.addSubview(smallestTeoCard)
           
            
        }
    }
    
    private func updateMessagePrompt(_ data: [String: Any?]) {
       
        let dealName = "\"\(data["Name"] as? String ?? "")\""
        let vendorName = data["VendorName"] as? String ?? ""
        let expiredDate = Utils.stringFromStringDate(data["EndAt"] as? String, toFormat: "dd MMM")
        
       //let text = "You have been assigne to the reward \(dealName) at \(vendorName), expired in \(expiredDate)."
        var text = LOCALIZED.you_have_been_assigned_to_the_reward_format.translate
        text = text.replacingOccurrences(of: "[%__TAG_BIG_TITLE__%]", with: dealName)
        text = text.replacingOccurrences(of: "[%__TAG_NAME__%]", with: vendorName)
        text = text.replacingOccurrences(of: "[%__TAG_END_DATE__%]", with: expiredDate)
        
        let mMutableString = NSMutableAttributedString(string: text, attributes:
            [
                NSAttributedString.Key.foregroundColor: UIColor(39, 39, 39),
                NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13.0)!
            ])
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4.0
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        mMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributeString.length))
        
        if let tRange = text.range(of: dealName) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(255, 193, 6), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 13.0)!, range:nsRange)
        }
        
        if let tRange = text.range(of: vendorName) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(39, 39, 39), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 13.0)!, range:nsRange)
        }
        
        if let tRange = text.range(of: expiredDate) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(39, 39, 39), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 13.0)!, range:nsRange)
        }
        
        self.lblPrompt.attributedText = mMutableString
    }
    
    private func handleGetInfo() {
        if let tData = self.data {
            let dealId = tData["dealId"] as? String ?? ""
            
            Utils.showLoading()
            
            APICommonServices.getTEODealDetail(dealId) { (resp) in
                mPrint("handleGetTEODetail", resp)
                
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    if let data = resp["data"] as? [String: Any?], let detail = data["deal"]  as? [String : Any?] {
                        self.dealInfo = detail
                    }
                }
                
                self.fillData()
                
                Utils.dismissLoading()
            }
        }
    }
    
    
    func config(_ data: [String: Any?]?, onClickedAcceptCallback _onClickedAcceptCallback: DataCallback? = nil, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        
        self.onClickedAcceptCallback = _onClickedAcceptCallback
        self.onClickedCancelCallback = _onClickedCancelCallback
        
        self.handleGetInfo()
    }
    
}
