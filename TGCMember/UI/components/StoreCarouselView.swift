//
//  VendorSummaryView.swift
//  TGCMember
//
//  Created by vang on 5/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Kingfisher

class StoreCarouselView: UIViewController {
    
    @IBOutlet weak var tbDeals: UITableView!
    
    private var data:[String: Any?]?
    private var summaryData:[String: Any?]?
    private var list = [Wallet.Deal]()
    
    var scrollItemCallback: DataCallback?
    var onClickedItemCallback: DataCallback?
    var onClickedMoreStoreCallback: DataCallback?
    
    private var cellHeightDict = [IndexPath: CGFloat]()
    
    private let heightCategoryView: CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeTopDeal), name: .ON_CHANGE_TOP_DEAL_INTRO_DEALS, object: nil)
        tbDeals.scrollsToTop = false
        tbDeals.contentInset.bottom = 500
        self.tbDeals.register(UINib(nibName: PullUpDealCell.className(), bundle: nil), forCellReuseIdentifier: PullUpDealCell.className())
//        self.tbDeals.register(UINib(nibName: "CarouselDealCell", bundle: nil), forCellReuseIdentifier: "CarouselDealCell")
//        self.tbDeals.register(UINib(nibName: "SearchNotFoundCell", bundle: nil), forCellReuseIdentifier: "SearchNotFoundCell")
//        self.tbDeals.register(UINib(nibName: "CategoryViewCell", bundle: nil), forCellReuseIdentifier: "CategoryViewCell")
    
    }

    
    func config(_ data: [String: Any?]?, scrollItemCallback _scrollItemCallback: DataCallback? = nil, onClickedItemCallback _onClickedItemCallback: DataCallback? = nil, onClickedMoreStoreCallback _onClickedMoreStoreCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.scrollItemCallback = _scrollItemCallback
        self.onClickedItemCallback = _onClickedItemCallback
        self.onClickedMoreStoreCallback = _onClickedMoreStoreCallback
    }
    
    
    func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        print("Passing all touches to the next view (if any), in the view stack.")
        return false
    }
    
    public func attach(_ pullVC: PullUpController) {
        self.tbDeals?.attach(to: pullVC)
    }
    
    public func updateData() {
        list = GLOBAL.GlobalVariables.listDealsOnMap
        cellHeightDict.removeAll()
        tbDeals.reloadData()
    }
    
    @objc func onChangeTopDeal() {
        tbDeals.reloadData()
        DispatchQueue.main.async {
            self.tbDeals.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
    
    private func openViewAddTrip() {
        let content = CreateTripView().loadView()
        content.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        content.layoutIfNeeded()
        content.config(nil, onClickedCancelCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
        }, onClickedAddTripCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tParams = data {
                self.handleCreateNewTrip(tParams)
            }
            
        })
        
        Utils.showAlertWithCustomView(content, bgColor: UIColor(47, 72, 88, 0.8), animation: false)
    }
    
    private func openUpgrade(_ data: [String: Any?]?) {
        if let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController(), let tData = data {
            let toPlan = tData["CanUpTo"] as? String ?? ""
            
            let selectedIndex = (UPGRADE_PLAN(rawValue: toPlan) ?? UPGRADE_PLAN.FREE).viewIndex
            
            let newUpgradeVC = NewUpgradeViewController()
            newUpgradeVC.fromCarousel = true
            newUpgradeVC.initPlanIndex = selectedIndex
            
            topVC.navigationController?.pushViewController(newUpgradeVC, animated: true)
        }
        
    }
    
    private func handleCreateNewTrip(_ params: [String: Any?]) {
        mPrint("handleCreateNewTrip params --> ", params)
        Utils.showLoading()
        
        APICommonServices.createNewTrip(params as [String : Any]) { (resp) in
            mPrint("handleCreateNewTrip --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                   
                } else {
                    Utils.dismissLoading()
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
        }
    }
    
    func scrollToDeal(_ data: [String: Any]) {
        guard let deal = try? Wallet.Deal(dict: data) else { return }
            
        if let index = list.firstIndex(where: { $0.instanceDealID == deal.instanceDealID }), index < tbDeals.numberOfRows(inSection: 0) {
            tbDeals.reloadData()
            DispatchQueue.main.async {
                self.tbDeals.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
            }
        }
    }
}


extension StoreCarouselView: UITableViewDelegate, UITableViewDataSource {
    
//    private func needIncreaseHeight(_ indexPath: IndexPath) -> [String: Any?] {
//
//        var willNotify: Bool = false
//        var willIncrease: Bool = false
//
//        var sectionFirst =  [String: Any?]()
//        for section in list {
//            let isNotFound = section["IsNotFound"] as? Bool ?? false
//
//            if !isNotFound {
////                sectionFirst = section
//                break
//            }
//        }
//
//        let sectionInfo = list[indexPath.section]
//
//        if sectionInfo["StoreType"] as? String == sectionFirst["StoreType"] as? String && (sectionInfo["StoreType"] as? String ?? "") != "" && (sectionFirst["StoreType"] as? String ?? "") != "" {
//            if indexPath.row == 0 {
//                willNotify = true
//            }
//
//            if let carousels = sectionInfo["List"] as? [[String: Any?]], carousels.count > 0, indexPath.row == 0 {
//                let data = carousels[0]
//
//                if Utils.isCarouselDeal(data) && Utils.isHaveItemMore(data) {
//                    willIncrease = true
//                }
//            }
//        }
//
//        return ["willNotify": willNotify, "willIncrease": willIncrease]
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let deal = list[safe: indexPath.row] else { return UITableViewCell() }
        
        let cell = PullUpDealCell.dequeue(tableView, indexpath: indexPath)
        cell.dealImageView.image = nil
        cell.dealImageView.setKfImage(deal.photo ?? "")
        
        if let prevDeal = list[safe: indexPath.row - 1],
           prevDeal.vendorSummary?.name == deal.vendorSummary?.name {
            cell.vendorNameLabel.isHidden = true
            cell.vendorLogoImageView.superview?.isHidden = true
            if let startDate = deal.dealStartDate, let endDate = deal.dealEndDate {
                cell.locationLabel.isHidden = false
                cell.locationLabel.text = "\(startDate.toString(format: "EEE, MMM dd, yyyy")) - \(endDate.toString(format: "EEE, MMM dd, yyyy"))"
            } else {
                cell.locationLabel.isHidden = true
            }
            
        } else {
            cell.vendorLogoImageView.superview?.isHidden = false
            cell.vendorNameLabel.isHidden = false
            cell.vendorNameLabel.text = deal.vendorSummary?.name?.uppercased()
            cell.vendorLogoImageView.image = nil
            cell.vendorLogoImageView.setKfImage(deal.vendorSummary?.logoURL ?? "", contentMode: .scaleAspectFit)
            cell.locationLabel.text = deal.vendorSummary?.fullAddress
            cell.locationLabel.isHidden = false
        }
        
        cell.statusButton.isHidden = deal.promotionText.isEmpty
        cell.dealNameLabel.text = deal.name?.uppercased()
        
        cell.statusButton.applyGradient(deal.type)
        
        cell.categoryIconImageView.image = deal.type?.iconValue
        
        cell.statusButton.titleLabel?.adjustsFontSizeToFitWidth = true
        cell.statusButton.setTitleColor(deal.isWhiteFont ? .white : Color.darkBlue, for: .normal)
        
        cell.favouriteButton.setImage(deal.isFavorite == true ? #imageLiteral(resourceName: "favorite_active") : #imageLiteral(resourceName: "favorite_button"), for: .normal)
        cell.distanceLabel.text = distance(deal)
        
        cell.onTapFavourite = { [unowned self] in
            toggleFavorite(deal, index: indexPath.row)
        }
        
        cell.statusButton.setTitle(deal.promotionText, for: .normal)
        
        getImage(from: deal.isWhiteFont ? (deal.vendorSummary?.category?.imageURLStyle2Selected ?? "") : (deal.vendorSummary?.category?.imageURLStyle2 ?? ""), completion: { image in
            DispatchQueue.main.async {
                let attr = NSAttributedString(string: deal.promotionText)
                cell.statusButton.setAttributedTitle(NSAttributedString.withImagePrefix(image, text: attr, color: deal.isWhiteFont ? .white : Color.darkBlue), for: .normal)
            }
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightDict[indexPath] = cell.frame.height
    }
    
    func toggleFavorite(_ deal: Wallet.Deal, index: Int) {
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        Utils.showLoading()
        
        Repository.shared.toggleFavoriteStatusOfDeal(request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                var deal = deal
                deal.isFavorite = response.response == .favorite
                
                NotificationCenter.default.post(name: .UPDATE_DEAL, object: deal)
                NotificationCenter.default.post(name: .UPDATE_FAVE_DEAL, object: deal)
                
                var list = self.list
                list[index] = deal
                
                self.list = list
                
                DispatchQueue.main.async {
                    self.tbDeals.reloadData()
                    Utils.dismissLoading()
                }
            case .failure(let error):
                print(error.message)
            }
            
            DispatchQueue.main.async {
                Utils.dismissLoading()
            }
        })
    }
    
    func getImage(from urlString: String,
                  completion: @escaping GenericCallback<UIImage>) {
           if let url = URL(string: urlString) {
            KingfisherManager.shared.retrieveImage(with: url) { [weak self] result in
                guard let self = self else { return }
                
                switch result {
                case .success(let imageResult):
                    completion(imageResult.image)
                case .failure(let error):
                    print(error.errorDescription)
                }
            }
        }
    }
    
    func distance(_ deal: Wallet.Deal) -> String {
        guard let location = deal.vendorSummary?.location, let lat = location.lat, let lng = location.lng, lat != 0, lng != 0 else { return "" }
        let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: lat, endLong: lng)
    
        return "\(String(format: "%.1f", tDistance/1000)) km"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let checkInfo = self.needIncreaseHeight(indexPath)
//        
//        let isNotify = checkInfo["willNotify"] as? Bool ?? false
//        let willIncrease = checkInfo["willIncrease"] as? Bool ?? false
//       
//        if isNotify {
//            NotificationCenter.default.post(name: .UPDATED_SECONDE_HIEGHT_PULL, object: ["isMore": willIncrease])
//        }
        
        return cellHeightDict[indexPath] ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeightDict[indexPath] ?? 300
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let deal = list[safe: indexPath.row] else { return }
        
        showVc(with: Storyboard.wallet, classType: DealDetailViewController.self, viewController: {  vc in
            vc.viewModel = DealDetailViewModel(repository: Repository.shared, deal: deal)
        })
    }
}

