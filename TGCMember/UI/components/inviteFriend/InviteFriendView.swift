//
//  InviteFriendView.swift
//  TGCMember
//
//  Created by vang on 8/26/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

public enum ACTION_INVITE_EMAIL : String {
    case REMOVE = "remove"
    case ADD = "add"
}

class InviteFriendView: UIView {

    private var data: [String: Any?]?
    
    private var onClickedRemoveCallback: DataCallback?
    private var onClickedAddCallback: DataCallback?
    
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var coverBtnView: UIView!
    @IBOutlet weak var coverInputView: UIView!
    
    @IBAction func onClickedAction(_ sender: Any) {
    
        if var tData = self.data  {
            let tType = tData["action"] as? String ?? ACTION_INVITE_EMAIL.ADD.rawValue
            
            if tType == ACTION_INVITE_EMAIL.ADD.rawValue {
                let email = self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                tData["email"] = email
                self.onClickedAddCallback?(tData)
            } else {
                self.onClickedRemoveCallback?(tData)
            }
        }
    }
    
    func loadView() -> InviteFriendView {
        let aView = Bundle.main.loadNibNamed("InviteFriendView", owner: self, options: nil)?[0] as! InviteFriendView
        
        return aView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        decorate()
    }
    
    private func decorate() {
        self.coverInputView.layer.cornerRadius = 3.0
        self.coverInputView.layer.masksToBounds = true
        self.coverInputView.layer.borderColor = UIColor(205, 207, 214).cgColor
        self.coverInputView.layer.borderWidth = 0.5
        
        self.coverBtnView.layer.cornerRadius = 3.0
        self.coverBtnView.layer.masksToBounds = true
        self.coverBtnView.layer.borderColor = UIColor(205, 207, 214).cgColor
        self.coverBtnView.layer.borderWidth = 0.5
    }
    
    public func config(_ data: [String: Any?]?, onClickedRemoveCallback _onClickedRemoveCallback: DataCallback? = nil, onClickedAddCallback _onClickedAddCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        self.onClickedAddCallback = _onClickedAddCallback
        self.onClickedRemoveCallback = _onClickedRemoveCallback
        
        self.fillData()
    }
    
    
    private func fillData() {
        
        if let tData = self.data  {
            let tType = tData["action"] as? String ?? ACTION_INVITE_EMAIL.ADD.rawValue
            let email = tData["email"] as? String ?? ""
            
            self.tfEmail.isUserInteractionEnabled = (tType == ACTION_INVITE_EMAIL.ADD.rawValue) ? true : false
            self.btnAction.setImage(tType == ACTION_INVITE_EMAIL.ADD.rawValue ? UIImage(named: "add_yellow_ic") : UIImage(named: "close_red_ic"), for: .normal)
            
            
            self.tfEmail.text = email
            
            self.tfEmail.textColor = (tType == ACTION_INVITE_EMAIL.REMOVE.rawValue) ? UIColor(155, 155, 155) : UIColor.black
            
        }
    }
    
    

}
