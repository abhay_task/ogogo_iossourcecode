//
//  MoreDealStoreView.swift
//  TGCMember
//
//  Created by vang on 1/16/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class MoreDealStoreView: UIView {
    
    @IBOutlet weak var lblNumberMore: UILabel!
    @IBOutlet weak var moreView: HighlightableView!
    
    @IBOutlet weak var leftCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightCollectionConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    
    private var onClickedCardCallback: DataCallback?
    private var onClickedCloseCallback: DataCallback?
    
    private var data: [String: Any?]?
    private var currentItems:[[String: Any?]] = []
    
    private var widthItem: CGFloat = 140
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateStoreInfo(_:)), name: .UPDATE_MORE_STORED_AGAIN, object: nil)
        self.collectionView.register(UINib(nibName: "NewDealCell", bundle: nil), forCellWithReuseIdentifier: "NewDealCell")
    
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .UPDATE_MORE_STORED_AGAIN, object: nil)
    }
    
    @objc func updateStoreInfo(_ notification: Notification) {
        self.getDetailsAgain()
    }
    
    private func getDetailsAgain() {
        
        if var tData = self.data, let vendorSummary = tData["VendorSummary"] as? [String: Any?], let vendorId = vendorSummary["VendorID"] as? String {
            Utils.showLoading()
            APICommonServices.getMoreDealsOfDeal(vendorId) { [weak self] (resp) in
                guard let self = self else {return}
                mPrint("getMoreDealsOfDeal --> ", resp)
                
                if let resp = resp, let data = resp["data"] as? [String: Any?], let status = resp["status"] as? Bool, status == true  {
                    tData["NumberOfMultiDeals"] = data["NumberOfMultiDeals"] as? Int ?? 0
                    tData["ListMultiDeals"] = data["ListMultiDeals"] as? [[String: Any?]] ?? []
                    
                    self.data = tData
                    
                    self.fillData()
                }
                
                Utils.dismissLoading()
                
            }
        }
        
    }
    
    private func decorate() {
        
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
        self.onClickedCloseCallback?(self.data)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> MoreDealStoreView {
        let aView = Bundle.main.loadNibNamed("MoreDealStoreView", owner: self, options: nil)?[0] as! MoreDealStoreView
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
            let numberMore = tData["NumberOfMultiDeals"] as? Int ?? 0
            
            if numberMore == 0 { 
                self.onClickedCloseCallback?(self.data)
            } else {
                self.lblNumberMore?.text = "\(numberMore) MORE"
                
                let listMore = tData["ListMultiDeals"] as? [[String: Any?]] ?? []
                self.currentItems = listMore
                
                let numberRow = self.currentItems.count/2 + self.currentItems.count%2
                
                let limitHeight = SCREEN_HEIGHT - 200
                let heightContent = 70 + numberRow * 165
                self.heightContentConstraint?.constant = min(CGFloat(heightContent), CGFloat(limitHeight))
                
                
                let remainWidth: CGFloat = SCREEN_WIDTH - 280 - 12
                var tPadding: CGFloat = 10.0
                
                if remainWidth > 60 {
                    tPadding = 20.0
                }
                
                self.widthContentConstraint?.constant = 280 + 12 + tPadding * 2
                self.leftCollectionConstraint?.constant = tPadding - 5
                self.rightCollectionConstraint?.constant = tPadding - 5
                
                self.widthItem = 151
                
                self.collectionView.reloadData()
            }
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedCloseCallback _onClickedCloseCallback: DataCallback? = nil, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onClickedCloseCallback = _onClickedCloseCallback
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }
    
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MoreDealStoreView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentItems.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var data = currentItems[indexPath.row]
        
        //data["DealProgram"] = "sweepstake"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewDealCell", for: indexPath) as! NewDealCell
        
        cell.config(data, onClickedCellCallback: { [weak self] (pData) in
            guard let self = self else {return}
            
            if var _data = pData {
                _data["fromMoreStored"] = true
                self.onClickedCardCallback?(_data)
            }
        })
        
        
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.widthItem, height: 165) // vangdoan
    }
    
    
    
    
}
