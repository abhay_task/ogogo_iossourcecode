//
//  VendorTabDetailViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/5/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class VendorTabDetailViewController: UIViewController {

    var onClickTab: Bool = false
    var beginScroll: Bool = false
    
    @IBOutlet weak var coverTabControl: UIView!
    @IBOutlet weak var tabsCollectionView: UICollectionView!
    @IBOutlet weak var tbDetails: UITableView!

    private var heightAbout: CGFloat  = 265
    private var heightMemberOffer: CGFloat = 280
    private var heightNormalDeals: CGFloat = ((UIScreen.main.bounds.width - 50) * 150 / 325) + 110
    private var heightCarouselDeals: CGFloat = 520.0
    
    private var heightEvents: CGFloat = ((UIScreen.main.bounds.width - 50) * 150 / 325) + 90
    var allowScrollToInitTab: Bool = false
    var initTab: VENDOR_INIT_TAB = VENDOR_INIT_TAB.ABOUT

    private var selectedTab: Int = 0
    var tabs: [[String: String]] = []
    var aboutInfo: [String: Any?]?
    var offerInfo:[String: Any?]?
    var deals:[Any] = []
    var originalDeals:[[String: Any?]] = []
    var events:[[String: Any?]] = []
    
    var parentVC: UIViewController?
    var memberOffers: [String] = ["Old Fashioned", "Margarita", "Long Island Iced", "Tequila", "Offer - 555", "Offer - 666"]
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabsCollectionView.delegate = self
        self.tabsCollectionView.dataSource = self
        
        self.tabsCollectionView.register(UINib(nibName: "TabControlCell", bundle: nil), forCellWithReuseIdentifier: "TabControlCell")

        self.tbDetails.delegate = self
        self.tbDetails.dataSource = self
        
        self.tbDetails.panGestureRecognizer.addTarget(self, action: #selector(handleScrollViewGestureRecognizer(_:)))
        self.tbDetails.register(UINib(nibName: "TabAboutCell", bundle: nil), forCellReuseIdentifier: "TabAboutCell")
        self.tbDetails.register(UINib(nibName: "TabBasicDealCell", bundle: nil), forCellReuseIdentifier: "TabBasicDealCell")
        self.tbDetails.register(UINib(nibName: "TabDealCell", bundle: nil), forCellReuseIdentifier: "TabDealCell")
        self.tbDetails.register(UINib(nibName: "CarouselDealCell", bundle: nil), forCellReuseIdentifier: "CarouselDealCell")
        self.tbDetails.register(UINib(nibName: "TabEventCell", bundle: nil), forCellReuseIdentifier: "TabEventCell")
           
        self.tbDetails.addObserver(self, forKeyPath:"contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        if offerInfo == nil {
            self.tbDetails.isHidden = false
            
            Utils.dismissLoading()
        } else {
            self.tbDetails.isHidden = true
        }
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tbDetails.refreshControl = refreshControl
        } else {
            tbDetails.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(reloadVendor(_:)), for: .valueChanged)
        
    }
    
    @objc private func reloadVendor(_ sender: Any) {
        // Fetch Weather Data
        self.refreshControl.endRefreshing()
        
        NotificationCenter.default.post(name: .REFRESH_VENDOR_DETAIL, object: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if beginScroll && onClickTab == false {
            if let dictChange = change, let newPoint: CGPoint = dictChange[NSKeyValueChangeKey.newKey] as? CGPoint {
                //print("CHANGE OBSERVED: \(newPoint.y)")
                
                let section = self.getSectionAt(newPoint)
                
                if (selectedTab != section) {
                    selectedTab = section
                    self.tabsCollectionView.reloadData()
                }
            }
        }
    }
    
    private func getSectionAt(_ newPoint: CGPoint) -> Int {
        var tSection = 0
        
        for index in 0..<tabs.count {
            let indexReversed = (tabs.count - 1) - index
            
            var yHeader:CGFloat = 0.0
            if indexReversed > 0 {
                yHeader =  self.tbDetails.rectForHeader(inSection: indexReversed).origin.y
            }
            
            if newPoint.y >= yHeader {
                tSection = indexReversed
                break
            }
        }
        
        return tSection
    }
    

    
    @objc private func handleScrollViewGestureRecognizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            beginScroll = true
        default:
            break
        }
    }
    
    public func forceUpdateData() {
//        UIView.performWithoutAnimation {
//            if tabs.count > 1  {
//
//                let tDict =  tabs[1]
//                if tDict["key"] == VENDOR_INIT_TAB.BASIC_DEAL.tabSwitch {
//                    self.tbDetails.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .none)
//                }
//            }
//        }
        self.tbDetails.reloadData()
        self.tabsCollectionView.reloadData()
    }
    
    private func getNumberDealInWallet() -> String {
        var number = 0
        for deal in self.originalDeals {
            if deal["IsWallet"] as? Bool ?? false {
                number += 1
            }
        }
        
        if number == 0 {
            return ""
        }
        
        return String(number)
    }

    private func handleScrollToInitTab() {
        Utils.dismissLoading()
        
        if self.initTab != VENDOR_INIT_TAB.ABOUT && allowScrollToInitTab {

            self.tbDetails.isHidden = false
            self.selectedTab = self.initTab.rawValue
            self.tabsCollectionView.reloadData()
            
            allowScrollToInitTab = false
            self.tbDetails.scrollToRow(at: IndexPath(row: 0, section: self.selectedTab), at: .top, animated: true)
        } else if self.initTab == VENDOR_INIT_TAB.ABOUT {
            allowScrollToInitTab = false
        
            self.tbDetails.isHidden = false
        }
    }
    
    private func handleUseBasicDeal(_ info:[String: Any?]?) {
        if let tParent = self.parentVC {
            Utils.checkLogin(tParent) {  [weak self]  (status) in
                guard let self = self else {return}
                
                if status == LOGIN_STATUS.SUCCESS_IMMEDIATELY {
                    if let tData = self.offerInfo {
//                        Utils.showLoading()
//
//                        let dealId = data["ID"] as? String
//                        let code = "member"
//                        APICommonServices.doUseDeal(dealId, code) { [weak self] (resp) in
//                            guard let self = self else {return}
//
//                            //mPrint("doUseDeal", resp)
//                            if let resp = resp, let status = resp["status"] as? Bool {
//
//                                if let data = resp["data"] as? [String: Any?], status == true {
//                                    NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
//                                }
//
//                                Utils.showAlert(self, "", resp["message"] as? String ?? "")
//                            }
//
//                            Utils.dismissLoading()
//                        }
                        Utils.showClaimAlert(tData, successCallback: { (success, data) in
                            if success {
                                NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                            }
                        })
                        
                    }
                } else if status == LOGIN_STATUS.SUCCESS_AFTER {
                    //print("WILL DO USE DEAL AFTER")
                }
            }

        }
    }
    
    private func handleLikeBasicDeal(_ info:[String: Any?]?) {
        if let data = info, let tParent = self.parentVC {
            Utils.checkLogin(tParent) {  [weak self]  (status) in
                guard let self = self else {return}
                
                if status == LOGIN_STATUS.SUCCESS_IMMEDIATELY {
                    Utils.showLoading()
                    
                    let offerId = data["ID"] as? String
                    APICommonServices.toogleLikeDeal(offerId) { [weak self] (resp) in
                        guard let self = self else {return}
                        //mPrint("toogleLikeOffer", resp)
                        if let resp = resp, let status = resp["status"] as? Bool {
                            
                            if let data = resp["data"] as? [String: Any?], status == true {
                                 NotificationCenter.default.post(name: .TOOGLE_LIKE_DEAL, object: data)
                            }
                            
                          //  Utils.showAlert(self, "", resp["message"] as? String ?? "")
                        }
                        
                        Utils.dismissLoading()
                    }
                } else if status == LOGIN_STATUS.SUCCESS_AFTER {
                    // refesh view data
                }
            }
        }
    }
    
    
    
    private func handleShowAvailableTime(_ data: [String: Any?]?) {
        
        if let tData = data, let sender = tData["sender"] as? UIButton {
          
            let availableTimeVC = AvailableTimeView()
            let availableTimes = tData["AvailableTimes"] as? [[String: Any?]] ?? []
            availableTimeVC.availableTimes = availableTimes
           
            
            availableTimeVC.preferredContentSize = CGSize(width: 190, height: CGFloat(availableTimes.count * 40))

            let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: availableTimeVC)
            presentationController.sourceView = sender
            presentationController.sourceRect = sender.bounds
            presentationController.permittedArrowDirections = [.up]
            
            self.present(availableTimeVC, animated: true)
        }

    }

}


// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension VendorTabDetailViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.tabs.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let tabCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TabControlCell", for: indexPath) as! TabControlCell
        
        var tDict = [String : Any]()
        tDict["title"] = tabs[indexPath.row]["value"]!
        tDict["key"] =  tabs[indexPath.row]["key"]!
        tDict["tag"] =  indexPath.row
        tDict["selectedIndex"] =  self.selectedTab
        
        if tabs[indexPath.row]["key"]! == VENDOR_INIT_TAB.DEALS.tabSwitch && self.getNumberDealInWallet() != "" {
            tDict["number"] = self.getNumberDealInWallet()
        }
        
        tabCell.config(tDict, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            self.onClickTab = true
            
            if let tData = data {
                self.selectedTab = tData["tag"] as! Int
                
                self.tabsCollectionView.reloadData()
                
                if self.selectedTab == 0 {
                    self.tbDetails.setContentOffset(CGPoint(x: 0, y: -self.tbDetails.contentInset.top), animated: true)
                } else {
                    self.tbDetails.scrollToRow(at: IndexPath(row: 0, section: self.selectedTab), at: .top, animated: true)
                }
                
            }
            
        })
        
        return tabCell
        
        
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let tWidth = UIScreen.main.bounds.width/CGFloat(self.tabs.count)
        let tWidth = UIScreen.main.bounds.width/CGFloat(4)
        
        return CGSize(width: tWidth, height: 35)
    }
    
    
    
}

extension VendorTabDetailViewController: UITableViewDelegate, UITableViewDataSource {
   
    func refreshCellAtIndexPath(indexPath: IndexPath) -> Void {
        UIView.performWithoutAnimation {
             self.tbDetails.reloadRows(at: [indexPath], with: .none)
        }
        
        setTimeout({
            self.handleScrollToInitTab()
        }, 100)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabs[section]["key"] == VENDOR_INIT_TAB.ABOUT.tabSwitch || tabs[section]["key"] == VENDOR_INIT_TAB.BASIC_DEAL.tabSwitch {
            return 1
        } else if tabs[section]["key"] == VENDOR_INIT_TAB.DEALS.tabSwitch {
            return deals.count
        } else if tabs[section]["key"] == VENDOR_INIT_TAB.EVENTS.tabSwitch {
            return events.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.ABOUT.tabSwitch {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TabAboutCell") as? TabAboutCell
            cell?.parentVC = self.parentVC
            
            cell?.config(self.aboutInfo, onClickedOpenNow: { [weak self] (info) in
                guard let self = self else {return}
              
                self.handleShowAvailableTime(info)
            })
            
            return cell!
        } else if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.BASIC_DEAL.tabSwitch {
            let offerCell = tableView.dequeueReusableCell(withIdentifier: "TabBasicDealCell") as? TabBasicDealCell
            
            offerCell?.config(self.offerInfo, onClickedUseCallback: { [weak self]  (info) in
                guard let self = self else {return}
                self.handleUseBasicDeal(info)
                
            }, onClickedLikeCallback: { [weak self] (info) in
                guard let self = self else {return}
                
                self.handleLikeBasicDeal(info)
                
            }, onHeightChanged: { [weak self] (info) in
                guard let self = self else {return}
                
                if let tInfo = info {
                    let contentHeight = tInfo["contentHeight"] as? CGFloat
                    
                    if let tHeight = contentHeight {
                        self.heightMemberOffer = tHeight
                    }
                    
                    self.allowScrollToInitTab = true
                    
                    self.refreshCellAtIndexPath(indexPath: indexPath)
                    
                }
               
            })
            
            return offerCell!
        } else if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.DEALS.tabSwitch {
            let dealData = self.deals[indexPath.row]
            
            if dealData is Array<Any> {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CarouselDealCell") as? CarouselDealCell
                
                cell?.config(["dealList": dealData])
                
                return cell!
                
            } else {
                let dealCell = tableView.dequeueReusableCell(withIdentifier: "TabDealCell") as? TabDealCell
                dealCell?.config(dealData as! [String : Any?], onClickedCell: { [weak self] (data) in
                    guard let self = self else {return}
    
                    let dealDetailVC = DealDetailViewController()
                    dealDetailVC.dealInfo = data
                    self.navigationController?.pushViewController(dealDetailVC, animated: true)
    
                }, onClickedMore: { [weak self] (data) in
                    guard let self = self else {return}
                })
    
    
                return dealCell!
            }

            return UITableViewCell()
            
        } else if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.EVENTS.tabSwitch {
            let eventData = self.events[indexPath.row]
            
            let eventCell = tableView.dequeueReusableCell(withIdentifier: "TabEventCell") as? TabEventCell
            eventCell?.config(eventData, onClickedCell: { [weak self] (data) in
                guard let self = self else {return}
                
                let eventDetailVC = EventDetailViewController()
                eventDetailVC.eventInfo = data
                self.navigationController?.pushViewController(eventDetailVC, animated: true)
            }, onClickedMore: { [weak self] (data) in
                    guard let self = self else {return}
            })
            
            return eventCell!
        }
        
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tabs.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.ABOUT.tabSwitch {
            return self.heightAbout
        } else  if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.BASIC_DEAL.tabSwitch {
            return self.heightMemberOffer
        } else if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.DEALS.tabSwitch {
            let data = self.deals[indexPath.row]
            
            if data is Array<Any> {
                return self.heightCarouselDeals
            }
            
            return self.heightNormalDeals
         
        } else if tabs[indexPath.section]["key"] == VENDOR_INIT_TAB.EVENTS.tabSwitch {
            return self.heightEvents
        }
        
        return 0
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clear
        
        let lblTitle = UILabel()
        lblTitle.text = tabs[section]["value"]
        lblTitle.textColor = UIColor(228, 109, 87)
        lblTitle.font = UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 15)
        
        header.addSubview(lblTitle)
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(header).offset(25)    
            make.right.equalTo(header).offset(25)
            make.bottom.equalTo(header).offset(tabs[section]["key"] == VENDOR_INIT_TAB.ABOUT.tabSwitch ? -15 : 0)
        }
        
        let line = UIImageView()
        line.image = UIImage(named: "line")
        header.addSubview(line)
        line.snp.makeConstraints { (make) in
            make.top.equalTo(header)
            make.left.equalTo(header).offset(25)
            make.right.equalTo(header).offset(-25)
            make.height.equalTo(1)
        }
        
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       if (tabs[section]["key"] == VENDOR_INIT_TAB.ABOUT.tabSwitch) {
            return 0.0
        }
        
        return 40.0
        
    }
    

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if self.initTab != VENDOR_INIT_TAB.ABOUT && contentOfferFinishedLoad && indexPath.section == self.initTab.rawValue {
//            self.selectedTab = indexPath.section
//            self.tabsCollectionView.reloadData()
//
//            contentOfferFinishedLoad = false
//            tableView.scrollToRow(at: IndexPath(row: 0, section: indexPath.section), at: .top, animated: true)
//        }
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self .perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
        
    }
    
    @objc internal func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget:self)
    
        onClickTab = false
    }
    
}



