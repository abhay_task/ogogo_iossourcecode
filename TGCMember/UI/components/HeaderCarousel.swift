//
//  HeaderCarousel.swift
//  TGCMember
//
//  Created by vang on 1/7/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class HeaderCarousel: UIView {
    
    private var data:[String: Any?]?
    private var onClickedUpgradeCallback: DataCallback?
    
    @IBOutlet weak var imageUpgrade: UIImageView!
    @IBOutlet weak var coverTxt: UIView!
    @IBOutlet weak var coverBadge: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var coverUpgrade: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.decorate()
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        self.onClickedUpgradeCallback?(self.data)
    }
    
    private func decorate() {
        coverBadge.layer.cornerRadius = 4.0
        coverBadge.layer.masksToBounds = true
        
        coverTxt.backgroundColor = UIColor.white
        coverTxt.clipsToBounds = false
        coverTxt.layer.applySketchShadow(color: UIColor("rgba 120 121 147 1"), alpha: 0.2, x: 0, y: 2, blur: 16, spread: 0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> HeaderCarousel {
        let aView = Bundle.main.loadNibNamed("HeaderCarousel", owner: self, options: nil)?[0] as! HeaderCarousel
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblTitle.text = tData["Title"] as? String ?? ""
            
            let canUpgrade = tData["CanUp"] as? Bool ?? false
            self.coverUpgrade.isHidden = !canUpgrade
            
            self.imageUpgrade.sd_setImage(with:  URL(string: tData["IconUpgrade"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
            })
        }
    }
    
    
    func config(_ data: [String: Any?]?, onClickedUpgradeCallback _onClickedUpgradeCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedUpgradeCallback = _onClickedUpgradeCallback
        
        self.fillData()
    }
    
    
}
