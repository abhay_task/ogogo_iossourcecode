//
//  BaseCardMiddle.swift
//  TGCMember
//
//  Created by vang on 12/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class BaseCardMiddle: UIView {

    open func config(_ data: [String: Any?]?, onClickedBackCallback _onClickedBackCallback: DataCallback? = nil, onClickedNextCallback _onClickedNextCallback: DataCallback? = nil, onClickedChatCallback _onClickedChatCallback: DataCallback? = nil) -> Void {
    }
}
