//
//  GoldCard.swift
//  TGCMember
//
//  Created by vang on 8/8/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum DealCardSize: String {
    case BIG = "BigDealCard"
    case SMALL = "SmallDealCard"
}

class DealStyle {
    var bgImage: UIImage?
    var isHideDot: Bool = false
    var companyFont: UIFont?
    var companyColor: UIColor?
    
    var bigTitleFont: UIFont?
    var bigTitleColor: UIColor?
    var smallTitleFont: UIFont?
    var smallTitleColor: UIColor?
    
    var descriptionFont: UIFont?
    var descriptionColor: UIColor?
    
    var expiredDateFont: UIFont?
    var expiredDateColor: UIColor?
    
    var discountFont: UIFont?
    
    var lineImage: UIImage?
    
    var bgCoverTextColor: UIColor?
    var bgFooterColor: UIColor?
    
    var clickMoreSize: CGSize = .zero
    
    func configForBlack(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.isHideDot = false
        self.bgImage = UIImage(named: "black_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 0 0 0")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 185 185 185")
            self.clickMoreSize = CGSize(width: 170, height: 170)
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 222 230 156")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 222 230 156")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 225 225 225")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.clickMoreSize = CGSize(width: 146, height: 42)
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.clickMoreSize = CGSize(width: 202, height: 57)
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 121, height: 36)
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 148 44")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 148 44")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            self.clickMoreSize = CGSize(width: 146, height: 42)
        }
    }
    
    func configForSilver(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.bgImage = UIImage(named: "silver_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 51 51 51")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 51 51 51")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 51 51 51")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 230 230 230")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 170, height: 170)
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 27 12 68")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 26 26 26")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
  
            self.clickMoreSize = CGSize(width: 146, height: 42)
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 0 0 0")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 41 4 38")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 41 4 38")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 41 4 38")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
      
            self.clickMoreSize = CGSize(width: 202, height: 57)
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 26 26 26")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            
            self.lineImage = UIImage(named: "w_silver_line")
            self.clickMoreSize = CGSize(width: 121, height: 36)
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 148 44")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 148 44")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 146, height: 42)
        }
    }
    
    func configForGold(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.bgImage = UIImage(named: "gold_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 51 51 51")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 51 51 51")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 51 51 51")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 230 230 230")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 170, height: 170)
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 27 12 68")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 26 26 26")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 146, height: 42)
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 0 0 0")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 41 4 38")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 41 4 38")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 41 4 38")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 202, height: 57)
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 26 26 26")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            
            self.lineImage = UIImage(named: "w_gold_line")
            self.clickMoreSize = CGSize(width: 121, height: 36)
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 148 44")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 148 44")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.clickMoreSize = CGSize(width: 146, height: 42)
        }
    }
    
    func configForBasic(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.isHideDot = false
        self.bgImage = UIImage(named: "basic_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 51 51 51")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 51 51 51")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 51 51 51")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 230 230 230")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 170, height: 170)
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 27 12 68")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 26 26 26")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 146, height: 42)
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 0 0 0")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 41 4 38")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 41 4 38")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 41 4 38")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 202, height: 57)
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 26 26 26")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 0 0 0")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 0 0 0")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            
            self.lineImage = UIImage(named: "w_silver_line")
            self.clickMoreSize = CGSize(width: 121, height: 36)
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 148 44")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 148 44")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 51 51 51")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
    
            self.clickMoreSize = CGSize(width: 146, height: 42)
        }
    }
    
    func configForTEO(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.isHideDot = true
        self.bgImage = UIImage(named: "teo_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 0 0 0")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 185 185 185")
            
            self.clickMoreSize = CGSize(width: 170, height: 170)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 222 230 156")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 222 230 156")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 225 225 225")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)

            self.clickMoreSize = CGSize(width: 146, height: 42)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)

            self.clickMoreSize = CGSize(width: 202, height: 57)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
    
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 121, height: 36)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 148 44")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 148 44")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            self.clickMoreSize = CGSize(width: 146, height: 42)
            
            self.expiredDateColor = UIColor(hexFromString: "#FFC106")
            self.expiredDateFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: (cardSize == .BIG) ? 14 : (cardSize == .SMALL ? 6 : 5))
        }
    }
    
    func configForLoyalty(_ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.bgImage = UIImage(named: "loyalty_deal_bg")
        
        if theme == .ONE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 30: 12)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 27 : 11)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM_ITALIC.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 230 230 230")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgCoverTextColor = UIColor("rgb 47 72 88")
            self.clickMoreSize = CGSize(width: 170, height: 170)
        } else if theme == .TWO {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5 )
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 23 : 11)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 225 225 225")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.clickMoreSize = CGSize(width: 146, height: 42)
        } else if theme == .THREE {
            self.companyFont = UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 20)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 12)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
        
            self.clickMoreSize = CGSize(width: 202, height: 57)
        } else if theme == .FOUR {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 12 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 28 : 11)
            self.bigTitleColor = UIColor("rgb 255 255 255")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 26 : 10)
            self.smallTitleColor = UIColor("rgb 255 255 255")
            
            self.descriptionFont = UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 13 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
            
            self.bgFooterColor = UIColor("rgb 51 51 51")
            self.clickMoreSize = CGSize(width: 121, height: 36)
            
        } else if theme == .FIVE {
            self.companyFont = UIFont(name: MONTSERRAT_FONT.BLACK.rawValue, size: cardSize == .BIG ? 13 : 5)
            self.companyColor = UIColor("rgb 255 255 255")
            
            self.bigTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 29 : 12)
            self.bigTitleColor = UIColor("rgb 255 193 6")
            
            self.smallTitleFont = UIFont(name: ULTRA_FONT.REGULAR.rawValue, size: cardSize == .BIG ? 31 : 12)
            self.smallTitleColor = UIColor("rgb 255 193 6")
            
            self.descriptionFont = UIFont(name: MONTSERRAT_FONT.MEDIUM.rawValue, size: cardSize == .BIG ? 14 : 6)
            self.descriptionColor = UIColor("rgb 255 255 255")
            
            self.discountFont = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: cardSize == .BIG ? 16 : 5)
    
            self.clickMoreSize = CGSize(width: 146, height: 42)
        }
    }
    
    required convenience init(_ dealType: DEAL_TYPE = .BLACK, _ theme:  TEMPLATE_CARD_DEAL = .ONE, _ cardSize: DealCardSize = .BIG) {
        self.init()
        
        if dealType == .BLACK {
            self.configForBlack(theme, cardSize)
        } else if dealType == .SILVER {
            self.configForSilver(theme, cardSize)
        } else if dealType == .GOLD {
            self.configForGold(theme, cardSize)
        } else if dealType == .BASIC {
            self.configForBasic(theme, cardSize)
        } else if dealType == .TEO {
            self.configForTEO(theme, cardSize)
        } else if dealType == .LOYALTY {
            self.configForLoyalty(theme, cardSize)
        }
    }
}

class DealCardFront: BaseCardFront {
    @IBOutlet weak var heightLine2Constraint: NSLayoutConstraint!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var lblBigTitle: UILabel!
    @IBOutlet weak var lblSmallTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var thumbnailDeal: UIImageView!
    @IBOutlet weak var coverTextView: UIView!
    
    @IBOutlet weak var overlayLoyalty: UIView!
    @IBOutlet weak var coverLoyalty: UIView!
    @IBOutlet weak var bgDot: UIImageView!
    
    @IBOutlet weak var bottomTitle2Constraint: NSLayoutConstraint!
    @IBOutlet weak var btnMoreHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnMoreWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var btnMoreDetail: UIButton!
    
    @IBOutlet weak var lineThumbnail1: UIImageView!
    @IBOutlet weak var lineThumbnail2: UIImageView!
    
    @IBOutlet weak var coverFooterView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var outsideDiscount: UIView!
    @IBOutlet weak var coverDiscount: UIView!
    @IBOutlet weak var coverCate: UIView!
    
    @IBOutlet weak var coverTextViewTEO: UIView!
    @IBOutlet weak var lblDescriptionTEO: UILabel!
    @IBOutlet weak var lblExpiredDateTEO: UILabel!
    
    @IBOutlet weak var whiteLine1: UIView!
    @IBOutlet weak var whiteLine2: UIView!
    
    // LOYALTY SPECIFIC
    @IBOutlet weak var loyaltyBgImage: UIImageView!
    @IBOutlet weak var loyaltyThumbnail: UIImageView!
    @IBOutlet weak var loyaltyLblTitle1: UILabel!
    @IBOutlet weak var loyaltyLblTitle2: UILabel!
    @IBOutlet weak var loyaltyLblCateName: UILabel!

    @IBOutlet weak var loyaltyLblDescription: UILabel!
    @IBOutlet weak var loyaltyVendorLogo: UIImageView!
    @IBOutlet weak var lblExpireDate: UILabel!
    
    @IBOutlet weak var lblDayOfWeek: UILabel!
    
    private var data: [String: Any?]?
    private var onClickedCardCallback: DataCallback?


    @IBAction func onClickedMoreDetail(_ sender: Any) {
        self.onClickedCardCallback?(self.data)
    }
    
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // self.detailView = self.loadSecondView()
    }
    
    
    private func fillData() {
        if let tData = self.data {
            //mPrint("deal ->fillData", tData)
            let tCardSize = DealCardSize(rawValue: String(describing: type(of: self))) ?? .BIG
            
            let _dealType = Utils.getDealType(tData)
            let typeTpl = Utils.getTemplateDeal(tData)
            
            let tSummary = tData["VendorSummary"] as? [String: Any?] ?? [String: Any?]()
            let tCategory = tSummary["Category"] as? [String: Any?] ?? [String: Any?]()
            decorate(dealType: _dealType, theme: typeTpl, cardSize: tCardSize)
            
            let maskCard = tData["maskCard"] as? MASK_CARD ?? MASK_CARD.NONE
            
            self.coverLoyalty?.isHidden =  _dealType != .LOYALTY
            self.contentView?.isHidden =  _dealType == .LOYALTY
     
            if maskCard == .CLAIMED {
                coverMask?.alpha = 1
            } else {
                coverMask?.alpha = 0
            }
            
            let cateURL = tCategory["ImageUrlSelected"] as? String ?? ""
            self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in

            })
            
            let thumbnailDeal = tData["Photo"] as? String ?? ""
            self.bgImage?.sd_setImage(with: URL(string: thumbnailDeal), completed: { (_, _, _, _) in
                
            })
            
            var title1 = tData["TitleLine1"] as? String ?? ""
            var title2 = tData["TitleLine2"] as? String ?? ""
            
            if typeTpl == .TWO {
                title1 = title1.capitalized
                title2 = title2.capitalized
            } else {
                title1 = title1.uppercased()
                title2 = title2.uppercased()
            }
            
            self.lblBigTitle.text = title1 == "" ? " " : title1
            self.lblSmallTitle.text = title2 == "" ? " " : title2
           
            self.lblCompany.text = (tSummary["Name"] as? String ?? "").uppercased()
            self.lblDescription.text = tData["ShortDescription"] as? String ?? ""
            self.lblDescriptionTEO.text = tData["ShortDescription"] as? String ?? ""

            
            let nameDotBg = "\(_dealType.rawValue)_dot_\(typeTpl.rawValue)"
            self.bgDot?.image = UIImage(named: nameDotBg)
            
            let txtDiscount = (tData["LabelTxt"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            self.outsideDiscount.isHidden = txtDiscount == ""
            
            self.lblDiscount.text = txtDiscount
            
            var strDate = LOCALIZED.expiry_date_format.translate
            strDate = strDate.replacingOccurrences(of: "[%__TAG_END_DATE__%]", with: Utils.stringFromStringDate(tData["ExpiryTime"] as? String, toFormat: "dd MMM yyyy"))
            self.lblExpiredDateTEO?.text = strDate
            
            
            let nameButton = "click_detail_\(_dealType.rawValue)\(typeTpl.rawValue)"
            self.btnMoreDetail?.setImage(UIImage(named: nameButton), for: .normal)
            
            
            // CONFIG FOR LOYALTY
            if _dealType == .LOYALTY {
                self.loyaltyBgImage?.sd_setImage(with: URL(string: tData["BackGroundLoyalty"] as? String ?? ""), completed: { [weak self] (_, _, _, _) in
                    guard let self = self else { return }
                               
                })
                
                self.loyaltyThumbnail?.sd_setImage(with: URL(string: thumbnailDeal), completed: { [weak self] (_, _, _, _) in
                    guard let self = self else { return }
                
                })
                
                self.loyaltyLblTitle1?.text = (tData["TitleLine1"] as? String ?? "").capitalized
                self.loyaltyLblTitle2?.text = (tData["TitleLine2"] as? String ?? "").capitalized
                
                self.bottomTitle2Constraint?.constant = title2 == "" ? 8 : 0
                
                
                self.loyaltyLblCateName?.text = (tCategory["Name"] as? String ?? "").capitalized
                
                self.loyaltyLblDescription?.setAttribute(tData["LongDescription"] as? String ?? "", color: .white, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 3.5)
                
                self.loyaltyVendorLogo?.sd_setImage(with: URL(string: tSummary["LogoUrl"] as? String ?? ""), completed: { (_, _, _, _) in
                    
                })
                
                if let fromDate = tData["DealStartDate"] as? String, let toDate = tData["DealEndDate"] as? String, fromDate != "", toDate != "" {
                    self.lblExpireDate?.text = "\(Utils.stringFromStringDate(fromDate, toFormat: "EEE, dd MMM, yyyy")) - \(Utils.stringFromStringDate(toDate, toFormat: "EEE, dd MMM, yyyy"))"
                } else {
                    self.lblExpireDate?.text = ""
                }
                
                self.lblDayOfWeek?.text = tData["TxtDayOfWeek"] as? String ?? ""
            }
            
            if tCardSize == .BIG {
                let tStyle = DealStyle(_dealType, typeTpl, tCardSize)
                
                self.btnMoreWidthConstraint?.constant = tStyle.clickMoreSize.width
                self.btnMoreHeightConstraint?.constant = tStyle.clickMoreSize.height
            } else {
                self.btnMoreWidthConstraint?.constant = 58
                self.btnMoreHeightConstraint?.constant = 17
            }
            
        }
    }
    
    func decorate(dealType: DEAL_TYPE = .BLACK, theme: TEMPLATE_CARD_DEAL = .ONE, cardSize: DealCardSize = .BIG) {
        self.coverCate?.isHidden = true
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.outsideDiscount?.frame ?? CGRect.zero
        rectShape.position = self.outsideDiscount?.center ?? CGPoint.zero
        
        rectShape.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: rectShape.bounds.width, height: rectShape.bounds.height), byRoundingCorners: [.topLeft], cornerRadii: CGSize(width: 25, height: 25)).cgPath
        
        self.outsideDiscount?.layer.mask = rectShape
        
        self.coverCate?.layer.cornerRadius = 20.0
        self.coverCate?.layer.borderColor =  UIColor.white.cgColor
        self.coverCate?.layer.borderWidth = 1.0
        self.coverCate?.layer.masksToBounds = true
        
        self.coverDiscount?.layer.cornerRadius = 3.0
        self.coverDiscount?.layer.masksToBounds = true
    
        self.loyaltyVendorLogo?.layer.applySketchShadow(color: UIColor("rgb 255 190 1"), alpha: 0.88, x: 0, y: 0, blur: 8, spread: 0)
        
        //self.lblExpireDate?.layer.applySketchShadow(color: UIColor("177 228 255"), alpha: 1, x: 0, y: 0, blur: 2, spread: 0)
        self.lblExpireDate?.layer.shadowColor = UIColor("rgb 177 228 255").cgColor
        self.lblExpireDate?.layer.shadowRadius = 2.0
        self.lblExpireDate?.layer.shadowOpacity = 1.0
        self.lblExpireDate?.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.lblExpireDate?.layer.masksToBounds = false
        
        self.lblDayOfWeek?.layer.shadowColor = UIColor("rgb 177 228 255").cgColor
        self.lblDayOfWeek?.layer.shadowRadius = 2.0
        self.lblDayOfWeek?.layer.shadowOpacity = 1.0
        self.lblDayOfWeek?.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.lblDayOfWeek?.layer.masksToBounds = false
        
        self.bgImage?.layer.cornerRadius = 25.0
        coverFooterView?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        coverFooterView?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        contentView?.clipsToBounds = cardSize == .BIG ? false : true
        contentView?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        
        coverLoyalty?.clipsToBounds = cardSize == .BIG ? false : true
        coverLoyalty?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        
        loyaltyBgImage?.clipsToBounds = true
        loyaltyBgImage?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        
        overlayLoyalty?.clipsToBounds = cardSize == .BIG ? false : true
        overlayLoyalty?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        
        loyaltyThumbnail?.layer.cornerRadius = 6.0
        
        coverMask?.alpha = 0
        coverMask?.clipsToBounds = true
        coverMask?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
        
        if cardSize == .BIG {
            contentView?.layer.applySketchShadow(color: UIColor(92, 92, 92), alpha: 0.5, x: 2, y: 2, blur: 4, spread: 0)
        } else {
            contentView?.layer.applySketchShadow(color: UIColor(92, 92, 92), alpha: 0.5, x: 1, y: 1, blur: 2, spread: 0)
        }
        
        if theme == .FIVE {
            thumbnailDeal?.layer.cornerRadius = cardSize == .BIG ? 25.0 : 10.0
            thumbnailDeal?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
        
        let tStyle = DealStyle(dealType, theme, cardSize)
        
        self.bgImage?.image = tStyle.bgImage
        
        self.lblCompany?.textColor = tStyle.companyColor
        self.lblCompany?.font = tStyle.companyFont ?? FONT_DEFAULT
        
        self.lblBigTitle?.textColor = tStyle.bigTitleColor
        self.lblBigTitle?.font = tStyle.bigTitleFont ?? FONT_DEFAULT
        
        self.lblSmallTitle?.textColor = tStyle.smallTitleColor
        self.lblSmallTitle?.font = tStyle.smallTitleFont ?? FONT_DEFAULT
        
        self.coverTextView?.backgroundColor = tStyle.bgCoverTextColor
        self.coverTextViewTEO?.backgroundColor = tStyle.bgCoverTextColor
        
        self.lblDescription?.textColor = tStyle.descriptionColor
        self.lblDescription?.font = tStyle.descriptionFont ?? FONT_DEFAULT
        
        self.lblDescriptionTEO?.textColor = tStyle.descriptionColor
        self.lblDescriptionTEO?.font = tStyle.descriptionFont ?? FONT_DEFAULT
        
        self.lblExpiredDateTEO?.textColor = tStyle.expiredDateColor
        self.lblExpiredDateTEO?.font = tStyle.expiredDateFont ?? FONT_DEFAULT
        
        self.lblDiscount?.textColor =  dealType == .LOYALTY ?  UIColor("rgb 47 72 88") : UIColor.white
        self.coverDiscount?.backgroundColor = dealType == .LOYALTY ? UIColor("rgb 255 193 6") :  UIColor("rgb 228 109 87")
        self.lblDiscount?.font = tStyle.discountFont ?? FONT_DEFAULT
        self.coverFooterView?.backgroundColor = dealType == .LOYALTY ? UIColor("rgb 47 72 88") : UIColor("rgb 51 51 51")
        
        self.whiteLine1?.isHidden = dealType != .BLACK
        self.whiteLine2?.isHidden = dealType != .BLACK
        
        self.lineThumbnail1?.image = tStyle.lineImage
        self.lineThumbnail2?.image = tStyle.lineImage
        
        self.bgDot?.isHidden = tStyle.isHideDot
        
        self.lblExpiredDateTEO?.textColor = tStyle.expiredDateColor
        self.lblExpiredDateTEO?.font = tStyle.expiredDateFont ?? FONT_DEFAULT
        
//        if dealType == .TEO {
//            self.coverTextViewTEO?.isHidden = false
//            self.coverTextView?.isHidden = true
//        } else {
            self.coverTextViewTEO?.isHidden = true
            self.coverTextView?.isHidden = false
        //}
        
        if dealType == .BLACK || dealType == .TEO || dealType == .LOYALTY || dealType == .BASIC {
            self.heightLine2Constraint?.constant = 0
        } else {
            if cardSize == .BIG {
                self.heightLine2Constraint?.constant = 6
            } else {
                self.heightLine2Constraint?.constant = 3
            } 
        }
    }
    
    override func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }
    
}

class BigDealCard: DealCardFront {
    func loadTheme(_ theme: TEMPLATE_CARD_DEAL = .ONE) -> BigDealCard {
        let aView = Bundle.main.loadNibNamed("DealCardFront", owner: self, options: nil)?[theme.templateIndex] as! BigDealCard
        
        return aView
    }
}

class SmallDealCard: DealCardFront {
    func loadTheme(_ theme: TEMPLATE_CARD_DEAL = .ONE) -> SmallDealCard {
        let aView = Bundle.main.loadNibNamed("SmallDealCard", owner: self, options: nil)?[theme.templateIndex] as! SmallDealCard
        
        return aView
    }
}
