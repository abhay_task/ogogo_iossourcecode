//
//  DealCardBack.swift
//  TGCMember
//
//  Created by vang on 8/9/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import WebKit
import GoogleMaps

class DealCardBack: BaseCardBack {
    
    private var data: [String: Any?]?
    private var onClickedBackCallback: DataCallback?
    private var onClickedNextCallback: DataCallback?
    private var onClickedChatCallback: DataCallback?
    
    private let selectedMarker = GMarker().loadSelectedMarker()
    
    @IBOutlet weak var topAddressConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgImage: UIImageView!
    
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var imageRating: UIImageView!
    @IBOutlet weak var lblShortDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnPhoneNumber: UIButton!
    @IBOutlet weak var btnWebsite: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var coverCate: UIView!
    
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var wkWebview: WKWebView!
    
    @IBOutlet weak var coverTime: UIView!
    @IBOutlet weak var heightTimeConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var coverPhone: UIView!
    @IBOutlet weak var heightPhoneConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var coverWebsite: UIView!
    @IBOutlet weak var heightWebsiteConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomAddressConstraint: NSLayoutConstraint!
    
    
    func loadView() -> DealCardBack {
        let aView = Bundle.main.loadNibNamed("DealCardBack", owner: self, options: nil)?[0] as! DealCardBack
        
        return aView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.wkWebview?.uiDelegate = self
        self.wkWebview?.navigationDelegate = self
        self.wkWebview?.scrollView.showsVerticalScrollIndicator = false
        self.wkWebview?.scrollView.showsHorizontalScrollIndicator = false
        
        decorate()
    }
    
    private func decorate() {
        self.coverCate?.isHidden = true
        
        bodyView?.layer.cornerRadius = 10.0
        bodyView?.layer.masksToBounds = true
        
        self.bgImage?.layer.cornerRadius = 25.0
        contentView?.layer.cornerRadius = 25.0
        contentView?.layer.applySketchShadow(color: UIColor(92, 92, 92), alpha: 0.5, x: 2, y: 2, blur: 4, spread: 0)
        
        self.coverCate?.layer.cornerRadius = 20.0
        self.coverCate?.layer.borderColor =  UIColor.white.cgColor
        self.coverCate?.layer.borderWidth = 1.0
        self.coverCate?.layer.masksToBounds = true
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.onClickedBackCallback?(self.data)
    }
    
    @IBAction func onClickedNext(_ sender: Any) {
        self.onClickedNextCallback?(self.data)
    }
    
    @IBAction func onClickedWebsite(_ sender: Any) {
        if let tData = self.data, let summary = tData["VendorSummary"] as? [String: Any?], let siteUrl = summary["SiteURL"] as? String {
            Utils.openURL(siteUrl)
        }
    }
    
    @IBAction func onClickedChat(_ sender: Any) {
        self.onClickedChatCallback?(self.data)
    }
    
    @IBAction func onClickedNumberPhone(_ sender: Any) {
        if let tData = self.data, let summary = tData["VendorSummary"] as? [String: Any?] {
            let number = Utils.getPhoneNumber(summary).replacingOccurrences(of: " ", with: "")
            Utils.callNumber(number)
        }
    }
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let tPoint = CGPoint(x: point.x, y: point.y - 0.05)
        
        let newPoint = mapView.projection.coordinate(for: tPoint)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 13.0)
        
        mapView.animate(with: camera)
    }
    
    private func showPositonDeal(_ pLocation:  [String: Any?]?) {
        var lat: Double?
        var lng: Double?
 
        if let location = pLocation {
            if location["lat"] is String {
                lat = Double(location["lat"] as? String ?? "0")
                lng = Double(location["lng"] as? String ?? "0")
            } else {
                lat = location["lat"] as? Double
                lng = location["lng"] as? Double
            }
        }
        
        let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
        
        let marker = GMSMarker()
        marker.position = position
        
        if let tData = self.data {
            let nameBadgeImage = "badge_\(tData["BadgeColor"] as? String ?? "red")"
            let namePinSelectImage = "g_selected_\(tData["PingIcon"] as? String ?? "basic")"
            let shortBadge = (tData["ShortLabelTxt"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            
            selectedMarker.thumbnail.image = nil
            selectedMarker.gBigIcon?.image = UIImage(named: namePinSelectImage)
            selectedMarker.lblBadge?.text = shortBadge
            selectedMarker.badgeImage?.image = UIImage(named: nameBadgeImage)
//            selectedMarker.dealName?.text = tData["Name"] as? String ?? ""
            selectedMarker.coverBadge?.isHidden = shortBadge == ""
            
//            if let summary = tData["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let tCategoryName = category["Name"] as? String {
//                selectedMarker.categoryName?.text = tCategoryName
//            } else {
//                selectedMarker.categoryName?.text = ""
//            }
            
            marker.icon = Utils.imageWithView(view: selectedMarker)
            
            if let summary = tData["VendorSummary"] as? [String: Any?], let photoURL = summary["LogoUrl"] as? String {
                selectedMarker.thumbnail.sd_setImage(with:  URL(string: photoURL), completed: {  [weak self]  (_, _, _, _) in
                    guard let self = self else {return}
                    marker.iconView = nil
                    marker.icon = Utils.imageWithView(view: self.selectedMarker)
                })
            }
        }
        
        marker.zIndex = 9999
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.6)
        
        marker.map = mapView
        
        self.animateToPoint(position)
        
    }
    
    private func fillData() {
        
        if let tData = self.data {
            let dealShowType = Utils.getDealType(tData)
            
            if dealShowType == .BLACK {
                self.bgImage?.image = UIImage(named: "black_deal_bg")
            } else if dealShowType == .SILVER {
                self.bgImage?.image = UIImage(named: "silver_deal_bg")
            } else if dealShowType == .GOLD {
                self.bgImage?.image = UIImage(named: "gold_deal_bg")
            } else if dealShowType == .BASIC {
                self.bgImage?.image = UIImage(named: "basic_deal_bg")
            } else if dealShowType == .TEO {
                self.bgImage?.image = UIImage(named: "teo_deal_bg")
            } else if dealShowType == .LOYALTY {
                self.bgImage?.image = UIImage(named: "loyalty_deal_bg")
            }
            
            let content = tData["ContentDetail"] as? String ?? ""
            
            let urlPath = Bundle.main.url(forResource: "\(Constants.DEFAULT_FONT)", withExtension: "ttf")
            
            let htmlCode = String(format: Constants.CONTENT_WEB_FORMAT, content)
            self.wkWebview?.loadHTMLString(htmlCode, baseURL: urlPath)
            
            let summary = tData["VendorSummary"] as? [String: Any?] ??  [String: Any?]()
            let category = summary["Category"] as? [String: Any?] ??  [String: Any?]()
            
            var tVendorName = summary["Name"] as? String ?? " "
            var tCateName = category["Name"] as? String ?? " "
            tVendorName = tVendorName.capitalized
            tCateName = tCateName.uppercased()
            
            self.lblVendorName.text = tVendorName
            self.lblCategoryName.text = tCateName
            
            self.lblShortDescription.setAttribute(summary["Description"] as? String ?? " ", color: .black, font: UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 14), spacing: 3)
            
            let addressText = Utils.getFullAddress(summary)//(summary["Address"] as? String ?? " ").trimmingCharacters(in: .whitespacesAndNewlines)
            let widthAddress = addressText.width(withConstrainedHeight: 30, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14) ?? UIFont.systemFont(ofSize: 14))
            if widthAddress < 226 {
                self.bottomAddressConstraint?.constant = -3
                self.topAddressConstraint?.constant = -10
            } else {
                self.bottomAddressConstraint?.constant = 5
                self.topAddressConstraint?.constant = 0
            }
//            print("addressText width -- > ", " \(addressText) --- > \(addressText.width(withConstrainedHeight: 30, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14) ?? UIFont.systemFont(ofSize: 14)))")
            self.lblAddress.text = addressText
            
            let cateURL = category["ImageUrlSelected"] as? String ?? ""
            self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
                
            })
            
            
            let tLocation = summary["Location"] as? [String: Any]
            self.showPositonDeal(tLocation)
            
            if let fromAt = tData["FromAtVendor"] as? String, let toAt = tData["ToAtVendor"] as? String, fromAt != "", toAt != "" {
                coverTime?.isHidden = false
                heightTimeConstraint?.constant = 25
                
                self.lblTime?.text = "\(fromAt) \(LOCALIZED.txt_to_title.translate.lowercased()) \(toAt)"
            } else {
                coverTime?.isHidden = true
                heightTimeConstraint?.constant = 0
            }
            
            let phoneNumber = Utils.getPhoneNumber(summary)
            
            if phoneNumber != "" {
                coverPhone?.isHidden = false
                heightPhoneConstraint?.constant = 25
                
                let number = "    \(phoneNumber)"
                self.btnPhoneNumber.setTitle(number, for: .normal)
            } else {
                coverPhone?.isHidden = false
                heightPhoneConstraint?.constant = 25
                
                self.btnPhoneNumber.setTitle("   123", for: .normal)
            }
            
            if let tSite = summary["SiteURL"] as? String, tSite != "" {
                coverWebsite?.isHidden = false
                heightWebsiteConstraint?.constant = 25
                
                let siteUrl = "    \(tSite)"
                self.btnWebsite.setTitle(siteUrl, for: .normal)
            } else {
                coverWebsite?.isHidden = true
                heightWebsiteConstraint?.constant = 0
            }
            
            
        }
        
    }
    
    
    override func config(_ data: [String: Any?]?, onClickedBackCallback _onClickedBackCallback: DataCallback? = nil, onClickedNextCallback _onClickedNextCallback: DataCallback? = nil, onClickedChatCallback _onClickedChatCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedBackCallback = _onClickedBackCallback
        self.onClickedNextCallback = _onClickedNextCallback
        self.onClickedChatCallback = _onClickedChatCallback
        
        self.fillData()
    }
    
    
}

extension DealCardBack:  WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    // self.heightWebviewConstraint.constant = height as! CGFloat
                    
                    self.contentView.isHidden = false
                    
                    Utils.dismissLoading()
                })
            }
            
        })
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            let tLink = navigationAction.request.url?.absoluteString
            
            if let link = tLink {
                Utils.openURL(link)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    
    
}
