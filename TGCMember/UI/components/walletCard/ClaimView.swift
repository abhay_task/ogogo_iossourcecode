//
//  ClaimView.swift
//  TGCMember
//
//  Created by vang on 8/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ClaimView: UIView {

    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var coverInput: UIView!
    @IBOutlet weak var viewContent: UIView!
    private var data: [String: Any?]?
    private var onClickedConfirmCallback: DataCallback?
    private var onClickedCancelCallback: DataCallback?
    private var onCompletedCallback: SuccessedDataCallback?
    
    func loadView() -> ClaimView {
        let aView = Bundle.main.loadNibNamed("ClaimView", owner: self, options: nil)?[0] as! ClaimView
        
        return aView
    }
    
    private func dismissKeyboard() {
        self.tfCode.resignFirstResponder()
    }
    
    @IBAction func onClickedConfirm(_ sender: Any) {
        self.onClickedConfirmCallback?(self.data)
        
        self.dismissKeyboard()
        
        Utils.showLoading()
        setTimeout({
            self.doClaim()
        }, 500)

    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.onClickedCancelCallback?(self.data)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        decorate()
    }
    
    private func decorate() {
        self.coverInput.backgroundColor = .clear
        
        self.viewContent.layer.cornerRadius = 5.0
        self.viewContent.layer.masksToBounds = true
        
        self.coverInput.layer.cornerRadius = 5
        self.coverInput.layer.masksToBounds = true
        self.coverInput.layer.borderWidth = 0.5
        self.coverInput.layer.borderColor = UIColor("rgb 205 207 214").cgColor
        
        self.btnConfirm?.layer.cornerRadius = 3.0
        self.btnConfirm.setTitle(LOCALIZED.use_deal.translate.uppercased(), for: .normal)
        
        
        self.updateBtnConfirm(false)
        
        self.btnCancel.setTitleUnderline(LOCALIZED.txt_cancel_title.translate, color: UIColor("rgb 228 109 87"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13))
    }
    
    private func updateBtnConfirm(_ enable: Bool = true) {
        self.btnConfirm.isEnabled = enable
        self.btnConfirm.backgroundColor = enable ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        
        if enable {
            self.btnConfirm.layer.applySketchShadow()
        } else {
            self.btnConfirm.layer.removeSketchShadow()
        }
        
    }
    
    private func fillData() {
        if let tData = self.data {
            var promptText = LOCALIZED.please_ask_for_their_OGOGO_code.translate
            var vendorName = ""
            
            if let summary = tData["VendorSummary"] as? [String: Any?] {
                vendorName = summary["Name"] as? String ?? ""
            }
            
            promptText = promptText.replacingOccurrences(of: "[%__TAG_NAME__%]", with: vendorName)
            self.lblPrompt.setAttribute(promptText, color: UIColor("rgb 255 193 6"), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 20), spacing: 3)
            self.lblPrompt.textAlignment = .center
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedConfirmCallback _onClickedConfirmCallback: DataCallback? = nil, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil, onCompletedCallback _onCompletedCallback: SuccessedDataCallback? = nil) -> Void {
        self.data = data
        
        self.onClickedConfirmCallback = _onClickedConfirmCallback
        self.onClickedCancelCallback = _onClickedCancelCallback
        self.onCompletedCallback = _onCompletedCallback
        
        fillData()
    }
    
    private func doClaim() {
        Utils.showLoading()
        
        let vendorCode = self.tfCode.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        var tParams = [String: Any]()
        tParams["code"] = vendorCode
        
        if let tData = self.data {
            let instanceDealID = tData["InstanceDealID"] as? String ?? ""
            tParams["instance_deal_id"] = instanceDealID
        }
        
        APICommonServices.doUseDeal(tParams) { [weak self] (resp) in
            guard let self = self else {return}
            Utils.dismissLoading()
            
            mPrint("claimADeal", resp)
            if let resp = resp, let status = resp["status"] as? Bool {
                var tDict = [String: Any?]()
                if let data = resp["data"] as? [String: Any?] {
                    NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                    tDict = data
                }
                
                Utils.showResultUseDealAlert(status, self.data) { [weak self] (success, pData) in
                    guard let self = self else {return}
                    
                     self.onCompletedCallback?(status, pData)
                }
            } else {
                Utils.showResultUseDealAlert(false, self.data) { [weak self] (success, pData) in
                    guard let self = self else {return}
                    
                    self.onCompletedCallback?(false, pData)
                }
            }
        }
        
    }

}

extension ClaimView: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            if updatedText.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                self.updateBtnConfirm(true)
            } else {
                self.updateBtnConfirm(false)
            }
            
        }
        
        return true
    }
    
    
}
