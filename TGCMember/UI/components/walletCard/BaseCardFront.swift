//
//  BaseCardFront.swift
//  TGCMember
//
//  Created by vang on 12/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class BaseCardFront: UIView {
    @IBOutlet weak var coverMask: UIView!
    @IBOutlet weak var maskAlpha: UIView!
    @IBOutlet weak var maskImage: UIImageView!
    
    open func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        
    }

    public func addAnimationClaim(_ completed: FinishedCallback? = nil) {
           
           self.maskImage?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
           UIView.animate(withDuration: 1.0,
                          delay: 0,
                          usingSpringWithDamping: 0.3,
                          initialSpringVelocity: 5.0,
                          options: .allowUserInteraction,
                          animations: { [weak self] in
                            guard let self = self else {return}
                               self.maskImage?.transform = .identity
                               self.maskImage?.alpha = 1
                               self.coverMask?.alpha = 1
               },
                          completion: { [weak self] (finished) in
                               guard let self = self else {return}
                               self.maskImage?.alpha = 1
                               self.coverMask?.alpha = 1
                               completed?()
               })
           
       }
}
