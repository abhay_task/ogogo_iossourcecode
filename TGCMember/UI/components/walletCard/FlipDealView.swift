//
//  FlipDealView.swift
//  TGCMember
//
//  Created by vang on 8/9/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
public enum SCREEN_CARD: Int {
    case FRONT
    case MIDDLE
    case BACK
}

class FlipDealView: BaseView {
    func updateLanguages() {
        
    }
    
    
    private var data: [String: Any?]?
    private var onClickedCardCallback: DataCallback?
    
    private var frontView: BaseCardFront?
    private var middleView: BaseCardMiddle?
    private var backView: BaseCardBack?
    
    
    @IBOutlet weak var cardView: UIView!
    
    func loadView() -> FlipDealView {
        let flipDealView = Bundle.main.loadNibNamed("FlipDealView", owner: self, options: nil)?[0] as! FlipDealView
        
        return flipDealView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func getViewFromScreen(_ screen: SCREEN_CARD) -> UIView? {
        if screen == .FRONT {
            return self.frontView
        } else if screen == .MIDDLE {
            return self.middleView
        }
        
        return self.backView
    }
    
    func flip(fromScreen: SCREEN_CARD, toScreen: SCREEN_CARD, isBack: Bool = false, completed: FinishedCallback? = nil) {
        
        let fromView = self.getViewFromScreen(fromScreen)
        let toView = self.getViewFromScreen(toScreen)
        
        UIView.transition(from: fromView ?? UIView(), to: toView ?? UIView(), duration: 0.5, options: isBack ?  UIView.AnimationOptions.transitionFlipFromRight : UIView.AnimationOptions.transitionFlipFromLeft, completion: { _ in
            completed?()
        })
        
        fromView?.isHidden = true
        toView?.isHidden = false
        
    }
    
    public func addAnimationClaim(forFront:Bool = false, forBack:Bool = false, completed: FinishedCallback? = nil) {
        if forFront {
            self.frontView?.addAnimationClaim(completed)
        } else if forBack {
            
        }
    }

    
    private func fillData() {
        if let tData = self.data {
            //mPrint("flip deal data -> ", self.data)
            var disableMiddle: Bool = true
            var disableBack: Bool = false
            
            if let _ = tData["LongDescription"] as? String {
                disableMiddle = false
            }
            
            var typeTpl = Utils.getTemplateDeal(tData)
            let dealType = Utils.getDealType(tData)
            
            if dealType == .LOYALTY {
                disableMiddle = true
                disableBack = true
                typeTpl = .ONE
            }
            
            self.frontView = BigDealCard().loadTheme(typeTpl)
            
            if disableMiddle {
                self.middleView = nil
            } else {
                self.middleView = DealCardMiddle().loadView()
            }
            
            if disableBack {
                self.backView = nil
            } else {
                self.backView = DealCardBack().loadView()
            }
            
            self.frontView?.config(tData, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                    if disableMiddle && disableBack {
                        // Do nothing
                        self.onClickedCardCallback?(data)
                    } else {
                        if disableMiddle {
                            self.flip(fromScreen: .FRONT, toScreen: .BACK)
                        } else {
                            self.flip(fromScreen: .FRONT, toScreen: .MIDDLE)
                        }
                    }
                })
            
            self.middleView?.config(tData, onClickedBackCallback: { [weak self] (data) in
                guard let self = self else {return}
                self.flip(fromScreen: .MIDDLE, toScreen: .FRONT, isBack: true)
                
                }, onClickedNextCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    self.flip(fromScreen: .MIDDLE, toScreen: .BACK)
                })
            
            self.backView?.config(tData, onClickedBackCallback: { [weak self] (data) in
                guard let self = self else {return}
                if disableMiddle {
                    self.flip(fromScreen: .BACK, toScreen: .FRONT, isBack: true)
                } else {
                    self.flip(fromScreen: .BACK, toScreen: .MIDDLE, isBack: true)
                }
                
                
                }, onClickedNextCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.flip(fromScreen: .BACK, toScreen: .FRONT)
                    
                })
            
            if let _firstView = self.frontView {
                _firstView.frame = self.cardView.bounds
                self.cardView.addSubview(_firstView)
                _firstView.isHidden = false
            }
            
            if let _secondView = self.middleView {
                _secondView.frame = self.cardView.bounds
                self.cardView.addSubview(_secondView)
                _secondView.isHidden = true
            }
            
            if let _thirdView = self.backView {
                _thirdView.frame = self.cardView.bounds
                self.addSubview(_thirdView)
                _thirdView.isHidden = true
            }
            
        }
        
    }
    
    public func config(_ data: [String: Any?]?, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil) -> Void {
        self.data = data
    
        self.onClickedCardCallback = _onClickedCardCallback
        
        self.fillData()
    }
    
    
}
