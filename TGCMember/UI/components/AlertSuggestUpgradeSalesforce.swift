//
//  AlertSuggestUpgradeSalesforce.swift
//  TGCMember
//
//  Created by vang on 10/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

public enum CONFIRM_UPGRADE_SALESFORCE: String {
    case NO = "no"
    case YES = "yes"
    case ASK_ME_LATER = "later"
    case TGC_DISTRIBUTOR = "distributor"
}

class AlertSuggestUpgradeSalesforce: UIView {

    private var data: [String: Any?]?
    
    private var onClickedCloseCallback: DataCallback?
    private var onClickedNoCallback: DataCallback?
    private var onClickedYesCallback: DataCallback?
    private var onClickedTGCDistributorCallback: DataCallback?
    private var onClickedLaterCallback: DataCallback?
    
    @IBOutlet weak var lblPrompt1: UILabel!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnLater: UIButton!
    @IBOutlet weak var btnDistributor: UIButton!
    @IBOutlet weak var coverView: UIView!

    override func awakeFromNib() {
       super.awakeFromNib()
       // Initialization code
       
       self.decorate()
    }

    private func decorate() {
        coverView?.layer.cornerRadius = 3.0
        coverView?.layer.masksToBounds = true
       
        self.btnDistributor?.setTitleUnderline(LOCALIZED.tgc_distributor.translate, color: UIColor(228, 109, 87), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 15))
        
        self.btnNo?.backgroundColor = .white
        self.btnNo?.layer.cornerRadius = 3.0
        self.btnNo?.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.4, x: 0, y: 0, blur: 3, spread: 0)
        
        self.btnYes?.backgroundColor = UIColor(255, 193, 6)
        self.btnYes?.layer.cornerRadius = 3.0
        self.btnYes?.layer.applySketchShadow(color: UIColor(203, 203, 203), alpha: 0.33, x: 0, y: 0, blur: 6, spread: 0)
       
        self.btnLater?.setTitleUnderline(LOCALIZED.ask_me_later.translate, color: UIColor(228, 109, 87), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 15))
        
        self.btnNo?.setTitle(LOCALIZED.txt_no_title.translate, for: .normal)
        self.btnYes?.setTitle(LOCALIZED.txt_yes_title.translate, for: .normal)

        
    }

    @IBAction func onClickedNo(_ sender: Any) {
       self.onClickedNoCallback?(self.data)
    }

    @IBAction func onClickedYes(_ sender: Any) {
       self.onClickedYesCallback?(self.data)
    }
    
    @IBAction func onClickedDistributor(_ sender: Any) {
        self.onClickedTGCDistributorCallback?(self.data)
    }

    @IBAction func onClickedLater(_ sender: Any) {
        self.onClickedLaterCallback?(self.data)
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
        self.onClickedCloseCallback?(self.data)
    }

    override init(frame: CGRect) {
       super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }


    func loadView() -> AlertSuggestUpgradeSalesforce {
       let aView = Bundle.main.loadNibNamed("AlertSuggestUpgradeSalesforce", owner: self, options: nil)?[0] as! AlertSuggestUpgradeSalesforce
       
       return aView
    }

    private func fillData() {
        if let tData = self.data {
            let inWhere = tData["in_where"] as? String ?? ""
            
            var fullPrompt1 = LOCALIZED.there_is_no_distributor_in.translate
            fullPrompt1 = fullPrompt1.replacingOccurrences(of: "[%__TAG_NAME__%]", with: inWhere)
            
            var mMutableString = NSMutableAttributedString()
            
            mMutableString = NSMutableAttributedString(string: fullPrompt1, attributes: [NSAttributedString.Key.font:UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14.0)!])
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 4
            paragraphStyle.alignment = .center
            mMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, mMutableString.length))
        
            if let tRange = fullPrompt1.range(of: inWhere) {
                let nsRange = fullPrompt1.nsRange(from: tRange)
                
                mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(39, 39, 39), range:nsRange)
                mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 14.0)!, range:nsRange)
            }
     
            
            self.lblPrompt1.attributedText = mMutableString
        }
    }

    func config(_ data: [String: Any?]?, onClickedYesCallback _onClickedYesCallback: DataCallback? = nil, onClickedNoCallback _onClickedNoCallback: DataCallback? = nil, onClickedTGCDistributorCallback _onClickedTGCDistributorCallback: DataCallback? = nil, onClickedLaterCallback _onClickedLaterCallback: DataCallback? = nil, onClickedCloseCallback _onClickedCloseCallback: DataCallback? = nil) -> Void {
        
        self.data = data
       
        self.onClickedYesCallback = _onClickedYesCallback
        self.onClickedNoCallback = _onClickedNoCallback
        self.onClickedTGCDistributorCallback = _onClickedTGCDistributorCallback
        self.onClickedLaterCallback = _onClickedLaterCallback
        self.onClickedCloseCallback = _onClickedCloseCallback
       
        self.fillData()
    }

}
