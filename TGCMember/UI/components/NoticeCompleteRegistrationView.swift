//
//  NoticeCompleteRegistrationView.swift
//  TGCMember
//
//  Created by vang on 7/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NoticeCompleteRegistrationView: UIView {

    @IBOutlet weak var lblSystemHasRecorded: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblYouCan: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    private var data: [String: Any?]?
    private var onClickedCancelCallback: DataCallback?
    private var onClickedCompleteCallback: DataCallback?
    private var onClickedCloseCallback: DataCallback?
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnComplete: UIButton!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    private func decorate() {
        coverView?.layer.cornerRadius = 3.0
        coverView?.layer.masksToBounds = true
        
        self.btnComplete?.layer.cornerRadius = 3.0
        self.btnComplete?.layer.applySketchShadow()
        
        self.btnContinue?.layer.cornerRadius = 3.0
        self.btnContinue?.layer.applySketchShadow()
        
        self.btnCancel?.setTitleUnderline(LOCALIZED.txt_cancel_title.translate, color: UIColor(228, 109, 87), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 16))
        
        self.lblTitle?.text = LOCALIZED.txt_notice_title.translate
        self.lblYouCan?.setAttribute(LOCALIZED.you_can_complete_your_registration.translate, color: UIColor(39, 39, 39), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 4)
        self.lblYouCan?.textAlignment = .center
        
        self.lblSystemHasRecorded?.setAttribute(LOCALIZED.the_system_has_recorded_your_report.translate, color: UIColor(39, 39, 39), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 4)
        self.lblSystemHasRecorded?.textAlignment = .center
        
        self.btnComplete?.setTitle(LOCALIZED.txt_complete_registration.translate, for: .normal)
        self.btnContinue?.setTitle(LOCALIZED.text_continue.translate, for: .normal)
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.onClickedCancelCallback?(self.data)
    }
    
    @IBAction func onClickedComplete(_ sender: Any) {
        self.onClickedCompleteCallback?(self.data)
    }

    
    @IBAction func onClickedClose(_ sender: Any) {
        self.onClickedCloseCallback?(self.data)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> NoticeCompleteRegistrationView {
        let noticeView = Bundle.main.loadNibNamed("NoticeCompleteRegistrationView", owner: self, options: nil)?[0] as! NoticeCompleteRegistrationView
        
        return noticeView
    }
    
    func loadReportSuccessView() -> NoticeCompleteRegistrationView {
        let noticeView = Bundle.main.loadNibNamed("NoticeCompleteRegistrationView", owner: self, options: nil)?[1] as! NoticeCompleteRegistrationView
        
        return noticeView
    }
    
    private func fillData() {
        
    }
    
    func config(_ data: [String: Any?]?, onClickedCompleteCallback _onClickedCompleteCallback: DataCallback? = nil, onClickedCancelCallback _onClickedCancelCallback: DataCallback? = nil, onClickedCloseCallback _onClickedCloseCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        
        self.onClickedCompleteCallback = _onClickedCompleteCallback
        self.onClickedCancelCallback = _onClickedCancelCallback
        self.onClickedCloseCallback = _onClickedCloseCallback
        
        self.fillData()
    }
}
