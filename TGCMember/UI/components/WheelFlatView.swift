//
//  WheelSliceView.swift
//  TGCMember
//
//  Created by vang on 8/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Foundation

class WheelFlatView: FortuneWheelSliceProtocol {
    public enum Style {
        case one
        case two
        case three
        case four
        case five
        case six
    }
    
    public var title: String
    public var subview: UIView?
    public var degree: CGFloat = 0.0
    
    public var backgroundColor: UIColor? {
        switch style {
        case .one: return UIColor(54, 68, 94)
        case .two: return UIColor(30, 46, 74)
        case .three: return UIColor(20, 30, 45)
        case .four: return UIColor(26, 41, 65)
        case .five: return UIColor(31, 48, 76)
        case .six: return UIColor(154, 22, 24)
        }
    }
    
    public var fontColor: UIColor {
        return UIColor(181, 156, 83)
    }
    
    public var offsetFromExterior:CGFloat {
        return 10.0
    }
    
    public var font: UIFont {
        switch style {
        case .one: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        case .two: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        case .three: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        case .four: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        case .five: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        case .six: return UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!
        }
    }
    
    
    public var style:Style = .one
    
    public init(subview:UIView) {
        self.title = ""
        self.subview = subview
    }
    
    public convenience init(subview:UIView, degree:CGFloat) {
        self.init(subview: subview)
        self.degree = degree
    }

}
