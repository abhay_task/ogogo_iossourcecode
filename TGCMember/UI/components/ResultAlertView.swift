//
//  ResultAlertView.swift
//  TGCMember
//
//  Created by vang on 7/18/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ResultAlertView: UIView {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var lblTitleFailed: UILabel!
    @IBOutlet weak var lblTitleSuccess: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var lblMessageFailed: UILabel!
    @IBOutlet weak var lblMessageSuccess: UILabel!
    @IBOutlet weak var lblFailedContinue: UILabel!
    
    @IBOutlet weak var lblSuccessContinue: UILabel!
    private var data: [String: Any?]?
    private var onClickedButtonCallback: DataCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        decorate()
    }
    
    @IBAction func onClickedBtn(_ sender: Any) {
        self.onClickedButtonCallback?(self.data)
    }
    
    
    private func decorate() {
        coverView?.layer.cornerRadius = 8.0
        coverView?.layer.masksToBounds = true
        
        self.lblFailedContinue?.text = LOCALIZED.text_continue.translate
        self.lblSuccessContinue?.text = LOCALIZED.text_continue.translate
        
        self.lblTitleSuccess?.text = LOCALIZED.success_exclamation.translate
        self.lblTitleFailed?.text = LOCALIZED.title_oops.translate
        
        self.lblMessageFailed?.setAttribute(LOCALIZED.msg_upgrade_failed.translate, color: UIColor(30, 38, 71), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 4)
        self.lblMessageFailed?.textAlignment = .center
        
        self.lblMessageSuccess?.setAttribute(LOCALIZED.your_account_have_been_updated_successfully.translate, color: UIColor(30, 38, 71), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 4)
        self.lblMessageSuccess?.textAlignment = .center
    
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadSuccessView() -> ResultAlertView {
        let successView = Bundle.main.loadNibNamed("ResultAlertView", owner: self, options: nil)?[1] as! ResultAlertView
        
        return successView
    }
    
    func loadFailedView() -> ResultAlertView {
        let failedView = Bundle.main.loadNibNamed("ResultAlertView", owner: self, options: nil)?[0] as! ResultAlertView
        
        return failedView
    }
    
    private func fillData() {
        
    }
    
    func config(_ data: [String: Any?]?, onClickedButtonCallback _onClickedButtonCallback: DataCallback? = nil) -> Void {
        
        self.data = data
        self.onClickedButtonCallback = _onClickedButtonCallback
        
        self.fillData()
    }
    
    
}
