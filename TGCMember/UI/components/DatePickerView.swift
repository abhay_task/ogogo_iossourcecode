//
//  DatePickerView.swift
//  TGCMember
//
//  Created by vang on 10/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum PICKER_TYPE {
    case FROM
    case TO
}

class DatePickerView: UIViewController {

    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var minimumDate: String? = nil
    var maximumDate: String? = nil
    var titleText: String = ""
    var selectedDate: String? = nil
    
    var callback: DataCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        
        setRangeDate()
    }
    
    private func decorate() {
        self.displayMask(true)
        
//        self.btnCancel.setTitle(LOCALIZED.txt_cancel_title.translate, for: .normal)
        self.btnCancel.setTitle(titleText, for: .normal)
        self.btnConfirm.setTitle(LOCALIZED.txt_confirm_title.translate, for: .normal)
        
        self.datePicker.locale = Locale(identifier: GLOBAL.GlobalVariables.currentLanguage.rawValue)
        self.datePicker.calendar = Calendar(identifier: .gregorian)
        
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
    }
    
    private func displayMask(_ show: Bool = true) {
        if show {
            self.maskView.alpha = 0
            
            UIView.animate(withDuration: 1, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.maskView.alpha = 0.1
            }, completion: { (finished: Bool) in
                self.maskView.alpha = 0.1
            })
        } else {
            UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions() , animations: {
                       self.maskView.alpha = 0
                   }, completion: { (finished: Bool) in
                       self.maskView.alpha = 0
            })
        }
    }
    
    private func setRangeDate() {
        if let tDate = self.minimumDate, tDate != "" {
            let nsDate = Utils.dateFromStringDate(tDate, fromFormat: "yyyy-MM-dd")
            self.datePicker.minimumDate = nsDate
          
        }
        
        if let tDate = self.maximumDate, tDate != "" {
            let nsDate = Utils.dateFromStringDate(tDate, fromFormat: "yyyy-MM-dd")
            self.datePicker.maximumDate = nsDate
        
        } 
        
        if let sDate = self.selectedDate, sDate != ""{
            let nsDate = Utils.dateFromStringDate(sDate, fromFormat: "yyyy-MM-dd")
            
            self.datePicker.date = nsDate ?? Date()
        }
    }

    @IBAction func onClickedConfirm(_ sender: Any) {
        self.displayMask(false)
        self.callback?(["Date": self.datePicker.date.toString(format: "yyyy-MM-dd")])
        self.dismissFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.displayMask(false)
        
        self.dismissFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
   

}
