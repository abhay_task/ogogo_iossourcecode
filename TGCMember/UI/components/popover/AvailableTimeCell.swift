//
//  AvailableTimeCell.swift
//  TGCMember
//
//  Created by vang on 5/30/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class AvailableTimeCell: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    private var data: [String: Any?]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func fillData() {
        if let tData = self.data {
            self.lblDay.text = "\(tData["Day"] as? String ?? "")".upperCaseFirstLetter()
            self.lblTime.text = "\(tData["StartAt"] as? String ?? "") - \(tData["EndAt"] as? String ?? "")"
        } else {
            self.lblTime.text = ""
        }
        
    }
    
    public func config(_ data: [String: Any?]?) -> Void {
        self.data = data

        self.fillData()
    }
}
