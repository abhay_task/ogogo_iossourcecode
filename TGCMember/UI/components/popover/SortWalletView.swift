//
//  AvailableTimeView.swift
//  TGCMember
//
//  Created by vang on 5/30/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class SortWalletView: UIViewController {

    @IBOutlet weak var tbContent: UITableView!
    
    var sortOptions: [[String: Any?]] = []
    var selectedSort:SORT_OPTIONS = SORT_OPTIONS.NONE
    var onChangedSort: DataCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.tbContent.register(UINib(nibName: "SortWalletCell", bundle: nil), forCellReuseIdentifier: "SortWalletCell")
    }
    
    public func updateData() {
        self.tbContent.reloadData()
    }
}


extension SortWalletView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sortOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = sortOptions[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortWalletCell") as? SortWalletCell
        
        cell?.config(data, onClickedCell: { [weak self] (data) in
            guard let self = self else {return}
         
            if let callback = self.onChangedSort {
                callback(data)
            }
        })
        
        return cell!
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    
    
}
