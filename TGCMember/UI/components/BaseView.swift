//
//  BaseView.swift
//  TGCMember
//
//  Created by vang on 10/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

typealias BaseView = MView & LanguageProtocol

class MView: UIView {
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLanguagesImmediately(_:)), name: .UPDATE_LANGUAGES_IMMEDIATELY, object: nil)
        
        setup()
    }
    
    func setup() {
        guard let view = self as? BaseView else {
            return
        }
        
        view.updateLanguages()
    }
    
    @objc func updateLanguagesImmediately(_ notification: Notification) {
        guard let view = self as? BaseView else {
            return
        }
        
        view.updateLanguages()
    }
    
    func openChatDetail(_ data : [String: Any?]?) {
        if let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController() {
            let chatVC = ChatDetailViewController()
            chatVC.data = data
            
            topVC.navigationController?.pushViewController(chatVC, animated: true)
        }
        
    }
    
}
