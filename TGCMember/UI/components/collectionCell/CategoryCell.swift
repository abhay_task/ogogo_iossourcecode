//
//  CategoryCell.swift
//  TGCMember
//
//  Created by vang on 11/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var lblCateName: UILabel!
    @IBOutlet weak var icChecked: UIImageView!
    @IBOutlet weak var coverContent: UIView!
    @IBOutlet weak var loadingImage: UIActivityIndicatorView!
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var iconCate: UIImageView!
    
    @IBOutlet weak var btnCover: UIButton!
    
    @IBAction func onClickedCell(_ sender: Any) {
        Utils.dismissKeyboard()
        
        var unSelected: Bool = false
        if let tData = self.data, let tSelected = GLOBAL.GlobalVariables.selectedCategory, let uID = tSelected["ID"] as? String {
            if uID == tData["ID"] as? String ?? "" {
                 unSelected = true
            }
        }
        
        if unSelected {
            GLOBAL.GlobalVariables.selectedCategory = nil
        } else {
            GLOBAL.GlobalVariables.selectedCategory = self.data
        }
       
        GLOBAL.GlobalVariables.selectedDeal = nil
        self.onClickedCellCallback?(self.data)
        
        NotificationCenter.default.post(name: .ON_SELECTED_CATEGORY_SEARCH, object: GLOBAL.GlobalVariables.selectedCategory)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectedCategory), name: .ON_SELECTED_CATEGORY_SEARCH, object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(onSelectedFromMap), name: .ON_SELECTED_DEAL_ON_MAP, object: nil)
        
        decorate()
        
    }
    
    private func decorate() {
        coverContent?.layer.cornerRadius = 8
        coverContent?.backgroundColor = .white
        coverContent?.layer.applySketchShadow(color: UIColor(120, 121, 147), alpha: 0.2, x: 0, y: 2, blur: 16, spread: 0)
        self.iconCate.contentMode = .scaleAspectFit
        
        
    }
    
    
    @objc func onSelectedFromMap(_ notification: Notification) {
        //mPrint("", notification.object as? [String: Any])
        if let uData = notification.object as? [String: Any], let summary = uData["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let uID = category["ID"] as? String, let tData = self.data {
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                self.displayHighlight()
            } else {
                self.disableHighlight()
            }
        }
    }
    
    @objc func onSelectedCategory(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], let tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                self.displayHighlight()
            } else {
                self.disableHighlight()
            }
        } else {
            self.disableHighlight()
        }
    }
    
    private func displayHighlight() {
        self.coverContent.layer.borderWidth = 1.2
        self.coverContent.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        
        self.icChecked.isHidden = false
    }
    
    private func disableHighlight() {
        self.icChecked.isHidden = true
        
        self.coverContent.layer.borderWidth = 0
        self.coverContent.layer.borderColor = UIColor.white.cgColor
    }
    
    
    private func fillData() {
        if let tData = self.data  {
            //mPrint("fillData", tData)
            if let tSelected = GLOBAL.GlobalVariables.selectedCategory, let uID = tSelected["ID"] as? String {
                if uID == tData["ID"] as? String ?? "" {
                    self.displayHighlight()
                } else {
                    self.disableHighlight()
                }
            } else {
                self.disableHighlight()
            }
            
            
            self.loadingImage.startAnimating()
            self.iconCate.sd_setImage(with:  URL(string: tData["ImageUrl"] as? String ?? ""), completed: {  [weak self]  (image, error, _, _) in
                guard let self = self else {return}
                
                self.loadingImage.stopAnimating()
            })
            
            self.lblCateName.text = tData["Name"] as? String ?? ""
            
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_CATEGORY_SEARCH, object: nil)
    }
    
}
