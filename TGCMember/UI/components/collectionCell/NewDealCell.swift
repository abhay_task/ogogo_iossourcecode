//
//  NewDealCell.swift
//  TGCMember
//
//  Created by vang on 12/18/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NewDealCell: UICollectionViewCell {
    
    @IBOutlet weak var lblNumberMore: UILabel!
    @IBOutlet weak var moreView: HighlightableView!
    @IBOutlet weak var imageSpecial: UIImageView!
    private var data: [String: Any?]?
    
    private var onClickedCellCallback: DataCallback?
    private var onClickedMoreStoreCallback: DataCallback?
    
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var sweepstakeLbl1: UILabel!
    @IBOutlet weak var sweepstakeLbl2: UILabel!
    @IBOutlet weak var sweepstakeLbl3: UILabel!
    @IBOutlet weak var coverSweepstake: UIView!
    @IBOutlet weak var coverBasic: UIView!
    
    @IBOutlet weak var coverNumber: UIView!
    @IBOutlet weak var coverImageCenter: UIView!
    @IBOutlet weak var maskSwipeDaily: UIView!
    @IBOutlet weak var maskTEO: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var lblTitleLine1: UILabel!
    @IBOutlet weak var lblTitleLine2: UILabel!
    @IBOutlet weak var smallTitle: UILabel!
    
    @IBOutlet weak var coverThumbnail: UIView!
    @IBOutlet weak var coverCate: UIView!
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var lbTag: UILabel!
    @IBOutlet weak var coverLike: UIView!
    
    
    @IBOutlet weak var coverTag: UIView!
    @IBOutlet weak var coverBody: UIView!
    
    @IBOutlet weak var withProgressTEOConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var coverTEOProgress: UIView!
    @IBOutlet weak var lblInvited: UILabel!
    @IBOutlet weak var coverTEO: UIView!
    
    @IBOutlet weak var coverSpecial: UIView!
    @IBOutlet weak var maskSelected: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectedDeal), name: .ON_SELECTED_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onHighlightDeal), name: .ON_HIGHLIGHT_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDisableHighlightDeal), name: .ON_DISABLE_HIGHLIGHT_DEAL_ON_CAROUSEL, object: nil)
        decorate()
    }
    
    private func decorate() {
        self.moreView?.layer.cornerRadius = 8.0
        
        self.coverCate?.isHidden = true
        
        coverTag?.layer.cornerRadius = 0.7
        coverTag?.layer.masksToBounds = true
        
        self.maskSelected.layer.borderWidth = 5.0
        self.maskSelected.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        self.maskSelected.layer.cornerRadius = 10.0
        
        self.coverBody.layer.cornerRadius = 7
        self.coverThumbnail.layer.cornerRadius = 7
        self.coverThumbnail.layer.masksToBounds = true
        
        
        self.maskSwipeDaily.layer.cornerRadius = 7
        self.maskSwipeDaily.layer.masksToBounds = true
        
        self.coverLike.layer.cornerRadius = 10.0
        self.coverLike.layer.masksToBounds = true
        
        self.coverCate.layer.cornerRadius = 11.0
        self.coverCate.layer.borderColor =  UIColor.white.cgColor
        self.coverCate.layer.borderWidth = 1.0
        self.coverCate.layer.masksToBounds = true
        
        self.coverImageCenter.layer.cornerRadius = 26.5
        self.coverImageCenter.layer.masksToBounds = true
        
        self.coverNumber.layer.cornerRadius = 9
        self.coverNumber.layer.masksToBounds = true
        
        self.maskTEO.layer.cornerRadius = 7
        self.maskTEO.layer.masksToBounds = true
        
        self.coverTEO.layer.cornerRadius = 3.0
        self.coverTEO.layer.masksToBounds = true
        
        self.coverTEOProgress.layer.cornerRadius = 3.4
        self.coverTEOProgress.layer.masksToBounds = true
        
        self.progressView.layer.cornerRadius = 3.4
        self.progressView.layer.masksToBounds = true
        
    }
    
    @IBAction func onClickedCell(_ sender: Any) {
        Utils.dismissKeyboard()
        self.onClickedCellCallback?(self.data)
    }
    
    @IBAction func onClickedMoreDeal(_ sender: Any) {
        Utils.dismissKeyboard()
        self.onClickedMoreStoreCallback?(self.data)
    }
    
    @objc func onDisableHighlightDeal(_ notification: Notification) {
        self.disableHighlight()
    }
    
    @objc func onSelectedDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], let _data = self.data {
            let dealShowType = Utils.getDealType(_data)
            
            var tData = _data
            
            if dealShowType == DEAL_TYPE.UNSWIPED_DAILY {
                tData = _data["DealThumb"] as? [String: Any?] ??  [String: Any?]()
            }
            
            let uID = uData["ID"] as? String ?? ""
            let uStoreType = uData["StoreType"] as? String ?? ""
            
            let mID = tData["ID"] as? String ?? ""
            let mStoreType = tData["StoreType"] as? String ?? ""
            
            
            
            var uDealType = uData["Type"] as? String ?? ""
            
            if let activeCodeRequired = uData["ActiveCodeRequired"] as? Bool, activeCodeRequired == true {
                uDealType = DEAL_TYPE.SPECIAL_CARD.rawValue
            }
            
            if (uID == mID && uID != "" && uStoreType == mStoreType) || (uDealType == dealShowType.rawValue && dealShowType == DEAL_TYPE.UNSWIPED_DAILY)  || (uDealType == dealShowType.rawValue && dealShowType == DEAL_TYPE.SPECIAL_CARD) {
                self.displayHighlight()
            } else {
                self.disableHighlight()
            }
        }
    }
    
    @objc func onHighlightDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], let _data = self.data {
            let dealShowType = Utils.getDealType(_data)
            
            var tData = _data
            
            if dealShowType == DEAL_TYPE.UNSWIPED_DAILY {
                tData = _data["DealThumb"] as? [String: Any?] ??  [String: Any?]()
            }
            
            let uID = uData["ID"] as? String ?? ""
            let uStoreType = uData["StoreType"] as? String ?? ""
            
            let mID = tData["ID"] as? String ?? ""
            let mStoreType = tData["StoreType"] as? String ?? ""
            
            if uID == mID && uStoreType == mStoreType {
                self.displayHighlight()
            } else {
                self.disableHighlight()
            }
        }
    }
    
    private func displayHighlight() {
        self.maskSelected.alpha = 1
    }
    
    private func disableHighlight() {
        self.maskSelected.alpha = 0
    }
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_HIGHLIGHT_DEAL_ON_CAROUSEL, object: nil)
    }
    
    private func fillData() {
        if let _data = self.data {
            //let numberMore = _data["NumberOfMultiDeals"] as? Int ?? 0
            let numberMore = 0
            self.lblNumberMore?.text = "\(numberMore) MORE"
            
            let dealShowType = Utils.getDealType(_data)
            
            var tData = _data
            
            self.moreView?.isHidden = numberMore == 0 ? true : false
            
            self.coverBasic.alpha = 0
            self.coverSweepstake.alpha = 0
            self.maskSwipeDaily.alpha = 0
            self.maskTEO.alpha = 0
            self.coverSpecial.alpha = 0
            
            if dealShowType == DEAL_TYPE.UNSWIPED_DAILY {
                self.coverBasic.alpha = 1
                self.maskSwipeDaily.alpha = 1
                
                tData = _data["DealThumb"] as? [String: Any?] ??  [String: Any?]()
                
                self.lblNumber.text = "\(_data["NumberOfDeal"] as? Int ?? 1)"
                
            } else if dealShowType == DEAL_TYPE.SWEEPSTAKE {
                self.coverSweepstake.alpha = 1
                
                self.lblCost.text = tData["LabelTxt"] as? String ?? ""
          
                let title1 = tData["TitleLine1"] as? String ?? ""
                let title2 = tData["TitleLine2"] as? String ?? ""
                self.sweepstakeLbl1.text = title1 == "" ? " " : title1
                self.sweepstakeLbl2.text = title2 == "" ? " " : title2
                
                if let summary = tData["VendorSummary"] as? [String: Any?] {
                    self.sweepstakeLbl3.text = summary["Name"] as? String ?? ""
                } else {
                    self.sweepstakeLbl3.text = ""
                }
            } else if dealShowType == DEAL_TYPE.TEO {
               
                if let assignmentProgram = tData["AssignmentProgram"] as? [String: Any?] {
                    let status = assignmentProgram["Status"] as? String
                    if status == TEO_STATUS.COMPLETED.rawValue {
                        self.maskTEO.alpha = 0
                    } else {
                        self.maskTEO.alpha = 1
                        let numberOfAssignee = tData["NumberOfAssigneeCompleted"] as? Int ?? 0
                        let totalOfAssignee = assignmentProgram["TotalOfRequiredAssignee"] as? Int ?? 1
                        self.lblInvited.text = "\(numberOfAssignee)/\(totalOfAssignee) \(numberOfAssignee > 0 ? "Friends" : "Friend") Invited"
                        
                        self.withProgressTEOConstraint.constant = CGFloat((CGFloat(numberOfAssignee) / CGFloat(totalOfAssignee)) * 93.0)
                    }
                } else {
                    self.maskTEO.alpha = 0
                }
                
                self.coverBasic.alpha = 1
            } else if dealShowType == DEAL_TYPE.SPECIAL_CARD {
                self.coverBasic.alpha = 0
                self.maskSwipeDaily.alpha = 0
                self.coverSweepstake.alpha = 0
                self.maskTEO.alpha = 0
                self.coverSpecial.alpha = 1
                
                self.imageSpecial.sd_setImage(with:  URL(string: tData["Photo"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                    guard let self = self else {return}
                    
                })
            } else {
                self.coverBasic.alpha = 1
                self.maskSwipeDaily.alpha = 0
                self.coverSweepstake.alpha = 0
                self.maskTEO.alpha = 0
            }
            
            if let tSelected = GLOBAL.GlobalVariables.selectedDeal, let uID = tSelected["ID"] as? String {
                let uStoreType = tSelected["StoreType"] as? String ?? ""
                let mStoreType = tData["StoreType"] as? String ?? ""
                
                if uID == tData["ID"] as? String ?? "" && uStoreType == mStoreType {
                    self.displayHighlight()
                } else {
                    self.disableHighlight()
                }
            } else {
                self.disableHighlight()
            }
            
            let title1 = tData["TitleLine1"] as? String ?? ""
            let title2 = tData["TitleLine2"] as? String ?? ""
            self.lblTitleLine1.text = title1 == "" ? " " : title1
            self.lblTitleLine2.text = title2 == "" ? " " : title2
            
            
            let txtDiscount = (tData["LabelTxt"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            self.coverTag.isHidden = txtDiscount == ""
            
            self.lbTag.text = txtDiscount
            
            if let summary = tData["VendorSummary"] as? [String: Any?] {
                self.smallTitle.text = summary["Name"] as? String ?? ""
            } else {
                self.smallTitle.text = ""
            }
            
            let isLike = tData["IsFavorite"] as? Bool ?? false
            self.likeImage.image = UIImage(named: isLike ? "new_like_ic" : "new_unlike_ic")
            
            self.thumbnail.sd_setImage(with:  URL(string: tData["Photo"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
            })
            
            if let summary = tData["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let cateURL = category["ImageUrlSelected"] as? String  {
                self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
                    
                })
            }
            
            
            
            self.decorateWithStyle(dealShowType)
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil, onClickedMoreStoreCallback _onClickedMoreStoreCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        self.onClickedMoreStoreCallback = _onClickedMoreStoreCallback
        
        self.fillData()
    }
    
    
    private func decorateWithStyle(_ type: DEAL_TYPE = .BLACK) {
        if type == .BASIC {
            bgImage.image = UIImage(named: "bg_basic_portrait")
            lblTitleLine1.textColor = UIColor("rgb 47 72 88")
            lblTitleLine2.textColor = UIColor("rgb 47 72 88")
            smallTitle.textColor = UIColor("rgb 47 72 88")
        } else if type == .BLACK {
            bgImage.image = UIImage(named: "bg_black_portrait")
            lblTitleLine1.textColor = .white
            lblTitleLine2.textColor = .white
            smallTitle.textColor = .white
            
        } else if type == .SILVER {
            bgImage.image = UIImage(named: "bg_silver_portrait")
            lblTitleLine1.textColor = UIColor("rgb 47 72 88")
            lblTitleLine2.textColor = UIColor("rgb 47 72 88")
            smallTitle.textColor = UIColor("rgb 47 72 88")
        } else if type == .GOLD {
            bgImage.image = UIImage(named: "bg_gold_portrait")
            lblTitleLine1.textColor = UIColor("rgb 47 72 88")
            lblTitleLine2.textColor = UIColor("rgb 47 72 88")
            smallTitle.textColor = UIColor("rgb 47 72 88")
        } else if type == .GOLD {
            bgImage.image = UIImage(named: "bg_gold_portrait")
            lblTitleLine1.textColor = UIColor("rgb 47 72 88")
            lblTitleLine2.textColor = UIColor("rgb 47 72 88")
            smallTitle.textColor = UIColor("rgb 47 72 88")
        } else if type == .LOYALTY {
            bgImage.image = UIImage(named: "bg_loyalty_portrait")
            lblTitleLine1.textColor = .white
            lblTitleLine2.textColor = .white
            smallTitle.textColor = .white
        } else if type == .TEO {
            bgImage.image = UIImage(named: "bg_ted_portrait")
            lblTitleLine1.textColor = .white
            lblTitleLine2.textColor = .white
            smallTitle.textColor = .white
        } else if type == .UNSWIPED_DAILY {
            bgImage.image = UIImage(named: "bg_silver_portrait")
            lblTitleLine1.textColor = UIColor("rgb 47 72 88")
            lblTitleLine2.textColor = UIColor("rgb 47 72 88")
            smallTitle.textColor = UIColor("rgb 47 72 88")
        }  else if type == .SWEEPSTAKE {
            bgImage.image = UIImage(named: "bg_sweepstake_portrait")
            lblTitleLine1.textColor = .white
            lblTitleLine2.textColor = .white
            smallTitle.textColor = .white
        }
    }
}
