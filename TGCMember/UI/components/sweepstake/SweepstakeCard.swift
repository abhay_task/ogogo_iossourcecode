//
//  SweepstakeCard.swift
//  TGCMember
//
//  Created by Vang Doan on 9/1/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit


enum SweepstakeCardSize: String {
    case BIG = "BigSweepstakeCard"
    case SMALL = "SmallSweepstakeCard"
}


class SweepstakeCard: BaseCardFront {
    
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var vendorImage: UIImageView!
    @IBOutlet weak var lblDatetime: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblVendorName: UILabel!
    private var data: [String: Any?]?
    private var onCompletedCallback: DataCallback?
    private var onClickedCardCallback: DataCallback?
    private var onClickedChatCallback: DataCallback?
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.decorate()
    }
    
    @IBAction func onClickedChat(_ sender: Any) {
        self.onClickedChatCallback?(self.data)
    }
    
    private func decorate() {
        let tCardSize = SweepstakeCardSize(rawValue: String(describing: type(of: self))) ?? .BIG
        
        coverMask?.alpha = 0
        coverMask?.clipsToBounds = true
        coverMask?.layer.cornerRadius = tCardSize == .BIG ? 25.0 : 10.0
        
        self.vendorImage?.layer.cornerRadius = 10.0
        self.vendorImage?.layer.masksToBounds = true
    }
    
    private func fillData() {
        if let tData = self.data {
            let maskCard = tData["maskCard"] as? MASK_CARD ?? MASK_CARD.NONE
            
            if maskCard == .CLAIMED {
                coverMask?.alpha = 1
            } else {
                coverMask?.alpha = 0
            }
            
            var sweepstakePrize = SWEEPSTAKE_PRIZE.NONE
            if let tPrize = tData["Prize"] as? String {
                sweepstakePrize = SWEEPSTAKE_PRIZE(rawValue: tPrize) ?? SWEEPSTAKE_PRIZE.NO_WIN
            } else if let sweepstakeInfo = tData["SweepstakesInfo"] as? [String: Any?] {
                let sPrize = sweepstakeInfo["Prize"] as? String ?? ""
                sweepstakePrize = SWEEPSTAKE_PRIZE(rawValue: sPrize) ?? SWEEPSTAKE_PRIZE.NO_WIN
            }
            
            if sweepstakePrize == .FREE_CARD {
                self.bgImage.image = UIImage(named: "sweepstake_free_deal_bg")
            } else if sweepstakePrize == .NO_WIN {
                self.bgImage.image = UIImage(named: "no_win_card")
            } else {
                self.bgImage.image = UIImage(named: "sweepstake_deal_bg")
            }
            
            let summary = tData["VendorSummary"] as? [String: Any?] ?? [String: Any?]()
            self.vendorImage?.sd_setImage(with: URL(string: summary["LogoUrl"] as? String ?? ""), completed: { (_, _, _, _) in
                
            })
            
            self.lblCost?.text = tData["LabelTxt"] as? String ?? ""
            
            let fromHighlight = "11 Jul 2019"
            let toHighlight = "11 Nov 2019"
            let textExpiry = "From: \(fromHighlight) To: \(toHighlight)"
            self.lblDatetime.setTextWithHighlight(textExpiry, .white, UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 12), highlightText: [fromHighlight, toHighlight], highlightColor: [.white, .white], highlightFont: [UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: 12), UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 12)])
            
            let vendorHighlight = summary["Name"] as? String ?? ""
            let textVendor = "Vendor name: \(vendorHighlight)"
            self.lblVendorName.setTextWithHighlight(textVendor, .white, UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 12), highlightText: [vendorHighlight], highlightColor: [.white], highlightFont: [UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: 12)])
            
            let addHighlight = summary["Address"] as? String ?? ""
            let textAdd = "Address: \(addHighlight)"
            self.lblAddress.setTextWithHighlight(textAdd, .white, UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 12), highlightText: [addHighlight], highlightColor: [.white], highlightFont: [UIFont(name: ROBOTO_SLAB_FONT.BOLD.rawValue, size: 12)])
            
            
            self.lblDescription.setAttribute(tData["ShortDescription"] as? String ?? "", color: .white, font: UIFont(name: ROBOTO_SLAB_FONT.REGULAR.rawValue, size: 12), spacing: 2.5)
            
        }
        
        self.onCompletedCallback?(["instance": self])
        
    }
    
    public func config(_ data: [String: Any?]?, onCompletedCallback _onCompletedCallback: DataCallback? = nil, onClickedCardCallback _onClickedCardCallback: DataCallback? = nil, onClickedChatCallback _onClickedChatCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onCompletedCallback = _onCompletedCallback
        self.onClickedCardCallback = _onClickedCardCallback
        self.onClickedChatCallback = _onClickedChatCallback
        
        self.fillData()
    }
    
}

class BigSweepstakeCard: SweepstakeCard {
    func loadView() -> BigSweepstakeCard {
        let aView = Bundle.main.loadNibNamed("SweepstakeCard", owner: self, options: nil)?[0] as! BigSweepstakeCard
        
        return aView
    }
}

class SmallSweepstakeCard: SweepstakeCard {
    func loadView() -> SmallSweepstakeCard {
        let aView = Bundle.main.loadNibNamed("SmallSweepstakeCard", owner: self, options: nil)?[0] as! SmallSweepstakeCard
        
        return aView
    }
}
