//
//  MLocationManager.swift
//  TGCMember
//
//  Created by vang on 10/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class MLocationManager: NSObject {
    
    var locationManager: CLLocationManager?
    var callbackLocation: DataCallback?
    
    public override init() {
        super.init()
        
    }
    
    func getCurrentLocation(callback: DataCallback? = nil) {
        self.callbackLocation = callback
        if  locationManager == nil {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            
            locationManager?.requestWhenInUseAuthorization()
        }
        
        
        
        locationManager?.startUpdatingLocation()
    }
    
    func stopGetCurrentLocation() {
        locationManager?.stopUpdatingLocation()
        self.callbackLocation = nil
    }
    
}


extension MLocationManager: CLLocationManagerDelegate {
    
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse ||   status == .authorizedAlways {
            // you're good to go!
            manager.startUpdatingLocation()
        } else if status == .denied {
            
        }
    }
    
    
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let _userLocation = locations.last {
            
            if GLOBAL.GlobalVariables.sysConfigModel.isHardCodeLocation {
                GLOBAL.GlobalVariables.latestLat = GLOBAL.GlobalVariables.sysConfigModel.hardLocationLat
                GLOBAL.GlobalVariables.latestLng = GLOBAL.GlobalVariables.sysConfigModel.hardLocationLng
            } else {
                GLOBAL.GlobalVariables.latestLat = _userLocation.coordinate.latitude
                GLOBAL.GlobalVariables.latestLng = _userLocation.coordinate.longitude
            }
            
            self.callbackLocation?(nil)
            
            self.stopGetCurrentLocation()
        }
        
    }
    
}

