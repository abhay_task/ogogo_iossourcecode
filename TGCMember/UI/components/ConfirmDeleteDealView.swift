//
//  ResultUpgradeAlert.swift
//  TGCMember
//
//  Created by vang on 1/10/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class ConfirmDeleteDealView: UIView {
    
    private var onClickedYesCallback: DataCallback?
    private var onClickedNoCallback: DataCallback?
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    
    private var data: [String: Any?]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.decorate()
    }
    
    private func decorate() {
        coverView?.layer.cornerRadius = 5.0
        coverView?.layer.masksToBounds = true
        
        self.btnNo?.layer.cornerRadius = 3.0
        self.btnNo?.layer.applySketchShadow()
        
        self.btnNo?.setTitle(LOCALIZED.no_i_dont_want_to.translate, for: .normal)
        
        self.lblMessage?.setAttribute(LOCALIZED.are_you_sure_you_want_to_delete_the_deal.translate, color: UIColor("rgb 39 39 39"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 2.5)
        self.lblMessage?.textAlignment = .center

        self.btnYes.setTitleUnderline(LOCALIZED.yes_please_proceed.translate, color: UIColor("rgb 228 109 87"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13))
        
    }
    
    @IBAction func onClickedNo(_ sender: Any) {
        self.onClickedNoCallback?(self.data)
    }
    
    @IBAction func onClickedYes(_ sender: Any) {
           self.onClickedYesCallback?(self.data)
    }
   
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func loadView() -> ConfirmDeleteDealView {
        let aView = Bundle.main.loadNibNamed("ConfirmDeleteDealView", owner: self, options: nil)?[0] as! ConfirmDeleteDealView
        
        return aView
    }
    
    private func fillData() {
        if let tData = self.data {
    
            
        }
    }
    
    func config(_ data: [String: Any?]?, onClickedYesCallback _onClickedYesCallback: DataCallback? = nil, onClickedNoCallback _onClickedNoCallback: DataCallback? = nil) -> Void {
        self.data = data
        
        self.onClickedYesCallback = _onClickedYesCallback
        self.onClickedNoCallback = _onClickedNoCallback
        
        self.fillData()
    }
}
