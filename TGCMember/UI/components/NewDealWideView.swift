//
//  NewDealWideView.swift
//  TGCMember
//
//  Created by vang on 12/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class NewDealWideView: UIView {
    @IBOutlet weak var timeImage: UIImageView!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var coverImageView: UIView!
    @IBOutlet weak var coverView: UIView!
    private var data: [String: Any?]?
    private var onClickedCellCallback: DataCallback?
    
    @IBOutlet weak var cateImage: UIImageView!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var coverLike: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    func loadView() -> NewDealWideView {
        let aView = Bundle.main.loadNibNamed("NewDealWideView", owner: self, options: nil)?[0] as! NewDealWideView
        
        return aView
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateFavorite(_:)), name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUseDeal(_:)), name: .UPDATE_USE_DEAL, object: nil)
        
        decorate()
    }
    
    @objc func updateUseDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["DealID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                tData["IsUseDeal"] = true
                self.data = tData
            }
        }
    }
    
    @objc func updateFavorite(_ notification: Notification) {
        
        if let uData = notification.object as? [String: Any], var tData = self.data {
            
            let uID = uData["ID"] as? String ?? ""
            let mID = tData["ID"] as? String ?? ""
            
            if uID == mID {
                let isFavorite = uData["Favorite"] as? Bool ?? false
                tData["IsFavorite"] = isFavorite
                
                self.data = tData
                
                self.fillData()
            }
        }
    }
    
    
    private func decorate() {
        coverView.layer.cornerRadius = 10.0
        //        coverView.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.16, x: 0, y: 0, blur: 9, spread: 0)
        //        coverView.layer.borderWidth = 0.2
        //        coverView.layer.borderColor = UIColor(151, 151, 151).cgColor
        
        coverImageView.layer.cornerRadius = 10.0
        coverImageView.layer.masksToBounds = true
        
        self.bgImage.layer.cornerRadius = 10.0
        
        self.coverLike.layer.cornerRadius = 10.0
        self.coverLike.layer.masksToBounds = true
    }
    
    @IBAction func clickedCell(_ sender: Any) {
        self.onClickedCellCallback?(self.data)
    }
    
    
    private func fillData() {
        if let tData = self.data {
            self.lblName.text = tData["Name"] as? String ?? ""
            
            self.lblAddress.text = Utils.getFullAddress(tData["VendorSummary"] as? [String: Any?])
            self.lblTime.text = "\(Utils.stringFromStringDate(tData["StartAt"] as? String, toFormat: "HH:mm a")) to \(Utils.stringFromStringDate(tData["EndAt"] as? String, toFormat: "HH:mm a"))"
            
            
            let isLike = tData["IsFavorite"] as? Bool ?? false
            self.likeImage.image = UIImage(named: isLike ? "new_like_ic" : "new_unlike_ic")
            
            self.thumbnailImage.sd_setImage(with:  URL(string: tData["Photo"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
            })
            
            if let summary = tData["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let cateURL = category["ImageUrl"] as? String  {
                self.cateImage?.sd_setImage(with: URL(string: cateURL), completed: { (_, _, _, _) in
                    
                })
            }
            

            let dealShowType = Utils.getDealType(tData)
            
            self.decorateWithStyle(dealShowType)
        }
    }
    
    private func decorateWithStyle(_ type: DEAL_TYPE = .BLACK) {
        if type == .BLACK {
            bgImage.image = UIImage(named: "bg_black_landscape")
            lblName.textColor = UIColor(222, 230, 156)
            lblAddress.textColor = .white
            lblTime.textColor = .white
            locationImage.image = UIImage(named: "location_white_ic")
            timeImage.image = UIImage(named: "time_white_ic")
            
        } else if type == .SILVER {
            bgImage.image = UIImage(named: "bg_silver_landscape")
            lblName.textColor = UIColor(47, 72, 88)
            lblAddress.textColor = UIColor(47, 72, 88)
            lblTime.textColor = UIColor(47, 72, 88)
            locationImage.image = UIImage(named: "location_blue_ic")
            timeImage.image = UIImage(named: "time_blue_ic")
        } else if type == .GOLD {
            bgImage.image = UIImage(named: "bg_gold_landscape")
            lblName.textColor = UIColor(47, 72, 88)
            lblAddress.textColor = UIColor(47, 72, 88)
            lblTime.textColor = UIColor(47, 72, 88)
            locationImage.image = UIImage(named: "location_blue_ic")
            timeImage.image = UIImage(named: "time_blue_ic")
        }
    }
    
    public func config(_ data: [String: Any?]?, onClickedCellCallback _onClickedCellCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedCellCallback = _onClickedCellCallback
        
        self.fillData()
        
        self.layoutIfNeeded()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_FAVORITE_DEAL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATE_USE_DEAL, object: nil)
    }
}
