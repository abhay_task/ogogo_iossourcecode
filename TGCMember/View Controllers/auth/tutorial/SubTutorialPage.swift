//
//  SubTutorialPage.swift
//  TGCMember
//
//  Created by vang on 3/11/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class SubTutorialPage: BaseViewController {
    func updateLanguages() {
        btnNext?.setTitle("               \(LOCALIZED.txt_next_title.translate)               ", for: .normal)
    }
    
    private var data:[String: Any?]?
    private var onClickedNextCallback: DataCallback?
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var coverDescription: UIView!
    @IBOutlet weak var topContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var bottomNextConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        decorate()
        
        self.fillData()
    }
    
    private func decorate() {
        topContentConstraint?.constant = TOP_SKIP_TUTORIAL + (SCREEN_HEIGHT <= 736 ? (SCREEN_HEIGHT <= 677 ? 15 : 30) : 50)
     
        if let tData = self.data {
            let index = tData["currentIndex"] as? Int ?? 0
            let total = tData["totalCount"] as? Int ?? 0
            
            if index == (total - 1) {
                self.btnNext?.isHidden = false
                self.bottomNextConstraint?.constant = BOTTOM_PAGING_TUTORIAL + 35 + (SCREEN_HEIGHT <= 736 ? 10 : 25)
            } else {
                self.btnNext?.isHidden = true
                self.bottomNextConstraint?.constant = BOTTOM_PAGING_TUTORIAL - 20
            }
        } else {
            self.btnNext?.isHidden = true
            self.bottomNextConstraint?.constant = BOTTOM_PAGING_TUTORIAL - 20
        }
        
        
    }
    
    @IBAction func onClickedNext(_ sender: Any) {
        self.onClickedNextCallback?(self.data)
    }
    
    func config(_ data: [String: Any?]?, onClickedNextCallback _onClickedNextCallback: DataCallback? = nil) -> Void {
        self.data = data
        self.onClickedNextCallback = _onClickedNextCallback
    }
    
    private func fillData() {
        if let tData = self.data {
            lblTitle?.setAttribute(tData["Title"] as? String ?? "", color: UIColor("rgb 255 193 6"), font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 28), spacing: 2)
                  lblTitle?.textAlignment = .center
            lblTitle?.textAlignment = .center
            
            lblSubTitle.setAttribute(tData["TitleDescription"] as? String ?? "", color: UIColor.white, font: UIFont(name: ROBOTO_FONT.BLACK.rawValue, size: 18), spacing: 3)
            
            self.logoImage?.sd_setImage(with: URL(string: tData["Photo"] as? String ?? ""), completed: { (_, _, _, _) in
                
            })
            
            self.renderDescriptionView(tData)
        }
    }
    
    private func renderDescriptionView(_ data: [String: Any?]) {
        self.coverDescription?.backgroundColor = .clear
        self.contentView?.backgroundColor = .clear
        
        self.coverDescription?.removeSubviews()
        var contentDescriptionView: UIView = UIView()
        contentDescriptionView.backgroundColor = .clear
        
        if let descriptions = data["Description"] as? [String?] {
            Utils.buildTutorialDescription(descriptions) { (info) in
                if let tInfo = info {
                    contentDescriptionView = tInfo["contentView"] as? UIView ?? UIView()
                }
            }
        } else {
            contentDescriptionView.snp.makeConstraints { (make) in
                make.width.equalTo(SCREEN_WIDTH)
                make.height.equalTo(5)
            }
        }
        
        self.coverDescription?.addSubview(contentDescriptionView)
        
        contentDescriptionView.snp.makeConstraints({ (make) in
            make.top.equalTo(self.coverDescription)
            make.left.equalTo(self.coverDescription)
            make.right.equalTo(self.coverDescription)
            make.bottom.equalTo(self.coverDescription)
        })
    }
}
