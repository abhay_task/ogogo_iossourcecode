//
//  SignUpViewController.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Nantes


class SignUpViewController: BaseViewController {

    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverBody: UIView!
    @IBOutlet weak var lblWarningPhone: UILabel!
    @IBOutlet weak var lblWarningConfirmPassword: UILabel!
    @IBOutlet weak var lblWarningPassword: UILabel!
    @IBOutlet weak var lblWarningEmail: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coverEmailWarning: UIView!
    @IBOutlet weak var coverPhoneWarning: UIView!
    @IBOutlet weak var coverPasswordWarning: UIView!
    @IBOutlet weak var coverConfirmPasswordWarning: UIView!
    
    @IBOutlet weak var btnSkipNow: UIButton!
    @IBOutlet weak var coverEmailView: UIView!

    @IBOutlet weak var lblPhoneCode: UILabel!
    @IBOutlet weak var coverTCView: UIView!
    @IBOutlet weak var coverSignInHere: UIView!
    @IBOutlet weak var btnCheckTC: UIButton!
    @IBOutlet weak var coverConfirmPasswordView: UIView!
    @IBOutlet weak var coverPasswordView: UIView!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfNumberPhone: UITextField!
    @IBOutlet weak var coverTextNumber: UIView!
    @IBOutlet weak var coverNumber: UIView!
    @IBOutlet weak var btnSignUp: HighlightableButton!
    @IBOutlet weak var btnShowConfirmPassword: UIButton!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var imageValid: UIImageView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblSignUp: UILabel!
    @IBOutlet weak var lblForFree: UILabel!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    private var selectedCountry: CountryModel = CountryModel()
    
    var codeOnCard: String?
    var isSignUpFirst: Bool = false
    var isFromSweepstake: Bool = false
    
    private var isShowPass: Bool = true
    private var isShowConfirmPass: Bool = true
    private var isAggreedTC: Bool = true
    
    var flowCallback: LoginCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        decorate()
        
        _ = validateAllField()
    }

    @IBAction func onClickedBack(_ sender: Any) {
        self.handleGoBack()
    }
    
    private func onClickSignHere() {
        if self.codeOnCard != nil || isSignUpFirst {
            let loginVC = LoginViewController()
            loginVC.isSignUpFirst = self.isSignUpFirst
            loginVC.codeOnCard = self.codeOnCard
            self.navigationController?.pushViewController(loginVC, animated: true)
        } else {
            self.handleGoBack(true)
        }
    }
    
    private func handleGoBack(_ toSignIn: Bool = false) {
        if self.isFromSweepstake {
            if toSignIn {
                let loginVC = LoginViewController()
                loginVC.isSignUpFirst = true
                loginVC.codeOnCard = self.codeOnCard
                self.navigationController?.pushViewController(loginVC, animated: true)
            } else {
                GLOBAL.GlobalVariables.appDelegate?.openNextScreenAfterChecked(nil)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }

    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        
            let tBottom = keyboardSize.height - self.heightFooterConstraint.constant - 10
          
            self.bottomBodyConstraint?.constant = tBottom
            
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    guard let self = self else {return}
              
                    self.coverBody.superview?.layoutIfNeeded()
                }, completion:  { finished in
                    self.bottomBodyConstraint?.constant = tBottom
            })
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint?.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self = self else {return}
          
                self.coverBody.superview?.layoutIfNeeded()
            }, completion:  { finished in
                self.bottomBodyConstraint?.constant = 0
        })
    }
    
    private func dismissKeyboard() {
        self.tfEmail.resignFirstResponder()
        self.tfNumberPhone.resignFirstResponder()
        self.tfPassword.resignFirstResponder()
        self.tfConfirmPassword.resignFirstResponder()
    }
    
 
    private func decorate() {
        coverBody?.layer.cornerRadius = CGFloat(40)
        coverBody?.layer.masksToBounds = true
        coverBody?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.heightFooterConstraint.constant = Utils.isIPhoneNotch() ? 90 : 70
        
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailView.layer.borderWidth = 0.5
        self.coverEmailView.layer.cornerRadius = 3.0
        self.coverEmailView.layer.masksToBounds = true
        
        self.coverNumber.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverNumber.layer.borderWidth = 0.5
        self.coverNumber.layer.cornerRadius = 3.0
        self.coverNumber.layer.masksToBounds = true
        
        self.coverTextNumber.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverTextNumber.layer.borderWidth = 0.5
        self.coverTextNumber.layer.cornerRadius = 3.0
        self.coverTextNumber.layer.masksToBounds = true
        
        
        self.coverPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPasswordView.layer.borderWidth = 0.5
        self.coverPasswordView.layer.cornerRadius = 3.0
        self.coverPasswordView.layer.masksToBounds = true
        
        self.coverConfirmPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverConfirmPasswordView.layer.borderWidth = 0.5
        self.coverConfirmPasswordView.layer.cornerRadius = 3.0
        self.coverConfirmPasswordView.layer.masksToBounds = true
        
        tooglePassword()
        toogleConfirmPassword()
        
        self.coverEmailWarning.isHidden = true
        self.coverPhoneWarning.isHidden = true
        self.coverPasswordWarning.isHidden = true
        self.coverConfirmPasswordWarning.isHidden = true
        
        self.btnCheckTC.setImage(isAggreedTC ? UIImage(named: "u_check") : UIImage(named: "u_uncheck"), for: .normal)
        
        btnSignUp.alpha = self.isAggreedTC ? 1.0 : 0.7
        btnSignUp.isUserInteractionEnabled = self.isAggreedTC
        
        self.lblPhoneCode.text = "\(self.selectedCountry.flag ?? "") \(self.selectedCountry.phoneCode ?? "")"
    }
    
    
    private func setupPrivacyPolicyLabel() {
        self.coverTCView.removeSubviews()
        
        let labelTerm: NantesLabel = .init(frame: .zero)
        labelTerm.delegate = self
        labelTerm.numberOfLines = 0
        labelTerm.textAlignment = .left
        labelTerm.font = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0)
        labelTerm.textColor =  UIColor(hexFromString: "#6B6B6B")
        
        let text = "\(LOCALIZED.i_have_agreed_with.translate) \(LOCALIZED.terms_and_conditions.translate)"
        labelTerm.text = text
        labelTerm.linkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelTerm.activeLinkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelTerm.addLink(to: URL(string: TERMS_POLICY)!, withRange: (text as NSString).range(of: LOCALIZED.terms_and_conditions.translate))
        
        self.coverTCView.addSubview(labelTerm)
        
        labelTerm.snp.makeConstraints { (make) in
            make.left.equalTo(self.coverTCView)
            make.right.equalTo(self.coverTCView)
            make.centerY.equalTo(self.coverTCView)
        }
    }
    
    private func setupSignInHereLabel() {
        self.coverSignInHere.removeSubviews()
        
        let labelSignInHere: NantesLabel = .init(frame: .zero)
        labelSignInHere.delegate = self
        labelSignInHere.numberOfLines = 0
        labelSignInHere.textAlignment = .center
        labelSignInHere.font = UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15.0)
        labelSignInHere.textColor =  UIColor(hexFromString: "#6B6B6B")
        
        let text = "\(LOCALIZED.already_have_account.translate) \(LOCALIZED.sign_in_here.translate)"
        
        labelSignInHere.text = text
        labelSignInHere.linkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelSignInHere.activeLinkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15.0) ?? UIFont.systemFont(ofSize: 15),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelSignInHere.addLink(to: URL(string: "httsp://signin.here")!, withRange: (text as NSString).range(of: LOCALIZED.sign_in_here.translate))
        
        self.coverSignInHere.addSubview(labelSignInHere)
        
        labelSignInHere.snp.makeConstraints { (make) in
            make.left.equalTo(self.coverSignInHere)
            make.right.equalTo(self.coverSignInHere)
            make.centerY.equalTo(self.coverSignInHere)
        }
    }
    
    @IBAction func onClickShowCountry(_ sender: Any) {
        self.dismissKeyboard()
        
        let countryListVC = CountryListViewController()
        countryListVC.selectedCountry = self.selectedCountry
        countryListVC.selectedCountryCallback = { [weak self] (data) in
            guard let self = self else {return}
            
            self.selectedCountry = CountryModel(data)
            self.lblPhoneCode.text = "\(self.selectedCountry.flag ?? "") \(self.selectedCountry.phoneCode ?? "")"
            
        }
        
        self.addSubviewAsPresent(countryListVC)
    }
    
    @IBAction func onClickedBG(_ sender: Any) {
        dismissKeyboard()
    }
    
    @IBAction func onClickedCheckTC(_ sender: Any) {
        self.isAggreedTC = !self.isAggreedTC
        
        self.btnCheckTC.setImage(isAggreedTC ? UIImage(named: "u_check") : UIImage(named: "u_uncheck"), for: .normal)
        
        _ = validateAllField()
    }
    
    @IBAction func onClickedShowPass(_ sender: Any) {
        tooglePassword()
    }
    
    @IBAction func onClickedShowConfirmPass(_ sender: Any) {
        toogleConfirmPassword()
    }
    
    private func tooglePassword() {
        isShowPass = !isShowPass
        
        self.tfPassword.isSecureTextEntry = !isShowPass
        
        self.btnShowPassword.setImage(isShowPass ? UIImage(named: "show_pass") : UIImage(named: "hide_pass"), for: .normal)
    }
    
    private func toogleConfirmPassword() {
        isShowConfirmPass = !isShowConfirmPass
        
        self.tfConfirmPassword.isSecureTextEntry = !isShowConfirmPass
        
        self.btnShowConfirmPassword.setImage(isShowConfirmPass ? UIImage(named: "show_pass") : UIImage(named: "hide_pass"), for: .normal)
    }
    
    @IBAction func onClickedSignUp(_ sender: Any) {
        self.doSignUp()
    }
    
    @IBAction func onClickedSkipForNow(_ sender: Any) {
        
        if let discoveryVC = GLOBAL.GlobalVariables.discoveryInstance {
            self.navigationController?.popToViewController(discoveryVC, animated: true)
        } else {
            let _discoveryVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
            self.navigationController?.pushViewController(_discoveryVC, animated: true)
        }
    }
    
    private func validateAllField(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = true
        
        let isValidEmail = self.validateEmail(pText, withoutField)
        let isValidPhone = self.validatePhoneNumber(pText, withoutField)
        let isValidPassword = self.validatePassword(pText, withoutField)
        let isValidConfirmPassword = self.validateConfirmPassword(pText, withoutField == self.tfPassword ? pText : self.tfPassword.text ?? "", withoutField)
        
        if !isValidEmail || !isValidPhone || !isValidPassword || !isValidConfirmPassword {
            isValid = false
        }
        
        //let isInputText = tfEmail.text != "" && tfNumberPhone.text != "" && tfPassword.text != "" && tfConfirmPassword.text != ""
        
        self.btnSignUp.backgroundColor =  isValid && self.isAggreedTC ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSignUp.isUserInteractionEnabled = isValid && self.isAggreedTC
        
        if  isValid && self.isAggreedTC {
            self.btnSignUp.layer.applySketchShadow()
        } else {
            self.btnSignUp.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    private func doSignUp() {
        
        self.dismissKeyboard()
        
        if !validateAllField() {
            return
        }
    
        Utils.showLoading()
        
        let params = [
            "user_id": Utils.getUserId(),
            "email": self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "password": self.tfPassword.text ?? "",
            "phone": self.tfNumberPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "country_code": self.selectedCountry.countryCode ?? "",
            "phone_code": self.selectedCountry.phoneCode ?? "",
            "current_lat" : GLOBAL.GlobalVariables.latestLat,
            "current_lng": GLOBAL.GlobalVariables.latestLng
            ]  as [String : Any]
        
        mPrint("PARAMS REGISTER --> ", params)
        
        APICommonServices.register(params) { (resp) in
            mPrint("register --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                    let message = resp["message"] as? String ?? ""
                    
                    Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                        guard let self = self else {return}
                        
                        let data = resp["data"] as? [String: Any?]
                        
                        let userInfo = data != nil ? data!["user"] as? [String: Any?] : nil
                        Utils.setUserInfo(userInfo)
                        Utils.setAppToken(resp["token"] as? String)
                        Utils.updatePushingToken()
                        
                        NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                        
                        Repository.shared.getInfoTarget(completionHandler: { result in
                            switch result {
                            case .success(let infoTarget):
                                CacheManager.shared.infoTarget = infoTarget
                            case .failure(let error):
                                print(print(error.message))
                            }
                            
                            if GLOBAL.GlobalVariables.isHomeScreenReady {
                                NotificationCenter.default.post(name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
                            }
                            
                            if let tCode = self.codeOnCard {
                                self.doActivate(tCode)
                            } else {
                                let sweepstakeId = Utils.getSweepstakeId()
                                
                                if sweepstakeId != "" {
                                    Utils.showLoading()
                                    Utils.submitSweepstakeResult(sweepstakeId, onCompletion: {  [weak self] (success, data) in
                                        guard let self = self else {return}
                                        
                                        Utils.dismissLoading()
                                        
                                        if success {
                                            if let homeVC = GLOBAL.GlobalVariables.discoveryInstance {
                                                self.navigationController?.popToViewController(homeVC, animated: true)
                                            } else {
                                                let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                                self.navigationController?.pushViewController(homeVC, animated: true)
                                            }
                                        } else {
                                            let tMessage = data as? String ?? ""
                                            Utils.showConfirmAlert(self, "", tMessage, LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                                                guard let self = self else {return}
                                                
                                                let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                                self.navigationController?.pushViewController(homeVC, animated: true)
                                            }, cancelCallback: nil)
                                        }
                                    })
                                } else if Utils.isActivateCard() {
                                    if let homeVC = GLOBAL.GlobalVariables.discoveryInstance {
                                        self.navigationController?.popToViewController(homeVC, animated: true)
                                    } else {
                                        let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                        self.navigationController?.pushViewController(homeVC, animated: true)
                                    }
                                } else {
                                    let newUpgradeVC = NewUpgradeViewController()
                                    newUpgradeVC.initPlanIndex = UPGRADE_PLAN.BASIC.viewIndex
                                    
                                    self.navigationController?.pushViewController(newUpgradeVC, animated: true)
                                }
                            }
                        })
                    }, cancelCallback: nil)
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
        
    }
    
    private func doActivate(_ tCode: String) {
        Utils.showLoading()
  
        APICommonServices.activateCard(tCode) { (resp) in
            mPrint("activateCard when sign-up finished --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                
                if status {
                    let data = resp["data"] as? [String: Any?]
                    let userInfo = data != nil ? data!["user"] as? [String: Any?] : nil
                    
                    Utils.setUserInfo(userInfo)
                }
                
                let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            
            Utils.dismissLoading()
        }
    }
    
    
    private func validateEmail(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
        
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverEmailWarning.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.imageValid.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverEmailView.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
        
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverEmailView.layer.borderColor =  INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailWarning.isHidden  = true
        self.imageValid.isHidden  = true
    }
    
    private func validatePhoneNumber(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let checkText = withoutField == self.tfNumberPhone ? pText : self.tfNumberPhone.text
        
        if checkText != "" {
            isValid = true
//            var isValid = false
//            if let text = checkText, text.count > 10 {
//                isValid = true
//            }
            
            self.coverPhoneWarning.isHidden  = isValid
            self.coverTextNumber.layer.borderColor = isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor
            
        } else {
            self.resetViewPhone()
        }
        
        return isValid
        
    }
    
    private func resetViewPhone() {
        self.coverTextNumber.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPhoneWarning.isHidden  = true
    }

    
    private func validatePassword(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let checkText = withoutField == self.tfPassword ? pText : self.tfPassword.text
        if checkText != "" {
            isValid = false
            if let text = checkText, text.count > 5 {
                isValid = true
            }
            
            self.coverPasswordWarning.isHidden  = withoutField == self.tfPassword ? true : isValid
            self.coverPasswordView.layer.borderColor = withoutField == self.tfPassword ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
            
        } else {
            self.resetViewPassword()
        }
        
        return isValid
    }
    
    
    private func resetViewPassword() {
        self.coverPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPasswordWarning.isHidden  = true
    }
    
    
    private func validateConfirmPassword(_ pText: String = "", _ passText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        let checkText = withoutField == self.tfConfirmPassword ? pText : self.tfConfirmPassword.text
        
        if checkText != "" {
            isValid =  passText == checkText
        
            self.coverConfirmPasswordWarning.isHidden  = withoutField == self.tfConfirmPassword ? true : isValid
            self.coverConfirmPasswordView.layer.borderColor = withoutField == self.tfConfirmPassword ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        
        } else {
            self.resetViewConfirmPassword()
        }
        
        return isValid
    }
    
    private func resetViewConfirmPassword() {
        self.coverConfirmPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverConfirmPasswordWarning.isHidden  = true
    }
}


extension SignUpViewController: NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        if link.absoluteString == TERMS_POLICY {
            Utils.openURL(link.absoluteString)
        } else {
            self.onClickSignHere()
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfEmail {
            tfNumberPhone.becomeFirstResponder()
        } else if textField == tfNumberPhone {
            tfPassword.becomeFirstResponder()
        } else if textField == tfPassword {
            tfConfirmPassword.becomeFirstResponder()
        } else if textField == tfConfirmPassword {
            self.doSignUp()
        }
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
              _ = self.validateAllField(updatedText, textField)
            
            if textField == self.tfPassword || textField == self.tfConfirmPassword {
                textField.text = updatedText
                
                return false
            }
            
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            } else if textField == self.tfNumberPhone {
                self.resetViewPhone()
            } else if textField == self.tfPassword {
                self.resetViewPassword()
            } else if textField == self.tfConfirmPassword {
                self.resetViewConfirmPassword()
            }
        }, 100)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateAllField()
    }
}


extension SignUpViewController {
    func updateLanguages() {
        self.lblSignUp.text = LOCALIZED.txt_sign_up.translate
        self.tfEmail.placeholder = LOCALIZED.enter_your_email_address.translate
        self.lblWarningEmail.text = LOCALIZED.please_enter_a_valid_email.translate
        
        self.lblPhoneNumber.text = LOCALIZED.txt_phone_number.translate
        self.tfNumberPhone.placeholder = LOCALIZED.enter_phone_number.translate
        self.lblWarningPhone.text = LOCALIZED.please_enter_a_valid_phone_number.translate
        
        self.lblPassword.text = LOCALIZED.txt_password_title.translate
        self.tfPassword.placeholder = LOCALIZED.enter_password.translate
        self.lblWarningPassword.text = LOCALIZED.please_enter_at_least_6_characters.translate
        
        self.lblConfirmPassword.text = LOCALIZED.txt_confirm_password.translate
        self.tfConfirmPassword.placeholder = LOCALIZED.enter_password.translate
        self.lblWarningConfirmPassword.text = LOCALIZED.password_and_confirm_password_does_not_match.translate
        
        self.lblForFree.text = LOCALIZED.want_to_try_for_free.translate
        self.btnSkipNow.setTitleUnderline(LOCALIZED.skip_for_now.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
        
        self.btnSignUp.setTitle(LOCALIZED.txt_sign_up.translate, for: .normal)
        
        setupSignInHereLabel()
        setupPrivacyPolicyLabel()
    }
}
