//
//  ScannerQRViewController.swift
//  TGCMember
//
//  Created by vang on 7/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import MTBBarcodeScanner

class ScannerQRViewController: BaseViewController {
    
    var scanner: MTBBarcodeScanner?
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var lblTitle: NSLayoutConstraint!
    @IBOutlet weak var centerView: UIView!
    
    var parentVC: UIViewController = UIViewController()
    var successCallback: DataCallback?
    var isStopScan: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.scanner = MTBBarcodeScanner(previewView: self.previewView)
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        self.showFocusCenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setTimeout({
            self.startScan()
        }, 200)
        
    }
    
  
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.scanner?.stopScanning()
    }
    
    
    private func showFocusCenter() {
        UIView.animate(withDuration: 0.4, delay: 0, options: UIView.AnimationOptions() , animations: {
            self.centerView.alpha = 1
            
        }, completion: { (finished: Bool) in
            self.centerView.alpha = 1
        })
    }

    @IBAction func onClickedBack(_ sender: Any) {
       
        self.dismissFromSuperview(true) { (finished) in
            self.willMove(toParent: nil)
            self.removeFromParent()
            self.view.removeFromSuperview()
        }
    }
    
    func startScan() {
        self.isStopScan = false
        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    try self.scanner?.startScanning(resultBlock: { codes in
                        if self.isStopScan {
                            print("WAITING >>")
                        } else {
                            self.isStopScan = true
                            if let codes = codes {
                                for code in codes {
                                    let stringValue = code.stringValue!
                                    self.handleCheckCode(stringValue)
                                }
                            }
                        }
                    })
                } catch {
                    print("Unable to start scanning")
                }
            } else {
                Utils.showAlert(self.parentVC, MLocalized("Scanning Unavailable"), MLocalized("This app does not have permission to access the camera"))
            }
        })
    }
    
    private func handleCheckCode(_ code: String) {
        if let callback = self.successCallback {
            callback(["code": code])
        }
    
    }

    
}

extension ScannerQRViewController {
    func updateLanguages() {
        
    }
}
