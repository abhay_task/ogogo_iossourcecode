//
//  ActiveCardViewController.swift
//  TGCMember
//
//  Created by vang on 6/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class FreeAccountViewController: BaseViewController {

    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblTitleScreen: UILabel!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var willReceiveView: UIView!
    @IBOutlet weak var heightWillReciveConstraint: NSLayoutConstraint!
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCopyRight: UILabel!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCodeView: UIView!
    @IBOutlet weak var lblInputScratchCode: UILabel!
    @IBOutlet weak var btnActivate: HighlightableButton!
    @IBOutlet weak var coverCodeWarning: UIView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var scanner: ScannerQRViewController?
    var flowCallback: LoginCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.decorate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        GLOBAL.GlobalVariables.freeAccountInstance = self
    }

    @IBAction func onClickedScan(_ sender: Any) {
        self.dismissKeyboard()
        
        
        
        
        self.scanner = ScannerQRViewController()
        self.scanner?.parentVC = self
        self.scanner?.successCallback = {  [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data {
                let scanCode = tData["code"] as? String ?? ""
                
                self.doVerifyActivateCode(scanCode, true)
            }
        }
        
        if let tScanner = self.scanner {
            self.addSubviewAsPresent(tScanner)
        }

    }
    
    @IBAction func onClickedActivate(_ sender: Any) {
        self.doVerifyActivateCode(self.tfCode.text ?? "")
    }
    
    @IBAction func onClickedFindCode(_ sender: Any) {
        
    }
    
    @IBAction func onClickedSkip(_ sender: Any) {
        GLOBAL.GlobalVariables.freeAccountInstance = nil
        if let callback = self.flowCallback {
            callback(LOGIN_STATUS.SUCCESS_AFTER)
        } else {
            let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        GLOBAL.GlobalVariables.freeAccountInstance = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    private func dismissKeyboard() {
        self.tfCode.resignFirstResponder()
    }
    
    private func decorate() {
        self.topHeaderConstraint.constant = Utils.isIPhoneNotch() ? 30 : 10
        self.bottomFooterConstraint.constant = defaultBottom
        
        self.coverCodeView.layer.borderColor = UIColor(205, 207, 214, 1).cgColor
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.cornerRadius = 3.0
        self.coverCodeView.layer.masksToBounds = true
        
        self.coverCodeWarning.isHidden = true
        self.lblCopyRight.text = Utils.getCopyright()
        
        _ = validateCode()
        
        self.getFreePackages()

    }

    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - 50 - self.bottomFooterConstraint.constant
            self.bottomScrollConstraint.constant = tBottom
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomScrollConstraint.constant = 0
    }
    
    private func renderWillReceive(_ data: [[String: Any?]] = []) {
        self.willReceiveView.backgroundColor = .clear
        Utils.buildBenefitDescription(data) { [weak self] (info) in
            guard let self = self else {return}
            
            if let tInfo = info {
                let contentView = tInfo["contentView"] as? UIView ?? UIView()
                let contentHeight = tInfo["contentHeight"] as? CGFloat ?? 0.0
                self.heightWillReciveConstraint.constant = contentHeight
                self.willReceiveView.addSubview(contentView)
                
                contentView.snp.makeConstraints({ (make) in
                    make.top.equalTo(self.willReceiveView)
                    make.left.equalTo(self.willReceiveView)
                    make.right.equalTo(self.willReceiveView)
                    make.bottom.equalTo(self.willReceiveView)
                })
            }   
        }
    }
    
    private func getFreePackages() {
        Utils.showLoading()
        
        APICommonServices.getAvailablePackages { [weak self] (resp) in
            guard let self = self else {return}
            
            //mPrint("getFreePackages", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], let freePlan = data["free-member"]  as? [String : Any?], status == true  {
     
                    let descriptions = freePlan["Description"] as?  [[String: Any?]] ?? []
                    self.renderWillReceive(descriptions)
                    
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func doActivate(_ showLoading: Bool = true, _ withoutCheck: Bool = false) {
        self.dismissKeyboard()
        
        if !withoutCheck {
            if !validateCode() {
                return
            }
        }
        
        if showLoading {
            Utils.showLoading()
        }
        
        let tCode = self.tfCode.text
        
        APICommonServices.activateCard(tCode) { (resp) in
            mPrint("activateCard", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                    let data = resp["data"] as? [String: Any?]
                    let userInfo = data != nil ? data!["user"] as? [String: Any?] : nil
                    Utils.setUserInfo(userInfo)
                    
                    let message = resp["message"] as? String ?? ""
                    Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                        guard let self = self else {return}
                        
                        if let callback = self.flowCallback {
                            callback(LOGIN_STATUS.SUCCESS_AFTER)
                        } else { // after sign-up
                            if let discoveryIns = GLOBAL.GlobalVariables.discoveryInstance {
                                self.navigationController?.popToViewController(discoveryIns, animated: true)
                            } else {
                                let _discoveryIns = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                self.navigationController?.pushViewController(_discoveryIns, animated: true)
                            }
                        }
                     }, cancelCallback: nil)
                } else {
                    Utils.showConfirmAlert(self, "", resp["message"] as? String ?? "", LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                        guard let self = self else {return}
                        
                        self.scanner?.startScan()
                    }, cancelCallback: nil)
                   
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func doVerifyActivateCode(_ tCode: String, _ withoutCheck: Bool = false) {
        self.dismissKeyboard()
        
        if !withoutCheck {
            if !validateCode() {
                return
            }
        }
        
        Utils.showLoading()
  
        APICommonServices.verifyCodeOnCard(tCode) { (resp) in
            mPrint("verifyCodeOnCard at FREE Account", resp)
            
            if let resp = resp {
                let status = resp["error"] as? String ?? VERIFY_CARD_STATUS.INVALID.rawValue
                
                let verifyStatus = VERIFY_CARD_STATUS(rawValue: status) ?? VERIFY_CARD_STATUS.INVALID
                
                if verifyStatus == .VALID_NOT_USE {
                    self.doActivate(false, true)
                } else {
                    Utils.dismissLoading()
                    self.redirectToResultView(verifyStatus, tCode, data: resp["data"] as? [String: Any])
                }
            } else {
                 Utils.dismissLoading()
            }
        }
    }
    
    private func redirectToResultView(_ status: VERIFY_CARD_STATUS = .INVALID, _ code: String?, data: [String: Any]? = nil) {
        let resultVC = ResultVerifyCodeViewController()
        resultVC.data = data
        resultVC.scratchCode = code ?? ""
        resultVC.verifyStatus = status == .VALID_IS_USED ? VERIFY_CODE_STATUS.SUCCESS : VERIFY_CODE_STATUS.FAILED
        
        self.navigationController?.pushViewController(resultVC, animated: true)
        
    }
    
    private func validateCode(_ pText: String = "", _ showWarning: Bool = true) -> Bool {
        let checkText = pText != "" ? pText : self.tfCode.text
        
        var isValid = false
        
        if checkText != "" {
            if let text = checkText, text.count > 3 {
                isValid = true
            }
            
            if showWarning {
                self.coverCodeWarning.isHidden  = isValid
                self.coverCodeView.layer.borderWidth = isValid ? 0.5 : 1.0
                self.coverCodeView.layer.borderColor = isValid ? UIColor(205, 207, 214, 1).cgColor : UIColor(220, 78, 65, 1).cgColor
            }

        } else {
            self.resetViewCode()
        }
       
        self.btnActivate.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnActivate.isUserInteractionEnabled = isValid
        
        
        if isValid {
            self.btnActivate.layer.applySketchShadow()
        } else {
            self.btnActivate.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    private func resetViewCode() {
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.borderColor =  UIColor(205, 207, 214, 1).cgColor
        self.coverCodeWarning.isHidden  = true
    }
}

extension FreeAccountViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.doVerifyActivateCode(self.tfCode.text ?? "")
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateCode(updatedText, false)
        }
        
        return true
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfCode {
            self.resetViewCode()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         _ = validateCode()
    }
}

extension FreeAccountViewController {
    func updateLanguages() {
        self.lblInputScratchCode.text = LOCALIZED.input_scratch_code_on_card.translate
        self.tfCode.placeholder = LOCALIZED.enter_scratch_code.translate
        self.lblPrompt.text = LOCALIZED.you_re_free_member_now_it_s_time_to_upgrade.translate
        self.btnActivate.setTitle(LOCALIZED.txt_activate_title.translate, for: .normal)
    }
}
