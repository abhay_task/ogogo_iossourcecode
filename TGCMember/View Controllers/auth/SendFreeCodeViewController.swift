//
//  SendFreeCodeViewController.swift
//  TGCMember
//
//  Created by vang on 3/3/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class SendFreeCodeViewController: BaseViewController {
    func updateLanguages() {
        
    }
    
    
    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverBody: UIView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblTitleCode: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var btnSend: HighlightableButton!
    
    @IBOutlet weak var coverCodeView: UIView!
    @IBOutlet weak var coverEmailView: UIView!
    @IBOutlet weak var imageValid: UIImageView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWarningEmail: UILabel!
    @IBOutlet weak var coverEmailWarning: UIView!
    
    @IBOutlet weak var coverBack: HighlightableView!
    
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    
    let bottomFooter: CGFloat = Utils.isIPhoneNotch() ? 50 : 30
    var isFromSignUp: Bool = true
    var backToDealDetail: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let userInfo = Utils.getUserInfo() {
            self.lblCode?.text = userInfo["GiftVirtualCard"] as? String ?? ""
        } else {
            self.lblCode?.text = ""
        }
        
        decorate()
        
        _ = validateAllField()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func onClickedRemind(_ sender: Any) {
        self.handleGoBack()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.handleGoBack()
    }
    
    @IBAction func onClickedSend(_ sender: Any) {
        self.doSend()
    }
    
    private func doSend() {
        self.dismissKeyboard()
        
        if !validateAllField() {
            return
        }
        
        Utils.showLoading()
        
        let params = [
            "code": self.lblCode.text ?? "",
            "email": self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            ]  as [String : Any]
        
        mPrint("Send Gift Card PARAMS --> ", params)
        
        APICommonServices.sendGiftCard(params) { (resp) in
            mPrint("Send Gift Card RESP --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                let message = resp["message"] as? String ?? ""

                if status {
                    Utils.getMyProfile { [weak self] (success) in
                        guard let self = self else {return}

                        Utils.dismissLoading()

                        NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                        if GLOBAL.GlobalVariables.isHomeScreenReady {
                            NotificationCenter.default.post(name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
                        }

                        Utils.showAlertStyle(message, status) {  [weak self] in
                            guard let self = self else {return}
                            
                            self.handleGoBack()
                        }
                    }
                } else {
                    Utils.dismissLoading()
                    Utils.showAlertStyle(message, status)
                }
            } else {
                Utils.dismissLoading()
            }
        }
    }
    
    private func handleGoBack(_ toSignIn: Bool = false) {
        self.dismissKeyboard()
        
        if let dealDetailIns = GLOBAL.GlobalVariables.dealDetailInstance, self.backToDealDetail == true {
            self.navigationController?.popToViewController(dealDetailIns, animated: true)
        } else {
           if let discoveryIns = GLOBAL.GlobalVariables.discoveryInstance {
               dismiss(animated: true, completion: nil)
            } else if let appDelegate = Utils.getAppDelegate() {
                appDelegate.initViewController(Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className()))
            }
        }
        
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let tBottom = keyboardSize.height - self.heightFooterConstraint.constant - 10
            
            self.bottomBodyConstraint?.constant = tBottom
            
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self = self else {return}
                
                self.coverBody.superview?.layoutIfNeeded()
                }, completion:  { finished in
                    self.bottomBodyConstraint?.constant = tBottom
            })
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint?.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let self = self else {return}
            
            self.coverBody.superview?.layoutIfNeeded()
            }, completion:  { finished in
                self.bottomBodyConstraint?.constant = 0
        })
    }
    
    private func dismissKeyboard() {
        self.tfEmail.resignFirstResponder()
    }
    
    
    private func decorate() {
        bottomFooterConstraint?.constant = bottomFooter
        
        coverBody?.layer.cornerRadius = CGFloat(40)
        coverBody?.layer.masksToBounds = true
        coverBody?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.coverBack?.layer.cornerRadius = 20.0
        self.coverBack?.layer.applySketchShadow(color: UIColor("rgba 147 139 120 1.0"), alpha: 0.12, x: 3, y: 4, blur: 20, spread: 0)
        
        self.coverCodeView.backgroundColor = UIColor("rgb 234 234 234")
        self.coverCodeView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.cornerRadius = 3.0
        self.coverCodeView.layer.masksToBounds = true
        
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailView.layer.borderWidth = 0.5
        self.coverEmailView.layer.cornerRadius = 3.0
        self.coverEmailView.layer.masksToBounds = true
        
        
        
        self.coverEmailWarning.isHidden = true
        
    }
    
    @IBAction func onClickedBG(_ sender: Any) {
        dismissKeyboard()
    }
    
    
    private func validateAllField(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = true
        
        let isValidEmail = self.validateEmail(pText, withoutField)
        
        if !isValidEmail {
            isValid = false
        }
        
        self.btnSend?.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSend?.isUserInteractionEnabled = isValid
        
        if  isValid {
            self.btnSend?.layer.applySketchShadow()
        } else {
            self.btnSend?.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    
    private func validateEmail(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
        
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverEmailWarning.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.imageValid.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverEmailView.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
        
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverEmailView.layer.borderColor =  INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailWarning.isHidden  = true
        self.imageValid.isHidden  = true
    }
    
}

extension SendFreeCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateAllField(updatedText, textField)
            
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            }
        }, 100)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateAllField()
    }
}
