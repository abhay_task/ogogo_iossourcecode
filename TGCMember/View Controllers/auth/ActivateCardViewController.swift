//
//  ActiveCardViewController.swift
//  TGCMember
//
//  Created by vang on 6/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class VerifyCodeViewController: BaseViewController {
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var lblCopyRight: UILabel!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCodeView: UIView!
    @IBOutlet weak var lblInputScratchCode: UILabel!
    @IBOutlet weak var lblActivateCard: UILabel!
    @IBOutlet weak var btnActivate: HighlightableButton!
    @IBOutlet weak var btnFindCode: UIButton!
    @IBOutlet weak var coverCodeWarning: UIView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var flowCallback: LoginCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.decorate()
    }

    @IBAction func onClickedActivate(_ sender: Any) {
        self.doActivate()
    }
    
    @IBAction func onClickedFindCode(_ sender: Any) {
        
    }
    
    @IBAction func onClickedSkip(_ sender: Any) {

        if let callback = self.flowCallback {
            callback(LOGIN_STATUS.SUCCESS_AFTER)
        } else {
            let homeVC = HomeViewController()
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    @IBAction func onClickedScan(_ sender: Any) {
        let scannerVC = ScannerQRViewController()
        self.addSubviewAsPresent(scannerVC)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    private func dismissKeyboard() {
        self.tfCode.resignFirstResponder()
    }
    
    private func decorate() {

        self.bottomFooterConstraint.constant = defaultBottom
        
        self.coverCodeView.layer.borderColor = UIColor(205, 207, 214, 1).cgColor
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.cornerRadius = 3.0
        self.coverCodeView.layer.masksToBounds = true
        
        
        self.btnFindCode.setTitleUnderline(MLocalized("How to find the code"), color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: APP_FONT.LIGHT.rawValue, size: 13))
        
        self.btnSkip.setTitleUnderline(MLocalized("No, I don’t have one"), color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: APP_FONT.REGULAR.rawValue, size: 15))
        
        self.coverCodeWarning.isHidden = true
        self.lblCopyRight.text = Utils.copyRight()
        
        _ = validateCode()
    }

    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - 50 - self.bottomFooterConstraint.constant
            self.bottomScrollConstraint.constant = tBottom
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomScrollConstraint.constant = 0
    }
    
    private func doActivate() {
        self.dismissKeyboard()
        
        if !validateCode() {
            return
        }
        
        Utils.showLoading()
        
        let tCode = self.tfCode.text
        
        APICommonServices.verifyCodeOnCard(tCode) { (resp) in
            mPrint("verifyCodeOnCard", resp)
            
            if let resp = resp {
                let status = resp["error"] as? String ?? VERIFY_CARD_STATUS.INVALID.rawValue
                self.redirectToResultView(VERIFY_CARD_STATUS(rawValue: status) ?? VERIFY_CARD_STATUS.INVALID, tCode, data: resp["data"] as? [String: Any])
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func redirectToResultView(_ status: VERIFY_CARD_STATUS = .INVALID, _ code: String?, data: [String: Any]? = nil) {
        
        if status == .VALID_NOT_USE {
            let signUpVC = SignUpViewController()
            signUpVC.codeOnCard = code
            self.navigationController?.pushViewController(signUpVC, animated: true)
        } else {
            let resultVC = ResultVerifyCodeViewController()
            resultVC.data = data
            resultVC.verifyStatus = status == .VALID_IS_USED ? VERIFY_CODE_STATUS.SUCCESS : VERIFY_CODE_STATUS.FAILED
            
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
    
    }
    
    private func validateCode(_ pText: String = "", _ showWarning: Bool = true) -> Bool {
        let checkText = pText != "" ? pText : self.tfCode.text
        
        var isValid = false
        
        if checkText != "" {
            if let text = checkText, text.count > 3 {
                isValid = true
            }
            
            if showWarning {
                self.coverCodeWarning.isHidden  = isValid
                self.coverCodeView.layer.borderWidth = isValid ? 0.5 : 1.0
                self.coverCodeView.layer.borderColor = isValid ? UIColor(205, 207, 214, 1).cgColor : UIColor(220, 78, 65, 1).cgColor
            }

        } else {
            self.resetViewCode()
        }
       
        self.btnActivate.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnActivate.isUserInteractionEnabled = isValid
        
        
        if isValid {
            self.btnActivate.layer.applySketchShadow()
        } else {
            self.btnActivate.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    private func resetViewCode() {
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.borderColor =  UIColor(205, 207, 214, 1).cgColor
        self.coverCodeWarning.isHidden  = true
    }
}

extension VerifyCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.doActivate()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateCode(updatedText, false)
        }
        
        return true
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfCode {
            self.resetViewCode()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         _ = validateCode()
    }
}

extension VerifyCodeViewController {
    func updateLanguages() {
        
    }
}
