//
//  ActiveCardViewController.swift
//  TGCMember
//
//  Created by vang on 6/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class VerifyCodeViewController: BaseViewController {
    
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var lblCopyRight: UILabel!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCodeView: UIView!
    @IBOutlet weak var lblInputScratchCode: UILabel!
    @IBOutlet weak var lblActivateCard: UILabel!
    @IBOutlet weak var btnActivate: HighlightableButton!
    @IBOutlet weak var btnFindCode: UIButton!
    @IBOutlet weak var coverCodeWarning: UIView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    var isFromSweepstake: Bool = false
    
    var flowCallback: LoginCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.decorate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        GLOBAL.GlobalVariables.verifyCodeInstance = self
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        GLOBAL.GlobalVariables.verifyCodeInstance = nil
        
        if isFromSweepstake {
           GLOBAL.GlobalVariables.appDelegate?.openNextScreenAfterChecked(nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onClickedActivate(_ sender: Any) {
        self.doVerifyActivateCode(self.tfCode.text ?? "")
    }
    
    @IBAction func onClickedFindCode(_ sender: Any) {
        let content = FindCodeView().loadView()
        
        content.config(nil, onClickedCloseCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    @IBAction func onClickedSkip(_ sender: Any) {

        if let callback = self.flowCallback {
            callback(LOGIN_STATUS.SUCCESS_AFTER)
        } else {
            let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    @IBAction func onClickedScan(_ sender: Any) {
        let scannerVC = ScannerQRViewController()
        scannerVC.parentVC = self
        scannerVC.successCallback = {  [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data {
                let scanCode = tData["code"] as? String ?? ""
                
                self.doVerifyActivateCode(scanCode, true)
            }
        }

        
        self.addSubviewAsPresent(scannerVC)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    private func dismissKeyboard() {
        self.tfCode.resignFirstResponder()
    }
    
    private func decorate() {

        self.bottomFooterConstraint.constant = defaultBottom
        
        self.coverCodeView.layer.borderColor = UIColor(205, 207, 214, 1).cgColor
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.cornerRadius = 3.0
        self.coverCodeView.layer.masksToBounds = true
        
        
        self.coverCodeWarning.isHidden = true
        
        
        _ = validateCode()
    }

    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - 50 - self.bottomFooterConstraint.constant
            self.bottomScrollConstraint.constant = tBottom
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomScrollConstraint.constant = 0
    }
    
    private func doVerifyActivateCode(_ tCode: String, _ withoutCheck: Bool = false) {
        self.dismissKeyboard()
        
        if !withoutCheck {
            if !validateCode() {
                return
            }
        }

        Utils.showLoading()
        
        APICommonServices.verifyCodeOnCard(tCode) { (resp) in
            mPrint("verifyCodeOnCard", resp)
            
            if let resp = resp {
                let status = resp["error"] as? String ?? VERIFY_CARD_STATUS.INVALID.rawValue
                self.redirectToResultView(VERIFY_CARD_STATUS(rawValue: status) ?? VERIFY_CARD_STATUS.INVALID, tCode, data: resp["data"] as? [String: Any])
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func redirectToResultView(_ status: VERIFY_CARD_STATUS = .INVALID, _ code: String?, data: [String: Any]? = nil) {
        
        if status == .VALID_NOT_USE {
            let signUpVC = SignUpViewController()
            signUpVC.codeOnCard = code
            self.navigationController?.pushViewController(signUpVC, animated: true)
        } else {
            let resultVC = ResultVerifyCodeViewController()
            resultVC.data = data
            resultVC.scratchCode = code ?? ""
            resultVC.verifyStatus = status == .VALID_IS_USED ? VERIFY_CODE_STATUS.SUCCESS : VERIFY_CODE_STATUS.FAILED
            
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
    }
    
    private func validateCode(_ pText: String = "", _ showWarning: Bool = true) -> Bool {
        let checkText = pText != "" ? pText : self.tfCode.text
        
        var isValid = false
        
        if checkText != "" {
            if let text = checkText, text.count > 3 {
                isValid = true
            }
            
            if showWarning {
                self.coverCodeWarning.isHidden  = isValid
                self.coverCodeView.layer.borderWidth = isValid ? 0.5 : 1.0
                self.coverCodeView.layer.borderColor = isValid ? UIColor(205, 207, 214, 1).cgColor : UIColor(220, 78, 65, 1).cgColor
            }

        } else {
            self.resetViewCode()
        }
       
        self.btnActivate.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnActivate.isUserInteractionEnabled = isValid
        
        
        if isValid {
            self.btnActivate.layer.applySketchShadow()
        } else {
            self.btnActivate.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    private func resetViewCode() {
        self.coverCodeView.layer.borderWidth = 0.5
        self.coverCodeView.layer.borderColor =  UIColor(205, 207, 214, 1).cgColor
        self.coverCodeWarning.isHidden  = true
    }
}

extension VerifyCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.doVerifyActivateCode(self.tfCode.text ?? "")
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateCode(updatedText, false)
        }
        
        return true
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfCode {
            self.resetViewCode()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
         _ = validateCode()
    }
}

extension VerifyCodeViewController {
    func updateLanguages() {
        self.btnFindCode.setTitleUnderline(LOCALIZED.how_to_find_the_code.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13))
        
        self.btnSkip.setTitleUnderline(LOCALIZED.no_i_don_t_have_one.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
        
        self.tfCode.placeholder = LOCALIZED.enter_scratch_code.translate
        self.lblInputScratchCode.text = LOCALIZED.input_scratch_code_on_card.translate
        self.lblActivateCard.text = LOCALIZED.txt_activate_card.translate
        
        self.btnActivate.setTitle(LOCALIZED.txt_activate_title.translate, for: .normal)
        
        self.lblCopyRight.text = Utils.getCopyright()
    }
}
