//
//  WebPaymentViewController.swift
//  TGCMember
//
//  Created by vang on 7/18/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import WebKit

class WebPaymentViewController: BaseViewController {

    var data: [String: Any?]?
    var successCallback: SuccessedCallback?
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.decorate()
        self.loadWebView()
    }

    @IBAction func onClickedClose(_ sender: Any) {
        self.dismissFromSuperview()
    }
    
    private func decorate() {
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        self.webView.scrollView.showsVerticalScrollIndicator = false
        self.webView.scrollView.showsHorizontalScrollIndicator = false
        
        
    }
    
    private func showWarningPayFailed(_ message: String) {
        Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_cancel_title.translate, LOCALIZED.try_again.translate, confirmCallback: { [weak self] (_) in
            guard let self = self else {return}
                // reload page
                self.dismissFromSuperview()
            
            }, cancelCallback:{ [weak self] (_) in
                guard let self = self else {return}
                self.loadWebView()
               
            })
    }
    
    private func loadWebView() {
        if let tPayementTransaction = self.data, let config = tPayementTransaction["Config"] as? [String: Any?], let purchase =  tPayementTransaction["PurchaseItem"] as? [String: Any?] {
            let paymentPostURL = config["PaymentPostURL"] as? String ?? ""
            
            var urlRequest = URLRequest(url: URL(string: paymentPostURL)!)
            urlRequest.httpMethod = "POST"
           
            let userId = GLOBAL.GlobalVariables.userInfo!["ID"] as? String ?? ""
            let fullName = GLOBAL.GlobalVariables.userInfo!["DisplayName"] as? String ?? ""
            let userEmail = GLOBAL.GlobalVariables.userInfo!["Email"] as? String ?? ""
            
            let params = [
                "app_name": "member",
                "payment_kind": KIND_PAYMENT_METHOD.SUBSCRIPTION.rawValue,
                "package_id": tPayementTransaction["package_id"] as! String,
                "payment_method": tPayementTransaction["payment_method"] as! String,
                "currency_symbol": config["CurrencySymbol"] as? String ?? "",
                "tgc_transaction_id": config["TransactionID"] as? String ?? "",
                "tgc_item_id": config["ItemID"] as? String ?? "",
                "total_amount": purchase["Amount"] as? Int ?? 0,
                "item_name": purchase["Name"] as? String ?? "",
                "item_id": purchase["ID"] as? String ?? "",
                "user_id": userId,
                "full_name": fullName,
                "gateway_id": tPayementTransaction["ID"] as? String ?? "",
                "user_email": userEmail,
                "customer_id": tPayementTransaction["CustomerStripeID"] as? String ?? ""
                ] as [String : Any]
       
            let jsonData = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            urlRequest.httpBody = jsonData
            urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            
            webView.load(urlRequest)
        }
    }

}

extension WebPaymentViewController:  WKUIDelegate, WKNavigationDelegate {
    
    func updateLanguages() {
     
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let tLink = webView.url?.absoluteString {
            print(tLink)
            
            if tLink.contains("checkout") {
                Utils.dismissLoading()
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {

            decisionHandler(WKNavigationActionPolicy.cancel)
            
            return
        } else {
            let tLink = navigationAction.request.url?.absoluteString
            
            if let link = tLink {
                if link.contains("success") {
                    successCallback?(true)
                    self.dismissFromSuperview()
                } else if link.contains("message") {
                    let tMessage = Utils.getQueryStringParameter(url: link, param: "message")
                    
                    self.showWarningPayFailed(tMessage ?? "")
                    
                }
            }
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
}
