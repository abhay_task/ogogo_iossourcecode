//
//  LoginViewController.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Nantes

let defaultBottom: CGFloat = Utils.isIPhoneNotch() ? 20 : 0

public enum INPUT_COLOR {
    case NORMAL
    case ERROR
}

extension INPUT_COLOR {
    var color: UIColor {
        get {
            switch self {
            case .NORMAL:
                return UIColor(205, 207, 214, 1)
            case .ERROR:
                return UIColor(220, 78, 65, 1)
            }
        }
    }
}

class LoginViewController: BaseViewController {

    @IBOutlet weak var heightLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightHeaderConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnSkipNow: HighlightableButton!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var coverEmailView: UIView!
    @IBOutlet weak var coverPassView: UIView!
  
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imageValid: UIImageView!
    @IBOutlet weak var coverEmailWarning: UIView!
    @IBOutlet weak var coverPasswordWarning: UIView!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDontHaveAccount: UILabel!
    @IBOutlet weak var btnSignUp: HighlightableButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: HighlightableButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var lblOR: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var logoTop: UIImageView!
    @IBOutlet weak var coverBody: UIView!
    var flowCallback: LoginCallback?
    var isShowPass: Bool = true
    var referralInfo: [String: Any?]?
    var codeOnCard: String?
    var isSignUpFirst: Bool = false
    var isForcedLogin: Bool = false
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }

    
    @IBAction func onClickedBack(_ sender: Any) {

        self.dismissKeyboard()
        
        if let callback = self.flowCallback {
            callback(LOGIN_STATUS.NOT_YET)
        } else {
           self.navigationController?.popViewController(animated: true)
        }
    
    }
    
    @IBAction func onClickedSkipNow(_ sender: Any) {
        self.dismissKeyboard()
        
        if let homeVC = GLOBAL.GlobalVariables.discoveryInstance {
            self.navigationController?.popToViewController(homeVC, animated: true)
        } else {
            let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    @IBAction func onClickedForgot(_ sender: Any) {
        self.dismissKeyboard()
        
        self.navigationController?.pushViewController(ForgotPasswordViewController(), animated: true)
    }

    
    @IBAction func onClickedSignUp(_ sender: Any) {
        self.dismissKeyboard()
        
        if self.codeOnCard != nil || self.isSignUpFirst {
            self.navigationController?.popViewController(animated: true)
        } else {
            let signUpVC = SignUpViewController()
            signUpVC.flowCallback = self.flowCallback
            
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
        
        
    }
    
    @IBAction func onClickedShowPass(_ sender: Any) {
        self.tooglePassword()
    }
    
    private func tooglePassword() {
        isShowPass = !isShowPass
        
        self.tfPassword.isSecureTextEntry = !isShowPass
        
        self.btnShowPass.setImage(isShowPass ? UIImage(named: "show_pass") : UIImage(named: "hide_pass"), for: .normal)
    }
    
    @IBAction func onClickedSignIn(_ sender: Any) {        

        doLogin()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        decorate()
        
        if let referral = self.referralInfo {
            self.tfEmail.text = referral["Email"] as? String
        }
        
        _ = validateAllField()
        self.tooglePassword()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfEmail.text = "sachin@venturepoint.net"
        tfPassword.text = "Ogogo@123"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func decorate() {
        
        coverBody?.layer.cornerRadius = CGFloat(40)
        coverBody?.layer.masksToBounds = true
        coverBody?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        self.btnBack.isHidden = self.isForcedLogin

        self.coverPassView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPassView.layer.borderWidth = 0.5
        self.coverPassView.layer.cornerRadius = 3.0
        self.coverPassView.layer.masksToBounds = true
        
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailView.layer.borderWidth = 0.5
        self.coverEmailView.layer.cornerRadius = 3.0
        self.coverEmailView.layer.masksToBounds = true
        
        self.heightFooterConstraint.constant = Utils.isIPhoneNotch() ? 90 : 70
        
        self.coverEmailWarning.isHidden = true
        self.coverPasswordWarning.isHidden = true
    }
    
    private func validateAllField(_ textCheck: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = true
        
        let isValidEmail = self.validateEmail(textCheck, withoutField)
        let isValidPassword = self.validatePassword(textCheck, withoutField)
        
        if !isValidEmail || !isValidPassword {
            isValid = false
        }
        
        self.btnSignIn.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSignIn.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnSignIn.layer.applySketchShadow()
        } else {
            self.btnSignIn.layer.removeSketchShadow()
        }
        
        
        return isValid
    }
    
    private func validateEmail(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
    
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverEmailWarning.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.imageValid.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverEmailView.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
     
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailWarning.isHidden  = true
        self.imageValid.isHidden  = true
    }
    
    private func validatePassword(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let checkText = withoutField == self.tfPassword ? pText : self.tfPassword.text
        if checkText != "" {
            isValid = false
            if let text = checkText, text.count > 5 {
                isValid = true
            }
            
            self.coverPasswordWarning.isHidden  = withoutField == self.tfPassword ? true : isValid
            self.coverPassView.layer.borderColor = withoutField == self.tfPassword ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
           
        } else {
            self.resetViewPassword()
        }
        
        return isValid
    }
    
    private func resetViewPassword() {
        self.coverPassView.layer.borderColor =  INPUT_COLOR.NORMAL.color.cgColor
        self.coverPasswordWarning.isHidden  = true
    }
    
    
    
    private func setupPrivacyPolicyLabel() {
        self.footerView.removeSubviews()
        
        let labelTerm: NantesLabel = .init(frame: .zero)
        labelTerm.delegate = self
        labelTerm.numberOfLines = 0
        labelTerm.textAlignment = .center
        labelTerm.font = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0)
        labelTerm.textColor =  UIColor(hexFromString: "#6B6B6B")
        
        let text = "\(LOCALIZED.by_signing_in_i_accept_the.translate) \(LOCALIZED.terms_and_conditions.translate)"
        labelTerm.text = text
        labelTerm.linkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelTerm.activeLinkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#E46D57"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        labelTerm.addLink(to: URL(string: TERMS_POLICY)!, withRange: (text as NSString).range(of: LOCALIZED.terms_and_conditions.translate))
        
        self.footerView.addSubview(labelTerm)
        
        labelTerm.snp.makeConstraints { (make) in
            make.left.equalTo(self.footerView).offset(5)
            make.centerY.equalTo(self.footerView)
            make.right.equalTo(self.footerView).offset(-5)
        }
    
    }
    
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - self.heightFooterConstraint.constant - 10
            self.bottomBodyConstraint?.constant = tBottom
            
            self.heightHeaderConstraint.constant = 150
            self.heightLogoConstraint.constant = 71
            self.centerLogoConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    guard let self = self else {return}
                    self.headerView.superview?.layoutIfNeeded()
                    self.coverBody.superview?.layoutIfNeeded()
                }, completion:  { finished in
                    self.heightHeaderConstraint.constant = 150
                    self.heightLogoConstraint.constant = 71
                    self.centerLogoConstraint.constant = 0
                    
                    self.bottomBodyConstraint?.constant = tBottom
            })
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint?.constant = 0
        
        self.heightHeaderConstraint.constant = 240
        self.heightLogoConstraint.constant = 104
//        self.centerLogoConstraint.constant = 25
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self = self else {return}
                self.headerView.superview?.layoutIfNeeded()
                self.coverBody.superview?.layoutIfNeeded()
            }, completion:  { finished in
                self.heightHeaderConstraint.constant = 240
                self.heightLogoConstraint.constant = 87
//                self.centerLogoConstraint.constant = 25
                
                self.bottomBodyConstraint?.constant = 0
        })
    }
    
    private func doLogin() {
        self.dismissKeyboard()
        
        if !validateAllField() {
            return
        }
        
        Utils.showLoading()
        
        let email = self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let password = self.tfPassword.text ?? ""

        let request = LoginRequest(email: email, password: password, userID: Utils.getUserId(), pushingToken: GLOBAL.GlobalVariables.fcmToken)
        
//        repository.login(request: request, completionHandler: { [weak self] result in
//            guard let self = self else { return }
//            
//            switch result {
//            case .success(let response):
//                if response.status {
////                    Utils.setU
//                }
//            case .failure(let error):
//                break
//            }
//        })
        
        APICommonServices.login(email, password) { (resp) in
            //mPrint("login", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                    let userInfo = resp["user"] as? [String: Any?]
                    
                    Utils.setUserInfo(userInfo)
                    Utils.setAppToken(resp["token"] as? String)
                    Utils.updatePushingToken()
                    
                    NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                    
                    if GLOBAL.GlobalVariables.isHomeScreenReady {
                        NotificationCenter.default.post(name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
                    }
                    
                    Repository.shared.getInfoTarget(completionHandler: { result in
                        switch result {
                        case .success(let infoTarget):
                            CacheManager.shared.infoTarget = infoTarget
                        case .failure(let error):
                            print(print(error.message))
                        }
                        
                        if let callback = self.flowCallback {
                            callback(LOGIN_STATUS.SUCCESS_AFTER)
                        } else {
                            let sweepstakeId = Utils.getSweepstakeId()
                            if sweepstakeId != "" {
                                Utils.showLoading()
                                Utils.submitSweepstakeResult(sweepstakeId, onCompletion: {  [weak self] (success, data) in
                                    guard let self = self else {return}
                                    
                                    Utils.dismissLoading()
                                    
                                    if success {
                                        if let homeVC = GLOBAL.GlobalVariables.discoveryInstance {
                                            self.navigationController?.popToViewController(homeVC, animated: true)
                                        } else {
                                            let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                            self.navigationController?.pushViewController(homeVC, animated: true)
                                        }
                                    } else {
                                        let tMessage = data as? String ?? ""
                                        Utils.showConfirmAlert(self, "", tMessage, LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                                            guard let self = self else {return}
                                            
                                            let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                            self.navigationController?.pushViewController(homeVC, animated: true)
                                            }, cancelCallback: nil)
                                    }
                                })
                            } else {//if Utils.isActivateCard() {
                                if let homeVC = GLOBAL.GlobalVariables.discoveryInstance {
                                    self.navigationController?.popToViewController(homeVC, animated: true)
                                } else {
                                    let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                                    self.navigationController?.pushViewController(homeVC, animated: true)
                                }
                            }
//                                else {
//                                if let tCode = self.codeOnCard {
//                                    self.doActivate(tCode)
//                                } else {
//                                    let newUpgradeVC = NewUpgradeViewController()
//                                    newUpgradeVC.initPlanIndex = UPGRADE_PLAN.BASIC.viewIndex
//
//                                    self.navigationController?.pushViewController(newUpgradeVC, animated: true)
//                                }
//
//                            }
                        }
                    })
                      
                } else {
                    Utils.showAlertStyle(resp["message"] as? String ?? "", false)
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func dismissKeyboard() {
        self.tfEmail.resignFirstResponder()
        self.tfPassword.resignFirstResponder()
    }
    
    private func doActivate(_ tCode: String) {
        Utils.showLoading()
        
        APICommonServices.activateCard(tCode) { (resp) in
            mPrint("activateCard when SIGN_IN finished --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                
                if status {
                    let data = resp["data"] as? [String: Any?]
                    let userInfo = data != nil ? data!["user"] as? [String: Any?] : nil
                    
                    Utils.setUserInfo(userInfo)
                }
                
                let homeVC = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                self.navigationController?.pushViewController(homeVC, animated: true)
            }
            
            Utils.dismissLoading()
        }
    }

}

extension LoginViewController: NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        Utils.openURL(link.absoluteString)
    }
}


extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.tfEmail {
            tfPassword.becomeFirstResponder()
        } else {
            self.doLogin()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateAllField(updatedText, textField)
            
            if textField == self.tfPassword {
                textField.text = updatedText
                
                return false
            }
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            } else if textField == self.tfPassword {
                self.resetViewPassword()
            }
        }, 100)
      
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       _ = validateAllField()
    }
}

extension LoginViewController {
    func updateLanguages() {
        lblWarning.text = LOCALIZED.please_enter_a_valid_email.translate
        self.lblSignIn.text = LOCALIZED.txt_sign_in.translate
        self.btnSignIn.setTitle(LOCALIZED.txt_sign_in.translate, for: .normal)
        self.lblEmail.text = LOCALIZED.txt_email_title.translate
        self.tfEmail.placeholder = LOCALIZED.enter_your_email_address.translate
        self.lblPassword.text = LOCALIZED.txt_password_title.translate
        self.tfPassword.placeholder = LOCALIZED.enter_password.translate
        self.lblOR.text = LOCALIZED.txt_or_title.translate
        self.btnSkipNow.setTitleUnderline(LOCALIZED.skip_for_now.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
        
        self.btnForgotPassword.setTitleUnderline(LOCALIZED.forgot_password_ask.translate, color: UIColor(hexFromString: "#6B6B6B"), font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13.0))
        
        self.lblDontHaveAccount.text = LOCALIZED.dont_have_an_account_yet.translate
        
        self.btnSignUp.setTitleUnderline(LOCALIZED.please_sign_up_here.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
        
        setupPrivacyPolicyLabel()
    }
}
