//
//  AddWishViewController.swift
//  TGCMember
//
//  Created by jonas on 4/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit
import Toast_Swift

class AddWishViewController: BaseViewController, UITextFieldDelegate {
    @IBOutlet weak var addToWishListButton: CustomButton!
    @IBOutlet weak var nameOfWishTextField: UITextField!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var whatIsYourWishLabel: UILabel!
    @IBOutlet weak var selectCategoryInteractiveView: InteractiveView!
    @IBOutlet weak var selectCategoryLabel: UILabel!
    
    var viewModel: WishlistViewModel!
    
    lazy var autoCompleteView: SuggestionAutocompleteView = {
        return SuggestionAutocompleteView()
    }()
    
    var onAdd: VoidCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupActions()
        addToWishListButton.isEnabled = viewModel.wishToEdit != nil
        nameOfWishTextField.delegate = self
        
        nameOfWishTextField.text = viewModel.wishToEdit?.name
        selectCategoryLabel.text = viewModel.wishToEdit?.categoryName
        addToWishListButton.setTitle(viewModel.wishToEdit == nil ? "Add to Wishlist" : "Update Wish", for: .normal)
        
        let categories = CacheManager.shared.infoTarget?.data.categories ?? []
        if let index = categories.firstIndex(where: { $0.id == viewModel.wishToEdit?.categoryIDs?.first }) {
            viewModel.selectedCategory = categories[index]
        }
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        let position = nameOfWishTextField.convert(nameOfWishTextField.bounds, to: view)
//        let point = CGPoint(x: position.minX, y: position.maxY)
//        
//        self.autoCompleteView.show(in: view,
//                        frame: CGRect(origin: point,
//                                      size: CGSize(width: nameOfWishTextField.bounds.width,
//                                                   height: (55.0 * 5))),
//                        list: ["1", "23", "44"])
//    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        
        viewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
        }
        
        viewModel.onComplete = { [weak self] in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.dismissLoadingIndicator()
                self.onAdd?()
                let alertView = TGCAlertViews.loadInfo(title: LOCALIZED.wish_created_title.translate,
                                                       message: LOCALIZED.wish_created_message.translate,
                                                       buttonTitle: LOCALIZED.okay.translate)
                Utils.showAlertWithCustomView(alertView)
                alertView.onTapInfoButton = { [unowned self] in
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
        viewModel.onEditComplete = { [weak self] in
            guard let self = self else { return }
            
            self.dismissLoadingIndicator()
            self.view.endEditing(true)
            
            self.showToast(text: "Wish item successfully updated!")
        }
        
        viewModel.onCompleteWithMessage = { [weak self] message in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.dismissLoadingIndicator()
                self.showAlert(message: message)
            }
        }
    }
    
    private func showToast(text: String) {
        var style = ToastStyle()
        style.cornerRadius = 20
        
        let point = CGPoint(x: view.center.x, y: view.frame.maxY * 0.9)
        view.makeToast(text,
                  point: point
                  , title: nil, image: nil, style: style, completion: nil)
    }
    
    func setupActions() {
        let categories = CacheManager.shared.infoTarget?.data.categories ?? []
        selectCategoryInteractiveView.onTap = { [unowned self] _ in
            nameOfWishTextField.resignFirstResponder()
            let catView = CategoryListView.loadNib()
            let data = categories.map { ($0.imageURLSelected ?? "", $0.name ?? "") }
            let index = categories.firstIndex(where: { $0.id == (viewModel.selectedCategory?.id ?? viewModel.wishToEdit?.categoryIDs?.first) })

            catView.setData(data, selectedIndex: index ?? 0)
            
            Utils.showAlertWithCustomView(catView)
            
            catView.onTapSave = { [weak self] index in
                guard let self = self else { return }
                
                let selectedCategory = categories[index]
                self.selectCategoryLabel.text = selectedCategory.name
                self.selectCategoryLabel.textColor = Color.customLabel
                
                self.viewModel.selectedCategory = selectedCategory
                self.viewModel.generateRequest(name: self.nameOfWishTextField.text ?? "")
                self.addToWishListButton.isEnabled = self.nameOfWishTextField.text?.isEmpty != true && self.viewModel.selectedCategory != nil
            }
        }
        
        nameOfWishTextField.addTarget(self, action: #selector(nameTextFieldEditingChanged(_:)), for: .editingChanged)
    }
    
    @objc func nameTextFieldEditingChanged(_ sender: UITextField) {
        addToWishListButton.isEnabled = sender.text?.isEmpty != true && viewModel.selectedCategory != nil
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapAddToWishlistButton(_ sender: Any) {
        viewModel.generateRequest(name: nameOfWishTextField.text)
        viewModel.wishToEdit != nil ? viewModel.editWish() : viewModel.createWish()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension AddWishViewController {
    func updateLanguages() {
        
    }
}
