//
//  WishlistViewController.swift
//  TGCMember
//
//  Created by jonas on 4/14/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class WishlistViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navTitleLabel: UILabel!
    
    var viewModel: WishlistViewModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.getWishlist()
        
        doInfoView()
    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        
        viewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
        }
        
        viewModel.onComplete = { [weak self] in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.dismissLoadingIndicator()
                self.tableView.reloadData()
            }
        }
        
        viewModel.onCompleteWithMessage = { [weak self] message in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.dismissLoadingIndicator()
                self.tableView.reloadData()
                self.showAlert(message: message)
            }
        }
    }
    
    func doInfoView() {
        guard !CacheManager.shared.wishlistOpened else {
            return
        }
        
        CacheManager.shared.wishlistOpened.toggle()
        
        let infoView = TGCAlertViews.loadInfo(message: LOCALIZED.wishlist_info_message.translate, buttonTitle: LOCALIZED.okay.translate)
        Utils.showAlertWithCustomView(infoView)
    }
    
    @IBAction func didTapAddButton(_ sender: Any) {
        goToAddWishVc()
    }
    
    @IBAction func didTapInfoButton(_ sender: Any) {
        let infoView = TGCAlertViews.loadInfo(message: LOCALIZED.wishlist_info_message.translate, buttonTitle: LOCALIZED.okay.translate)
        Utils.showAlertWithCustomView(infoView)
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func goToAddWishVc(wish: WishlistResponse.DataClass.List.Record? = nil) {
        showVc(with: Storyboard.wishlist, classType: AddWishViewController.self, viewController: { vc in
            vc.viewModel = WishlistViewModel(repository: Repository.shared, wish: wish)
            vc.onAdd = { [unowned self] in
                viewModel.getWishlist()
            }
        })
    }
}

extension WishlistViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.wishes.isEmpty && !viewModel.isLoading ? 1 : viewModel.numberOfWishes
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.wishes.isEmpty, !viewModel.isLoading {
            let cell = EmptyWishListCell.dequeue(tableView, indexpath: indexPath)
            cell.onTapMakeWish = { [unowned self] in
                goToAddWishVc()
            }
            return cell
        }
        
        let cell = WishCell.dequeue(tableView, indexpath: indexPath)
        cell.selectionStyle = .none
        cell.categoryNameLabel.text = viewModel.wishes[safe: indexPath.row]?.categoryName ?? "-"
        cell.nameLabel.text = viewModel.wishes[safe: indexPath.row]?.name ?? "-"
        cell.onTapRemove = { [unowned self] in
            let confirmView = TGCAlertViews.loadConfirm(message: LOCALIZED.wishlist_delete_message.translate,
                                                        leftBtnTitle: LOCALIZED.txt_cancel_title.translate,
                                                        rightBtnTitle: LOCALIZED.delete.translate)
            Utils.showAlertWithCustomView(confirmView)
            
            confirmView.onTapRightButton = { [unowned self] in
                viewModel.deleteWish(at: indexPath.row)
            }
        }
        
        cell.onTapEdit = { [unowned self] in
            goToAddWishVc(wish: viewModel.wishes[safe: indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if viewModel.wishes.isEmpty {
            return tableView.frame.height * 0.9
        }
        
        return UITableView.automaticDimension
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return !viewModel.wishes.isEmpty
//    }
//
//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        if viewModel.wishes.isEmpty {
//            return .none
//        }
//
//        return .delete
//    }
}

extension WishlistViewController {
    func updateLanguages() {
        if viewModel.wishes.isEmpty {
            
        }
    }
}

