//
//  DealTableCell.swift
//  TGCMember
//
//  Created by jonas on 1/23/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

protocol DealTableProtocol: SearchResultProtocol {
    
}

class DealTableCell: SearchResultCell {
    @IBOutlet weak var deleteButton: CustomButton?
    var onTapDelete: VoidCallBack?
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        onTapDelete?()
    }
}
