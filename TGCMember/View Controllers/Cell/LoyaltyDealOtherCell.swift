//
//  LoyaltyDealOtherCell.swift
//  TGCMember
//
//  Created by jonas on 3/10/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class LoyaltyDealOtherCell: UICollectionViewCell, CellProvider {
    
    @IBOutlet weak var dealImageView: UIImageView!
}
