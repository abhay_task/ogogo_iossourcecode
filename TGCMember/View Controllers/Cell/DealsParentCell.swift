//
//  WalletDealCell.swift
//  TGCMember
//
//  Created by jonas on 1/17/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class DealsCollectionView: UICollectionView {
    var carouselIndex = 0
}

protocol DealsParentProtocol {
    func dealTypeTitleAt(_ index: Int) -> String
    func listCountAt(_ index: Int) -> Int
    func imageURLStringOfDealTypeLogoAt(_ index: Int) -> String
    func tripDateAt(_ index: Int) -> String?
}
class DealsParentCell: UITableViewCell, CellProvider {
    @IBOutlet weak var dealsCollectionView: DealsCollectionView!
    @IBOutlet weak var dealColorImageView: UIImageView!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var dealTypeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleContainerInteractiveView: InteractiveView!
    @IBOutlet weak var dealsCollectionViewHeight: NSLayoutConstraint!
    
    var onTapSeeAll: VoidCallBack?
    var onTapTitleContainer: VoidCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        dealsCollectionViewHeight.constant = 217
        seeAllButton.setTitleColor(Color.green, for: .normal)
        titleContainerInteractiveView.onTap = { [unowned self] _ in
            onTapTitleContainer?()
        }
    }
    
    func setContents(item: DealsParentProtocol, index: Int) {
        dealTypeLabel.text = item.dealTypeTitleAt(index)
        seeAllButton.setTitle("\(LOCALIZED.see_all.translate.replacingOccurrences(of: "$number", with: "\(item.listCountAt(index))"))", for: .normal)
        dealColorImageView.setKfImage(item.imageURLStringOfDealTypeLogoAt(index),
                                      contentMode: .scaleAspectFit)
        if let tripDateString = item.tripDateAt(index) {
            dateLabel.text = tripDateString
            dateLabel.isHidden = false
            seeAllButton.isHidden = item.listCountAt(index) <= 0
            titleContainerInteractiveView.isUserInteractionEnabled = true
        } else {
            dateLabel.isHidden = true
            seeAllButton.isHidden = item.listCountAt(index) <= 0
            titleContainerInteractiveView.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func didTapSeeAllButton(_ sender: Any) {
        print("SEE ALL")
        onTapSeeAll?()
    }
}

protocol CellProvider {}

extension CellProvider where Self: UITableViewCell {
    static func dequeue(_ tableView: UITableView, indexpath: IndexPath) -> Self {
        let cell = tableView.dequeueReusableCell(withIdentifier: Self.className(), for: indexpath) as! Self
        return cell
    }
}

extension CellProvider where Self: UICollectionViewCell {
    static func dequeue(_ collectionView: UICollectionView, indexpath: IndexPath) -> Self {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: Self.className(), for: indexpath) as! Self
        return cell
    }
}
