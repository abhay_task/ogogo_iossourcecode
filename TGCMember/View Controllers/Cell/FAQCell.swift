//
//  FAQCell.swift
//  TGCMember
//
//  Created by jonas on 9/1/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class FAQCell: UITableViewCell, CellProvider {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var answerLabelContainerView: UIView!
}
