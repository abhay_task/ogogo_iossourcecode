//
//  DialogCategoryCell.swift
//  TGCMember
//
//  Created by jonas on 4/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class DialogCategoryCell: UITableViewCell, CellProvider {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemsStackView: UIStackView!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        itemsStackView.arrangedSubviews.forEach { $0.removeFromView() }
    }
}
