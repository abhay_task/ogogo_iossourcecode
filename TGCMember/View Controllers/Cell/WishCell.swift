//
//  WishCell.swift
//  TGCMember
//
//  Created by jonas on 4/15/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class WishCell: UITableViewCell, CellProvider {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint?
    @IBOutlet weak var leftConstraint: NSLayoutConstraint?
    @IBOutlet weak var removeInteractiveView: InteractiveView?
    
    var onTapRemove: VoidCallback?
    var onTapEdit: VoidCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        removeInteractiveView?.onTap = { [unowned self] _ in
            onTapRemove?()
        }
    }
    
    func setupContents(_ wish: WishProtocol, noPadding: Bool = false) {
        nameLabel.text = wish.name ?? "-"
        categoryNameLabel.text = wish.categoryName ?? "-"
        leftConstraint?.constant = noPadding ? 0 : 20
        rightConstraint?.constant = noPadding ? 0 : 20
    }
    
    @IBAction func didTapEditButton(_ sender: Any) {
        onTapEdit?()
    }
    
    @IBAction func didTapRemoveButton(_ sender: Any) {
        onTapRemove?()
    }
}
