//
//  DealsEmptyCell.swift
//  TGCMember
//
//  Created by jonas on 1/17/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class DealsEmptyCell: UICollectionViewCell, CellProvider {
    
    @IBOutlet weak var messageLabel: UILabel!
}
