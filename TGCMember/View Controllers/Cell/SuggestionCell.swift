//
//  SuggestionCell.swift
//  TGCMember
//
//  Created by jonas on 4/22/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell, CellProvider {
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var vendorNameLabel: UILabel!
}
