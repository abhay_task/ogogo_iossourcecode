//
//  CollectionDealCell.swift
//  TGCMember
//
//  Created by jonas on 1/17/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

protocol WalletDealProtocol {
    func imageURLStringOfDealAt(_ index: Int, groupIndex: Int) -> String
    func vendorTitleAt(_ index: Int, groupIndex: Int) -> String
    func detailsAt(_ index: Int, groupIndex: Int) -> String
    func locationAt(_ index: Int, groupIndex: Int) -> String
    func distanceAt(_ index: Int, groupIndex: Int) -> String
    func imageURLStringOfVendorLogoAt(_ index: Int, groupIndex: Int) -> String
    func dealTypeAt(_ index: Int, groupIndex: Int) -> DealType
    func isFavoriteAt(_ index: Int, groupIndex: Int) -> Bool
    func deleteButtonShouldHide(_ index: Int) -> Bool
    func validityAt(_ index: Int, groupIndex: Int) -> String
}

class DealCell: UICollectionViewCell, CellProvider {
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var vendorTitleLabel: UILabel!
    @IBOutlet weak var dealDetaillsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var vendorLogoLabel: UIImageView!
    @IBOutlet weak var dealTypeImageView: UIImageView!
    @IBOutlet weak var favoriteButton: CustomButton!
    @IBOutlet weak var deleteButton: CustomButton!
    @IBOutlet weak var validityLabel: UILabel!
    
    var onToggleFavorite: VoidCallback?
    var onTapDelete: VoidCallback?
    
    @IBAction func didTapFavoriteButton(_ sender: Any) {
        onToggleFavorite?()
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        onTapDelete?()
    }
    
    func setContents(item: WalletDealProtocol, groupIndex: Int, index: Int) {
        dealImageView.setKfImage(item.imageURLStringOfDealAt(index, groupIndex: groupIndex))
        vendorTitleLabel.text = item.vendorTitleAt(index, groupIndex: groupIndex)
        dealDetaillsLabel.text = item.detailsAt(index, groupIndex: groupIndex)
        locationLabel.text = item.locationAt(index, groupIndex: groupIndex)
        distanceLabel.text = item.distanceAt(index, groupIndex: groupIndex)
        vendorLogoLabel.setKfImage(item.imageURLStringOfVendorLogoAt(index, groupIndex: groupIndex), contentMode: .scaleAspectFit)
        dealTypeImageView.image = item.dealTypeAt(index, groupIndex: groupIndex).iconValue
        favoriteButton.setImage(item.isFavoriteAt(index, groupIndex: groupIndex) ? #imageLiteral(resourceName: "favorite_active") : #imageLiteral(resourceName: "favorite_button"), for: .normal)
        deleteButton.isHidden = item.deleteButtonShouldHide(groupIndex)
        validityLabel.text = item.validityAt(index, groupIndex: groupIndex)
    }
}
