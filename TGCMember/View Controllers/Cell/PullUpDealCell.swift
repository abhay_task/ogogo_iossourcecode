//
//  PullUpDealCell.swift
//  TGCMember
//
//  Created by jonas on 6/13/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class PullUpDealCell: UITableViewCell, CellProvider {

    @IBOutlet weak var statusButton: GradientButton!
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var vendorLogoImageView: UIImageView!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dealNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var favouriteButton: CustomButton!
    
    var onTapFavourite: VoidCallback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        vendorNameLabel.textColor = Color.darkBlue
        dealNameLabel.textColor = Color.darkBlue
        locationLabel.textColor = Color.gray
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        dealImageView.image = nil
        vendorLogoImageView.image = nil
        
        vendorLogoImageView.cancelKF()
        dealImageView.cancelKF()
    }
    
    @IBAction func didTapFavouriteButton(_ sender: Any) {
        onTapFavourite?()
    }
}
