//
//  ConversationCell.swift
//  TGCMember
//
//  Created by jonas on 10/23/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell, CellProvider {

    @IBOutlet weak var otherPeopleImageView: UIImageView!
    @IBOutlet weak var otherPersoNameLabel: UILabel!
    @IBOutlet weak var otherPersonVendorLabel: UILabel!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    var onTapDelete: VoidCallback?
    var onTapPin: VoidCallback?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        otherPeopleImageView.image = nil
        otherPeopleImageView.cancelKF()
    }
    
    @IBAction func didTapDeleteButton(sender: UIButton) {
        onTapDelete?()
    }
    
    @IBAction func didTapPinButon(sender: UIButton) {
        onTapPin?()
    }
}
