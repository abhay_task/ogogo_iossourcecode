//
//  ListSearchCell.swift
//  TGCMember
//
//  Created by jonas on 7/9/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class ListSearchCell: UITableViewCell, CellProvider {
    @IBOutlet weak var textTitleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
}
