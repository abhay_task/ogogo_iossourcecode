//
//  EmptyWishListCell.swift
//  TGCMember
//
//  Created by jonas on 4/15/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class EmptyWishListCell: UITableViewCell, CellProvider {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var makeAWishButton: CustomButton!
    
    var onTapMakeWish: VoidCallback?
    
    @IBAction func didTapMakeAWishButton(_ sender: Any) {
        onTapMakeWish?()
    }
}
