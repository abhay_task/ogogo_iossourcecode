//
//  PlaceCell.swift
//  TGCMember
//
//  Created by jonas on 1/28/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

protocol PlaceProcotol {
    func cityNameAt(_ index: Int) -> String
    func countryNameAt(_ index: Int) -> String
}

class PlaceCell: UITableViewCell, CellProvider {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func setContents(_ item: PlaceProcotol, index: Int) {
        cityNameLabel.text = item.cityNameAt(index)
        countryLabel.text = item.countryNameAt(index)
    }
}
