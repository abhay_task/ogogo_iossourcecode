//
//  InterestedCell.swift
//  TGCMember
//
//  Created by jonas on 1/17/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

protocol InterestedProtocol {
    func imageURLStringOfCategoryAt(_ index: Int) -> String
    func nameOfCategoryAt(_ index: Int) -> String
}

class InterestedCell: UICollectionViewCell, CellProvider {
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var borderView: CustomView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    internal var cellIsSelected: Bool = false {
        didSet {
            checkImageView.isHidden = !cellIsSelected
            borderView.borderColor = cellIsSelected ? .systemYellow : .clear
            borderView.borderWidth = cellIsSelected ? 1 : 0
        }
    }
    
    func setContents(item: InterestedProtocol,
                     index: Int,
                     isSelected: Bool = false) {
//        contentView.bringSubviewToFront(checkImageView)
        cellIsSelected = isSelected
        categoryImageView.setKfImage(item.imageURLStringOfCategoryAt(index), contentMode: .scaleAspectFit)
        categoryNameLabel.text = item.nameOfCategoryAt(index)
        categoryNameLabel.numberOfLines = 0
    }
}
