//
//  SearchResultCell.swift
//  TGCMember
//
//  Created by jonas on 1/21/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

protocol SearchResultProtocol {
    func vendorNameAt(_ index: Int) -> String
    func imageURLStringAt(_ index: Int) -> String
    func detailAt(_ index: Int) -> String
    func addressAt(_ index: Int) -> String
    func distanceAt(_ index: Int) -> String
    func validityStringAt(_ index: Int) -> (String, Bool)
    func promotionTextAt(_ index: Int) -> String
    func imageURLStringOfVendorLogoAt(_ index: Int) -> String
    func dealTypeAt(_ index: Int) -> DealType
    func isBlackOrBasicTypeAt(_ index: Int) -> Bool
}

class SearchResultCell: UITableViewCell, CellProvider {
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var promotionButton: GradientButton! {
        didSet {
            promotionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet weak var vendorLogoLabel: UIImageView!
    @IBOutlet weak var dealTypeImageView: UIImageView!
    
    var onErrorImageSet: IntCallBack?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        dealImageView.image = nil
        vendorLogoLabel.image = nil
        
        dealImageView.cancelKF()
        vendorLogoLabel.cancelKF()
    }
    
    func setContents(_ item: SearchResultProtocol, index: Int) {
        vendorNameLabel.text = item.vendorNameAt(index).uppercased()
        dealImageView.setKfImage(item.imageURLStringAt(index))
        detailLabel.text = item.detailAt(index)
        addressLabel.text = item.addressAt(index)
        promotionButton.isHidden = item.promotionTextAt(index).isEmpty
        promotionButton.setTitle(item.promotionTextAt(index), for: .normal)
        promotionButton.setTitleColor(item.isBlackOrBasicTypeAt(index) ? .white : Color.darkBlue, for: .normal)
        distanceLabel.text = item.distanceAt(index)
//        promotionButton.setBackgroundImage(item.dealTypeAt(index).bgImageValue, for: .normal)
        promotionButton.applyGradient(item.dealTypeAt(index))
        vendorLogoLabel.setKfImage(item.imageURLStringOfVendorLogoAt(index))
        dealTypeImageView.image = item.dealTypeAt(index).iconValue
        let (validity, isExpired) = item.validityStringAt(index)
        validityLabel.textColor = !isExpired ? Color.skyBlue : Color.red
        validityLabel.text = validity
    }
}
