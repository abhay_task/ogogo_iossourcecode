//
//  FilterCell.swift
//  TGCMember
//
//  Created by jonas on 7/3/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell, CellProvider {

    @IBOutlet weak var backgroundCustomView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
}
