//
//  MyProfileViewController.swift
//  TGCMember
//
//  Created by vang on 5/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import TagListView

class MyProfileViewController: BaseViewController {
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userProfilePhotoImageView: UIImageView!
    
    @IBOutlet weak var leftStackView: UIStackView!
    @IBOutlet weak var kidsCountLabel: UILabel!
    @IBOutlet weak var educationValueLabel: UILabel!
    @IBOutlet weak var marriageStatusLabel: UILabel!
    @IBOutlet weak var ageValueLabel: UILabel!
    @IBOutlet weak var genderValueLabel: UILabel!
    @IBOutlet weak var myAddressValueLabel: UILabel!
    @IBOutlet weak var nationalityValueLabel: UILabel!
    @IBOutlet weak var languagesValueLabel: UILabel!
    @IBOutlet weak var partnerImageView: UIImageView!
    
    @IBOutlet weak var partnerLocationLabel: UILabel!
    @IBOutlet weak var partnerSubLabel: UILabel!
    @IBOutlet weak var partnerNameLabel: UILabel!
    @IBOutlet weak var tagListView: TagListView! {
        didSet {
            tagListView.textFont = UIFont(name: Font.RobotoRegular, size: 13.5) ?? .systemFont(ofSize: 13.5)
            
        }
    }
    @IBOutlet weak var editButton: UnderlinedButton!
    @IBOutlet weak var profilePhotoContainerInteractiveView: InteractiveView!
    
    @IBOutlet weak var genderContainerView: UIView!
    @IBOutlet weak var bdayContainerView: UIView!
    @IBOutlet weak var marriageContainerView: UIView!
    @IBOutlet weak var interestedInContainerView: UIView!
    @IBOutlet weak var educationLevelContainerView: UIView!
    @IBOutlet weak var kidCountContainerView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var vendorContainerSpace: NSLayoutConstraint!
    @IBOutlet weak var upgradeButton: UIButton!
    @IBOutlet weak var membershipLabel: UILabel!
    @IBOutlet weak var upgradeButtonContainerView: UIView!
    @IBOutlet weak var vendorContainerInteractiveView: InteractiveView!
    @IBOutlet weak var interestedLabel: UILabel!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var myWishListButton: CustomButton!
    @IBOutlet weak var subscriptionHistoryButton: CustomButton!
    @IBOutlet weak var activityLogsButton: CustomButton!
    @IBOutlet weak var contactButton: CustomButton!
    @IBOutlet weak var deleteMyAccountButton: CustomButton!
    
    var work: DispatchWorkItem?
    var viewModel: MyProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        setupActions()
    }
    
    
    override func setupUI() {
        editButton.setTitle(LOCALIZED.txt_edit.translate, for: .normal)
        editButton.setTitleColor(Color.red, for: .normal)
        upgradeButton.setTitle(LOCALIZED.txt_upgrade_title.translate.uppercased(), for: .normal)
        interestedLabel.text = "\(LOCALIZED.label_interested.translate):"
        vendorNameLabel.text = LOCALIZED.partner_name.translate
        myWishListButton.setTitle(LOCALIZED.my_wish_list.translate, for: .normal)
        subscriptionHistoryButton.setTitle(LOCALIZED.subscription_history.translate, for: .normal)
        activityLogsButton.setTitle(LOCALIZED.activity_logs.translate, for: .normal)
        contactButton.setTitle(LOCALIZED.contact_tgc_for_help.translate, for: .normal)
        deleteMyAccountButton.setTitle(LOCALIZED.delete_my_acount.translate, for: .normal)
    }
    
    func setupActions() {
        photoCallback = { [unowned self] data in
            guard let imageData = data?["imageData"] as? Data else { return }
            viewModel.updateAvatar(data: imageData)
            userProfilePhotoImageView.image = UIImage(data: imageData)
            NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
        }
        profilePhotoContainerInteractiveView.onTap = { [unowned self] _ in
            showPickerPhoto()
        }
        
        viewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
        }
        
        viewModel.onComplete = { [unowned self] in
            NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
            setupData()
            dismissLoadingIndicator()
        }
        
        vendorContainerInteractiveView.onTap = { [unowned self] _ in
            guard viewModel.vendor?.summary?.businessName != nil else { return }
            
            if let id = viewModel.vendor?.summary?.vendorID {
                self.openChatDetail(["PartnerID": id])
            }
            
        }
    }
    
    func setupData() {
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            upgradeButtonContainerView.isHidden = !_upgradeModel.canUpNextStep
            
        } else {
            upgradeButtonContainerView.isHidden = true
        }
        membershipLabel.text = membershipPlan
        nameLabel.text = name
        emailLabel.text = email
        userProfilePhotoImageView.setKfImage(avatarURL)
        genderContainerView.isHidden = gender == nil
        genderValueLabel.text = gender?.label
        bdayContainerView.isHidden = birthyear == "0"
        ageValueLabel.text = birthyear
        marriageContainerView.isHidden = marriageStatus == nil
        marriageStatusLabel.text = marriageStatus?.label
        educationLevelContainerView.isHidden = educationLevel == nil
        educationValueLabel.text = educationLevel?.label
        let kidsCount = viewModel.user?.details?.kid ?? 0
        kidsCountLabel.text = kidsCount > 0 ? childrenCount(kidsCount > 1 ? " \(LOCALIZED.children.translate)" : " \(LOCALIZED.child.translate)") : "No child"
        interestedInContainerView.isHidden = interestedIn?.data.isEmpty != false
        myAddressValueLabel.superview?.superview?.isHidden = homeAddress.isEmpty
        nationalityValueLabel.superview?.superview?.isHidden = nationality.isEmpty
        languagesValueLabel.superview?.superview?.isHidden = languages.isEmpty
        myAddressValueLabel.text = homeAddress
        nationalityValueLabel.text = nationality
        languagesValueLabel.text = languages
        
        tagListView.isHidden = interestedIn?.data.isEmpty != false
        tagListView.removeAllTags()
        tagListView.addTags(interestedIn?.data.compactMap { $0.name } ?? [])
        
        leftStackView.isHidden = gender == nil && birthyear == "0" && marriageStatus == nil && homeAddress.isEmpty && interestedIn?.data.isEmpty != false
        
        if gender == nil, birthyear == "0",marriageStatus == nil, interestedIn?.data.isEmpty != false, educationLevel == nil, (viewModel.user?.details?.kid ?? 0) <= 0 {
            emptyView.isHidden = true
            vendorContainerSpace.constant = 0
        } else if homeAddress.isEmpty {
            emptyView.isHidden = true
            vendorContainerSpace.constant = 40
        } else {
            emptyView.isHidden = false
            vendorContainerSpace.constant = 40
        }
        //Partner
        partnerNameLabel.text = viewModel.vendor?.summary?.businessName ?? "Unknown"
        partnerImageView.setKfImage(viewModel.vendor?.summary?.logoURL ?? "", placeholder: #imageLiteral(resourceName: "icon_avatar_unknow"))
        partnerSubLabel.isHidden = viewModel.vendor?.summary?.categoryName?.isEmpty != false
        partnerSubLabel.text = viewModel.vendor?.summary?.categoryName
        partnerLocationLabel.isHidden = viewModel.vendor?.summary?.categoryName?.isEmpty != false
        partnerLocationLabel.text = viewModel.vendor?.summary?.address()
    }
    
    func updateLanguages() {
        editButton.setTitle(LOCALIZED.txt_edit.translate, for: .normal)
    }
  
    @IBAction func onClickedBack(_ sender: Any) {
        
        if navigationController?.popViewController(animated: true) == nil {
            dismissWithPopAnim()
        }
    }
    
    @IBAction func didTapUpgradeButton(_ sender: Any) {
        let newUpgradeVC = NewUpgradeViewController()
        
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            newUpgradeVC.initPlanIndex = _upgradeModel.canUpToNextStep.viewIndex
        }
        
        self.navigationController?.pushViewController(newUpgradeVC, animated: true)
    }
    
    @IBAction func didTapEditButton(_ sender: Any) {
        let editProfileView = EditProfileView()
        editProfileView.show(user: self)
        
        editProfileView.onTapGenderView = { [unowned self] editProfileView in
            let pickerView = PickerTableView()
            let genders: [KeyPair<String>] = viewModel.infoTarget?.data.genders.map { ($0.key, $0.value) } ?? []
            let index = genders.firstIndex(where: { $0.value == gender?.label }) ?? 0
            pickerView.show(.gender(genders), selectedRow: index)
            pickerView.onSelectRow = { [unowned self] index in
                let gender = genders[index]
                (editProfileView.genderInteractiveView.subviews.first as? UILabel)?.text = gender.value
                viewModel.request?.gender = gender
            }
        }
        
        editProfileView.onTapBirthYearView = { [unowned self] editProfileView in
            let pickerView = CustomAlertPickerView()
            let elements: [Int] = Array(1940...2020)
            let index = elements.firstIndex(where: { $0 == viewModel.request?.birthYear }) ?? 0
            pickerView.show(elements.map { "\($0)" }, selectedRow: index)
            pickerView.onSelectRow = { [unowned self] index in
                let year = elements[safe: index] ?? 1990
                (editProfileView.ageRangeInteractiveView.subviews.first as? UILabel)?.text = "\(year)"
                viewModel.request?.birthYear = year
            }
        }
        
        editProfileView.onTapMarriageStatusView = { [unowned self] editProfileView in
            let pickerView = PickerTableView()
            let statusList: [KeyPair<String>] = viewModel.infoTarget?.data.marriages.map { ($0.key, $0.value) } ?? []
            let index = statusList.firstIndex(where: { $0.value == marriageStatus?.label }) ?? 0
            pickerView.show(.marriageStatus(statusList), selectedRow: index)
            pickerView.onSelectRow = { [unowned self] index in
                let status = statusList[index]
                (editProfileView.marriageStatusPickerInteractiveView
                    .subviews.first as? UILabel)?.text = status.value
                viewModel.request?.marriageStatus = status
            }
        }
        
        editProfileView.onTapEducationLevelView = { [unowned self] editProfileView in
            let pickerView = PickerTableView()
            let levels: [KeyPair<String>] = viewModel.infoTarget?.data.educationLevels.map { ($0.key, $0.value) } ?? []
            let index = levels.firstIndex(where: { $0.value == educationLevel?.label }) ?? 0
            pickerView.show(.educationLevel(levels), selectedRow: index)
            pickerView.onSelectRow = { [unowned self] index in
                let level = levels[index]
                (editProfileView.educationInteractiveView
                    .subviews.first as? UILabel)?.text = level.value
                viewModel.request?.educationLevel = level
            }
        }
        
        editProfileView.onTapInterestedInView = { [unowned self] editprofileView in
            let checkListView = CheckListView()
            let list = viewModel.infoTarget?.data.categories ?? []
            let selectedIndices: [Int] = viewModel.request?.categories.compactMap { cat in list.firstIndex(where: { item in item.id == cat.id  }) } ?? []
            
            checkListView.show(items: list, selectedIndices: selectedIndices)
            checkListView.onChangeSelectedIndices = { indices in
                let interestedInArray = indices.map { list[$0] }
                (editProfileView.interestedInPickerInteractiveView.subviews.first as? UILabel)?.text = interestedInArray.compactMap { $0.name }.joined(separator: ",")
                viewModel.request?.categories = interestedInArray
                editProfileView.tagListView.removeAllTags()
                editProfileView.tagListView.addTags(interestedInArray.map { $0.name ?? "" })
            }
        }
        
        editProfileView.onTapNationalityView = { [unowned self] editProfileView in
            let listSearchDialogView = ListSearchDialogView.loadView()
            let nationalities = Repository.shared.toCodableObject([Country].self, from: "countries")?.compactMap { $0.nationality } ?? []
            listSearchDialogView.config(title: "Select your nationality",
                                        list: nationalities,
                                        selectedStrings: viewModel.request?.nationality ?? "")
            
            listSearchDialogView.onSelect = { strings in
                let joined = strings.joined(separator: ",")
                (editProfileView.nationalityInteractiveView
                    .subviews.first as? UILabel)?.text = joined
                viewModel.request?.nationality = joined
            }
            
            Utils.showAlertWithCustomView(listSearchDialogView)
        }
        
        editProfileView.onTapMyAddressView = { [unowned self] editProfileView in
            let addressSearchDialogView = AddressSearchDialogView.loadView()
            addressSearchDialogView.config(title: "My Address",
                                           subtitle: "Add Address",
                                           list: [],
                                           currentAddress: viewModel.request?.homeAddress)
            
            Utils.showAlertWithCustomView(addressSearchDialogView)
            
            addressSearchDialogView.onAddresstextChanged = { [weak self] items in
                guard let self = self else { return }
                
                self.viewModel.autoCompleteRequest?.cancel()
                self.work?.cancel()
                
                self.work = DispatchWorkItem(block: {
                    self.viewModel.getPlaceAutocomplete(items.0, completion: { predictions in
                        DispatchQueue.main.async {
                            addressSearchDialogView.config(title: "My Address", subtitle: "Add Address", list: predictions)
                        }
                    })
                })
                
                items.1.onSelect = { item in
                    addressSearchDialogView.clearButton.isHidden = false
                    addressSearchDialogView.searchTextField.text = item.desc
                    items.1.config(list: [])
                    addressSearchDialogView.searchTextField.resignFirstResponder()
                }
                
                if let work = self.work {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: work)
                }
            }
            
            addressSearchDialogView.onTapSaveButton = { text in
                (editProfileView.myAddressInteractiveView
                    .subviews.first as? UILabel)?.text = text
                viewModel.request?.homeAddress = text
            }
        }
        
        editProfileView.onTapLanguageSpokenView = { [unowned self] editProfileView in
            let listSearchDialogView = ListSearchDialogView.loadView()
            let languages = Repository.shared.toCodableObject([UserLanguage].self, from: "languages_list") ?? []
            listSearchDialogView.config(title: "Select your language",
                                        list: languages.map { $0.language },
                                        selectedStrings: viewModel.request?.languages ?? "",
                                        isMultipleSelection: true)
            
            listSearchDialogView.onSelect = { strings in
                let joined = strings.joined(separator: ",")
                (editProfileView.languageSpokenInteractiveView
                    .subviews.first as? UILabel)?.text = joined
                viewModel.request?.languages = joined
            }
            
            Utils.showAlertWithCustomView(listSearchDialogView)
        }
        
        editProfileView.onRemoveInterestedInItem = { [unowned self] title in
            if let index = viewModel.request?.categories.firstIndex(where: { title == $0.name }) {
                viewModel.request?.categories.remove(at: index)
                let interestedInArray = viewModel.request?.categories ?? []
                (editProfileView.interestedInPickerInteractiveView.subviews.first as? UILabel)?.text = interestedInArray.compactMap { $0.name }.joined(separator: ",")
            }
        }
        
        editProfileView.onTapProfessionInteractiveView = { [unowned self] vw in
            let alertView = JobListPickerAlertView.loadView()
            Utils.showAlertWithCustomView(alertView)
        }
        
        editProfileView.onTapWorkAddressInteractiveView = { [unowned self] vw in
            let addressSearchDialogView = AddressSearchDialogView.loadView()
            addressSearchDialogView.config(title: "Work Address",
                                           subtitle: "Add Work Address",
                                           list: [],
                                           currentAddress: nil)//viewModel.request?.homeAddress)
            
            Utils.showAlertWithCustomView(addressSearchDialogView)
            
            addressSearchDialogView.onAddresstextChanged = { [weak self] items in
                guard let self = self else { return }
                
                self.viewModel.autoCompleteRequest?.cancel()
                self.work?.cancel()
                
                self.work = DispatchWorkItem(block: {
                    self.viewModel.getPlaceAutocomplete(items.0, completion: { predictions in
                        DispatchQueue.main.async {
                            addressSearchDialogView.config(title: "Work Address", subtitle: "Add Work Address", list: predictions)
                        }
                    })
                })
                
                items.1.onSelect = { item in
                    addressSearchDialogView.clearButton.isHidden = false
                    addressSearchDialogView.searchTextField.text = item.desc
                    items.1.config(list: [])
                    addressSearchDialogView.searchTextField.resignFirstResponder()
                }
                
                if let work = self.work {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: work)
                }
            }
            
            addressSearchDialogView.onTapSaveButton = { text in
                (editProfileView.workAddressInteractiveView
                    .subviews.first as? UILabel)?.text = text
//                viewModel.request?.homeAddress = text
            }
        }
        
        editProfileView.onTapSalaryInteractiveView = { [unowned self] _ in
            let salaryDialog = SalaryDialogView.loadView()
            
            Utils.showAlertWithCustomView(salaryDialog)
        }
        
        editProfileView.onChangeDisplayName = { [unowned self] name in
            viewModel.request?.displayName = name
        }
        
        editProfileView.onChangeChildrenCount = { [unowned self] count in
            viewModel.request?.childrenCount = count
        }
        
        editProfileView.onTapSave = { [unowned self] in
            viewModel.updateMemberTargetingDetails()
        }
        
        viewModel.onComplete = { [unowned self] in
            NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
            editProfileView.dismiss()
            setupData()
            dismissLoadingIndicator()
        }
    }
    
    @IBAction func didTapSubscriptionHistroyButton(_ sender: Any) {
        Utils.showAvailableSoon()
    }
    
    @IBAction func didTapActivityLogsButton(_ sender: Any) {
        Utils.showAvailableSoon()
    }
    
    @IBAction func didTapContactForHelpButton(_ sender: Any) {
        Utils.sendEmail(delegate: self, subject: "Ogogo Member (iOS)")
    }
    
    @IBAction func didTapDeleteMyAccountButton(_ sender: Any) {
        let confirmAlertView = DeleteConfirmAlertView(type: .account)
        
        Utils.showAlertWithCustomView(confirmAlertView)
        
        confirmAlertView.onTapDelete = {
            Utils.showAvailableSoon()
        }
    }
    
    @IBAction func didTapMyWishlistButton(_ sender: Any) {
        showVc(with: Storyboard.wishlist, classType: WishlistViewController.self, viewController: { vc in
            vc.viewModel = WishlistViewModel(repository: Repository.shared)
        })
    }
    
    @IBAction func didTapMessagesButton(_ sender: Any) {
        showVc(with: Storyboard.messages, classType: MessagesViewController.self)
    }
    //    private func decorate() {
//
//        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
//            coverUpgrade.isHidden = !_upgradeModel.canUpNextStep
//        } else {
//            coverUpgrade.isHidden = true
//        }
//    }

//    private func fillData() {
//        if let _userInfo = GLOBAL.GlobalVariables.userInfo {
//            self.lblName.text = _userInfo["DisplayName"] as? String ?? ""
//            self.lblRole.text = _userInfo["CurrentPlan"] as? String ?? ""
//
//            if let subExpiredAt = _userInfo["SubscriptionExpiredAt"] as? String {
//                self.lblExpired.isHidden = false
//                let tDate = Utils.stringFromStringDate(subExpiredAt, toFormat: "MMM yyyy")
//
//                var textExpired = LOCALIZED.expired_in_date.translate
//                textExpired = textExpired.replacingOccurrences(of: "[%__TAG_DATE__%]", with: tDate)
//
//                self.lblExpired.text = textExpired
//            } else {
//                self.lblExpired.isHidden = true
//            }
//
//            self.loadingAvatar.startAnimating()
//            self.avatarImage.sd_setImage(with:  URL(string: _userInfo["AvatarURL"] as? String ?? ""), completed:  {  [weak self]  (_, _, _, _) in
//                guard let self = self else {return}
//
//                self.loadingAvatar.stopAnimating()
//            })
//
//            if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
//                self.imageUpgrade.sd_setImage(with:  URL(string: _upgradeModel.iconUpgradeNextStep), completed:  {  [weak self]  (_, _, _, _) in
//                    guard let self = self else {return}
//
//                })
//            } else {
//                self.imageUpgrade.image = nil
//            }
//        }
//    }
}

extension MyProfileViewController: ProfileProtocol {
    var childrenCount: ((String) -> String) {
        return { [unowned self] text in "\(viewModel.user?.details?.kid ?? 0)\(text)" }
    }
    
    var name: String { viewModel.user?.displayName ?? "" }
    var email: String { viewModel.user?.email ?? "" }
    var avatarURL: String { viewModel.user?.avatarURL ?? "" }
    var gender: User.Details.LabelValue? { viewModel.user?.details?.gender }
    var birthyear: String { "\(viewModel.user?.details?.birthYear ?? 0)" }
    var marriageStatus: User.Details.LabelValue? { viewModel.user?.details?.marriage }
    var educationLevel: User.Details.LabelValue? { viewModel.user?.details?.education }
    var interestedIn: User.Details.InterestedIn? { viewModel.user?.details?.interestedIn }
    var membershipPlan: String { CacheManager.shared.currentPlan?.name ?? viewModel.user?.currentPlan ?? "Free Membership" }
    var homeAddress: String { viewModel.user?.details?.homeAddress ?? "" }
    var nationality: String { viewModel.user?.details?.nationality ?? ""}
    var languages: String { viewModel.user?.details?.languages ?? "" }
}
