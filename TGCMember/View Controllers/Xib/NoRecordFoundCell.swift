//
//  NoRecordFoundCell.swift
//  TGCMember
//
//  Created by jonas on 2/22/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class NoRecordFoundCell: UITableViewCell, CellProvider {
    
    @IBOutlet weak var mainTitleLabell: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
}
