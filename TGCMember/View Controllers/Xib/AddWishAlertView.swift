//
//  AddWishAlertView.swift
//  TGCMember
//
//  Created by jonas on 5/7/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class AddWishAlertView: UIView, UITextFieldDelegate {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var chooseCategoryInteractiveView: InteractiveView!
    @IBOutlet weak var wishNameTextField: CustomTextField!
    @IBOutlet weak var addWishTitleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var onTapCancel: VoidCallback?
    var onTapSave: GenericCallback<(Int, String)>?
    
    var selectedCategoryIndex: Int = 0
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("AddWishAlertView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        saveButton.isEnabled = false
        categoryNameLabel.text = CacheManager.shared.infoTarget?.data.categories.first?.name
        wishNameTextField.delegate = self
        setupActions()
    }
    
    private func setupActions() {
        let categories = CacheManager.shared.infoTarget?.data.categories ?? []
        chooseCategoryInteractiveView.onTap = { [unowned self] _ in
            wishNameTextField.resignFirstResponder()
            
            let catView = CategoryListView.loadNib()
            let data = categories.map { ($0.imageURLSelected ?? "", $0.name ?? "") }
            catView.setData(data, selectedIndex: selectedCategoryIndex)
            
            Utils.showAlertWithCustomView(catView)
            
            catView.onTapSave = { [weak self] index in
                guard let self = self else { return }
                
                let selectedCategory = categories[index]
                self.categoryNameLabel.text = selectedCategory.name
                self.categoryNameLabel.textColor = Color.customLabel
                self.selectedCategoryIndex = index
            }
        }
        wishNameTextField.addTarget(self, action: #selector(onTextChange(_:)), for: .editingChanged)
    }
    
    @objc func onTextChange(_ sender: UITextField) {
        saveButton.isEnabled = sender.text?.isEmpty != true
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        onTapCancel?()
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        onTapSave?((selectedCategoryIndex, wishNameTextField.text ?? ""))
        Utils.closeCustomAlert()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
