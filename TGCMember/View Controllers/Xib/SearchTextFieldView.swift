//
//  SearchTextFieldView.swift
//  TGCMember
//
//  Created by jonas on 1/21/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit
import AMPopTip
import Toast_Swift

class SearchTextFieldView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var searchTextField: CustomTextField!
    @IBOutlet weak var filterButton: CustomButton!
    
    var onSearch: GenericCallback<String>?
    
    lazy var autoCompleteView: SuggestionAutocompleteView = {
        return SuggestionAutocompleteView()
    }()
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("SearchTextFieldView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        searchTextField.addPadding(.left(10))
        searchTextField.addPadding(.right(25))
        searchTextField.returnKeyType = .search
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(onSearchTextEdit(_:)), for: .editingChanged)
    }
    
    func show(in view: UIView) {
        view.addSubview(self)
        contentView.slideUpPresent({ [unowned self] in
            let list = Array(CacheManager.shared.recentSearches.suffix(5))
            showRecent(with: list, onSelectText: { text in
                searchTextField.text = text
            })
        })
        
        searchTextField.becomeFirstResponder()
        contentView.addTapToDismissKeyboard()
    }
    
    func dismiss() {
        endEditing(true)
        removeRecentView()
        contentView.slideDownDismiss(duration: 0.3, { [weak self] in
            guard let self = self else { return }
            self.removeFromSuperview()
        })
    }
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        guard let text = searchTextField.text, !text.isEmpty else { return }
        CacheManager.shared.appendRecentSearches(with: text)
        self.searchTextField.resignFirstResponder()
        onSearch?(text)
    }
    
    @IBAction func didtapBackButton(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func didTapFilterButton(_ sender: Any) {
//        let popTip = PopTip(frame: .zero)
//        popTip.shouldShowMask = true
//        popTip.maskColor = Color.customLabel.withAlphaComponent(0.7)
//        popTip.bubbleColor = Color.customBackground
//        popTip.cornerRadius = 10
//        popTip.offset = filterButton.frame.width * 0.8
//        popTip.edgeMargin = 10
//
//        let popTipView = FilterPopTipView.loadFromNib()
//        popTipView.selectedType = CacheManager.shared.mapFilterType
//        popTipView.tableView.reloadData()
//        popTipView.tableView.setNeedsDisplay()
//        popTipView.frame.size = CGSize(width: screenWidth * 0.4, height: popTipView.tableView.contentSize.height - 10)
//
//        let sourceRect = convert(filterButton.frame, to: self)
//        popTip.show(customView: popTipView, direction: .auto, in: self, from: sourceRect, duration: .none)
//        endEditing(true)
//
//        popTipView.onSelectType = { type in
//            CacheManager.shared.mapFilterType = type
//        }
//
//        // TODO: - Show toast for not available yet
//        showToast(text: "Not available at this time")
        let filterView = TGCFilterView.loadView()
        Utils.showAlertWithCustomView(filterView)
    }
    
    @objc func onSearchTextEdit(_ sender: UITextField) {
        guard let text = sender.text else { return }
        
        let list = Array(CacheManager.shared.recentSearches.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: text, options: .caseInsensitive) != nil
        }).suffix(5))
        
        showRecent(with: list, onSelectText: { text in
            self.searchTextField.text = text
        })
    }
    
    private func showRecent(with list: [String], onSelectText: @escaping GenericCallback<String>) {
        let position = searchTextField.convert(searchTextField.bounds, to: self)
        let point = CGPoint(x: position.minX, y: position.maxY + 4)
        
        self.autoCompleteView.show(in: self,
                        frame: CGRect(origin: point,
                                      size: CGSize(width: searchTextField.bounds.width,
                                                   height: (45.0 * list.count.toCGFloat))),
                        searches: list)
        self.autoCompleteView.onSelectText = { text in
            onSelectText(text)
        }
    }
    
    private func removeRecentView() {
        self.autoCompleteView.dismiss()
    }
    
    private func showToast(text: String) {
        var style = ToastStyle()
        style.cornerRadius = 20
        
        let point = CGPoint(x: center.x, y: frame.maxY * 0.9)
        makeToast(text,
                  point: point
                  , title: nil, image: nil, style: style, completion: nil)
    }
}

extension SearchTextFieldView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = searchTextField.text, !text.isEmpty else { return false }
        CacheManager.shared.appendRecentSearches(with: text)
        onSearch?(text)
        self.searchTextField.resignFirstResponder()
        return true
    }
}
