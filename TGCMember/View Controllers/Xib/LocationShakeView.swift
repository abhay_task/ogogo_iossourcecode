//
//  LocationShakeView.swift
//  TGCMember
//
//  Created by jonas on 6/10/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class LocationShakeView: UIView {
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var leftButton: CustomButton!
    @IBOutlet weak var rightButton: CustomButton!
    @IBOutlet weak var centerButton: CustomButton!
    
    var onTapLeftButton: VoidCallback?
    var onTapRightButton: VoidCallback?
    var onDismiss: VoidCallback?
    
    func config(title: String? = nil,
                message: NSAttributedString? = nil) {
        alertTitleLabel.text = title
        messageLabel.attributedText = message
    }
    
    static func loadView() -> LocationShakeView {
        let aView = Bundle.main.loadNibNamed("LocationShakeView", owner: self, options: nil)?[safe: 0] as! LocationShakeView
        return aView
    }
    
    @IBAction func didTapLeftButton(_ sender: Any) {
        onTapLeftButton?()
        Utils.closeCustomAlert()
        onDismiss?()
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        onTapRightButton?()
        Utils.closeCustomAlert()
        onDismiss?()
    }
    
    @IBAction func didTapCenterButton(_ sender: Any) {
        UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
        Utils.closeCustomAlert()
        onDismiss?()
    }
}
