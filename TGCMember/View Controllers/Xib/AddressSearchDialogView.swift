//
//  AddressSearchDialogView.swift
//  TGCMember
//
//  Created by jonas on 7/9/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class AddressSearchDialogView: UIView {
    @IBOutlet weak var dialogTitleLabel: UILabel!
    @IBOutlet weak var addAddressLabel: UILabel!
    @IBOutlet weak var addressListView: SuggestionAutocompleteView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var leftButton: CustomButton!
    @IBOutlet weak var rightButton: CustomButton!
    @IBOutlet weak var clearButton: UIButton!
    
    var onAddresstextChanged: GenericCallback<(String, SuggestionAutocompleteView)>?
    
    var onTapSaveButton: StringCallBack?
    
    func config(title: String? = nil, subtitle: String? = nil, list: [PlacePredictionResponse.Prediction], currentAddress: String? = nil) {
        if let currentAddress = currentAddress {
            searchTextField.text = currentAddress
        }
    
        clearButton.isHidden = currentAddress?.isEmpty != false
        dialogTitleLabel.text = title
        addAddressLabel.text = subtitle
        addressListView.config(list: list)
        searchTextField.addTarget(self, action: #selector(onSearchTextFieldTextChanged(sender:)), for: .editingChanged)
    }
    
    static func loadView() -> AddressSearchDialogView {
        let aView = Bundle.main.loadNibNamed(AddressSearchDialogView.className(), owner: self, options: nil)?[safe: 0] as! AddressSearchDialogView
        return aView
    }
    
    @IBAction func didTapLeftButton(_ sender: Any) {
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        if let text = searchTextField.text, !text.isEmpty {
            onTapSaveButton?(text)
            
            Utils.closeCustomAlert()
        }
    }
    
    @objc func onSearchTextFieldTextChanged(sender: UITextField) {
        guard let text = sender.text else { return }
        
        onAddresstextChanged?((text, addressListView))
    }
    
    @IBAction func didTapClearButton(_ sender: UIButton) {
        sender.isHidden = true
        searchTextField.text = ""
        onAddresstextChanged?(("", addressListView))
    }
}
