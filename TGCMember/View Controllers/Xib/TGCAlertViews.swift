//
//  TGCAlertViews.swift
//  TGCMember
//
//  Created by jonas on 4/20/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class TGCAlertViews: UIView, UITextViewDelegate {
    // Info
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoButton: CustomButton!
    @IBOutlet weak var infoMessageLabel: UITextView!
    // Confirm
    @IBOutlet weak var confirmIconImageView: UIImageView!
    @IBOutlet weak var leftButton: CustomButton!
    @IBOutlet weak var rightButton: CustomButton!
    @IBOutlet weak var confirmMessageLabel: UILabel!
    
    var onTapLeftButton: VoidCallback?
    var onTapRightButton: VoidCallback?
    var onTapInfoButton: VoidCallback?
    
    var onTapURL: VoidCallback?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    static func loadInfo(title: String? = nil,
                         message: String?,
                         attributedMessage: NSAttributedString? = nil,
                         buttonTitle: String = "Okay") -> TGCAlertViews {
        let aView = Bundle.main.loadNibNamed("TGCAlertViews", owner: self, options: nil)?[safe: 0] as! TGCAlertViews
        aView.titleLabel.isHidden = title == nil
        aView.titleLabel.text = title
        aView.infoMessageLabel.isHidden = message == nil && attributedMessage == nil
        
        if let attributedMessage = attributedMessage {
            aView.infoMessageLabel.isSelectable = true
            aView.infoMessageLabel.delegate = aView
            aView.infoMessageLabel.textAlignment = .center
            aView.infoMessageLabel.attributedText = attributedMessage
            aView.infoMessageLabel.linkTextAttributes = [NSAttributedString.Key.font: UIFont(name: Font.RobotoMedium, size: 17)!,
                                                         NSAttributedString.Key.foregroundColor: Color.yellow]
        } else {
            aView.infoMessageLabel.text = message
        }

        
        aView.infoButton.setTitle(buttonTitle, for: .normal)
        return aView
    }
    
    static func loadConfirm(image: UIImage = #imageLiteral(resourceName: "alert_failed_ic"),
                            message: String?,
                            leftBtnTitle: String = "Cancel",
                            rightBtnTitle: String = "Delete") -> TGCAlertViews {
        let aView = Bundle.main.loadNibNamed("TGCAlertViews", owner: self, options: nil)?[safe: 1] as! TGCAlertViews
        aView.confirmIconImageView.image = image
        aView.confirmMessageLabel.text = message
        
        aView.leftButton.setTitle(leftBtnTitle, for: .normal)
        aView.rightButton.setTitle(rightBtnTitle, for: .normal)
        return aView
    }
    
    @IBAction func didTapInfoButton(_ sender: Any) {
        Utils.closeCustomAlert()
        onTapInfoButton?()
    }
    
    @IBAction func didTapLeftButton(_ sender: Any) {
        Utils.closeCustomAlert()
        onTapLeftButton?()
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        Utils.closeCustomAlert()
        onTapRightButton?()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        onTapURL?()
        return true
    }
}
