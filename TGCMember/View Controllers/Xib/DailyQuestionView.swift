//
//  DailyQuestionView.swift
//  TGCMember
//
//  Created by jonas on 11/19/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class DailyQuestionView: CustomView {
    
    @IBOutlet weak var dailyQuestionsLabel: UILabel!
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var onTapX: VoidCallback?
    var onTapCheck: VoidCallback?
    
    static func loadView() -> DailyQuestionView {
        let aView = Bundle.main.loadNibNamed(DailyQuestionView.className(), owner: self, options: nil)?[safe: 0] as! DailyQuestionView
        return aView
    }
    
    @IBAction func didTapXButton(_ sender: UIButton) {
        Utils.closeCustomAlert()
        onTapX?()
    }
    
    @IBAction func didTapCheckButton(_ sender: UIButton) {
        Utils.closeCustomAlert()
        onTapCheck?()
    }
}
