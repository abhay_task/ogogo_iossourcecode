//
//  CustomActionSheetView.swift
//  TGCMember
//
//  Created by jonas on 10/24/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class CustomActionSheetView: UIView {
    @IBOutlet weak var pinInteractiveView: InteractiveView!
    @IBOutlet weak var archiveInteractiveView: InteractiveView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var pinlabel: UILabel!
    @IBOutlet weak var archiveLabel: UILabel!
    
    var onTapPin: VoidCallback?
    var onTapArchive: VoidCallback?
    var onDismiss: VoidCallback?
    
    static func loadView() -> CustomActionSheetView {
        let aView = Bundle.main.loadNibNamed(CustomActionSheetView.className(), owner: self, options: nil)?[safe: 0] as! CustomActionSheetView
        return aView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pinInteractiveView.onTap = { [unowned self] _ in
            onTapPin?()
        }
        
        archiveInteractiveView.onTap = { [unowned self] _ in
            onTapArchive?()
        }
        
        let tapToDismissGest = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        addGestureRecognizer(tapToDismissGest)
    }
    
    @objc func dismissView() {
        actionSheetView.slideDownDismiss({
            self.removeFromSuperview()
        })
        onDismiss?()
    }
    
    func show() {
        actionSheetView.slideUpPresent()
    }
}
