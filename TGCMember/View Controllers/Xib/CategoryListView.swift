//
//  CategoryListView.swift
//  TGCMember
//
//  Created by jonas on 4/6/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class CategoryListView: UIView {
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var servicesButton: UIButton!
    @IBOutlet weak var productsButton: UIButton!
    @IBOutlet weak var selectCategoryLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: DialogCategoryCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: DialogCategoryCell.className())
        }
    }
    
    private var selectedIndex: Int = 0
    private var data = [(String, String)]()
    
    var onTapSave: GenericCallback<Int>?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    static func loadNib() -> CategoryListView {
        return Bundle.main.loadNibNamed("CategoryListView", owner: self, options: nil)?.first as! CategoryListView
    }
    
    func setData(_ data: [(String, String)], selectedIndex: Int = 0) {
        self.data = data
        self.selectedIndex = selectedIndex
        tableView.reloadData()
    }
    
    @IBAction func didTapProductsButton(_ sender: Any) {
        
    }
    
    @IBAction func didTapServicesButton(_ sender: Any) {
        
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        onTapSave?(selectedIndex)
        Utils.closeCustomAlert()
    }
}

extension CategoryListView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DialogCategoryCell.dequeue(tableView, indexpath: indexPath)
        guard let dta = self.data[safe: indexPath.row] else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.iconImageView.setKfImage(dta.0, contentMode: .scaleAspectFit)
        cell.nameLabel.text = dta.1
        cell.checkImageView.isHidden = selectedIndex != indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let prevIndexPath = IndexPath(row: selectedIndex, section: 0)
        selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath, prevIndexPath], with: .none)
    }
}
