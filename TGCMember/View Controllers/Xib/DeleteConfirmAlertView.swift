//
//  DeleteConfirmAlertView.swift
//  TGCMember
//
//  Created by jonas on 1/26/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

enum DeleteAlertType {
    case generic(String)
    case account
}

class DeleteConfirmAlertView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertMessageLabel: UILabel!
    @IBOutlet weak var detailsStackView: UIStackView!
    @IBOutlet weak var buttonHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorLineImageView: UIImageView!
    
    var onTapCancel: VoidCallback?
    var onTapDelete: VoidCallback?
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        
        super.init(frame: frame)
        commonInit()
        handleType(.generic("Are you sure you want to delete this deal?"))
    }
    
    convenience init(type: DeleteAlertType = .generic("Are you sure you want to delete this deal?")) {
        self.init()
        
        handleType(type)
    }
    
    private func handleType(_ type: DeleteAlertType) {
        switch type {
        case .generic(let message):
            alertMessageLabel.text = message
            alertTitleLabel.isHidden = true
            separatorLineImageView.isHidden = true
        case .account:
            alertTitleLabel.isHidden = false
            separatorLineImageView.isHidden = false
            buttonHeight.constant = 65
            detailsStackView.spacing = 20
            alertImageView.image = #imageLiteral(resourceName: "trash_ic")
            alertMessageLabel.text = "Are you sure you want to delete your account? If you delete your account, you will permanently lose your profile, deals and offers."
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DeleteConfirmAlertView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func didTapCancelButton(_ sender: Any) {
        Utils.closeCustomAlert()
        onTapCancel?()
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        Utils.closeCustomAlert()
        onTapDelete?()
    }
}
