//
//  TGCFilterView.swift
//  TGCMember
//
//  Created by jonas on 11/14/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class TGCFilterView: UIView {
    @IBOutlet weak var filter: UILabel!
    @IBOutlet weak var resetButton: UIButton! {
        didSet {
            let resetAttr = NSAttributedString(string: "Reset",
                                               attributes: [NSAttributedString.Key.font: UIFont(name: Font.RobotoRegular, size: 15)!,
                                                                             NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
            resetButton.setAttributedTitle(resetAttr, for: .normal)
        }
    }
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var distanceSlider: TGCSlider!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            let nib = UINib(nibName: InterestedCell.className(), bundle: nil)
            collectionView.register(nib, forCellWithReuseIdentifier: InterestedCell.className())
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    var categories: [Wallet.Category]? { CacheManager.shared.infoTarget?.data.categories }
    
    var onTapCancel: VoidCallback?
    var onTapSave: VoidCallback?
    
    static func loadView() -> TGCFilterView {
        let aView = Bundle.main.loadNibNamed(TGCFilterView.className(), owner: self, options: nil)?[safe: 0] as! TGCFilterView
        return aView
    }
    
    @IBAction func didTapResetButton(_ sender: UIButton) {
        
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        Utils.closeCustomAlert()
        onTapCancel?()
    }
    
    @IBAction func didTapSaveButton(_ sender: UIButton) {
        Utils.closeCustomAlert()
        onTapSave?()
    }
}

extension TGCFilterView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categories?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = InterestedCell.dequeue(collectionView, indexpath: indexPath)
        let category = categories?[safe: indexPath.item]
        cell.categoryImageView.setKfImage(category?.imageURLSelected ?? "", contentMode: .scaleAspectFit)
        cell.categoryNameLabel.text = category?.name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width * 0.28,
                      height: collectionView.frame.width * 0.28)
    }
}
