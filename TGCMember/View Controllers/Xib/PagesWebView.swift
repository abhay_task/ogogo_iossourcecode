//
//  PagesWebView.swift
//  TGCMember
//
//  Created by jonas on 5/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit
import WebKit

protocol PagesWebViewDelegate {
    func redirectedUrl(with request: URLRequest, webView: PagesWebView)
}

class PagesWebView: UIView {

    @IBOutlet var contentView: UIView!
    var webView: WKWebView!
    var delegate: PagesWebViewDelegate?
    
    let activityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("PagesWebView", owner: self, options: nil)
        guard let content = contentView else { return }
        
        var newBounds = self.bounds
        newBounds.size.height = newBounds.height + (Device.iPhoneWithNotch ? 110 : 80)
        content.frame = newBounds
        
        content.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(content)
    }
    
    func initView(data: Data, loadingYOffset: CGFloat? = nil){
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: contentView.frame, configuration: webConfiguration)
        webView.autoresizesSubviews = true
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        contentView.addSubview(webView)
        contentView.clipsToBounds = true
        webView.scrollView.showsVerticalScrollIndicator = false
        
//        guard let htmlString = String(data: data, encoding: .utf8) else { return }
//        webView.loadHTMLString(htmlString, baseURL: nil)
        guard let baseURL = URL(string: BASE_URL) else { return }
        
        webView.load(data, mimeType: "text/html", characterEncodingName: "utf-8", baseURL: baseURL)
        
        addActivityIndicator(loadingYOffset: loadingYOffset != nil ? loadingYOffset! : 0)
    }
    
    func initView(urlRequest: URLRequest, loadingYOffset: CGFloat? = nil){
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: contentView.frame, configuration: webConfiguration)
        webView.autoresizesSubviews = true
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        contentView.addSubview(webView)
        contentView.clipsToBounds = true
        
        webView.load(urlRequest)
        
        addActivityIndicator(loadingYOffset: loadingYOffset != nil ? loadingYOffset! : 0)
    }
    
    func addActivityIndicator(loadingYOffset: CGFloat){
        activityIndicatorView.style = .gray
        activityIndicatorView.autoresizesSubviews = true
        activityIndicatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        activityIndicatorView.center = CGPoint(x: contentView.center.x, y: contentView.center.y + loadingYOffset)
    }
    
    func removeActivityIndicator(){
        activityIndicatorView.removeFromSuperview()
    }
}

extension PagesWebView: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        removeActivityIndicator()
        
//        let button = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 50)))
//        button.backgroundColor = .green
//
//        webView.addSubview(button)
//
//        button.addTarget(self, action: #selector(byu), for: .touchUpInside)
    }
    
    @objc func byu() {
        removeFromView()
    }
}
