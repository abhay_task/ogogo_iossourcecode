//
//  ListSearchDialogView.swift
//  TGCMember
//
//  Created by jonas on 7/8/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class ListSearchDialogView: UIView {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var dialogTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.separatorStyle = .singleLine
            tableView.separatorColor = Color.customLabel
            tableView.dataSource = self
            tableView.delegate = self
            let nib = UINib(nibName: ListSearchCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: ListSearchCell.className())
        }
    }
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var leftButton: CustomButton!
    @IBOutlet weak var rightButton: CustomButton!
    @IBOutlet weak var xButton: UIButton!
    
    private var list = [String]()
    private var filteredList = [String]()
    
    var onSelect: GenericCallback<[String]>?
    
    private var selectedStrings = [String]()
    private var isSearching: Bool = false
    private var isMultipleSelection: Bool = false
    private var searchText: String = "" {
        didSet {
            if searchText.isEmpty {
                filteredList = list
            } else {
                filteredList = list.filter({ $0.lowercased().contains(searchText.lowercased()) })
            }
            
            tableView.reloadData()
        }
    }
    
    func config(title: String? = nil,
                list: [String],
                selectedStrings: String,
                isMultipleSelection: Bool = false,
                isSearchable: Bool = true) {
        self.list = list
        self.filteredList = list
        self.searchView.isHidden = !isSearchable
        
        xButton.isHidden = true
        self.selectedStrings = selectedStrings.split(separator: ",").map({ $0.trimmingCharacters(in: .whitespaces) })
        self.isMultipleSelection = isMultipleSelection
        dialogTitleLabel.text = title
        searchTextField.addTarget(self, action: #selector(onSearchTextChanged(sender:)), for: .editingChanged)
    }
    
    static func loadView() -> ListSearchDialogView {
        let aView = Bundle.main.loadNibNamed(ListSearchDialogView.className(), owner: self, options: nil)?[safe: 0] as! ListSearchDialogView
        return aView
    }
    
    @IBAction func didTapLeftButton(_ sender: Any) {
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        onSelect?(selectedStrings)
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapXButton(_ sender: UIButton) {
        sender.isHidden = true
        searchTextField.text?.removeAll()
        searchText = ""
    }
    
    @objc func onSearchTextChanged(sender: UITextField) {
        guard let text = sender.text else { return }
        
        searchText = text
        xButton.isHidden = text.isEmpty
    }
}

extension ListSearchDialogView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ListSearchCell.dequeue(tableView, indexpath: indexPath)
        cell.textTitleLabel.text = filteredList[safe: indexPath.row]
        cell.checkImageView.isHidden = !selectedStrings.contains(cell.textTitleLabel.text ?? "")
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ListSearchCell, let title = cell.textTitleLabel.text else { return }
        
        guard isMultipleSelection else {
            selectedStrings = [title]
            tableView.reloadData()
            return
        }
        
        if selectedStrings.contains(title) {
            selectedStrings.removeAll(where: { $0 == title })
        } else {
            selectedStrings.append(title)
        }
        
        tableView.reloadData()
    }
}
