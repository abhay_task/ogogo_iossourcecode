//
//  TGCSlider.swift
//  TGCMember
//
//  Created by jonas on 11/14/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

@IBDesignable
class TGCSlider: UIView, UIGestureRecognizerDelegate {
    @IBInspectable
    var minValue: Int = 0
    @IBInspectable
    var maxValue: Int = 300
    
    @IBInspectable
    var unitValue: String = "km"
    
    @IBInspectable
    var ovalColor: UIColor = UIColor.init(255, 193, 6)
    
    @IBInspectable
    var innerOvalColor: UIColor = .white
    
    @IBInspectable
    var trackColor: UIColor = .gray
    
    var currentValue: Int = 0 {
        didSet {
            indicatorLabel.text = "\(currentValue) \(unitValue)"
        }
    }
    
    lazy var indicatorLabel: UILabel = {
        return UILabel()
    }()
    
    lazy var subIndicatorLabel: UILabel = {
        return UILabel()
    }()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let fillLayer = CAShapeLayer()
        let path = UIBezierPath(rect: CGRect(origin: CGPoint(x: 0, y: rect.height * 0.45),
                                             size: CGSize(width: rect.width, height: rect.height * 0.1)))
        fillLayer.path = path.cgPath
        fillLayer.fillColor = trackColor.cgColor
        
        layer.addSublayer(fillLayer)
        
        let ovalView = UIView(frame: CGRect(origin: .zero,
                                            size: CGSize(width: rect.height * 0.45,
                                                         height: rect.height * 0.45)))
        ovalView.center = CGPoint(x: ovalView.frame.width * 0.5,
                                  y: path.bounds.center.y)
        ovalView.layer.cornerRadius = (rect.height * 0.45) * 0.5
        ovalView.backgroundColor = ovalColor
        let ovalInnerDotLayer = CAShapeLayer()
        ovalInnerDotLayer.path = UIBezierPath(ovalIn: CGRect(origin: .zero,
                                                             size: CGSize(width: ovalView.frame.width * 0.5,
                                                                          height: ovalView.frame.height * 0.5))).cgPath
        ovalInnerDotLayer.frame = UIBezierPath(ovalIn: CGRect(origin: .zero,
                                                              size: CGSize(width: ovalView.frame.width * 0.5,
                                                                           height: ovalView.frame.height * 0.5))).bounds
        ovalInnerDotLayer.position = CGPoint(x: ovalView.center.x, y: ovalView.frame.height * 0.5)
        ovalInnerDotLayer.fillColor = innerOvalColor.cgColor
        ovalView.layer.addSublayer(ovalInnerDotLayer)
        
        [indicatorLabel, subIndicatorLabel].enumerated().forEach {
            $1.text = "\(minValue) \(unitValue)"
            $1.font = UIFont(name: Font.RobotoRegular, size: 14)
            $1.textColor = .gray
            $1.textAlignment = .center
            $1.frame = CGRect(origin: .zero,
                                          size: CGSize(width: indicatorLabel.intrinsicContentSize.width,
                                                                      height: 20))
            if $0 == 0 {
                $1.center = CGPoint(x: ovalView.center.x,
                                    y: ovalView.center.y - 20)
            } else {
                $1.center = CGPoint(x: ovalView.center.x,
                                    y: ovalView.center.y + 20)
            }
            
            addSubview($1)
        }
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didMoveOvalView(_:)))
        panGesture.delegate = self
        ovalView.addGestureRecognizer(panGesture)
        
        addSubview(ovalView)
        startPosition = ovalView.center
    }
    
    var startPosition: CGPoint?
    
    @objc func didMoveOvalView(_ gestureRecogizer: UIPanGestureRecognizer) {
        guard let start = startPosition,
        let movingView = gestureRecogizer.view else { return }
        
        switch gestureRecogizer.state {
        case .began:
            break
        case .changed:
            let translation = gestureRecogizer.translation(in: self)
            
            guard movingView.center.x <= (frame.width - start.x) else {
                movingView.center = CGPoint(x: (frame.width - start.x), y: start.y)
                return
            }
            
            guard movingView.center.x >= start.x else {
                movingView.center = start
                return
            }
            
            let xValue = movingView.center.x + translation.x
            
            movingView.center = CGPoint(x: xValue,
                                        y: movingView.center.y)
            gestureRecogizer.setTranslation(.zero, in: self)
            indicatorLabel.center.x = xValue
            let totalValueInBetween = frame.width - start.x
            let currentValuePercentage = xValue / totalValueInBetween
            let currentVal = Int(CGFloat(maxValue) * currentValuePercentage)
            currentValue = currentVal
            
            if movingView.center.x < start.x {
                currentValue = minValue
                movingView.center = start
                indicatorLabel.center.x = start.x
                return
            }
            
            if movingView.center.x >= (frame.width - start.x) {
                currentValue = maxValue
                movingView.center = CGPoint(x: (frame.width - start.x), y: start.y)
                indicatorLabel.center.x = frame.width - start.x
            }
            
            indicatorLabel.frame.size.width = indicatorLabel.intrinsicContentSize.width
        default:
            break
        }
    }
}
