//
//  JobListPickerAlertView.swift
//  TGCMember
//
//  Created by jonas on 11/12/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation
import UIKit

enum JobType: CaseIterable {
    case accounting
    case adminHr
    case arts
    case sales
    case services
    
    var stringValue: String {
        switch self {
        case .accounting: return "Accounting/Finance"
        case .adminHr: return "Admin/Human Resources"
        case .arts: return "Arts/Media/Communnications"
        case .sales: return "Sales/Marketing"
        case .services: return "Services"
        }
    }
    
    var listValue: [String] {
        return ["Marketing", "Sales-Corporate", "Retail Sales", "Merchandising"]
    }
}

class JobListPickerAlertView: UIView {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableVw: UITableView! {
        didSet {
            tableVw.dataSource = self
            tableVw.delegate = self
            
            let nib = UINib(nibName: ListSearchCell.className(), bundle: nil)
            tableVw.register(nib, forCellReuseIdentifier: ListSearchCell.className())
        }
    }
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    private var selectedIndices = [IndexPath]()
    private var selectedSections = [Int]()
    
    static func loadView() -> JobListPickerAlertView {
        let aView = Bundle.main.loadNibNamed(JobListPickerAlertView.className(), owner: self, options: nil)?[safe: 0] as! JobListPickerAlertView
        return aView
    }
    
    @IBAction func didTapCancelBtn(_ sender: UIButton) {
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapSaveBtn(_ sender: UIButton) {
        Utils.closeCustomAlert()
    }
}

extension JobListPickerAlertView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return JobType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let list = JobType.allCases[section].listValue
        if selectedSections.contains(section) {
            return list.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ListSearchCell.dequeue(tableView, indexpath: indexPath)
        cell.checkImageView.isHidden = !selectedIndices.contains(indexPath)
        cell.textTitleLabel.font = UIFont(name: Font.RobotoRegular, size: 15)
        cell.textTitleLabel.text = JobType.allCases[indexPath.section].listValue[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selectedIndices.firstIndex(of: indexPath) {
            selectedIndices.remove(at: index)
        } else {
            selectedIndices.append(indexPath)
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = InteractiveView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 50)))
        vw.setupGestures()
        let titleLbl = UILabel()
        titleLbl.font = UIFont(name: Font.RobotoMedium, size: 15)
        titleLbl.isUserInteractionEnabled = false
        titleLbl.frame = vw.bounds
        titleLbl.frame.size = CGSize(width: vw.frame.width - 40, height: vw.frame.height)
        titleLbl.center = vw.center
        
        vw.addSubview(titleLbl)
        titleLbl.text = JobType.allCases[section].stringValue
        
        vw.onTap = { [unowned self] _ in
            if let index = selectedSections.firstIndex(of: section) {
                selectedSections.remove(at: index)
            } else {
                selectedSections.append(section)
            }
            
            tableView.reloadData()
        }
        
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
