//
//  TermsConditionsAlertView.swift
//  TGCMember
//
//  Created by jonas on 5/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class TermsConditionsAlertView: UIView {
    
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var noButton: CustomButton!
    @IBOutlet weak var rightButton: CustomButton!
    
    var onTapLeftButton: VoidCallback?
    var onTapRightButton: VoidCallback?
    
    func config(title: String? = nil, message: String? = nil) {
        alertTitleLabel.text = title
        messageLabel.text = message
    }
    
    static func loadView() -> TermsConditionsAlertView {
        let aView = Bundle.main.loadNibNamed("TermsConditionsAlertView", owner: self, options: nil)?[safe: 0] as! TermsConditionsAlertView
        return aView
    }
    
    @IBAction func didTapNoButton(_ sender: Any) {
        onTapLeftButton?()
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapRightButton(_ sender: Any) {
        onTapRightButton?()
        Utils.closeCustomAlert()
    }
}
