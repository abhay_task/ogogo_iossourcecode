//
//  SalaryDialogView.swift
//  TGCMember
//
//  Created by jonas on 11/24/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation
import UIKit

enum SalaryType {
    case monthly(String?)
    case annually(String?)
}

class SalaryDialogView: UIView {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var monthlyBtn: UIButton!
    @IBOutlet weak var annuallyBtn: UIButton!
    @IBOutlet weak var currencyInteractiveView: InteractiveView!
    @IBOutlet var currencyLabels: [UILabel]!
    
    @IBOutlet weak var incomeTextField: CustomTextField!
    
    var selectedSalaryPeriod: SalaryType = .monthly(nil) {
        didSet {
            switch selectedSalaryPeriod {
            case .monthly:
                monthlyBtn.setTitleColor(Color.customBackground, for: .normal)
                monthlyBtn.backgroundColor = Color.yellow
                annuallyBtn.setTitleColor(Color.customLabel, for: .normal)
                annuallyBtn.backgroundColor = Color.customBackground
            case .annually:
                monthlyBtn.setTitleColor(Color.customLabel, for: .normal)
                monthlyBtn.backgroundColor = Color.customBackground
                
                annuallyBtn.setTitleColor(Color.customBackground, for: .normal)
                annuallyBtn.backgroundColor = Color.yellow
            }
        }
    }
    
    static func loadView(with salaryType: SalaryType = .monthly(nil)) -> SalaryDialogView {
        let aView = Bundle.main.loadNibNamed(SalaryDialogView.className(), owner: self, options: nil)?[safe: 0] as! SalaryDialogView
        aView.selectedSalaryPeriod = salaryType
        return aView
    }
    
    @IBAction func didTapMonthlyBtn(_ sender: UIButton) {
        selectedSalaryPeriod = .monthly(incomeTextField.text)
    }
    
    @IBAction func didTapAnnnuallyBtn(_ sender: UIButton) {
        selectedSalaryPeriod = .annually(incomeTextField.text)
    }
    
    @IBAction func didTapCancelBtn(_ sender: UIButton) {
        Utils.closeCustomAlert()
    }
    
    @IBAction func didTapSaveBtn(_ sender: UIButton) {
        Utils.closeCustomAlert()
    }
}
