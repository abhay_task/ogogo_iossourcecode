//
//  EditProfileView.swift
//  TGCMember
//
//  Created by jonas on 12/8/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit
import TagListView

class EditProfileView: UIView {
    // Labels
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var ageRannge: UILabel!
    @IBOutlet weak var marriageStatus: UILabel!
    @IBOutlet weak var numberOfChildren: UILabel!
    @IBOutlet weak var educationLevel: UILabel!
    @IBOutlet weak var interestedIn: UILabel!
    @IBOutlet weak var nationality: UILabel!
    @IBOutlet weak var myAddress: UILabel!
    @IBOutlet weak var languageSpoken: UILabel!
    @IBOutlet weak var jobProfession: UILabel!
    @IBOutlet weak var workAddress: UILabel!
    @IBOutlet weak var salary: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: CustomView!
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var genderInteractiveView: InteractiveView!
    @IBOutlet weak var ageRangeInteractiveView: InteractiveView!
    @IBOutlet weak var marriageStatusPickerInteractiveView: InteractiveView!
    @IBOutlet weak var childrenCountTextField: CustomTextField!
    @IBOutlet weak var educationInteractiveView: InteractiveView!
    @IBOutlet weak var interestedInPickerInteractiveView: InteractiveView!
    
    @IBOutlet weak var nationalityInteractiveView: InteractiveView!
    @IBOutlet weak var myAddressInteractiveView: InteractiveView!
    @IBOutlet weak var languageSpokenInteractiveView: InteractiveView!
    @IBOutlet weak var professionInteractiveView: InteractiveView!
    @IBOutlet weak var workAddressInteractiveView: InteractiveView!
    @IBOutlet weak var salaryInteractiveView: InteractiveView!
    
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var cancelButton: CustomButton!
    @IBOutlet weak var tagListView: TagListView! {
        didSet {
            tagListView.textFont = UIFont(name: Font.RobotoRegular, size: 12) ?? .systemFont(ofSize: 15)
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var editProfileLabel: UILabel!
    
    var onTapGenderView: ((EditProfileView) -> Void)?
    var onTapBirthYearView: ((EditProfileView) -> Void)?
    var onTapMarriageStatusView: ((EditProfileView) -> Void)?
    var onTapEducationLevelView: ((EditProfileView) -> Void)?
    var onTapInterestedInView: ((EditProfileView) -> Void)?
    var onTapProfessionInteractiveView: ((EditProfileView) -> Void)?
    var onTapWorkAddressInteractiveView: ((EditProfileView) -> Void)?
    var onTapSalaryInteractiveView: ((EditProfileView) -> Void)?
    var onChangeDisplayName: ((String) -> Void)?
    var onChangeChildrenCount: ((Int) -> Void)?
    var onRemoveInterestedInItem: ((String) -> Void)?
    
    var onTapNationalityView: ((EditProfileView) -> Void)?
    var onTapMyAddressView: ((EditProfileView) -> Void)?
    var onTapLanguageSpokenView: ((EditProfileView) -> Void)?
    
    var onTapSave: (() -> Void)?
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        
        super.init(frame: frame)
        commonInit()
    }
    
    func setupUI() {
        backgroundColor = .clear
        cancelButton.setTitle(LOCALIZED.txt_cancel_title.translate, for: .normal)
        cancelButton.setTitleColor(Color.red, for: .normal)
        editProfileLabel.text = LOCALIZED.edit_profile.translate
        name.text = LOCALIZED.txt_name_title.translate
        ageRannge.text = LOCALIZED.txt_age_range.translate
        marriageStatus.text = LOCALIZED.marriage_status.translate
        numberOfChildren.text = LOCALIZED.number_of_kids.translate
        gender.text = LOCALIZED.txt_gender.translate
        educationLevel.text = LOCALIZED.education_level.translate
        interestedIn.text = LOCALIZED.interested_in.translate
        saveButton.setTitle(LOCALIZED.txt_save.translate, for: .normal)
        nameTextField.placeholder = LOCALIZED.enter_your_name.translate
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EditProfileView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        
        genderInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapGenderView?(self)
        }
        
        ageRangeInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapBirthYearView?(self)
        }
        
        marriageStatusPickerInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapMarriageStatusView?(self)
        }
        
        educationInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapEducationLevelView?(self)
        }
        
        interestedInPickerInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapInterestedInView?(self)
        }
        
        nationalityInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapNationalityView?(self)
        }
        
        myAddressInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapMyAddressView?(self)
        }
        
        languageSpokenInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapLanguageSpokenView?(self)
        }
        
        professionInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapProfessionInteractiveView?(self)
        }
        
        workAddressInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapWorkAddressInteractiveView?(self)
        }
        
        salaryInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapSalaryInteractiveView?(self)
        }
        
        childrenCountTextField.delegate = self
        nameTextField.delegate = self
        
        addTapToDismissKeyboard()
    }
    
    func show(user: ProfileProtocol) {
        getKeyWindow().addSubview(self)
        parentView.zoomInAnimate()
        
        nameTextField.text = user.name
        (genderInteractiveView.subviews.first as? UILabel)?.text = user.gender?.label
        
        (ageRangeInteractiveView.subviews.first as? UILabel)?.text = user.birthyear
        (marriageStatusPickerInteractiveView.subviews.first as? UILabel)?.text = user.marriageStatus?.label
        childrenCountTextField.text = user.childrenCount("")
        (educationInteractiveView.subviews.first as? UILabel)?.text = user.educationLevel?.label
        (interestedInPickerInteractiveView.subviews.first as? UILabel)?.text = user.interestedIn?.label
        tagListView.addTags(user.interestedIn?.data.compactMap { $0.name } ?? [])
        
        (myAddressInteractiveView.subviews.first as? UILabel)?.text = user.homeAddress.isEmpty ? "Add Address" : user.homeAddress
        (nationalityInteractiveView.subviews.first as? UILabel)?.text = user.nationality.isEmpty ? "Select your nationality" : user.nationality
        (languageSpokenInteractiveView.subviews.first as? UILabel)?.text = user.languages.isEmpty ? "Select your languages" : user.languages
        
        (professionInteractiveView.subviews.first as? UILabel)?.text =  "Select your Job/Profession"
        (workAddressInteractiveView.subviews.first as? UILabel)?.text =  "Add Work Address"
        (salaryInteractiveView.subviews.first as? UILabel)?.text =  "Select your salary"
        
        tagListView.delegate = self
    }
    
    func dismiss() {
        guard parentView != nil else { return }
        parentView.zoomOutAnimate(completion: { [weak self] in
            guard let self = self else { return }
            self.removeFromSuperview()
        })
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        onTapSave?()
    }
    
    @IBAction func didTapDismissButton(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func onChangeDisplayNameText(_ sender: UITextField) {
        guard let text = sender.text else { return }
        onChangeDisplayName?(text)
    }
    
    @IBAction func didChageChildrenCountTextField(_ sender: UITextField) {
        guard let text = sender.text,
              let count = Int(text) else { return }
        onChangeChildrenCount?(count)
    }
}

extension EditProfileView: TagListViewDelegate {
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagListView.removeTag(title)
        onRemoveInterestedInItem?(title)
    }
}

extension EditProfileView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
        return true
    }
}

extension EditProfileView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        endEditing(true)
    }
}
