//
//  PlaceAutocompleteView.swift
//  TGCMember
//
//  Created by jonas on 1/28/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class SuggestionAutocompleteView: UIView {
    @IBOutlet weak var contentView: CustomView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: PlaceCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: PlaceCell.className())
            
            let nib2 = UINib(nibName: SuggestionCell.className(), bundle: nil)
            tableView.register(nib2, forCellReuseIdentifier: SuggestionCell.className())
            
            let nib3 = UINib(nibName: RecentSearchCell.className(), bundle: nil)
            tableView.register(nib3, forCellReuseIdentifier: RecentSearchCell.className())
        }
    }
    
    // Places
    private var list = [PlacePredictionResponse.Prediction]()
    var onSelect: GenericCallback<PlacePredictionResponse.Prediction>?
    var onSelectText: GenericCallback<String>?
    
    private var wishSuggestionList = [String]()
    private var searches = [String]()
    private var isDismissable = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("SuggestionAutocompleteView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        clipsToBounds = false
    }
    
    func show(in view: UIView, frame: CGRect, list: [PlacePredictionResponse.Prediction]) {
        self.frame = frame
        
        self.list = list
        if list.isEmpty {
            dismiss()
        } else {
            view.addSubviewOnce(self)
        }
        
        fadeInPresent()
        tableView.reloadData()
    }
    
    func show(in view: UIView, frame: CGRect, list: [String]) {
        self.frame = frame
        
        self.wishSuggestionList = list
        if list.isEmpty {
            dismiss()
        } else {
            view.addSubviewOnce(self)
        }
        
        fadeInPresent()
        tableView.reloadData()
    }
    
    func show(in view: UIView, frame: CGRect, searches: [String]) {
        self.frame = frame
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        tableView.backgroundColor = .clear
        
        self.searches = searches
        view.addSubviewOnce(self)
        
        fadeInPresent()
        tableView.isScrollEnabled = false
        tableView.reloadData()
    }
    
    func config(list: [PlacePredictionResponse.Prediction]) {
        isDismissable = false
        self.list = list
        tableView.reloadData()
    }
    
    func updateTextList(_ list: [String]) {
        if !searches.isEmpty {
            searches = list
        } else {
            wishSuggestionList = list
        }
        
        tableView.reloadData()
    }
    
    func dismiss() {
        guard isDismissable else { return }
        fadeOutDismiss()
    }
}

extension SuggestionAutocompleteView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !searches.isEmpty {
            return searches.count
        }
        
        if !wishSuggestionList.isEmpty {
            return wishSuggestionList.count
        }
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !searches.isEmpty {
            let cell = RecentSearchCell.dequeue(tableView, indexpath: indexPath)
            cell.selectionStyle = .none
            cell.termlabel.text = searches[safe: indexPath.row]
            return cell
        }
        
        if !wishSuggestionList.isEmpty {
            return SuggestionCell.dequeue(tableView, indexpath: indexPath)
        }
        
        let cell = PlaceCell.dequeue(tableView, indexpath: indexPath)
        cell.setContents(self, index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !searches.isEmpty { return UITableView.automaticDimension }
        return 55
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        if !searches.isEmpty {
            guard let item = searches[safe: indexPath.row] else { return }
            onSelectText?(item)
            dismiss()
        }
        
        if !wishSuggestionList.isEmpty {
            guard let item = wishSuggestionList[safe: indexPath.row] else { return }
            onSelectText?(item)
            dismiss()
        }
        
        if !list.isEmpty {
            guard let item = list[safe: indexPath.row] else { return }
            onSelect?(item)
            dismiss()
        }
        
    }
}

extension SuggestionAutocompleteView: PlaceProcotol {
    func cityNameAt(_ index: Int) -> String {
        return list[safe: index]?.structuredFormatting.mainText ?? ""
    }
    
    func countryNameAt(_ index: Int) -> String {
        return list[safe: index]?.structuredFormatting.secondaryText ?? ""
    }
}
