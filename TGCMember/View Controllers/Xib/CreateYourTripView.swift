//
//  CreateYourTripView.swift
//  TGCMember
//
//  Created by jonas on 1/25/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class CreateYourTripView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var parentView: CustomView!
    @IBOutlet weak var cancelButton: CustomButton!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var goingToNameTextField: CustomTextField!
    @IBOutlet weak var tripNameTextField: CustomTextField!
    @IBOutlet weak var fromDateInteractiveView: InteractiveView!
    @IBOutlet weak var toDateInteractiveView: InteractiveView!
    @IBOutlet weak var travellingWithInteractiveView: InteractiveView!
    @IBOutlet weak var interestedInInteractiveView: InteractiveView!
    @IBOutlet weak var goingForInteractiveView: InteractiveView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var goingToTextFieldClearButton: UIButton!
    
    // Labels
    @IBOutlet weak var planYourTripLabel: UILabel!
    @IBOutlet weak var tripInfopLabel: UILabel!
    @IBOutlet weak var myTripNameLabel: UILabel!
    @IBOutlet weak var goingToLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var extraInfoLabel: UILabel!
    @IBOutlet weak var travellingWithLabel: UILabel!
    @IBOutlet weak var interestedInLabel: UILabel!
    @IBOutlet weak var goingForLabel: UILabel!
    @IBOutlet var selectDateLabels: [UILabel]!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var wishlistTableView: UITableView! {
        didSet {
            let nib = UINib(nibName: WishCell.className(), bundle: nil)
            wishlistTableView.register(nib, forCellReuseIdentifier: WishCell.className())
            wishlistTableView.dataSource = self
            wishlistTableView.delegate = self
        }
    }
    
    @IBOutlet weak var deleteButton: CustomButton!
    @IBOutlet weak var wishListTableViewHeight: NSLayoutConstraint!

    private var willEdit: Bool = false
    var onTapTravellingWithInteractiveView: GenericCallback<CreateYourTripView>?
    var onTapInterestedInInteractiveView: GenericCallback<CreateYourTripView>?
    var onTapGoingForInteractiveView: GenericCallback<CreateYourTripView>?
    
    var onNameChange: GenericCallback<String>?
    var onLocationTextChange: GenericCallback<String>?
    var selectedFromDateChanged: GenericCallback<String>?
    var selectedToDateChanged: GenericCallback<String>?
    var onAddWish: GenericCallback<(Int, String)>?
    var onTapRemoveWish: GenericCallback<Int>?
    var onTapDelete: VoidCallback?
    
    var presentingVC: UIViewController?
    var selectedFromDate: String = "" {
        didSet {
            selectedFromDateChanged?(selectedFromDate)
        }
    }
    var selectedToDate: String = "" {
        didSet {
            selectedToDateChanged?(selectedToDate)
        }
    }
    
    var wishList = [WishProtocol]() {
        didSet { reloadWishListTableView() }
    }
    
    var onTapSave: (() -> Void)?
    
    var goingToTextFieldBottomOrigin: CGPoint {
        let position = goingToNameTextField.convert(goingToNameTextField.bounds, to: containerView)
        let point = CGPoint(x: position.minX, y: position.maxY)
        
        return point
    }
    
    override init(frame: CGRect) {
        let frame = UIScreen.main.bounds
        
        super.init(frame: frame)
        commonInit()
    }
    
    func setupUI() {
        backgroundColor = .clear
        cancelButton.setTitle(LOCALIZED.txt_cancel_title.translate, for: .normal)
        cancelButton.setTitleColor(Color.red, for: .normal)
        tripNameTextField.addTarget(self, action: #selector(onNameTextChanged(_:)), for: .editingChanged)
        goingToNameTextField.addTarget(self, action: #selector(onLocationTextChanged(_:)), for: .editingChanged)
        saveButton.isEnabled = false
        goingToTextFieldClearButton.isHidden = true
        
        planYourTripLabel.text = LOCALIZED.plan_your_trip.translate
        tripInfopLabel.text = LOCALIZED.trip_info_star.translate
        myTripNameLabel.text = LOCALIZED.my_trip_name_star.translate
        tripNameTextField.placeholder = LOCALIZED.please_enter_your_trip_name.translate
        goingToLabel.text = LOCALIZED.i_m_going_to_star.translate
        goingToNameTextField.placeholder = LOCALIZED.enter_here.translate
        fromLabel.text = LOCALIZED.txt_from_star.translate
        toLabel.text = LOCALIZED.txt_to_star.translate
        selectDateLabels.forEach { $0.text = LOCALIZED.select_time.translate }
        extraInfoLabel.text = LOCALIZED.extra_info_dot.translate
        travellingWithLabel.text = LOCALIZED.travelling_with.translate
        interestedInLabel.text = LOCALIZED.im_interested_in.translate
        goingForLabel.text = LOCALIZED.going_for.translate
        saveButton.setTitle(willEdit ? "Update Trip" : LOCALIZED.txt_add_trip.translate, for: .normal)
        goingToNameTextField.delegate = self
        tripNameTextField.delegate = self
        scrollView.delegate = self
        deleteButton.superview?.isHidden = !willEdit
        reloadWishListTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CreateYourTripView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setupUI()
        setupInteractiveViewActions()
    }
    
    private func setupInteractiveViewActions() {
        fromDateInteractiveView.onTap = { [unowned self] ownView in
            endEditing(true)
            if let vc = presentingVC {
                let popup = DatePickerView()
                popup.titleText = "From *"
                
                if let toDate = selectedToDate.toDate(), toDate.isInThePast {
                    popup.maximumDate = Date().toString(format: "yyyy-MM-dd")
                } else {
                    popup.maximumDate = self.selectedToDate
                }
                
                popup.minimumDate = Date().toString(format: "yyyy-MM-dd")
                
                popup.selectedDate = self.selectedFromDate
                
                popup.view.slideUpPresent()
                popup.callback = { [unowned self] (data) in
                    if let tData = data {
                        (ownView.subviews.first(where: { $0 is UILabel }) as? UILabel)?.text = (tData["Date"] as? String ?? "").toDate(format: "yyyy-MM-dd")?.toString(format: "MMM dd, yyyy")
                        selectedFromDate = tData["Date"] as? String ?? ""
                        
                        if let fromDateString = tData["Date"] as? String,
                           let fromDate = fromDateString.toDate(), let toDate = selectedToDate.toDate(), toDate < fromDate {
                            selectedToDate = Date().toString(format: "yyyy-MM-dd")
                            (toDateInteractiveView.subviews.first(where: { $0 is UILabel }) as? UILabel)?.text = selectedToDate.toDate(format: "yyyy-MM-dd")?.toString(format: "MMM dd, yyyy")
                        }
                    }
                }
                
//                vc.addSubviewAsPresent(popup)
                popup.modalTransitionStyle = .crossDissolve
                vc.present(popup, animated: true, completion: nil)
            }
        }
        
        toDateInteractiveView.onTap = { [unowned self] ownView in
            endEditing(true)
            if let vc = presentingVC  {
                let popup = DatePickerView()
                popup.titleText = "To *"
                
                if let fromDate = selectedFromDate.toDate(), fromDate.isInThePast {
                    popup.minimumDate = Date().adjust(.day, offset: 1).toString(format: "yyyy-MM-dd")
                } else {
                    popup.minimumDate = selectedFromDate.isEmpty ? Date().adjust(.day, offset: 1).toString(format: "yyyy-MM-dd") : selectedFromDate
                }
                
                popup.selectedDate = selectedToDate.isEmpty ? Date().adjust(.day, offset: 1).toString(format: "yyyy-MM-dd") : selectedToDate
                
                popup.view.slideUpPresent()
                popup.callback = { [unowned self] (data) in
                    if let tData = data {
                        (ownView.subviews.first(where: { $0 is UILabel }) as? UILabel)?.text = (tData["Date"] as? String ?? "").toDate(format: "yyyy-MM-dd")?.toString(format: "MMM dd, yyyy")
                        selectedToDate = tData["Date"] as? String ?? ""
                    }
                }
                
//                vc.addSubviewAsPresent(popup)
                popup.modalTransitionStyle = .crossDissolve
                vc.present(popup, animated: true, completion: nil)
            }
        }
        
        travellingWithInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapTravellingWithInteractiveView?(self)
        }
        
        interestedInInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapInterestedInInteractiveView?(self)
        }
        
        goingForInteractiveView.onTap = { [unowned self] _ in
            endEditing(true)
            onTapGoingForInteractiveView?(self)
        }
    }
    
    func set(with presentingVC: UIViewController,
             wishList: [WishProtocol]? = nil,
             willEdit: Bool = false,
             trip: TripPreferences? = nil) {
        
        tripNameTextField.text = trip?.name
//        going.text = trip?.goingFor.value
        self.wishList = trip?.wishList ?? wishList ?? []
        self.presentingVC = presentingVC
        self.willEdit = willEdit
        setupUI()
    }
    
    func dismiss() {
        endEditing(true)
        Utils.closeCustomAlert()
    }

    
    @IBAction func didTapSaveButton(_ sender: Any) {
//        if willEdit {
//            Utils.showAvailableSoon()
//            return
//        }
        
        onTapSave?()
    }
    
    @IBAction func didTapDismissButton(_ sender: Any) {
        dismiss()
    }
    
    @objc func onNameTextChanged(_ sender: UITextField) {
        onNameChange?(sender.text ?? "")
    }
    
    @objc func onLocationTextChanged(_ sender: UITextField) {
        onLocationTextChange?(sender.text ?? "")
        goingToTextFieldClearButton.isHidden = sender.text?.isEmpty == true
    }
    
    @IBAction func didTapClearButton(_ sender: UIButton) {
        goingToNameTextField.text?.removeAll()
        onLocationTextChange?("")
        sender.isHidden = true
    }
    @IBAction func didTapAddWishButton(_ sender: Any) {
        let addWishAlertView = AddWishAlertView()
        Utils.showAlertWithCustomView(addWishAlertView)
        
        addWishAlertView.onTapSave = { [unowned self] prop in
            onAddWish?(prop)
        }
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        onTapDelete?()
    }
}

extension CreateYourTripView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
    }
}

extension CreateYourTripView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        endEditing(true)
    }
}

extension CreateYourTripView: UITableViewDataSource, UITableViewDelegate {
    func reloadWishListTableView() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.wishlistTableView.reloadData()
            self.wishListTableViewHeight.constant = 80.0 * self.wishList.count.toCGFloat
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = WishCell.dequeue(tableView, indexpath: indexPath)
        guard let wish = wishList[safe: indexPath.row] else { return UITableViewCell() }
        cell.setupContents(wish, noPadding: true)
        cell.onTapRemove = { [unowned self] in
            onTapRemoveWish?(indexPath.row)
            reloadWishListTableView()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
