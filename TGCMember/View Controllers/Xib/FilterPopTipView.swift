//
//  FilterPopTipView.swift
//  TGCMember
//
//  Created by jonas on 7/3/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class FilterPopTipView: UIView {
    
    enum FilterType: Int, CaseIterable {
        case nearby = 0
        case neighboring
        case country
        case everywhere
        
        var stringValue: String {
            switch self {
            case .country: return "Country"
            case .everywhere: return "Everywhere"
            case .nearby: return "Nearby area"
            case .neighboring: return "Neighboring area"
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: FilterCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: FilterCell.className())
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.isScrollEnabled = false
            tableView.bounces = false
        }
    }
    
    private let types = FilterType.allCases
    var selectedType: FilterType = .nearby
    var onSelectType: GenericCallback<FilterType>?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension FilterPopTipView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FilterCell.dequeue(tableView, indexpath: indexPath)
        cell.backgroundCustomView.backgroundColor = indexPath.row == selectedType.rawValue ? Color.yellow : .clear
        cell.selectionStyle = .none
        cell.titleLabel.text = types[safe: indexPath.row]?.stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let type = types[safe: indexPath.row] else { return }
        
        onSelectType?(type)
    }
}
