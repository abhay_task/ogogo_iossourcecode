//
//  InviteFriendsViewController.swift
//  TGCMember
//
//  Created by vang on 8/23/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class InviteFriendsViewController: BaseViewController {
    func updateLanguages() {
        self.lblPromptPhoto.text = LOCALIZED.your_photo_will_appear.translate
        self.lblInviteFriend.text = LOCALIZED.invite_your_friends_via_email_address.translate
        
    }
    
    var data: [String: Any?]?
    
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var heightCoverEmailConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPromptPhoto: UILabel!
    @IBOutlet weak var btnChangePhoto: UIButton!
    @IBOutlet weak var avatar: UIImageView!
    
    @IBOutlet weak var coverInputView: UIView!
    @IBOutlet weak var tvMessage: UITextView!
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblInviteFriend: UILabel!
    
    @IBOutlet weak var btnAddEmail: UIButton!
    @IBOutlet weak var coverEmailView: UIView!
    
    @IBOutlet weak var coverInput: UIView!
    @IBOutlet weak var coverBtnAdd: UIView!
    @IBOutlet weak var coverAddEmail: UIView!
    @IBOutlet weak var tfEmail: UITextField!
    
    private var assignmentId: String = ""
    private var listEmails:[String] = []
    private var listAssignees:[[String: String]] = []
    private var base64Str: String = ""
    private var totalAssignee: Int = 5
    
    var callback: SuccessedCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        decorate()
        fillData()
        
        _ = validateForm()
        
        renderInviteEmails()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - 120
            self.bottomBodyConstraint.constant = tBottom
            
            setTimeout({
                let bottomOffset: CGPoint = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.scrollView.contentInset.bottom)
                self.scrollView.setContentOffset(bottomOffset, animated: true)
            }, 100)
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint.constant = 30
    }
    
    @IBAction func onClickedAddEmail(_ sender: Any) {
        self.dismissKeyboard()
        
        let tEmail = self.tfEmail.text
        
        self.handleAddEmail(tEmail ?? "")
    }
    
    private func decorate() {
        
        self.coverInput.layer.cornerRadius = 3.0
        self.coverInput.layer.masksToBounds = true
        self.coverInput.layer.borderColor = UIColor(205, 207, 214).cgColor
        self.coverInput.layer.borderWidth = 0.5
        
        self.coverBtnAdd.layer.cornerRadius = 3.0
        self.coverBtnAdd.layer.masksToBounds = true
        self.coverBtnAdd.layer.borderColor = UIColor(205, 207, 214).cgColor
        self.coverBtnAdd.layer.borderWidth = 0.5
        
        self.avatar.layer.borderColor = UIColor(42, 39, 34).cgColor
        self.avatar.layer.borderWidth = 3.5
        self.avatar.layer.masksToBounds = true
        self.avatar.layer.cornerRadius = 52.0
        
        self.btnChangePhoto.setTitleUnderline(LOCALIZED.change_photo.translate, color: UIColor(255, 193, 6), font: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 13))
        
        
        self.coverInputView.layer.borderColor = UIColor(205, 207, 214).cgColor
        self.coverInputView.layer.borderWidth = 0.5
        self.coverInputView.layer.cornerRadius = 3.0
    }
    
    private func fillData() {
        Utils.showLoading()
        
        if let userInfo = GLOBAL.GlobalVariables.userInfo {
            self.avatar.sd_setImage(with:  URL(string: userInfo["AvatarURL"] as? String ?? ""), completed:  {  [weak self]  (image, error, _, _) in
                guard let self = self else {return}
                
                if let tImage = image {
                    let imageData = tImage.jpegData(compressionQuality: 1.0)
                    
                    if let iData = imageData {
                        self.base64Str = iData.base64EncodedString(options: .lineLength64Characters)
                    }
                }

            })
        }
        
        if let tData = self.data {
            updateMessagePrompt(tData)
            if let assignmentProgram = tData["AssignmentProgram"] as? [String: Any?] {
                self.totalAssignee = assignmentProgram["TotalOfRequiredAssignee"] as? Int ?? 5
                self.assignmentId = assignmentProgram["ID"] as? String ?? ""
            }
        }
        
        self.reBuildEmails {
            if self.listEmails.count > self.totalAssignee {
                let sliceArray = self.listEmails.prefix(self.totalAssignee)
                self.listEmails = Array(sliceArray)
            }
            
            //Utils.cachedEmailsTE(self.listEmails)
            
            self.renderInviteEmails()
            self.resetAddEmail()
            
            _ = self.validateForm()
            
            Utils.dismissLoading()
        }
        
    }
    
    private func getListEmailFromString(_ emails: String) -> [String] {
        let arr = emails.split(separator: ",")
        var arrEmails: [String] = []
        for email in arr {
            arrEmails.append(String(email))
        }
        
        return arrEmails
    }
    
    private func reBuildEmails(_ completed: FinishedCallback? = nil) {
        self.listEmails = []
        
        APICommonServices.getTempTEOInfo(self.assignmentId) { [weak self] (resp) in
            guard let self = self else {return}
            mPrint("getTempTEOInfo", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] , let assignment = data["assignment"]  as? [String : Any?] {
                    let emailsTE = assignment["TemporaryEmail"] as? [String] ?? []
                    
                    self.getEmailsValidFromCached(cachedEmails: emailsTE) {
                        self.setTempTEOInfo {
                            completed?()
                        }
                    }
                }
            } else {
                Utils.dismissLoading()
            }
            
        }
    }
    
    private func setTempTEOInfo(completion: FinishedCallback? = nil) {
        let tEmails = self.buildEmails()
            
        APICommonServices.setTempTEOInfo(self.assignmentId, tEmails, self.base64Str, self.lblPrompt.text ?? "", completion: { [weak self] (resp) in
            guard let self = self else {return}
            mPrint("setTempTEOInfo", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {

            }
            
            completion?()
          
        })
    }
    
    private func getEmailsValidFromCached(_ index: Int = 0, cachedEmails: [String], completion: FinishedCallback? = nil) {
        if index == cachedEmails.count {
            completion?()
            
            return
        }
        
        let email = cachedEmails[index]
     
        self.checkEmailFromServer(email, callback: { [weak self] (success, data) in
            guard let self = self else {return}
            
            if success {
                let tEmail = data as? String ?? ""
                if tEmail != "" {
                    self.listEmails.append(tEmail)
                }
            }
            
            self.getEmailsValidFromCached(index + 1, cachedEmails: cachedEmails, completion: completion)
          
        })
       
    }
    
    private func updateMessagePrompt(_ data: [String: Any?]) {
        
        let dealName = "\"\(data["Name"] as? String ?? "")\""
        let vendorName = data["VendorName"] as? String ?? ""
        let expiredDate = Utils.stringFromStringDate(data["EndAt"] as? String, toFormat: "dd MMM")

        var text = LOCALIZED.you_were_assigned_to_the_reward_format.translate
        text = text.replacingOccurrences(of: "[%__TAG_BIG_TITLE__%]", with: dealName)
        text = text.replacingOccurrences(of: "[%__TAG_NAME__%],", with: vendorName)
        text = text.replacingOccurrences(of: "[%__TAG_END_DATE__%]", with: expiredDate)
        //let text = "You were assigned to the reward \(dealName) at \(vendorName), expired in \(expiredDate)."
        
        let mMutableString = NSMutableAttributedString(string: text, attributes:
            [
                NSAttributedString.Key.foregroundColor: UIColor(155, 155, 155),
                NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13.0)!
            ])
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4.0
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        mMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributeString.length))
        
        if let tRange = text.range(of: dealName) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(155, 155, 155), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!, range:nsRange)
        }
        
        if let tRange = text.range(of: vendorName) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(155, 155, 155), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!, range:nsRange)
        }
        
        if let tRange = text.range(of: expiredDate) {
            let nsRange = text.nsRange(from: tRange)
            
            mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(155, 155, 155), range:nsRange)
            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 13.0)!, range:nsRange)
        }
        
        
        self.lblPrompt.attributedText = mMutableString
    }
    
    
    private func dismissKeyboard() {
        self.tvMessage.resignFirstResponder()
        self.tfEmail.resignFirstResponder()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.goBack()
    }
    
    private func goBack() {
        self.dismissKeyboard()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    @IBAction func onClickedChangePhoto(_ sender: Any) {
        self.dismissKeyboard()
        
        self.photoCallback = { [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data, let iData = tData["imageData"] as? Data {
                self.base64Str = iData.base64EncodedString(options: .lineLength64Characters)
                self.avatar.image = UIImage(data: iData)
            }
        }
        
        self.showPickerPhoto()
    }

    
    private func getAssignFromEmail(_ index: Int = 0, completion: FinishedCallback? = nil) {
        if index == self.listEmails.count {
            completion?()
            
            return
        }
        
        let email = self.listEmails[index]
        var dealId = ""
        
        if let tData = self.data {
            dealId = tData["ID"] as? String ?? ""
        }
        
        var cData =  [String: Any]()
        cData["kind"] = LINK_TYPE.TELL_EVERYONE.rawValue
        cData["from_app"] = Constants.BRANCH_CHANNEL
        cData["email"] = email
        cData["deal_id"] = dealId
        
        Utils.getDeepLinkWithCustomData(cData) { (resp) in
            var link = ""
            if let tData = resp {
                link = tData["deepLink"] as? String ?? ""
            }
            
            self.listAssignees.append(["email": email, "deeplink": link])
            
            self.getAssignFromEmail(index + 1, completion: completion)
        }
    }
    
    private func buildEmails() -> String {
        var tEmails = ""
        
        for email in self.listEmails {
            if email != "" {
                if tEmails != "" {
                    tEmails += ","
                }
                
                tEmails += email
            }
        }
        
        return tEmails
    }
    
    
    @IBAction func onClickedSend(_ sender: Any) {
        self.dismissKeyboard()
     
        if !validateForm() {
            return
        }

        Utils.showLoading()

        self.listAssignees = []
        self.getAssignFromEmail {
            APICommonServices.sendInviteFriends(self.base64Str, self.lblPrompt.text ?? "", self.listAssignees) { (resp) in
                mPrint("sendInviteFriends", resp)
                
                if let resp = resp, let status = resp["status"] as? Bool {
                    
                    if status {
                        Utils.showConfirmAlert(self, "", resp["message"] as? String ?? "", LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { [weak self] (_) in
                            guard let self = self else {return}
                            
                            self.callback?(true)
                            //Utils.cachedEmailsTE(nil)
                            
                            self.goBack()
                        }, cancelCallback: nil)
                    } else {
                         Utils.showAlert(self, "", resp["message"] as? String ?? "")
                    }
                }
                
                Utils.dismissLoading()
            }
        }
            

    }
    
    private func validateForm(_ pMessage: String? = nil) -> Bool {
        var message = ""
        
        if pMessage == nil {
            message = self.tfEmail.text ?? ""
        } else {
            message = pMessage ?? ""
        }
        
        var isValid = false
        
        if self.listEmails.count == self.totalAssignee {
            isValid = true
        } else if message != "" {
            self.btnAddEmail.isEnabled = true
            
            let tMessage = message.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if tMessage != "" && self.listEmails.count == self.totalAssignee {
                isValid = true
            }
        } else {
            self.btnAddEmail.isEnabled = false
        }
    
        
        self.btnSend.layer.cornerRadius = 3.0
        self.btnSend.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSend.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnSend.layer.applySketchShadow()
        } else {
            self.btnSend.layer.removeSketchShadow()
        }
        
        if self.listEmails.count == self.totalAssignee {
            self.coverAddEmail.isHidden = true
        } else {
            self.coverAddEmail.isHidden = false
        }
        
        return isValid
    }
    
    private func renderInviteEmails() {
        for subview in self.coverEmailView.subviews {
            subview.removeFromSuperview()
        }
        
        let heightView: CGFloat = 45
        let spaceItem: CGFloat = 10

        var topConstraint: CGFloat = 0
        
        var tDict = [String: Any]()
        for email in self.listEmails {
            tDict["email"] = email
            tDict["action"] = ACTION_INVITE_EMAIL.REMOVE.rawValue
            
            let removeView = InviteFriendView().loadView()
            removeView.config(tDict, onClickedRemoveCallback: { [weak self] (data) in
                guard let self = self else {return}
                if let tData = data {
                    let tEmail = tData["email"] as? String ?? ""
                    self.handleRemoveEmail(tEmail)
                }
            })
            
            self.coverEmailView.addSubview(removeView)
            
            removeView.snp.makeConstraints { (make) in
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.height.equalTo(heightView)
                make.top.equalToSuperview().offset(topConstraint)
            }
            
            topConstraint += (heightView + spaceItem)
        }
        
    
        let heightCoverEmail: CGFloat = (CGFloat(self.listEmails.count) * heightView) + (CGFloat(self.listEmails.count) * spaceItem)
        
        self.heightCoverEmailConstraint.constant = heightCoverEmail
        
    }
    
    private func isExistEmail(_ email: String) -> Bool {
        let existEmail = self.listEmails.first(where: {$0 == email})
        
        if let _ = existEmail {
            return true
        }
        
        return false
    }
    
    private func handleAddEmail(_ pEmail: String) {
        let email = pEmail.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        
        var isValid = true
        var message: String = ""
        
        if !Utils.isValidEmail(email) {
            isValid = false
            message = LOCALIZED.please_enter_a_valid_email.translate
        } else if isExistEmail(email) {
            isValid = false
            
            message = LOCALIZED.email_format_is_existed.translate
            message = message.replacingOccurrences(of: "[%__TAG_EMAIL__%]", with: email)
        }
        
        if isValid {
            self.validateEmailInvite(email)
        } else {
            Utils.showAlert(self, "", message)
        }
    
    }
    
    private func resetAddEmail() {
        self.tfEmail.text = ""
        self.btnAddEmail.isEnabled = false
        
        if self.listEmails.count < self.totalAssignee {
            self.tfEmail.becomeFirstResponder()
        }
    }
    
    private func checkEmailFromServer(_ email: String, callback _callback: SuccessedDataCallback? = nil) {
        APICommonServices.validateInviteEmail(email) { (resp) in
            mPrint("checkEmailFromServer", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status == true {
                    _callback?(true, email)
                } else {
                    _callback?(false, resp["message"] as? String ?? "")
                }
            }
         
        }
    }
    
    private func validateEmailInvite(_ email: String) {
        Utils.showLoading()
        
        self.checkEmailFromServer(email, callback: { [weak self] (success, data) in
            guard let self = self else {return}
            
            let tEmail = data as? String ?? ""
            
            if success {
                self.listEmails.append(tEmail)
                
                Utils.cachedEmailsTE(self.listEmails)
                
                self.setTempTEOInfo {
                    self.renderInviteEmails()
                    
                    self.resetAddEmail()
                    
                    _ = self.validateForm()
                    
                    Utils.dismissLoading()
                }
            } else {
                Utils.dismissLoading()
                
                Utils.showAlert(self, "", tEmail)
            }
        })
        
    }
    
    private func handleRemoveEmail(_ email: String) {
        if let index = self.listEmails.firstIndex(of: email) {
            self.listEmails.remove(at: index)
            
            //Utils.cachedEmailsTE(self.listEmails)
            
            self.renderInviteEmails()
            
            self.setTempTEOInfo()
        }
        
        _ = validateForm()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
}

extension InviteFriendsViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        _ = validateForm()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let tText = textView.text, let textRange = Range(range, in: tText) {
            let updatedText = tText.replacingCharacters(in: textRange, with: text)
            
            _ = self.validateForm(updatedText)
            
        }
        
        return true
    }
}

extension InviteFriendsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _ = validateForm()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateForm(updatedText)
        }
        
        return true
    }
    
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == self.tfCode {
//            self.resetViewCode()
//        }
//
//        return true
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        _ = validateCode()
//    }
}
