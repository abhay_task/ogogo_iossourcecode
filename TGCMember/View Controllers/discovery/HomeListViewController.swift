//
//  HomeListViewController.swift
//  TGCMember
//
//  Created by vang on 11/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class HomeListViewController: BaseViewController {
    func updateLanguages() {
        lblMapView.text = LOCALIZED.map_view.translate
    }
    
    @IBOutlet weak var coverBtnSearch: UIView!
    @IBOutlet weak var coverSearch: UIView!
    @IBOutlet weak var lblMapView: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var coverMenu: UIView!
    @IBOutlet weak var coverMapView: HighlightableView!
    @IBOutlet weak var coverWallet: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var lblTabItems: [UILabel]!
    @IBOutlet var coverTabItems: [UIView]!
    
    var tabClickeCallback: TabClickedCallback?
    
    
    enum TAB_LIST: Int {
        case CATEGORY
        case VENDOR
        case NEWEST
        case MOST_LIKED
    }
    
    private var tempTabIndex: Int = TAB_LIST.CATEGORY.rawValue
    private var mTabBarController: UITabBarController = UITabBarController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        renderContent()
        onActive(fromIndex: 0, toIndex: tempTabIndex)
    }
    
    private func renderContent() {

        let tabActionList = TabCategoryController()
        let tabTripList = TabVendorController()
        let tabEventList = TabNewestController()
        let tabDealAndOffer = TabMostLikedController()
        
        
        mTabBarController.delegate = self
        mTabBarController.viewControllers = [tabActionList, tabTripList, tabEventList, tabDealAndOffer]
        mTabBarController.tabBar.isHidden = true
        mTabBarController.selectedIndex = tempTabIndex
        
        self.contentView.addSubview(mTabBarController.view)
        self.addChild(mTabBarController)
        
        mTabBarController.view.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
    }
    
    func navigate(toIndex: Int) -> Void {
           //  let controllerIndex = tabBarController.viewControllers?.firstIndex(of: viewController)
       
           
           if mTabBarController.selectedIndex == toIndex || toIndex == self.tempTabIndex {
               return;
           }
           
           self.onActive(fromIndex: self.tempTabIndex, toIndex: toIndex)
           
           self.tempTabIndex = toIndex
         
           
           // Get the views.
           let fromView = mTabBarController.selectedViewController?.view
           let toView = mTabBarController.viewControllers![toIndex].view
           
           // Get the size of the view area.
           let viewSize: CGRect = fromView!.frame
           let scrollRight: Bool = toIndex > mTabBarController.selectedIndex
           
           // Add the to view to the tab bar view.
           fromView!.superview?.addSubview(toView!)
           
           // Position it off screen.
           let screenWidth: CGFloat = UIScreen.main.bounds.width
           toView!.frame = CGRect(x: (scrollRight ? screenWidth : -screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
           
           
           UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
               fromView!.frame = CGRect(x: (scrollRight ? -screenWidth : screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
               toView!.frame = CGRect(x: 0, y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
           }, completion: {  [weak self] (finished) in
               guard let self = self else {return}
               
               if finished {
                   // Remove the old view from the tabbar view.
                   fromView!.removeFromSuperview()
                   self.mTabBarController.selectedIndex = toIndex
               }
           })
           
       }
    
    private func onActive(fromIndex: Int, toIndex: Int) {
        
        for cover in coverTabItems {
            cover.backgroundColor = .white
            cover.layer.borderWidth = 0.5
            cover.layer.cornerRadius = 19.0
            cover.layer.borderColor = UIColor(195, 195, 195).cgColor
        }
        
        for label in lblTabItems {
            if label.tag == toIndex {
                label.font = UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 12)
            } else {
                label.font = UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 12)
            }
        }
        
        let coverTab = coverTabItems.first(where: {$0.tag == toIndex})
        coverTab?.backgroundColor = UIColor(255, 193, 6)
        coverTab?.layer.borderWidth = 0
        coverTab?.layer.borderColor = UIColor.clear.cgColor
    
    }
    
    private func decorate() {
        self.topView.backgroundColor = UIColor(255, 193, 6)
        //self.topView.layer.cornerRadius = 19.0
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.topView.frame
        rectShape.position = self.topView.center
        rectShape.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: self.topView.bounds.height), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 40, height: 40)).cgPath
        
        
        self.topView.layer.mask = rectShape
        
        self.topView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        self.coverMenu.backgroundColor = .white
        self.coverMenu.layer.cornerRadius = 20.0
        self.coverMenu.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverWallet.backgroundColor = .white
        self.coverWallet.layer.cornerRadius = 20.0
        self.coverWallet.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverMapView.backgroundColor = UIColor(47, 72, 88)
        self.coverMapView.layer.cornerRadius = 19.0
        self.coverMapView.layer.applySketchShadow(color: UIColor(120, 121, 147, 1), alpha: 0.5, x: 3, y: 4, blur: 20, spread: 0)
        
        self.coverBtnSearch.backgroundColor = UIColor(228, 109, 87, 1)
        self.coverBtnSearch.layer.cornerRadius = 20.0
        self.coverBtnSearch.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverSearch.backgroundColor = UIColor(248, 248, 248)
        self.coverSearch.layer.borderWidth = 0.2
        self.coverSearch.layer.borderColor = UIColor(151, 151, 151).cgColor
        self.coverSearch.layer.cornerRadius = 19.0
        
    }
    
    @IBAction func onClickedTab(_ sender: Any) {
        let button = sender as! UIButton
        
        self.navigate(toIndex: button.tag)
    }
    
    @IBAction func onClickedMapView(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.MAP_VIEW.rawValue)
    }
    
    @IBAction func onClickedMenu(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.MENU.rawValue)
    }
    
    @IBAction func onClickedWallet(_ sender: Any) {
//        Utils.checkLogin(GLOBAL.GlobalVariables.discoveryInstance) { [weak self] (loginStatus) in
//            guard let self = self else {return}
//
//            if loginStatus != .NOT_YET {
//                self.tabClickeCallback?(DISCOVERY_SCREEN_ID.WALLET_VIEW.rawValue)
//            }
//        }
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.WALLET_VIEW.rawValue)
    }
    
    
}


extension HomeListViewController: UITabBarControllerDelegate {
    
    
}
