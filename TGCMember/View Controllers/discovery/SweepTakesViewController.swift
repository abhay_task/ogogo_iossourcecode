//
//  SweepTakesViewController.swift
//  TGCMember
//
//  Created by vang on 8/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class SweepstakeViewController: BaseViewController {
    func updateLanguages() {
        
    }
  
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var wonImage: UIImageView!
    @IBOutlet weak var btnAddToWallet: UIButton!
    @IBOutlet weak var btnSpin: UIButton!
    @IBOutlet weak var coverCard: UIView!
    @IBOutlet weak var coverWheel: UIView!
   
    @IBOutlet weak var heightBigWheelConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBigWhellConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageCircle: UIImageView!
    
    var spinningSmallWheel: TTFortuneWheel?
    var spinningBigWheel: TTFortuneWheelView?
    private var cardResultIndex: Int = 4
    
    private var customAlert: CustomAlertView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        decorate()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        renderSmallWheel()
        renderBigWheel()
        
        setTimeout({
            self.showConfirmRegistration()
        }, 300)
        
    }

    @IBAction func onClickedAddToWallet(_ sender: Any) {
        Utils.keepResultSweeptakes(["result": "$100"])
        
        self.dismissView(true)
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.dismissView(false)
    }
    
    private func dismissView(_ isWin: Bool = true) {
        self.dismissFromSuperview(true, completion: { (finished) in
            self.removeFromParent()
            self.view.removeFromSuperview()
            
            if GLOBAL.GlobalVariables.isCheckReferralScreenReady {
                NotificationCenter.default.post(name: .FINISHED_SWEEPTAKES, object: ["isWin": isWin])
            }
        })
    }
    
    private func decorate() {
        self.btnAddToWallet.layer.cornerRadius = 22.5
        self.btnAddToWallet.layer.applySketchShadow(color: UIColor(67, 67, 67), alpha: 1, x: 0, y: 8, blur: 15, spread: 0)
        
        if UIScreen.main.bounds.height <= 667 {
            self.heightBigWheelConstraint.constant = 800
            self.bottomBigWhellConstraint.constant = -270
        } else {
            self.heightBigWheelConstraint.constant = 900
            self.bottomBigWhellConstraint.constant = -320
        }
        
    }
    
    private func showConfirmRegistration() {
        let content = InputVendorInfo().loadView()
        
        content.config(nil, onClickedSubmitCallback: { [weak self] (data) in
            guard let self = self else {return}
            
                self.customAlert?.close()
            
                self.handleSubmitVendorName(data)
            
            }, onClickedCancelCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.customAlert?.close()
                
            })
        
        Utils.showAlertWithCustomView(content) { [weak self] (customAlert) in
            guard let self = self else {return}
            self.customAlert = customAlert
        }
    }
    
    private func handleSubmitVendorName(_ data: [String: Any?]?) {
        
    }
    
    private func showWon() {
        self.btnAddToWallet.alpha = 1
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            // HERE
            self.wonImage.alpha = 1
            self.wonImage.transform = CGAffineTransform.identity.scaledBy(x: 2, y: 2) // Scale your image
            
        }) { (finished) in
                self.wonImage.alpha = 1
                UIView.animate(withDuration: 1, animations: {
                self.wonImage.transform = CGAffineTransform.identity // undo in 1 seconds
            })
        }
    }
    
    private func renderBigWheel() {
        var imageWidth = 180
        var imageHeight = 269
        
        if UIScreen.main.bounds.height <= 667 {
            imageWidth = 150
            imageHeight = 224
        }
        
        let imgView1 = UIImageView()
        imgView1.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView1.image = UIImage(named: "$10000")
        
        let imgView2 = UIImageView()
        imgView2.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView2.image = UIImage(named: "$1000")
        
        let imgView3 = UIImageView()
        imgView3.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView3.image = UIImage(named: "$100")
        
        let imgView4 = UIImageView()
        imgView4.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView4.image = UIImage(named: "$50")
        
        let imgView5 = UIImageView()
        imgView5.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView5.image = UIImage(named: "Free_card")
        
        let imgView6 = UIImageView()
        imgView6.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight)
        imgView6.image = UIImage(named: "no_win_card")
 
        
        let flatViews = [ WheelFlatView.init(subview: imgView1),
                          WheelFlatView.init(subview: imgView2),
                          WheelFlatView.init(subview: imgView3),
                          WheelFlatView.init(subview: imgView4),
                          WheelFlatView.init(subview: imgView5),
                          WheelFlatView.init(subview: imgView6)
        ]
        
        
        let tX = -((self.coverCard.bounds.height - UIScreen.main.bounds.width) / 2)
        let tFrame = CGRect(x: tX, y: 0, width: self.coverCard.bounds.height, height: self.coverCard.bounds.height)
        
        spinningBigWheel = TTFortuneWheelView(frame: tFrame, flatViews: flatViews)
        spinningBigWheel?.equalSlices = true
        spinningBigWheel?.frameStroke.width = 0
        spinningBigWheel?.flatViews.enumerated().forEach { (pair) in
            let flat = pair.element as! WheelFlatView
            let offset = pair.offset
            switch offset {
            case 0: flat.style = .one
            case 1: flat.style = .two
            case 2: flat.style = .three
            case 3: flat.style = .four
            case 4: flat.style = .five
            case 5: flat.style = .six
                
            default: flat.style = .one
            }
        }
        
        if let wheelView = spinningBigWheel {
            self.coverCard.addSubview(wheelView)
        }
    }
    
    private func renderSmallWheel() {
        self.btnSpin.isHidden = false
        self.imageCircle.isHidden = false
        
        let slices = [ WheelSliceView.init(title: "$10,000"),
                       WheelSliceView.init(title: "$1,000"),
                       WheelSliceView.init(title: "$100"),
                       WheelSliceView.init(title: "$50"),
                       WheelSliceView.init(title: "FREE CARD"),
                       WheelSliceView.init(title: "No win")
        ]
        
        spinningSmallWheel = TTFortuneWheel(frame: self.coverWheel.bounds, slices: slices)
        spinningSmallWheel?.equalSlices = true
        spinningSmallWheel?.frameStroke.width = 0
        spinningSmallWheel?.slices.enumerated().forEach { (pair) in
            let slice = pair.element as! WheelSliceView
            let offset = pair.offset
            switch offset {
            case 0: slice.style = .one
            case 1: slice.style = .two
            case 2: slice.style = .three
            case 3: slice.style = .four
            case 4: slice.style = .five
            case 5: slice.style = .six

            default: slice.style = .one
            }
        }
        
        if let wheelView = spinningSmallWheel {
            self.coverWheel.addSubview(wheelView)
        }
    
    }

    private func disableSpin() {
        self.btnSpin.isUserInteractionEnabled = false
        self.btnSpin.setImage(UIImage(named: "button_spin_disable"), for: .normal)
    }
    
    @IBAction func spin(_ sender: Any) {
    
        self.disableSpin()
        
        spinningSmallWheel?.startAnimating()
       
        setTimeout({
            self.spinningSmallWheel?.startAnimating(fininshIndex: self.cardResultIndex) { (finished) in
                //print(finished)
            }
        }, 4000)
        
        spinningBigWheel?.startAnimating()
        
        setTimeout({
            self.spinningBigWheel?.startAnimating(fininshIndex: self.cardResultIndex) { [weak self] (finished) in
                guard let self = self else { return }
                
                if self.cardResultIndex != 5 {
                    self.showWon()
                } else {
                    self.btnBack.isHidden = false
                }
            }
        }, 4000)
        
        
    }
}
