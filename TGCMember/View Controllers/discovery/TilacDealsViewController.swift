//
//  DailyDealsViewController.swift
//  TGCMember
//
//  Created by vang on 6/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass


private var numberOfCards: Int = 3

class TilacDealsViewController: BaseViewController {
    
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var topVerticalCardConstraint: NSLayoutConstraint!
    
    private var headerView: HeaderSwipeCard?
    
    var selectedCard: [String: Any?]?
    var startLocation:CGPoint = CGPoint.zero
    var originalHeaderFrame: CGRect = .zero
    let disablePanVertical: Bool = true
    
    var draggableCardViews: [DraggableCardView] = []
    var swipeCardFrames: [CGRect] = []
    
    var isLoadFinished: Bool = false
    var tDirection : PanDirection = .None
    var deals: [[String: Any?]] = [["ID": "57d75ded-5081-44fa-9031-462258b3b3cb"], ["ID": "c89c81bf-a978-4cb4-be07-5b2b646e8da5"], ["ID": "3ef8c486-402b-4639-8269-38b32bd03e30"], ["ID": "979d8236-c9f3-463a-b576-5c87949c8228"], ["ID": "f5b39b01-24d2-4363-b935-e2d42b9fe515"]]
    private var dealPending: [String: Any?]?
    private var items:[[String: Any?]] = [] {
        didSet {
            if items.count > 1 {
                carousel.type = .rotary
            } else {
                carousel.type = .coverFlow
            }
            
            carousel.stopAtItemBoundary = true
            
            self.decorateHeader()
        }
        
    }
    
    
    //var deals: [[String: Any?]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        
        carousel.isVertical = true
        self.getListDealsFromIds()
        
    }
    
    private func decorateHeader() {
        guard let _ = self.headerView else {
            
            self.headerView = HeaderSwipeCard().loadView()
            self.headerView?.config(onClickedUndoCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                NotificationCenter.default.post(name: .FORCE_UNDO_ONE_SWIPE_CARD, object: self.dealPending)
                
                self.dealPending = nil
            })
            
            self.view.addSubview(self.headerView!)
            
            
            let heightHeader = Utils.isIPhoneNotch() ? 110 : 100
            self.headerView?.snp.makeConstraints({ (make) in
                make.top.equalTo(self.view).offset(0)
                make.right.equalTo(self.view).offset(0)
                make.width.equalTo(SCREEN_WIDTH)
                make.height.equalTo(heightHeader)
            })
            
            return
        }
    }
    
    
    
    private func updateHeaderView(currentStatus: MASK_DEAL_STATUS = .NONE, pendingStatus: MASK_DEAL_STATUS = .NONE, isHideUndo: Bool = true) {
        var textPending: String = ""
        var textCurrent: String = ""
        let isHideCurrentStatus = !isHideUndo
        let isHideHeader = (currentStatus == .NONE && pendingStatus == .NONE) ? true : false
        
        
        if pendingStatus == .REMOVE {
            textPending = LOCALIZED.refused_offer.translate.uppercased()
        } else if pendingStatus == .CHECK {
            textPending = LOCALIZED.added_to_wallet.translate.uppercased()
        } else if pendingStatus == .DISLIKE {
            textPending = LOCALIZED.offer_blocked.translate.uppercased()
        } else if pendingStatus == .LIKE {
            textPending = LOCALIZED.added_to_favorites.translate.uppercased()
        }
        
        if currentStatus == .REMOVE {
            textCurrent = LOCALIZED.refusing_offer.translate.uppercased()
        } else if currentStatus == .CHECK {
            textCurrent = LOCALIZED.adding_to_wallet.translate.uppercased()
        } else if currentStatus == .DISLIKE {
            textCurrent = LOCALIZED.blocking_offer.translate.uppercased()
        } else if currentStatus == .LIKE {
            textCurrent = LOCALIZED.adding_to_favorites.translate.uppercased()
        }
        
        var tData = [String: Any]()
        tData["isHideUndo"] = isHideUndo
        tData["isHideCurrentStatus"] = isHideCurrentStatus
        tData["textPending"] = textPending
        tData["textCurrent"] = textCurrent
        tData["isHideHeader"] = isHideHeader
        tData["isHidePrompt"] = true
        
        if isHideUndo == false {
            print("")
        }
        
        self.headerView?.updateView(tData)
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.dismissSelf()
    }
    
    private func getListDealsFromIds() {
        Utils.showLoading()
        let ids = self.buildIds(self.deals)
        
        APICommonServices.getListDealsFromPNS(ids) { [weak self] (resp) in
            guard let self = self else {return}
            
            //mPrint("getListDealsFromPNS", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                
                if let data = resp["data"] as? [String: Any?] , let listDeals = data["listDeals"] as? [[String : Any?]]  {
                    self.items = listDeals
                } else {
                    self.items = []
                }
            } else {
                self.items = []
            }
            
            
            self.carousel.reloadData()
            
            Utils.dismissLoading()
            
        }
    }
    
    private func buildIds(_ deals: [[String: Any?]]) -> String {
        var IDs = ""
        
        for item in self.deals {
            let dealId = item["ID"] as? String ?? ""
            
            if dealId != "" {
                if IDs != "" {
                    IDs += ","
                }
                
                IDs += dealId
            }
        }
        
        return IDs
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    private func dismissSelf() {
        self.dismissFromSuperview(true, completion: { [weak self] (finished) in
            guard let self = self else {return}
            
            GLOBAL.GlobalVariables.shouldShowDailyDeals = false
            
            self.removeFromParent()
            self.view.removeFromSuperview()
        })
        
    }
    
    private func decorate() {
        
    }
    
}
extension TilacDealsViewController: iCarouselDataSource, iCarouselDelegate {
    private func removeItemFromList(_ data: [String: Any?]?) {
        //        if let tData = data {
        //            self.items.removeAll(where: { $0["ID"] as? String == tData["ID"] as? String })
        //
        //            if self.items.count == 0 {
        //                self.dismissFromSuperview()
        //            } else {
        //                self.carousel.reloadData()
        //            }
        //
        //        }
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let cardData = self.items[index]
        
        let itemSwipe = ItemCarouselSwipe().loadView()
        itemSwipe.config(cardData, onRemoveCardCallback: {  [weak self] (data) in
            guard let self = self else {return}
            mPrint("call api  REMOVE", data!["ID"] as? String ?? "")
            
            self.removeItemFromList(data)
            
            }, onCheckCardCallback: {  [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  CHECK", cardData!["ID"] as? String ?? "")
                
                self.removeItemFromList(cardData)
                
            }, onDislikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  DISLIKE", cardData!["ID"] as? String ?? "")
                
                self.removeItemFromList(cardData)
            }, onLikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                mPrint("call api  LIKE", cardData!["ID"] as? String ?? "")
                
                self.removeItemFromList(cardData)
                
            }, onNeedUpdateHeaderCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data {
                    let onlyShowHide = tData["onlyShowHide"] as? Bool ?? false
                    let isHidden = tData["isHidden"] as? Bool ?? false
                    
                    if onlyShowHide {
                        self.headerView?.isHidden = isHidden
                    } else {
                        let tCurrentStatus = tData["currentStatus"] as? MASK_DEAL_STATUS ?? MASK_DEAL_STATUS.NONE
                        let tPendingStatus = tData["pendingStatus"] as? MASK_DEAL_STATUS ?? MASK_DEAL_STATUS.NONE
                        let tIsHideUndo = tData["isHideUndo"] as? Bool ?? true
                        
                        self.updateHeaderView(currentStatus: tCurrentStatus, pendingStatus: tPendingStatus, isHideUndo: tIsHideUndo)
                    }
                    
                    
                    let tDealPending = tData["dealPending"] as? [String: Any?] ?? self.dealPending
                    self.dealPending = tDealPending
                    
                    
                }
            }, onSubmitDealIfHaveCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                NotificationCenter.default.post(name: .FORCE_SUBMIT_SWIPE_CARD, object: self.dealPending)
        })
        
        
        return itemSwipe
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        if option == .spacing{
            return 20
        }
        
        return value
    }
}





extension TilacDealsViewController {
    func updateLanguages() {
        
    }
}

