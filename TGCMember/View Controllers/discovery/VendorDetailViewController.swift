//
//  VendorDetailViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class VendorDetailViewController: BaseViewController {
    
    @IBOutlet weak var loadingLogo: UIActivityIndicatorView!
    @IBOutlet weak var loadingBanner: UIActivityIndicatorView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var coverTabDetailView: UIView!
    @IBOutlet weak var backTopConstraint: NSLayoutConstraint!
    
    var summaryData: [String: Any?]?
    var vendorId: String?
    
    var initTab: VENDOR_INIT_TAB = VENDOR_INIT_TAB.ABOUT
    
    @IBAction func onClickedChat(_ sender: Any) {
        Utils.checkLogin(self) { [weak self] (loginStatus) in
            guard let self = self else {return}
            
            if loginStatus != .NOT_YET {
                if let summary = self.summaryData {
                    self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
                }
            }
        }
    }
    
    @IBAction func onClickedMore(_ sender: Any) {
        
    }
    
    private var tabDetailVC: VendorTabDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPage(_:)), name: .REFRESH_VENDOR_DETAIL, object: nil)
        
        // Do any additional setup after loading the view.
        
        backTopConstraint.constant = Utils.isIPhoneNotch() ?  30 : 10
        
        self.fillData()
        
    }
    
    @objc private func refreshPage(_ sender: Any) {
        self.getVendorDetail()
    }
    
    func updateLanguages() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getVendorDetail()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        if self.tabDetailVC != nil {
            self.tabDetailVC?.removeFromParent()
        }
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func fillData() {
        if let data = self.summaryData {
            self.lblVendorName.text  = data["Name"] as? String
            self.lblCategoryName.text = data["CategoryName"] as? String

            self.loadingBanner.startAnimating()
            self.bannerImage.sd_setImage(with:  URL(string: data["BannerURL"] as? String ?? ""), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingBanner.stopAnimating()
            })
            
            self.loadingLogo.startAnimating()
            self.logoImage.sd_setImage(with:  URL(string: data["LogoUrl"] as? String ?? ""), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingLogo.stopAnimating()
            })
        }
    }
    
    private func showTabsDetail(_ data: [String: Any?]) {
        let isUpdate: Bool = self.tabDetailVC != nil
        
        if self.tabDetailVC == nil {
            self.tabDetailVC = VendorTabDetailViewController()
        }
        
        self.tabDetailVC?.parentVC = self
        
    
        var tabs: [[String: String]] = []
        if let aboutVendor = data["vendor_detail"]  as? [String : Any?] {
            tabs.append(["value": LOCALIZED.txt_about_title.translate, "key": VENDOR_INIT_TAB.ABOUT.tabSwitch])
            self.tabDetailVC?.aboutInfo = aboutVendor
        }
        
        if let listOffers = data["list_offer_related"]  as? [[String : Any?]] {
            if listOffers.count > 0 {
                tabs.append(["value": LOCALIZED.txt_basic_deal.translate, "key": VENDOR_INIT_TAB.BASIC_DEAL.tabSwitch])
                
                self.tabDetailVC?.offerInfo = listOffers[0]
                
            }
        }
        
        if let listDeals = data["list_deal_related"]  as? [[String : Any?]] {
            tabs.append(["value": LOCALIZED.txt_deals_title.translate, "key": VENDOR_INIT_TAB.DEALS.tabSwitch])
            
            var fListDeals:[Any]  = []
            let carouselDeals = Utils.getCarouselDeals(listDeals)
            //let normalDeals = Utils.getNormalDeals(listDeals)
            let normalDeals: [[String: Any?]] = []
            
            if carouselDeals.count > 0 {
                fListDeals.append(carouselDeals)
            }
            
            if normalDeals.count > 0 {
                fListDeals.append(contentsOf: normalDeals)
            }

            self.tabDetailVC?.originalDeals = listDeals
            self.tabDetailVC?.deals = fListDeals
        }
        
        if let listEvents = data["list_event_related"]  as? [[String : Any?]] {
            tabs.append(["value": LOCALIZED.txt_events_title.translate, "key": VENDOR_INIT_TAB.EVENTS.tabSwitch])
            self.tabDetailVC?.events = listEvents
        }
        
        
        self.tabDetailVC?.tabs = tabs
        
        if isUpdate {
            self.tabDetailVC?.forceUpdateData()
            
            Utils.dismissLoading()
        } else {
            self.tabDetailVC?.initTab = self.initTab
            
            self.coverTabDetailView.addSubview(self.tabDetailVC!.view)
            self.addChild(self.tabDetailVC!)
            
            self.tabDetailVC!.view.snp.makeConstraints { (make) in
                make.top.equalTo(self.coverTabDetailView)
                make.left.equalTo(self.coverTabDetailView)
                make.right.equalTo(self.coverTabDetailView)
                make.bottom.equalTo(self.coverTabDetailView)
            }
        }
    }
    
    private func updateBasicInfoIfNeed(_ data: [String: Any?]) {
        guard let _ = self.summaryData else {
            
            if let aboutVendor = data["vendor_detail"]  as? [String : Any?] {
                self.summaryData = aboutVendor
                
                self.fillData()
            }
            
            return
        }
    }
    
    private func handleGetDetails(_ vendorId: String) {
        Utils.showLoading()
        APICommonServices.getVendorDetail(vendorId) { (resp) in
            mPrint("getVendorDetail", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?]  {
                    self.updateBasicInfoIfNeed(data)
                    self.showTabsDetail(data)
                }
            } else {
                Utils.dismissLoading()
            }
            
        }
    }
    
    private func getVendorDetail() {
        if let data = self.summaryData,  let vendorId = data["VendorID"] as? String {
            self.handleGetDetails(vendorId)
        } else if let tVendorId = self.vendorId {
            self.handleGetDetails(tVendorId)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .REFRESH_VENDOR_DETAIL, object: nil)
    }

}
