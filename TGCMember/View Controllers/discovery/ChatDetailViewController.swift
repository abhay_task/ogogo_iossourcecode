//
//  ChatDetailViewController.swift
//  TGCMember
//
//  Created by vang on 6/4/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum MESSAGE_TYPE: Int {
    case TEXT = 1
    case IMAGE = 2
    case VIDEO = 3
    case LOCATION = 4
}

class MemberChat {
    var id: String?
    var avatar: String?
    var name: String?
    var businessName: String?
    
    required convenience init(_ data : [String: Any?]?) {
        self.init()
        
        if let tData = data {
            self.id = tData["_id"] as? String ?? ""
            self.avatar = tData["avatar"] as? String ?? ""
            self.name = tData["name"] as? String ?? ""
            self.businessName = tData["business_name"] as? String ?? ""
        }
    }
}

class MMessage {
    var messageId: String?
    var createdAt: Int = 0
    var text: String?
    var image: String?
    var video: String?
    var location: [String: Double]?
    var isRead: Bool = true
    var ownerId: String?
    var type: Int = 0
    
    required convenience init(_ data : [String: Any?]?) {
        self.init()
    
        if let tData = data {
            self.messageId = tData["ID"] as? String ?? ""
            let createdAt = tData["createdAt"]
            if createdAt is String {
                let tDate = Utils.dateFromStringDate(createdAt as? String ?? "", fromFormat: FROM_FORMAT_DEFAULT)
                self.createdAt = tDate?.milliseconds ?? 0
            } else {
                self.createdAt = tData["createdAt"] as? Int ?? 0
            }
            
            self.type = tData["type"] as? Int ?? 0
            self.text = tData["text"] as? String ?? ""
            self.image = tData["image"] as? String ?? ""
            self.video = tData["video"] as? String ?? ""
            self.ownerId = tData["ownerId"] as? String ?? ""
            
        }
    }
}

let SECS_DATE_FORMAT: String = "dd MMM yyyy"


class ChatDetailViewController: BaseViewController   {
    var keyboardHeight : CGFloat = 0.0
    let defaultBottom: CGFloat = Utils.isIPhoneNotch() ? 10 : 0
    
    @IBOutlet weak var lblStatusPartner: UILabel!
    @IBOutlet weak var loadingAvatar: UIActivityIndicatorView!
    @IBOutlet weak var imagePartner: UIImageView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblPartnerName: UILabel!
    @IBOutlet weak var tfMessage: UITextField!
    @IBOutlet weak var coverInput: UIView!
    @IBOutlet weak var tblMessages : UITableView!
    @IBOutlet weak var bottomBarHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var heightHeaderConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    
    //var imagePicker : UIImagePickerController?
    
    var data: [String: Any?]?
    var totalMessages : [MMessage] = []
    var arrDates : [Date] = []
    var groupMessages : [[MMessage]] = [] {
        didSet {
            print(groupMessages)
        }
    }
    var otherOnTyping = false
    
    var TODAY = Utils.stringFromDate(Date(), toFormat: SECS_DATE_FORMAT)
    
    var groupId: String?
    var partnerId: String?
    var partnerInfo: MemberChat?
    
    private var fbServices: FirebaseServices?
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkCurrentChat(_:)), name: .CHECK_CHAT_EXIST, object: nil)
        
        // Do any additional setup after loading the view.
        tblMessages.register(UINib(nibName: "MyMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MyMessageDetailTableViewCell")
        tblMessages.register(UINib(nibName: "OtherMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherMessageDetailTableViewCell")
        tblMessages.register(UINib(nibName: "MyImageMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MyImageMessageDetailTableViewCell")
        tblMessages.register(UINib(nibName: "OtherImageMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherImageMessageDetailTableViewCell")
        tblMessages.register(UINib(nibName: "MyLocationMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MyLocationMessageDetailTableViewCell")
        tblMessages.register(UINib(nibName: "OtherLocationMessageDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherLocationMessageDetailTableViewCell")
        
        tblMessages.register(UINib(nibName: "ThreeDotsTableViewCell", bundle: nil), forCellReuseIdentifier: "ThreeDotsTableViewCell")
        
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tblMessages.refreshControl = refreshControl
        } else {
            tblMessages.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(getPreviousMessage(_:)), for: .valueChanged)
        
        self.decorate()
        
        self.startAChat { [weak self] (success) in
            guard let self = self else {return}
            
            if success {
                self.getHistoryChat(false)
            } else {
                Utils.dismissLoading()
            }
        }
        

    }

    @objc func checkCurrentChat(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let tGroupId = uData["group_id"] as? String ?? ""
            
            if tGroupId == self.groupId {
                let message = uData["message"] as? [String: Any]
                
                if let mess = message {
                    let mMessage = MMessage(mess)
                    
                    if !self.isExistMessage(mMessage) {
                        self.handleAppendMessage(mMessage)
                    }
                }
            }
            
        }
    }
    
    @objc private func getPreviousMessage(_ sender: Any) {
        // Fetch Weather Data
        self.refreshControl.endRefreshing()
        
        self.getHistoryChat(false, false, false)
    }
    
    func updateLanguages() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        GLOBAL.GlobalVariables.numberChatReady += 1
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let _ = self.presentedViewController {
            return
        }
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        
        self.bottomFooterConstraint.constant = keyboardHeight
        
    }
    
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let _ = self.presentedViewController {
            return
        }
        
        self.bottomFooterConstraint.constant = defaultBottom
    }
    
    private func decorate() {
        self.lblStatusPartner.text = LOCALIZED.active_now.translate
        self.tfMessage.placeholder = LOCALIZED.type_a_message.translate
        
        self.imagePartner.layer.cornerRadius = 21.0
        self.imagePartner.layer.masksToBounds = true
        
        self.coverInput.layer.cornerRadius = 19.0
        self.coverInput.layer.borderWidth = 0.0
        self.coverInput.backgroundColor = UIColor(hexFromString: "#F3F4F7")
        
        self.coverInput.layer.masksToBounds = true
        
        self.bottomFooterConstraint.constant = defaultBottom
        self.heightHeaderConstraint.constant = Utils.isIPhoneNotch() ? 100 : 80
    }
    
    private func dismissKeyboard() {
        tfMessage.resignFirstResponder()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.fbServices?.deleteOldMessage()
        GLOBAL.GlobalVariables.numberChatReady -= 1
        
        self.navigationController?.popViewController(animated: true)
    }

 
    @IBAction func onClickedAdd(_ sender: Any) {
        self.dismissKeyboard()
        //self.showPickerPhoto()
    }
    
    @IBAction func onClickedSend(_ sender: Any) {
        self.handleSendTextMessage()
    }
    
    private func findArrayContain(_ mesasge: MMessage) -> [MMessage]{
        var arrContain:[MMessage] = []
        
        for array in self.groupMessages {
            if array.contains(where: {$0.createdAt == mesasge.createdAt}) {
                // it exists, do something
                arrContain = array
                
                break
            }
        }
        
        return arrContain
    }
    
    private func isFirst(_ mesasge: MMessage) -> Bool {
        
        let arrayContain = self.findArrayContain(mesasge)
        
        let indexOfA = arrayContain.firstIndex{ $0.createdAt == mesasge.createdAt } ?? 0
        
        if indexOfA == 0 || arrayContain.count == 1 {
            return true
        }
        
        let preIndex = indexOfA - 1 // alsway >= 0
        let nextIndex = indexOfA + 1
        
        if nextIndex == arrayContain.count {
            let _preMessage = arrayContain[preIndex]
            
            if self.isMineMessage(mesasge) != self.isMineMessage(_preMessage) {
                return true
            }
            
            return false
        }


        let preMessage = arrayContain[preIndex]
        
        if self.isMineMessage(mesasge) != self.isMineMessage(preMessage)  {
            return true
        }
        
        
        return false
    }
    
    private func isMiddle(_ mesasge: MMessage) -> Bool {
        let arrayContain = self.findArrayContain(mesasge)
        
        let indexOfA = arrayContain.firstIndex{ $0.createdAt == mesasge.createdAt } ?? 0
        let preIndex = indexOfA - 1 // alsway >= 0
        let nextIndex = indexOfA + 1
        
        if arrayContain.count == 1 {
            return true
        }
        
        if indexOfA == 0 {
            let _nextMessage = arrayContain[nextIndex]
            
            if self.isMineMessage(mesasge) != self.isMineMessage(_nextMessage) {
                return true
            }
            
            return false
            
        }
        
        if nextIndex == arrayContain.count {
            let _preMessage = arrayContain[preIndex]
            
            if self.isMineMessage(mesasge) != self.isMineMessage(_preMessage) {
                return true
            }
            
            return false
        }
        
        let preMessage = arrayContain[preIndex]
        let nextMessage = arrayContain[nextIndex]
        
        if self.isMineMessage(mesasge) == self.isMineMessage(preMessage) && self.isMineMessage(mesasge) == self.isMineMessage(nextMessage) {
            return true
        } else if self.isMineMessage(mesasge) != self.isMineMessage(preMessage) && self.isMineMessage(mesasge) != self.isMineMessage(nextMessage) {
            return true
        }
        
        
        return false
    }
    
    private func isEnd(_ mesasge: MMessage) -> Bool {
        let arrayContain = self.findArrayContain(mesasge)
        
        let indexOfA = arrayContain.firstIndex{ $0.createdAt == mesasge.createdAt } ?? 0
        let preIndex = indexOfA - 1 // alsway >= 0
        let nextIndex = indexOfA + 1
        
        if arrayContain.count == 1 {
            return true
        }
        
        if indexOfA == 0 {
            let _nextMessage = arrayContain[nextIndex]
            
            if self.isMineMessage(mesasge) != self.isMineMessage(_nextMessage) {
                return true
            }
            
            return false
        }
        

        if nextIndex == arrayContain.count {
            return true
        }
        
        let preMessage = arrayContain[preIndex]
        let nextMessage = arrayContain[nextIndex]
        
        if self.isMineMessage(mesasge) == self.isMineMessage(preMessage) && self.isMineMessage(mesasge) != self.isMineMessage(nextMessage) {
            return true
        } else if self.isMineMessage(mesasge) != self.isMineMessage(preMessage) && self.isMineMessage(mesasge) != self.isMineMessage(nextMessage) {
            return true
        }
        
        return false
    }
    
    private func isExistMessage(_ message: MMessage?) -> Bool {
        if message == nil {
            return true
        }
        
        let arrMessages = self.totalMessages.filter { (item) -> Bool in
            if item.createdAt == message!.createdAt, message?.messageId == item.messageId {
                return true
            }
            
            return false
            
        }
        
        if arrMessages.count > 0 {
            return true
        }
        
        return false
       
    }
    
    private func addListenerMesasges() {
        self.fbServices?.addListenerMessage(firebaseCallback: { (status, data) in
            
            if status == .ADDED {
                let mMessage = MMessage(data as? [String : Any?])
                
                if !self.isExistMessage(mMessage) {
                    //mPrint("FIREBASE MESSAGE ADDED --> ", data)
                    self.handleAppendMessage(mMessage)
                }
            }
        
        })
    }
    
    private func uploadImage(_ base64: String, _ callback: DataCallback?) {
        let params = [
            "image": base64,
            "group_id": self.groupId ?? ""
        ]
        
        APICommonServices.uploadImageFromChat(params) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("uploadImageFromChat", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true {
               
            }
            
            callback?(["url": ""])
        }
    }
    
    private func handleSendTextMessage() {
        if let txt = self.tfMessage.text?.trimmingCharacters(in: .whitespacesAndNewlines) , txt != "" {
            self.tfMessage.text = ""
            let strNowTime = Date().milliseconds
            
            let txtMessage = [
                             "createdAt": strNowTime,
                             "text": txt,
                             "ownerId": Utils.getUserId(),
                             "type": MESSAGE_TYPE.TEXT.rawValue] as [String : Any?
                            ]
            
            let mMessage = MMessage(txtMessage)
            self.handleAppendMessage(mMessage)
            
            self.sendMessageToCloud(txtMessage as [String : Any])
        }
    }
    
    private func  handleSendFileMessage(_ fileData : Data) {
        let strNowTime = Date().milliseconds
        
        let base64Str = fileData.base64EncodedString(options: .lineLength64Characters)
        
        var fileMessage = [
            "createdAt": strNowTime,
            "image": base64Str,
            "ownerId": Utils.getUserId(),
            "type": MESSAGE_TYPE.IMAGE.rawValue] as [String : Any?]
   
        let mMessage = MMessage(fileMessage)
        self.handleAppendMessage(mMessage)
        
        self.uploadImage(base64Str) { (data) in
            
            if let tData = data, let urlImage = tData["url"] as? String {
                fileMessage["image"] = urlImage
            } else {
                fileMessage["image"] = ""
            }
            
            self.sendMessageToCloud(fileMessage as [String : Any])
    
        }
    }
    
    private func handleAppendMessage(_ mMessage: MMessage) {
        var isHandled = false
        
        if arrDates.count == 0 {
            arrDates.append(Date())
        }
        
        if let last = arrDates.last {
            let lastStr = Utils.stringFromDate(last, toFormat: SECS_DATE_FORMAT)
            
            if lastStr != TODAY {
       
                let date = Utils.dateFromMilliseconds(mMessage.createdAt)!
                if !groupMessages.contains(where: { $0.contains(where: { $0.createdAt == mMessage.createdAt }) == true }) {
                    arrDates.append(date)
                    groupMessages.append([mMessage])
                }
                
                
                isHandled = true
            }
            
            setTimeout({ [weak self] in
                 guard let self = self else {return}
                
                 self.scrollToBottom()
            }, 500)
        }
        
        if !isHandled && groupMessages.count > 0 {
            if !groupMessages.contains(where: { $0.contains(where: { $0.createdAt == mMessage.createdAt }) == true }) {
                groupMessages[groupMessages.count - 1].append(mMessage)
            }
        }
        
        if !totalMessages.contains(where: { $0.createdAt == mMessage.createdAt }) {
            totalMessages.append(mMessage)
        }
        
        tblMessages.reloadData()
        
    }
    
    @objc func didClickLongPressOnCell(gestureReconizer : UIGestureRecognizer) {
        
    }
    
    private func scrollToBottom(_ animation: Bool = true) {
        if let cnt = groupMessages.last?.count, cnt > 0 {
            if self.otherOnTyping {
                let idxPath = IndexPath(item: 0, section: arrDates.count)
                tblMessages.scrollToRow(at: idxPath, at: UITableView.ScrollPosition.bottom, animated: animation)
                Utils.dismissLoading()
            } else {
                let idxPath = IndexPath(item: cnt - 1, section: arrDates.count - 1)
                
                setTimeout({ [weak self] in
                    guard let self = self else {return}
                    self.tblMessages.scrollToRow(at: idxPath, at: UITableView.ScrollPosition.bottom, animated: animation)
                    Utils.dismissLoading()
                }, 200)
            }
        } else {
            Utils.dismissLoading()
        }
    }
    
    private func disableChat() {
        self.footerView.isUserInteractionEnabled = false
        self.footerView.alpha = 0.5
    }
    
    private func sendMessageToCloud(_ message: [String: Any]) {
        
        self.fbServices?.addMesasge(data: message, firebaseCallback: { (status, data) in
            //mPrint("addMesasge FIREBASE --> ", data)
        })
        
        self.sendMessageToServer(message)
    }
    
    private func sendMessageToServer(_ message: [String: Any]) {
        
        let params = [
            "group_id": self.groupId ?? "",
            "message": message
            ] as [String : Any]
        
        APICommonServices.sendMessage(params) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("sendMessageToServer", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true {
                
            }
        }
    }
    
    private func startAChat(_ callback: SuccessedCallback?) {
        Utils.showLoading()
        
        if let tData = self.data {
            let partnerId = tData["PartnerID"] as? String ?? ""
            
            self.partnerId = partnerId
        }
        
        APICommonServices.startAChat(self.partnerId ?? "") { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("startAChat", resp)
            var isSuccess = false
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true {
                if let data = resp["data"] as? [String: Any?], status == true {
                    isSuccess = true
                    self.configBeforeStartChat(data)
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                    self.disableChat()
                }
            }
            
            if let tCallback = callback {
                tCallback(isSuccess)
            } else {
                Utils.dismissLoading()
            }
        }
    }
    
    private func getHistoryChat(_ showLoading: Bool = true, _ listener: Bool = true, _ scrollBottom: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        let message = self.totalMessages.first
        var messageId = ""
        if let tMessage = message {
            messageId = tMessage.messageId ?? ""
        }
        
        APICommonServices.getHistoryMessages(self.groupId ?? "", messageId: messageId, limit: 50) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("getHistoryMessages", resp)
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], status == true  {
                    
                    let list = data["list"] as? [[String : Any?]]
                    if list == nil {
                         self.handleAfterGetHistory([], listener, scrollBottom)
                    } else {
                        self.handleAfterGetHistory(list!, listener, scrollBottom)
                    }
                } else {
                    Utils.dismissLoading()
                    //Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
        }
    }
    
    private func configBeforeStartChat(_ data: [String: Any?]) {
        if let pathFirebase = data["firebase_path"]  as? String {
            self.fbServices = FirebaseServices(path: pathFirebase)
        }
        
        if let groupChatID = data["group_id"]  as? String {
            self.groupId = groupChatID
        }
        
        if let members = data["members"] as? [[String: Any?]], members.count == 2 {
            let user1 = members[0] as [String: Any?]
            let user2 = members[1] as [String: Any?]
            
            let cID = user1["_id"] as? String
            
            if cID == Utils.getUserId() {
                self.partnerInfo = MemberChat(user2)
            } else {
                self.partnerInfo = MemberChat(user1)
            }
            
            self.loadingAvatar.startAnimating()
            self.imagePartner.sd_setImage(with:  URL(string: self.partnerInfo?.avatar ?? ""), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingAvatar.stopAnimating()
            })
            
            self.lblPartnerName.text = self.partnerInfo?.businessName
        }
        
    }
    
    private func handleAfterGetHistory(_ messages: [[String: Any?]], _ listener: Bool = true, _ scrollBottom: Bool = true) {
        
        for index in 0..<messages.count {
            let indexReversed = (messages.count - 1) - index
            let mMess = MMessage(messages[indexReversed])
            
            self.totalMessages.insert(mMess, at: 0)
        }
        
//        for mess in messages {
//            let mMess = MMessage(mess)
//            self.totalMessages.append(mMess)
//        }

        self.buildGroupMessages()
        self.tblMessages.reloadData()

        setTimeout({ [weak self] in
            guard let self = self else {return}
            
            if listener {
                self.addListenerMesasges()
            }
            
            if scrollBottom {
                self.scrollToBottom()
            } else {
                Utils.dismissLoading()
            }
            
        }, 1000)
    }
    
    func buildGroupMessages() {
        
        let dict = Dictionary(grouping: totalMessages) { (mMessage) -> Date in
            let date = Utils.dateFromMilliseconds(mMessage.createdAt)!
            
            return date
        }
        
        var sortedArr : [Date] = []
        if dict.count == 0 {
            let date = Date()
            sortedArr = [date]
        } else {
            sortedArr = dict.keys.sorted()
        }
        
        groupMessages.removeAll()
        arrDates.removeAll()
        
        if sortedArr.count == 1 {
            let values = dict[sortedArr[0]]
            
            if values != nil {
                arrDates.append(contentsOf: sortedArr)
            }
            
            groupMessages.append(values ?? [])
        } else {
            arrDates.append(contentsOf: sortedArr)
            
            for date in sortedArr {
                let values = dict[date]
                groupMessages.append(values ?? [])
            }
        }
        
    }
    
    func isMineMessage(_ message : MMessage?) -> Bool {
        var isMine = true
        
        if let mMess = message {
             isMine = mMess.ownerId == Utils.getUserId()
        }
        
        return isMine
    }
    
    func markUserReadToLastMessage(_ messageId : String){
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .CHECK_CHAT_EXIST, object: nil)
    }


}

extension ChatDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        let secs = arrDates.count
        return secs + (otherOnTyping ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let secs = arrDates.count
        
        if section == secs { // typing
            return 1
        } else { // normal message
            return groupMessages[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : MessageDetailTableViewCell? = nil
        
        let secs = arrDates.count
        
        if indexPath.section == secs { // on typing
            return tableView.dequeueReusableCell(withIdentifier: "ThreeDotsTableViewCell") as! ThreeDotsTableViewCell
        } else { // normal message
            let arrMesses = groupMessages[indexPath.section]
            
            let mMess = arrMesses[indexPath.row]
            if groupMessages.last?.last?.messageId == mMess.messageId {
                self.markUserReadToLastMessage(mMess.messageId ?? "")
            }
            
            let mType = mMess.type
            
            let isMine = self.isMineMessage(mMess)
            
            if mType == MESSAGE_TYPE.TEXT.rawValue {
                if isMine {
                    cell = tableView.dequeueReusableCell(withIdentifier: "MyMessageDetailTableViewCell") as? MessageDetailTableViewCell
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "OtherMessageDetailTableViewCell") as? MessageDetailTableViewCell
                }
            } else if mType == MESSAGE_TYPE.IMAGE.rawValue {
                if isMine {
                    cell = tableView.dequeueReusableCell(withIdentifier: "MyImageMessageDetailTableViewCell") as? MessageDetailTableViewCell
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "OtherImageMessageDetailTableViewCell") as? MessageDetailTableViewCell
                }
            } else if mType == MESSAGE_TYPE.LOCATION.rawValue {
                if isMine {
                    cell = tableView.dequeueReusableCell(withIdentifier: "MyLocationMessageDetailTableViewCell") as? MessageDetailTableViewCell
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "OtherLocationMessageDetailTableViewCell") as? MessageDetailTableViewCell
                }
            } else if mType == MESSAGE_TYPE.VIDEO.rawValue {
                if isMine {
                    cell = tableView.dequeueReusableCell(withIdentifier: "MyImageMessageDetailTableViewCell") as? MessageDetailTableViewCell
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "OtherImageMessageDetailTableViewCell") as? MessageDetailTableViewCell
                }
            }
            
            cell?.isFirst = self.isFirst(mMess)
            cell?.isMiddle = self.isMiddle(mMess)
            cell?.isEnd = self.isEnd(mMess)
            cell?.avatarUrl = self.partnerInfo?.avatar ?? ""
            
            cell?.data = mMess
        }
        
        if let _ = cell {
            
        } else {
            //             let orgMess = groupMessages[indexPath.section][indexPath.row]
            return UITableViewCell()
        }
        
        cell?.table = tableView
        cell?.selectionStyle = .none
        cell?.parentVC = self
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tfMessage.isFirstResponder {
            self.dismissKeyboard()
            
            return
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? MessageDetailTableViewCell {
            if let mess = cell.data, mess.type == MESSAGE_TYPE.IMAGE.rawValue, let url = mess.image {
                let vc = ImageDetailViewer(nibName: "ImageDetailViewer", bundle: nil)
                vc.url = url
                
                present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let secs = arrDates.count
        
        if section == secs { // typing
            return 0
        } else { // normal message
            
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let secs = arrDates.count
        
        if section == secs { // typing
            let v = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 0))
            v.backgroundColor = .white
            return v
        } else { // normal message
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50))
            view.backgroundColor = UIColor.clear
            
            let v = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 30))
            v.backgroundColor = .clear
            v.textColor = UIColor(hexFromString: "#4b5b69")
            v.textAlignment = .center
            v.font = UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15)
            let str = Utils.stringFromDate(arrDates[section],  toFormat: SECS_DATE_FORMAT)
            v.text = str == TODAY ? LOCALIZED.txt_today_title.translate : str
            
            view.addSubview(v)
            
            return view

        }
        
    }
}


extension ChatDetailViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.handleSendTextMessage()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let cnt = groupMessages.last?.count, cnt > 0 {
            let idxPath = IndexPath(item: cnt - 1, section: arrDates.count - 1)
            tblMessages.scrollToRow(at: idxPath, at: UITableView.ScrollPosition.bottom, animated: true)
        }
        
        self.scrollToBottom()
        
        return true
    }
    
}


//extension ChatDetailViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
//
//    private func showPickerPhoto() {
//        if let _ = imagePicker {
//
//        } else {
//            imagePicker = UIImagePickerController()
//        }
//
//        guard let imagePicker = imagePicker else {return}
//
//        imagePicker.delegate = self
//        let alert = UIAlertController(title: "Select a Photo", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Take Photo...", style: .default, handler: { _ in
//            self.openCamera()
//        }))
//
//        alert.addAction(UIAlertAction(title: "Choose from Library...", style: .default, handler: { _ in
//            self.openGallary()
//        }))
//
//        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
//
//        /*If you want work actionsheet on ipad
//         then you have to use popoverPresentationController to present the actionsheet,
//         otherwise app will crash on iPad */
//
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    private func openCamera() {
//        guard let imagePicker = imagePicker else {return}
//        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
//            imagePicker.sourceType = UIImagePickerController.SourceType.camera
//            imagePicker.allowsEditing = false
//
//            self.present(imagePicker, animated: true, completion: nil)
//        } else {
//            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//
//            self.present(alert, animated: true, completion: nil)
//        }
//    }
//
//    private func openGallary() {
//        guard let imagePicker = imagePicker else {return}
//        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        imagePicker.allowsEditing = false
//
//        self.present(imagePicker, animated: true, completion: nil)
//    }
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//            if let data = pickedImage.jpegData(compressionQuality: 0.7) {
//                self.handleSendFileMessage(data)
//            }
//        }
//        picker.dismiss(animated: true, completion: nil)
//    }
//
//
//
//}
