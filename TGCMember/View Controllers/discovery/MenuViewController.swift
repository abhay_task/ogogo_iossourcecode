//
//  SliderMenuViewController.swift
//  TGCMember
//
//  Created by vang on 5/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController {
    func updateLanguages() {
        self.lblSignOut.text = LOCALIZED.sign_out.translate
        self.lblSignIn.text = LOCALIZED.txt_login_title.translate
        decorateUI()
        mapData()
    }
    
    @IBOutlet weak var bottomVersionConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSignOut: HighlightableView!
    @IBOutlet weak var btnSignIn: HighlightableView!
    @IBOutlet weak var heightUpgradeConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageUpgrade: UIImageView!
    @IBOutlet weak var coverUpgrade: UIView!
    
    @IBOutlet weak var coverTxt: UIView!
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var loadingAvatar: UIActivityIndicatorView!
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var footerView: HighlightableView!
    @IBOutlet weak var lblSignOut: UILabel!
    @IBOutlet weak var lblSignIn: UILabel!
    
    @IBOutlet weak var topCloseConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverAvatar: UIView!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    private var selectedIndex: Int = 0
    private var listItems: [[String: Any?]] = []
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var upgradeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var outerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.isHidden = true
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMenuView(_:)), name: .REFRESH_MENU_VIEW, object: nil)
        
        self.table.delegate = self
        self.table.dataSource = self
        self.table.register(UINib(nibName: "MenuItemCell", bundle: nil), forCellReuseIdentifier: "MenuItemCell")
        
        decorateUI()
        mapData()
        outerViewTapToDismiss()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let transform = CGAffineTransform(translationX: -screenWidth, y: 0)
        containerView.transform = transform
        containerView.isHidden = false
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            self.containerView.transform = .identity
            self.containerView.layoutIfNeeded()
        }, completion: nil)
    }
    
    func outerViewTapToDismiss() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissMenu))
        tap.delegate = self
        outerView.addGestureRecognizer(tap)
    }
    
    @objc func refreshMenuView(_ notification: Notification) {
        self.mapData()
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        self.handleUpgrade()
    }
    
    @IBAction func onClickedAvatar(_ sender: Any) {
        if !Utils.isAnonymous() {
            self.toggleLeft()
            setTimeout({
                GLOBAL.GlobalVariables.discoveryInstance?.navigationController?.pushViewController(self.myProfileVc, animated: true)
            }, 300)
        }
    }
    
    @IBAction func onClickedSignIn(_ sender: Any) {
//        self.toggleLeft()
        dismissMenu()
        
        setTimeout({
            GLOBAL.GlobalVariables.discoveryInstance?.navigationController?.pushViewController(LoginViewController(), animated: true)
        }, 300)
    }
    
    @IBAction func onClickedSignOut(_ sender: Any) {
        
        Utils.showConfirmAlertStyle(LOCALIZED.sign_out_message.translate, confirmCallback: { [weak self] in
            guard let self = self else {return}
            Utils.showLoading()
            
            Utils.logoutApp(callback: { [weak self] (success, resp) in
                guard let self = self else {return}
                
                if success {
                    IntercomManager.shared.logout()
                    
                    //NotificationCenter.default.post(name: .FORCE_SIGN_IN, object: nil)
                    GLOBAL.GlobalVariables.quickSignUpModel = QuickSignUpModel(nil)
                    Utils.cachedSignUpInfo()
                    
                    Utils.initApp { (success) in
//                        GLOBAL.GlobalVariables.isFirstInitApp = true
                        
                        Utils.dismissLoading()
                        
                        NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                        NotificationCenter.default.post(name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
                        
//                        self.toggleLeft()
                        self.dismissMenu()
                    }
                    
                    CacheManager.shared.currentPlan = nil
                    CacheManager.shared.termsAndConditionsAccepted = nil
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                        self.showFreeMemberReminderDialog()
                    })
                    
                } else if let tResp = resp as? [String: Any] {
                    Utils.dismissLoading()
                    
                    Utils.showAlertView("", tResp["message"] as? String ?? "")
                } else {
                    Utils.dismissLoading()
                }
                }, forceSignIn: true)
            
            }, cancelCallback: nil)
        
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
//        self.toggleLeft()
        dismissMenu()
    }
    
    @objc func dismissMenu() {
        let transform = CGAffineTransform(translationX: -screenWidth, y: 0)
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseInOut, animations: {
            self.containerView.transform = transform
            self.containerView.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    private func mapData() {
        self.listItems = [
            ["title": LOCALIZED.my_account.translate, "icon": "menu_account_ic", "type": "text", "enable": !Utils.isAnonymous()],
            ["title": LOCALIZED.txt_upgrade_title.translate, "icon": "menu_upgrade_ic", "type": "text", "enable": true],
            ["title": LOCALIZED.terms_and_conditions.translate, "icon": "terms-and-condittions_icon", "type": "text", "enable": true],
            ["title": LOCALIZED.faqs_title.translate, "icon": "menu_help_ic", "type": "text", "enable": true],
            ["title": LOCALIZED.setting_title.translate, "icon": "menu_setting_ic", "type": "text", "enable": true],
            //["title": LOCALIZED.about_us.translate, "icon": "menu_about_us_ic", "type": "text"],
            //["title": LOCALIZED.help_and_support.translate, "icon": "menu_help_ic", "type": "text"],
            //["title": LOCALIZED.report_issues.translate, "icon": "menu_report_ic", "type": "text"]
            ] as [[String: Any]]
        
        
        if Utils.isRequireSignUpToGetDeal() || Utils.isSuggestGiftCode() {
            self.listItems.append(["title": Utils.isRequireSignUpToGetDeal() ? "Sign Up – Get your 1st Deal" : "Gift 1 Code to a Friend", "type": "button"])
        }
        
        mPrint("_userInfo --> ", GLOBAL.GlobalVariables.userInfo)
        
        if let userInfo = GLOBAL.GlobalVariables.userInfo {
            self.lblFullname.text = userInfo["DisplayName"] as? String
            self.lblRole.text = CacheManager.shared.currentPlan?.name ?? userInfo["CurrentPlan"] as? String
            
            self.loadingAvatar.startAnimating()
            self.imageAvatar.sd_setImage(with:  URL(string: userInfo["AvatarURL"] as? String ?? ""), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingAvatar.stopAnimating()
            })
            
            self.footerView.isUserInteractionEnabled = true
            self.footerView.alpha = 1.0
        } else {
            self.heightFooterConstraint.constant = 0
            self.footerView.isUserInteractionEnabled = false
            self.footerView.alpha = 0.5
        }
        
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            coverUpgrade.isHidden = !_upgradeModel.canUpNextStep
            heightUpgradeConstraint?.constant = _upgradeModel.canUpNextStep ? 33.0 : 0
            
            self.imageUpgrade.sd_setImage(with:  URL(string: _upgradeModel.iconUpgradeNextStep), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
            })
            
        } else {
            coverUpgrade.isHidden = true
            heightUpgradeConstraint?.constant = 0
            self.imageUpgrade.image = nil
        }
        
        self.btnSignIn?.isHidden = !Utils.isAnonymous()
        self.btnSignOut?.isHidden = Utils.isAnonymous()
        
        self.bottomVersionConstraint?.constant = Utils.isIPhoneNotch() ? 20 : 10
        
        
        self.table.reloadData()
    }
    
    private func decorateUI() -> Void {
        var version = Utils.getVersionApp()
        
        if SIGN_KEY != "LIVE" {
            version = "\(version)(\(Utils.getVersionBuild())) \(SIGN_KEY)"
        }
        
        self.lblVersion.text = "Version \(version)"
        
        topCloseConstraint.constant = Utils.isIPhoneNotch() ? 40 : 10
        self.coverAvatar.layer.borderColor = UIColor(255, 193, 6).cgColor
        self.coverAvatar.layer.borderWidth = 0.8
        self.coverAvatar.layer.cornerRadius = 30.0
        self.coverAvatar.layer.masksToBounds = true
        
        self.imageAvatar.layer.borderColor = UIColor(42, 39, 34).cgColor
        self.imageAvatar.layer.borderWidth = 0.0
        self.imageAvatar.layer.cornerRadius = 25.0
        self.imageAvatar.layer.masksToBounds = true
        
        coverTxt?.backgroundColor = UIColor.white
        coverTxt?.clipsToBounds = false
        coverTxt?.layer.applySketchShadow(color: UIColor("rgba 120 121 147 1"), alpha: 0.2, x: 0, y: 2, blur: 16, spread: 0)
        upgradeLabel.text = LOCALIZED.txt_upgrade_title.translate
    }
    
    private func handleUpgrade() {
        let upgradeVC = NewUpgradeViewController()
        
        if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
            upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
        }
        
        let navVc = UINavigationController()
        navVc.viewControllers = [upgradeVC]
        self.presentWithPushAnim(vc: navVc)
    }
    
    private func handleMyAccount() {
        setTimeout({
            if Utils.isAnonymous() {
                let loginVC = LoginViewController()
                let navVc = UINavigationController()
                navVc.pushViewController(loginVC, animated: false)
                self.presentWithPushAnim(vc: navVc)
            } else {
                let navVc = UINavigationController()
                navVc.pushViewController(self.myProfileVc, animated: false)
                self.presentWithPushAnim(vc: navVc)
            }
        }, 300)
    }
    
    private func handleTermsAndConditions() {
        setTimeout({
            self.presentWithPushAnim(vc: self.termsAndConditionsVC)
        }, 300)
    }
    
    private func handleFAQs() {
        Utils.showLoading()
        
        Repository.shared.getFaqs(completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            Utils.dismissLoading()
            
            switch result {
            case .success(let response):
                let FAQsVc = self.faqsVC
                FAQsVc.faqs = response.faqs
                let navVc = UINavigationController()
                navVc.viewControllers = [FAQsVc]

                DispatchQueue.main.async {
                    self.presentWithPushAnim(vc: navVc)
                }
            case .failure(let error):
                print(error.message)
            }
        })
    }
    
    private func handleSetting() {
        setTimeout({
            let navVc = UINavigationController()
            navVc.pushViewController(self.settingVC, animated: false)
            self.presentWithPushAnim(vc: navVc)
        }, 300)
    }
    
    private func gotoPage(_ index: Int, _ type: String = "") -> Void {
//        self.toggleLeft()
        
        if type == "button" {
            if let topVC = GLOBAL.GlobalVariables.appDelegate?.getTopViewController() {
                setTimeout({
                    if Utils.isRequireSignUpToGetDeal() {
                        topVC.navigationController?.pushViewController(QuickSignUpViewController(), animated: true)
                    } else if Utils.isSuggestGiftCode() {
                        let sendCodeVC = SendFreeCodeViewController()
                        sendCodeVC.isFromSignUp = false
                        topVC.navigationController?.pushViewController(sendCodeVC, animated: true)
                    }
                }, 300)
            }
            
        } else {
            let item = listItems[index]
            
            let type = item["title"] as? String ?? ""
            
            if (type == LOCALIZED.my_account.translate) {
                self.handleMyAccount()
            } else if (type == LOCALIZED.txt_upgrade_title.translate) {
                self.handleUpgrade()
            } else if (type == LOCALIZED.terms_and_conditions.translate) {
                handleTermsAndConditions()
            } else if (type == LOCALIZED.faqs_title.translate) {
                handleFAQs()
            } else if (type == LOCALIZED.setting_title.translate) {
                handleSetting()
            } else if (type == LOCALIZED.help_and_support.translate) {
                
            } else if (type == LOCALIZED.report_issues.translate) {
                
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .REFRESH_MENU_VIEW, object: nil)
    }
}


extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell") as? MenuItemCell
        
        var tData = listItems[indexPath.row]
        tData["index"] = indexPath.row
        tData["selectedIndex"] = self.selectedIndex
        
        cell?.config(tData, onClickedCellCallback: {  [weak self]  (info)  in
            guard let self = self else {return}
            
            if let tData = info {
                let tIndex = tData["index"] as? Int ?? 0
                let tType = tData["type"] as? String ?? ""
                
                
                self.selectedIndex = tIndex
                self.table.reloadData()
                
                self.gotoPage(tIndex, tType)
            }
            }, onChangedNotification: {  [weak self]  (info)  in
                guard let self = self else {return}
                
                self.table.reloadData()
                
        })
        
        return cell!
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    
    
}

extension MenuViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == containerView || touch.view?.isDescendant(of: containerView) == true {
            return false
        }
        
        return true
    }
}
