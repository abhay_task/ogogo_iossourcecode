//
//  EventDetailViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps
import WebKit
import Branch

class EventDetailViewController: BaseViewController {

    @IBOutlet weak var wkWebview: WKWebView!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var lblBookmark: UILabel!
    @IBOutlet weak var loadingBanner: UIActivityIndicatorView!
    @IBOutlet weak var coverRelatedEvents: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var heightWebviewConstraint: NSLayoutConstraint!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bookmarkImage: UIImageView!
    @IBOutlet weak var leftBookmarkConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftShareConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftContactConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftMoreConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightCoverEventConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var itemWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var bookmarkView: HighlightableView!
    @IBOutlet weak var shareView: HighlightableView!
    @IBOutlet weak var contactView: HighlightableView!
    @IBOutlet weak var moreView: HighlightableView!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDatetime: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var coverMoreEventView: UIView!
    
    @IBOutlet weak var lblMoreFrom: UILabel!
    
    var eventInfo: [String: Any?]?
    var relatedEvents: [[String: Any?]] = []
    var isRefresh : Bool = false
    var forceUpdate: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.contentView.isHidden = true
      
        
        self.wkWebview.uiDelegate = self
        self.wkWebview.navigationDelegate = self
        self.wkWebview.scrollView.showsVerticalScrollIndicator = false
        self.wkWebview.scrollView.showsHorizontalScrollIndicator = false
        
        self.decorate()
        
        self.fillHeaderData()
        
        self.getMoreEvents()
        
        self.scrollView.delegate = self
        self.scrollView.addPullToRefresh { [weak self] in
            guard let self = self else {return}
           
            self.didPullToRefresh()
        }

        self.getEventDetail()
    }
    
    
    private func didPullToRefresh() {
        self.getEventDetail()
    }
    
   

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }
    
    @IBAction func onClickedShowFullMap(_ sender: Any) {
        let fullMapVC = FullMapViewController()
        fullMapVC.dealInfo = self.eventInfo
                      
        self.addSubviewAsPresent(fullMapVC)

    }
    
    @IBAction func onClickedMore(_ sender: Any) {
        
    }
    
    @IBAction func onClickedContact(_ sender: Any) {
        Utils.checkLogin(self) { [weak self] (loginStatus) in
            guard let self = self else {return}
            
            if loginStatus != .NOT_YET {
                if let tData = self.eventInfo, let summary = tData["VendorSummary"] as? [String: Any?] {
                     self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
                }
            }
        }
        
    }
    
    @IBAction func onClickedBookmark(_ sender: Any) {
        
        Utils.checkLogin(self) {  [weak self]  (status) in
            guard let self = self else {return}
            
            if status == LOGIN_STATUS.SUCCESS_IMMEDIATELY {
                self.handleBookmarkEvent()
            } else if status == LOGIN_STATUS.SUCCESS_AFTER {
                self.getEventDetail()
            }
        }
    }
    
    private func getEventDetail() {
        if let tData = self.eventInfo {
            Utils.showLoading()
            
            let eventId = tData["ID"] as? String ?? ""
            
            APICommonServices.getEventDetail(eventId) { (resp) in
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    if let data = resp["data"] as? [String: Any?] , let detail = data["event"]  as? [String : Any?] {
                        //mPrint("getEventDetail", detail)
                        
                        self.eventInfo = detail
                        
                        self.fillData()
                    }
                } else {
                    Utils.dismissLoading()
                }
                
                 self.isRefresh = false
            }
        }
    }
    
    private func handleBookmarkEvent() {
        if let data = self.eventInfo {
            Utils.showLoading()
            
            let eventId = data["ID"] as? String
            APICommonServices.toogleBookmarkEvent(eventId) { [weak self] (resp) in
                guard let self = self else {return}
                
                if let resp = resp, let status = resp["status"] as? Bool {
                    
                    if let data = resp["data"] as? [String: Any?], status == true {
                        let isBookmark = data["Bookmark"] as? Bool ?? false
                        self.bookmarkImage.image = isBookmark ? UIImage(named: "bookmarked") : UIImage(named: "unbookmark")
                        
                        NotificationCenter.default.post(name: .TOOGLE_BOOKMARK_EVENT, object: data)
                    }

                    //Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
                
                Utils.dismissLoading()
            }
        }
    }
    
    private func getBranchLinkProperty() -> BranchLinkProperties {
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        lp.campaign = "gcc referral"
        lp.channel = Constants.BRANCH_CHANNEL
        lp.feature = "sharing"
        lp.stage = "new event"
        lp.tags = ["tgc", "member"]
        
        return lp
    }
    
    @IBAction func onClickedShare(_ sender: Any) {
        let buo = BranchUniversalObject.init(canonicalIdentifier: "Event")
        
        if let data = self.eventInfo {
             buo.contentMetadata.customMetadata["eventID"] = data["ID"] as? String ?? ""
        }
        
        let message = "Check out this link"
        buo.showShareSheet(with: getBranchLinkProperty(), andShareText: message, from: self) { (activityType, completed) in
            print(activityType ?? "")
        }
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func decorate() {
        let leftSpace: CGFloat = 25.0
        let spaceItem: CGFloat = 15.0
        let totalSpace: CGFloat = leftSpace * 2.0 + spaceItem * 3.0
        let widthItem = (UIScreen.main.bounds.width - CGFloat(totalSpace)) / 4.0
        
        
        itemWidthConstraint.constant = widthItem
        leftBookmarkConstraint.constant = leftSpace
        leftShareConstraint.constant = widthItem + leftSpace + spaceItem
        leftContactConstraint.constant = widthItem * 2.0 + leftSpace + spaceItem * 2.0
        leftMoreConstraint.constant = widthItem * 3.0 + leftSpace + spaceItem * 3.0
        
        
        bookmarkView.layer.cornerRadius = 7
        bookmarkView.layer.borderWidth = 1
        bookmarkView.layer.masksToBounds = true
        bookmarkView.layer.borderColor = UIColor(0, 0, 0, 0.4).cgColor
        
        shareView.layer.cornerRadius = 7
        shareView.layer.borderWidth = 1
        shareView.layer.masksToBounds = true
        shareView.layer.borderColor = UIColor(0, 0, 0, 0.4).cgColor
        
        
        contactView.layer.cornerRadius = 7
        contactView.layer.borderWidth = 1
        contactView.layer.masksToBounds = true
        contactView.layer.borderColor = UIColor(0, 0, 0, 0.4).cgColor
        
        
        moreView.layer.cornerRadius = 7
        moreView.layer.borderWidth = 1
        moreView.layer.masksToBounds = true
        moreView.layer.borderColor = UIColor(0, 0, 0, 0.4).cgColor
       
    }
    
   private func fillHeaderData() {
        if let data = self.eventInfo {
            Utils.showLoading()
            
            lblEventName.text = data["Name"] as? String
            
            if let summary = data["VendorSummary"] as? [String: Any?], let tVendorName = summary["Name"] as? String {
                lblVendorName.text = tVendorName
            } else {
                lblVendorName.text = ""
            }
            
            self.loadingBanner.startAnimating()
            self.bannerImage.sd_setImage(with:  URL(string: data["HeaderImageUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingBanner.stopAnimating()
            })
            
            let isBookmark = data["IsBookmark"] as? Bool ?? false
            
            self.bookmarkImage.image = isBookmark ? UIImage(named: "bookmarked") : UIImage(named: "unbookmark")
            
            
        }
    
    }
    
    private func fillData() {
        
        if let data = self.eventInfo {
            
            self.fillHeaderData()
            
            let summary = data["VendorSummary"] as? [String: Any?]
            self.lblAddress.text = Utils.getFullAddress(summary)
            
            
            self.lblDatetime.text = "\(Utils.stringFromStringDate(data["StartAt"] as? String, toFormat: "MMM dd")) at \(Utils.stringFromStringDate(data["StartAt"] as? String, toFormat: "HH:mm")) - \(Utils.stringFromStringDate(data["EndAt"] as? String, toFormat: "MMM dd")) at \(Utils.stringFromStringDate(data["EndAt"] as? String, toFormat: "HH:mm"))"
            
          
            if let tData = data["VendorSummary"] as? [String: Any?] {
                let tLocation = tData["Location"] as? [String: Any]
                
                self.showPositonEvent(tLocation)
                
                var tText = LOCALIZED.more_from_format.translate
                tText = tText.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tData["Name"] as? String ?? "")
                
                self.lblMoreFrom.text = tText
            }
            
            self.loadHtmlContentDetail()
        
            
        } else {
            self.hideView()
        }
   
    }
    
    private func getMoreEvents() {
        if let data = self.eventInfo,  let eventId = data["ID"] as? String {
      
            APICommonServices.getEventDetail(eventId) { (resp) in
                
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    //mPrint("self.eventInfo", resp)
                    if let data = resp["data"] as? [String: Any?], let list = data["list"]  as? [String : Any?], let records = list["Records"]  as? [[String : Any?]] {
                         //mPrint("relatedEvents", data)
                        self.relatedEvents = records
                        self.showMoreEvent()
                    } else {
                        self.hideMoreEventView()
                    }
                } else {
                    self.hideMoreEventView()
                }
            }
        }
    }
    
    private func hideMoreEventView() {
        coverRelatedEvents.isHidden = true
        heightCoverEventConstraint.constant = 0
    }

    private func loadHtmlContentDetail() {
        if let data = self.eventInfo {
            let content = data["ContentDetail"] as? String ?? ""
            
            let urlPath = Bundle.main.url(forResource: "\(Constants.DEFAULT_FONT)", withExtension: "ttf")
            
            let htmlCode = Utils.getHTMLFromContent(content: content, bgColor: "#f6f7fb")
            self.wkWebview.loadHTMLString(htmlCode, baseURL: urlPath)
        } else {
            self.heightWebviewConstraint.constant = 0
        }
        
    }
    
    private func hideView(_ hide: Bool = true) {
        self.topView.isHidden = hide
        self.scrollView.isHidden = hide
    }
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 13.0)
        mapView.animate(with: camera)
    }
    
    private func showPositonEvent(_ location:  [String: Any?]?) {
        if location != nil {
            let position = CLLocationCoordinate2D(latitude: location!["lat"] as! CLLocationDegrees, longitude: location!["lng"] as! CLLocationDegrees)
            
            let marker = GMSMarker()
            marker.position = position
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.icon = UIImage(named: "point_marker")
            
            marker.map = mapView
            self.animateToPoint(position)
        }
        
    }
    
    func showMoreEvent() {
        self.coverRelatedEvents.isHidden = false
        
        let ratioThumb:CGFloat = 158/99
        let spaceTop: CGFloat = 10.0
        let lengthArr = self.relatedEvents.count
        
        let widthItem: CGFloat = (UIScreen.main.bounds.width - 60) / 2
        let heightImage: CGFloat = widthItem / ratioThumb
        
        let heightItem: CGFloat = heightImage + 75.0
        let newHeight = (round(CGFloat(lengthArr)/2.0) * heightItem)
        
        heightCoverEventConstraint.constant = newHeight
        
        var topConstraint: CGFloat = 0
        
        for index in 0..<lengthArr {
            let eventItem = NormalEventCell().loadView()
            let eventData = self.relatedEvents[index]
            
            eventItem.config(eventData) { [weak self] (data) in
                guard let self = self else {return}
                
                let eventDetailVC = EventDetailViewController()
                eventDetailVC.eventInfo = data
                
                self.navigationController?.pushViewController(eventDetailVC, animated: true)
            }
            
            self.coverMoreEventView.addSubview(eventItem)
            
            eventItem.snp.makeConstraints { (make) in
                make.height.equalTo(heightItem)
                make.width.equalTo(widthItem)
                make.top.equalTo(self.coverMoreEventView).offset(topConstraint)
                
                if index%2 == 0 {
                    make.left.equalTo(self.coverMoreEventView)
                } else {
                    make.right.equalTo(self.coverMoreEventView)
                }
            }
            
            if index%2 == 1 {
                topConstraint += (heightItem + spaceTop)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

}

extension EventDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !self.isRefresh {
            if scrollView.contentOffset.y < -60.0 {
                
                for subview in scrollView.subviews {
                    if subview is UIRefreshControl {
                        let rf = subview as! UIRefreshControl

                        self.isRefresh = true
                        rf.beginRefreshing()
                        rf.sendActions(for: .valueChanged)
                      
                        break
                    }
                }
            }
        }
       
    }
}

extension EventDetailViewController:  WKUIDelegate, WKNavigationDelegate {
    
    func updateLanguages() {
        self.lblBookmark.text = LOCALIZED.txt_bookmark_title.translate
        self.lblShare.text = LOCALIZED.txt_share_title.translate
        self.lblContact.text = LOCALIZED.txt_contact_title.translate
        self.lblMore.text = LOCALIZED.txt_more_title.translate
        
        self.lblDetails.text = LOCALIZED.txt_details_title.translate
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.heightWebviewConstraint.constant = height as! CGFloat
                    
                    self.contentView.isHidden = false
                    
                    Utils.dismissLoading()
                })
            }
            
        })
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            let tLink = navigationAction.request.url?.absoluteString
            
            if let link = tLink {
                Utils.openURL(link)
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
            return
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }



}
