//
//  PullQuickViewController.swift
//  TGCMember
//
//  Created by vang on 4/26/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

let PULL_FULL_HEIGHT: CGFloat = SCREEN_HEIGHT
let PULL_SECOND_HEIGHT: CGFloat = Utils.isIPhoneNotch() ? 318: 308


public enum Position {
    case bottom
    case middle
    case top
}

class PullQuickViewController: PullUpController {
    @IBOutlet weak var anchor: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var coverSummaryView: UIView!
    @IBOutlet weak var coverBottom: UIView!
    @IBOutlet weak var maskCoverBottom: UIView!
    @IBOutlet weak var bottomSummaryConstraint: NSLayoutConstraint!

    var storeCarouselView: StoreCarouselView?
    
    public let firstReviewHeight: CGFloat = Utils.isIPhoneNotch() ? 70 : 40
    public var secondHeight: CGFloat = PULL_SECOND_HEIGHT
    private var fullHeight: CGFloat = PULL_FULL_HEIGHT - 100 + 35
    
    var beginScroll: Bool = false
    
    var currentEventIndex: Int = 0
    var currentPosition: Position = .bottom
    var initialState: InitialState = .contracted
    
    private var isPrepareView: Bool = false
//    private var sectionsInfo: [String] = []
    
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return firstReviewHeight
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }
    
    public var portraitSize: CGSize = .zero
    
    var onClickedMyCurrentLocationCallback: DataCallback?
    var onDisplayCardOnScreenCallback: DataCallback?
    
    var forceDismissSummary: Bool = false
    

    @IBAction func onClickedCurrentLocation(_ sender: Any) {
        self.onClickedMyCurrentLocationCallback?(nil)
    }

    
    public func getCurrentHeight() -> CGFloat {
        if currentPosition == .bottom {
            return firstReviewHeight
        } else if currentPosition == .middle {
            return secondHeight
        }
        
        return 0
        
    }
    
    public func getCurrentPosition() -> Position {
        return currentPosition
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
        
        GLOBAL.GlobalVariables.pullQuickInstance = self
        
        portraitSize = CGSize(width: UIScreen.main.bounds.width, height: fullHeight)
        decorate()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateSummaryViewData(_:)), name: .ON_UPDATE_STORE_CAROUSEL_VIEW, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectedOnMap(_:)), name: .ON_SELECTED_DEAL_ON_MAP, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDismissSummaryView(_:)), name: .ON_DISSMIS_STORE_CAROUSEL_VIEW, object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateSecondHeight(_:)), name: .UPDATED_SECONDE_HIEGHT_PULL, object: nil)
        self.updateLanguages()
        
        NotificationCenter.default.post(name: .UPDATED_POSITION_LOCATION, object: ["stickyPoint": self.firstReviewHeight, "fullHeight": fullHeight])
        
         NotificationCenter.default.post(name: .UPDATED_POSITION_PICKER_CENTER, object: ["stickyPoint": self.firstReviewHeight, "fullHeight": fullHeight, "position": self.currentPosition])
    }
    
    deinit {
           NotificationCenter.default.removeObserver(self, name: .ON_UPDATE_STORE_CAROUSEL_VIEW, object: nil)
           NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_DEAL_ON_MAP, object: nil)
           NotificationCenter.default.removeObserver(self, name: .ON_DISSMIS_STORE_CAROUSEL_VIEW, object: nil)
           NotificationCenter.default.removeObserver(self, name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: .UPDATED_SECONDE_HIEGHT_PULL, object: nil)
       }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           
           if !self.isPrepareView {
               self.isPrepareView = true
               
               preloadBasicDeal()
           }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tHeight = keyboardSize.height
            
            self.bottomSummaryConstraint.constant = tHeight + 20
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomSummaryConstraint.constant = 20
    }
    

    @objc func onDismissSummaryView(_ notification: Notification) {
        self.dismissStoreView()
    }
    
    @objc func onSelectedOnMap(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            self.showSummaryView(uData)
            self.storeCarouselView?.scrollToDeal(uData)
        }
    }
    
    @objc func onUpdateSummaryViewData(_ notification: Notification) {
        //self.dismissKeyboard()
        self.storeCarouselView?.updateData()
    }
    
    
    @objc func updateSecondHeight(_ notification: Notification) {
        if let uData = notification.object as? [String: Any], let isMore = uData["isMore"] as? Bool, isMore == true {
            secondHeight = PULL_SECOND_HEIGHT + 30
        } else {
            secondHeight = PULL_SECOND_HEIGHT
        }
     
    }
   
    
    private func decorate() {

        self.anchor?.layer.cornerRadius = 1.5
        self.maskCoverBottom?.backgroundColor = .clear
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.maskCoverBottom.frame
        rectShape.position = self.maskCoverBottom.center
//        rectShape.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: PULL_FULL_HEIGHT), byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 40, height: 40)).cgPath
        rectShape.fillColor = UIColor.white.cgColor
        self.maskCoverBottom?.layer.insertSublayer(rectShape, at: 0)
        self.maskCoverBottom?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
//        coverBottom?.layer.cornerRadius = CGFloat(40)
        coverBottom.backgroundColor = .clear
        coverBottom?.layer.masksToBounds = true
        coverBottom?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    
        self.coverSummaryView?.backgroundColor = .clear
        
    }
    
    
    
    
    private func preloadBasicDeal() {
        if (self.storeCarouselView == nil) {
            self.storeCarouselView = StoreCarouselView()
            
            self.storeCarouselView?.config(nil, scrollItemCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                
                }, onClickedItemCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    
                    if self.currentPosition == .top {
                        GLOBAL.GlobalVariables.selectedDeal = data
                        self.onDisplayCardOnScreenCallback?(data)
                    } else {
                        self.onDisplayCardOnScreenCallback?(data)
                        GLOBAL.GlobalVariables.selectedDeal = data
                    }
                    
                    NotificationCenter.default.post(name: .ON_SELECTED_DEAL_ON_CAROUSEL, object: data)
                    
                }, onClickedMoreStoreCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    NotificationCenter.default.post(name: .ON_CLICKED_MORE_STORE_ON_CAROUSEL, object: data)
                    
            })
            
            self.storeCarouselView?.view.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: self.coverSummaryView.bounds.height)
            
            self.coverSummaryView.addSubview(self.storeCarouselView!.view)
            UIView.performWithoutAnimation {
                self.coverSummaryView.layoutIfNeeded()
            }
            
            self.addChild(self.storeCarouselView!)
        }
        
        self.storeCarouselView?.attach(self)
        
        self.storeCarouselView?.updateData()
    }
    
    private func dismissStoreView() {
        self.close(fullScreen: true, animated: false)
        GLOBAL.GlobalVariables.selectedCategory = nil
        
        //self.vendorSummaryView?.dismissSelectedCategory()
        
    }
    
    private func showSummaryView(_ data: Any?) -> Void {
        if self.currentPosition == .bottom || self.currentPosition == .middle  {
            self.navigateTo(secondHeight, PULL_FULL_HEIGHT, animated: currentPosition != .middle)
        }
    }
    
    private func getParamsRequest() -> [String: Any] {
        var params = [String: Any]()
        params["lat"] = GLOBAL.GlobalVariables.latestLat
        params["lng"] = GLOBAL.GlobalVariables.latestLng
        
        if GLOBAL.GlobalVariables.selectedAvailableFilterHome != AVAILABLE_TIME.ALL {
            params["type_time"] = GLOBAL.GlobalVariables.selectedAvailableFilterHome.timeSwitch
        }
        
        if let sCategory = GLOBAL.GlobalVariables.selectedCategoryFilterHome {
            params["category_id"] = sCategory["ID"] as? String ?? ""
        }
        
        return params
    }
    
    private func layoutTopViewIfNeed(_ stickyPoint: CGFloat) {
        let tSpace = fullHeight - stickyPoint
        let maxLeft: CGFloat = 75.0
        let minLeft: CGFloat = 20.0
        let anchor: CGFloat = 100
        
        if tSpace < anchor {
            let _space = anchor - tSpace
            let percent = min(_space, maxLeft) / maxLeft
            let value = percent * maxLeft
            
            //print("did drag to percent --->  \(percent)")
            
        
            self.contentView?.backgroundColor = UIColor("rgba 255 255 255 \(percent)")
   
            //self.anchor.alpha = 1.0 - percent
            self.maskCoverBottom?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: Float(min(1.0 - percent, 0.24)), x: 3, y: 4, blur: 13, spread: 0)
            
        } else {
            self.contentView?.backgroundColor = .clear
       
            self.anchor?.alpha = 1.0
            self.maskCoverBottom?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        }
    }
    
    private func dismissKeyboard() {
        Utils.dismissKeyboard()
    }
    
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
        
        NotificationCenter.default.post(name: .UPDATED_POSITION_LOCATION, object: ["stickyPoint": stickyPoint, "fullHeight": fullHeight])
        //print("pullUpControllerWillMove --> ", stickyPoint);
        //self.layoutTopViewIfNeed(stickyPoint)
        if (stickyPoint == firstReviewHeight) {
            currentPosition = .bottom
        } else if (stickyPoint == secondHeight) {
            currentPosition = .middle
        } else if (stickyPoint == fullHeight) {
            currentPosition = .top
        }
        
        
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        //print("did move to \(stickyPoint)")
         NotificationCenter.default.post(name: .UPDATED_POSITION_PICKER_CENTER, object: ["stickyPoint": stickyPoint, "fullHeight": fullHeight, "position": self.currentPosition])
        
        //        if stickyPoint == firstReviewHeight  || stickyPoint == secondHeight {
        //
        //            GLOBAL.GlobalVariables.selectedCategory = nil
        //            self.vendorSummaryView?.dismissSelectedCategory()
        //
        //            NotificationCenter.default.post(name: .UN_FOCUS_DEAL_ON_MAP, object: nil)
        //            NotificationCenter.default.post(name: .ON_DISABLE_HIGHLIGHT_DEAL_ON_CAROUSEL, object: nil)
        //        }
        
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
        
        //print("did drag to \(point)")
       
        self.dismissKeyboard()
    }
    
    
    // MARK: - PullUpController
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            return [secondHeight]
        case .expanded:
            return [firstReviewHeight, secondHeight]
        }
    }
    
    
    override var pullUpControllerSkipPointVerticalVelocityThreshold: CGFloat {
        return 100
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.5,
                           //                           delay: 0,
                //                           usingSpringWithDamping: 0.7,
                //                           initialSpringVelocity: 0,
                //                           options: .curveEaseInOut,
                animations: animations,
                completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }
    
}




extension PullQuickViewController {
    
    func updateLanguages() {
        
    }
}



