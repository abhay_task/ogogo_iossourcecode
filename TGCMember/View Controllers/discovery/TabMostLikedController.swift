//
//  TabNewestController.swift
//  TGCMember
//
//  Created by vang on 11/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabMostLikedController: BaseViewController {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var tblDeals: UITableView!
    private var listDeals:[[String: Any?]]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(updateFavorite(_:)), name: .TOOGLE_FAVORITE_DEAL, object: nil)
        
        self.tblDeals.register(UINib(nibName: "NewDealWideCell", bundle: nil), forCellReuseIdentifier: "NewDealWideCell")
        
        decorate()
        
        getDealsMostLiked()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .TOOGLE_FAVORITE_DEAL, object: nil)
    }
    
    @objc func updateFavorite(_ notification: Notification) {
        self.getDealsMostLiked(false)
    }
    
    
    private func decorate() {
        
    }
    
    private func getDealsMostLiked(_ showLoading: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        APICommonServices.getDealsMostLiked { [weak self] (resp) in
            guard let self = self else {return}
            mPrint("getDealsMostLiked", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] , let list = data["list"]  as? [String : Any?], let records = list["Records"] as? [[String : Any?]] {
                    
                    self.mapDataAfterFetch(records)
                }
            }
            
            if showLoading {
                Utils.dismissLoading()
            }
        }
        
    }
    
    private func mapDataAfterFetch(_ records: [[String : Any?]]) {
        var tList:[[String: Any?]] = []
        
        for index in 0..<records.count {
            let item = records[index]
            
            let isFavorite = item["IsFavorite"] as? Bool ?? false
            
            if isFavorite {
                tList.append(item)
            }
        }
        
        self.listDeals = tList
        
        self.tblDeals.reloadData()
    }
}

extension TabMostLikedController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDeals.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listDeals[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewDealWideCell") as? NewDealWideCell
        
        cell?.config(data as? [String: Any?], onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data, let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController() {
                let dealDetailVC = DealDetailViewController()
                dealDetailVC.dealInfo = tData
                
                topVC.navigationController?.pushViewController(dealDetailVC, animated: true)
            }
        })
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
         return ((SCREEN_WIDTH - 40) * (154/335)) + 12
    }
    
    
}
