//
//  FullMapViewController.swift
//  TGCMember
//
//  Created by vang on 7/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class FullMapViewController: UIViewController {

    @IBOutlet weak var coverBack: UIView!
    var dealInfo: [String: Any?]?
    @IBOutlet weak var mapView: GMSMapView!
    var viewModel: DealDetailViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        decorate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showPositon()
    }

    @IBAction func onClickedClose(_ sender: Any) {
        self.dismissFromSuperview()
    }
    
    private func decorate() {
        coverBack?.layer.cornerRadius = 15.0
        
        configMapStyle()
    }
    
    private func configMapStyle() {
        APICommonServices.fetchContentFromJSON(Constants.GOOGLE_MAP_STYLE_URL) { (resp) in
            if let tResp = resp, let tData = tResp["data"] as? [[String: Any?]] {
                
                let jsonData = try! JSONSerialization.data(withJSONObject: tData, options: .prettyPrinted)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    // print("fetchContentFromJSON STRING --> ", jsonString)
                    
                    setTimeout({
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(jsonString: jsonString)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }, 0)
                } else {
                    if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }
                }
            }
        }
    }
    
    func showPositon() {
        if let location = viewModel.deal.vendorSummary?.location {
            
            let position = CLLocationCoordinate2D(latitude: location.lat ?? 0, longitude: location.lng ?? 0)
            
            if let dealType = viewModel.deal.type {
                let marker = GMSMarker()
                marker.position = position
                
                let iconImageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 45)))
                iconImageView.image = dealType.iconValue
                marker.iconView = iconImageView
                
                marker.zIndex = 9999
                marker.groundAnchor = CGPoint(x: 0.5, y: 1)
                
                marker.map = mapView
                
                self.animateToPoint(position)
            }
        }
    }
    
    func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 17.0)
        mapView.animate(with: camera)
    }
}
