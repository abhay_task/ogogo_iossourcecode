//
//  MessagesViewController.swift
//  TGCMember
//
//  Created by jonas on 10/23/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit
import Toast_Swift

class MessagesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var conversations = [GroupMessagesResponse.DataClass.List.Record]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset.top = 20
        tableView.tableFooterView = UIView()
        
        showLoadingIndicator()
        Repository.shared.getGroupMessages(completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            self.dismissLoadingIndicator()
            
            switch result {
            case .success(let response):
                self.conversations = response.data?.list?.records?.filter { $0.lastMessageSendAt != nil }.sorted(by: { CacheManager.shared.pinnedConversationIDs.contains($0.partner?.id ?? "") && !CacheManager.shared.pinnedConversationIDs.contains($1.partner?.id ?? "") }) ?? []
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    @IBAction func didTapBackButton(sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleLoangPressed(_ recognizer: UIGestureRecognizer) {
        if recognizer.state == .began {
            
            guard let index = recognizer.view?.tag,
                    let convo = conversations[safe: index],
                  let id = convo.partner?.id  else { return }
            
            let actionSheetView = CustomActionSheetView.loadView()
            actionSheetView.frame = UIScreen.main.bounds
            
            view.addSubview(actionSheetView)
            actionSheetView.show()
            actionSheetView.pinlabel.text = CacheManager.shared.pinnedConversationIDs.contains(id) ? "Unpin" : "Pin"
            
            actionSheetView.onTapPin = { [unowned self] in
                if CacheManager.shared.pinnedConversationIDs.contains(id) {
                    CacheManager.shared.pinnedConversationIDs.removeAll(where: { $0 == id })
                } else {
                    CacheManager.shared.pinnedConversationIDs.append(id)
                }
                
                conversations = conversations.sorted(by: { CacheManager.shared.pinnedConversationIDs.contains($0.partner?.id ?? "") && !CacheManager.shared.pinnedConversationIDs.contains($1.partner?.id ?? "") })
                
                actionSheetView.dismissView()
                
                showToast(text: CacheManager.shared.pinnedConversationIDs.contains(id) ? "Pinned" : "Unpinned")
            }
            
            actionSheetView.onTapArchive = { [unowned self] in
                actionSheetView.dismissView()
                showToast(text: "Not available at this time")
            }
        }
    }
    
    private func showToast(text: String) {
        var style = ToastStyle()
        style.cornerRadius = 20
        
        let point = CGPoint(x: view.center.x, y: view.frame.maxY * 0.9)
        view.makeToast(text,
                  point: point
                  , title: nil, image: nil, style: style, completion: nil)
    }
}

extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ConversationCell.dequeue(tableView, indexpath: indexPath)
        
        if let conversation = conversations[safe: indexPath.row] {
            cell.otherPeopleImageView.setKfImage(conversation.partner?.avatar ?? "")
            cell.otherPersoNameLabel.text = conversation.partner?.name
            cell.otherPersonVendorLabel.text = conversation.partner?.businessName
            cell.contentView.subviews.first?.tag = indexPath.row
            cell.pinButton.isHidden = !CacheManager.shared.pinnedConversationIDs.contains(conversation.partner?.id ?? "")
            
            let gest = UILongPressGestureRecognizer(target: self, action: #selector(handleLoangPressed(_:)))
            cell.contentView.subviews.first?.addGestureRecognizer(gest)
            
            cell.onTapDelete = { [unowned self] in
                let alertView = DeleteConfirmAlertView()
                
                Utils.showAlertWithCustomView(alertView)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let conversation = conversations[safe: indexPath.row], let id = conversation.partner?.id else { return }
        
        openChatDetail(["PartnerID": id])
    }
}

extension MessagesViewController {
    func updateLanguages() {
        
    }
}
