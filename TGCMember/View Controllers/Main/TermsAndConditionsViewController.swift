//
//  TermsAndConditionsViewController.swift
//  TGCMember
//
//  Created by jonas on 5/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit
import Toast_Swift

class TermsAndConditionsViewController: BaseViewController {
    
    @IBOutlet weak var declineButton: CustomButton!
    @IBOutlet weak var acceptButton: CustomButton!
    @IBOutlet weak var headerheight: NSLayoutConstraint!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var pageWebView: PagesWebView!
    @IBOutlet weak var buttonsBottomSpace: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerheight.constant = Device.iPhoneWithNotch ? 120 : 100
        navTitleLabel.text = LOCALIZED.terms_and_conditions.translate
        
//        guard let request = Repository.shared.generateTermsAndConditionsContentRequest() else { return }
        Repository.shared.getTermsAndCondtionsData(completionHandler: { [weak self] data in
            guard let self = self else { return }
            self.pageWebView.initView(data: data, loadingYOffset: -40)
        })
        
        if CacheManager.shared.termsAndConditionsAccepted == true {
            [acceptButton, declineButton].forEach { $0?.superview?.isHidden = true }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: { [weak self] in
                guard let self = self else { return }
                self.showToast(text: "Accepted")
            })
        }
        
        if !Device.iPhoneWithNotch {
            buttonsBottomSpace.constant = 20
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        dismissWithPopAnim()
    }
    
    @IBAction func didTapDeclineButton(_ sender: Any) {
        let alertView = TermsConditionsAlertView.loadView()
        Utils.showAlertWithCustomView(alertView)
        
        alertView.onTapRightButton = { [unowned self] in
            Utils.showLoading()
            DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: { [weak self] in
                Utils.dismissLoading()
                
                CacheManager.shared.termsAndConditionsAccepted = false
                let dialogView = TermsConditionsAlertView.loadView()
                dialogView.config(title: "Ogogo Member",
                                  message: "Sorry, you cannot use the app because you declined the Terms & Conditions.\nPlease uninstall and re-install this app if you would like to consider.")
                Utils.showAlertWithCustomView(dialogView)
                
                dialogView.noButton.isHidden = true
                dialogView.rightButton.setTitle("Okay", for: .normal)
                dialogView.onTapRightButton = {
                    exit(2)
                }
            })
        }
    }
    
    @IBAction func didTapAcceptButton(_ sender: Any) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: { [weak self] in
            guard let self = self else { return }
            Utils.dismissLoading()
            CacheManager.shared.termsAndConditionsAccepted = true
            
            self.acceptButton.isEnabled = false
            self.declineButton.isEnabled = false
            
            self.showToast(text: "Accepted")
        })
        
        Utils.showLoading()
    }
    
    func showToast(text: String) {
        var style = ToastStyle()
        style.cornerRadius = 20
        
        let point = CGPoint(x: view.center.x, y: (acceptButton.superview?.superview?.frame.origin.y ?? 0) - 35)
        view.makeToast(text,
                       point: point
                       , title: nil, image: nil, style: style, completion: nil)
    }
}

extension TermsAndConditionsViewController {
    func updateLanguages() {
        
    }
}
