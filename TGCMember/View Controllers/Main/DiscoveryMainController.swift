//
//  MainViewController.swift
//  TGCMember
//
//  Created by vang on 5/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Kingfisher
import CardStackView

enum DISCOVERY_SCREEN_ID: Int {
    case MENU = -1
    case MAP_VIEW = 0
    case LIST_VIEW = 1
    case WALLET_VIEW = 2
}

enum SWIPE_DEAL_TYPE: String {
    case STARTER = "starter"
    case DAILY = "daily"
}
class DiscoveryMainController: BaseViewController {
    
    @IBOutlet weak var menuButton: UIButton! {
        didSet {
            menuButton.imageView?.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var walletButton: UIButton!
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var menuButtonContainerView: CustomView!
    @IBOutlet weak var searchButtonContainerView: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var searchTextFieldView: UIView!
    
    private var mTabBarController: UITabBarController = UITabBarController()
    private var tempTabIndex: Int =  DISCOVERY_SCREEN_ID.MAP_VIEW.rawValue
    
    var inputTimer: Timer?
    var isShowSearchInput: Bool = false
    var callbackMyWallet: DataCallback?
    var isFreeMemberDialogShowed = false
    var tabMapView = HomeMapViewController()
    let baseViewModel = BaseViewModel(repository: Repository.shared)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        CacheManager.shared.dailyShowDatePerUser = [:]
        
        if CacheManager.shared.termsAndConditionsAccepted == false {
            showCannotUseAppDialog()
            return
        }
        
        refreshViewAfterAuthorized()
            
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(getSwipeDeals(_:)), name: .BEGIN_CHECK_SWIPE_DEALS, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePositionLocation(_:)), name: .UPDATED_POSITION_LOCATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterAuthorized), name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterAuthorized), name: .REFRESH_MENU_VIEW, object: nil)
//
//        homeWalletViewModel.getDeals()
        renderContent()
        buttonContainerView.roundCorners([.topRight, .bottomRight], radius: 12)
//        onActive(fromIndex: 0, toIndex: tempTabIndex)
    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()

        baseViewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
        }

        baseViewModel.onComplete = { [weak self] in
            guard let self = self else { return }

            self.dismissLoadingIndicator()
        }

        baseViewModel.onErrorWithMessage = { [weak self] msg in
            guard let self = self else { return }

            self.dismissLoadingIndicator()
            print(msg)
        }

        baseViewModel.onHomeSearchMapComplete = { [weak self] vm in
            guard let self = self else { return }

            self.dismissLoadingIndicator()

            self.showVc(with: Storyboard.wallet, classType: SearchResultViewController.self, viewController: { vc in
                vc.viewModel = vm
            })
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .BEGIN_CHECK_SWIPE_DEALS, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATED_POSITION_LOCATION, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_UPDATE_CATEGORY_VIEW, object: nil)
    }
    
    func showCannotUseAppDialog() {
        let dialogView = TermsConditionsAlertView.loadView()
        dialogView.config(title: "Ogogo Member",
                          message: "Sorry, you cannot use the app because you declined the Terms & Conditions.\nPlease uninstall and re-install this app if you would like to consider.")
        Utils.showAlertWithCustomView(dialogView, bgColor: .black)
        
        dialogView.noButton.isHidden = true
        dialogView.rightButton.setTitle("Okay", for: .normal)
        dialogView.onTapRightButton = {
            exit(2)
        }
    }
    
    @objc func refreshViewAfterAuthorized() {
        if let urlString = (GLOBAL.GlobalVariables.userInfo)?["AvatarURL"] as? String,
           let url = URL(string: urlString) {
            KingfisherManager.shared.retrieveImage(with: url) { [weak self] result in
                guard let self = self else { return }
                
                switch result {
                case .success(let imageResult):
                    DispatchQueue.main.async {
                        self.menuButton.setImage(imageResult.image, for: .normal)
                    }
                case .failure(let error):
                    print(error.errorDescription)
                }
            }
        } else {
            menuButton.setImage(nil, for: .normal)
        }
    
    }
    
    
    @IBAction func onClickedSearch(_ sender: Any) {
//        self.isShowSearchInput = !self.isShowSearchInput
//            if GLOBAL.GlobalVariables.selectedSearchText != "" {
//                GLOBAL.GlobalVariables.selectedSearchText = ""
//
//                self.textFieldDidChange()
//            }
//        searchInputScreen.show(in: contentView)
        
        let searchFieldView = SearchTextFieldView()
        searchFieldView.show(in: contentView)
        
        searchFieldView.onSearch = { [unowned self] text in
            var request = tabMapView.createRequest()
            request.keyword = text
            
            baseViewModel.searchMapForDeals(request: request)
        }
    }
    
    @IBAction func onClickedMenu(_ sender: Any) {
//        self.toggleLeft()
        let vc = MenuViewController()
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        
        present(vc, animated: false, completion: nil)
    }
    
    @IBAction func onClickedClear(_ sender: Any) {
//        self.tfSearch.text = ""
        GLOBAL.GlobalVariables.selectedSearchText = ""
        
//        self.btnClear.alpha = 0
        
        self.textFieldDidChange()
    }
    
    
    
    private func handleFilter() {
        GLOBAL.GlobalVariables.selectedDeal = nil
        NotificationCenter.default.post(name: .FILTER_WITH_TEXT_ON_CAROUSEL, object: nil)
    }
    
    
    private func layoutTopViewIfNeed(_ stickyPoint: CGFloat, _ fullHeight: CGFloat) {
        let tSpace = fullHeight - stickyPoint
        let maxLeft: CGFloat = 75.0
        let anchor: CGFloat = 100
        
        if tSpace < anchor {
            let _space = anchor - tSpace
            let percent = min(_space, maxLeft) / maxLeft
            
            
            //print("did drag to percent --->  \(percent)")
            self.maskView?.backgroundColor = UIColor("rgba 255 255 255 \(percent)")
            self.maskView?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: Float(min(0.24, percent)), x: 3, y: 4, blur: 13, spread: 0)
        } else {
            self.maskView?.backgroundColor = .clear
            self.maskView?.layer.removeSketchShadow()
        }
    }
    
    
    @objc func updatePositionLocation(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let stickyPoint = uData["stickyPoint"] as? CGFloat ?? 0.0
            let fullHeight = uData["fullHeight"] as? CGFloat ?? 0.0
            
            self.layoutTopViewIfNeed(stickyPoint, fullHeight)
            
        }
    }
    
    @objc func getSwipeDeals(_ notification: Notification) {
        let userID = try? User(dict: GLOBAL.GlobalVariables.userInfo ?? [:]).id
        if GLOBAL.GlobalVariables.isFirstInitApp, CacheManager.shared.shouldShowStarter(userID ?? "") {
            setTimeout({
                self.checkToShowStarter()
            }, TimeInterval(GLOBAL.GlobalVariables.sysConfigModel.timePushStarter * 1000))
        } else {
            guard CacheManager.shared.shouldShowDaily(userID ?? "") else { return }
            setTimeout({
                self.checkToShowDaily()
            }, 100)
        }
    }
    
    func showDailyQuestions() {
        let cards = [DailyQuestionView.loadView(), DailyQuestionView.loadView(), DailyQuestionView.loadView()]
        let cardStackView = CardStackView(cards: cards, showsPagination: false, maxAngle: 10, randomAngle: false)
        cardStackView.translatesAutoresizingMaskIntoConstraints = false
    
        Utils.showAlertWithCustomView(cardStackView)
        
        cardStackView.onFinished = {
            Utils.closeCustomAlert()
        }
        
        
        NSLayoutConstraint(item: cardStackView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: cardStackView.superview,
                           attribute: .centerX,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        NSLayoutConstraint(item: cardStackView,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: cardStackView.superview,
                           attribute: .centerY,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        NSLayoutConstraint(item: cardStackView,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: cardStackView.superview,
                           attribute: .width,
                           multiplier: 0.75,
                           constant: 0).isActive = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        GLOBAL.GlobalVariables.discoveryInstance = self
        
        if !isFreeMemberDialogShowed {
            showFreeMemberReminderDialog()
            isFreeMemberDialogShowed.toggle()
        }
        //MARK: Hide by Abhay
       // showDailyQuestions()
    }
    
    private func checkToShowDaily() {
        setTimeInterval({ [weak self] (number, timer) in
            guard let self = self else {return}
            print("checkToShowDaily - ING")
            if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
                if GLOBAL.GlobalVariables.isShowLoyaltyFirst == false || _upgradeModel.canUpToNextStep == .BASIC {
                    timer.invalidate()
                    self.handleGetSwipeDeals(.DAILY)
                }
            }
        }, 1000)
    }
    
    private func checkToShowStarter() {
        setTimeInterval({ [weak self] (number, timer) in
            guard let self = self else {return}
            print("checkToShowStarter - ING")
                if Utils.isValidToSwipeDeal() {
                    timer.invalidate()
                    self.handleGetSwipeDeals(.STARTER)
                }
            }, 1000)
    }
    
    private func handleGetSwipeDeals(_ type: SWIPE_DEAL_TYPE = .DAILY) {
        Utils.showLoading()
        
        APICommonServices.getSwipeDeals(type.rawValue) { [weak self] (resp) in
            guard let self = self else {return}
            
            GLOBAL.GlobalVariables.isFirstInitApp = false
            // mPrint("getSwipeDeal(\(type.rawValue.uppercased())) --> ", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true, let deals = resp["data"] as? [[String: Any?]], deals.count > 0  {
                if let userID = try? User(dict: GLOBAL.GlobalVariables.userInfo ?? [:]).id {
                    if type == .DAILY {
                        // TODO: -
                        CacheManager.shared.dailyShowDatePerUser[userID] = Date()
                    } else {
                        CacheManager.shared.starterShowDatePerUser[userID] = Date()
                    }
                }
                
                self.openSwipeDeals(deals, false, false, type == SWIPE_DEAL_TYPE.STARTER)
            }
            
            Utils.dismissLoading()
            
        }
    }
    
    func updateLanguages() {
        
    }

    
    
    @IBAction func onClickedTabMapView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .THINGS_TO_DO
        //        self.navigate(toIndex: WALLET_SCREEN_ID.THINGS_TO_DO.rawValue)
        
    }
    
    @IBAction func onClickedTabListView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .TRIP_LIST
        //        self.navigate(toIndex: WALLET_SCREEN_ID.TRIP_LIST.rawValue)
    }
    
    @IBAction func onClickedTabWalletView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .SAVED_EVENTS
        //        self.navigate(toIndex: WALLET_SCREEN_ID.SAVED_EVENTS.rawValue)
    }
    
    @IBAction func didTapWalletButton(_ sender: Any) {
        presentVc(with: .overFullScreen, transition: .crossDissolve, storyboard: Storyboard.wallet, classType: HomeWalletViewController.self, viewController: { vc in
            vc.viewModel = HomeWalletViewModel(repository: Repository.shared)
        })
    }
    
    private func toggleMenu() {
        self.toggleLeft()
    }
    
    private func renderContent() {
        
//        let tabMapView = HomeMapViewController()
        tabMapView.tabClickeCallback = { tabIndex in
            self.navigate(toIndex: tabIndex)
        }
        
        let tabListView = HomeListViewController()
        tabListView.tabClickeCallback = { tabIndex in
            self.navigate(toIndex: tabIndex)
        }
        
        // NEW
        let walletVc = homeWalletVC
        walletVc.tabClickeCallback = { [unowned self] index in
            navigate(toIndex: index)
        }
        
        mTabBarController.delegate = self
        //        mTabBarController.viewControllers = [tabMapView, tabListView, tabWalletView]
        mTabBarController.viewControllers = [tabMapView]
        mTabBarController.tabBar.isHidden = true
        mTabBarController.selectedIndex = tempTabIndex
        
        self.contentView.addSubview(mTabBarController.view)
        self.addChild(mTabBarController)
        
        mTabBarController.view.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
        
        contentView.bringSubviewToFront(walletButton)
        contentView.bringSubviewToFront(menuButtonContainerView)
        contentView.bringSubviewToFront(searchButtonContainerView)
        contentView.bringSubviewToFront(buttonContainerView)
    }
    
    private func onActive(fromIndex: Int, toIndex: Int) {
        
        
    }
    
    
    func navigate(toIndex: Int) -> Void {
        //  let controllerIndex = tabBarController.viewControllers?.firstIndex(of: viewController)
        if toIndex == DISCOVERY_SCREEN_ID.MENU.rawValue {
            self.toggleMenu()
            
            return
        }
        
        
        if mTabBarController.selectedIndex == toIndex || toIndex == self.tempTabIndex {
            return;
        }
        
        self.onActive(fromIndex: self.tempTabIndex, toIndex: toIndex)
        
        self.tempTabIndex = toIndex
        
        
        // Get the views.
        let fromView = mTabBarController.selectedViewController?.view
        let toView = mTabBarController.viewControllers![toIndex].view
        
        // Get the size of the view area.
        let viewSize: CGRect = fromView!.frame
        let scrollRight: Bool = toIndex > mTabBarController.selectedIndex
        
        // Add the to view to the tab bar view.
        fromView!.superview?.addSubview(toView!)
        
        // Position it off screen.
        let screenWidth: CGFloat = UIScreen.main.bounds.width
        toView!.frame = CGRect(x: (scrollRight ? screenWidth : -screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            fromView!.frame = CGRect(x: (scrollRight ? -screenWidth : screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
            toView!.frame = CGRect(x: 0, y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        }, completion: {  [weak self] (finished) in
            guard let self = self else {return}
            
            if finished {
                // Remove the old view from the tabbar view.
                fromView!.removeFromSuperview()
                self.mTabBarController.selectedIndex = toIndex
            }
        })
        
    }
    
}

extension DiscoveryMainController: UITextFieldDelegate {
    
    func textFieldDidChange() {
        if let timer =  self.inputTimer {
            timer.invalidate()
            self.inputTimer = nil;
        }
        
        self.inputTimer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
            self.handleFilter()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.handleFilter()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            GLOBAL.GlobalVariables.selectedSearchText = updatedText
            
            self.textFieldDidChange()
            //self.handleFilter()
        }
        
        return true
    }
    
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == self.tfSearch {
//            if let _pull = GLOBAL.GlobalVariables.pullQuickInstance {
//                _pull.navigateTo(PULL_FULL_HEIGHT, PULL_FULL_HEIGHT, animated: true)
//            }
//        }
//
//        return true
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //_ = validateCode()
    }
}

extension DiscoveryMainController: UITabBarControllerDelegate {
    
    
}


extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
