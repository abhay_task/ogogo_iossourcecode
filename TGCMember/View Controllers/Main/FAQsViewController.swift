//
//  FAQsViewController.swift
//  TGCMember
//
//  Created by jonas on 5/21/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit
import Toast_Swift

class FAQsViewController: BaseViewController {
    @IBOutlet weak var headerheight: NSLayoutConstraint!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var faqs = [FAQsResponse.FAQ]()
    
    var selectedIndices = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitleLabel.text = LOCALIZED.faqs_title.translate
        headerheight.constant = Device.iPhoneWithNotch ? 120 : 100
        
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        dismissWithPopAnim()
    }
    
    @IBAction func didTapIntercomButton(_ sender: Any) {
        IntercomManager.shared.showMessenger()
    }
}

extension FAQsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = FAQCell.dequeue(tableView, indexpath: indexPath)
        cell.selectionStyle = .none
        cell.questionLabel.text = faqs[indexPath.row].question
        cell.answerLabel.text = faqs[indexPath.row].answer
        cell.answerLabelContainerView.isHidden = !selectedIndices.contains(indexPath.row)
        cell.answerLabel.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndices.contains(indexPath.row) {
            selectedIndices.removeAll(where: { $0 == indexPath.row })
        } else {
            selectedIndices.append(indexPath.row)
        }
        
        tableView.reloadData()
    }
}

extension FAQsViewController {
    func updateLanguages() {
        
    }
}
