//
//  AboutViewController.swift
//  TGCMember
//
//  Created by jonas on 8/27/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

private enum InteractiveViewType: Int {
    case learnMoreOgogo = 0
    case learnMoreGcard
    case joinVendor
    case joinPartner
    case appVersion
    
    var url: String? {
        switch self {
        case .appVersion: return nil
        case .joinPartner: return "https://sales.thegcard.com/"
        case .joinVendor: return "https://vendor.thegcard.com/"
        case .learnMoreGcard: return "https://www.thegcard.com/"
        case .learnMoreOgogo: return "https://www.ogogo.com/"
        }
    }
}

class AboutViewController: BaseViewController {
    @IBOutlet weak var headerheight: NSLayoutConstraint!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet var interactiveViews: [InteractiveView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerheight.constant = Device.iPhoneWithNotch ? 120 : 100
        
        versionLabel.text = "\(Bundle.main.releaseVersionNumber ?? "") build \(Bundle.main.buildVersionNumber ?? "")"
        
        interactiveViews.forEach { vw in
            vw.onTap = { [unowned self] _ in
                let type = InteractiveViewType(rawValue: vw.tag)
                
                if let url = type?.url {
                    guard let url = URL(string: url) else { return }
                    UIApplication.shared.open(url)
                } else {
                    view.makeToast("Last update on: August 27, 2021")
                }
            }
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension AboutViewController {
    func updateLanguages() {
        
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}
