//
//  HomeWalletViewController.swift
//  TGCMember
//
//  Created by vang on 11/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class HomeWalletViewController: BaseViewController {
    fileprivate enum CollectionType: Int {
        case interested = 0
        case deal
    }
    
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var interestedCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: NoRecordFoundCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: NoRecordFoundCell.className())
        }
    }
    @IBOutlet weak var allDealsLabel: UILabel!
    @IBOutlet weak var allDealsInteractiveView: InteractiveView!
    @IBOutlet weak var myWalletLabel: UILabel!
    
    var tabClickeCallback: TabClickedCallback?
    
    var callback: DataCallback?
    
    private var cardOnScreen: CardOnScreenView?
    var viewModel: HomeWalletViewModel!
    var work: DispatchWorkItem?
    lazy var autoCompleteView: SuggestionAutocompleteView = {
        return SuggestionAutocompleteView()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getDeals()
    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        
        viewModel.onStartLoading = { [unowned self] in
            DispatchQueue.main.async { [unowned self] in
                showNativeLoading()
                showLoadingIndicator()
                tableView.reloadData()
            }
        }
        
        viewModel.onComplete = { [weak self] in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.dismissNativeLoading()
                self.dismissLoadingIndicator()
                self.tableView.reloadData()
                self.interestedCollectionView.reloadData()
            }
        }
        
        viewModel.onErrorWithMessage = { [weak self] msg in
            DispatchQueue.main.async {
                guard let self = self else { return }
                self.dismissNativeLoading()
                self.dismissLoadingIndicator()
                self.tableView.reloadData()
                print(msg)
            }
        }
        
        viewModel.onSearchComplete = { [weak self] vm in
            DispatchQueue.main.async {guard let self = self else { return }
                self.dismissNativeLoading()
                self.dismissLoadingIndicator()
                self.tableView.reloadData()
                self.showVc(with: Storyboard.wallet, classType: SearchResultViewController.self, viewController: { vc in
                    vc.viewModel = vm
                })
            }
        }
    }
    
    override func setupUI() {
        super.setupUI()
        
        allDealsLabel.textColor = Color.green
        
        allDealsInteractiveView.onTap = { [unowned self] _ in
            showVc(with: Storyboard.wallet, classType: DealsViewController.self, viewController: { [unowned self] vc in
                let vm = DealsViewModel(repository: viewModel.repository, dealType: .all)
                vc.viewModel = vm
            })
        }
        
        myWalletLabel.text = LOCALIZED.txt_my_wallet.translate
        allDealsLabel.text = LOCALIZED.all_deals.translate
    }
    
    @IBAction func didTapPlanYourTripButton(_ sender: Any) {
        initiateTripView()
    }
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        let searchFieldView = SearchTextFieldView()
        searchFieldView.show(in: view)
        
        searchFieldView.onSearch = { [unowned self] text in
            viewModel.searchDeals(with: text)
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func initiateTripView(_ onExistingTrip: Bool = false, index: Int? = nil) {
        let createTripView = CreateYourTripView()
        
        viewModel.getTripPreferences(completionHandler: { [weak self] trip in
            guard let self = self else { return }
            
            if onExistingTrip, let index = index {
                self.viewModel.setTripPreferencesAt(index)
            }
            
            createTripView.set(with: self, wishList: onExistingTrip ? self.viewModel.tripPreferences?.wishList : [], willEdit: onExistingTrip)
            
            DispatchQueue.main.async {
                Utils.showAlertWithCustomView(createTripView, bgColor: UIColor(47, 72, 88, 0.8), animation: false)
            }
            
            self.dismissLoadingIndicator()
            
            let travellingList = trip.travellingWithList.map { $0.value }
            let goingForList = trip.goingForList.map { $0.value }
            let interestedInList = trip.interestedInList.map { $0.value }
            
            let travellingWithLabel = createTripView.travellingWithInteractiveView.subviews.first as? UILabel
            let goingForLabel = createTripView.goingForInteractiveView.subviews.first as? UILabel
            let interestedInLabel = createTripView.interestedInInteractiveView.subviews.first as? UILabel
            
            travellingWithLabel?.text = self.viewModel.tripPreferences?.travellingWith.value
            interestedInLabel?.text = self.viewModel.tripPreferences?.interestedIn.map { $0.value }.joined(separator: ", ")
            goingForLabel?.text = self.viewModel.tripPreferences?.goingFor.value
            
            createTripView.tripNameTextField.text = self.viewModel.tripPreferences?.name
            createTripView.goingToNameTextField.text = self.viewModel.tripPreferences?.address
            (createTripView.toDateInteractiveView.subviews.first as? UILabel)?.text = self.viewModel.tripPreferences?.toDate.toDate()?.toString(format: "MMM dd, yyyy")
            (createTripView.fromDateInteractiveView.subviews.first as? UILabel)?.text = self.viewModel.tripPreferences?.fromDate.toDate()?.toString(format: "MMM dd, yyyy")
            createTripView.selectedFromDate = self.viewModel.tripPreferences?.fromDate ?? ""
            createTripView.selectedToDate = self.viewModel.tripPreferences?.toDate ?? ""
            
            
            createTripView.onTapTravellingWithInteractiveView = { tripView in
                let pickerView = PickerTableView()
                
                let list = travellingList
                let selectedIndex = goingForList.firstIndex(where: { self.viewModel.tripPreferences?.travellingWith.value == $0 }) ?? 0
                pickerView.show(.generic(list), title: "I'm travelling with", selectedRow: selectedIndex)
                pickerView.onSelectRow = { index in
                    self.viewModel.tripPreferences?.travellingWith = trip.travellingWithList[index]
                    travellingWithLabel?.text = list[safe: index]
                }
            }
            
            createTripView.onTapGoingForInteractiveView = { tripView in
                let pickerView = PickerTableView()
                
                let list = goingForList
                let selectedIndex = goingForList.firstIndex(where: { self.viewModel.tripPreferences?.goingFor.value == $0 }) ?? 0
                pickerView.show(.generic(list), title: "I'm going for", selectedRow: selectedIndex)
                pickerView.onSelectRow = { index in
                    self.viewModel.tripPreferences?.goingFor = trip.goingForList[index]
                    goingForLabel?.text = list[safe: index]
                }
            }
            
            createTripView.onTapInterestedInInteractiveView = { tripView in
                let checkListView = CheckListView()
                
                let list = interestedInList
                let selectedIndices = self.viewModel.tripPreferences?.interestedIn.compactMap { item in
                    list.firstIndex(where: { item.value == $0 })
                }
                checkListView.show(items: list, selectedIndices: selectedIndices ?? [0])
                checkListView.onChangeSelectedIndices = { indices in
                    self.viewModel.tripPreferences?.interestedIn = indices.map { trip.interestedInList[$0] }
                    let selectedItems = indices.compactMap { list[safe: $0] }
                    interestedInLabel?.text = selectedItems.joined(separator: ",")
                }
            }
            
            createTripView.selectedFromDateChanged = { dateString in
                self.viewModel.tripPreferences?.fromDate = dateString
            }
            
            createTripView.selectedToDateChanged = { dateString in
                self.viewModel.tripPreferences?.toDate = dateString
            }
            
            // Text change
            createTripView.onNameChange = { text in
                self.viewModel.tripPreferences?.name = text
            }
            
            createTripView.onLocationTextChange = { text in
                
                self.viewModel.autoCompleteRequest?.cancel()
                self.work?.cancel()
                
                guard !text.isEmpty else {
                    self.autoCompleteView.dismiss()
                    return
                }
                
                self.work = DispatchWorkItem(block: {
                    self.viewModel.getPlaceAutocomplete(text, completion: { [weak self] predictions in
                        guard let self = self else { return }
                        DispatchQueue.main.async {
                            self.autoCompleteView.show(in: createTripView.containerView,
                                            frame: CGRect(origin: createTripView.goingToTextFieldBottomOrigin,
                                                          size: CGSize(width: createTripView.goingToNameTextField.bounds.width,
                                                                       height: (55.0 * 5))),
                                            list: predictions)
                            
                        }
                    })
                })
                
                self.autoCompleteView.onSelect = { item in
                    createTripView.goingToNameTextField.text = item.desc
                    self.viewModel.getPlaceDetails(item.placeID)
                }
                
                if let work = self.work {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: work)
                }
            }
        })

        self.viewModel.onTripPreferencesCompletionCallback = { bool in
            createTripView.saveButton.isEnabled = bool
        }
        
        createTripView.onTapSave = {
            if let fromDate = self.viewModel.tripPreferences?.fromDate,
               let toDate = self.viewModel.tripPreferences?.toDate {
                
                if fromDate > toDate {
                    let alertView = TGCAlertViews.loadInfo(title: "Invalid dates", message: "Start date should not ahead of end date.")
                    Utils.showAlertWithCustomView(alertView)
                    return
                }
            }
            
            createTripView.dismiss()
            
            self.viewModel.saveTrip(completion: { object in
                let alertView = CustomAlert().loadOneButtonView()
                Utils.showAlertWithCustomView(alertView)
                
                alertView.config(object.dictionary, onClickedConfirmCallback: { _ in
                    Utils.closeCustomAlert()
                    self.viewModel.getDeals()
                })
            })
        }
        
        createTripView.onAddWish = { [unowned self] prop in
            viewModel.appendWishInTripPref(index: prop.0, name: prop.1)
            createTripView.wishList = viewModel.tripPreferences?.wishList ?? []
        }
        
        createTripView.onTapRemoveWish = { [unowned self] index in
            viewModel.tripPreferences?.wishList.remove(at: index)
            createTripView.wishList = viewModel.tripPreferences?.wishList ?? []
        }
        
        createTripView.onTapDelete = { [unowned self] in
            if let index = index {
                let confirmAlert = DeleteConfirmAlertView()
                Utils.showAlertWithCustomView(confirmAlert)
                
                confirmAlert.onTapDelete = { [unowned self] in
                    viewModel.deleteTripAt(index, completion: { object in
                        DispatchQueue.main.async {
                            let alertView = CustomAlert().loadOneButtonView()
                            Utils.showAlertWithCustomView(alertView)
                            
                            alertView.config(object.dictionary, onClickedConfirmCallback: { _ in
                                Utils.closeCustomAlert()
                                self.viewModel.getDeals()
                            })
                        }
                    })
                }
            }
        }
    }
}



extension HomeWalletViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isLoading { return 0 }
        return viewModel.numberOfCarousels == 0 ? 1 : viewModel.numberOfCarousels
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.numberOfCarousels == 0 {
            let cell = NoRecordFoundCell.dequeue(tableView, indexpath: indexPath)
            return cell
        }
        
        let cell = DealsParentCell.dequeue(tableView, indexpath: indexPath)
        cell.dealsCollectionView.carouselIndex = indexPath.row
        cell.dealsCollectionView.reloadData()
        cell.setContents(item: viewModel, index: indexPath.row)
        cell.onTapSeeAll = { [unowned self] in
            showVc(with: Storyboard.wallet, classType: DealsViewController.self, viewController: { vc in
                vc.viewModel = viewModel.generateDealsVM(indexPath.row)
            })
        }
        
        cell.onTapTitleContainer = { [unowned self] in
            initiateTripView(true, index: indexPath.row)
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.numberOfCarousels == 0 {
            return screenHeight * 0.4
        }
        
        return UITableView.automaticDimension
    }
}

extension HomeWalletViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let type = CollectionType(rawValue: collectionView.tag) ?? .interested
        
        switch type {
        case .interested:
            return viewModel.numberOfCategories
        case .deal:
            guard let collectionView = collectionView as? DealsCollectionView else { return 1 }
            let count = viewModel.dealsListCountAt(collectionView.carouselIndex)
            return count == 0 ? 1 : count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let type = CollectionType(rawValue: collectionView.tag) ?? .interested
        
        switch type {
        case .interested:
            let cell = InterestedCell.dequeue(collectionView, indexpath: indexPath)
            cell.setContents(item: viewModel,
                             index: indexPath.item,
                             isSelected: viewModel.isCategorySelectedAt(indexPath.item))
            return cell
        case .deal:
            let collectionView = collectionView as! DealsCollectionView
            
            if viewModel.dealsListCountAt(collectionView.carouselIndex) == 0 {
                let cell = DealsEmptyCell.dequeue(collectionView, indexpath: indexPath)
                return cell
            }
            
            if !viewModel.hasDealID(indexPath.item, groupIndex: collectionView.carouselIndex) {
                let cell = LoyaltyDealOtherCell.dequeue(collectionView, indexpath: indexPath)
                cell.dealImageView.setKfImage(viewModel.imageURLStringOfDealAt(indexPath.item, groupIndex: collectionView.carouselIndex), contentMode: .scaleAspectFit)
                return cell
            }
            
            let cell = DealCell.dequeue(collectionView, indexpath: indexPath)
            cell.onToggleFavorite = { [unowned self] in
                viewModel.toggleFavoriteAt(indexPath.item,
                                           groupIndex: collectionView.carouselIndex)
            }
            
            cell.onTapDelete = { [unowned self] in
                let confirmAlert = DeleteConfirmAlertView()
                Utils.showAlertWithCustomView(confirmAlert)
                
                confirmAlert.onTapDelete = { [unowned self] in
                    viewModel.deleteDealAt(indexPath.item, groupIndex: collectionView.carouselIndex)
                }
            }
            
            cell.setContents(item: viewModel,
                             groupIndex: collectionView.carouselIndex,
                             index: indexPath.item)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let type = CollectionType(rawValue: collectionView.tag) ?? .interested
        
        switch type {
        case .interested:
            return CGSize(width: collectionView.frame.height * 0.86, height: collectionView.frame.height * 0.92)
        case .deal:
            let collectionView = collectionView as! DealsCollectionView
            if viewModel.dealsListCountAt(collectionView.carouselIndex) == 0 {
                return CGSize(width: screenWidth - 40, height: collectionView.bounds.height)
            }
            
            if !viewModel.hasDealID(indexPath.item, groupIndex: collectionView.carouselIndex) {
                return CGSize(width: collectionView.frame.height * 0.75, height: collectionView.frame.height * 0.8)
            }
            
            return CGSize(width: 273, height: collectionView.frame.height * 0.97)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = CollectionType(rawValue: collectionView.tag) ?? .interested
        
        switch type {
        case .interested:
            viewModel.appendCategory(indexPath.item)
        case .deal:
            guard let collectionView = collectionView as? DealsCollectionView else { return }
            let count = viewModel.dealsListCountAt(collectionView.carouselIndex)
            
            guard count > 0 else { return }
            
            if !viewModel.hasDealID(indexPath.item, groupIndex: collectionView.carouselIndex) {
                let upgradeVC = NewUpgradeViewController()
                
                if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
                    upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
                }
                upgradeVC.backToDealDetail = true
                show(upgradeVC, sender: nil)
                return
            }
            
            if viewModel.carouselDealTypeAt(collectionView.carouselIndex) == .loyalty {
                self.cardOnScreen = CardOnScreenView().loadView()
                self.cardOnScreen?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                
                Utils.showAlertWithCustomView(self.cardOnScreen!, bgColor: UIColor("rgba 47 72 88 0.8"))
                
                let tData = [
                    "cardData": viewModel.carouselList[safe: collectionView.carouselIndex]?.deals?[safe: indexPath.item]?.dictionary ?? [:],
                    "cardPoint": self.cardOnScreen?.frame.center
                    ] as [String : Any?]
                
                self.cardOnScreen?.config(tData,  onClickedCloseCallback: { _ in
                    
                    Utils.closeCustomAlert()
                }, onClickedChatCallback: { data in
                    Utils.closeCustomAlert()
                    
                    if let tData = data, let summary = tData["VendorSummary"] as? [String: Any?] {
                        self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
                    }
                }, onClickedCardCallback: { [weak self] _ in
                    guard let self = self else { return }
                    
                    if let content = self.cardOnScreen {
                        content.onClickedCloseCallback?(content.data)
                    }
                    self.showVc(with: Storyboard.wallet, classType: DealDetailViewController.self, viewController: { [unowned self] vc in
                        do {
                            let vm = try self.viewModel.generateDealDetailVM(indexPath.item, groupIndex: collectionView.carouselIndex)
                            vc.viewModel = vm
                        } catch let e {
                            print(e)
                        }
                    })
                })
                return
            }
            showVc(with: Storyboard.wallet, classType: DealDetailViewController.self, viewController: { [unowned self] vc in
                do {
                    let vm = try viewModel.generateDealDetailVM(indexPath.item, groupIndex: collectionView.carouselIndex)
                    vc.viewModel = vm
                } catch let e {
                    print(e)
                }
            })
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        let type = CollectionType(rawValue: collectionView.tag) ?? .interested
//
//        switch type {
//        case .interested:
//            viewModel.appendCategory(indexPath.item)
//            collectionView.reloadData()
//        case .deal:
//            break
//        }
//    }
}
