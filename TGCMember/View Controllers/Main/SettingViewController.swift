//
//  SettingViewController.swift
//  TGCMember
//
//  Created by jonas on 8/26/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {
    @IBOutlet weak var headerheight: NSLayoutConstraint!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var aboutInteractiveView: InteractiveView!
    @IBOutlet weak var languageInteractiveView: InteractiveView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var chosenLanguageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerheight.constant = Device.iPhoneWithNotch ? 120 : 100
        chosenLanguageLabel.text = CacheManager.shared.currentLanguage.stringValue
        
        aboutInteractiveView.onTap = { [unowned self] _ in
            let vc = Storyboard.main.instantiateViewController(withIdentifier: AboutViewController.className())
            navigationController?.pushViewController(vc, animated: true)
        }
        
        languageInteractiveView.onTap = { [unowned self] _ in
            let listSearchDialogView = ListSearchDialogView.loadView()
            let languages = LANGUAGE.allCases
            listSearchDialogView.config(title: "Select Language",
                                        list: languages.map { $0.stringValue },
                                        selectedStrings: GLOBAL.GlobalVariables.currentLanguage.stringValue,
                                        isSearchable: false)
            
            listSearchDialogView.onSelect = { strings in
                let joined = strings.joined(separator: ",")
                chosenLanguageLabel.text = joined
                
                if let rawValue = languages.first(where: { $0.stringValue == joined })?.rawValue,
                   let lang = LANGUAGE(rawValue: rawValue) {
                    
                    Utils.setLanguage(lang)
                    
                    view.makeToast("Language updated.")
                }
            }
            
            Utils.showAlertWithCustomView(listSearchDialogView)
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        dismissWithPopAnim()
    }
}

extension SettingViewController {
    func updateLanguages() {
        
    }
}
