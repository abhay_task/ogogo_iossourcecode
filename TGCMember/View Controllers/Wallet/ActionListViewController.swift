//
//  ActionListViewController.swift
//  TGCMember
//
//  Created by vang on 5/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum Const {
    static let closeCellHeight: CGFloat = 115
    static let openCellHeight: CGFloat = 600
}

class ActionListViewController: BaseViewController {
    
    var cellHeights: [CGFloat] = []
    
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableActions: UITableView!
    
    private var actionsList:[[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(onScrollToTodoItem(_:)), name: .REFRESH_THEN_SCROLL_TO_ACTION_AND_EXPAND, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshListTodo(_:)), name: .REFRESH_TEO_IN_LIST, object: nil)
        
        self.tableActions.register(UINib(nibName: "WelcomeDealCell", bundle: nil), forCellReuseIdentifier: "WelcomeDealCell")
        self.tableActions.register(UINib(nibName: "InviteFriendsCell", bundle: nil), forCellReuseIdentifier: "InviteFriendsCell")
        self.tableActions.register(UINib(nibName: "TEOCell", bundle: nil), forCellReuseIdentifier: "TEOCell")
        
        getActionsList()
        
        self.lblVersion.text = "Version \(Utils.getVersionApp()) | Server: \(BASE_URL)"
        
    }
    
    
    @objc func onScrollToTodoItem(_ notification: Notification) {
        
        self.getActionsList(completed: {
            if let _ = GLOBAL.GlobalVariables.todoWaitingId {
                self.handleScroll()
            }
        })
    }
    
    @objc func refreshListTodo(_ notification: Notification) {
        self.getActionsList()
    }
    
    private func handleScroll() {
        if let todoId = GLOBAL.GlobalVariables.todoWaitingId, self.actionsList.count > 0 {
            let indexCell = self.actionsList.firstIndex{ $0["ID"] as? String == todoId }
            
            if let rowIndex = indexCell {
                self.tableActions.scrollToRow(at: IndexPath(row: rowIndex, section: 0), at: .top, animated: true)
                
                setTimeout({
                    self.handleExpandCell(IndexPath(row: rowIndex, section: 0), forceOpen: true)
                }, 200)
            }
        }
        
        GLOBAL.GlobalVariables.todoWaitingId = nil
    }
    
    
    private func configCell() {
        cellHeights = Array(repeating: Const.closeCellHeight, count: self.actionsList.count)
        tableActions.estimatedRowHeight = Const.closeCellHeight
        tableActions.rowHeight = UITableView.automaticDimension
    }
  
    
    private func getActionsList(completed _completed: FinishedCallback? = nil) {
        Utils.showLoading()
    
        self.actionsList = []
        
        APICommonServices.getToDoList { (resp) in
            //mPrint("getToDoList", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                
                if status {
                    if let data = resp["data"] as? [String: Any?], let list = data["list"] as? [[String : Any?]] {
                        self.actionsList = list
                    }
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            self.configCell()
            
            self.tableActions.reloadData(completion: {
                _completed?()
            })
            
            Utils.dismissLoading()
        }
    
    }
    
    private func handleOnClickedCardTEO(_ data: [String: Any?]?) {
        if let tData = data, let status = tData["Status"] as? String, let dealData = tData["DealTellEveryone"] as? [String: Any?] {
            //mPrint("tData", tData)
            if status == TEO_STATUS.NEW.rawValue {
                let vc = InviteFriendsViewController()
                vc.data = dealData
                vc.callback = { [weak self] (success) in
                    guard let self = self else {return}
                    
                    self.getActionsList()
                }
                
                if let mainTabVC = self.tabBarController {
                    mainTabVC.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                // DO NOTHING
            }
            
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .REFRESH_THEN_SCROLL_TO_ACTION_AND_EXPAND, object: nil)
        NotificationCenter.default.removeObserver(self, name: .REFRESH_TEO_IN_LIST, object: nil)
    }
}


extension ActionListViewController: UITableViewDelegate, UITableViewDataSource {
    private func handleExpandCell(_ indexPath: IndexPath, forceOpen: Bool = false) {
        let rowData = actionsList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            let cell = self.tableActions.cellForRow(at: indexPath) as! TEOCell
            
            
            var duration = 0.0
            let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight
            
            if forceOpen {
                if cellIsCollapsed {
                    cellHeights[indexPath.row] = Const.openCellHeight
                    cell.expand(true, animated: true, completion: nil)
                    duration = 0.3
   
                    UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                        self.tableActions.beginUpdates()
                        self.tableActions.endUpdates()
                        
                        // fix https://github.com/Ramotion/folding-cell/issues/169
                        if cell.frame.maxY > self.tableActions.frame.maxY {
                            self.tableActions.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
                        }
                    }, completion: nil)
                }
                
            } else {
                if cellIsCollapsed {
                    cellHeights[indexPath.row] = Const.openCellHeight
                    cell.expand(true, animated: true, completion: nil)
                    duration = 0.3
                } else {
                    cellHeights[indexPath.row] = Const.closeCellHeight
                    cell.expand(false, animated: true, completion: nil)
                    duration = 0.3
                }
                
                UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                    self.tableActions.beginUpdates()
                    self.tableActions.endUpdates()
                    
                    // fix https://github.com/Ramotion/folding-cell/issues/169
                    if cell.frame.maxY > self.tableActions.frame.maxY {
                        self.tableActions.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
                    }
                }, completion: nil)
            }
        }
    }
    
    private func didSelectRowAt(_ indexPath: IndexPath, _ data: [String: Any?]?) {
        self.handleExpandCell(indexPath)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowData = actionsList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TEOCell") as? TEOCell
            
            cell?.config(rowData, onClickedCellCallback: {[weak self] (info) in
                guard let self = self else {return}
                
                self.didSelectRowAt(indexPath, info)
                
                }, onClickedCardCallback: {[weak self] (info) in
                    guard let self = self else {return}
                    
                    self.handleOnClickedCardTEO(rowData)
                    
                })
            
            cell?.layoutIfNeeded()
            
            return cell!
        }
        
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowData = actionsList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            return cellHeights[indexPath.row]
        }
        
        return 460.0
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowData = actionsList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            guard case let cell as TEOCell = cell else {
                return
            }
            
            cell.backgroundColor = .clear
            
            if cellHeights[indexPath.row] == Const.closeCellHeight {
                cell.expand(false, animated: false, completion: nil)
            } else {
                cell.expand(true, animated: false, completion: nil)
            }
        }
        
        
    }

    
}

extension ActionListViewController {
    func updateLanguages() {
        self.lblTitle.text = LOCALIZED.things_to_do.translate
    }
}
