//
//  DealDetailViewController.swift
//  TGCMember
//
//  Created by jonas on 1/18/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit
import GoogleMaps
import WebKit

class DealDetailViewController: BaseViewController, UITextViewDelegate {
    var dealInfo: [String: Any?]?
    var callback: FinishedCallback?
    // Outlets
    @IBOutlet weak var favoriteButton: CustomButton!
    @IBOutlet weak var shareButton: CustomButton!
    @IBOutlet weak var headerBannerImageView: UIImageView!
    @IBOutlet weak var statusButton: GradientButton!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var useButton: CustomButton!
    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var dealNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var vendorLogoImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dateUsedButton: CustomButton!
    @IBOutlet weak var blueBannerTextView: UITextView!
    @IBOutlet weak var untilLabel: UILabel!
    @IBOutlet weak var deleteButton: CustomButton!
    
    var viewModel: DealDetailViewModel!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getDealDetails()
        setupData()
        configMapStyle()
        
        scrollView.addPullToRefresh { [unowned self] in
            viewModel.getDealDetails()
        }
        
        vendorNameLabel.textColor = Color.darkBlue
        dealNameLabel.textColor = Color.darkBlue
        useButton.setTitleColor(Color.darkBlue, for: .normal)
        blueBannerTextView.superview?.backgroundColor = Color.skyBlue
        
        distanceLabel.textColor = Color.green
        
        categoryNameLabel.textColor = Color.gray
        locationLabel.textColor = Color.gray
        websiteLabel.textColor = Color.gray
        contactNumberLabel.textColor = Color.gray
        timeLabel.textColor = Color.gray
        scheduleLabel.textColor = Color.gray
        detailsLabel.textColor = Color.gray
        availabilityLabel.textColor = Color.gray
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(notification:)), name: .REFRESH_MENU_VIEW, object: nil)
    }
    
    override func setupViewModelCallbacks() {
        
        self.viewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
        }
        
        viewModel.onComplete = { [weak self] in
            guard let self = self else { return }
            
            self.dismissLoadingIndicator()
            self.setupData()
        }
        
        viewModel.onDeleteComplete = { [weak self] in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.dismissLoadingIndicator()
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: .RELOAD_WALLET, object: nil)
            }
        }
        
        viewModel.onErrorWithMessage = { [weak self] message in
            guard let self = self else { return }
            
            print(message)
            self.dismissLoadingIndicator()
            self.setupData()
        }
    }
    
    func setupData() {
        guard let deal = viewModel.deal else { return }
        
        deleteButton.isHidden = (deal.expiryTime?.isInThePast == true || deal.dateUsed != nil)
        statusButton.applyGradient(deal.type)
        headerBannerImageView.setKfImage(viewModel.deal.photo ?? "")
        vendorNameLabel.text = deal.vendorSummary?.name?.uppercased()
        dealNameLabel.text = "\(deal.titleLine1 ?? "")\n\(deal.titleLine2 ?? "")"
        vendorLogoImageView.setKfImage(deal.vendorSummary?.logoURL ?? "", contentMode: .scaleAspectFit)
        statusButton.isHidden = deal.promotionText.isEmpty
        statusButton.setTitle(deal.promotionText.uppercased(), for: .normal)
        statusButton.setTitleColor(deal.isWhiteFont ? .white : Color.darkBlue, for: .normal)
//        statusButton.setBackgroundImage(viewModel.deal.type?.bgImageValue ?? UIImage(), for: .normal)
        categoryNameLabel.text = deal.vendorSummary?.category?.name
        categoryIconImageView.setKfImage(deal.vendorSummary?.category?.imageURLStyle3 ?? "", contentMode: .scaleAspectFit)
        
        locationLabel.text = deal.vendorSummary?.fullAddress
        locationLabel.superview?.isHidden = deal.vendorSummary?.fullAddress?.isEmpty != false
        
        contactNumberLabel.text = "\(deal.vendorSummary?.phoneCode ?? "") \(deal.vendorSummary?.phone ?? "")"
        let timeSched = "\(deal.vendorSummary?.availableTime?.last?.startAt?.toString(format: "hh:mm aa", forTime: true) ?? "") - \(deal.vendorSummary?.availableTime?.last?.endAt?.toString(format: "hh:mm aa", forTime: true) ?? "")"
        timeLabel.text = timeSched
        timeLabel.superview?.isHidden = timeSched.count <= 3
        
        websiteLabel.superview?.isHidden = deal.vendorSummary?.siteURL?.isEmpty == true
        websiteLabel?.text = deal.vendorSummary?.siteURL
        
        let dealEndDateString = deal.expiryTime == nil ? "" : "Until \(deal.expiryTime?.toString(format: "eee, MMM dd, yyyy") ?? "")"
        scheduleLabel.text = deal.txtDayOfWeek
        scheduleLabel.superview?.isHidden = deal.txtDayOfWeek?.isEmpty != false
        
        untilLabel.text = dealEndDateString
        untilLabel.superview?.isHidden = dealEndDateString.isEmpty
        
        availabilityLabel.text = deal.shortDescription
        availabilityLabel.isHidden = deal.shortDescription?.isEmpty != false
        
        detailsLabel.text = deal.longDescription
        detailsLabel.isHidden = deal.longDescription?.isEmpty != false
        
        favoriteButton.setImage(deal.isFavorite == true ? #imageLiteral(resourceName: "favorite_active") : #imageLiteral(resourceName: "favorite_button") , for: .normal)
        useButton.isHidden = !deal.usable
        statusButton.titleLabel?.adjustsFontSizeToFitWidth = true
        dateUsedButton.isHidden = true
        dateUsedButton.isHidden = deal.dateUsed == nil
        dateUsedButton.setTitle(LOCALIZED.used_on_format.translate.replacingOccurrences(of: tagDateString, with: deal.dateUsed?.toString(format: "dd/MM") ?? ""), for: .normal)
        
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            if _upgradeModel.canUpNextStep {
               setupBlueBannerAttributes()
            }
            if _upgradeModel.canUpToNextStep == .GOLD || _upgradeModel.canUpToNextStep == .FREE {
                blueBannerTextView.superview?.isHidden = true
                useButton.isHidden = !deal.usable
            } else if _upgradeModel.canUpToNextStep == .SILVER {
                if deal.type == .silver || deal.type == .gold {
                    blueBannerTextView.superview?.isHidden = false
                    useButton.isHidden = true
                } else {
                    blueBannerTextView.superview?.isHidden = true
                    useButton.isHidden = !deal.usable
                }
            } else {
                blueBannerTextView.superview?.isHidden = false
                useButton.isHidden = true
            }
        } else {
            blueBannerTextView.superview?.isHidden = true
        }
        
        showPositonDeal()
    }
    
    func setupBlueBannerAttributes(isFreeMember: Bool = false) {
        let currentPlan = GLOBAL.GlobalVariables.userInfo?["CurrentPlan"] as? String ?? ""
        
        var fullText = LOCALIZED.blue_banner_message.translate.replacingOccurrences(of: "$(currentPlan)", with: currentPlan)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        if !isFreeMember {
            fullText = LOCALIZED.blue_banner_message_2.translate.replacingOccurrences(of: "$(currentPlan)", with: currentPlan)
        }
        
        let mutableString = NSMutableAttributedString(string: fullText,
                                                      attributes: [NSAttributedString.Key.foregroundColor: Color.customBackground, NSAttributedString.Key.font: UIFont(name: Font.RobotoRegular, size: 13.5)!, NSAttributedString.Key.paragraphStyle: paragraph])
        if isFreeMember {
            mutableString.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                         NSAttributedString.Key.link: "www.clickhere.com",
                                         NSAttributedString.Key.foregroundColor: Color.customBackground],
                                        range: NSRange(location: fullText.distance(of: LOCALIZED.upgrade_to_use_deals.translate) ?? 0,
                                                       length: LOCALIZED.upgrade_to_use_deals.translate.count))
            blueBannerTextView.linkTextAttributes = [NSAttributedString.Key.font: UIFont(name: Font.RobotoRegular, size: 13.5)!,
                                                         NSAttributedString.Key.foregroundColor: Color.customBackground]
        } else {
            mutableString.addAttributes([NSAttributedString.Key.link: "www.clickhere.com"], range: NSRange(location: 0, length: fullText.count))
            blueBannerTextView.linkTextAttributes = [NSAttributedString.Key.font: UIFont(name: Font.RobotoRegular, size: 13.5)!,
                                                         NSAttributedString.Key.foregroundColor: Color.customBackground]
        }
        
        blueBannerTextView.attributedText = mutableString
        blueBannerTextView.delegate = self
        blueBannerTextView.isUserInteractionEnabled = true
        blueBannerTextView.isSelectable = true
        
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        handleUpgrade()
        return true
    }
    
    @objc func refresh(notification: NSNotification) {
        setupData()
    }
    
    private func handleUpgrade() {
        let upgradeVC = NewUpgradeViewController()
        upgradeVC.backToDealDetail = true
        
        if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
            upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
        }
        
        navigationController?.pushViewController(upgradeVC, animated: true)
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapFavoriteButton(_ sender: Any) {
        viewModel.toggleFavorite()
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        let confirmAlert = DeleteConfirmAlertView()
        Utils.showAlertWithCustomView(confirmAlert)
        
        confirmAlert.onTapDelete = { [unowned self] in
            viewModel.deleteDeal()
        }
    }
    
    @IBAction func didTapShareButton(_ sender: Any) {
        let image = OptionalImageActivityItemSource(image: headerBannerImageView.image ?? #imageLiteral(resourceName: "logo_menu"))
        let vendorName = viewModel.deal.vendorSummary?.name ?? ""
        let dealName = viewModel.deal.name ?? ""
        let appstoreURL = "https://apps.apple.com/us/app/ogogo/id1481165357"
        let playStoreURL = "https://play.google.com/store/apps/details?id=com.ogogo.member"
        
        let text = "Join OGOGO to see a great deal and look for \(vendorName)\n\"\(dealName)\"\n\n\nAndroid: \(playStoreURL)\n\niOS: \(appstoreURL)"
        
        let objectsToShare: [Any] = [image, text]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.postToFacebook]
        
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func didTapChatNowButton(_ sender: Any) {
        self.openChatDetail(["PartnerID":  viewModel.deal.vendorSummary?.vendorID ?? ""])
    }
    
    @IBAction func didTapExpandMapButton(_ sender: Any) {
        let fullMapVC = FullMapViewController()
        fullMapVC.dealInfo = self.dealInfo
        fullMapVC.viewModel = viewModel
        self.addSubviewAsPresent(fullMapVC)
    }
    
    @IBAction func didTapUseButton(_ sender: Any) {
        Utils.checkLogin(self) {  [weak self]  (status) in
            guard let self = self else { return }
            
            if status == LOGIN_STATUS.SUCCESS_IMMEDIATELY {
                if !self.viewModel.isDealHasClaimCode() {
                    Utils.doUseDeal(self.viewModel.deal.dictionary) { success, data in
                        
                        Utils.showResultUseDealAlert(success, data as? [String: Any], completion: { status, data in
                            if status {
                                NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                                NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                                self.viewModel.getDealDetails()
                            }
                        })
                    }
                    return
                }
                
                Utils.showClaimAlert(self.viewModel.deal.dictionary, bgColor: UIColor(0, 0, 0, 0.2)) {  [weak self] (success, data) in
                    guard let self = self else {return}
                    
                    if success {
                        Utils.closeCustomAlert()
                        
                        NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                        NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                        
                        self.viewModel.getDealDetails()
                    }
                }
            }
        }
    }
    
    static func doUseDeal(_ cardData: [String: Any?]?, successCallback: SuccessedDataCallback? = nil) {
        Utils.showLoading()
        
        var tParams = [String: Any]()
        tParams["code"] = ""
        if let tData = cardData {
            let instanceDealID = tData["InstanceDealID"] as? String ?? ""
            tParams["instance_deal_id"] = instanceDealID
        }
        
        APICommonServices.doUseDeal(tParams) { (resp) in
            mPrint("claimADeal Not need enter CODE --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool {
                var tDict = [String: Any?]()
                if let data = resp["data"] as? [String: Any?] {
                    NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                    tDict = data
                }
                
                successCallback?(status, cardData)
            }
            
            Utils.dismissLoading()
        }
    }
}

// Language Protocol
extension DealDetailViewController {
    func updateLanguages() {
        
    }
}

extension DealDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -60.0 {
            
            for subview in scrollView.subviews {
                if subview is UIRefreshControl {
                    let rf = subview as! UIRefreshControl
                
                    rf.beginRefreshing()
                    rf.sendActions(for: .valueChanged)
                    
                    break
                }
            }
        }
    }
}

// MAP CONFIG
extension DealDetailViewController {
    private func configMapStyle() {
        mapView.isUserInteractionEnabled = false
        APICommonServices.fetchContentFromJSON(Constants.GOOGLE_MAP_STYLE_URL) { (resp) in
            if let tResp = resp, let tData = tResp["data"] as? [[String: Any?]] {
                
                let jsonData = try! JSONSerialization.data(withJSONObject: tData, options: .prettyPrinted)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    // print("fetchContentFromJSON STRING --> ", jsonString)
                    
                    setTimeout({
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(jsonString: jsonString)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }, 0)
                } else {
                    if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }
                }
            }
        }
    }
    
    private func showPositonDeal() {
        if let location = viewModel.deal.vendorSummary?.location {
            let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: location.lat ?? 0, endLong: location.lng ?? 0)
            distanceLabel.text = "\(String(format: "%.1f", tDistance/1000)) km"
            
            
            let position = CLLocationCoordinate2D(latitude: location.lat ?? 0, longitude: location.lng ?? 0)
            
            if let dealType = viewModel.deal.type {
                let marker = GMSMarker()
                marker.position = position
                
                let iconImageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 45)))
                iconImageView.image = dealType.iconValue
                marker.iconView = iconImageView
                
                marker.zIndex = 9999
                marker.groundAnchor = CGPoint(x: 0.5, y: 1)
                
                marker.map = mapView
                
                self.animateToPoint(position)
            }
        }
    }
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 17.0)
        mapView.animate(with: camera)
    }
}

class OptionalImageActivityItemSource: NSObject, UIActivityItemSource {
    let image: UIImage
    weak var viewController: UIViewController?
    
    init(image: UIImage) {
        self.image = image
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return image
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        if activityType?.rawValue == "net.whatsapp.WhatsApp.ShareExtension" || activityType?.rawValue == "com.facebook.Messenger.ShareExtension" {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                guard let presentedViewController = activityViewController.presentedViewController else { return }
                let appName = String(activityType?.rawValue.split(separator: ".")[safe: 2] ?? "")
                let alert = UIAlertController(title: "\(appName + (appName.isEmpty ? "" : " "))Doesn't Support Text + Image",
                                              message: "Unfortunately this app doesn’t support sharing both text and an image at the same time. As a result, only the text will be shared.",
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK",
                                              style: .cancel, handler: nil))
                presentedViewController.present(alert, animated: true, completion: nil)
            }
            return nil
        } else {
            return image
        }
    }
}
