//
//  SearchResultViewController.swift
//  TGCMember
//
//  Created by jonas on 1/21/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class SearchResultViewController: BaseViewController {
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let nib = UINib(nibName: NoRecordFoundCell.className(), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: NoRecordFoundCell.className())
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    
    var viewModel: SearchResultViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTitleLabel.text = "Search (\(viewModel.totalSearchResultCount))"
    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        
        viewModel.onComplete = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
        
        viewModel.onErrorWithMessage = { message in
            print(message)
        }
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension SearchResultViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfDeals == 0 ? 1 : viewModel.numberOfDeals
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.numberOfDeals == 0 {
            let cell = NoRecordFoundCell.dequeue(tableView, indexpath: indexPath)
            return cell
        }
        
        let cell = SearchResultCell.dequeue(tableView, indexpath: indexPath)
        cell.setContents(viewModel, index: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.numberOfDeals == 0 { return }
        showVc(with: Storyboard.wallet, classType: DealDetailViewController.self, viewController: { [unowned self] vc in
            do {
                vc.viewModel = try viewModel.generateDealDetailVM(indexPath.row)
            } catch let e {
                print(e)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !viewModel.isPaginating, viewModel.isLastDeal(with: indexPath.row), viewModel.numberOfDeals > 0 {
            viewModel.isPaginating = true
            viewModel.paginateDeals()
            
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
        } else {
            tableView.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.numberOfDeals == 0 {
            return screenHeight * 0.5
        }
        
        return UITableView.automaticDimension
    }
}

extension SearchResultViewController {
    func updateLanguages() {
        
    }
}
