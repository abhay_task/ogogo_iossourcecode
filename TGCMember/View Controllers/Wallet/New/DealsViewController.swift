//
//  DealsViewController.swift
//  TGCMember
//
//  Created by jonas on 1/22/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

private enum TableViewType: Int {
    case active = 0
    case used
    case expired
    
    var scrollXPosition: CGFloat {
        switch self {
        case .active: return 0.0
        case .used: return screenWidth
        case .expired: return screenWidth * 2
        }
    }
}

class DealsViewController: BaseViewController {
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet var selectedIndicatorViews: [UIView]!
    @IBOutlet weak var activeTableView: UITableView!
    @IBOutlet weak var usedTableView: UITableView!
    @IBOutlet weak var expiredTableView: UITableView!
    @IBOutlet var selectedButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    fileprivate var currentTableViewType: TableViewType = .active {
        didSet {
            scrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: currentTableViewType.scrollXPosition,
                                                                  y: 0),
                                                  size: scrollView.bounds.size),
                                           animated: true)
        }
    }
    
    var tableView: UITableView {
        switch currentTableViewType {
        case .active: return activeTableView
        case .expired: return expiredTableView
        case .used: return usedTableView
        }
    }
    
    var viewModel: DealsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.currentActivity = .active
        navTitleLabel.text = viewModel.navTitleText()
        let nib = UINib(nibName: NoRecordFoundCell.className(), bundle: nil)
        [activeTableView, usedTableView, expiredTableView].forEach {
            $0?.register(nib, forCellReuseIdentifier: NoRecordFoundCell.className())
            $0?.contentInset.top = 30
        }
    }
    
    override func setupViewModelCallbacks() {
        super.setupViewModelCallbacks()
        
        viewModel.onStartLoading = { [unowned self] in
            showLoadingIndicator()
            tableView.reloadData()
        }
        
        viewModel.onComplete = { [weak self] in
            guard let self = self else { return }
            
            self.tableView.reloadData()
            self.dismissLoadingIndicator()
        }
        
        viewModel.onErrorWithMessage = { [weak self] message in
            guard let self = self else { return }
            print(message)
            self.dismissLoadingIndicator()
        }
    }
    
    @IBAction func didTapActivityTypeButton(_ sender: UIButton) {
        guard sender != selectedButton else { return }
        
        guard let title = sender.currentTitle?.lowercased(),
              let activityType = DealActivityType.initWith(string: title),
              let tableViewType = TableViewType(rawValue: activityType.tag) else { return }
        
        currentTableViewType = tableViewType
        selectedIndicatorViews.first(where: { selectedButton.tag == $0.tag })?.alpha = 0
        selectedIndicatorViews.first(where: { sender.tag == $0.tag })?.alpha = 1
        sender.titleLabel?.font = UIFont(name: Font.RobotoMedium, size: sender.titleLabel?.font.pointSize ?? 13)
        selectedButton.titleLabel?.font = UIFont(name: Font.RobotoRegular, size: sender.titleLabel?.font.pointSize ?? 13)
        
        selectedButton = sender
        viewModel.currentActivity = activityType
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension DealsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isLoading { return 0 }
        return viewModel.deals.count == 0 ? 1 : viewModel.deals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if viewModel.deals.count == 0 {
            let cell = NoRecordFoundCell.dequeue(tableView, indexpath: indexPath)
            return cell
        }
        let cell = DealTableCell.dequeue(tableView, indexpath: indexPath)
        cell.setContents(viewModel, index: indexPath.row)
        cell.deleteButton?.isHidden = viewModel.dealTypeAt(indexPath.row) == .basic || viewModel.dealTypeAt(indexPath.row) == .loyalty
        cell.onTapDelete = { [unowned self] in
            let confirmAlert = DeleteConfirmAlertView()
            Utils.showAlertWithCustomView(confirmAlert)
            
            confirmAlert.onTapDelete = { [unowned self] in
                viewModel.deleteDealAt(indexPath.row)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.deals.count > 0  {
            tableView.tableFooterView = nil
            if !viewModel.isPaginating, viewModel.isLastDeal(indexPath.row) {
                viewModel.isPaginating = true
                viewModel.getDeals()

                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 44)

                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
            }
        } else {
            tableView.tableFooterView = nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.deals.count == 0 { return }
        showVc(with: Storyboard.wallet, classType: DealDetailViewController.self, viewController: { [unowned self] vc in
            do {
                vc.viewModel = try viewModel.generateDealDetailVM(indexPath.row)
            } catch let e {
                print(e)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.deals.count == 0 {
            return screenHeight * 0.5
        }
        
        return UITableView.automaticDimension
    }
}

extension DealsViewController {
    func updateLanguages() {
        
    }
}
