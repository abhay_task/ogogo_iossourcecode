//
//  EventTripViewController.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class EventTripViewController: BaseTripDetailViewController {
    override func updateLanguages() {
        
    }
    
    var eventList: [[String: Any?]] = []
    
    @IBOutlet weak var tblEvent: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblEvent.register(UINib(nibName: "EventWideCell", bundle: nil), forCellReuseIdentifier: "EventWideCell")
        
        decorate()
        
        renderView()
    }


    private func decorate() {
     
    }

    private func renderView() {
        if self.eventList.count == 0 {
            willDisplayEmptyView(true)
        } else {
            willDisplayEmptyView(false)
            
            self.tblEvent.reloadData()
        }
    }
 
}

extension EventTripViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.eventList.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.eventList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventWideCell") as? EventWideCell

        cell?.config(data, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}

            let eventDetailVC = EventDetailViewController()
            eventDetailVC.eventInfo = data
           
            self.navigationController?.pushViewController(eventDetailVC, animated: true)
            
        })

        return cell!
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Utils.getHeightWideEvent()
    }
    
}
