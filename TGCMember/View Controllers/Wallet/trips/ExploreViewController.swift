//
//  ExploreViewController.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class ExploreViewController: BaseViewController {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var vendorSummaryView: VendorSummaryViewController?
    var vendorInfoWindow : GMarker?
    private let selectedMarker = GMarker().loadSelectedMarker()
    private var vendorsList: [[String: Any]]?
    var data: [String: Any?]?
    var tappedMarker : GMSMarker?
    
    let heightSummary: CGFloat = 270
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        
        self.getListVendor()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }

    @IBAction func onClickedBack(_ sender: Any) {
        self.dismissFromSuperview(true) { (finished) in
            self.willMove(toParent: nil)
            self.removeFromParent()
            self.view.removeFromSuperview()
            
        }
    }
   
    func getListVendor() {
        Utils.showLoading()
        
        var tAddr = ""
        if let tData = self.data {
            tAddr = tData["CountryName"] as? String ?? ""
        }
        
//        let params = ["lat": GLOBAL.GlobalVariables.latestLat, "lng": GLOBAL.GlobalVariables.latestLng]
//
//        APICommonServices.getNearMeListVendor(params as [String : Any]) { (resp) in
//
//            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
//                if let data = resp["data"] as? [String: Any] , let list = data["list"]  as? [[String : Any]] {
//                    //mPrint("getNearMeListVendor", list)
//                    self.vendorsList = list
//                    self.displayVendorsOnMap()
//                }
//            }
//
//            Utils.dismissLoading()
//        }
        
        APICommonServices.getListVendorAtAddress(tAddr) { (resp) in
            mPrint("getListVendorAtAddress", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any] , let list = data["list"]  as? [[String : Any]] {

                    self.vendorsList = list
                    self.displayVendorsOnMap()
                }
            }

            Utils.dismissLoading()
        }
    }
    
    func mapVendorsList() -> Void {
        
        if let tapped = tappedMarker {
            if let data = tapped.userData as? [String: Any] {
              self.vendorsList = self.vendorsList!.map {
                    var vendor = $0
                
                    if vendor["ID"] as? String == data["ID"] as? String {
                         vendor["selected"] = true
                    } else {
                        vendor["selected"] = false
                    }
                
                    return vendor
                }
            }
        } else {
            self.vendorsList = self.vendorsList!.map {
                var vendor = $0
                vendor["selected"] = false
                
                return vendor
            }
        }
        
    }
    
    func displayVendorsOnMap() {
        mapView.clear()
        
    
        self.mapVendorsList()
        
        let mPaths = GMSMutablePath()
        
        if let tVendors = self.vendorsList {
            for vendor in tVendors {
                var lat: Double?
                var lng: Double?
                
                if let location = vendor["Location"] as? [String: Any], let aDouble = location["lat"] as? Double {
                    lat = aDouble
                }
                
                if let location = vendor["Location"] as? [String: Any], let bDouble = location["lng"] as? Double {
                    lng = bDouble
                }
            
                let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
                
                let marker = GMSMarker()
                marker.position = position
                marker.userData = vendor
                marker.map = mapView
                
                mPaths.add(position)
                
                if (vendor["selected"] as? Bool == true) {
                    selectedMarker.thumbnail.image = nil
                    marker.icon = Utils.imageWithView(view: selectedMarker)
                    selectedMarker.thumbnail.sd_setImage(with:  URL(string: vendor["LogoUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                        guard let self = self else {return}
                        marker.iconView = nil
                        marker.icon = Utils.imageWithView(view: self.selectedMarker)
                    })
                    
                    marker.zIndex = 9999
                    marker.groundAnchor = CGPoint(x: 0.5, y: 210/311)
                } else {
                    marker.zIndex = 8888
                    marker.groundAnchor = CGPoint(x: 0.5, y: 1)
                    marker.icon = UIImage(named: "marker_ic")
                }
                
            }
            
        }
        
        self.mapView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds.init(path: mPaths)))

    }
    
    @objc private func swiped(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .down:
                //print("Swiped Down")
                self.dismissVendorSummary()
            default:
                break
            }
        }
    }
    
    func showVendorSummary(_ data: Any?) -> Void {

            if (self.vendorSummaryView == nil) {
                self.vendorSummaryView = VendorSummaryViewController()

                let swipeDown = UISwipeGestureRecognizer(target: self, action:#selector(swiped(_:)))
                swipeDown.direction = .down
                self.vendorSummaryView?.view.addGestureRecognizer(swipeDown)
                self.vendorSummaryView?.onClickedAnchorCallback = { [weak self] (data) in
                                                                    guard let self = self else {return}

                                                                    self.dismissVendorSummary()
                                                                  }

                self.vendorSummaryView?.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: heightSummary)
                self.view.addSubview(self.vendorSummaryView!.view)
                self.addChild(self.vendorSummaryView!)


                self.vendorSummaryView?.view.snp.makeConstraints({ (make) in
                    make.left.equalTo(self.view)
                    make.right.equalTo(self.view)
                    make.bottom.equalTo(self.view)
                    make.height.equalTo(heightSummary)
                })

                UIView.performWithoutAnimation {
                    self.vendorSummaryView?.view.layoutIfNeeded()
                }


                UIView.animate(withDuration: 0.3,
                               animations: { [weak self] in
                                guard let self = self else {return}
                                self.view.layoutIfNeeded()
                               },
                               completion: nil)

            }

            self.vendorSummaryView?.updateData(data as! [String : Any?])
        }
        
        func dismissVendorSummary() {
            vendorInfoWindow?.removeFromSuperview()
            
            if (tappedMarker != nil) {
                tappedMarker = nil
                self.displayVendorsOnMap()
            }
            
            if (self.vendorSummaryView != nil) {
                
                self.vendorSummaryView?.view.snp.updateConstraints({ (make) in
                    make.bottom.equalTo(self.view).offset(heightSummary + 50)
                })
                
                UIView.animate(withDuration: 0.5,
                               animations: { [weak self] in
                                guard let self = self else {return}
                                self.view.layoutIfNeeded()
                               },
                               completion: { [weak self] finished in
                                    guard let self = self else {return}
                                
    //                                self.vendorSummaryView?.view.removeFromSuperview()
    //                                self.vendorSummaryView?.removeFromParent()
                                
                                    self.vendorSummaryView = nil
                               })
            }
        }

}

extension ExploreViewController: GMSMapViewDelegate, CLLocationManagerDelegate {
    
    public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return  nil
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       
        if let tTapMarker = tappedMarker, let userInfo = tTapMarker.userData as? [String: Any?] , let mInfo = marker.userData as? [String: Any?] {
            if userInfo["ID"] as? String == mInfo["ID"] as? String {
                return false
            }
        }
        
        
        tappedMarker = marker
        
        self.displayVendorsOnMap()

        let position = marker.position

        self.animateToPoint(position, false)
        
        self.showVendorSummary(marker.userData)
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.dismissVendorSummary()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let position = tappedMarker?.position
        
        if let tPosition = position {
            vendorInfoWindow?.center = mapView.projection.point(for: tPosition)
            vendorInfoWindow?.center.y -= 65
            vendorInfoWindow?.center.x += 40
        }
       
    }
    
    func animateToPoint(_ position: CLLocationCoordinate2D, _ useCamera: Bool = true) {
        if useCamera == true {
            let point = mapView.projection.point(for: position)
            let newPoint = mapView.projection.coordinate(for: point)
            let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 13.0)
            mapView.animate(with: camera)
        } else {
            mapView.animate(toLocation: position)
        }
        
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse ||   status == .authorizedAlways {
            // you're good to go!
            manager.startUpdatingLocation()
        } else if status == .denied {
            self.getListVendor()
            
        }
    }
    
    

   
}
