//
//  TripListViewController.swift
//  TGCMember
//
//  Created by vang on 5/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit


let HEIGHT_CREATE_TRIP_VIEW: CGFloat = 460 // 575
let TOP_CREATE_TRIP: CGFloat = SCREEN_HEIGHT <= 667 ? 5 : 15

enum TRIP_SECTION: Int {
    case PLANNED
    case PAST
    
    
    var translate : String {
        switch self {
        case .PLANNED : return LOCALIZED.txt_planned_title.translate
        case .PAST : return LOCALIZED.txt_past_title.translate
        }
    }
}

class TripListViewController: BaseViewController {
    
    func updateLanguages() {
        self.lblTitleScreen.text = LOCALIZED.txt_trips_title.translate
        self.btnNewTrip.setTitleUnderline(" \(LOCALIZED.txt_new_trip.translate)", color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 14))
        
        self.lblPrompt1.setAttribute(LOCALIZED.text_planning_your_trip.translate, color: UIColor(39, 39, 39), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 4)
        self.lblPrompt1.textAlignment = .center
        
        self.btnPlanFirstTrip.setTitle(LOCALIZED.plan_my_first_trip.translate, for: .normal)
    }
    
  
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblTrips: UITableView!
    @IBOutlet weak var lblTitleScreen: UILabel!
    @IBOutlet weak var btnNewTrip: UIButton!
    @IBOutlet weak var coverNewTrip: UIView!
    @IBOutlet weak var coverTableView: UIView!
    @IBOutlet weak var coverNoTrips: UIView!
    @IBOutlet weak var btnPlanFirstTrip: UIButton!
    @IBOutlet weak var lblPrompt2: UILabel!
    @IBOutlet weak var lblPrompt1: UILabel!
    @IBOutlet weak var coverCreateNoTrip: UIView!
    @IBOutlet weak var heightCoverCreateNoTripConstraint: NSLayoutConstraint!

    @IBOutlet weak var bottomTblConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverContentNoTrip: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    //    @IBOutlet weak var heightNoTripConstraint: NSLayoutConstraint!
    private var isCreatingTrip: Bool = false
    private var noTrip: Bool = true
    
    private var sectionsInfo: [String] = []
    private var plannedTrips:[[String: Any?]] = []
    private var pastTrips:[[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tblTrips.register(UINib(nibName: "CreateTripCell", bundle: nil), forCellReuseIdentifier: "CreateTripCell")
        self.tblTrips.register(UINib(nibName: "PlannedTripCell", bundle: nil), forCellReuseIdentifier: "PlannedTripCell")
        self.tblTrips.register(UINib(nibName: "PastTripCell", bundle: nil), forCellReuseIdentifier: "PastTripCell")
        
        self.decorate()
        
        self.getTripsList {
            self.handleAfterFetch()
        }
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            GLOBAL.GlobalVariables.heightKeyboard = keyboardSize.height
            
            let tBottom = keyboardSize.height + 10
          

            if self.noTrip {
                self.bottomScrollConstraint.constant = tBottom
            } else {
                self.bottomTblConstraint.constant = tBottom
            }
        }
    }
       
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomScrollConstraint.constant = 0
        self.bottomTblConstraint.constant = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        
    }
    
    @IBAction func onClickedNewTrip(_ sender: Any) {
        self.showCreateTripForm()
    }
    
    
    private func showCreateTripForm() {

        let createTripView = CreateTripView().loadView()
        createTripView.parent = self
        createTripView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 10)
        
        createTripView.config(nil, onClickedCancelCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            self.hideCreateTripForm()
            
        }, onClickedAddTripCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tParams = data {
                self.createNewTrip(tParams)
            }
            
        })
         
        if self.noTrip {
            createTripView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 10)
            self.coverCreateNoTrip.addSubview(createTripView)
            UIView.performWithoutAnimation {
                createTripView.layoutIfNeeded()
            }
            
            createTripView.snp.makeConstraints { (make) in
                       make.left.equalToSuperview()
                       make.top.equalToSuperview()
                       make.right.equalToSuperview()
                       make.bottom.equalToSuperview()
                   }
                   
           UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.heightCoverCreateNoTripConstraint.constant = HEIGHT_CREATE_TRIP_VIEW
                self.coverCreateNoTrip.alpha = 1
                self.coverContentNoTrip.alpha = 0
                
                self.view.layoutIfNeeded()
               
           }, completion: { (finished: Bool) in
                self.heightCoverCreateNoTripConstraint.constant = HEIGHT_CREATE_TRIP_VIEW
                self.coverContentNoTrip.alpha = 0
                self.coverCreateNoTrip.alpha = 1
               
                self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: HEIGHT_CREATE_TRIP_VIEW)
            
           })
            
           setTimeout({
                self.scrollView.setContentOffset(.zero, animated: true)
            }, 400)
        } else {
            self.willDisplayNewTrip(true)
        }
        
       
    }
    
    private func hideCreateTripForm() {
        if self.noTrip {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.heightCoverCreateNoTripConstraint.constant = TOP_CREATE_TRIP
                self.coverCreateNoTrip.alpha = 0
                self.coverContentNoTrip.alpha = 1
                
                self.view.layoutIfNeeded()
                
            }, completion: { (finished: Bool) in
                self.heightCoverCreateNoTripConstraint.constant = TOP_CREATE_TRIP
                self.coverContentNoTrip.alpha = 1
                self.coverCreateNoTrip.alpha = 0
                
                self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 470 + TOP_CREATE_TRIP)
                
                self.coverCreateNoTrip.removeSubviews()
            })
            
            setTimeout({
                self.scrollView.setContentOffset(.zero, animated: true)
             }, 400)
            
        } else {
           self.willDisplayNewTrip(false)
        }
    }
      
    
    private func willDisplayNewTrip(_ willDisplay: Bool = false) {
        self.coverNewTrip.alpha = willDisplay ? 0 : 1
        self.isCreatingTrip = willDisplay
        
        self.tblTrips.beginUpdates()
        self.tblTrips.endUpdates()
        
        setTimeout({
            self.tblTrips.setContentOffset(.zero, animated: true)
        }, 400)
    }

    private func decorate() {
       
        self.btnNewTrip.setTitleUnderline(" \(LOCALIZED.txt_new_trip.translate)", color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 14))
        self.heightCoverCreateNoTripConstraint.constant = TOP_CREATE_TRIP
        
        self.coverNewTrip.alpha = 0
        self.coverNoTrips.alpha = 0
        self.coverTableView.alpha = 0

        self.btnPlanFirstTrip.layer.cornerRadius = 5.0
        self.btnPlanFirstTrip.backgroundColor = UIColor(255, 193, 6, 1)
        self.btnPlanFirstTrip.layer.applySketchShadow()
    }
    
    private func handleAfterFetch() {
        self.coverNewTrip.alpha = self.noTrip ? 0 : 1
        self.coverNoTrips.alpha = self.noTrip ? 1 : 0
        self.coverTableView.alpha = self.noTrip ? 0 : 1
        
        setTimeout({
            self.tblTrips.setContentOffset(.zero, animated: true)
        }, 500)
    }
 
    private func gotoTripDetail(_ data: [String: Any?]?, fromPast: Bool = false) {
        if var tData = data {
            tData["fromPast"] = fromPast
            let tripDetailVC = TripDetailViewController()
            tripDetailVC.data = tData
            self.navigationController?.pushViewController(tripDetailVC, animated: true)
        }
    }
    
    private func createNewTrip(_ params: [String: Any?]) {
        mPrint("createNewTrip params --> ", params)
        Utils.showLoading()
        
        APICommonServices.createNewTrip(params as [String : Any]) { (resp) in
            mPrint("createNewTrip --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                    self.willDisplayNewTrip(false)
                    
                    self.getTripsList {
                        self.handleAfterFetch()
                    }
                } else {
                    Utils.dismissLoading()
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
           
        }
        

    }
  
    private func getTripsList(completed _completed: FinishedCallback? = nil) {
        Utils.showLoading()
    
        self.sectionsInfo = []
        self.plannedTrips = []
        self.pastTrips = []
        
        APICommonServices.getTripsList { (resp) in
            mPrint("getTripsList", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                
                if status {
                    if let data = resp["data"] as? [String: Any?] {
                        
                        if let listPlanned = data["planned"] as? [[String : Any?]] {
                            self.plannedTrips = listPlanned
                        }
                        
                        if let listPast = data["past"] as? [[String : Any?]] {
                            self.pastTrips = listPast
                        }
                    }
                } else {
                    //Utils.showAlert(self, "", resp["message"] as? String ?? "")   
                }
            } 
       
            self.sectionsInfo.append("")
            
            if self.plannedTrips.count > 0 {
                self.sectionsInfo.append(TRIP_SECTION.PLANNED.translate)
            }
            
            if self.pastTrips.count > 0 {
                self.sectionsInfo.append(TRIP_SECTION.PAST.translate)
            }
            
            self.noTrip = (self.plannedTrips.count == 0 && self.pastTrips.count == 0)
            
            self.tblTrips.reloadData(completion: {
                _completed?()
            })
            
            Utils.dismissLoading()
        }
    }

}

extension TripListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if sectionsInfo[section] == TRIP_SECTION.PLANNED.translate {
             return self.plannedTrips.count
        } else if sectionsInfo[section] == TRIP_SECTION.PAST.translate {
            return self.pastTrips.count
        }
        
        return 0
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let data = tripsList[indexPath.row]
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateTripCell") as? CreateTripCell
            cell?.parent = self
            cell?.subContent?.isHidden = !isCreatingTrip
            cell?.config(nil, onClickedAddTrip: { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data {
                     self.createNewTrip(tData)
                }
              
                
            }, onClickedCancel: { [weak self] (data) in
                guard let self = self else {return}
               
                
                self.willDisplayNewTrip(false)
            })
            
            return cell!
        } else {
            if sectionsInfo[indexPath.section] == TRIP_SECTION.PLANNED.translate {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlannedTripCell") as? PlannedTripCell
                
                var tData = self.plannedTrips[indexPath.row]
                tData["currentIndex"] = indexPath.row
                tData["numItems"] = self.plannedTrips.count
                
                cell?.config(tData, onClickedCellCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.gotoTripDetail(data)
                }, onClickedMoreCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                })
                
                return cell!
            } else if sectionsInfo[indexPath.section] == TRIP_SECTION.PAST.translate {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PastTripCell") as? PastTripCell
                var tData = self.pastTrips[indexPath.row]
                
                cell?.config(tData, onClickedCellCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.gotoTripDetail(data, fromPast: true)
                }, onClickedMoreCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                })
                
                return cell!
            }
        }
        
        
        return UITableViewCell()
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsInfo.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            let cell = tableView.cellForRow(at: indexPath) as? CreateTripCell
            
            if let tCell = cell {
                tCell.reset()
            }
            
            cell?.subContent?.isHidden = !isCreatingTrip
            
            if isCreatingTrip {
                return HEIGHT_CREATE_TRIP_VIEW
            }
            
            return 0
        } else {
            let widthItem = SCREEN_WIDTH - 50
            
            if sectionsInfo[indexPath.section] == TRIP_SECTION.PLANNED.translate {
                return widthItem * (123 / 327)
            } else if sectionsInfo[indexPath.section] == TRIP_SECTION.PAST.translate {
                return widthItem * (123 / 327) + 10
            }
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
            
        let header = UIView()
        header.backgroundColor = UIColor(hexFromString: "#F6F7FB")
        
        let lblTitle = UILabel()
        lblTitle.text = sectionsInfo[section]
        lblTitle.textColor = UIColor(228, 109, 87)
        lblTitle.font = UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 15)
        
        header.addSubview(lblTitle)
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(header).offset(25)
            make.right.equalTo(header).offset(25)
            make.bottom.equalTo(header).offset(-10)
        }
        
        
        return header

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (sectionsInfo[section] == TRIP_SECTION.PLANNED.translate || sectionsInfo[section] == TRIP_SECTION.PAST.translate) {
            return 60.0
        }

        return 0

    }
}
