//
//  DealTripViewController.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class DealTripViewController: BaseTripDetailViewController {
    override func updateLanguages() {
        
    }
    
    var dealList: [[String: Any?]] = []

    @IBOutlet weak var tblDeal: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblDeal.register(UINib(nibName: "DealWideCell", bundle: nil), forCellReuseIdentifier: "DealWideCell")
        
        decorate()
        
        renderView()
    }

    private func decorate() {
     
    }
    
    private func renderView() {
        if self.dealList.count == 0 {
            willDisplayEmptyView(true)
        } else {
            willDisplayEmptyView(false)
            
            self.tblDeal.reloadData()
        }
    }
    
}

extension DealTripViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.dealList.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.dealList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealWideCell") as? DealWideCell
       
        cell?.config(data, onClickedCell: { [weak self] (data) in
           guard let self = self else {return}
           
           if let tData = data {
               let dealDetailVC = DealDetailViewController()
//               dealDetailVC.dealInfo = tData
               self.navigationController?.pushViewController(dealDetailVC, animated: true)
           }}, onClickedMore: { [weak self] (data) in
               guard let self = self else {return}
               
           })
       
       return cell!
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return heightWideDeal - 10
         return SCREEN_WIDTH - 20
    }
    
}
