//
//  TripDetailViewController.swift
//  TGCMember
//
//  Created by vang on 5/15/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit


enum TAB_TRIP_DETAIL: Int {
    case NONE = -1
    case TO_DO = 0
    case EVENT = 1
    case DEAL = 2
    case DISCOVER = 3
    
    var key : String {
        switch self {
            case .NONE : return ""
            case .TO_DO : return "thingToDo"
            case .EVENT : return "event"
            case .DEAL : return "wallet"
            case .DISCOVER : return ""
        }
    }
}

let PADDING_TAB: CGFloat = 25
let WIDTH_TAB: CGFloat = 50
let HEIGHT_SPACE: CGFloat = 20

class TripDetailViewController: UIViewController {

    var data: [String: Any?]?
     
    @IBOutlet weak var heightLineConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightCoverEditConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverTabView: UIView!
    @IBOutlet weak var coverDetailView: UIView!
    @IBOutlet weak var heightEditConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverEditTrip: UIView!
    @IBOutlet weak var btnEditTrip: UIButton!
    @IBOutlet weak var discoverRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dealLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var eventLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgDiscover: UIView!
    @IBOutlet weak var bgDeal: UIView!
    @IBOutlet weak var bgEvent: UIView!
    @IBOutlet weak var bgAction: UIView!
    @IBOutlet weak var coverDiscover: UIView!
    @IBOutlet weak var coverDeal: UIView!
    @IBOutlet weak var coverEvent: UIView!
    @IBOutlet weak var coverAction: UIView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var lblTripName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    private var mTabBarController: UITabBarController = UITabBarController()
    private var tempTabIndex: Int = TAB_TRIP_DETAIL.TO_DO.rawValue
    
    private var tripInfo: [String: Any?]?
    private var todoList: [[String: Any?]] = []
    private var eventList: [[String: Any?]] = []
    private var dealList: [[String: Any?]] = []
    
    private var updateTripForm = CreateTripView().loadView()
    
    var isEdit: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.decorate()
        
        
        getTripDetail { (success) in
            if !success {
                self.resetData()
            }
            
            self.renderContent()
        }
        
        onActive(fromIndex: 0, toIndex: tempTabIndex)
    }
    
    private func resetData() {
        tripInfo = nil
        todoList = []
        eventList = []
        dealList = []
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func toggleEditView() {

        if self.isEdit {
            self.updateTripForm.parent = self
            self.updateTripForm.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 10)
            
            self.updateTripForm.config(self.tripInfo, onClickedCancelCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.btnEditTrip.isHidden = false
                self.isEdit = false
                self.toggleEditView()
                
            }, onClickedAddTripCallback: { [weak self] (data) in
                guard let self = self else {return}
                self.btnEditTrip.isHidden = false
                
                if let tParams = data {
                   // self.createNewTrip(tParams)
                }
                
            })
            
            
            self.coverEditTrip.addSubview(self.updateTripForm)
            UIView.performWithoutAnimation {
                self.updateTripForm.layoutIfNeeded()
            }
            
            self.updateTripForm.snp.makeConstraints { (make) in
                make.left.equalToSuperview()
                make.top.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalToSuperview()
            }
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.heightSpaceConstraint.constant = HEIGHT_SPACE
                self.heightLineConstraint.constant = 1
                
                self.heightEditConstraint.constant = HEIGHT_CREATE_TRIP_VIEW
                self.heightCoverEditConstraint.constant = SCREEN_HEIGHT - 185 - (Utils.isIPhoneNotch() ? 20 : 0) - HEIGHT_SPACE
                
                self.coverEditTrip.alpha = 1
                self.contentView.alpha = 0
                self.coverTabView.alpha = 0
                
                self.view.layoutIfNeeded()
                
            }, completion: { (finished: Bool) in
                self.heightSpaceConstraint.constant = HEIGHT_SPACE
                self.heightLineConstraint.constant = 1
                self.heightCoverEditConstraint.constant = SCREEN_HEIGHT - 185 - (Utils.isIPhoneNotch() ? 20 : 0)
                self.heightEditConstraint.constant = HEIGHT_CREATE_TRIP_VIEW
                
                self.coverEditTrip.alpha = 1
                self.contentView.alpha = 0
                self.coverTabView.alpha = 0
                
            })
            
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions() , animations: {
                self.heightLineConstraint.constant = 0
                self.heightSpaceConstraint.constant = 0
            
                self.heightCoverEditConstraint.constant = 0
                self.heightEditConstraint.constant = 0
                self.coverEditTrip.alpha = 0
                self.contentView.alpha = 1
                self.coverTabView.alpha = 1
                
                self.view.layoutIfNeeded()
                
            }, completion: { (finished: Bool) in
                self.heightLineConstraint.constant = 0
                self.heightSpaceConstraint.constant = 0
                self.heightCoverEditConstraint.constant = 0
                self.heightEditConstraint.constant = 0
                self.coverEditTrip.alpha = 0
                self.contentView.alpha = 1
                self.coverTabView.alpha = 1
                
                //self.coverCreateNoTrip.removeSubviews()
            })
        }
    }
    
    
   
    private func getTripDetail(_ completed: SuccessedCallback? = nil) {
        Utils.showLoading()
        
        var tripId = ""
        
        if let tData = self.data {
            tripId = tData["ID"] as? String ?? ""
        }
  
       
        Utils.getTripDetail(tripId, type: .NONE) { (success, data) in
            if let tData = data as? [String: Any?]  {
                self.tripInfo = tData["trip"] as? [String : Any?]
                self.todoList = tData[TAB_TRIP_DETAIL.TO_DO.key] as? [[String : Any?]] ?? []
                self.eventList = tData[TAB_TRIP_DETAIL.EVENT.key] as? [[String : Any?]] ?? []
                self.dealList = tData[TAB_TRIP_DETAIL.DEAL.key] as? [[String : Any?]] ?? []
            }

            completed?(success)

            Utils.dismissLoading()
        }
    
    }
    
    private func decorate() {
        self.btnEditTrip.setTitleUnderline("  \(LOCALIZED.txt_edit_trip.translate)", color: UIColor(hexFromString: "#E46D57"))
        //self.btnEditTrip.setTitle(LOCALIZED.txt_edit_trip.translate, for: .normal)
        if self.isEdit {
            self.heightSpaceConstraint.constant = HEIGHT_SPACE
            self.heightLineConstraint.constant = 1
           
            self.heightCoverEditConstraint.constant = SCREEN_HEIGHT - 185 - (Utils.isIPhoneNotch() ? 20 : 0) - HEIGHT_SPACE
        } else {
            self.heightCoverEditConstraint.constant = 0
            self.heightLineConstraint.constant = 0
            self.heightSpaceConstraint.constant = 0
        }
        
        self.heightContentConstraint.constant = SCREEN_HEIGHT - 185
        
        
        if let tData = data {
            let fromPast = tData["fromPast"] as? Bool ?? true
            
            self.btnEditTrip.isHidden = fromPast
            
            let strDate = tData["FromDateAt"] as? String ?? ""
            
            self.lblTripName.text = tData["Name"] as? String ?? ""
            self.lblDate.text = "\(tData["CountryName"] as? String ?? ""), \(Utils.stringFromStringDate(strDate, toFormat: "MMM dd, yyyy"))"
        }
        
        self.actionLeftConstraint.constant = PADDING_TAB
        
        let tLeftItem:CGFloat = (SCREEN_WIDTH - (PADDING_TAB * 2) - (WIDTH_TAB * 4)) / 3
        
        self.eventLeftConstraint.constant = tLeftItem
        self.dealLeftConstraint.constant = tLeftItem
        
        self.discoverRightConstraint.constant = PADDING_TAB
    }
    
    @IBAction func onClickedEditTrip(_ sender: Any) {
        Utils.showAvailableSoon()
//        self.btnEditTrip.isHidden = true
//        self.isEdit = true
//        self.toggleEditView()
    }
    
    @IBAction func onClickedDiscover(_ sender: Any) {
        if let app = GLOBAL.GlobalVariables.appDelegate, let vc = app.getTopViewController()  {
            let exploreVC = ExploreViewController()
            exploreVC.data = self.data
            
            vc.addSubviewAsPresent(exploreVC)
        }
    }
    
    @IBAction func onClickedDeal(_ sender: Any) {
         self.navigate(toIndex: TAB_TRIP_DETAIL.DEAL.rawValue)
    }
    
    @IBAction func onClickedEvent(_ sender: Any) {
         self.navigate(toIndex: TAB_TRIP_DETAIL.EVENT.rawValue)
    }
    
    @IBAction func onClickedTodo(_ sender: Any) {
         self.navigate(toIndex: TAB_TRIP_DETAIL.TO_DO.rawValue)
    }
    
    func navigate(toIndex: Int) -> Void {
        //  let controllerIndex = tabBarController.viewControllers?.firstIndex(of: viewController)

        if mTabBarController.selectedIndex == toIndex || toIndex == self.tempTabIndex {
            return;
        }
        
        self.onActive(fromIndex: self.tempTabIndex, toIndex: toIndex)
        
        self.tempTabIndex = toIndex
      
        
        // Get the views.
        let fromView = mTabBarController.selectedViewController?.view
        let toView = mTabBarController.viewControllers![toIndex].view
        
        // Get the size of the view area.
        let viewSize: CGRect = fromView!.frame
        let scrollRight: Bool = toIndex > mTabBarController.selectedIndex
        
        // Add the to view to the tab bar view.
        fromView!.superview?.addSubview(toView!)
        
        // Position it off screen.
        let screenWidth: CGFloat = UIScreen.main.bounds.width
        toView!.frame = CGRect(x: (scrollRight ? screenWidth : -screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            fromView!.frame = CGRect(x: (scrollRight ? -screenWidth : screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
            toView!.frame = CGRect(x: 0, y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        }, completion: {  [weak self] (finished) in
            guard let self = self else {return}
            
            if finished {
                // Remove the old view from the tabbar view.
                fromView!.removeFromSuperview()
                self.mTabBarController.selectedIndex = toIndex
            }
        })
        
    }
    
    private func onActive(fromIndex: Int, toIndex: Int) {

        coverAction.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0, x: 0, y: 0, blur: 0, spread: 0)
        coverEvent.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0, x: 0, y: 0, blur: 0, spread: 0)
        coverDeal.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0, x: 0, y: 0, blur: 0, spread: 0)
        coverDiscover.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0, x: 0, y: 0, blur: 0, spread: 0)
        
        if toIndex == TAB_TRIP_DETAIL.TO_DO.rawValue {
            coverAction.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.18, x: 0, y: -7, blur: 5, spread: 0)
        }
        
        if toIndex == TAB_TRIP_DETAIL.EVENT.rawValue {
            coverEvent.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.18, x: 0, y: -7, blur: 5, spread: 0)
        }
        
        if toIndex == TAB_TRIP_DETAIL.DEAL.rawValue {
            coverDeal.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.18, x: 0, y: -7, blur: 5, spread: 0)
        }
        
        if toIndex == TAB_TRIP_DETAIL.DISCOVER.rawValue {
            coverDiscover.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.18, x: 0, y: -7, blur: 5, spread: 0)
        }
        
        self.bgAction.backgroundColor = toIndex == TAB_TRIP_DETAIL.TO_DO.rawValue ? UIColor(255, 193, 6) : UIColor(47, 38, 12) // yellow -> brown
        self.bgEvent.backgroundColor = toIndex == TAB_TRIP_DETAIL.EVENT.rawValue ? UIColor(255, 193, 6) : UIColor(47, 38, 12) // yellow -> brown
        self.bgDeal.backgroundColor = toIndex == TAB_TRIP_DETAIL.DEAL.rawValue ? UIColor(255, 193, 6) : UIColor(47, 38, 12) // yellow -> brown
        self.bgDiscover.backgroundColor = toIndex == TAB_TRIP_DETAIL.DISCOVER.rawValue ? UIColor(255, 193, 6) : UIColor(47, 38, 12) // yellow -> brown
    }


    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    private func renderContent() {

        let tabTodo = TodoTripViewController()
        tabTodo.data = self.data
        tabTodo.todoList = self.todoList
        
        let tabEvent = EventTripViewController()
        tabEvent.data = self.data
        tabEvent.eventList = self.eventList
        
        let tabDeal = DealTripViewController()
        tabEvent.data = self.data
        tabDeal.dealList = self.dealList

        mTabBarController.delegate = self
        mTabBarController.viewControllers = [tabTodo, tabEvent, tabDeal]
        mTabBarController.tabBar.isHidden = true
        mTabBarController.selectedIndex = tempTabIndex
        
        self.contentView.addSubview(mTabBarController.view)
        self.addChild(mTabBarController)
        
        mTabBarController.view.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
        
    }

}


extension TripDetailViewController: UITabBarControllerDelegate {
    
}

