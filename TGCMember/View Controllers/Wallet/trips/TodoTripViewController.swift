//
//  TodoTripViewController.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TodoTripViewController: BaseTripDetailViewController {
    override func updateLanguages() {
        
    }
    
    var cellHeights: [CGFloat] = []
    @IBOutlet weak var tblTodo: UITableView!
    
    var todoList: [[String: Any?]] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblTodo.register(UINib(nibName: "TEOCell", bundle: nil), forCellReuseIdentifier: "TEOCell")
        
        decorate()
        
        renderView()
    }
    
    private func decorate() {
       
    }
    
    private func configCell() {
          cellHeights = Array(repeating: Const.closeCellHeight, count: self.todoList.count)
          tblTodo.estimatedRowHeight = Const.closeCellHeight
          tblTodo.rowHeight = UITableView.automaticDimension
      }
    
    private func renderView() {
        if self.todoList.count == 0 {
            willDisplayEmptyView(true)
        } else {
            willDisplayEmptyView(false)
            self.configCell()
            self.tblTodo.reloadData()
        }
    }
 
    private func handleOnClickedCardTEO(_ data: [String: Any?]?) {
        if let tData = data, let status = tData["Status"] as? String, let dealData = tData["DealTellEveryone"] as? [String: Any?] {
            //mPrint("tData", tData)
            if status == TEO_STATUS.NEW.rawValue {
                let vc = InviteFriendsViewController()
                vc.data = dealData
                vc.callback = { [weak self] (success) in
                    guard let self = self else {return}
                    
                    //self.getActionsList()
                }
                
               self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                // DO NOTHING
            }
            
        }
    }
    
    
}

extension TodoTripViewController : UITableViewDelegate, UITableViewDataSource {
    private func didSelectRowAt(_ indexPath: IndexPath, _ data: [String: Any?]?) {
           self.handleExpandCell(indexPath)
    }
       
    private func handleExpandCell(_ indexPath: IndexPath, forceOpen: Bool = false) {
         let rowData = todoList[indexPath.row]
         
         if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
             let cell = self.tblTodo.cellForRow(at: indexPath) as! TEOCell
             
             
             var duration = 0.0
             let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight
             
             if forceOpen {
                 if cellIsCollapsed {
                     cellHeights[indexPath.row] = Const.openCellHeight
                     cell.expand(true, animated: true, completion: nil)
                     duration = 0.3
    
                     UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                         self.tblTodo.beginUpdates()
                         self.tblTodo.endUpdates()
                         
                         if cell.frame.maxY > self.tblTodo.frame.maxY {
                             self.tblTodo.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
                         }
                     }, completion: nil)
                 }
                 
             } else {
                 if cellIsCollapsed {
                     cellHeights[indexPath.row] = Const.openCellHeight
                     cell.expand(true, animated: true, completion: nil)
                     duration = 0.3
                 } else {
                     cellHeights[indexPath.row] = Const.closeCellHeight
                     cell.expand(false, animated: true, completion: nil)
                     duration = 0.3
                 }
                 
                 UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
                     self.tblTodo.beginUpdates()
                     self.tblTodo.endUpdates()
                     
                     // fix https://github.com/Ramotion/folding-cell/issues/169
                     if cell.frame.maxY > self.tblTodo.frame.maxY {
                         self.tblTodo.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.top, animated: true)
                     }
                 }, completion: nil)
             }
         }
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return todoList.count
    }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowData = todoList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TEOCell") as? TEOCell
            
            cell?.config(rowData, onClickedCellCallback: {[weak self] (info) in
                guard let self = self else {return}
                
                self.didSelectRowAt(indexPath, info)
                
                }, onClickedCardCallback: {[weak self] (info) in
                    guard let self = self else {return}
                    
                    self.handleOnClickedCardTEO(rowData)
                    
                })
            
            cell?.layoutIfNeeded()
            
            return cell!
        }
        
        return UITableViewCell()
    }
      
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let rowData = todoList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            return cellHeights[indexPath.row]
        }
        
        return 460.0
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let rowData = todoList[indexPath.row]
        
        if rowData["Type"] as? String == TODO_TYPE.TEO.rawValue {
            guard case let cell as TEOCell = cell else {
                return
            }
            
            cell.backgroundColor = .clear
            
            if cellHeights[indexPath.row] == Const.closeCellHeight {
                cell.expand(false, animated: false, completion: nil)
            } else {
                cell.expand(true, animated: false, completion: nil)
            }
        }
        
        
    }
}
