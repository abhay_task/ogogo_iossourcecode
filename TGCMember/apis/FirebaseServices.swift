//
//  FirebaseServices.swift
//  TGCMember
//
//  Created by vang on 7/9/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import Foundation
import FirebaseFirestore

public enum FB_DATA_STATUS: String {
    case UPDATED
    case ADDED
    case REMOVED
}

let FB_CHANNEL: String = "/member"
let numberKeepMessage = 5

class FirebaseServices {
    
    private var _path: String?
    
    init(path: String) {
        self._path = path
    }
    
    func addListenerMessage(firebaseCallback: FirebaseCallback?) {
        let db = Firestore.firestore()
        let messRef = db.collection(self._path ?? "")
        
        messRef.addSnapshotListener { snapshot, error in
            guard let _ = error else {
                if let snap = snapshot {
                    for doc in snap.documentChanges {
                        if doc.type == .added {
                            firebaseCallback?(.ADDED, doc.document.data())
                        } else if doc.type == .modified {
                            firebaseCallback?(.UPDATED, doc.document.data())
                        } else if doc.type == .removed {
                            firebaseCallback?(.REMOVED, doc.document.data())
                        }
                    }
                }
                
                return
            }
        }
    }
    
   func addListenerMessage(_ path: String, firebaseCallback: FirebaseCallback?) {
        let db = Firestore.firestore()
        let messRef = db.collection(path)
        
        messRef.addSnapshotListener { querySnapshot, error in
            if let error = error {
                print("Error retreiving collection: \(error)")
            }
        }
    }
    
    
    func deleteOldMessage() { // DELETE OLD MESSAGES
        let db = Firestore.firestore()
        let messRef = db.collection(self._path ?? "")
        
        messRef.order(by: "createdAt", descending: false).getDocuments { (snapshot, error) in
            guard let _ = error else {
                if let snap = snapshot {
                    for index in 0..<snap.documents.count {
                        if index < snap.documents.count - numberKeepMessage {
                            let doc = snap.documents[index]
                            doc.reference.delete()
                        }
                    }
                }
                
                return
            }
        }
    }
    
    
    func addMesasge(data: [String: Any?] , firebaseCallback: FirebaseCallback?) {
        let db = Firestore.firestore()
        let messRef = db.collection(self._path ?? "")
        
        let aMess = messRef.document()
    
        aMess.setData(data as [String : Any]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                firebaseCallback?(.ADDED, data)
                
                //self.deleteOldMessage()
            }
        }

        
        
        
        
        
    }
    
}
