//
//  SuccessResponse.swift
//  TGCMember
//
//  Created by jonas on 1/20/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

// MARK: - SuccessResponse
struct ServerResponse: Codable {
    let message: String?
    private let _response: String?
    let status: Bool?
    
    var response: ResponseType { ResponseType(rawValue: _response?.lowercased() ?? "") ?? .none }
    
    enum CodingKeys: String,  CodingKey {
        case message
        case _response = "response"
        case status
    }
    
    enum ResponseType: String {
        case none
        case favorite
        case unfavorite
    }
    
    enum MessageText: String {
        case success = "Successful"
    }
    
    init(success: Bool) {
        message = ""
        _response = success ? "Successful" : "Failure"
        status = success
    }
}
