//
//  Place.swift
//  TGCMember
//
//  Created by jonas on 1/28/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

// MARK: - PlaceAPIResponse
struct PlaceAPIResponse: Codable {
    let result: Result

    enum CodingKeys: String, CodingKey {
        case result
    }
    
    // MARK: - Result
    struct Result: Codable {
        let addressComponents: [AddressComponent]
        let adrAddress, formattedAddress: String
        let geometry: Geometry
        let name: String
        let placeID: String

        enum CodingKeys: String, CodingKey {
            case addressComponents = "address_components"
            case adrAddress = "adr_address"
            case formattedAddress = "formatted_address"
            case geometry, name
            case placeID = "place_id"
        }
    }

    // MARK: - AddressComponent
    struct AddressComponent: Codable {
        let longName, shortName: String
        let types: [String]

        enum CodingKeys: String, CodingKey {
            case longName = "long_name"
            case shortName = "short_name"
            case types
        }
    }

    // MARK: - Geometry
    struct Geometry: Codable {
        let location: Location
    }

    // MARK: - Location
    struct Location: Codable {
        let lat, lng: Double
    }
}

// MARK: - PlacePredictionResponse
struct PlacePredictionResponse: Codable {
    let predictions: [Prediction]
    
    // MARK: - Prediction
    struct Prediction: Codable {
        let desc: String
        let placeID: String
        let structuredFormatting: StructuredFormatting

        enum CodingKeys: String, CodingKey {
            case desc = "description"
            case placeID = "place_id"
            case structuredFormatting = "structured_formatting"
        }
    }

    // MARK: - StructuredFormatting
    struct StructuredFormatting: Codable {
        let mainText: String?
        let secondaryText: String?

        enum CodingKeys: String, CodingKey {
            case mainText = "main_text"
            case secondaryText = "secondary_text"
        }
    }

}
