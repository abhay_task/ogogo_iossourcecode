//
//  SearchWallet.swift
//  TGCMember
//
//  Created by jonas on 1/21/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

// MARK: - SearchWallet
struct SearchWallet: Codable {
    let data: DataClass
    let message: String
    let status: Bool
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let limit, nextPage, offset, page: Int
        let prevPage: Int
        var deals: [Wallet.Deal]?
        let totalPage, totalRecord: Int

        enum CodingKeys: String, CodingKey {
            case limit = "Limit"
            case nextPage = "NextPage"
            case offset = "Offset"
            case page = "Page"
            case prevPage = "PrevPage"
            case deals = "Records"
            case totalPage = "TotalPage"
            case totalRecord = "TotalRecord"
        }
        
        mutating func prepend(_ deals: [Wallet.Deal]) {
            self.deals = deals + (self.deals ?? [])
        }
    }
}
