//
//  Package.swift
//  TGCMember
//
//  Created by jonas on 3/10/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

// MARK: - Package
struct PackageResponse: Codable {
    let data: DataClass?
    let message: String?
    let status: Bool?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let isNeedPayViaWeb: Bool?
        let freeMember: FreeMember?
        let packages: [Package]?

        enum CodingKeys: String, CodingKey {
            case isNeedPayViaWeb = "IsNeedPayViaWeb"
            case freeMember = "free-member"
            case packages
        }
    }
    
    // MARK: - FreeMember
    struct FreeMember: Codable {
        let currency: String?
        let freeMemberDescription: [Description]?
        let isBuyMoreCard, isPrimaryPackage, isSubscribed: Bool?
        let level: Int?
        let name, title: String?

        enum CodingKeys: String, CodingKey {
            case currency = "Currency"
            case freeMemberDescription = "Description"
            case isBuyMoreCard = "IsBuyMoreCard"
            case isPrimaryPackage = "IsPrimaryPackage"
            case isSubscribed = "IsSubscribed"
            case level = "Level"
            case name = "Name"
            case title = "Title"
        }
    }
    
    // MARK: - Description
    struct Description: Codable {
        let enable: Bool?
        let name: String?
    }
    
    // MARK: - PackageElement
    struct Package: Codable {
        let amount: String?
        let canDownGrade, canPayByCredit: Bool?
        let currency: String?
        let packageDescription: [Description]?
        let id: String?
        let imageURL: String?
        let isBuyMoreCard, isCurrentPlan, isPaymentFree, isPrimaryPackage: Bool?
        let isSubscribed: Bool?
        let level: Int?
        let name: String?
        let numberPurchases, price: Int?
        let saleAmount: String?
        let salePrice: Int?
        let statusSubscription: Bool?
        let subDescription, subscriptionUnix, textButtonUpgradeFree, title: String?
        let totalAmountPurchases: String?
        let totalPurchases: Int?
        let type: String?

        enum CodingKeys: String, CodingKey {
            case amount = "Amount"
            case canDownGrade = "CanDownGrade"
            case canPayByCredit = "CanPayByCredit"
            case currency = "Currency"
            case packageDescription = "Description"
            case id = "ID"
            case imageURL = "ImageURL"
            case isBuyMoreCard = "IsBuyMoreCard"
            case isCurrentPlan = "IsCurrentPlan"
            case isPaymentFree = "IsPaymentFree"
            case isPrimaryPackage = "IsPrimaryPackage"
            case isSubscribed = "IsSubscribed"
            case level = "Level"
            case name = "Name"
            case numberPurchases = "NumberPurchases"
            case price = "Price"
            case saleAmount = "SaleAmount"
            case salePrice = "SalePrice"
            case statusSubscription = "StatusSubscription"
            case subDescription = "SubDescription"
            case subscriptionUnix = "SubscriptionUnix"
            case textButtonUpgradeFree = "TextButtonUpgradeFree"
            case title = "Title"
            case totalAmountPurchases = "TotalAmountPurchases"
            case totalPurchases = "TotalPurchases"
            case type = "Type"
        }
    }
}
