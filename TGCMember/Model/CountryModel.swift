//
//  CountryModel.swift
//  TGCMember
//
//  Created by jonas on 7/9/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation

struct Country: Codable {
    private let _numCode: String?
    var numCode: Int { Int(_numCode ?? "0") ?? 0 }
    let alpha2: String?
    let alpha3: String?
    let shortName: String?
    let nationality: String?
    
    enum CodingKeys: String, CodingKey {
        case _numCode = "num_code"
        case alpha2 = "alpha_2_code"
        case alpha3 = "alpha_3_code"
        case shortName = "en_short_name"
        case nationality = "nationality"
    }
}
