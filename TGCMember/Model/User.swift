
import Foundation

protocol ProfileProtocol {
    var name: String { get }
    var email: String { get }
    var avatarURL: String { get }
    var gender: User.Details.LabelValue? { get }
    var birthyear: String { get }
    var marriageStatus: User.Details.LabelValue? { get }
    var educationLevel: User.Details.LabelValue? { get }
    var childrenCount: ((String) -> String) { get }
    var interestedIn: User.Details.InterestedIn? { get }
    var homeAddress: String { get }
    var nationality: String { get }
    var languages: String { get }
}

struct UpdateProfileResponse: Codable {
    let data: UpdateProfileData
    
    struct UpdateProfileData: Codable {
        let user: User
    }
}

// MARK: - UserClass
struct User: Codable {
    let id: String
    var avatarURL: String?
    var displayName: String?
    let email: String?
    let details: Details?
    let vendor: Vendor?
    let currentPlan, currentState: String?
    let country: Country?
    let firstName: String?
    let lastName: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case avatarURL = "AvatarURL"
        case displayName = "DisplayName"
        case email = "Email"
        case details = "MemberTargeting"
        case vendor = "VendorPerson"
        case currentPlan = "CurrentPlan"
        case currentState = "CurrentState"
        case country = "Country"
        case firstName = "FirstName"
        case lastName = "LastName"
    }
    
    init(dict: [String: Any?]) throws {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
        let decoder = JSONDecoder()
        let result = try decoder.decode(User.self, from: jsonData)
        self = result
    }
    
    // MARK: - MemberTargeting
    struct Details: Codable {
        var ageRange: LabelValue?
        var birthYear: Int?
        var education, gender: LabelValue?
        var interestedIn: InterestedIn?
        var kid: Int?
        var marriage: LabelValue?
        var homeAddress: String?
        var nationality: String?
        var languages: String?

        enum CodingKeys: String, CodingKey {
            case ageRange = "AgeRange"
            case birthYear = "BirthYear"
            case education = "Education"
            case gender = "Gender"
            case interestedIn = "InterestedIn"
            case kid = "Kid"
            case marriage = "Marriage"
            case homeAddress = "HomeAddress"
            case languages = "Languages"
            case nationality = "Nationality"
        }
        
        // MARK: - AgeRange
        struct LabelValue: Codable {
            let label: String
            let value: Int

            enum CodingKeys: String, CodingKey {
                case label = "Label"
                case value = "Value"
            }
        }

        // MARK: - InterestedIn
        struct InterestedIn: Codable {
            let data: [Datum]
            let label, value: String

            enum CodingKeys: String, CodingKey {
                case data = "Data"
                case label = "Label"
                case value = "Value"
            }
            
            // MARK: - Datum
            struct Datum: Codable {
                let id, name: String

                enum CodingKeys: String, CodingKey {
                    case id = "ID"
                    case name = "Name"
                }
            }
        }
    }
    
    struct Vendor: Codable {
        let summary: Summary?
        
        enum CodingKeys: String, CodingKey {
            case summary = "VendorSummary"
        }
    }
    
    struct Summary: Codable {
        var businessName: String?
        var logoURL: String?
        var categoryName: String?
        private let _city: String?
        private let _country: String?
        var vendorID: String?
        
        func address() -> String? {
            return "\(_city ?? ""), \(_country ?? "")"
        }
        
        enum CodingKeys: String, CodingKey {
            case businessName = "BusinessName"
            case logoURL = "LogoUrl"
            case categoryName = "CategoryName"
            case _city = "City"
            case _country = "Country"
            case vendorID = "VendorID"
        }
    }
    
    // MARK: - Country
    struct Country: Codable {
        let alpha2, alpha3, flag, name: String
        let phoneCode: [String]

        enum CodingKeys: String, CodingKey {
            case alpha2, alpha3, flag, name
            case phoneCode = "phone_code"
        }
    }
}
