//
//  Wallet.swift
//  TGCMember
//
//  Created by jonas on 1/15/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation
import GoogleMaps

// MARK: - Wallet
struct Wallet: Codable {
    let data: DataClass
    let message: String
    let status: Bool
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let carousel: [Carousel]?
        let isNotFound: Bool?
        let categories: [Category]?

        enum CodingKeys: String, CodingKey {
            case carousel = "Carousel"
            case isNotFound = "IsNotFound"
            case categories = "category"
        }
        
        // MARK: - Carousel
        struct Carousel: Codable {
            let icon: String?
            let key: String?
            var deals: [Deal]?
            let title: String?
            let total: Int?
            private let _type: String?
            let fromDate, toDate, tripID: String?
            let name: String?
            // Computed
            var type: DealType? { DealType(rawValue: _type ?? "")  }
            var trip: TripObject?

            enum CodingKeys: String, CodingKey {
                case icon = "Icon"
                case key = "Key"
                case deals = "List"
                case title = "Title"
                case total = "Total"
                case _type = "Type"
                case fromDate = "FromDate"
                case toDate = "ToDate"
                case tripID = "TripID"
                case name = "Name"
                case trip = "Object"
            }
            
            struct DealDetail: Codable {
                let deal: Deal
                let message: String
                let status: Bool
            }
        
            struct TripObject: Codable {
                let id: String?
                let lat, lng: Double?
                let name, address, toDate, fromDate: String?
                let goingFor, travellingWith: String?
                private var _interestedIn: String?
                let wishes: [Wish]?
                
                var interestedIn: [String] { _interestedIn?.split(separator: ",").map { String($0) } ?? [] }

                enum CodingKeys: String, CodingKey {
                    case lat, lng, name, address, wishes, id
                    case toDate = "to_date"
                    case fromDate = "from_date"
                    case goingFor = "going_for"
                    case _interestedIn = "interested_in"
                    case travellingWith = "travelling_with"
                }
                
                struct Wish: Codable {
                    let name: String?
                    let categoryId: String?
                    let categoryName: String?
                    
                    enum CodingKeys: String, CodingKey {
                        case name
                        case categoryId = "category_id"
                        case categoryName = "category_name"
                    }
                }
            }
        }
}

// MARK: - AvailableTime
struct AvailableTime: Codable {
    let id, relatedID: String?
    let type: String?
    let day: Day?
    let startAt, endAt: String?
    let startDate, endDate: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case relatedID = "RelatedID"
        case type = "Type"
        case day = "Day"
        case startAt = "StartAt"
        case endAt = "EndAt"
        case startDate = "StartDate"
        case endDate = "EndDate"
    }
}

struct Deal: Codable, Equatable {
    let allowReSubmit: Bool?
    let badgeColor: String?
    let claimCode: String?
    let createdAt: String?
    private let _dateUsed: String?
    let dayOfWeek: DayOfWeek?
    private let _dealEndDate: String?
    let dealProgram: DealProgramEnum?
    let dealProgramName: DealProgramName?
    let _dealStartDate: String?
    let dealStoreStatus: Int?
    let dealStoreStatusName: String?
    let dealTypeShowInGUIString: String?
    private let _expiryTime: String?
    let id, instanceDealID: String?
    let fromAtVendor: String?
    var isFavorite: Bool?
    let isLimit, isPublish: Bool?
    let labelTxt, longDescription, name: String?
    let perUserLimit: Int?
    let photo: String?
    let promotionDescription: String?
    let promotionType: Int?
    let promotionTypeName: String?
    let promotionvalue: Int?
    let shortDescription, shortLabelTxt: String?
    let status: String?
    let tags: [Tag]?
    let template: Int?
    let titleLine1, titleLine2: String?
    let toAtVendor: String?
    let totalAvailable: Int?
    let txtDayOfWeek: String?
    let _type: String?
    let updatedAt: String?
    let vendorSummary: VendorSummary?
    let youCanUseThisDeal: Bool?
    let backGroundLoyalty: String?
    let numberOfMultiDeals: Int?
    let pingIcon: DealProgramEnum?
    var selected: Bool?
    
    var dealEndDate: Date? { _dealEndDate?.isoToDate() }
    var dealStartDate: Date? { _dealStartDate?.isoToDate() }
    var expiryTime: Date? { _expiryTime?.isoToDate() }
    var dateUsed: Date? { _dateUsed?.isoToDate() ?? _dateUsed?.toDate(format: "yyyy-MM-dd'T'HH:mm:ssZ") }
    var promotionText: String { (labelTxt ?? "FREE").uppercased() }
    var type: DealType? { DealType(rawValue: _type ?? "") }
    var isExpired: Bool { expiryTime != nil && expiryTime?.isInThePast == true }
    var tripInfo: TripInfo?
    
    var usable: Bool { 
        if type == .basic || type == .loyalty { return true }
        return dateUsed == nil && !isExpired
    }
    
    var isWhiteFont: Bool { type == .loyalty || type == .black || type == .basic }
    
    static func == (lhs: Deal, rhs: Deal) -> Bool {
        return lhs.id == rhs.id && lhs.instanceDealID == rhs.instanceDealID
    }
    
    var pinCategoryImageURL: String? {
        switch type {
        case .gold, .silver: return vendorSummary?.category?.imageURLStyle1
        default: return vendorSummary?.category?.imageURLStyle3
        }
    }

    enum CodingKeys: String, CodingKey {
        case allowReSubmit = "AllowReSubmit"
        case claimCode = "ClaimCode"
        case createdAt = "CreatedAt"
        case _dateUsed = "DateUsed"
        case badgeColor = "BadgeColor"
        case dayOfWeek = "DayOfWeek"
        case _dealEndDate = "DealEndDate"
        case dealProgram = "DealProgram"
        case dealProgramName = "DealProgramName"
        case _dealStartDate = "DealStartDate"
        case dealStoreStatus = "DealStoreStatus"
        case dealStoreStatusName = "DealStoreStatusName"
        case dealTypeShowInGUIString = "DealTypeShowInGUIString"
        case _expiryTime = "ExpiryTime"
        case fromAtVendor = "FromAtVendor"
        case id = "ID"
        case instanceDealID = "InstanceDealID"
        case isFavorite = "IsFavorite"
        case isLimit = "IsLimit"
        case isPublish = "IsPublish"
        case labelTxt = "LabelTxt"
        case longDescription = "LongDescription"
        case name = "Name"
        case perUserLimit = "PerUserLimit"
        case photo = "Photo"
        case promotionDescription = "PromotionDescription"
        case promotionType = "PromotionType"
        case promotionTypeName = "PromotionTypeName"
        case promotionvalue = "Promotionvalue"
        case shortDescription = "ShortDescription"
        case shortLabelTxt = "ShortLabelTxt"
        case status = "Status"
        case tags = "Tags"
        case template = "Template"
        case titleLine1 = "TitleLine1"
        case titleLine2 = "TitleLine2"
        case toAtVendor = "ToAtVendor"
        case totalAvailable = "TotalAvailable"
        case txtDayOfWeek = "TxtDayOfWeek"
        case _type = "Type"
        case updatedAt = "UpdatedAt"
        case vendorSummary = "VendorSummary"
        case youCanUseThisDeal = "YouCanUseThisDeal"
        case backGroundLoyalty = "BackGroundLoyalty"
        case numberOfMultiDeals = "NumberOfMultiDeals"
        case pingIcon = "PingIcon"
        case selected = "selected"
        case tripInfo = "TripInfor"
    }
    
    init(dict: [String: Any?]) throws {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
        let decoder = JSONDecoder()
        let result = try decoder.decode(Wallet.Deal.self, from: jsonData)
        self = result
    }
    
    // Trip Info
    struct TripInfo: Codable {
        let address, color, fromDateString, iconTrip, name, toDateString: String?
        
        enum CodingKeys: String, CodingKey {
            case address = "Address"
            case color = "Color"
            case fromDateString = "FromDateAt"
            case iconTrip = "IconTrip"
            case name = "Name"
            case toDateString = "ToDateAt"
        }
    }
    
    // MARK: - VendorSummary
    struct VendorSummary: Codable {
        let id, vendorID, categoryID: String?
        let name: String?
        let logoURL, bannerURL: String?
        let phone, phoneCode: String?
        let siteURL: String?
        let address: String?
        let address2, fullAddress: String?
        let city, state: String?
        let country: String?
        let countryCode: String?
        let countryName: String?
        let zipCode, postCode: String?
        let location: Location?
        let distance: Int?
        let timeOfUser: String?
        let category: Category?
        let availableTime: [AvailableTime]?
        let dealPrograms: [DealProgram]?

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case vendorID = "VendorID"
            case categoryID = "CategoryID"
            case name = "Name"
            case logoURL = "LogoUrl"
            case bannerURL = "BannerURL"
            case phone = "Phone"
            case phoneCode = "PhoneCode"
            case siteURL = "SiteURL"
            case address = "Address"
            case address2 = "Address2"
            case fullAddress = "FullAddress"
            case city = "City"
            case state = "State"
            case country = "Country"
            case countryCode = "CountryCode"
            case countryName = "CountryName"
            case zipCode = "ZipCode"
            case postCode = "PostCode"
            case location = "Location"
            case distance = "Distance"
            case timeOfUser = "TimeOfUser"
            case category = "Category"
            case availableTime = "AvailableTime"
            case dealPrograms = "DealPrograms"
        }
    }

    // MARK: - AvailableTime
    struct AvailableTime: Codable {
        let id, relatedID: String?
        let type: String?
        let day: Day?
        let _startAt, _endAt: String?
        
        let startDate, endDate: String?
        
        var startAt: Date? { _startAt?.isoToDate() }
        var endAt: Date? { _endAt?.isoToDate() }

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case relatedID = "RelatedID"
            case type = "Type"
            case day = "Day"
            case _startAt = "StartAt"
            case _endAt = "EndAt"
            case startDate = "StartDate"
            case endDate = "EndDate"
        }
    }
}
    
    // MARK: - Category
    struct Category: Codable {
        let id, name: String?
        let imageURL, imageURLSelected, imageURLStyle1, imageURLStyle1Selected: String?
        let imageURLStyle2, imageURLStyle2Selected, imageURLStyle3: String?

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case name = "Name"
            case imageURL = "ImageUrl"
            case imageURLSelected = "ImageUrlSelected"
            case imageURLStyle1 = "ImageUrlStyle1"
            case imageURLStyle1Selected = "ImageUrlStyle1Selected"
            case imageURLStyle2 = "ImageUrlStyle2"
            case imageURLStyle2Selected = "ImageUrlStyle2Selected"
            case imageURLStyle3 = "ImageUrlStyle3"
        }
    }
    
    // MARK: - DealProgram
    struct DealProgram: Codable {
        let id, dealID, vendorID: String?
        let dealProgram: DealProgramEnum?
        let totalAvailableOverride: Int?
        let perUserLimitOverride: Int?
        let isLimitOverride: Bool?
        let isClaimCodeOverride, isNeverExpired: Bool?
        let isPublishOverride, allowReSubmit: Bool?
        let status, priorityDeal: Int?
        let createdAt, updatedAt: String?
        let dealInfos: DealInfos?

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case dealID = "DealID"
            case vendorID = "VendorID"
            case dealProgram = "DealProgram"
            case totalAvailableOverride = "TotalAvailableOverride"
            case perUserLimitOverride = "PerUserLimitOverride"
            case isLimitOverride = "IsLimitOverride"
            case isClaimCodeOverride = "IsClaimCodeOverride"
            case isNeverExpired = "IsNeverExpired"
            case isPublishOverride = "IsPublishOverride"
            case allowReSubmit = "AllowReSubmit"
            case status = "Status"
            case priorityDeal = "PriorityDeal"
            case createdAt = "CreatedAt"
            case updatedAt = "UpdatedAt"
            case dealInfos = "DealInfos"
        }
    }

    // MARK: - DealInfos
    struct DealInfos: Codable {
        let id, vendorID: String?
        let tagsIDs: [String]?
        let name: String?
        let type: DealType?
        let titleLine1, titleLine2: String?
        let template: Int?
        let shortDescription, longDescription: String?
        let photo: String?
        let status: Int?
        let isPublish: Bool?
        let claimCode: String?
        let expiryTime: String?
        let totalAvailable, perUserLimit: Int?
        let isLimit: Bool?
        let promotionType, promotionvalue: Int?
        let promotionDescription: String?
        let backgroundImage: String?
        let createdAt, updatedAt: String?

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case vendorID = "VendorID"
            case tagsIDs = "TagsIDs"
            case name = "Name"
            case type = "Type"
            case titleLine1 = "TitleLine1"
            case titleLine2 = "TitleLine2"
            case template = "Template"
            case shortDescription = "ShortDescription"
            case longDescription = "LongDescription"
            case photo = "Photo"
            case status = "Status"
            case isPublish = "IsPublish"
            case claimCode = "ClaimCode"
            case expiryTime = "ExpiryTime"
            case totalAvailable = "TotalAvailable"
            case perUserLimit = "PerUserLimit"
            case isLimit = "IsLimit"
            case promotionType = "PromotionType"
            case promotionvalue = "Promotionvalue"
            case promotionDescription = "PromotionDescription"
            case backgroundImage = "BackgroundImage"
            case createdAt = "CreatedAt"
            case updatedAt = "UpdatedAt"
        }
    }

    // MARK: - GoogleParseFromLatLng
    struct GoogleParseFromLatLng: Codable {
        let city: String?
        let state: String?
        let county, street, suburb: String?
        let country: String?
        let postcode: String?
        let countryCode: String?
        let houseNumber: String?
        let stateDistrict: String?
        let formattedAddress: String?

        enum CodingKeys: String, CodingKey {
            case city = "City"
            case state = "State"
            case county = "County"
            case street = "Street"
            case suburb = "Suburb"
            case country = "Country"
            case postcode = "Postcode"
            case countryCode = "CountryCode"
            case houseNumber = "HouseNumber"
            case stateDistrict = "StateDistrict"
            case formattedAddress = "FormattedAddress"
        }
    }

    // MARK: - Location
    struct Location: Codable, Hashable {
        let lng, lat: Double?
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(lng)
            hasher.combine(lat)
        }
        
        func toCoordinates() -> CLLocationCoordinate2D? {
            if let lng = lng, let lat = lat {
                return CLLocationCoordinate2D(latitude: lat, longitude: lng)
            }
            
            return nil
        }
    }

}

extension Wallet {
    // MARK: - Tag
    struct Tag: Codable {
        let id, name: String?
        let status: Bool?

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case name = "Name"
            case status = "Status"
        }
    }

    enum Day: String, Codable {
        case friday = "friday"
        case monday = "monday"
        case saturday = "saturday"
        case sunday = "sunday"
        case thursday = "thursday"
        case tuesday = "tuesday"
        case wednesday = "wednesday"
    }
}


enum BadgeColor: String, Codable {
    case red = "red"
}

enum DayOfWeek: String, Codable {
    case empty = ""
    case mondayTuesdayWednesdayThursdayFridaySaturdaySunday = "monday,tuesday,wednesday,thursday,friday,saturday,sunday"
}

enum DealProgramEnum: String, Codable {
    case basic = "basic"
    case loyalty = "loyalty"
    case swipe = "swipe"
}

enum DealProgramName: String, Codable {
    case basic = "Basic"
    case daily = "Daily"
    case loyalty = "Loyalty"
}


//// MARK: - NeedToOverride
//struct NeedToOverride: Codable {
//    let isClaimCodeOverride: Bool?
//    let isLimitOverride: Bool
//    let isNeverExpired: Bool?
//    let isPublishOverride: Bool
//    let perUserLimitOverride: Int?
//    let status: Status
//    let statusInt, totalAvailableOverride: Int
//
//    enum CodingKeys: String, CodingKey {
//        case isClaimCodeOverride = "IsClaimCodeOverride"
//        case isLimitOverride = "IsLimitOverride"
//        case isNeverExpired = "IsNeverExpired"
//        case isPublishOverride = "IsPublishOverride"
//        case perUserLimitOverride = "PerUserLimitOverride"
//        case status = "Status"
//        case statusInt = "StatusInt"
//        case totalAvailableOverride = "TotalAvailableOverride"
//   }
//}
