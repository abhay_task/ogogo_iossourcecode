//
//  Trip.swift
//  TGCMember
//
//  Created by jonas on 1/26/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

// MARK: - PlanTripResponse
enum TripType: String {
    case travellingWith = "travelling_with"
    case interestedIn = "interested_in"
    case goingFor = "going_for"
}

struct Trip: Codable {
    let data: DataClass
    let message: String
    let status: Bool
    
    var travellingWithList: [DataClass.Answer] { data.list.first(where: { $0.key == .travellingWith })?.answers ?? [] }
    var interestedInList: [DataClass.Answer] { data.list.first(where: { $0.key == .interestedIn })?.answers ?? [] }
    var goingForList: [DataClass.Answer] { data.list.first(where: { $0.key == .goingFor })?.answers ?? [] }
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let list: [List]
        
        // MARK: - List
        struct List: Codable {
            let id: String
            private let _key: String
            let answers: [Answer]
            let multiSelect: Bool
            let question: String
            var key: TripType { return TripType(rawValue: _key) ?? .travellingWith }

            enum CodingKeys: String, CodingKey {
                case id = "ID"
                case _key = "Key"
                case answers = "ListAnwser"
                case multiSelect = "MultiSelect"
                case question = "Question"
            }
        }

        // MARK: - ListAnwser
        struct Answer: Codable {
            let id, value: String

            enum CodingKeys: String, CodingKey {
                case id = "ID"
                case value = "Value"
            }
        }
    }
}

struct TripPreferences {
    var id: String?
    var name: String = ""
    var address: String = ""
    var lat: Double?
    var lng: Double?
    var fromDate: String = ""
    var toDate: String = ""
    var travellingWith: Trip.DataClass.Answer
    var interestedIn: [Trip.DataClass.Answer]
    var goingFor: Trip.DataClass.Answer
    var wishList = [CreateWishRequest]()
    
    var isComplete: Bool {
        return !name.isEmpty && !address.isEmpty && lat != nil && lng != nil && !fromDate.isEmpty && !toDate.isEmpty
    }
    
    func toRequest() -> CreateTripRequest {
        return CreateTripRequest(id: id, tripPref: self)
    }
}
