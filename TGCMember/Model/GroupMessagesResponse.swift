//
//  GroupMessagesResponse.swift
//  TGCMember
//
//  Created by jonas on 10/23/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let groupMessagesResponse = try? newJSONDecoder().decode(GroupMessagesResponse.self, from: jsonData)

import Foundation

// MARK: - GroupMessagesResponse
struct GroupMessagesResponse: Codable {
    let data: DataClass?
    let message: String?
    let status: Bool?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let totalUnread: Int?
        let list: List?

        enum CodingKeys: String, CodingKey {
            case totalUnread = "TotalUnread"
            case list
        }
        
        // MARK: - List
        struct List: Codable {
            let limit, nextPage, offset, page: Int?
            let prevPage: Int?
            let records: [Record]?
            let totalPage, totalRecord: Int?

            enum CodingKeys: String, CodingKey {
                case limit = "Limit"
                case nextPage = "NextPage"
                case offset = "Offset"
                case page = "Page"
                case prevPage = "PrevPage"
                case records = "Records"
                case totalPage = "TotalPage"
                case totalRecord = "TotalRecord"
            }
            
            // MARK: - Record
            struct Record: Codable {
                let numberOfMember: Int?
                let createdAt, firebasePath, groupID, groupName: String?
                let lastMessage: LastMessage?
                let lastMessageSendAt: String?
                let lastReadMessage: Message?
                let members: [Partner]?
                let partner: Partner?
                let status: Status?
                let tellEveryone: TellEveryone?
                let unread: Int?
                let updatedAt: String?

                enum CodingKeys: String, CodingKey {
                    case numberOfMember = "NumberOfMember"
                    case createdAt = "created_at"
                    case firebasePath = "firebase_path"
                    case groupID = "group_id"
                    case groupName = "group_name"
                    case lastMessage = "last_message"
                    case lastMessageSendAt = "last_message_send_at"
                    case lastReadMessage = "last_read_message"
                    case members, partner, status
                    case tellEveryone = "tell_everyone"
                    case unread
                    case updatedAt = "updated_at"
                }
                
                // MARK: - LastMessage
                struct LastMessage: Codable {
                    let message: Message?
                    let user: Partner?
                }
                
                // MARK: - Partner
                struct Partner: Codable {
                    let id: String?
                    let avatar: String?
                    let businessName, name: String?
                    let role: Role?

                    enum CodingKeys: String, CodingKey {
                        case id = "_id"
                        case avatar
                        case businessName = "business_name"
                        case name, role
                    }
                }
                
                enum Role: String, Codable {
                    case member = "member"
                    case vendor = "vendor"
                }

                enum Status: String, Codable {
                    case accept = "accept"
                }
                
                // MARK: - Message
                struct Message: Codable {
                    let groupID, id, createdAt, image: String?
                    let location: Location?
                    let locationPoint: LocationPoint?
                    let ownerID, text: String?
                    let type: Int?
                    let typeName, video: String?

                    enum CodingKeys: String, CodingKey {
                        case groupID = "GroupID"
                        case id = "ID"
                        case createdAt, image, location, locationPoint
                        case ownerID = "ownerId"
                        case text, type, typeName, video
                    }
                    
                    // MARK: - Location
                    struct Location: Codable {
                        let latitude, longitude: String?
                    }
                    
                    // MARK: - LocationPoint
                    struct LocationPoint: Codable {
                        let lng, lat: Int
                    }
                }
                
                // MARK: - TellEveryone
                struct TellEveryone: Codable {
//                        let collections: JSONNull?
                    let isAssignTEOffer, isTEOfferAvailable: Bool
                    let numberOfAssigneeCompleted: Int
                    let status, statusName: String
                    let totalOfRequiredAssignee: Int

                    enum CodingKeys: String, CodingKey {
//                            case collections = "Collections"
                        case isAssignTEOffer = "IsAssignTEOffer"
                        case isTEOfferAvailable = "IsTEOfferAvailable"
                        case numberOfAssigneeCompleted = "NumberOfAssigneeCompleted"
                        case status = "Status"
                        case statusName = "StatusName"
                        case totalOfRequiredAssignee = "TotalOfRequiredAssignee"
                    }
                }
            }
        }
    }
}





















