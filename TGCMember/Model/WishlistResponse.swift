//
//  WishlistResponse.swift
//  TGCMember
//
//  Created by jonas on 4/15/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation

protocol WishProtocol {
    var categoryName: String? { get }
    var name: String? { get }
}

// MARK: - WishlistResponse
struct WishlistResponse: Codable {
    let data: DataClass?
    let message: String?
    let status: Bool?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        var list: List?
        
        // MARK: - List
        struct List: Codable {
            let limit, nextPage, offset, page: Int?
            let prevPage: Int?
            var records: [Record]?
            let totalPage, totalRecord: Int?

            enum CodingKeys: String, CodingKey {
                case limit = "Limit"
                case nextPage = "NextPage"
                case offset = "Offset"
                case page = "Page"
                case prevPage = "PrevPage"
                case records = "Records"
                case totalPage = "TotalPage"
                case totalRecord = "TotalRecord"
            }
            
            // MARK: - Record
            struct Record: Codable, WishProtocol {
                let categoryName, name, uid: String?
                let categoryIDs: [String]?
                let userID: String?

                enum CodingKeys: String, CodingKey {
                    case categoryIDs = "category_id"
                    case categoryName = "category_name"
                    case name, uid
                    case userID = "user_id"
                }
            }
        }
    }
}
