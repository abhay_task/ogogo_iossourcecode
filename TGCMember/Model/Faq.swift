//
//  Faq.swift
//  TGCMember
//
//  Created by jonas on 9/1/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation

// MARK: - FaqsResponse
struct FAQsResponse: Codable {
    let data: String
    let faqs: [FAQ]
    let status: Bool
    
    enum CodingKeys: String, CodingKey {
        case data, status
        case faqs = "message"
    }
    
    // MARK: - Message
    struct FAQ: Codable {
        let id, question, answer: String

        enum CodingKeys: String, CodingKey {
            case id = "ID"
            case question = "Question"
            case answer = "Answer"
        }
    }
}


