//
//  InfoTarget.swift
//  TGCMember
//
//  Created by jonas on 2/11/21.
//  Copyright © 2021 gkim. All rights reserved.
//
import Foundation

// MARK: - InfoTargetResponse
struct InfoTarget: Codable {
    let data: DataClass
    let status: Bool
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let ageRanges, educationLevels, genders: [String: String]
        let categories: [Wallet.Category]
        let marriages: [String: String]
        
//        var ageRanges: [String] { _ageRanges.compactMap({ $0.value }) }
//        var educationLevels: [String] { _educationLevels.compactMap({ $0.value }) }
//        var genders: [String] { _genders.compactMap({ $0.value }) }
//        var marriages: [String] { _marriages.compactMap({ $0.value }) }
        
        enum CodingKeys: String, CodingKey {
            case ageRanges = "AgeRanges"
            case educationLevels = "Educations"
            case genders = "Genders"
            case categories = "InterestIn"
            case marriages = "Marriages"
        }
    }
}
