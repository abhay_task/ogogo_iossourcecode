//
//  UpgradePackage.swift
//  TGCMember
//
//  Created by jonas on 12/4/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import Foundation

struct UpgradePackage: Codable {
    var canUpNextStep: Bool = false
    private let _canUpToNextStep: String
    var iconUpgradeNextStep: String = ""
    var canUpToNextStep: UPGRADE_PLAN { UPGRADE_PLAN(rawValue: _canUpToNextStep) ?? .BASIC }

    
    init(_ canUpNextStep: Bool,
         canUpToNextStep: String,
         iconUpgradeNextStep: String) {
        
        self.canUpNextStep = canUpNextStep
        self._canUpToNextStep = canUpToNextStep
        self.iconUpgradeNextStep = iconUpgradeNextStep
    }
}
