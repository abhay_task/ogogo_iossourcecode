//
//  AlertViewStruct.swift
//  TGCMember
//
//  Created by jonas on 1/29/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

struct CustomAlertObject: Codable {
    var success: Bool? = nil
    var message: String
    var titleBtnConfirm: String
    var titleBtnCancel: String?
}
