//
//  UserLanguage.swift
//  TGCMember
//
//  Created by jonas on 7/9/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation

struct UserLanguage: Codable {
    let language: String
    
    enum CodingKeys: String, CodingKey {
        case language = "Language"
    }
}
