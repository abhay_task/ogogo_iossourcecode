//
//  WishlistViewModel.swift
//  TGCMember
//
//  Created by jonas on 4/14/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation
import AVFAudio

class WishlistViewModel: BaseViewModel {
    
    private var list: WishlistResponse.DataClass.List?
    var wishes: [WishlistResponse.DataClass.List.Record] { list?.records ?? [] }
    var numberOfWishes: Int { wishes.count }
    
    var wishToEdit: WishlistResponse.DataClass.List.Record? 
    
    private var wishRequest: CreateWishRequest!
    var selectedCategory: Wallet.Category?
    var onEditComplete: VoidCallback?
    var onEditCompleteWithMessage: GenericCallback<String>?
    
    convenience init(repository: RepositoryProtocol,
                     wish: WishlistResponse.DataClass.List.Record? = nil) {
        self.init(repository: repository)
        self.wishToEdit = wish
    }
    
    // GET
    func getWishlist() {
        onStartLoading?()
        isLoading = true
        
        repository.getWishlist { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            switch result {
            case .success(let list):
                self.list = list
                self.onComplete?()
            case .failure(let error):
                self.onCompleteWithMessage?(error.message)
            }
        }
    }
    
    func generateRequest(name: String?) {
        guard let category = selectedCategory else {
            return
        }
        
        wishRequest = CreateWishRequest(id: wishToEdit?.uid,
                                        userId: CacheManager.shared.currentUser?.id ?? "",
                                        category: category,
                                        name: name ?? wishRequest.name)
    }
    
    // POST
    func createWish() {
        guard let request = wishRequest, !(request.name?.isEmpty ?? false) else { return }
        
        onStartLoading?()
        isLoading = true
        repository.createWish(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            switch result {
            case .success(let response):
                if response.status ?? false {
                    print("SUCCESS")
                    self.onComplete?()
                } else {
                    self.onCompleteWithMessage?(response.message ?? "")
                }
            case .failure(let error):
                self.onCompleteWithMessage?(error.message)
            }
        })
    }
    
    func editWish() {
        guard let request = wishRequest, !(request.name?.isEmpty ?? false) else { return }
        
        onStartLoading?()
        isLoading = true
        repository.editWish(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            switch result {
            case .success(let response):
                if response.status ?? false {
                    print("SUCCESS")
                    self.onEditComplete?()
                } else {
                    self.onCompleteWithMessage?(response.message ?? "")
                }
            case .failure(let error):
                self.onEditCompleteWithMessage?(error.message)
            }
        })
    }
    
    func deleteWish(at index: Int) {
        guard let id = wishes[safe: index]?.uid else { return }
        onStartLoading?()
        isLoading = true
        
        let request = IDRequest(id: id)
        repository.deleteWish(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            
            switch result {
            case .success(let response):
                print(response)
                if let index = self.wishes.firstIndex(where: { $0.uid == id }) {
                    self.list?.records?.remove(at: index)
                }
                self.onComplete?()
            case .failure(let error):
                self.onCompleteWithMessage?(error.message)
            }
        })
    }
    
    func editWish(at index: Int) {
        
    }
}
