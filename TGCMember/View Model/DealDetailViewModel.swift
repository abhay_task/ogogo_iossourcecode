//
//  DealDetailViewModel.swift
//  TGCMember
//
//  Created by jonas on 1/19/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

class DealDetailViewModel: BaseViewModel {
    
    var deal: Wallet.Deal!
    
    var onDeleteComplete: VoidCallback?
    
    convenience init(repository: RepositoryProtocol, deal: Wallet.Deal) {
        self.init(repository: repository)
        self.deal = deal
    }
    
    func getDealDetails() {
        onStartLoading?()
        
        repository.getDealDetails(deal.instanceDealID ?? "", completionHandler: { [weak self] result in
            guard let self = self else { return }
            if let deal = try? result.get() {
                self.deal = deal
                NotificationCenter.default.post(name: .UPDATE_DEAL, object: deal)
//                NotificationCenter.default.post(name: .REPOSITION_DEAL, object: deal)
            }
            self.onComplete?()
        })
    }
    
    func toggleFavorite() {
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.toggleFavoriteStatusOfDeal(request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                self.deal.isFavorite = response.response == .favorite
                NotificationCenter.default.post(name: .UPDATE_DEAL, object: self.deal)
                NotificationCenter.default.post(name: .UPDATE_FAVE_DEAL, object: self.deal)
                self.onComplete?()
            case .failure(let error):
                self.onErrorWithMessage?(error.message)
            }
        })
    }
    
    func deleteDeal() {
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.deleteDeal(request: request, completionHandler: { [weak self] (succeed, message) in
            guard let self = self else { return }
            
            if succeed {
                self.onDeleteComplete?()
                return
            }
            
            self.onErrorWithMessage?(message ?? "Failed")
        })
    }
    
    func isDealHasClaimCode() -> Bool {
        return deal.claimCode?.isEmpty == false
    }
}
