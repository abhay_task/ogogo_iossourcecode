//
//  BaseViewModel.swift
//  TGCMember
//
//  Created by jonas on 12/9/20.
//  Copyright © 2020 gkim. All rights reserved.
//

typealias VoidCallBack = () -> Void
typealias StringCallBack = (String) -> Void
typealias BoolCallBack = (Bool) -> Void
typealias IntCallBack = (Int) -> Void

import Foundation

class BaseViewModel {
    var onStartLoading: VoidCallBack?
    var onComplete: VoidCallBack?
    var onError: VoidCallBack?
    
    var onCompleteWithMessage: StringCallBack?
    var onErrorWithMessage: StringCallBack?
    var onHomeSearchMapComplete: GenericCallback<SearchResultViewModel>?
    
    let repository: RepositoryProtocol
    
    var userInfo: [String: Any?] { Utils.getUserInfo() ?? [:] }
    
    var isLoading: Bool = false
    
    lazy var infoTargetCategories: [Wallet.Category] = {
        return CacheManager.shared.infoTarget?.data.categories ?? []
    }()
    
    init(repository: RepositoryProtocol) {
        self.repository = repository
    }
    
    func searchMapForDeals(request: SearchDealsMapRequest) {
        onStartLoading?()
        
        repository.searchDealsOnMap(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                let searchResVM = SearchResultViewModel(repository: self.repository,
                                                        data: data,
                                                        keyword: request.keyword ?? "")
                self.onHomeSearchMapComplete?(searchResVM)
            case .failure(let error):
                self.onErrorWithMessage?(error.localizedDescription)
            }
        })
//        repository.searchDeals(request: request, completionHandler: { [weak self] result in
//            guard let self = self else { return }
//            switch result {
//            case .success(let data):
//                let searchResVM = SearchResultViewModel(repository: self.repository,
//                                                        data: data,
//                                                        keyword: keyword)
//                self.onHomeSearchMapComplete?(searchResVM)
//            case .failure(let error):
//                self.onErrorWithMessage?(error.localizedDescription)
//            }
//        })
    }
}
