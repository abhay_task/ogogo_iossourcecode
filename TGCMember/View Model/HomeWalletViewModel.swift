//
//  HomeWalletViewModel.swift
//  TGCMember
//
//  Created by jonas on 1/7/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

enum ViewModelError: Error {
    case failToGenerate
}

class HomeWalletViewModel: BaseViewModel {
    
    typealias DealsByType = (type: DealType, deals: [Wallet.Deal])
    private var _carouselList = [Wallet.DataClass.Carousel]() {
        didSet {
            carouselList = _carouselList
        }
    }
    
    var carouselList = [Wallet.DataClass.Carousel]()
    
    var numberOfCarousels: Int { carouselList.count }
    private var categories = [Wallet.Category]()
    var numberOfCategories: Int { categories.count }
    var selectedCategories = [Wallet.Category]() {
        didSet {
            getDeals()
        }
    }
    
    var autoCompleteRequest: TGCDataRequest?
    
    var tripPreferences: TripPreferences? {
        didSet {
            onTripPreferencesCompletionCallback?(tripPreferences?.isComplete ?? false)
        }
    }
    
    var trip: Trip?
    
    var onSearchComplete: GenericCallback<SearchResultViewModel>?
    var onTripPreferencesCompletionCallback: GenericCallback<Bool>?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init(repository: RepositoryProtocol) {
        super.init(repository: repository)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDealUpdated(_:)), name: .UPDATE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDealDeletion(_:)), name: .DELETE_DEAL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getDeals), name: .RELOAD_WALLET, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateFavoriteDeal(_:)), name: .UPDATE_FAVE_DEAL, object: nil)
    }
    
    @objc func getDeals(_ toggleIsLoading: Bool = true) {
        isLoading = toggleIsLoading
        onStartLoading?()
        
        let request = !selectedCategories.isEmpty ? FilterWalletRequest(categoryIDs: selectedCategories.map { $0.id ?? "" }.joined(separator: ",")) : nil
        repository.getDeals(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            self.isLoading = false
            switch result {
            case .success(let wallet):
                let carousel = wallet.data.carousel?.compactMap({ item -> Wallet.DataClass.Carousel? in
                    var carouselItem = item
                    let deals = item.deals?.filter { ($0.id != nil || $0.photo != nil) && $0.usable } ?? []
                    carouselItem.deals = deals.filter({ $0.vendorSummary?.name?.lowercased().contains("test") == false })
                    return carouselItem
                })
                
                self._carouselList = carousel?.sorted(by: { self.compareOptionalsWithLargeNil(lhs: $0.trip?.fromDate?.toDate(), rhs: $1.trip?.fromDate?.toDate()) }).sorted(by: { ($0.type?.orderValue ?? 0) > ($1.type?.orderValue ?? 0) }).filter({ $0.trip?.toDate?.toDate()?.isInThePast != true }) ?? []
                
                self.categories = wallet.data.categories ?? []
                
                self.handleFavoritesDealByCategories()
                
                self.onComplete?()
            case .failure(let error):
                self.onErrorWithMessage?(error.localizedDescription)
            }
        })
    }
    
    func compareOptionalsWithLargeNil<T: Comparable>(lhs: T?, rhs: T?) -> Bool {
        switch (lhs, rhs) {
        case let(l?, r?): return l < r // Both lhs and rhs are not nil
        case (nil, _): return true    // Lhs is nil
        case (_?, nil): return false    // Lhs is not nil, rhs is nil
        }
    }
    
    func getTrip(completion: @escaping GenericResult<Trip, ErrorResponse>) {
        
    }
    
    func dealsListCountAt(_ index: Int) -> Int {
        return carouselList[safe: index]?.deals?.count ?? 0
    }
    
    func toggleFavoriteAt(_ index: Int, groupIndex: Int) {
        guard let deal = carouselList[safe: groupIndex]?.deals?[safe: index] else { return }
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.toggleFavoriteStatusOfDeal(request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                var deal = deal
                deal.isFavorite = response.response == .favorite
                NotificationCenter.default.post(name: .UPDATE_DEAL, object: deal)
                NotificationCenter.default.post(name: .UPDATE_FAVE_DEAL, object: deal)
            case .failure(let error):
                self.onErrorWithMessage?(error.message)
            }
        })
    }
    
    func hasDealID(_ index: Int, groupIndex: Int) -> Bool {
        guard let deal = carouselList[safe: groupIndex]?.deals?[safe: index] else { return true }
        return deal.id != nil
    }
    
    func categoryAt(_ index: Int) -> Wallet.Category? {
        return categories[safe: index]
    }
    
    func appendCategory(_ index: Int) {
        onComplete?()
        if let currentIndex = selectedCategories.firstIndex(where: { $0.id == categories[safe: index]?.id }) {
            selectedCategories.remove(at: currentIndex)
            return
        }
        
        selectedCategories.append(categories[index])
    }
    
    func isCategorySelectedAt(_ index: Int) -> Bool {
        return selectedCategories.contains(where: { $0.id == categories[index].id })
    }
    
    @objc func onDealUpdated(_ notification: NSNotification) {
        guard let deal = notification.object as? Wallet.Deal else { return }
        
        if let groupIndex = carouselList.firstIndex(where: { $0.deals?.first(where: { $0.id == deal.id })?.id == deal.id }),
           let index = carouselList[safe: groupIndex]?.deals?.firstIndex(where: { $0.id == deal.id }) {
            
            carouselList[groupIndex].deals?[index] = deal
            onComplete?()
        }
    }
    
    @objc func onUpdateFavoriteDeal(_ notification: NSNotification) {
        if let deal = notification.object as? Wallet.Deal,
            let index = self.carouselList.firstIndex(where: { $0.type == .favorite }) {
            var faveDeals = self.carouselList[safe: index]?.deals ?? []

            if deal.isFavorite == true {
                faveDeals.append(deal)
            } else {
                faveDeals.removeAll(where: { $0.id == deal.id })
            }

            self.carouselList[index].deals = faveDeals
            
            handleFavoritesDealByCategories()

            self.onComplete?()
        } else {
            getDeals(false)
        }
    }
    
    @objc func onDealDeletion(_ notification: NSNotification) {
        guard let deal = notification.object as? Wallet.Deal else { return }
        
        if let groupIndex = carouselList.firstIndex(where: { $0.deals?.first(where: { $0.id == deal.id })?.id == deal.id }),
           let index = carouselList[safe: groupIndex]?.deals?.firstIndex(where: { $0.id == deal.id }) {
            
            carouselList[groupIndex].deals?.remove(at: index)
            onComplete?()
        }
    }
    
    @objc func handleFavoritesDealByCategories() {
        guard !selectedCategories.isEmpty else { return }
        
        if let favoriteCarouselIndex = carouselList.firstIndex(where: { $0.type == .favorite }) {
            let deals = carouselList[favoriteCarouselIndex].deals?.filter({ item in selectedCategories.contains(where: { item2 in item.vendorSummary?.category?.id == item2.id  }) == true }) ?? []
            
            carouselList[favoriteCarouselIndex].deals = deals
        }
    }
    
    func searchDeals(with keyword: String, lat: Double? = nil, lng: Double? = nil) {
        onStartLoading?()
        
        let request = SearchRequest(keyword: keyword, lat: lat, lng: lng)
        
        repository.searchDeals(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                let searchResVM = SearchResultViewModel(repository: self.repository,
                                                        data: data,
                                                        keyword: keyword,
                                                        fromWallet: true)
                self.onSearchComplete?(searchResVM)
            case .failure(let error):
                self.onErrorWithMessage?(error.localizedDescription)
            }
        })
    }
    
    func deleteDealAt(_ index: Int, groupIndex: Int) {
        guard let deal = carouselList[safe: groupIndex]?.deals?[safe: index] else { return }
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.deleteDeal(request: request, completionHandler: { [weak self] (succeed, message) in
            guard let self = self else { return }
            if succeed {
                self.getDeals()
                return
            }
            
            self.onErrorWithMessage?(message ?? "Failed")
        })
    }
    
    func carouselTripAt(_ index: Int) -> Wallet.DataClass.Carousel.TripObject? {
        return carouselList[safe: index]?.trip
    }
}

// Deals Parent Cell
extension HomeWalletViewModel: DealsParentProtocol {
    func dealTypeTitleAt(_ index: Int) -> String {
        let carousel = carouselList[safe: index]
        return (carousel?.type == .trip ? carousel?.name : carousel?.title) ?? "No Data"
    }
    
    func listCountAt(_ index: Int) -> Int {
        return carouselList[safe: index]?.deals?.count ?? 0
    }
    
    func imageURLStringOfDealTypeLogoAt(_ index: Int) -> String {
        return carouselList[safe: index]?.icon ?? "NO DATA"
    }
    
    func tripDateAt(_ index: Int) -> String? {
        if let dateFrom = carouselList[safe: index]?.fromDate?.toDate(format: "yyyy-MM-dd"),
           let dateTo = carouselList[safe: index]?.toDate?.toDate(format: "yyyy-MM-dd") {
            
            return "\(dateFrom.toString(format: "dd MMM yyyy")) - \(dateTo.toString(format: "dd MMM yyyy"))"
        }
        
        return nil
    }
}

// Deal Cell
extension HomeWalletViewModel: WalletDealProtocol {
    func imageURLStringOfDealAt(_ index: Int, groupIndex: Int) -> String {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.photo ?? "NO DATA"
    }
    
    func vendorTitleAt(_ index: Int, groupIndex: Int) -> String {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.vendorSummary?.name ?? "NO DATA"
    }
    
    func detailsAt(_ index: Int, groupIndex: Int) -> String {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.name ?? "NO DATA"
    }
    
    func locationAt(_ index: Int, groupIndex: Int) -> String {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.vendorSummary?.fullAddress ?? "NO DATA"
    }
    
    func distanceAt(_ index: Int, groupIndex: Int) -> String {
        guard let location = carouselList[safe: groupIndex]?.deals?[safe: index]?.vendorSummary?.location, let lat = location.lat, let lng = location.lng, lat != 0, lng != 0 else { return "" }
        let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: lat, endLong: lng)
    
        return "\(String(format: "%.1f", tDistance/1000))km"
    }
    
    func imageURLStringOfVendorLogoAt(_ index: Int, groupIndex: Int) -> String {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.vendorSummary?.logoURL ?? "NO DATA"
    }
    
    func dealTypeAt(_ index: Int, groupIndex: Int) -> DealType {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.type ?? .basic
    }
    
    func carouselDealTypeAt(_ index: Int) -> DealType {
        return carouselList[safe: index]?.type ?? .basic
    }
    
    func generateDealDetailVM(_ index: Int, groupIndex: Int) throws -> DealDetailViewModel {
        guard let deal = carouselList[safe: groupIndex]?.deals?[safe: index] else {
            throw ViewModelError.failToGenerate
        }
        
        return DealDetailViewModel(repository: repository, deal: deal)
    }
    
    func generateDealsVM(_ index: Int) -> DealsViewModel {
        return DealsViewModel(repository: repository,
                              dealType: carouselDealTypeAt(index),
                              trip: carouselTripAt(index))
    }
    
    func isFavoriteAt(_ index: Int, groupIndex: Int) -> Bool {
        return carouselList[safe: groupIndex]?.deals?[safe: index]?.isFavorite ?? false
    }
    
    func deleteButtonShouldHide(_ index: Int) -> Bool {
        return carouselList[safe: index]?.type == .favorite || carouselList[safe: index]?.type == .basic || carouselList[safe: index]?.type == .loyalty
    }
    
    func validityAt(_ index: Int, groupIndex: Int) -> String {
        guard let deal = carouselList[safe: groupIndex]?.deals?[safe: index], let expiryTime = deal.expiryTime else { return "" }
        return "Valid until \(expiryTime.toString(format: "dd MMM yyyy"))"
    }
    
    func setTripPreferencesAt(_ index: Int) {
        guard let carousel = carouselList[safe: index],
              carousel.type == .trip,
              let tripObject = carousel.trip else { return }
        
        let selectedTravellingWith = self.trip?.travellingWithList.first(where: { tripObject.travellingWith == $0.id })
        let selectedInterestedIn = self.trip?.interestedInList.filter { tripObject.interestedIn.contains($0.id) } ?? []
        let selectedGoingFor = self.trip?.goingForList.first(where: { $0.id == tripObject.goingFor })

        let wishes = tripObject.wishes?.compactMap { obj -> CreateWishRequest? in
            if let category = CacheManager.shared.infoTarget?.data.categories.first(where: { $0.id == obj.categoryId }) {
                return CreateWishRequest(userId: CacheManager.shared.currentUser?.id ?? "", category: category, name: obj.name)
            }
            
            return nil
        } ?? []
        
        tripPreferences = TripPreferences(id: carousel.tripID,
                                          name: tripObject.name ?? "",
                                          address: tripObject.address ?? "",
                                          lat: tripObject.lat ?? 0.0,
                                          lng: tripObject.lng ?? 0.0,
                                          fromDate: tripObject.fromDate ?? "",
                                          toDate: tripObject.toDate ?? "",
                                          travellingWith: selectedTravellingWith!,
                                          interestedIn: selectedInterestedIn,
                                          goingFor: selectedGoingFor!,
                                          wishList: wishes)
    }
    
    func deleteTripAt(_ index: Int, completion: @escaping GenericCallback<CustomAlertObject?>) {
        guard carouselDealTypeAt(index) == .trip, let trip = carouselList[safe: index], let tripId = trip.tripID else { return }
        
        onStartLoading?()
        
        let request = DeleteTripRequest(tripId: tripId)
        
        repository.deleteTrip(request: request) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let resp):
                let object = CustomAlertObject(success: resp.status,
                                               message: resp.message ?? "",
                                               titleBtnConfirm: LOCALIZED.txt_ok_title.translate,
                                               titleBtnCancel: nil)
                completion(object)
            case .failure(let error):
                completion(nil)
            }
            
            self.onComplete?()
        }
    }
}

// Interested
extension HomeWalletViewModel: InterestedProtocol {
    func imageURLStringOfCategoryAt(_ index: Int) -> String {
        return categories[safe: index]?.imageURLStyle3 ?? ""
    }
    
    func nameOfCategoryAt(_ index: Int) -> String {
        return categories[safe: index]?.name ?? "NO DATA"
    }
}

extension HomeWalletViewModel { // Plan your trip methods
    func getTripPreferences(completionHandler: @escaping ((Trip) -> Void)) {
        onStartLoading?()
        
        repository.getTripPreferences { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let trip):
                self.trip = trip
                guard let travelWithFirstElem = trip.travellingWithList.first,
                      let interestedInFirstElem = trip.interestedInList.first,
                      let goingForFirstElem = trip.goingForList.first else { return }
                let pref = TripPreferences(travellingWith: travelWithFirstElem,
                                           interestedIn: [interestedInFirstElem],
                                           goingFor: goingForFirstElem)
                self.tripPreferences = pref
                completionHandler(trip)
            case .failure(let error): self.onErrorWithMessage?(error.message)
            }
        }
    }
    
    // Google places
    func getPlaceAutocomplete(_ keyword: String, completion: @escaping GenericCallback<[PlacePredictionResponse.Prediction]>) {
        let request = PlaceRequest(input: keyword)
        
        autoCompleteRequest =  repository.getPlacesAutoComplete(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let predictions):
                completion(predictions)
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    func getPlaceDetails(_ id: String) {
        onStartLoading?()
        
        let request = PlaceRequest(placeID: id)
        repository.getPlaceDetails(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let res):
                self.tripPreferences?.address = res.formattedAddress
                self.tripPreferences?.lat = res.geometry.location.lat
                self.tripPreferences?.lng = res.geometry.location.lng
            case .failure(let error):
                print(error.localizedDescription)
            }
            
            self.onComplete?()
        })
    }
    
    func saveTrip(completion: @escaping GenericCallback<CustomAlertObject>) {
        guard let request = tripPreferences?.toRequest() else { return }
    
        onStartLoading?()
        repository.saveTrip(request: request, completionHandler: { result in
            switch result {
            case .success(let response):
                let object = CustomAlertObject(success: response.status,
                                               message: response.message ?? "",
                                               titleBtnConfirm: LOCALIZED.txt_ok_title.translate,
                                               titleBtnCancel: nil)
                completion(object)
            case .failure(let error):
                let object = CustomAlertObject(success: false,
                                               message: error.message,
                                               titleBtnConfirm: LOCALIZED.txt_ok_title.translate,
                                               titleBtnCancel: nil)
                completion(object)
            }
            
            self.onComplete?()
        })
    }
    
    func appendWishInTripPref(index: Int, name: String) {
        guard let cat = categories[safe: index] else { return }
        let wish = CreateWishRequest(userId: CacheManager.shared.currentUser?.id ?? "", category: cat, name: name)
        tripPreferences?.wishList.append(wish)
    }
}

//{“name”:“india”,“address”:“India”,“lat”:20.593684,“lng”:78.96288,“from_date”:“2021-01-28",“to_date”:“2021-01-30",“travelling_with”:“72dd829e-bb68-4dbc-ba96-b473fe6392dc”,“interested_in”:“6d5dc2f4-a7a9-4081-8fed-eabed06d007d,4cfa4b7c-daca-4cc4-ad87-50d4de3c0a03,ecde8ecd-2a53-47c3-9d5f-a2e0d9328af5",“going_for”:“b1384a13-982d-4360-85cc-9c07f4ac6b71"}
