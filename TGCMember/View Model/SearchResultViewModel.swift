//
//  SearchResultViewModel.swift
//  TGCMember
//
//  Created by jonas on 1/21/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

class SearchResultViewModel: BaseViewModel {
    
    private var data: SearchWallet.DataClass!
    var numberOfDeals: Int { data.deals?.count ?? 0 }
    private var deals: [Wallet.Deal] { data.deals ?? [] }
    var totalSearchResultCount: Int { deals.count } // FIXME: - Should be totalRecord of data
    var keyword: String!
    var isPaginating: Bool = false
    private var fromWallet: Bool = false
    
    convenience init(repository: RepositoryProtocol, data: SearchWallet.DataClass, keyword: String, fromWallet: Bool = false) {
        self.init(repository: repository)
        // FIXME: - Determine how to filter deals from backend
        self.fromWallet = fromWallet
        
        var deals = data.deals?.filter({ ($0.usable && !$0.isExpired) || $0.isFavorite == true  })
        
        if fromWallet {
            deals = deals?.filter({
                if $0.type == .basic, $0.isFavorite != true {
                    return false
                }
                
                return true
            })
        }
        
        var newData = data
        
        if fromWallet {
//            let list = Dictionary(grouping: deals ?? [], by: { $0.vendorSummary?.location?.hashValue ?? 0 })
//            let sortedList = list.compactMap({ $0.value.sorted(by: { ($0.type?.orderValue ?? 0) > ($1.type?.orderValue ?? 0)
//            }) })
            
            newData.deals = deals?.sorted(by: { ($0.type?.orderValue ?? 0) > ($1.type?.orderValue ?? 0) })//sortedList.flatMap { $0 }
        } else {
            newData.deals = deals?.sorted(by: { ($0.type?.orderValue ?? 0) > ($1.type?.orderValue ?? 0) })
        }
        
        self.data = newData
        self.keyword = keyword
    }
    
    func generateDealDetailVM(_ index: Int) throws -> DealDetailViewModel {
        guard let deal = data.deals?[safe: index] else {
            throw ViewModelError.failToGenerate
        }
        
        return DealDetailViewModel(repository: repository, deal: deal)
    }
    
    func paginateDeals() {
        let nextPage = data.nextPage
        guard nextPage <= data.totalPage, nextPage > data.page else {
            self.onComplete?()
            return
        }
        
        let request = SearchRequest(page: data.page + 1, keyword: keyword)
        
        repository.searchDeals(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(var data):
                data.deals = data.deals?.filter({ ($0.usable && !$0.isExpired) || $0.isFavorite == true  })
                
                if self.fromWallet {
                    data.deals = data.deals?.filter({
                        if $0.type == .basic, $0.isFavorite != true {
                            return false
                        }
                        
                        return true
                    })
                }
                
                if self.fromWallet {
                    let list = Dictionary(grouping: data.deals ?? [], by: { $0.vendorSummary?.location?.hashValue ?? 0 })
                    let sortedList = list.compactMap({ $0.value.sorted(by: { ($0.type?.orderValue ?? 0) > ($1.type?.orderValue ?? 0)
                    }) })
                    
                    data.deals = sortedList.flatMap { $0 }
                }
                
                data.prepend(self.deals)
                self.data = data
            case .failure(let error):
                self.onErrorWithMessage?(error.localizedDescription)
            }
            
            self.isPaginating = false
            self.onComplete?()
        })
    }
    
    func isLastDeal(with index: Int) -> Bool {
        return (numberOfDeals - 1) == index
    }
    
    func deleteDealAt(_ index: Int) {
        guard let deal = deals[safe: index] else { return }
        
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.deleteDeal(request: request, completionHandler: { [weak self] (succeed, message) in
            guard let self = self else { return }
            if succeed {
                self.data.deals?.removeAll(where: { $0.id == deal.id })
                self.onComplete?()
                return
            }
            
            self.onErrorWithMessage?(message ?? "Failed")
        })
    }
}

extension SearchResultViewModel: SearchResultProtocol {
    func vendorNameAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.name ?? ""
    }
    
    func imageURLStringAt(_ index: Int) -> String {
        return deals[safe: index]?.photo ?? ""
    }
    
    func detailAt(_ index: Int) -> String {
        return deals[safe: index]?.name ?? ""
    }
    
    func addressAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.fullAddress ?? ""
    }
    
    func distanceAt(_ index: Int) -> String {
        guard let location = deals[safe: index]?.vendorSummary?.location else { return "no data" }
        let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: location.lat ?? 0, endLong: location.lng ?? 0)
    
        return "\(String(format: "%.1f", tDistance/1000))km"
    }
    
    func validityStringAt(_ index: Int) -> (String, Bool) {
        guard let deal = deals[safe: index] else { return ("NO DATA", false) }
        let expiryTime = deal.expiryTime
        let isExpired = deal.expiryTime?.isInThePast
        
        let dateString = (deal.dateUsed ?? expiryTime)?.toString(format: "dd MMM, yyyy") ?? ""
        var string: String = ""
    
        if deal.dateUsed != nil {
            string = LOCALIZED.used_on_format.translate
        } else if isExpired ?? false {
            string = LOCALIZED.expired_on_format.translate
        } else {
            if deal.dateUsed != nil || expiryTime != nil {
                string = LOCALIZED.valid_until_format.translate
            }
        }
        
        string = string.replacingOccurrences(of: "[%__TAG_DATE__%]", with: dateString)
        
        return (string, ((isExpired ?? false) && deal.dateUsed == nil))
    }
    
    func promotionTextAt(_ index: Int) -> String {
        return deals[safe: index]?.promotionText ?? ""
    }
    
    func imageURLStringOfVendorLogoAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.logoURL ?? ""
    }
    
    func dealTypeAt(_ index: Int) -> DealType {
        return deals[safe: index]?.type ?? .basic
    }
    
    func isBlackOrBasicTypeAt(_ index: Int) -> Bool {
        return deals[safe: index]?.isWhiteFont ?? false
    }
}
