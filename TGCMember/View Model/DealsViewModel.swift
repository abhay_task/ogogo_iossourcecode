//
//  DealsViewModel.swift
//  TGCMember
//
//  Created by jonas on 1/22/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation

enum DealActivityType: String {
    case active
    case used
    case expired
    case all
    
    var tag: Int {
        switch self {
        case .active, .all: return 0
        case .used: return 1
        case .expired: return 2
        }
    }
    
    static func initWith(string: String) -> DealActivityType? {
        switch string {
        case "Active deals".lowercased(): return .active
        case "Expired deals".lowercased(): return .expired
        case "Used deals".lowercased(): return .used
        default: return nil
        }
    }
    
    var titleStringValue: String {
        switch self {
        case .active: return "Active Deals"
        case .expired: return "Expired Deals"
        case .used: return "Used Deals"
        default: return ""
        }
    }
}

class DealsViewModel: BaseViewModel {
    private var activeData: SearchWallet.DataClass?
    private var usedData: SearchWallet.DataClass?
    private var expiredData: SearchWallet.DataClass?
    
    var dealType: DealType!
    private var trip: Wallet.DataClass.Carousel.TripObject?
    
    var activeDeals: [Wallet.Deal] {
        activeData?.deals ?? []
    }
    
    var expiredDeals: [Wallet.Deal] {
        expiredData?.deals ?? []
    }
    
    var usedDeals: [Wallet.Deal] {
        usedData?.deals ?? []
    }
    
    var numberOfActiveDeals: Int { activeDeals.count }
    var numberOfExpiredDeals: Int { expiredDeals.count }
    var numberOfUsedDeals: Int { usedDeals.count }
    
    var currentActivity: DealActivityType = .active {
        didSet {
            onComplete?()
            switch currentActivity {
            case .active, .all:
                if activeData == nil {
                    getDeals()
                }
            case .expired:
                if expiredData == nil {
                    getDeals()
                }
            case .used:
                if usedData == nil {
                    getDeals()
                }
            }
        }
    }
    
    var deals: [Wallet.Deal] {
        switch currentActivity {
        case .active, .all: return activeDeals
        case .expired: return expiredDeals
        case .used: return usedDeals
        }
    }
    
    var isPaginating: Bool = false
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    convenience init(repository: RepositoryProtocol, dealType: DealType, trip: Wallet.DataClass.Carousel.TripObject? = nil) {
        self.init(repository: repository)
        self.dealType = dealType
        self.trip = trip
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateDeal(_:)), name: .REPOSITION_DEAL, object: nil)
    }
    
    @objc func updateDeal(_ notification: NSNotification) {
        guard let deal = notification.object as? Wallet.Deal else { return }
        
        var currentIndex: Int?
        
        if let activeDeals = activeData?.deals,
           let index = activeDeals.firstIndex(where: { $0.instanceDealID == deal.instanceDealID }) {
            if deal.usable {
                currentIndex = index
            }
            activeData?.deals?.remove(at: index)
        }
        
        if let usedDeals = usedData?.deals,
           let index = usedDeals.firstIndex(where: { $0.instanceDealID == deal.instanceDealID }) {
            if !deal.usable {
                currentIndex = index
            }
            usedData?.deals?.remove(at: index)
        }
        
        if let expiredDeals = expiredData?.deals,
           let index = expiredDeals.firstIndex(where: { $0.instanceDealID == deal.instanceDealID }) {
            currentIndex = index
            expiredData?.deals?.remove(at: index)
        }
        
        if deal.isExpired, deal.usable {
            expiredData?.deals?.insert(deal, at: currentIndex ?? 0)
        } else if deal.usable {
            activeData?.deals?.insert(deal, at: currentIndex ?? 0)
        } else {
            usedData?.deals?.insert(deal, at: currentIndex ?? 0)
        }
        
        self.onComplete?()
    }
    
    func getDeals() {
        guard !isLoading else { return }
        
        let nextPage: Int
        
        var data: SearchWallet.DataClass? {
            switch currentActivity {
            case .active, .all: return activeData
            case .expired: return expiredData
            case .used: return usedData
            }
        }
        
        if let data = data {
            nextPage = data.nextPage
            
            guard nextPage <= data.totalPage, nextPage > data.page else {
                onComplete?()
                return
            }
        } else {
            nextPage = 1
            isLoading = true
            onStartLoading?()
        }
        
        let request = SearchRequest(type: dealType, page: nextPage, activity: trip != nil ? .all : currentActivity, tripID: trip?.id)
        
        repository.searchDeals(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(var data):
                self.setData(&data)
            case .failure(let error):
                self.onErrorWithMessage?(error.localizedDescription)
            }
            
            self.isPaginating = false
            self.isLoading = false
            self.onComplete?()
        })
    }
    
    private
    func setData(_ data: inout SearchWallet.DataClass) {
        switch currentActivity {
        case .active, .all:
            data.prepend(self.activeDeals)
            if trip == nil {
                data.deals = (data.deals?.filter { ($0.usable && !$0.isExpired) && ($0.type != .loyalty && $0.type != .basic) })?.filter({ $0.vendorSummary?.name?.lowercased().contains("test") == false })
            } else {
                data.deals = (data.deals?.filter { $0.usable && !$0.isExpired }.filter{ $0.tripInfo?.name == trip?.name } ?? []).filter({ $0.vendorSummary?.name?.lowercased().contains("test") == false })
            }
            self.activeData = data
        case .expired:
            data.prepend(self.expiredDeals)
            data.deals = (data.deals?.filter { $0.isExpired  } ?? []).filter({ $0.vendorSummary?.name?.lowercased().contains("test") == false })
            self.expiredData = data
        case .used:
            data.prepend(self.usedDeals)
            data.deals = (data.deals?.filter { $0.dateUsed != nil } ?? []).filter({ $0.vendorSummary?.name?.lowercased().contains("test") == false })
            self.usedData = data
        }
    }
    
    func navTitleText() -> String {
        return trip?.name ?? "\(dealType.rawValue.upperCaseFirstLetter()) Deals"
    }
    
    func generateDealDetailVM(_ index: Int) throws -> DealDetailViewModel {
        var data: SearchWallet.DataClass? {
            switch currentActivity {
            case .active, .all: return activeData
            case .expired: return expiredData
            case .used: return usedData
            }
        }
        
        guard let deal = data?.deals?[safe: index] else {
            throw ViewModelError.failToGenerate
        }
        
        return DealDetailViewModel(repository: repository, deal: deal)
    }
    
    func isLastDeal(_ index: Int) -> Bool {
        var numberOfDeals: Int {
            switch currentActivity {
            case .active, .all: return numberOfActiveDeals
            case .expired: return numberOfExpiredDeals
            case .used: return numberOfUsedDeals
            }
        }
        
        return (numberOfDeals - 1) == index
    }
    
    func deleteDealAt(_ index: Int) {
        guard let deal = deals[safe: index] else { return }
        
        let request = IDRequest(id: deal.instanceDealID ?? "")
        
        onStartLoading?()
        
        repository.deleteDeal(request: request, completionHandler: { [weak self] (succeed, message) in
            guard let self = self else { return }
            if succeed {
                switch self.currentActivity {
                case .active, .all:
                    self.activeData?.deals?.removeAll(where: { $0.id == deal.id })
                case .expired:
                    self.expiredData?.deals?.removeAll(where: { $0.id == deal.id })
                case .used:
                    self.usedData?.deals?.removeAll(where: { $0.id == deal.id })
                }
                self.onComplete?()
                return
            }
            
            self.onErrorWithMessage?(message ?? "Failed")
        })
    }
}

extension DealsViewModel: DealTableProtocol {
    func vendorNameAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.name ?? ""
    }
    
    func imageURLStringAt(_ index: Int) -> String {
        return deals[safe: index]?.photo ?? ""
    }
    
    func detailAt(_ index: Int) -> String {
        return deals[safe: index]?.name ?? ""
    }
    
    func addressAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.fullAddress ?? ""
    }
    
    func validityStringAt(_ index: Int) -> (String, Bool) {
        guard let deal = deals[safe: index] else { return ("NO DATA", false) }
        let expiryTime = deal.expiryTime
        
        let dateString = (deal.dateUsed ?? expiryTime)?.toString(format: "dd MMM, yyyy") ?? ""
        var string: String = ""
    
        if deal.dateUsed != nil {
            string = LOCALIZED.used_on_format.translate
        } else if deal.isExpired {
            string = LOCALIZED.expired_on_format.translate
        } else {
            string = LOCALIZED.valid_until_format.translate
        }
        
        string = string.replacingOccurrences(of: "[%__TAG_DATE__%]", with: dateString)
        
        return (string, (deal.isExpired && deal.dateUsed == nil))
    }
    
    func distanceAt(_ index: Int) -> String {
        guard let location = deals[safe: index]?.vendorSummary?.location else { return "no data" }
        let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: location.lat ?? 0, endLong: location.lng ?? 0)
    
        return "\(String(format: "%.1f", tDistance/1000)) km"
    }
    
    func promotionTextAt(_ index: Int) -> String {
        return deals[safe: index]?.promotionText ?? ""
    }
    
    func imageURLStringOfVendorLogoAt(_ index: Int) -> String {
        return deals[safe: index]?.vendorSummary?.logoURL ?? ""
    }
    
    func dealTypeAt(_ index: Int) -> DealType {
        return deals[safe: index]?.type ?? .basic
    }
    
    func isBlackOrBasicTypeAt(_ index: Int) -> Bool {
        return deals[safe: index]?.isWhiteFont ?? false
    }
}
