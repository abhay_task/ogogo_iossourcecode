//
//  MyProfileViewModel.swift
//  TGCMember
//
//  Created by jonas on 12/9/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import Foundation

class MyProfileViewModel: BaseViewModel {
    lazy var user: User? = { try? User(dict: userInfo) }()
    lazy var vendor: User.Vendor? = { user?.vendor }()
    var infoTarget: InfoTarget? { CacheManager.shared.infoTarget }
    private let awsManager = AWSManager.shared
    var autoCompleteRequest: TGCDataRequest?
    
    lazy var request: UpdateUserProfileRequest? = {
        return generateUpdateRequest()
    }()
    
    func generateUpdateRequest() -> UpdateUserProfileRequest? {
        let memberTargeting = user?.details ?? User.Details(ageRange: nil,
                                                                  birthYear: 0,
                                                                  education: nil,
                                                                  gender: nil,
                                                                  interestedIn: nil,
                                                                  kid: 0,
                                                                  marriage: nil)
        let gender = infoTarget?.data.genders.first(where: { $0.key == "\(memberTargeting.gender?.value ?? 0)" })
        let childrenCount = memberTargeting.kid
        let displayName = user?.displayName
        let birthYear = memberTargeting.birthYear
        let marriageStatus = infoTarget?.data.marriages.first(where: { $0.key == "\(memberTargeting.marriage?.value ?? 0)" })
        let educationLevel = infoTarget?.data.educationLevels.first(where: { $0.key == "\(memberTargeting.education?.value ?? 0)" })
        let interestedIns = infoTarget?.data.categories.filter({ cat in
                                                                (memberTargeting.interestedIn?.data.contains(where: { item in item.id == cat.id  }) ?? false) }) ?? []
        let homeAddress = memberTargeting.homeAddress
        let nationality = memberTargeting.nationality
        let languages = memberTargeting.languages
        
        let request = UpdateUserProfileRequest(displayName: displayName ?? "",
                                               childrenCount: childrenCount ?? 0,
                                               gender: (gender?.key ?? "", gender?.value ?? ""),
                                               birthYear: birthYear ?? 0,
                                               marriageStatus: (marriageStatus?.key ?? "", marriageStatus?.value ?? ""),
                                               educationLevel: (educationLevel?.key ?? "", educationLevel?.value ?? ""),
                                               categories: interestedIns,
                                               nationality: nationality ?? "",
                                               homeAddress: homeAddress ?? "",
                                               languages: languages ?? "")
        
        return request
    }
    
    func updateMemberTargetingDetails() {
        guard let request = request else { return }
        
        onStartLoading?()
        repository.updateProfile(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let user):
                self.user = user
                Utils.setUserInfo(user.dictionary)
                self.onComplete?()
            case .failure(let error):
                print(error)
                self.onErrorWithMessage?(error.localizedDescription)
            }
        })
    }
    
    func updateAvatar(data: Data) {
        let key = "assets/images/user/avatar_\(UUID().uuidString).jpg"
        
        onStartLoading?()
        awsManager.uploadS3(data: data, name: key, completion: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(_):
                let predictedURLString = self.awsManager.baseURL + key
                let request = UpdateAvatarRequest(urlString: predictedURLString)
                self.repository.updateAvatarURL(request: request, completionHandler: { result in
                    switch result {
                    case .success(_):
                        self.user?.avatarURL = predictedURLString
                        Utils.setUserInfo(self.user.dictionary)
                        self.onComplete?()
                    case .failure(let response):
                        self.onCompleteWithMessage?(response.message)
                    }
                })
            case .failure(let response):
                self.onErrorWithMessage?(response.message)
            }
        })
    }
    
    // Google places
    func getPlaceAutocomplete(_ keyword: String, completion: @escaping GenericCallback<[PlacePredictionResponse.Prediction]>) {
        let request = PlaceRequest(input: keyword)
        
        autoCompleteRequest =  repository.getPlacesAutoComplete(request: request, completionHandler: { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let predictions):
                completion(predictions)
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
}
