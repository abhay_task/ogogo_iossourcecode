//
//  Repository.swift
//  TGCMember
//
//  Created by jonas on 12/2/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func updateProfile(request: UpdateUserProfileRequest,
                       completionHandler: @escaping GenericResult<User, ErrorResponse>)
    func getDeals(request: FilterWalletRequest?, completionHandler: @escaping GenericResult<Wallet, ErrorResponse>)
    func getDealDetails(_ id: String,
                        completionHandler: @escaping GenericResult<Wallet.Deal, ErrorResponse>)
    func toggleFavoriteStatusOfDeal(_ request: IDRequest,
                                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    func searchDeals(request: SearchRequest,
                     completionHandler: @escaping GenericResult<SearchWallet.DataClass, ErrorResponse>)
    func deleteDeal(request: IDRequest, completionHandler: @escaping GenericCallback<(Bool, String?)>)
    func getTripPreferences(completionHandler: @escaping GenericResult<Trip, ErrorResponse>)
    func saveTrip(request: CreateTripRequest,
                  completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    func deleteTrip(request: DeleteTripRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    
    // Google places
    @discardableResult
    func getPlacesAutoComplete(request: PlaceRequest,
                               completionHandler: @escaping GenericResult<[PlacePredictionResponse.Prediction], Error>) -> TGCDataRequest?
    func getPlaceDetails(request: PlaceRequest,
                         completionHandler: @escaping GenericResult<PlaceAPIResponse.Result,Error>)
    
    //Account
    func getInfoTarget(completionHandler: @escaping GenericResult<InfoTarget, ErrorResponse>)
    func updateAvatarURL(request: UpdateAvatarRequest, completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    func getPackages(completionHandler: @escaping GenericResult<PackageResponse, ErrorResponse>)
    
    //Wishlist
    func getWishlist(completionHandler: @escaping GenericResult<WishlistResponse.DataClass.List, ErrorResponse>)
    func createWish(request: CreateWishRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    func deleteWish(request: IDRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    func editWish(request: CreateWishRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>)
    
    //T&C
    func getTermsAndCondtionsData(completionHandler: @escaping GenericCallback<Data>) 
    
    //MAP
    @discardableResult
    func searchDealsOnMap(request: SearchDealsMapRequest,
                          completionHandler: @escaping GenericResult<SearchWallet.DataClass, ErrorResponse>) -> TGCDataRequest?
    func toCodableObject<T: Decodable>(_ model: T.Type,
                                       from fileName: String) -> T? 
    
    func readFile(urlString: String) -> Any?
    
    // FAQs
    func getFaqs(completionHandler: @escaping GenericResult<FAQsResponse, ErrorResponse>)
    
    // MESSAGES
    func getGroupMessages(completionHandler: @escaping GenericResult<GroupMessagesResponse, ErrorResponse>)
}

typealias GenericResult<T: Codable, U: Error> = (Result<T, U>) -> Void
typealias GenericCallback<T> = (T) -> Void
typealias VoidCallback = () -> Void

class Repository: RepositoryProtocol {
    static let shared = Repository()
    private let networkManager = NetworkManager.shared
    
    private init() {}
    
    func updateProfile(request: UpdateUserProfileRequest,
                       completionHandler: @escaping GenericResult<User, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.BASE_UPDATE.rawValue) else { return }
        print(request.parameters)
        networkManager.requestData(url: url, type: UpdateProfileResponse.self, method: .post, parameters: request.parameters, completionHandler: { result in
            switch result {
            case .success(let response): completionHandler(.success(response.data.user))
            case .failure(let error): completionHandler(.failure(.generic(error.localizedDescription)))
            }
        })
    }
    
    func getDeals(request: FilterWalletRequest?, completionHandler: @escaping GenericResult<Wallet, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.DEAL_WALLETS.rawValue) else { return }
        
        networkManager.requestData(url: url, type: Wallet.self, parameters: request?.parameters, completionHandler: {
            completionHandler($0)
        })
    }
    
    func getDealDetails(_ id: String,
                        completionHandler: @escaping GenericResult<Wallet.Deal, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.DEAL_DETAIL.rawValue + id) else { return }
        
        networkManager.requestData(url: url, type: Wallet.DataClass.Carousel.DealDetail.self, completionHandler: { result in
            switch result {
            case .success(let dealDetail):
                completionHandler(.success(dealDetail.deal))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        })
    }
    
    func toggleFavoriteStatusOfDeal(_ request: IDRequest,
                                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.LIKE_DEAL.rawValue) else { return }
        
        networkManager.requestData(url: url,
                                   type: ServerResponse.self,
                                   method: .post,
                                   parameters: request.parameters,
                                   completionHandler: { result in
                                    
                                    switch result {
                                    case .success(let response):
                                        guard response.response != .none else {
                                            completionHandler(.failure(.generic(response.message ?? "")))
                                            return
                                        }
                                        
                                        completionHandler(.success(response))
                                    case .failure(let error):
                                        completionHandler(.failure(.generic(error.localizedDescription)))
                                    }
        })
    }
    
    func searchDeals(request: SearchRequest,
                     completionHandler: @escaping GenericResult<SearchWallet.DataClass, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.DEALS_WALLETS_SEARCH.rawValue) else { return }
        
        networkManager.requestData(url: url, type: SearchWallet.self,
                                   parameters: request.parameters,
                                   completionHandler: { result in
            switch result {
            case .success(let wallet):
                completionHandler(.success(wallet.data))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        })
    }
    
    func deleteDeal(request: IDRequest, completionHandler: @escaping GenericCallback<(Bool, String?)>) {
        guard let url = URL(string: BASE_URL + API_URL.POST_MOVE_TO_TRASH.rawValue) else { return }
        
        networkManager.requestData(url: url,
                                   type: ServerResponse.self,
                                   method: .post,
                                   parameters: request.parameters,
                                   completionHandler: { result in
            switch result {
            case .success(let response):
                completionHandler((response.message == ServerResponse.MessageText.success.rawValue, nil))
            case .failure(let error):
                completionHandler((false, error.localizedDescription))
            }
        })
    }
    
    // Plan your trip
    func getTripPreferences(completionHandler: @escaping GenericResult<Trip, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.LIST_PREFERENCES_ENABLE.rawValue) else { return }
        
        networkManager.requestData(url: url, type: Trip.self, completionHandler: { completionHandler($0) })
    }
    
    @discardableResult
    func getPlacesAutoComplete(request: PlaceRequest,
                               completionHandler: @escaping GenericResult<[PlacePredictionResponse.Prediction], Error>) -> TGCDataRequest? {
        guard let url = URL(string: API_URL.GET_PLACE_AUTOCOMPLETE.rawValue) else { return nil }
        
        return networkManager.gRequestData(url: url, type: PlacePredictionResponse.self ,parameters: request.parameters,completionHandler: { result in
            switch result {
            case .success(let response):
                completionHandler(.success(response.predictions))
            case .failure(let error):
                completionHandler(.failure(error))
                
            }
        })
    }
    
    func getPlaceDetails(request: PlaceRequest,
                         completionHandler: @escaping GenericResult<PlaceAPIResponse.Result,Error>) {
        guard let url = URL(string: API_URL.GET_PLACE_DETAILS.rawValue) else { return }
        
        networkManager.gRequestData(url: url, type: PlaceAPIResponse.self, parameters: request.parameters, completionHandler: { result in
            switch result {
            case .success(let response):
                completionHandler(.success(response.result))
            case .failure(let error):
                completionHandler(.failure(error))
                
            }
        })
    }
    
    func saveTrip(request: CreateTripRequest,
                  completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.CREATE_TRIP.rawValue) else { return }
        
        networkManager.requestData(url: url,
                                   type: ServerResponse.self,
                                   method: .post,
                                   parameters: request.parameters,
                                   completionHandler: { completionHandler($0) })
    }
    
    // Account
    func getInfoTarget(completionHandler: @escaping GenericResult<InfoTarget, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.GET_TARGET_INFO.rawValue) else { completionHandler(.failure(.generic("Something went wrong"))); return }
        
        networkManager.requestData(url: url, type: InfoTarget.self, completionHandler: completionHandler)
    }
    
    func updateAvatarURL(request: UpdateAvatarRequest,
                         completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.UPDATE_AVATAR_ONLY.rawValue) else { return }
        
        networkManager.requestData(url: url, type: ServerResponse.self, method: .post, parameters: request.parameters, completionHandler: completionHandler)
    }
    
    func getPackages(completionHandler: @escaping GenericResult<PackageResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.GET_AVAILABLE_PACKAGES.rawValue) else { return }
        
        networkManager.requestData(url: url, type: PackageResponse.self, completionHandler: completionHandler)
    }
    
    //Wishlist
    func getWishlist(completionHandler: @escaping GenericResult<WishlistResponse.DataClass.List, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.GET_WISHLIST.rawValue + "?limit=1000") else { return } // Remove limit if fixed
        
        networkManager.requestData(url: url, type: WishlistResponse.self, completionHandler: { response in
            switch response {
            case .success(let resp):
                if var list = resp.data?.list {
                    list.records = list.records?.filter({ $0.categoryIDs?.isEmpty == false })
                    completionHandler(.success(list))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        })
    }
    
    func createWish(request: CreateWishRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.CREATE_DELETE_WISHLIST.rawValue) else { return }
        networkManager.requestData(url: url,
                                   type: ServerResponse.self,
                                   method: .post,
                                   parameters: request.parameters,
                                   completionHandler: { completionHandler($0) })
    }
    
    func deleteWish(request: IDRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.CREATE_DELETE_WISHLIST.rawValue + "/\(request.id)") else { return }
        
        networkManager.requestData(url: url, type: ServerResponse.self, method: .delete, completionHandler: { completionHandler($0) })
    }
    
    func editWish(request: CreateWishRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.EDIT_WISH.rawValue.replacingOccurrences(of: "$id", with: request.id ?? "")) else { return }
        networkManager.requestData(url: url,
                                   type: ServerResponse.self,
                                   method: .put,
                                   parameters: request.parameters,
                                   completionHandler: { completionHandler($0) })
    }
    
    //T&C
    func getTermsAndCondtionsData(completionHandler: @escaping GenericCallback<Data>) {
        guard let url = URL(string: BASE_URL + API_URL.TERMS_AND_CONDITIONS.rawValue) else { return }
        networkManager.requestData(with: url, completionHandler: { data, error in
            if let data = data {
                completionHandler(data)
                return
            }
            
            print(error)
        })
    }
    
    // MAP
    @discardableResult
    func searchDealsOnMap(request: SearchDealsMapRequest,
                          completionHandler: @escaping GenericResult<SearchWallet.DataClass, ErrorResponse>) -> TGCDataRequest? {
        guard let url = URL(string: BASE_URL + API_URL.GET_DEALS_ON_MAP.rawValue) else { return nil }
        
        let dataRequest = networkManager.requestData(url: url, type: SearchWallet.self, parameters: request.parameters, completionHandler: { result in
            switch result {
            case .success(let searchResult):
                completionHandler(.success(searchResult.data))
            case .failure(let errorResponse):
                completionHandler(.failure(errorResponse))
            }
        })
        
        return dataRequest
    }
    
    func deleteTrip(request: DeleteTripRequest,
                    completionHandler: @escaping GenericResult<ServerResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.DELETE_TRIP.rawValue) else { return }
        
        networkManager.requestData(url: url, type: ServerResponse.self, method: .post, parameters: request.parameters) { result in
            switch result {
            case .success(let resp):
                completionHandler(.success(resp))
            case .failure(let errorResponse):
                completionHandler(.failure(errorResponse))
            }
        }
    }
    
    func getFaqs(completionHandler: @escaping GenericResult<FAQsResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.GET_FAQS.rawValue) else { return }
        
        networkManager.requestData(url: url, type: FAQsResponse.self, completionHandler: completionHandler)
    }
    
    func getGroupMessages(completionHandler: @escaping GenericResult<GroupMessagesResponse, ErrorResponse>) {
        guard let url = URL(string: BASE_URL + API_URL.GET_GROUP_CHATS.rawValue) else { return }
        
        networkManager.requestData(url: url, type: GroupMessagesResponse.self, completionHandler: completionHandler)
    }
}

enum ErrorResponse: Error {
    case generic(String)
    
    var message: String {
        switch self {
        case .generic(let text):
            return text
        default:
            return ""
        }
    }
}

extension Repository {
    func readFile(urlString: String) -> Any? {
        if let url = URL(string: "file://\(urlString)") {
            
            do {
                let data = try Data(contentsOf: url)
                
                let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                
                return dict
                
            } catch (let e) {
                print(e)
                return nil
            }
        }
        
        return nil
    }
    
    func toCodableObject<T: Decodable>(_ model: T.Type,
                                       from fileName: String) -> T? {
        do {
            guard let pathUrl = Bundle.main.url(forResource: fileName, withExtension: "json") else { return nil }
            
            let jsonString = try String(contentsOf: pathUrl, encoding: .utf8)
            let data = jsonString.data(using: .utf8) ?? Data()
            
            let decoder = JSONDecoder()
            let objects = try decoder.decode(model.self, from: data)
            
            return objects
        } catch {
            print(error)
            return nil
        }
    }
}
