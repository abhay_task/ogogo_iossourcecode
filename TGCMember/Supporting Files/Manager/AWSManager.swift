//
//  AWSManager.swift
//  TGCMember
//
//  Created by jonas on 2/15/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import Foundation
import AWSS3

class AWSManager {
    static let shared = AWSManager()
    private let cacheManager = CacheManager.shared
    private init() {
        
    }
    
    let baseURL = "https://dev-gcc.s3-ap-southeast-1.amazonaws.com/"
    
    func uploadS3(data: Data,
                  name: String,
                  completion: @escaping GenericResult<String, ErrorResponse>) {
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: cacheManager.accessKey,
                                                               secretKey: cacheManager.secretKey)
        let configuration = AWSServiceConfiguration(region: .APSoutheast1,
                                                    credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        let expression = AWSS3TransferUtilityMultiPartUploadExpression()
        expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        
        AWSS3TransferUtility.default().uploadUsingMultiPart(data: data,
                                                            bucket: cacheManager.bucketName,
                                                            key: name,
                                                            contentType: "image/png",
                                                            expression: expression) { task, error in
            if let error = error {
                let response = ErrorResponse.generic(error.localizedDescription)
                completion(.failure(response))
                return
            }
            
            completion(.success("Success"))
        }.continueWith { task -> AnyObject? in
            if let error = task.error {
                let response = ErrorResponse.generic(error.localizedDescription)
                completion(.failure(response))
                return nil
            }
            
            print(task)
//            completion(.success("Success"))
            return nil
        }
    }
}

//if let data = #imageLiteral(resourceName: "remove_deal").jpegData(compressionQuality: 0.3) {
//    uploadS3(data: data, name: "assets/images/user/avatar_\(UUID().uuidString).jpg", progressHandler: {
//        print($0.fileURL)
//    }, completionHandler: { error in
//        print(error?.localizedDescription)
//    })
//}
