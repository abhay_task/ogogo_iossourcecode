//
//  CacheManager.swift
//  TGCMember
//
//  Created by jonas on 12/5/20.
//  Copyright © 2020 gkim. All rights reserved.
//
import Foundation

class CacheManager {
    // MARK: - Variables
    private let defaults = UserDefaults.standard
    static let shared = CacheManager()
    
    private let currentUserKey = "currentUser"
    private let infoTargetKey = "infoTarget"
    private let currentPlanKey = "currentPlan"
    // AWS
    private let accessKeyKey = "accessKey"
    private let secretKeyKey = "secretKey"
    private let bucketNameKey = "bucketNameKey"
    
    private let wishlistOpenedKey = "wishlistOpened"
    private let recentSearchesKey = "recentSearches"
    private let termsAndConditionsAcceptedKey = "termsAndConditionsAccepted"
    private let locationShakeDateTimeShownDictKey = "locationShakeDateTimeShownDict"
    private let mapFilterTypeKey = "mapFilterType"
    
    //Starter and daily deals booleans
    private let starterShowDateKey = "starterShowDate"
    private let dailyShowDateKey = "dailyShowDate"
    
    // Language
    private let currentLanguageKey = "currentLanguage"
    
    // Pinned conversations
    private let pinnedConversationIDsKey = "pinnedConversationIDs"
    
    private var encoder: JSONEncoder {
        return JSONEncoder()
    }
    
    private var decoder: JSONDecoder {
        return JSONDecoder()
    }
    
    var currentUser: User? {
        get {
            if let data = defaults.data(forKey: currentUserKey) {
                let user = try? decoder.decode(User.self, from: data)
                return user
            }
            
            return nil
        }
        
        set {
            if let data = try? encoder.encode(newValue) {
                defaults.set(data, forKey: currentUserKey)
                defaults.synchronize()
            }
        }
    }
    
    var infoTarget: InfoTarget? {
        get {
            if let data = defaults.data(forKey: infoTargetKey) {
                let infoTarget = try? decoder.decode(InfoTarget.self, from: data)
                return infoTarget
            }
            
            return nil
        }
        
        set {
            if let data = try? encoder.encode(newValue) {
                defaults.set(data, forKey: infoTargetKey)
                defaults.synchronize()
            }
        }
    }
    
    var currentPlan: PackageResponse.Package? {
        get {
            if let data = defaults.data(forKey: currentPlanKey) {
                let package = try? decoder.decode(PackageResponse.Package.self, from: data)
                return package
            }
            
            return nil
        }
        
        set {
            if let data = try? encoder.encode(newValue) {
                defaults.set(data, forKey: currentPlanKey)
                defaults.synchronize()
            }
        }
    }
    
    // MARK
    var accessKey: String {
        get {
            let string = defaults.string(forKey: accessKeyKey)
            return string ?? defAccessKey
        }
        
        set {
            defaults.set(newValue, forKey: accessKeyKey)
            defaults.synchronize()
        }
    }
    
    var secretKey: String {
        get {
            let string = defaults.string(forKey: secretKeyKey)
            return string ?? defSecretKey
        }
        
        set {
            defaults.set(newValue, forKey: secretKeyKey)
            defaults.synchronize()
        }
    }
    
    var bucketName: String {
        get {
            let string = defaults.string(forKey: bucketNameKey)
            return string ?? defBucketName
        }
        
        set {
            defaults.set(newValue, forKey: bucketNameKey)
            defaults.synchronize()
        }
    }
    
    var wishlistOpened: Bool {
        get {
            let bool = defaults.bool(forKey: wishlistOpenedKey)
            return bool
        }
        
        set {
            defaults.set(newValue, forKey: wishlistOpenedKey)
            defaults.synchronize()
        }
    }
    
    var recentSearches: [String] {
        get {
            if let array = defaults.array(forKey: recentSearchesKey) as? [String] {
                return array
            }
            
            return []
        }
        
        set {
            defaults.set(newValue, forKey: recentSearchesKey)
            defaults.synchronize()
        }
    }
    
    // Use this
    func appendRecentSearches(with text: String) {
        guard !recentSearches.contains(text) else { return }
        recentSearches.append(text)
    }
    
    var termsAndConditionsAccepted: Bool? {
        get {
            if let bool = defaults.object(forKey: termsAndConditionsAcceptedKey) as? Bool {
                return bool
            }
            
            return nil
        }
        
        set {
            defaults.set(newValue, forKey: termsAndConditionsAcceptedKey)
            defaults.synchronize()
        }
    }
    
    var locationShakeDateTimeShownDict: [String: Date?] {
        get {
            let dict = defaults.object(forKey: locationShakeDateTimeShownDictKey) as? [String: Date]
            return dict ?? [:]
        }
        
        set {
            defaults.set(newValue, forKey: locationShakeDateTimeShownDictKey)
            defaults.synchronize()
        }
    }
    
    var mapFilterType: FilterPopTipView.FilterType {
        get {
            
            let value = defaults.integer(forKey: mapFilterTypeKey)
            if let type = FilterPopTipView.FilterType(rawValue: value) {
                return type
            }
            
            return FilterPopTipView.FilterType.nearby
        }
        
        set {
            defaults.set(newValue.rawValue, forKey: mapFilterTypeKey)
            defaults.synchronize()
        }
    }
    
    var starterShowDatePerUser: [String: Date?] {
        get {
            let data = defaults.object(forKey: starterShowDateKey) as? [String: Date]
            return data ?? [:]
        }
        
        set {
            defaults.set(newValue, forKey: starterShowDateKey)
            defaults.synchronize()
        }
    }
    
    var dailyShowDatePerUser: [String: Date] {
        get {
            let data = defaults.object(forKey: dailyShowDateKey) as? [String: Date]
            return data ?? [:]
        }
        
        set {
            defaults.set(newValue, forKey: dailyShowDateKey)
            defaults.synchronize()
        }
    }
    
    // Starter / daily methods
    func shouldShowStarter(_ id: String) -> Bool {
        return starterShowDatePerUser[id] == nil
    }
    func shouldShowDaily(_ id: String) -> Bool {
        if let date = dailyShowDatePerUser[id] {
            return date.isInThePast != false && date.isInToday != true
        }
        
        return true
    }
    
    // New location shake
    func shouldShowLocationShake(for location: String) -> Bool {
        if let date = locationShakeDateTimeShownDict[location] as? Date {
            if date.isInThePast != false, date.isInToday != true {
                locationShakeDateTimeShownDict[location] = nil
            } else {
                return false
            }
        }
        
        return true
    }
    
    var currentLanguage: LANGUAGE {
        get {
            
            if let value = defaults.string(forKey: currentLanguageKey),
               let language = LANGUAGE(rawValue: value) {
                
                return language
            }
            
            return LANGUAGE.ENGLISH
        }
        
        set {
            defaults.set(newValue.rawValue, forKey: currentLanguageKey)
            defaults.synchronize()
        }
    }
    
    var pinnedConversationIDs: [String] {
        get {
            if let array = defaults.array(forKey: pinnedConversationIDsKey) as? [String] {
                return array
            }
            
            return []
        }
        
        set {
            defaults.set(newValue, forKey: pinnedConversationIDsKey)
            defaults.synchronize()
        }
    }
    
    func reset() {
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier ?? "")
        defaults.synchronize()
    }
}

