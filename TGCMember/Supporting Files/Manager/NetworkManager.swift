//
//  NetworkManager.swift
//  TGCMember
//
//  Created by jonas on 12/2/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import Foundation
import Alamofire

typealias TGCDataRequest = DataRequest

class NetworkManager {
    static let shared = NetworkManager()
    private init(){}
    
    func generateHeaders() -> HTTPHeaders {
        let headers: HTTPHeaders = [  "Content-Type": "application/json",
                                      "x-gcc-app-type": "member",
                                      "x-gcc-device-id": Utils.getDeviceId(),
                                      "x-gcc-device-type": "ios",
                                      "x-gcc-auth-token": GLOBAL.GlobalVariables.appToken ?? "",
                                      "Accept-Language": GLOBAL.GlobalVariables.currentLanguage.rawValue]
        return headers
    }
    
    @discardableResult
    func requestData<T: Decodable>(url: URL,
                                   type: T.Type,
                                   method: HTTPMethod = .get,
                                   parameters: Parameters? = nil,
                                   headers: HTTPHeaders? = nil,
                                   completionHandler: @escaping GenericResult<T, ErrorResponse>) -> TGCDataRequest {
        
        
        AF.request(url,
                   method: method,
                   parameters: parameters,
                   encoding: method == HTTPMethod.get ? URLEncoding.default : JSONEncoding.default,
                   headers: headers ?? generateHeaders()).responseData(completionHandler: { [weak self] responseData in
            guard let self = self else { return }
            print("URL: \(url.absoluteString)")
            print("Parameters: \(parameters ?? [:])")
            DispatchQueue.global(qos: .background).async {
                if let error = responseData.error {
                    DispatchQueue.main.async {
                        completionHandler(.failure(.generic(error.localizedDescription)))
                    }
                    return
                }
                
                guard let data = responseData.data else { return }
                
//                let jsonString = String(data: data, encoding: .utf8) ?? ""
//                print(jsonString)
                
                do {
                    let object = try self.decode(type, data: data)
                    DispatchQueue.main.async {
                        completionHandler(.success(object))
                    }
                } catch let e {
                    print(e)
                    // Remove if backend is fixed
                    if String(data: data, encoding: .utf8) == "{}" {
                        if let obj = ServerResponse(success: true) as? T {
                            completionHandler(.success(obj))
                            return
                        }
                    }
                    
                    DispatchQueue.main.async {
                        completionHandler(.failure(.generic(e.localizedDescription)))
                    }
                }
            }
        })
    }
    
    // FOR no headers
    @discardableResult
    func gRequestData<T: Decodable>(url: URL,
                                   type: T.Type,
                                   method: HTTPMethod = .get,
                                   parameters: Parameters? = nil,
                                   completionHandler: @escaping GenericResult<T, Error>) -> TGCDataRequest {
        
        AF.request(url,
                   method: method,
                   parameters: parameters,
                   encoding: method == HTTPMethod.post ? JSONEncoding.default : URLEncoding.default).responseData(completionHandler: { [weak self] responseData in
            guard let self = self else { return }
            
            if let error = responseData.error {
                completionHandler(.failure(error))
                return
            }
            
            guard let data = responseData.data else { return }
            
            let jsonString = String(data: data, encoding: .utf8) ?? ""
//            print(jsonString)
            
            do {
                let object = try self.decode(type, data: data)
                completionHandler(.success(object))
            } catch let e {
                print(e)
                completionHandler(.failure(e))
            }
        })
    }
    
    private func decode<T: Decodable>(_ model: T.Type,
                                               data: Data) throws -> T {
        let decoder = JSONDecoder()
        let object = try decoder.decode(model.self, from: data)
        
        return object
    }
    
    func requestData(with url: URL,
                     completionHandler: @escaping ((Data?, Error?) -> Void)) {
        guard let request = try? URLRequest(url: url, method: .get, headers: generateHeaders()) else { return }
        
        AF.request(request).responseData(completionHandler: { res in
            completionHandler(res.data, res.error)
        })
    }
}
