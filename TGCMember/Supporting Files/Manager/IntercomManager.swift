//
//  IntercomManager.swift
//  TGCMember
//
//  Created by jonas on 11/6/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation
import Intercom

class IntercomManager {
    static let shared = IntercomManager()
    
    private init() {}
    
    // Call only once in whole app lifecycle
    func setup() {
        Intercom.setApiKey("ios_sdk-1bda9d7ea352bac2da1e2515c8070111cd52559a", forAppId: "l8c0zpb7")
    }
    
    func registerUser(with id: String?) {
        guard let id = id else { return }
        Intercom.registerUser(withUserId: id)
    }
    
    func showMessenger() {
        Intercom.presentMessenger()
    }
    
    func logout() {
        Intercom.logout()
    }
}
