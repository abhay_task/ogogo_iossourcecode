//
//  APICommonServices.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import Alamofire

enum API_URL: String {
    case INIT_APP = "/member/api/auth/init-app"
    case SYTEM_CONFIG = "/member/api/get-configs"
    case LOGIN = "/member/api/auth/login"
    case LOGOUT = "/member/api/auth/logout"
    case SIGN_UP = "/member/api/profile/sign-up"
    case BASE_UPDATE = "/member/api/profile/update" // From Base_Update
    case GET_COUNTRIES_LIST = "/api/country"
    case REGISTER_PUSHING_TOKEN = "/member/api/register-pushing-token"
    case VENDOR_MAP_LIST = "/member/api/map/vendor/get-list"
    case VENDOR_MAP_NEAR_ME_LIST = "/member/api/map/vendor/list-near-me"
    case VENDOR_LIST_AT_ADDRESS = "/member/api/trip/vendors-near-trip"
    case VENDOR_SUMMARY = "/member/api/map/vendor/summary"
    case VENDOR_DETAIL = "/member/api/map/vendor/detail"
    case DEAL_DETAIL = "/member/api/deal/detail/"
    case TEO_DEAL_DETAIL = "/member/api/deal/single/tell-everyone"
    case TEO_SPLASH_DETAIL = "/member/api/deal/single/splash-screen"
    case EVENT_DETAIL = "/member/api/event/detail"
    case LIST_ALL_EVENTS = "/member/api/event/get-list-multi"
    case LIST_EVENT_SUGGEST_UPCOMING = "/member/api/map/vendor/list-event-suggest-upcoming"
    case LIST_AMAZING_OFFERS = "/member/api/map/vendor/list-amazing-offer-deal"
    case LIST_OFFER_SUGGEST_UPCOMING = "/member/api/map/vendor/list-offer-suggest-upcoming"
    case LIST_DEAL_SUGGEST_UPCOMING = "/member/api/map/vendor/list-deal-suggest-upcoming"
    case GET_CATEGORIES = "/member/api/vendor/get-categories"
    
    case BOOKMARK_EVENT = "/member/api/event/bookmark"
    case LIKE_DEAL = "/member/api/deal/like"
    case USE_DEAL = "/member/api/deal/use"
    case FAVOURITE_DEAL = "/member/api/deal/favorite/"
    case ADD_DEAL_TO_WALLET = "/member/api/deal/add-to-wallet"
    case REMOVE_DEAL_FROM_WALLET = "/member/api/deal/remove-from-wallet"
    
    case ACTIVATE_CARD = "/member/api/profile/buy-new-card"
    case SEND_GIFT = "/member/api/gift/send-gift-virtual-card"
    
    case START_A_SINGLE_CHAT = "/common/api/chat/start-single-chat"
    case GET_HISTORY_MESSAGES = "/common/api/chat/history"
    case GET_GROUP_CHATS = "/common/api/chat/list-groups-chat"
    case SEND_MESSAGE = "/common/api/chat/send"
    case UPLOAD_IMAGE_CHAT = "/common/api/chat/upload/image"
    
    case GET_AVAILABLE_PACKAGES = "/member/api/upgrade/get-available-packages"
    case GET_REFERRAL_INFO = "/api/referral"
    
    case GET_MAP_DETAILS = "/member/api/deal/home-map"
    
    // WALLET
    case LIST_DEAL_IN_WALLETS = "/member/api/deal/get-list-wallets"
    case LIST_DEAL_IN_WALLET_NEW = "/member/api/deal/get-list-wallet-new"
    case LOAD_MORE_IN_WALLET = "/member/api/deal/get-list-wallets-loadmore"
        // NEW
    case DEAL_WALLETS = "/member/api/deal/wallets"
    case DEALS_WALLETS_SEARCH = "/member/api/deal/all-swiped-deal-by-type-paging"
    
    //PAYMENT
    case GET_PAYMENT_METHODS = "/payment/api/get-available-payment-methods"
    case GENERATE_PAYMENT_TRANSACTION = "/payment/api/generate-payment-transaction"
    case CONFIRM_PAYMENT_TRANSACTION = "/payment/api/confirm-payment-transaction"
    case SUBSCRIPTION_UPGRADE = "/member/api/upgrade/subscription"
    case SUBSCRIPTION_DOWNGRADE = "/member/api/downgrade/subscription"
    case SUBSCRIPTION_CANCEL = "/member/api/subscription/cancel"
    case DEACTIVE_ACCOUNT = "/member/api/profile/cancel"
    case PAY_WITH_CARD = "/payment/api/pay-with-card"
    case CHANGE_PLAN = "/member/api/switch/plan"
    
    case SEND_REPORT = "/api/report-issue/send"
    case VERIFY_CODE_ON_CARD = "/member/api/validate-card"
    
    case RANDOM_SPLASH_DEAL = "/common/api/deal/random-splash-screen"
    
    case GET_LIST_DEALS_FROM_PNS = "/member/api/deal/get-list-details-to-pns"
    
    case INVITE_FRIENDS_VIA_EMAIL = "/member/api/tell-everyone/send-assignee"
    case VALIDATE_ASSIGNEE_EMAIL = "/member/api/tell-everyone/validate-assignee"
    
    case GET_CONFIG_SWEEPSTAKE = "/member/api/sweepstake/submit-code"
    case SUBMIT_SWEEPSTAKE_RESULT = "/member/api/sweepstake/final-sweepstake"
    case COMPLETE_TEO = "/member/api/tell-everyone/invitee-complete"
    case GET_TO_DO_LIST_IN_WALLET = "/member/api/thing-to-do/get-list"
    
    // TRIPS
    case GET_TRIPS_LIST_IN_WALLET = "/member/api/trip/list-by-member"
    case GET_TRIP_DETAIL = "/member/api/trip/details"
    case CREATE_NEW_TRIP = "/member/api/trip/create"
    case LIST_PURPOSE_CREATE_TRIP = "/member/api/trip/list-trip-preference-all"
    case LIST_COUNTRY_IN_CREATE_TRIP = "/api/countries-city-shipment-all"
    case LIST_PREFERENCES_ENABLE = "/member/api/trip/list-trip-preference-enable"
    case CREATE_TRIP = "/member/api/trip/handle"
    case DELETE_TRIP = "/member/api/trip/delete"
    
    case TRACK_NEW_LOCATION = "/member/api/tracks/location"
    case CONFIRM_UPGRADE_SALESFORCE = "/member/api/tracks/location/confirmation"
    case GET_INFO_SPLASH_SCREEN = "/member/api/splashscreen/get-by-id"
    
//    case GET_DEALS_ON_MAP = "/member/api/deal/init-map"
    case GET_DEALS_ON_MAP = "/member/api/deal/home-map-search"
    case GET_MORE_DEALS_ON_MAP_BY_CATEGORY = "/member/api/deal/init-map-by-category"
    
    case STORE_TEMP_INFO_TEO = "/member/api/tell-everyone/store-temp-info"
    case GET_TEMP_INFO_TEO = "/member/api/tell-everyone/get-temp-info"

    case GET_SWIPE_DEALS = "/member/api/swipe-deal/push-swipe-deal"
    
    case POST_ADD_TO_STORE = "/member/api/store/add-to-store"
    case POST_REMOVE_FROM_UNSWIPED = "/member/api/swipe-deal/remove-from-unswiped"
    case POST_MOVE_TO_TRASH =  "/member/api/store/remove-from-store"
    
    case GET_MY_PROFILE = "/member/api/profile/my-profile"
    case GET_MORE_DEALS_OF_DEAL = "/member/api/deal/deals-swiped-of-vendor"
    
    case GET_TUTORIAL_SCREEN_CONFIG = "/member/api/tutorial/get-screen-detail"
    
    case SEND_EMAIL_RESET_PASSWORD = "/member/api/send-mail-reset-password"
    
    case GET_LOYALTY_DEAL = "/member/api/deal/get-loyalty-deal"
    
    //PLACE
    case GET_PLACE_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/queryautocomplete/json"
    case GET_PLACE_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json"
    
    //Account
    case GET_TARGET_INFO = "/vendor/api/deal/get-infor-target"
    case UPDATE_AVATAR_ONLY = "/member/api/profile/update-avatar-only"
    
    //Wishlist
    case GET_WISHLIST = "/member/api/wish/list"
    case CREATE_DELETE_WISHLIST = "/member/api/wish"
    case EDIT_WISH = "/member/api/wish-update/$id"
    
    //T&C
    case TERMS_AND_CONDITIONS = "/terms-conditions"
    
    //FAQs
    case GET_FAQS = "/faqs"
}

class APICommonServices {
    
    private static func buildStringURL(_ apiUrl: API_URL) -> String {
        return  "\(BASE_URL)\(apiUrl.rawValue)"
    }
    
    private static func makeRequest(url: String, method: HTTPMethod, parameters: [String: Any]?,  completion: DataCallback?, tryAgainBlock: FinishedCallback? = nil) {
        if GLOBAL.GlobalVariables.connectionStatus == .DISCONNECTED {
            Utils.dismissLoading()
            Utils.showNetworkError()
        } else {
            let headers: HTTPHeaders = [  "Content-Type": "application/json",
                                          "x-gcc-app-type": "member",
                                          "x-gcc-device-id": Utils.getDeviceId(),
                                          "x-gcc-device-type": "ios",
                                          "x-gcc-auth-token": GLOBAL.GlobalVariables.appToken ?? "",
                                          "Accept-Language": GLOBAL.GlobalVariables.currentLanguage.rawValue]
            let _url = URL(string: url)!
            print("URL: \(_url.absoluteString)")
            print("Parameters: \(parameters ?? [:])")
            print("Headers: \(headers )")
            let tRequset = AF.request(_url,
                                             method: method,
                                             parameters: parameters,
                                             encoding: method == HTTPMethod.post ? JSONEncoding.default : URLEncoding.default,
                                             headers: headers)
                .validate()
                .responseJSON( completionHandler: { response in
                    
                    let httpCode = response.response?.statusCode
                    
                    if httpCode == nil {
                        print("<--- make REQUEST FAILED ---> ", url)
                        
                        if let _tryAgainBlock  = tryAgainBlock {
                            print("<--- tryAgainBlock REQUEST ---> ", url)
                            _tryAgainBlock()
                            
                        } else {
                            completion!(nil)
                        }
                    } else if httpCode == 403 {
                        NotificationCenter.default.post(name: .FORCE_SIGN_IN, object: nil)
                        // GLOBAL.GlobalVariables.dictionaryRequestStatus[url] = nil
                        completion!(nil)
                    } else {
                        // GLOBAL.GlobalVariables.dictionaryRequestStatus[url] = nil
                        print("<--- make REQUEST SUCCESS ---> ", url)
                        do {
                            if let dataResponse = response.data {
                                let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? [String: Any?]
                                 mPrint("response : ", jsonResponse) //Response result
                                
                                if var json = jsonResponse {
                                    json["httpCode"] = httpCode
                                    completion!(json )
                                } else {
                                    completion!(["httpCode": httpCode, "status": false, "data": nil])
                                }
                            } else {
                                completion!(["httpCode": httpCode, "status": false, "data": nil])
                            }
                        } catch let parsingError {
                            print("Error", parsingError)
                            completion!(["httpCode": httpCode, "status": false, "data": nil])
                        }
                    }
                })
            
            //GLOBAL.GlobalVariables.dictionaryRequestStatus[url] = tRequset
        }
        
        
        
    }
    
    private static func callHttp(url: String, method: HTTPMethod, parameters: [String: Any]?,  completion: DataCallback?) {
        let  tryAgainBlock: FinishedCallback = { () -> Void in
            makeRequest(url: url, method: method, parameters: parameters, completion: completion, tryAgainBlock: nil)
        }
        
        makeRequest(url: url, method: method, parameters: parameters, completion: completion, tryAgainBlock: tryAgainBlock)
        
    }
    
    private static func callHttp(url: API_URL, method: HTTPMethod, parameters: [String: Any]?,  completion: DataCallback?) {
        let _url: String = buildStringURL(url)
        
        callHttp(url: _url, method: method, parameters: parameters, completion: completion)
    }
    
    static func fetchContentFromJSON(_ urlStr: String, completion: DataCallback?) {
        if let url = URL(string: urlStr) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if let data = data {
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                        
                        completion?(["data": jsonResponse])
                    } catch {
                        completion?(nil)
                    }
                } else {
                    completion?(nil)
                }
            }.resume()
        }
    }
    
    static func initApp(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .INIT_APP,
            method: .post,
            parameters:  ["g": GLOBAL.GlobalVariables.fcmToken]) { (resp) in
                completion!(resp)
        }
        
    }
    
    static func getSystemConfig(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SYTEM_CONFIG,
            method: .get,
            parameters:  nil) { (resp) in
                completion!(resp)
        }
        
    }
    
    static func login(_ email: String, _ password: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LOGIN,
            method: .post,
            parameters:  ["user_id": Utils.getUserId(), "email": email, "password": password, "pushing_token": GLOBAL.GlobalVariables.fcmToken]) { (resp) in
                completion!(resp)
        }
    }
    
    static func logout(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LOGOUT,
            method: .post,
            parameters:  ["pushing_token": GLOBAL.GlobalVariables.fcmToken]) { (resp) in
                completion!(resp)
        }
        
    }
    
    static func updatePushingToken(_ token: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .REGISTER_PUSHING_TOKEN,
            method: .post,
            parameters:  ["pushing_token": token]) { (resp) in
                completion!(resp)
        }
    }
    
    static func register(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SIGN_UP,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func baseUpdate(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .BASE_UPDATE,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getMyProfile(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_MY_PROFILE,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getCountryList(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_COUNTRIES_LIST,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getAllListVendor(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .VENDOR_MAP_LIST,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getNearMeListVendor(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .VENDOR_MAP_NEAR_ME_LIST,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getListVendorAtAddress(_ address: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .VENDOR_LIST_AT_ADDRESS,
            method: .get,
            parameters: ["address": address]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getVendorSummary(_ summaryId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.VENDOR_SUMMARY))/\(summaryId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getVendorDetail(_ vendorId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.VENDOR_DETAIL))/\(vendorId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getDealDetail(_ dealId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.DEAL_DETAIL))/\(dealId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getTEODealDetail(_ dealId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.TEO_DEAL_DETAIL))/\(dealId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getTEOSplashDetail(_ dealId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.TEO_SPLASH_DETAIL))/\(dealId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getEventDetail(_ eventId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.EVENT_DETAIL))/\(eventId)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getAllEvents(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_ALL_EVENTS,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getEventsSuggestUpcoming(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_EVENT_SUGGEST_UPCOMING,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getAmazingOffers(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_AMAZING_OFFERS,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getOffersSuggestUpcoming(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_OFFER_SUGGEST_UPCOMING,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getDealsSuggestUpcoming(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_DEAL_SUGGEST_UPCOMING,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getListCategories(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_CATEGORIES,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func toogleBookmarkEvent(_ eventId: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .BOOKMARK_EVENT,
            method: .post,
            parameters: ["id": eventId ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func toogleLikeDeal(_ dealId: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIKE_DEAL,
            method: .post,
            parameters: ["id": dealId ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func toogleFavouriteDeal(_ dealId: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .FAVOURITE_DEAL,
            method: .post,
            parameters: ["id": dealId ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func addDealToWallet(_ dealId: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .ADD_DEAL_TO_WALLET,
            method: .post,
            parameters: ["id": dealId ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func removeDealFromWallet(_ dealId: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .REMOVE_DEAL_FROM_WALLET,
            method: .post,
            parameters: ["id": dealId ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func doUseDeal(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .USE_DEAL,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getListDealInWallets(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_DEAL_IN_WALLETS,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getListDealInWalletNew(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_DEAL_IN_WALLET_NEW,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func activateCard(_ code: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .ACTIVATE_CARD,
            method: .post,
            parameters: ["scratch_code": code ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func sendGiftCard(_ params: [String: Any]?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SEND_GIFT,
            method: .post,
            parameters:params) { (resp) in
                completion!(resp)
        }
    }
    
    static func verifyCodeOnCard(_ code: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .VERIFY_CODE_ON_CARD,
            method: .post,
            parameters: ["code": code ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getGroupChats(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_GROUP_CHATS,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
        
    }
    
    static func startAChat(_ partnerId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .START_A_SINGLE_CHAT,
            method: .post,
            parameters: ["partner_id": partnerId]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getHistoryMessages(_ groupId: String, messageId: String = "", limit: Int = 50, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.GET_HISTORY_MESSAGES))/\(groupId)"
        
        var params: [String: Any] = ["limit": limit]
        if messageId != "" {
            params["message_id"] = messageId
        }
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
        
    }
    
    static func sendMessage(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SEND_MESSAGE,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func uploadImageFromChat(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .UPLOAD_IMAGE_CHAT,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getAvailablePackages(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_AVAILABLE_PACKAGES,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    
    static func getPaymentMethods(_ packageId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_PAYMENT_METHODS,
            method: .post,
            parameters: ["payment_kind": KIND_PAYMENT_METHOD.SUBSCRIPTION.rawValue, "package_id": packageId]) { (resp) in
                completion!(resp)
        }
    }
    
    static func generateTransaction(_ packageId: String, paymemtMethod: String, transactionId: String = "", completion: DataCallback?) {
        var params: [String: Any] = ["payment_kind": KIND_PAYMENT_METHOD.SUBSCRIPTION.rawValue, "package_id": packageId, "payment_method": paymemtMethod]
        if transactionId != "" {
            params["payment_transaction_id"] = transactionId
        }
        
        APICommonServices.callHttp(
            url: .GENERATE_PAYMENT_TRANSACTION,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func confirmTransaction(_ transactionId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .CONFIRM_PAYMENT_TRANSACTION,
            method: .post,
            parameters: ["payment_kind": KIND_PAYMENT_METHOD.SUBSCRIPTION.rawValue, "payment_transaction_id": transactionId]) { (resp) in
                completion!(resp)
        }
    }
    
    static func upgradeSubscription(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SUBSCRIPTION_UPGRADE,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func downgradeSubscription(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SUBSCRIPTION_DOWNGRADE,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func cancelSubscription(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SUBSCRIPTION_CANCEL,
            method: .post,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func deactiveAccount(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .DEACTIVE_ACCOUNT,
            method: .post,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func payWithCard(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .PAY_WITH_CARD,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func changePlan(_ packageId: String, _ isUpgrade: Bool = true, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .CHANGE_PLAN,
            method: .post,
            parameters: ["package_id": packageId, "is_upgrade": isUpgrade]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getReferralInfo(_ refCode: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.GET_REFERRAL_INFO))/\(refCode)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    
    static func sendReport(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SEND_REPORT,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getRandomSplashDeal(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .RANDOM_SPLASH_DEAL,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    
    static func getListDealsFromPNS(_ ids: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.GET_LIST_DEALS_FROM_PNS))?ids=\(ids)"
        
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func sendInviteFriends(_ base64: String?, _ message: String, _ assignees: [[String: String]], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .INVITE_FRIENDS_VIA_EMAIL,
            method: .post,
            parameters: ["avatar_url": base64 ?? "", "message": message, "assignees": assignees]) { (resp) in
                completion!(resp)
        }
    }
    
    static func validateInviteEmail(_ email: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .VALIDATE_ASSIGNEE_EMAIL,
            method: .post,
            parameters: ["email": email ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getConfigSweepstake(_ code: String?, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_CONFIG_SWEEPSTAKE,
            method: .post,
            parameters: ["sweepstake_code": code ?? ""]) { (resp) in
                completion!(resp)
        }
    }
    
    static func submitSweepstakeResult(_ sweepstakeId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SUBMIT_SWEEPSTAKE_RESULT,
            method: .post,
            parameters: ["sweepstake_id": sweepstakeId]) { (resp) in
                completion!(resp)
        }
    }
    
    static func submitTEO(_ email: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .COMPLETE_TEO,
            method: .post,
            parameters: ["email": email]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getToDoList(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_TO_DO_LIST_IN_WALLET,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func loadMoreItemInWallet(_ walletType: String?, page: Int = 2, limit: Int = 20, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LOAD_MORE_IN_WALLET,
            method: .post,
            parameters: ["page": page, "limit": limit, "wallet_type": walletType]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getTripsList(completion: DataCallback?) {
        
        APICommonServices.callHttp(
            url: .GET_TRIPS_LIST_IN_WALLET,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getTripDetail(_ tripId: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.GET_TRIP_DETAIL))/\(tripId)"
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func createNewTrip(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .CREATE_NEW_TRIP,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getCountriesInCreateTrip(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_COUNTRY_IN_CREATE_TRIP,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getPurposeInCreateTrip(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .LIST_PURPOSE_CREATE_TRIP,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    
    static func trackNewLocation(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .TRACK_NEW_LOCATION,
            method: .post,
            parameters: ["lat": GLOBAL.GlobalVariables.latestLat, "lng": GLOBAL.GlobalVariables.latestLng]) { (resp) in
                completion!(resp)
        }
    }
    
    static func confirmUpgradeSalesforce(_ answer: String, _ region: String, _ trackingId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .CONFIRM_UPGRADE_SALESFORCE,
            method: .post,
            parameters: ["answer": answer, "region": region, "member_tracking_pns_id": trackingId]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getInfoSplashScreen(_ id: String, completion: DataCallback?) {
        let _url: String = "\(buildStringURL(.GET_INFO_SPLASH_SCREEN))/\(id)"
        APICommonServices.callHttp(
            url: _url,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getDealsNewest(completion: DataCallback? = nil) {
        APICommonServices.callHttp(
            url: .LIST_DEAL_SUGGEST_UPCOMING,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getDealsMostLiked(completion: DataCallback? = nil) {
        APICommonServices.callHttp(
            url: .LIST_DEAL_SUGGEST_UPCOMING,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    //    [int]page
    //    [int]limit
    //    [float]lat
    //    [float]lng
    static func getListDealsOnMapFirst(_ params: [String: Any], completion: DataCallback? = nil) {
        APICommonServices.callHttp(
            url: .GET_DEALS_ON_MAP,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getListDealsFromDealsOnMap(_ params: [String: Any], completion: DataCallback? = nil) {
        APICommonServices.callHttp(
            url: .DEALS_WALLETS_SEARCH,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    //    [int]page
    //    [int]limit
    //    [string]category_id
    //    [float]lat
    //    [float]lng
    static func getMoreDealsOnMapByCategory(_ params: [String: Any], completion: DataCallback? = nil) {
        APICommonServices.callHttp(
            url: .GET_MORE_DEALS_ON_MAP_BY_CATEGORY,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    // (param: assignment_id, emails, photo, message)
    static func setTempTEOInfo(_ assignmentId: String, _ emails: String, _ photo: String, _ message: String, completion: DataCallback?) {
        
        let params = ["assignment_id": assignmentId, "emails": emails, "photo": photo, "message": message]
        mPrint("setTempTEOInfo --> ", params)
        APICommonServices.callHttp(
            url: .STORE_TEMP_INFO_TEO,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getTempTEOInfo(_ assignmentId: String, completion: DataCallback?) {
        let params = ["assignment_id": assignmentId]
        APICommonServices.callHttp(
            url: .GET_TEMP_INFO_TEO,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    
    //type=(daily | starter)
    static func getSwipeDeals(_ type: String, completion: DataCallback?) {
        let params = ["type": type]
        
        APICommonServices.callHttp(
            url: .GET_SWIPE_DEALS,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func addDealToStore(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .POST_ADD_TO_STORE,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func removeDealFromUnswiped(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .POST_REMOVE_FROM_UNSWIPED,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func removeDealFromStore(_ instanceDealID: String, completion: DataCallback?) {
        let params = ["instance_deal_id": instanceDealID]
        APICommonServices.callHttp(
            url: .POST_MOVE_TO_TRASH,
            method: .post,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
    
    static func getMoreDealsOfDeal(_ vendorId: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_MORE_DEALS_OF_DEAL,
            method: .get,
            parameters: ["vendor_id": vendorId]) { (resp) in
                completion!(resp)
        }
    }
    
    
    static func getTutorialConfig(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_TUTORIAL_SCREEN_CONFIG,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func getLoyaltyDeal(completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_LOYALTY_DEAL,
            method: .get,
            parameters: nil) { (resp) in
                completion!(resp)
        }
    }
    
    static func sendEmailResetPassword(_ email: String, completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .SEND_EMAIL_RESET_PASSWORD,
            method: .post,
            parameters: ["email" : email]) { (resp) in
                completion!(resp)
        }
    }
    
    static func getMapDetails(_ params: [String: Any], completion: DataCallback?) {
        APICommonServices.callHttp(
            url: .GET_MAP_DETAILS,
            method: .get,
            parameters: params) { (resp) in
                completion!(resp)
        }
    }
}
