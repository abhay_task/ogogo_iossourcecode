//
//  Utils.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//


import UIKit
import SVProgressHUD
import WebKit
import Branch
import GoogleMaps
import MessageUI

public enum LOGIN_STATUS {
    case SUCCESS_IMMEDIATELY
    case SUCCESS_AFTER
    case NOT_YET
}

public enum LINK_TYPE: String {
    case INVITE = "invite"
    case MARKETING = "market"
    case SIGN_IN = "sign_in"
    
    case SWEEPTAKES = "sweeptakes"
    case TELL_EVERYONE = "tell_everyone"
}


public typealias DataCallback = (_ data: [String: Any?]?) -> Void
typealias FinishedCallback = () -> Void
typealias SuccessedCallback = (_ success: Bool) -> Void
typealias SuccessedDataCallback = (_ success: Bool, _ data: Any?) -> Void


typealias LoginCallback = (_ status: LOGIN_STATUS) -> Void
typealias TimeIntervalCallback = (_ number: Int, _ timer: Timer) -> Void
typealias FirebaseCallback = (_ status: FB_DATA_STATUS, _ data: Any?) -> Void

typealias TabClickedCallback = (_ tabIndex: Int) -> Void

let FROM_FORMAT_DEFAULT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
let FROM_FORMAT_DEFAULT_FALLBACK = "yyyy-MM-dd'T'HH:mm:ssZ"
let TO_FORMAT_DEFAULT = "dd MM yyyy"

let SCREEN_WIDTH: CGFloat = UIScreen.main.bounds.width
let SCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.height

func setTimeout(_ completed: @escaping FinishedCallback, _ timeInterval: TimeInterval) -> Void {
    DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval/1000) {
        // your code here
        completed()
    }
    
}

func toRadians(_ number: Double) -> Double {
    return number * .pi / 180
}

func toDegrees(_ number: Double) -> Double {
    return number * 180 / .pi
}

func setTimeInterval(_ completed: @escaping TimeIntervalCallback, _ timeInterval: TimeInterval) -> Void {
    var i: Int = 0
    Timer.scheduledTimer(withTimeInterval: timeInterval/1000, repeats: true) { timer in
        i += 1
        completed(i, timer)
    }
}

func MLocalized(_ key: String, _ comment: String = "") -> String {
    let cLanguage = GLOBAL.GlobalVariables.currentLanguage.rawValue
    
    let path = Bundle.main.path(forResource: cLanguage, ofType: "lproj")
    let lStr = NSLocalizedString(key, tableName: "Languages", bundle: Bundle(path: path ?? "") ?? Bundle.main, value: cLanguage, comment: "")
    
    if lStr == GLOBAL.GlobalVariables.currentLanguage.rawValue {
        return key
    }
    
    return lStr
}

func mPrint(_ comment: String = "", _ parameters: Any?) {
    #if DEBUG
    
    if let params = parameters {
        if params is String {
            print(comment, params)
        } else if let _ = parameters {
            let jsonData = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                print(comment, jsonString)
            }
        }
    } else {
        print(comment, "")
    }
    
    #endif
}

class Utils {
    
    static let shared = Utils()

    var isLoading = false {
        didSet {
            guard !isLoading else { return }
            Utils.dismissLoading()
        }
    }

    private init() {}
    
    static func showResultUseDealAlert(_ success: Bool = true, _ data: [String: Any?]?, completion: SuccessedDataCallback? = nil) {
        let content = NewResultAlertView().loadView()
        
        let tTitle = success ? LOCALIZED.congratulation.translate : LOCALIZED.error.translate
        
        var successMessage = LOCALIZED.you_ve_successfully_used_deal.translate
        let errorMessage = LOCALIZED.your_deal_got_something_wrong.translate
        
        var dealName = ""
        
        if let tData = data {
            dealName = "\"\(tData["Name"] as? String ?? "")\""
        }
        
//        errorMessage = errorMessage.replacingOccurrences(of: "[%__TAG_NAME__%]", with: dealName)
        successMessage = successMessage.replacingOccurrences(of: "[%__TAG_NAME__%]", with: dealName)
        
        var tData = [String: Any?]()
        tData["success"] = success
        tData["message"] = success ? successMessage : errorMessage
        tData["title"] = tTitle
        
        content.config(tData, onClickedActionCallback: { (data) in
            Utils.closeCustomAlert()
            
            completion?(success, data)
            
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    static func isValidToSwipeDeal() -> Bool {
        if !GLOBAL.GlobalVariables.isHomeAppear || GLOBAL.GlobalVariables.numberCustomAlertAvailable > 0 {
            return false
        }
        
        return true
    }
    
    
    
    static func isAnonymous() -> Bool {
        if let _ = getAppToken(), let userInfo = getUserInfo(), let email = userInfo["Email"] as? String, email != "" {
            return false
        }
        
        return true
    }
    
    static func isRequireSignUpToGetDeal() -> Bool {
        if let _ = getAppToken(), let userInfo = getUserInfo() {
            
            let email = userInfo["Email"] as? String ?? ""
            let cardID = userInfo["CardID"] as? String ?? ""
            
            if cardID == "" {
                return false
            }
            
            if email != "" {
                return false
            }
            
            return true
        }
        
        return false
    }
    
    static func isSuggestGiftCode() -> Bool {
        if let _ = getAppToken(), let userInfo = getUserInfo() {
            
            let email = userInfo["Email"] as? String ?? ""
            let giftVirtualCard = userInfo["GiftVirtualCard"] as? String ?? ""
            
            if giftVirtualCard != ""  && email != "" {
                return true
            }
            
            return false
        }
        
        return false
    }
    
    static func getAllDealsFromSections(_ sections: [[String: Any?]]) -> [[String: Any?]] {
        var listDeals: [[String: Any?]] = []
        
        for section in sections {
            
            let subList = section["List"] as? [[String: Any?]] ?? []
            
            for carousel in subList {
                if let type = carousel["StoreType"] as? String {
                    let list = carousel["List"] as? [[String: Any?]] ?? []
                    
                    if type == STORE_TYPE.NEARBY_DEALS.rawValue {
                        for item in list {
                            var eItem = item
                            eItem["StoreType"] = type
                            listDeals.append(eItem)
                        }
                    } else if type == STORE_TYPE.NEARBY_DEALS.rawValue {
                        
                    }
                }
            }
        }
        
        return listDeals
    }
    
    static func showAlertRequiredLocation() {
        GLOBAL.GlobalVariables.isShowAlertRequestLocation = true
        Utils.showConfirmAlertView("Turn On Location Services to Allow “OGOGO” to Determine Your Location", "", "Settings", nil, confirmCallback: { (data) in
            GLOBAL.GlobalVariables.isShowAlertRequestLocation = false
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }, cancelCallback: nil)
        
    }
    
    static func isCarouselDeal(_ data: [String: Any?]) -> Bool {
        if let _ = data["Title"] as? String {
            return true
        }
        
        return false
    }
    
    static func isHaveItemMore(_ data: [String: Any?]) -> Bool {
        return false
        //        if let list = data["List"] as? [[String: Any?]], list.count > 0 {
        //            var isHave: Bool = false
        //            for deal in list {
        //                let numberMore = deal["NumberOfMultiDeals"] as? Int ?? 0
        //
        //                if numberMore != 0 {
        //                    isHave = true
        //                    break
        //                }
        //            }
        //
        //            return isHave
        //        }
        //
        //        return false
    }
    
    static func getLoyaltyDeal(_ completion: SuccessedDataCallback? = nil) {
        APICommonServices.getLoyaltyDeal { (resp) in
            //mPrint("getLoyaltyDeal FROM SERVER --> ", resp)
            
            if let tResp = resp, let status = tResp["status"] as? Bool, status == true, let tDeal = tResp["deal"] as? [String: Any?] {
                completion?(true, tDeal)
            } else {
                completion?(true, nil)
            }
        }
    }
    
    static func getLoyaltyDealFromSections(_ sections: [[String: Any?]], _ completion: SuccessedDataCallback? = nil) {
        var layaltyDeal: [String: Any?]? = nil
        
        for section in sections {
            
            let subList = section["List"] as? [[String: Any?]] ?? []
            
            for carousel in subList {
                if let type = carousel["StoreType"] as? String {
                    let list = carousel["List"] as? [[String: Any?]] ?? []
                    
                    if type == STORE_TYPE.GLOBAL_SPECIAL_DEALS.rawValue {
                        for item in list {
                            
                            let dealType = getDealType(item)
                            
                            if dealType == DEAL_TYPE.LOYALTY {
                                layaltyDeal = item
                                break
                            }
                        }
                    }
                }
                
                if layaltyDeal != nil {
                    break
                }
            }
        }
        
        completion?(true, layaltyDeal)
    }
    
    static func getHeightWideEvent(_ width: CGFloat = (SCREEN_WIDTH - 50)) -> CGFloat {
        let heightImage: CGFloat = width * (150/325)
        
        return (heightImage + 67)
    }
    
    static func dismissKeyboard() {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    static func setLanguage(_ language: LANGUAGE = .ENGLISH) {
        CacheManager.shared.currentLanguage = language
//        UserDefaults.standard.set(language.rawValue, forKey: STORAGE_KEYS.APP_LANGUAGE.rawValue)
//        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.post(name: .UPDATE_LANGUAGES_IMMEDIATELY, object: nil)
    }
    
    static func updatePushingToken(_ completion: DataCallback? = nil) {
        APICommonServices.updatePushingToken(GLOBAL.GlobalVariables.fcmToken) { (resp) in
            //mPrint("updatePushingToken", resp)
            completion?(resp)
        }
    }
    
    static func getSystemConfig(_ completion: SuccessedCallback? = nil) {
        APICommonServices.getSystemConfig { (resp) in
            mPrint("getSystemConfig --> ", resp)
            
            if let tResp = resp, let status = tResp["status"] as? Bool  {
                if status {
                    GLOBAL.GlobalVariables.sysConfigModel = SysConfigModel(tResp)
                }
                
                GLOBAL.GlobalVariables.getConfigReady = true
                
                completion?(status)
            }
        }
    }
    
    static func initApp(_ completion: SuccessedCallback? = nil) {
        APICommonServices.initApp { (resp) in
            mPrint("initApp --> ", resp)
            
            if let tResp = resp, let status = tResp["status"] as? Bool  {
                if status {
                    if let tData = tResp["data"] as? [String: Any?] {
                        let userInfo = tData["user"] as? [String: Any?]
                        Utils.setUserInfo(userInfo)
                    }
                    
                    Utils.setAppToken(tResp["token"] as? String)
                    Utils.updatePushingToken()
                    
                    if let accessKey = tResp["AWS_ACCESS_KEY"] as? String,
                       let secretKey = tResp["AWS_SECRET_KEY"] as? String,
                       let bucketName = tResp["BUCKET_NAME"] as? String {
                        
                        CacheManager.shared.accessKey = accessKey
                        CacheManager.shared.secretKey = secretKey
                        CacheManager.shared.bucketName = bucketName
                    }
                }
                
                completion?(status)
            }
        }
    }
    
    static func getMyProfile(_ completion: SuccessedCallback? = nil) {
        APICommonServices.getMyProfile { (resp) in
        
            if let tResp = resp, let status = tResp["status"] as? Bool  {
                if status {
                    if let tData = tResp["data"] as? [String: Any?], let userInfo = tData["user"] as? [String: Any?] {
                        
                        mPrint("getMyProfile --> ", userInfo)
                        let user = try? User(dict: userInfo)
                        CacheManager.shared.currentUser = user
                        Utils.setUserInfo(userInfo)
                    }
                }
                
                completion?(status)
            }
        }
    }
    
    static func getTutorialConfig(_ completion: SuccessedCallback? = nil) {
        APICommonServices.getTutorialConfig { (resp) in
        
            if let tResp = resp, let status = tResp["status"] as? Bool  {
                 mPrint("getTutorialConfig --> ", tResp)
                if status {
                    if let tData = tResp["data"] as? [String: Any?], let list = tData["list"] as? [[String: Any?]] {
                        GLOBAL.GlobalVariables.tutorialConfig = list
                    }
                }
                
                completion?(status)
            }
        }
    }
    
    
    
    static func getVendorDetail(_ vendorId: String, onCompletion _onCompletion: SuccessedDataCallback? = nil) {
        APICommonServices.getVendorDetail(vendorId) { (resp) in
            mPrint("getVendorDetail", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?]  {
                    _onCompletion?(true, data)
                }
            } else {
                _onCompletion?(false, nil)
            }
        }
    }
    
    static func submitSweepstakeResult(_ sweepstakeId: String, onCompletion _onCompletion: SuccessedDataCallback? = nil) {
        APICommonServices.submitSweepstakeResult(sweepstakeId, completion: { (resp) in
            mPrint("submitSweepstakeResult", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                Utils.keepResultSweepstake("")
                
                if let userInfo = resp["user"] as? [String: Any?] {
                    Utils.setUserInfo(userInfo)
                }
                
                _onCompletion?(true, "")
            } else {
                _onCompletion?(false, "")
            }
        })
        
    }
    
    static func getTripDetail(_ tripId: String, type: TAB_TRIP_DETAIL = .NONE, onCompletion _onCompletion: SuccessedDataCallback? = nil) {
        APICommonServices.getTripDetail(tripId) { (resp) in
            mPrint("getTripDetail --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] {
                    
                    if type == .NONE {
                        _onCompletion?(true, data)
                    } else {
                        let nList = data[type.key] as? [[String : Any?]] ?? []
                        
                        _onCompletion?(true, nList)
                    }
                } else {
                    _onCompletion?(false, nil)
                }
            } else {
                _onCompletion?(false, nil)
            }
        }
    }
    
    static func handleLogicWhenLogout() {
        setUserInfo(nil)
        setAppToken(nil)
    }
    
    static func logoutApp(callback: SuccessedDataCallback? = nil, forceSignIn: Bool = false) {
        if forceSignIn {
            APICommonServices.logout { (resp) in
                mPrint("submit LOGOUT to server -> ", resp)
                
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    
                    self.handleLogicWhenLogout()
                    callback?(true, resp)
                } else {
                    
                    callback?(false, resp)
                }
            }
        } else {
            self.handleLogicWhenLogout()
            
            NotificationCenter.default.post(name: .REFRESH_HOME_PAGE, object: nil)
        }
        
    }
    
    static func readJSON(fileName: String) -> [String: Any?]? {
        
        let path = Bundle.main.path(forResource: fileName, ofType: "json")
        let jsonData = NSData(contentsOfMappedFile: path!)
        
        if let data = jsonData,
            let json = try? JSONSerialization.jsonObject(with: data as Data, options: []) as? [String: Any] {
            
            return json
        }
        
        return nil
    }
    
    static func getDistanceInMeter(startLat: Double, startLong: Double, endLat: Double, endLong: Double) -> Double {
        let sourceLoc = CLLocation(latitude: startLat, longitude: startLong)
        let destLoc =  CLLocation(latitude: endLat, longitude: endLong)
        
        let distanceInMeters = sourceLoc.distance(from: destLoc)
        
        return distanceInMeters
    }
    
    static func getCoordinateBounds(centerLatLng: CLLocationCoordinate2D, radius: Double) -> GMSCoordinateBounds {
        return GMSCoordinateBounds.init().includingCoordinate(Utils.computeOffset(from: centerLatLng, distance: radius, heading: 0.0))
            .includingCoordinate(Utils.computeOffset(from: centerLatLng, distance: radius, heading: 90.0))
            .includingCoordinate(Utils.computeOffset(from: centerLatLng, distance: radius, heading: 180.0))
            .includingCoordinate(Utils.computeOffset(from: centerLatLng, distance: radius, heading: 270.0))
    }
    
    static func computeOffset(from: CLLocationCoordinate2D, distance: Double, heading: Double) -> CLLocationCoordinate2D {
        let EARTH_RADIUS: Double = 6371009.0
        
        var distance = distance
        var heading = heading
        distance /= EARTH_RADIUS
        
        heading = toRadians(heading)
        
        let fromLat = toRadians(from.latitude)
        let fromLng = toRadians(from.longitude)
        let cosDistance = cos(distance)
        let sinDistance = sin(distance)
        let sinFromLat = sin(fromLat)
        let cosFromLat = cos(fromLat)
        let sinLat = cosDistance * sinFromLat + sinDistance * cosFromLat * cos(heading)
        let dLng = atan2(
            sinDistance * cosFromLat * sin(heading),
            cosDistance - sinFromLat * sinLat
        )
        
        return CLLocationCoordinate2D(latitude: toDegrees(asin(sinLat)), longitude: toDegrees(fromLng + dLng))
    }
    
    static func getVersionApp() -> String {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? ""
        let appVersion = "\(version)"
        
        return appVersion
    }
    
    static func getVersionBuild() -> String {
        let buildNo = Bundle.main.infoDictionary?["CFBundleVersion"] ?? ""
        let buildVersion = "\(buildNo)"
        
        return buildVersion
    }
    
    static func cachedEmailsTE(_ emails: [String]?) {
        if let tEmails = emails {
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: tEmails)
            UserDefaults.standard.set(encodedData, forKey: STORAGE_KEYS.EMAILS_TE.rawValue)
        } else {
            UserDefaults.standard.set(nil, forKey: STORAGE_KEYS.EMAILS_TE.rawValue)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    static func getCachedEmailsTE() -> [String]? {
        let decoded  = UserDefaults.standard.data(forKey: STORAGE_KEYS.EMAILS_TE.rawValue)
        
        if let tDec = decoded {
            let decodedInfo = NSKeyedUnarchiver.unarchiveObject(with: tDec) as? [String]
            if let data = decodedInfo {
                return data
            }
        }
        
        return nil
    }
    
    static func cachedSignUpInfo() {

        let tName = GLOBAL.GlobalVariables.quickSignUpModel.name
        let tEmail = GLOBAL.GlobalVariables.quickSignUpModel.email
        
        let tData = ["name": tName, "email": tEmail]
        
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: tData)
        UserDefaults.standard.set(encodedData, forKey: STORAGE_KEYS.QUICK_SIGN_UP.rawValue)
        
        UserDefaults.standard.synchronize()
    }
    
    static func getCachedSignUpInfo() -> QuickSignUpModel {
        let decoded  = UserDefaults.standard.data(forKey: STORAGE_KEYS.QUICK_SIGN_UP.rawValue)
        
        if let tDec = decoded {
            let decodedInfo = NSKeyedUnarchiver.unarchiveObject(with: tDec) as? [String: Any?]
            if let data = decodedInfo {
                let tModel = QuickSignUpModel(data)
                GLOBAL.GlobalVariables.quickSignUpModel = tModel
                
                return tModel
            }
        }
        
        return QuickSignUpModel(nil)
    }
    
    static func getPlace(for location: CLLocation, completion: @escaping (CLPlacemark?) -> Void) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil")
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }
    
    
    static func setUserInfo(_ userInfo: [String: Any?]?) {
        
        GLOBAL.GlobalVariables.upgradePackageModel = UpgradePackageModel(userInfo)
        GLOBAL.GlobalVariables.userInfo = userInfo
        IntercomManager.shared.registerUser(with: userInfo?["ID"] as? String)
        
        if userInfo == nil  {
            UserDefaults.standard.set(nil, forKey: STORAGE_KEYS.USER_INFO.rawValue)
        } else {
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userInfo!)
            UserDefaults.standard.set(encodedData, forKey: STORAGE_KEYS.USER_INFO.rawValue)
        }
        
        UserDefaults.standard.synchronize()
    }
    
    static func getUserInfo() -> [String: Any?]? {
        let decoded  = UserDefaults.standard.data(forKey: STORAGE_KEYS.USER_INFO.rawValue)
        
        if decoded == nil {
            GLOBAL.GlobalVariables.userInfo = nil
            GLOBAL.GlobalVariables.upgradePackageModel = nil
        } else {
            let decodedInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [String: Any?]
            GLOBAL.GlobalVariables.userInfo = decodedInfo
            GLOBAL.GlobalVariables.upgradePackageModel = UpgradePackageModel(decodedInfo)
            return decodedInfo
        }
        
        return nil
    }
    
    static func getUserId() -> String {
        if GLOBAL.GlobalVariables.userInfo != nil {
            let userId = GLOBAL.GlobalVariables.userInfo!["ID"] as! String
            
            return userId
        }
        
        return ""
    }
    
    static func setIsClickedOnWelcome() {
        let _key = "\(STORAGE_KEYS.CLICKED_ON_WELCOME.rawValue)_\(SIGN_KEY)"
        
        UserDefaults.standard.set(true, forKey: _key)
        UserDefaults.standard.synchronize()
    }
    
    static func getIsClickedOnWelcome() -> Bool {
        let _key = "\(STORAGE_KEYS.CLICKED_ON_WELCOME.rawValue)_\(SIGN_KEY)"
        
        let _value = UserDefaults.standard.bool(forKey: _key)
        
        return _value
    }
    
    static func setAppToken(_ token: String?) {
        GLOBAL.GlobalVariables.appToken = token
        let keyToken = "\(STORAGE_KEYS.APP_TOKEN.rawValue)_\(SIGN_KEY)"
        
        UserDefaults.standard.set(token, forKey: keyToken)
        UserDefaults.standard.synchronize()
    }
    
    static func getAppToken() -> String? {

        let keyToken = "\(STORAGE_KEYS.APP_TOKEN.rawValue)_\(SIGN_KEY)"
        let appToken = UserDefaults.standard.string(forKey: keyToken)
        
        GLOBAL.GlobalVariables.appToken = appToken
        
        return appToken
    }
    
    static func getDeviceId() -> String {
        var deviceId = UserDefaults.standard.string(forKey: STORAGE_KEYS.DEVICE_ID.rawValue)
        
        if deviceId == nil {
            deviceId = NSUUID().uuidString
            
            UserDefaults.standard.set(deviceId, forKey: STORAGE_KEYS.DEVICE_ID.rawValue)
            UserDefaults.standard.synchronize()
        }
        
        
        return deviceId ?? ""
    }
    
    static func imageWithView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    
    static func getHTMLFromContent(content: String, bgColor: String? = nil) -> String {
        var tFormat = Constants.CONTENT_WEB_FORMAT
        
        if let tBgColor = bgColor {
            tFormat = tFormat.replacingOccurrences(of: "BODY_BACKGROUND_COLOR", with: "background-color: \(tBgColor);")
        } else {
            tFormat = tFormat.replacingOccurrences(of: "BODY_BACKGROUND_COLOR", with: "")
        }
        
        
        let htmlCode = String(format: tFormat, content)
        
        return htmlCode
    }
    
    
    static func keepResultSweepstake(_ sweepstakeId: String) {
        UserDefaults.standard.set(sweepstakeId, forKey: STORAGE_KEYS.SWEEPSTAKE_RESULT.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func getSweepstakeId() -> String {
        let sweepstakeId = UserDefaults.standard.string(forKey: STORAGE_KEYS.SWEEPSTAKE_RESULT.rawValue)
        
        return sweepstakeId ?? ""
    }
    
    
    static func handleAddToWallet(atView: UIViewController, _ dealId: String, onBegin _onBegin: DataCallback? = nil, onCompletion _onCompletion: DataCallback? = nil) {
        _onBegin?(nil)
        
        APICommonServices.addDealToWallet(dealId) { (resp) in
            // mPrint("addDealToWallet", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], status == true {
                    NotificationCenter.default.post(name: .UPDATED_WALLET_SUGGESTED_AMAZING, object: data)
                    
                }
                
                _onCompletion?(resp)
                
                Utils.showAlert(atView, "", resp["message"] as? String ?? "")
            } else {
                _onCompletion?(nil)
            }
            
            Utils.dismissLoading()
        }
    }
    
    static func handleGetDealDetail(atView: UIViewController? = nil, _ dealId: String, onBegin _onBegin: DataCallback? = nil, onCompletion _onCompletion: DataCallback? = nil) {
        _onBegin?(nil)
        
        APICommonServices.getDealDetail(dealId) { (resp) in
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] , let detail = data["deal"]  as? [String : Any?] {
                    //mPrint("handleGetDealDetail", detail)
                    
                    _onCompletion?(detail)
                } else {
                    _onCompletion?(nil)
                }
            } else {
                _onCompletion?(nil)
            }
        }
    }
    
    
    
    static func getSweeptakesResult() -> [String: Any?]? {
        let decoded  = UserDefaults.standard.data(forKey: STORAGE_KEYS.SWEEPSTAKE_RESULT.rawValue)
        
        if decoded != nil {
            let decodedInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! [String: Any?]
            
            return decodedInfo
        }
        
        return nil
    }
    
    static func isActivateCard() -> Bool {
        let userInfo = Utils.getUserInfo()
        
        if let tUserInfo = userInfo, let cardId = tUserInfo["CardID"] as? String, cardId != "" {
            return true
        }
        
        return false
    }
    
    static func checkLogin(_ inView: UIViewController?, requireLogin: Bool = true, _ onCompletion: LoginCallback? = nil) {
        if getAppToken() != nil && getUserInfo() != nil {
            onCompletion?(LOGIN_STATUS.SUCCESS_IMMEDIATELY)
        } else {
            if requireLogin {
                if let vc = inView {
                    GLOBAL.GlobalVariables.sourceInstance = vc
                    
                    let flowCallback: LoginCallback = { (status) -> Void in
                        if let sourceVC = GLOBAL.GlobalVariables.sourceInstance {
                            vc.navigationController?.popToViewController(sourceVC, animated: true)
                        } else if let discoveryVC = GLOBAL.GlobalVariables.discoveryInstance {
                            vc.navigationController?.popToViewController(discoveryVC, animated: true)
                        }
                        
                        onCompletion?(status)
                    }
                    
                    let loginVC = LoginViewController()
                    loginVC.flowCallback = flowCallback
                    
                    vc.navigationController?.pushViewController(loginVC, animated: true)
                    
                }
            } else {
                onCompletion?(LOGIN_STATUS.NOT_YET)
            }
        }
    }
    
    static func getCopyright() -> String {
        let calendar = NSCalendar.init(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let currentYear = (calendar?.component(NSCalendar.Unit.year, from: Date()))!
        
        var str = LOCALIZED.copyright_by_tgc_format.translate
        str = str.replacingOccurrences(of: "[%__TAG_YEAR__%]", with: "\(currentYear)")
        
        
        return str
    }
    
    static func getTemplateDeal(_ dealObj: [String: Any?]?) -> TEMPLATE_CARD_DEAL {
        if let tData = dealObj, let num = tData["Template"] as? Int  {
            return TEMPLATE_CARD_DEAL(rawValue: num) ?? .ONE
        }
        
        return .ONE
    }
    
    static func getTemplateSplash(_ templateObject: [String: Any?]?) -> TEMPLATE_SPLASH_SCREEN {
        if let tData = templateObject, let tName = tData["TemplateName"] as? String  {
            return TEMPLATE_SPLASH_SCREEN(rawValue: tName) ?? .ONE
        }
        
        return .ONE
    }
    
    
    static func isValidTimeToGetSplashDeal() -> Bool {
        
        var strTime = UserDefaults.standard.string(forKey: STORAGE_KEYS.TIME_INTERVAL_GET_SPLASH_DEAL.rawValue)
        
        if let sTime = strTime {
            let nowDate = Date()
            let tDate = Utils.dateFromStringDate(sTime, fromFormat: FROM_FORMAT_DEFAULT)
            
            if let cDate = tDate {
                let soonDate = cDate.addingTimeInterval(Constants.TIME_INTERVAL_SPLASH_DEAL)
                
                if soonDate < nowDate {
                    strTime = Utils.stringFromDate(Date(), toFormat: FROM_FORMAT_DEFAULT)
                    UserDefaults.standard.set(strTime, forKey: STORAGE_KEYS.TIME_INTERVAL_GET_SPLASH_DEAL.rawValue)
                    UserDefaults.standard.synchronize()
                    
                    return true
                }
                
            }
            
            return false
            
        } else {
            strTime = Utils.stringFromDate(Date(), toFormat: FROM_FORMAT_DEFAULT)
            UserDefaults.standard.set(strTime, forKey: STORAGE_KEYS.TIME_INTERVAL_GET_SPLASH_DEAL.rawValue)
            UserDefaults.standard.synchronize()
        }
        
        return true
    }
    
    static func generateSweeptakesLink(completion: DataCallback? = nil) {
        var data: [String: Any]?
        data = ["kind": LINK_TYPE.SWEEPTAKES.rawValue]
        
        if let tData = data {
            getDeepLinkWithCustomData(tData) { (resp) in
                completion?(resp)
            }
        }
    }
    
    static func sendEmail(delegate: MFMailComposeViewControllerDelegate & UIViewController, subject: String? = nil) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = delegate
            mail.setToRecipients(["founders@thegcard.com"])
            if let subject = subject {
                mail.setSubject(subject)
            }

            delegate.present(mail, animated: true)
        } else {
            Utils.showAlertView(nil, "Please setup your mail account in able to send an email.")
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    static func getDeepLink(_ type: LINK_TYPE, completion: DataCallback? = nil) {
        var data: [String: Any]?
        
        if type == .INVITE {
            data = ["kind": LINK_TYPE.INVITE.rawValue,
                    "from_app": Constants.BRANCH_CHANNEL]
        } else if type == .MARKETING {
            if let userInfo = GLOBAL.GlobalVariables.userInfo {
                data = ["kind": LINK_TYPE.MARKETING.rawValue,
                        "from_app": Constants.BRANCH_CHANNEL,
                        "ref_code": userInfo["RefCode"] as? String ?? ""]
            }
        } else if type == .SIGN_IN {
            if let userInfo = GLOBAL.GlobalVariables.userInfo {
                data = ["kind": LINK_TYPE.SIGN_IN.rawValue,
                        "from_app": Constants.BRANCH_CHANNEL,
                        "ref_code": userInfo["RefCode"] as? String ?? ""]
            }
        }
        
        if let tData = data {
            getDeepLinkWithCustomData(tData) { (resp) in
                completion?(resp)
            }
        }
        
    }
    
    static  func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    static func openDealDetail(_ vc: UIViewController, _ dealInfo: [String: Any?]?) {
        //mPrint("openDealDetail", dealInfo)"
        guard let deal = try? Wallet.Deal(dict: dealInfo ?? [:]) else { return }
        
        let dealDetailVC = Storyboard.wallet.instantiateViewController(withIdentifier: DealDetailViewController.className()) as! DealDetailViewController
        dealDetailVC.dealInfo = dealInfo
        let vm = DealDetailViewModel(repository: Repository.shared, deal: deal)
        dealDetailVC.viewModel = vm
        
        vc.navigationController?.pushViewController(dealDetailVC, animated: true)
    }
    
    static func handleShowSplashDeal(_ topVC: UIViewController, _ data: [String: Any?]?) {
        if let tData = data, let tTemplate = tData["Template"] as? [String: Any?] {
            
            let typeTpl = Utils.getTemplateSplash(tTemplate)
            
            let splashDealView = SplashDealView().loadTheme(typeTpl)
            
            splashDealView.config(tData, onClickedDetailCallback: { (data) in
                if let tData = data, let dealInfo = tData["Deal"] as? [String: Any?] {
                    Utils.openDealDetail(topVC, dealInfo)
                }
            }, onClickedCloseCallback: { (data) in
                
            })
            
            splashDealView.showInView(topVC)    
        }
    }
    
    static func getDealType(_ data: [String: Any?]?) -> DEAL_TYPE {
        var dealType = DEAL_TYPE.NONE
        
        
        if let dealData = data {
            let needActive = dealData["ActiveCodeRequired"] as? Bool ?? false
            
            if needActive {
                dealType = DEAL_TYPE.SPECIAL_CARD
            } else {
                let dealProgramStr = dealData["DealProgram"] as? String ?? ""
                
                if dealProgramStr == DEAL_PROGRAM.NONE.rawValue {
                    dealType = DEAL_TYPE.UNSWIPED_DAILY
                } else if dealProgramStr == DEAL_PROGRAM.SWIPE.rawValue {
                    let dealTypeStr = dealData["Type"] as? String ?? ""
                    dealType = DEAL_TYPE(rawValue: dealTypeStr) ?? DEAL_TYPE.NONE
                } else {
                    dealType = DEAL_TYPE(rawValue: dealProgramStr) ?? DEAL_TYPE.NONE
                }
            }
        }
        
        return dealType
    }
    
    static func getCarouselDeals(_ list: [[String: Any?]]) -> [[String: Any?]] {
        //        var fList:[[String: Any?]] = []
        //        for item in list {
        //            let dealShowType = item["DealType"] as? String ?? ""
        //
        //            if dealShowType != "" {
        //                fList.append(item)
        //            }
        //        }
        //
        //        return fList
        
        return list
    }
    
    static func showClaimAlert(_ cardData: [String: Any?]?, bgColor: UIColor = UIColor(white: 0, alpha: 0.4), successCallback: SuccessedDataCallback? = nil) {
        let content = ClaimView().loadView()
        
        content.config(cardData, onClickedConfirmCallback: { (data) in
            
        }, onClickedCancelCallback: { (data) in
            
            Utils.closeCustomAlert()
            
        }, onCompletedCallback: { (success, data) in
            if success {
                Utils.closeCustomAlert()
            }
            
            successCallback?(success, data)
        })
        
        Utils.showAlertWithCustomView(content, bgColor: bgColor)
    }
    
    static func doUseDeal(_ cardData: [String: Any?]?, successCallback: SuccessedDataCallback? = nil) {
        Utils.showLoading()
        
        var tParams = [String: Any]()
        tParams["code"] = ""
        if let tData = cardData {
            let instanceDealID = tData["InstanceDealID"] as? String ?? ""
            tParams["instance_deal_id"] = instanceDealID
        }
        
        APICommonServices.doUseDeal(tParams) { (resp) in
            mPrint("claimADeal Not need enter CODE --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool {
                var tDict = [String: Any?]()
                if let data = resp["data"] as? [String: Any?] {
                    NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                    tDict = data
                }
                
                successCallback?(status, cardData)
            }
            
            Utils.dismissLoading()
        }
    }
    
    static func getNormalDeals(_ list: [[String: Any?]]) -> [[String: Any?]] {
        var fList:[[String: Any?]] = []
        for item in list {
            let dealShowType = item["DealType"] as? String ?? ""
            
            if dealShowType == "" {
                fList.append(item)
            }
        }
        
        return fList
    }
    
    static func getDeepLinkWithCustomData(_ data: [String: Any], completion: DataCallback? = nil) {
        let lp: BranchLinkProperties = BranchLinkProperties()
        
        lp.campaign = "gcc referral"
        lp.channel = Constants.BRANCH_CHANNEL
        lp.feature = "sharing"
        lp.stage = "new event"
        lp.tags = ["tgc", "member"]
        
        let buo = BranchUniversalObject.init(canonicalIdentifier: "Member")
        buo.contentMetadata.customMetadata = NSMutableDictionary(dictionary: data)
        buo.getShortUrl(with: lp) { (url, error) in
            completion?(["deepLink": url ?? ""])
        }
        
    }
    
        
    static func getPhoneNumber(_ pSummary: [String: Any?]?) -> String {
        
        if let summary = pSummary {
            
            let phoneNumber = (summary["Phone"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)

            if phoneNumber == "" {
                return ""
            }
            
            let phoneCode = (summary["PhoneCode"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            var finalPhone = ""

            if phoneCode == "" {
                finalPhone = phoneNumber
            } else {
                let firstChar = phoneNumber.prefix(1)
                if firstChar == "0" {
                    let nPhone = phoneNumber.replacingCharacters(in: ...phoneNumber.startIndex, with: "")
                    finalPhone = "\(phoneCode) \(nPhone)"
                } else {
                    finalPhone = "\(phoneCode) \(phoneNumber)"
                }
                
           }
            
            
            return finalPhone
        }
       
        
        return ""
    }
    static func getFullAddress(_ pSummary: [String: Any?]?) -> String {
        if let tData = pSummary {
//            let address = tData["Address"] as? String ?? ""
//            let address2 = tData["Address2"] as? String ?? ""
//            let city = tData["City"] as? String ?? ""
//            let state = tData["State"] as? String ?? ""
//            let country = tData["Country"] as? String ?? ""
//
//            let fullAddress =
//                ((address != "" ? (address + ", ") : "") +
//                    (address2 != "" ? (address2 + ", ") : "") +
//                    (city != "" ? (city + ", ") : "") +
//                    (state != "" ? (state + ", ") : "") +
//                    (country != "" ? country : "")
//            )
            
            let fullAddress = tData["FullAddress"] as? String ?? " "
            
            return fullAddress
        }
        
        return " "
    }
    
    static func getImageFromBase64(_ base64str: String) -> UIImage? {
        let dataDecoded = Data(base64Encoded: base64str, options: .ignoreUnknownCharacters)
        if let tData = dataDecoded {
            let decodedImage = UIImage(data: tData)
            
            if let _  = decodedImage {
                return decodedImage
            }
        }
        
        return nil
    }
    
    static func isValidEmail(_ testStr: String?) -> Bool {
        if testStr == nil {
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func dateFromMilliseconds(_ millisecond : Int, toFormat: String? = TO_FORMAT_DEFAULT) -> Date? {
        
        let cDate = Date(milliseconds: millisecond)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        
        let strDate = dateFormatter.string(from: cDate)
        let dt = dateFormatter.date(from: strDate)
        
        if dt == nil {
            return Date()
        }
        
        return dt
    }
    
    static func dateFromStringDate(_ strDate: String?, fromFormat: String? = FROM_FORMAT_DEFAULT) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        
        var dt = dateFormatter.date(from: strDate!)
        
        if dt == nil {
            let dateFormatterFallback = DateFormatter()
            dateFormatterFallback.dateFormat = FROM_FORMAT_DEFAULT_FALLBACK
            
            dt = dateFormatterFallback.date(from: strDate!)
            
            if dt == nil {
                return Date()
            }
        }
        
        return dt
    }
    
    
    static func stringFromDate(_ date: Date?, fromFormat: String? = FROM_FORMAT_DEFAULT, toFormat: String? = TO_FORMAT_DEFAULT) -> String {
        if date == nil {
            return ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        
        let strDate = dateFormatter.string(from: date!)
        let dt = dateFormatter.date(from: strDate)
        
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        if let tDT = dt {
            return dateFormatter.string(from: tDT)
        }
        
        return ""
    }
    
    static func stringFromMilliseconds(_ milliseconds : Int, toFormat: String? = TO_FORMAT_DEFAULT) -> String {
        if milliseconds == 0 {
            return ""
        }
        
        let cDate = Date(milliseconds: milliseconds)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        let strDate = dateFormatter.string(from: cDate)
        
        return strDate
    }
    
    
    
    static func stringFromStringDate(_ strDate: String?, fromFormat: String? = FROM_FORMAT_DEFAULT, toFormat: String? = TO_FORMAT_DEFAULT) -> String {
        if strDate == nil {
            return ""
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        
        
        var dt = dateFormatter.date(from: strDate!)
        
        if dt == nil {
            let dateFormatterFallback = DateFormatter()
            dateFormatterFallback.dateFormat = FROM_FORMAT_DEFAULT_FALLBACK
            
            dt = dateFormatterFallback.date(from: strDate!)
        }
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        
        if let tDT = dt {
            return dateFormatter.string(from: tDT)
        }
        
        return ""
    }
    
    
    
    static func getAppDelegate() -> AppDelegate? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        return appDelegate
    }
    
    static func stringClassFromString(_ className: String) -> AnyClass! {
        
        /// get namespace
        let namespace = Bundle.main.infoDictionary!["CFBundleExecutable"] as! String;
        
        /// get 'anyClass' with classname and namespace
        let cls: AnyClass = NSClassFromString("\(namespace).\(className)")!;
        
        // return AnyClass!
        return cls;
    }
    
    static func showAlertWithCustomView(_ customView: UIView, bgColor: UIColor = UIColor(white: 0, alpha: 0.4), animation: Bool = true, forceTop: Bool = false, completion: ((_ alertView: CustomAlertView) -> Void)? = nil) {
        customView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        
        let tTag = Date().milliseconds
        
        let alertView = CustomAlertView()
        alertView.tag = tTag
        
        GLOBAL.GlobalVariables.customAlertTags.append(tTag)
        
        alertView.containerView = customView
        
        alertView.show(bgColor, animation: animation, forceTop: forceTop)
        
        if let callback = completion {
            callback(alertView)
        }
        
        GLOBAL.GlobalVariables.rootVC?.changeTapGesturesEnabled(false)
        GLOBAL.GlobalVariables.rootVC?.addLeftGestures()
        
        GLOBAL.GlobalVariables.numberCustomAlertAvailable += 1
        
    }
    
    static func closeCustomAlert(forceTop: Bool = false, _ alertView: CustomAlertView? = nil) {
        if let alert = alertView {
            let tTag = alert.tag
            
            if let itemToRemoveIndex = GLOBAL.GlobalVariables.customAlertTags.firstIndex(of: tTag) {
                GLOBAL.GlobalVariables.customAlertTags.remove(at: itemToRemoveIndex)
            }
            
            alert.close()
        } else {
            if let latestTag = GLOBAL.GlobalVariables.customAlertTags.last, let _alertView = UIView().getKeyWindow().viewWithTag(latestTag) as? CustomAlertView {
                
                if let itemToRemoveIndex = GLOBAL.GlobalVariables.customAlertTags.firstIndex(of: latestTag) {
                    GLOBAL.GlobalVariables.customAlertTags.remove(at: itemToRemoveIndex)
                }
                
                _alertView.close()
            }
        }
        
        GLOBAL.GlobalVariables.rootVC?.changeTapGesturesEnabled(true)
        GLOBAL.GlobalVariables.rootVC?.addLeftGestures()
        
        GLOBAL.GlobalVariables.numberCustomAlertAvailable -= 1
    }
    
    static func showNetworkError() {
        if !GLOBAL.GlobalVariables.isShowNetworkAlert {
            GLOBAL.GlobalVariables.isShowNetworkAlert = true
            
            Utils.showAlertStyle(LOCALIZED.please_check_again_your_network_connection.translate, false, LOCALIZED.txt_ok_title.translate) {
                
                GLOBAL.GlobalVariables.isShowNetworkAlert = false
            }
        }
    }
    
    static func showAlert(_ viewController: UIViewController, _ title: String?, _ message: String?) {
        let alertController = UIAlertController(title: (title != nil) ? title! as String  : "", message: (message != nil) ? message! as String : "", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: LOCALIZED.txt_ok_title.translate, style: .default) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(action1)
        
        viewController.present(alertController, animated: true, completion: nil)
        
    }
    
    static func showAvailableSoon() {
        let alertView = CustomAlert().loadOneButtonView()
        Utils.showAlertWithCustomView(alertView)
        let object = CustomAlertObject(message: LOCALIZED.this_feature_will_be_available_soon.translate,
                                       titleBtnConfirm: LOCALIZED.txt_ok_title.translate,
                                       titleBtnCancel: nil)
        alertView.config(object.dictionary, onClickedConfirmCallback: { _ in
            Utils.closeCustomAlert()
        })
    }
    
    static func showAlertView(_ title: String?, _ message: String?) {
        let alert = UIAlertView.init(title: title ?? "", message: message ?? "", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: LOCALIZED.txt_ok_title.translate)
        
        alert.show()
    }
    
    static func showConfirmAlertView(_ title: String?, _ message: String?, _ titleButtonConfirm: String?, _ titleButtonCancel: String?, confirmCallback: DataCallback?, cancelCallback: DataCallback?) {
        
        let bAlertView = UIBAlertView.init(title: title ?? "", message: message ?? "", confirmButtonTitle: titleButtonConfirm ?? LOCALIZED.txt_ok_title.translate, cancelButtonTitle: titleButtonCancel)
        
        bAlertView?.show { (selectedIndex, selectedTitle, didCancel) in
            
            if didCancel {
                cancelCallback?(nil)
            } else {
                confirmCallback?(nil)
            }
        }
    }
    
    static func showConfirmAlert(_ viewController: UIViewController, _ title: String?, _ message: String?, _ titleButtonConfirm: String?, _ titleButtonCancel: String?, confirmCallback: DataCallback?, cancelCallback: DataCallback?) {
        let alertController = UIAlertController(title: (title != nil) ? title! as String  : "", message: (message != nil) ? message! as String : "", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: (titleButtonConfirm != nil) ? titleButtonConfirm! as String  : LOCALIZED.txt_ok_title.translate, style: .default) { (action:UIAlertAction) in
            if (confirmCallback != nil) {
                confirmCallback!(nil)
            }
            
        }
        
        alertController.addAction(action1)
        
        if cancelCallback != nil ||  titleButtonCancel != nil {
            let action2 = UIAlertAction(title: (titleButtonCancel != nil) ? titleButtonCancel! as String  : LOCALIZED.txt_cancel_title.translate, style: .cancel) { (action:UIAlertAction) in
                cancelCallback?(nil)
            }
            
            alertController.addAction(action2)
        }
        
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertViewAppStyle(_ title: String = "", _ message: String = "", _ success: Bool = true, _ titleButton: String?, confirmCallback: FinishedCallback? = nil) {
        
        let content = NewResultAlertView().loadView()
        
        var tData = [String: Any?]()
        tData["success"] = success
        tData["message"] = message
        tData["title"] = title
        
        content.config(tData, onClickedActionCallback: { (data) in
            Utils.closeCustomAlert()
            
            confirmCallback?()
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    static func showAlertStyle(_ message: String = "", _ success: Bool = true, _ titleButton: String = LOCALIZED.txt_ok_title.translate, confirmCallback: FinishedCallback? = nil) {
        
        let content = CustomAlert().loadOneButtonView()
        
        var tData = [String: Any?]()
        tData["success"] = success
        tData["message"] = message
        tData["titleBtnConfirm"] = titleButton
        
        content.config(tData, onClickedConfirmCallback: { (data) in
            
            confirmCallback?()
        }, onClickedCancelCallback: nil)
        
        Utils.showAlertWithCustomView(content, forceTop: true)
    }
    
    static func showConfirmAlertStyle(_ message: String?, _ titleConfirm: String? = LOCALIZED.txt_ok_title.translate, _ titleCancel: String = "", confirmCallback: FinishedCallback? = nil, cancelCallback: FinishedCallback? = nil) {
        let content = CustomAlert().loadTwoButtonView()
        
        var tData = [String: Any?]()
        tData["message"] = message
        tData["titleBtnConfirm"] = titleConfirm
        tData["titleBtnCancel"] = titleCancel
        
        content.config(tData, onClickedConfirmCallback: { (data) in
            confirmCallback?()
        }, onClickedCancelCallback:  { (data) in
            cancelCallback?()
        })
        
        Utils.showAlertWithCustomView(content, forceTop: true)
    }
    
    static let LOADING_VIEW_TAG = 45673
    
    static func showLoading() {
        if let window =  UIApplication.shared.windows.filter({$0.isKeyWindow}).first ?? UIApplication.shared.keyWindow {
            if var topController = window.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                guard !topController.view.subviews.contains(where: { $0.tag == LOADING_VIEW_TAG }) else { return }
                
                let loadingView = UIView(frame: UIScreen.main.bounds)
                loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                loadingView.tag = LOADING_VIEW_TAG

                let loadingImageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 45, height: 45)))
                loadingImageView.center = UIScreen.main.bounds.center
                loadingImageView.image = UIImage.gifImageWithName("loading")

                loadingView.addSubview(loadingImageView)
                
                topController.view.addSubviewOnce(loadingView)
            }
        }
    }
    
    static func dismissLoading() {
        if let window =  UIApplication.shared.windows.filter({$0.isKeyWindow}).first ?? UIApplication.shared.keyWindow {
            if var topController = window.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                topController.view.subviews.first(where: { $0.tag == LOADING_VIEW_TAG } )?.removeFromSuperview()
            }
        }
    }
    
    static func isIPhoneNotch() -> Bool {
        let tDeviceName = UIDevice.modelName.lowercased()
        if (tDeviceName.contains("x") || tDeviceName.contains("11")) {
            return true
        }
        
        return false
    }
    
    static func callNumber(_ number: String) {
        if let url = URL(string: "tel://\(number)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    static func openURL(_ url: String) {
        if let tUrl = URL(string: url) {
            UIApplication.shared.open(tUrl)
        }
    }
    
    static func buildBenefitDescription(_ descriptions: [[String: Any?]] = [], onCompletedCallback _onCompletedCallback: DataCallback?) {
        let willReceiveView = UIView()
        willReceiveView.backgroundColor = .clear
        willReceiveView.isUserInteractionEnabled = false
        
        
        var latestItem: UIView? = nil
        for index in 0..<descriptions.count {
            
            let info = descriptions[index] as [String: Any?]
            
            let tNode = self.buildNodeReceive(info)
            
            willReceiveView.addSubview(tNode)
            
            if index == (descriptions.count - 1) {
                if latestItem == nil {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(willReceiveView)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                        make.bottom.equalTo(willReceiveView)
                    }
                } else {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(latestItem!.snp.bottom).offset(10)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                        make.bottom.equalTo(willReceiveView)
                    }
                }
            } else {
                if latestItem == nil {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(willReceiveView)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                    }
                } else {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(latestItem!.snp.bottom).offset(10)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                    }
                }
                
            }
            
            latestItem = tNode
            
        }
        
        
        let tDict = ["contentView": willReceiveView] as [String : Any]
        
        _onCompletedCallback?(tDict)
    }
    
    
    static func buildTutorialDescription(_ descriptions: [String?] = [], onCompletedCallback _onCompletedCallback: DataCallback?) {
        let willReceiveView = UIView()
        willReceiveView.backgroundColor = .clear
        willReceiveView.isUserInteractionEnabled = false
        
        
        var latestItem: UIView? = nil
        for index in 0..<descriptions.count {
            
            let text = descriptions[index] ?? " "
            
            let tNode = self.buildNodeDescriptonTutorial(text)
            
            willReceiveView.addSubview(tNode)
            
            if index == (descriptions.count - 1) {
                if latestItem == nil {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(willReceiveView)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                        make.bottom.equalTo(willReceiveView)
                    }
                } else {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(latestItem!.snp.bottom).offset(10)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                        make.bottom.equalTo(willReceiveView)
                    }
                }
            } else {
                if latestItem == nil {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(willReceiveView)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                    }
                } else {
                    tNode.snp.makeConstraints { (make) in
                        make.top.equalTo(latestItem!.snp.bottom).offset(10)
                        make.left.equalTo(willReceiveView)
                        make.right.equalTo(willReceiveView)
                    }
                }
                
            }
            
            latestItem = tNode
            
        }
        
        
        let tDict = ["contentView": willReceiveView] as [String : Any]
        
        _onCompletedCallback?(tDict)
    }
    
    
    static func buildNodeReceive(_ data:[String: Any?]? = nil) -> UIView {
        
        if let tData = data {
            
            let cover = UIView()
            
            let checkImage = UIImageView()
            checkImage.image = UIImage(named: "enable_benefit_ic")
            checkImage.contentMode = .scaleAspectFit
            
            cover.addSubview(checkImage)
            
            let lblWillReceive = UILabel()
            lblWillReceive.numberOfLines = 0
            
            
            lblWillReceive.text = tData["name"] as? String ?? ""
            lblWillReceive.textColor = UIColor("rgb 47 72 88")
            lblWillReceive.font = UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14)
            
            
            cover.addSubview(lblWillReceive)
            
            checkImage.snp.makeConstraints { (make) in
                make.top.equalTo(cover)
                make.left.equalTo(cover)
                make.width.equalTo(18)
                make.height.equalTo(13)
            }
            
            lblWillReceive.snp.makeConstraints { (make) in
                make.top.equalTo(cover)
                make.left.equalTo(checkImage).offset(30)
                make.right.equalTo(cover)
                make.bottom.equalTo(cover)
            }
            
            return cover
        }
        
        
        return UIView()
    }
    
    
    static func buildNodeDescriptonTutorial(_ text: String) -> UIView {
        let cover = UIView()
        
        let checkImage = UIImageView()
        checkImage.image = UIImage(named: "white_check")
        checkImage.contentMode = .scaleAspectFit
        
        cover.addSubview(checkImage)
        
        let lblWillReceive = UILabel()
        lblWillReceive.numberOfLines = 0
        
        
        //lblWillReceive.text = text
        lblWillReceive.backgroundColor = .clear
        lblWillReceive.setAttribute("     \(text)", color: UIColor.white, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15), spacing: 5)
        
        cover.addSubview(lblWillReceive)
        
        checkImage.snp.makeConstraints { (make) in
            make.top.equalTo(cover).offset(2)
            make.left.equalTo(cover)
            make.width.equalTo(13)
            make.height.equalTo(12)
        }
        
        lblWillReceive.snp.makeConstraints { (make) in
            make.top.equalTo(cover)
            make.left.equalTo(cover)
            make.right.equalTo(cover)
            make.bottom.equalTo(cover)
        }
        
        return cover

    }
    
    static func buildBtnCategory(_ category: [String: Any?]?, onClickedCallback _onClickedCallback:DataCallback?, isActive: Bool = false, dynamic: Bool = true) -> UIView {
        let coverButton = HighlightableView()
        coverButton.clipsToBounds = false
        
        let background = UIView()
        background.backgroundColor = UIColor.white
        background.clipsToBounds = false
        background.layer.borderWidth = 0.5
        background.layer.cornerRadius = 7.0
        background.layer.borderColor = isActive ? UIColor(255, 193, 6, 1).cgColor : UIColor(195, 195, 195, 1).cgColor
        if isActive {
            background.layer.applySketchShadow(color: UIColor(197, 140, 0, 1), alpha: 0.22, x: 1, y: 4, blur: 8, spread: 0)
        } else {
            background.layer.applySketchShadow(color: UIColor(0, 0, 0, 1), alpha: 0.1, x: 0, y: 0, blur: 3, spread: 0)
        }
        
        coverButton.addSubview(background)
        
        background.snp.makeConstraints { (make) in
            make.top.equalTo(coverButton)
            make.left.equalTo(coverButton)
            make.right.equalTo(coverButton)
            make.bottom.equalTo(coverButton)
        }
        
        let coverInfo = UIView()
        
        coverButton.addSubview(coverInfo)
        
        coverInfo.snp.makeConstraints { (make) in
            make.top.equalTo(coverButton)
            make.bottom.equalTo(coverButton)
            make.left.equalTo(coverButton)
            make.right.equalTo(coverButton)
        }
        
        
        
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        
        let title = UILabel()
        
        title.textColor = isActive ? UIColor(255, 193, 6) : UIColor(107, 107, 107)
        title.font = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 11.0)
        title.numberOfLines = 3
        title.lineBreakMode = .byWordWrapping
        title.textAlignment = .center
        
        let loadingImage = UIActivityIndicatorView(style: .gray)
        
        if !dynamic {
            title.text = LOCALIZED.txt_more_title.translate
            icon.image = UIImage(named: "more_grey_ic")
            icon.contentMode = .center
        } else {
            loadingImage.hidesWhenStopped = true
            coverInfo.addSubview(loadingImage)
            
            
            loadingImage.startAnimating()
            
            title.text = category?["Name"] as? String
            
            icon.sd_setImage(with:  URL(string: isActive ? category?["ImageUrlSelected"] as? String ?? "" : category?["ImageUrl"] as? String ?? ""), completed: { (_, _, _, _) in
                loadingImage.stopAnimating()
            })
        }
        
        coverInfo.addSubview(icon)
        coverInfo.addSubview(title)
        
        icon.snp.makeConstraints { (make) in
            make.top.equalTo(coverInfo).offset(20)
            make.left.equalTo(coverInfo)
            make.right.equalTo(coverInfo)
            make.height.equalTo(30)
            make.width.equalTo(30)
        }
        
        if dynamic {
            loadingImage.snp.makeConstraints { (make) in
                make.centerX.equalTo(icon)
                make.centerY.equalTo(icon)
            }
        }
        
        title.snp.makeConstraints { (make) in
            make.top.equalTo(icon.snp.bottom).offset(6.0)
            make.left.equalTo(coverInfo.snp.left).offset(2)
            make.right.equalTo(coverInfo.snp.right).offset(-2)
        }
        
        let button = InfoButton(type: .system)
        coverButton.addSubview(button)
        
        button.actionHandler(controlEvents: .touchUpInside) {
            if let callback = _onClickedCallback {
                callback(category)
            }
        }
        
        button.data = category
        
        button.snp.makeConstraints { (make) in
            make.top.equalTo(coverButton)
            make.left.equalTo(coverButton)
            make.right.equalTo(coverButton)
            make.bottom.equalTo(coverButton)
        }
        
        
        return coverButton
    }
    
}

class ClosureSleeve {
    let closure: ()->()
    
    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }
    
    @objc func invoke () {
        closure()
    }
}


extension UILabel {
    func setMiddleLine(_ title: String? = "", color: UIColor = .black, font: UIFont = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13) ?? UIFont.systemFont(ofSize: 13), spacing: CGFloat = 0) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: title ?? "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributeString.length))
        
        self.attributedText = attributeString
    }
    
    func setAttribute(_ text: String? = "", color: UIColor = .black, font: UIFont? = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13), spacing: CGFloat = 2.5) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text ?? "")
        attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: font ?? UIFont.systemFont(ofSize: 13), range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: NSMakeRange(0, attributeString.length))
        
        self.attributedText = attributeString
    }
    
    func clone() -> UILabel{
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        return NSKeyedUnarchiver.unarchiveObject(with: data) as! UILabel
    }
    
    func setTextWithHighlight(_ pText: String = "", _ pColor: UIColor = .black, _ pFont: UIFont?, highlightText: [String], highlightColor: [UIColor], highlightFont: [UIFont?]) {
        var mMutableString = NSMutableAttributedString()
        
        
        mMutableString = NSMutableAttributedString(string: pText, attributes: [
            NSAttributedString.Key.font: pFont ?? UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13),
            NSAttributedString.Key.foregroundColor: pColor
        ])
        
        if highlightText.count == highlightColor.count && highlightColor.count == highlightFont.count {
            for index in 0..<highlightText.count {
                let tText = highlightText[index]
                let tColor = highlightColor[index]
                let tFont = highlightFont[index]
                
                if let tRange = pText.range(of: tText) {
                    let nsRange = pText.nsRange(from: tRange)
                    
                    mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: tColor, range:nsRange)
                    mMutableString.addAttribute(NSAttributedString.Key.font, value: tFont ?? UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13), range:nsRange)
                }
            }
        }
        
        self.attributedText = mMutableString
        
    }
    
    func setTextWithUnderline(_ pText: String = "", _ pColor: UIColor = .black, _ pFont: UIFont?, highlightText: [String], highlightColor: [UIColor], highlightFont: [UIFont?]) {
        var mMutableString = NSMutableAttributedString()
        
        
        mMutableString = NSMutableAttributedString(string: pText, attributes: [
            NSAttributedString.Key.font: pFont ?? UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13),
            NSAttributedString.Key.foregroundColor: pColor
        ])
        
        if highlightText.count == highlightColor.count && highlightColor.count == highlightFont.count {
            for index in 0..<highlightText.count {
                let tText = highlightText[index]
                let tColor = highlightColor[index]
                let tFont = highlightFont[index]
                
                if let tRange = pText.range(of: tText) {
                    let nsRange = pText.nsRange(from: tRange)
                    
                    mMutableString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range:nsRange)
                    mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: tColor, range:nsRange)
                    mMutableString.addAttribute(NSAttributedString.Key.font, value: tFont ?? UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13), range:nsRange)
                }
            }
        }
        
        self.attributedText = mMutableString
        
    }
}

extension UIButton {
    func actionHandler(controlEvents control: UIControl.Event, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: control)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
    
    func debounce(_ closure: @escaping FinishedCallback,_ delay: TimeInterval = 300) {
        self.isUserInteractionEnabled = false
        closure()
        setTimeout({
            self.isUserInteractionEnabled = true
        }, delay)
        
    }
    
    open func setTitleUnderline(_ title: String, color: UIColor = .black, font: UIFont? = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13)) {
        let attrs = [
            NSAttributedString.Key.font: font ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        
        let attributedTitle = NSAttributedString(string: title, attributes: attrs)
        self.setAttributedTitle(attributedTitle, for: .normal)
    }
    
    
    open func centerVertically(padding: CGFloat = 6.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
                return
        }
        
        let totalHeight = imageViewSize.height + titleLabelSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left:  0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
            
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            //top: 0.0,
            top: (totalHeight - imageViewSize.height),
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )
        
        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        
        return false
    }
}

extension UIViewController {
    open func addSubviewAsPresent(_ subviewController: UIViewController, animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        Utils.dismissKeyboard()
        
        subviewController.view.frame = CGRect(x: 0, y: SCREEN_HEIGHT, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        subviewController.view.layoutSubviews()
        
        view.addSubview(subviewController.view)
        self.addChild(subviewController)
        
        subviewController.view.snp.makeConstraints { (make) in
            make.top.equalTo(view)
            make.right.equalTo(view)
            make.left.equalTo(view)
            make.bottom.equalTo(view)
        }
        
        if (animated) {
            UIView.animate(withDuration: 0.3,
                           animations: { [weak self] in
                            guard let self = self else {return}
                            self.parent?.view.layoutIfNeeded()
                },
                           completion:  { finished in
                            completion?(finished)
            })
        }
        
    }
    
    open func dismissFromSuperview(_ animated: Bool = true, completion: ((Bool) -> Void)? = nil) {
        self.view.snp.removeConstraints()
        
        if (animated) {
            if self.view != nil && self.view.superview != nil {
                self.view.snp.updateConstraints { (make) in
                    make.top.equalToSuperview().offset(UIScreen.main.bounds.height)
                    make.left.equalToSuperview()
                    make.width.equalToSuperview()
                    make.height.equalTo(SCREEN_HEIGHT)
                }
                
                UIView.animate(withDuration: 0.5,
                               animations: { [weak self] in
                                guard let self = self else {return}
                                self.parent?.view.layoutIfNeeded()
                    },
                               completion: { finished in
                                completion?(finished)
                })
            }            
        } else {
            self.willMove(toParent: nil)
            self.removeFromParent()
            self.view.removeFromSuperview()
            
            completion?(true)
        }
    }
    
    
    open func addSubviewAsPush(_ subview: UIView, animated: Bool = true) {
        
    }
}

extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        if let slide = viewController as? SlideMenuController {
            return topViewController(slide.mainViewController)
        }
        return viewController
    }
}


extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension UICollectionView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}


extension UIView {
    func setGradient(colors: [UIColor]) {
        let layer = CAGradientLayer()
        layer.frame = self.bounds
        layer.colors = colors
        
        self.layer.addSublayer(layer)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
    func rotate(_ angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        
        self.layer.transform = CATransform3DMakeRotation(radians, 0, 0, 0.5);
        
        //self.transform = CGAffineTransform(rotationAngle: radians)
        
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 4.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI)
        rotateAnimation.duration = duration
        
        if let delegate: CAAnimationDelegate = completionDelegate as! CAAnimationDelegate? {
            rotateAnimation.delegate = delegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    func clearBackgrounds() {
        self.backgroundColor = .clear
        for subview in self.subviews {
            subview.clearBackgrounds()
        }
    }
    
    func removeSubviews() {
        for subview in self.subviews {
            subview.removeSubviews()
            
            subview.removeFromSuperview()
        }
        
    }
}

extension CALayer {
    func applySketchShadow(color: UIColor = UIColor(203, 203, 203, 1), alpha: Float = 1, x: CGFloat = 0, y: CGFloat = 8, blur: CGFloat = 15, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
    func removeSketchShadow() {
        shadowOpacity = 0.0
    }
}

extension UIColor {
    public convenience init(hexFromString: String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    public convenience init(_ red: Int, _ green: Int, _ blue: Int, _ alpha: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
    public convenience init(_ strColor: String) {
        let arr = strColor.split(separator: " ")
        // console.tlog("colorFromString ", arr);
        var red: Int = 0
        var green: Int = 0
        var blue: Int = 0
        var alpha: CGFloat = 1.0
        
        if arr.count == 4 {
            red = Int(arr[1]) ?? 0
            green = Int(arr[2]) ?? 0
            blue = Int(arr[3]) ?? 0
        } else if arr.count == 5 {
            red = Int(arr[1]) ?? 0
            green = Int(arr[2]) ?? 0
            blue = Int(arr[3]) ?? 0
            
            if let doubleValue = Double(arr[4]) {
                alpha = CGFloat(doubleValue)
            }
        }
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
    
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}

class CountryModel {
    var flag: String?
    var alpha3: String?
    var phoneCode: String?
    var name: String?
    var countryCode: String?
    
    required convenience init(_ data : [String: Any?]?) {
        self.init()
        
        if let tData = data {
            self.flag = tData["flag"] as? String ?? ""
            self.alpha3 = tData["alpha3"] as? String ?? ""
            self.name = tData["name"] as? String ?? ""
            self.countryCode = tData["alpha2"] as? String ?? ""
            
            if let aCodes = tData["phone_code"] as? [String], aCodes.count > 0 {
                self.phoneCode = aCodes[0]
            }
        }
    }
}

class InfoButton: UIButton {
    var data: Any?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}

class HighlightableView : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for v in subviews {
            if let btn = v as? UIButton {
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.addTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
        }
    }
    
    deinit {
        for v in subviews {
            if let btn = v as? UIButton {
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.removeTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
        }
    }
    
    @objc func didTouchUp(){
        self.alpha = 1.0
    }
    
    @objc func didTouchDown(){
        self.alpha = 0.7
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension UIImage {
    func rotate(_ radians: CGFloat) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func crop(to:CGSize) -> UIImage {
        
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        guard let newCgImage = contextImage.cgImage else { return self }
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        
        // Create bitmap image from context using the rect
        guard let imageRef: CGImage = newCgImage.cropping(to: rect) else { return self}
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, false, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized ?? self
    }
    
}


class ActiveImageLabelView : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    var labelColor: UIColor?
    //var labelColor: UIColor?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for v in subviews {
            if let btn = v as? UIButton {
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.addTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
            
        }
    }
    
    deinit {
        for v in subviews {
            if let btn = v as? UIButton {
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.removeTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
        }
    }
    
    @objc func didTouchUp() {
        self.alpha = 1.0
        
        for v in subviews {
            if let img = v as? UIImageView {
                let templateImage = img.image?.withRenderingMode(.alwaysOriginal)
                img.image = templateImage
                
            } else if let lbl = v as? UILabel {
                lbl.textColor = labelColor
            }
        }
    }
    
    @objc func didTouchDown() {
        self.alpha = 1.0
        for v in subviews {
            if let img = v as? UIImageView {
                let templateImage = img.image?.withRenderingMode(.alwaysTemplate)
                img.image = templateImage
                img.tintColor = .darkGray
            } else if let lbl = v as? UILabel {
                let cLbl = lbl.clone()
                labelColor = cLbl.textColor
                
                lbl.textColor = UIColor.darkGray
            }
        }
        
    }
}

class ActiveView : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for v in subviews {
            if let btn = v as? UIButton {
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.addTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
        }
    }
    
    deinit {
        for v in subviews {
            if let btn = v as? UIButton {
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
                btn.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
                btn.removeTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
            }
        }
    }
    
    @objc func didTouchUp() {
        self.alpha = 1.0
        
        for v in subviews {
            if let btn = v as? UIButton {
                btn.layer.cornerRadius = self.layer.cornerRadius
                btn.backgroundColor = UIColor.clear
            }
        }
    }
    
    @objc func didTouchDown() {
        self.alpha = 1.0
        for v in subviews {
            if let btn = v as? UIButton {
                btn.layer.cornerRadius = self.layer.cornerRadius
                btn.backgroundColor = UIColor(0, 0, 0, 0.3)
            }
        }
        
    }
}

class HighlightableButton : UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
        self.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
        self.addTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
        self.addTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
        
    }
    
    deinit {
        
        self.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchUpInside)
        self.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchCancel)
        self.removeTarget(self, action: #selector(didTouchUp), for: UIControl.Event.touchDragExit)
        self.removeTarget(self, action: #selector(didTouchDown), for: UIControl.Event.touchDown)
        
    }
    
    @objc func didTouchUp(){
        self.alpha = 1.0
    }
    
    @objc func didTouchDown(){
        self.alpha = 0.7
    }
}


extension StringProtocol {
    func nsRange(from range: Range<Index>) -> NSRange {
        return .init(range, in: self)
    }
}


extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
    
    var lastItem: Element {
        return self[self.endIndex - 1]
    }
    
}

extension String {
    func upperCaseFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func upperCaseFirstLetter() {
        self = self.upperCaseFirstLetter()
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func toDate(format: String? = nil) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format ?? "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: self)
        
        return date
    }
    
    func isoToDate() -> Date? {
        let trimmedIsoString = self.replacingOccurrences(of: "\\.\\d+", with: "", options: .regularExpression)
        let formatter = ISO8601DateFormatter()
        let date = formatter.date(from: trimmedIsoString)
        return date
    }
}

extension CGRect {
    var center: CGPoint { return CGPoint(x: midX, y: midY) }
}

extension UIScrollView {
    
    func addPullToRefresh(_ closure: @escaping ()->()) {
        self.alwaysBounceVertical = true
        self.bounces  = true
        let refreshControl = UIRefreshControl()
        
        //        let title = MLocalized("Pull to refresh")
        //        refreshControl.attributedTitle = NSAttributedString(string: title)
        
        
        let finshedBlock: FinishedCallback = {
            refreshControl.endRefreshing()
            setTimeout({
                closure()
            }, 300)
        }
        
        let sleeve = ClosureSleeve(finshedBlock)
        refreshControl.addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: .valueChanged)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        //self.refreshControl = refreshControl
        self.addSubview(refreshControl)
        
    }
}

extension UITextView {
    func hyperLink(originalText: String, hyperLink: String, urlString: String, color: UIColor = UIColor(hexFromString: "#6B6B6B"), linkColor: UIColor = UIColor(hexFromString: "#E46D57"), font: UIFont = UIFont(name: ROBOTO_FONT.LIGHT.rawValue, size: 13) ?? UIFont.systemFont(ofSize: 13)) {
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: font, range: fullRange)
        self.linkTextAttributes = [
            NSAttributedString.Key.foregroundColor: linkColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
        ]
        
        self.attributedText = attributedOriginalText
    }
}

class CustomTextView: UITextView {
    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer is UILongPressGestureRecognizer {
            gestureRecognizer.isEnabled = false
        }
        
        
        super.addGestureRecognizer(gestureRecognizer)
        
        return
    }
}


extension Date {
    var milliseconds: Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds: Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

//extension WKWebView {
//    override open var safeAreaInsets: UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//    }
//}
