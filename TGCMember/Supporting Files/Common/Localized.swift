import UIKit

public enum LOCALIZED {
    
    case app_name
    case default_notification_channel_name
    case txt_ok_title
    case current_version
    case txt_yes_title
    case txt_no_title
    case k_you_have_denied_the_permission
    case txt_go_to_setting_title
    case txt_cancel_title
    case please_click_back_again_to_exit
    case txt_notification_title
    case can_t_connect_to_server
    case try_again
    case txt_search_title
    case txt_send_title
    case wellcome_to_ogogo
    case sign_up_now_and_enjoy_discounts
    case get_started
    case already_have_an_account
    case txt_sign_in
    case enter_your_email_address
    case enter_password
    case near_me
    case upcoming_events_near_by
    case check_out_these_amazing_offers
    case see_all_underline
    case txt_details_title
    case txt_open_title
    case more_from_format
    case vendor_title_will_go_here
    case open_now
    case txt_view_more
    case txt_hide_title
    case txt_use_title
    case txt_liked_title
    case txt_favourite_title
    case use_deal
    case not_available_today
    case available_only_on
    case steps_to_claim
    case special_condition_to_be_met
    case txt_completed_title
    case txt_setting_title
    case report_issues
    case txt_help_title
    case txt_account_title
    case txt_upgrade_title
    case notifications_and_emails
    case refer_a_vendor
    case txt_my_wallet
    case things_to_do
    case saved_events
    case txt_filters_title
    case search_in
    case your_wallet
    case txt_my_favourites
    case txt_sort_by
    case expiration_date
    case txt_category_title
    case txt_level_title
    case txt_location_title
    case txt_km_title
    case apply_filters
    case txt_clear_title
    case txt_close_title
    case txt_about_title
    case txt_basic_deal
    case txt_deals_title
    case txt_offers_title
    case txt_events_title
    case txt_submit_title
    case txt_deal_title
    case txt_offer_title
    case txt_event_title
    case select_a_category_or_type_keyword
    case txt_available_two_dots
    case enter_a_location
    case txt_today_title
    case txt_tomorrow_title
    case txt_this_week
    case txt_next_week
    case valid_until_format
    case expired_on_format
    case used_on_format
    case txt_gold_title
    case txt_silver_title
    case txt_black_title
    case expiration_date_asc
    case expiration_date_desc
    case distance_asc
    case distance_desc
    case name_asc
    case name_desc
    case txt_bookmark_title
    case txt_share_title
    case txt_contact_title
    case txt_more_title
    case type_your_message_here
    case enter_scratch_code
    case please_enter_scratch_code
    case please_enter_a_valid_email
    case please_enter_at_least_6_characters
    case please_enter_password
    case please_enter_confirm_password
    case password_and_confirm_password_does_not_match
    case please_enter_a_valid_phone_number
    case please_read_and_check_terms_conditions
    case txt_email_title
    case txt_password_title
    case txt_confirm_password
    case txt_next_title
    case txt_done_title
    case forgot_password_ask
    case dont_have_an_account_yet
    case please_sign_up_here
    case by_signing_in_i_accept_the
    case terms_and_conditions
    case txt_sign_up
    case already_have_account
    case sign_in_here
    case i_have_agreed_with
    case txt_phone_number
    case please_enter_phone_number
    case enter_phone_number
    case txt_code_title
    case txt_choose_country
    case txt_search_country
    case txt_activate_card
    case input_scratch_code_on_card
    case how_to_find_the_code
    case txt_activate_title
    case txt_skip_title
    case copyright_by_tgc_format
    case active_now
    case you_re_free_member_now_it_s_time_to_upgrade
    case upgrade_for_more_benefits
    case what_you_will_receive
    case view_history_subscription
    case don_t_want_to_subscribe_anymore
    case txt_deactivate_account
    case cancel_subscription
    case current_plan
    case txt_downgrade_title
    case you_have_joined_ogogo
    case txt_payment_methods
    case txt_card_title
    case txt_confirm_title
    case txt_pay_via
    case your_account_have_been_updated_successfully
    case are_you_sure_you_want_to_downgrade_upgrade_member_format
    case are_you_sure_you_want_to_deactivate_account
    case are_you_sure_you_want_to_cancel_subscription
    case no_i_don_t_have_one
    case text_title_report_scratch_code
    case txt_report_issue
    case access_the_app_without_the_card
    case txt_your_name
    case enter_your_name
    case where_you_bought_the_card_from
    case name_of_sales_person_vendor
    case additional_information
    case txt_send_report
    case it_seems_that_this_card
    case is_this_your_account
    case yes_log_in
    case i_don_t_recognize_this_email
    case txt_notice_title
    case you_can_complete_your_registration
    case txt_complete_registration
    case the_system_has_recorded_your_report
    case place_the_qr_code_in_the_frame
    case scan_qr_code
    case text_continue
    case txt_or_title
    case skip_for_now
    case want_to_try_for_free
    case hint_city_location
    case txt_email_address
    case success_exclamation
    case title_oops
    case msg_upgrade_failed
    case txt_exit_title
    case txt_offer_detail
    case from_to_format
    case txt_free_card
    case txt_no_win
    case value_10000
    case value_1000
    case value_100
    case value_50
    case txt_add_to_wallet
    case please_enter_the_vendor_name
    case enter_vendor_name
    case swipe_offer_everyday
    case click_to_undo
    case refusing_offer
    case blocking_offer
    case adding_to_favorites
    case adding_to_wallet
    case refused_offer
    case added_to_wallet
    case offer_blocked
    case added_to_favorites
    case txt_tap_to_claim
    case please_input_vendor
    case by_confirm_accept
    case enter_code_here
    case you_are_assigned
    case do_you_accept_this_reward_or_not
    case sure_i_accept_take_me_to_the_process
    case no_i_dont_want
    case you_have_been_assigned_to_the_reward_format
    case you_were_assigned_to_the_reward_format
    case change_photo
    case your_photo_will_appear
    case txt_message_title
    case invite_your_friends_via_email_address
    case cannot_choose_image
    case email_format_is_existed
    case successfully_assigned_email
    case expiry_date_format
    case this_feature_will_be_available_soon
    case txt_trips_title
    case text_planning_your_trip
    case plan_my_first_trip
    case about_your_trip
    case what_is_the_name_of_this_trip
    case what_is_the_purpose_of_the_trip
    case where_will_you_plan_to_go
    case eg_tokyo_japan
    case txt_from_title
    case txt_to_title
    case extra_coming_soon
    case how_many_members
    case tags_of_things_that_you_want_to_do
    case txt_add_trip
    case txt_new_trip
    case txt_planned_title
    case txt_past_title
    case txt_edit_trip
    case this_trip_is_empty
    case new_deals_offers_and_events
    case explore_the_area
    case there_is_no_distributor_in
    case tgc_distributor
    case ask_me_later
    case ok_i_got_it
    case txt_get_the_card_scratch_the_code
    case type_a_message
    case txt_type_to_search_in
    case txt_view
    case my_account
    case about_us
    case help_and_support
    case sign_out
    case sign_out_message
    case list_view
    case map_view
    case txt_vendor_title
    case txt_newest_title
    case most_liked
    case use_this_deal_now
    case delete_deal
    case txt_wallet_title
    case txt_map_title
    case click_here_for
    case txt_more_info
    case txt_from_format_to_format
    case vendor_name_format
    case address_format
    case please_check_again_your_network_connection
    case txt_flip_back
    case txt_delete_deal
    case swipe_left_or_right_to_play_with_daily_deals
    case txt_later_title
    case you_just_added_1_deal_into_wallet
    case you_just_refused_1_deal
    case format_friends_invited
    case sorry_no_result_found
    case we_cannot_find_the_item
    case move_to_trash
    case upgrade_to_get_more_benefits
    case input_scratch_code_on_OGOGO_activation_card
    case local
    case global
    case lifetime
    case congratulation
    case you_ve_successfully_upgraded_to
    case error
    case the_scratch_code_you_just_entered_isnt_available
    case are_you_sure_you_want_to_delete_the_deal
    case no_i_dont_want_to
    case yes_please_proceed
    case please_ask_for_their_OGOGO_code
    case ready_to_claim_your_deals_discounts_and_offers
    case it_is_time_to_purchase_an_ogogo_activation_card
    case your_tag_name_allows_you_to
    case benefits_of_tag_name
    case click_here_to_see_vendor_locations
    case tag_name_is_defined_as_within_your_city_region
    case tag_name_is_defined_as_anywhere_around_the_world
    case upgrade_to_tag_name
    case downgrade_to_tag_name
    case txt_notifications_title
    case expired_in_date
    case my_profile
    case txt_membership_title
    case you_ve_successfully_used_deal
    case your_deal_got_something_wrong
    case tag_name_more
    case txt_stored_title
    case turn_on_location_services_to_allow_tag_name
    case txt_error_title
    case txt_chat_title
    case add_your_trip
    case name_of_trip
    case txt_purpose_title
    case enter_name_of_this_trip
    case enter_the_purpose_of_this_trip
    case i_would_like_to_look_for
    case sign_up_get_your_1st_deal
    case congratulations_exclamation
    case you_are_now_eligible_for_lifetime_deals
    case please_enter_your_name
    case remind_me_later
    case here_is_the_free_ogogo
    case enter_your_friends_email_address_to_gift
    case gift_1_code_to_a_friend
    case take_a_look
    case no_thanks
    case text_version_format
    case you_are_now_browsing_as_a_free_member
    case upgrade_to_use_deals
    case txt_login_title
    case tutorial_5_title
    case tutorial_5_message
    case tutorial_4_title
    case tutorial_4_message
    case tutorial_3_title
    case tutorial_3_message
    case tutorial_2_title
    case tutorial_2_message
    case tutorial_1_title
    case tutorial_1_message
    case click_here_to_view
    case click_here
    case enter_code_sign_up
    case choose_your_plan
    case free_member
    case reset_password
    case message_reset_password
    case until_format_tag_date
    case all_deals
    case see_all
    case there_are_no_deals_addded_in_your_trip
    case type_anything_here
    case search_tag_number_of_total
    case plan_your_trip
    case trip_info_star
    case my_trip_name_star
    case please_enter_your_trip_name
    case i_m_going_to_star
    case enter_here
    case txt_from_star
    case txt_to_star
    case select_time
    case extra_info_dot
    case travelling_with
    case im_interested_in
    case going_for
    case txt_edit
    case txt_name_title
    case txt_gender
    case birth_year
    case marriage_status
    case no_kid
    case education_level
    case number_of_kids
    case interested_in
    case language_spoken
    case txt_save
    case edit_profile
    case my_wish_list
    case subscription_history
    case activity_logs
    case contact_tgc_for_help
    case delete_my_acount
    case txt_age_range
    case label_interested
    case partner_name
    case you_are_already_a_tag_member
    case free
    case children
    case child
    case wishlist_info_message
    case okay
    case wishlist_delete_message
    case delete
    case wish_created_title
    case wish_created_message
    case faqs_title
    case free_version_reminder_message
    case new_location_shake_message
    case blue_banner_message
    case please_use_deals_title
    case blue_banner_message_2
    case setting_title
    
    var translate : String {
        switch self {
        case .app_name : return MLocalized("app_name")
        case .default_notification_channel_name : return MLocalized("default_notification_channel_name")
        case .txt_ok_title : return MLocalized("txt_ok_title")
        case .current_version : return MLocalized("current_version")
        case .txt_yes_title : return MLocalized("txt_yes_title")
        case .txt_no_title : return MLocalized("txt_no_title")
        case .k_you_have_denied_the_permission : return MLocalized("k_you_have_denied_the_permission")
        case .txt_go_to_setting_title : return MLocalized("txt_go_to_setting_title")
        case .txt_cancel_title : return MLocalized("txt_cancel_title")
        case .please_click_back_again_to_exit : return MLocalized("please_click_back_again_to_exit")
        case .txt_notification_title : return MLocalized("txt_notification_title")
        case .can_t_connect_to_server : return MLocalized("can_t_connect_to_server")
        case .try_again : return MLocalized("try_again")
        case .txt_search_title : return MLocalized("txt_search_title")
        case .txt_send_title : return MLocalized("txt_send_title")
        case .wellcome_to_ogogo : return MLocalized("wellcome_to_ogogo")
        case .sign_up_now_and_enjoy_discounts : return MLocalized("sign_up_now_and_enjoy_discounts")
        case .get_started : return MLocalized("get_started")
        case .already_have_an_account : return MLocalized("already_have_an_account")
        case .txt_sign_in : return MLocalized("txt_sign_in")
        case .enter_your_email_address : return MLocalized("enter_your_email_address")
        case .enter_password : return MLocalized("enter_password")
        case .near_me : return MLocalized("near_me")
        case .upcoming_events_near_by : return MLocalized("upcoming_events_near_by")
        case .check_out_these_amazing_offers : return MLocalized("check_out_these_amazing_offers")
        case .see_all_underline : return MLocalized("see_all_underline")
        case .txt_details_title : return MLocalized("txt_details_title")
        case .txt_open_title : return MLocalized("txt_open_title")
        case .more_from_format : return MLocalized("more_from_format")
        case .vendor_title_will_go_here : return MLocalized("vendor_title_will_go_here")
        case .open_now : return MLocalized("open_now")
        case .txt_view_more : return MLocalized("txt_view_more")
        case .txt_hide_title : return MLocalized("txt_hide_title")
        case .txt_use_title : return MLocalized("txt_use_title")
        case .txt_liked_title : return MLocalized("txt_liked_title")
        case .txt_favourite_title : return MLocalized("txt_favourite_title")
        case .use_deal : return MLocalized("use_deal")
        case .not_available_today : return MLocalized("not_available_today")
        case .available_only_on : return MLocalized("available_only_on")
        case .steps_to_claim : return MLocalized("steps_to_claim")
        case .special_condition_to_be_met : return MLocalized("special_condition_to_be_met")
        case .txt_completed_title : return MLocalized("txt_completed_title")
        case .txt_setting_title : return MLocalized("txt_setting_title")
        case .report_issues : return MLocalized("report_issues")
        case .txt_help_title : return MLocalized("txt_help_title")
        case .txt_account_title : return MLocalized("txt_account_title")
        case .txt_upgrade_title : return MLocalized("txt_upgrade_title")
        case .notifications_and_emails : return MLocalized("notifications_and_emails")
        case .refer_a_vendor : return MLocalized("refer_a_vendor")
        case .txt_my_wallet : return MLocalized("txt_my_wallet")
        case .things_to_do : return MLocalized("things_to_do")
        case .saved_events : return MLocalized("saved_events")
        case .txt_filters_title : return MLocalized("txt_filters_title")
        case .search_in : return MLocalized("search_in")
        case .your_wallet : return MLocalized("your_wallet")
        case .txt_my_favourites : return MLocalized("txt_my_favourites")
        case .txt_sort_by : return MLocalized("txt_sort_by")
        case .expiration_date : return MLocalized("expiration_date")
        case .txt_category_title : return MLocalized("txt_category_title")
        case .txt_level_title : return MLocalized("txt_level_title")
        case .txt_location_title : return MLocalized("txt_location_title")
        case .txt_km_title : return MLocalized("txt_km_title")
        case .apply_filters : return MLocalized("apply_filters")
        case .txt_clear_title : return MLocalized("txt_clear_title")
        case .txt_close_title : return MLocalized("txt_close_title")
        case .txt_about_title : return MLocalized("txt_about_title")
        case .txt_basic_deal : return MLocalized("txt_basic_deal")
        case .txt_deals_title : return MLocalized("txt_deals_title")
        case .txt_offers_title : return MLocalized("txt_offers_title")
        case .txt_events_title : return MLocalized("txt_events_title")
        case .txt_submit_title : return MLocalized("txt_submit_title")
        case .txt_deal_title : return MLocalized("txt_deal_title")
        case .txt_offer_title : return MLocalized("txt_offer_title")
        case .txt_event_title : return MLocalized("txt_event_title")
        case .select_a_category_or_type_keyword : return MLocalized("select_a_category_or_type_keyword")
        case .txt_available_two_dots : return MLocalized("txt_available_two_dots")
        case .enter_a_location : return MLocalized("enter_a_location")
        case .txt_today_title : return MLocalized("txt_today_title")
        case .txt_tomorrow_title : return MLocalized("txt_tomorrow_title")
        case .txt_this_week : return MLocalized("txt_this_week")
        case .txt_next_week : return MLocalized("txt_next_week")
        case .valid_until_format : return MLocalized("valid_until_format")
        case .expired_on_format : return MLocalized("expired_on_format")
        case .used_on_format : return MLocalized("used_on_format")
        case .txt_gold_title : return MLocalized("txt_gold_title")
        case .txt_silver_title : return MLocalized("txt_silver_title")
        case .txt_black_title : return MLocalized("txt_black_title")
        case .expiration_date_asc : return MLocalized("expiration_date_asc")
        case .expiration_date_desc : return MLocalized("expiration_date_desc")
        case .distance_asc : return MLocalized("distance_asc")
        case .distance_desc : return MLocalized("distance_desc")
        case .name_asc : return MLocalized("name_asc")
        case .name_desc : return MLocalized("name_desc")
        case .txt_bookmark_title : return MLocalized("txt_bookmark_title")
        case .txt_share_title : return MLocalized("txt_share_title")
        case .txt_contact_title : return MLocalized("txt_contact_title")
        case .txt_more_title : return MLocalized("txt_more_title")
        case .type_your_message_here : return MLocalized("type_your_message_here")
        case .enter_scratch_code : return MLocalized("enter_scratch_code")
        case .please_enter_scratch_code : return MLocalized("please_enter_scratch_code")
        case .please_enter_a_valid_email : return MLocalized("please_enter_a_valid_email")
        case .please_enter_at_least_6_characters : return MLocalized("please_enter_at_least_6_characters")
        case .please_enter_password : return MLocalized("please_enter_password")
        case .please_enter_confirm_password : return MLocalized("please_enter_confirm_password")
        case .password_and_confirm_password_does_not_match : return MLocalized("password_and_confirm_password_does_not_match")
        case .please_enter_a_valid_phone_number : return MLocalized("please_enter_a_valid_phone_number")
        case .please_read_and_check_terms_conditions : return MLocalized("please_read_and_check_terms_conditions")
        case .txt_email_title : return MLocalized("txt_email_title")
        case .txt_password_title : return MLocalized("txt_password_title")
        case .txt_confirm_password : return MLocalized("txt_confirm_password")
        case .txt_next_title : return MLocalized("txt_next_title")
        case .txt_done_title : return MLocalized("txt_done_title")
        case .forgot_password_ask : return MLocalized("forgot_password_ask")
        case .dont_have_an_account_yet : return MLocalized("dont_have_an_account_yet")
        case .please_sign_up_here : return MLocalized("please_sign_up_here")
        case .by_signing_in_i_accept_the : return MLocalized("by_signing_in_i_accept_the")
        case .terms_and_conditions : return MLocalized("terms_and_conditions")
        case .txt_sign_up : return MLocalized("txt_sign_up")
        case .already_have_account : return MLocalized("already_have_account")
        case .sign_in_here : return MLocalized("sign_in_here")
        case .i_have_agreed_with : return MLocalized("i_have_agreed_with")
        case .txt_phone_number : return MLocalized("txt_phone_number")
        case .please_enter_phone_number : return MLocalized("please_enter_phone_number")
        case .enter_phone_number : return MLocalized("enter_phone_number")
        case .txt_code_title : return MLocalized("txt_code_title")
        case .txt_choose_country : return MLocalized("txt_choose_country")
        case .txt_search_country : return MLocalized("txt_search_country")
        case .txt_activate_card : return MLocalized("txt_activate_card")
        case .input_scratch_code_on_card : return MLocalized("input_scratch_code_on_card")
        case .how_to_find_the_code : return MLocalized("how_to_find_the_code")
        case .txt_activate_title : return MLocalized("txt_activate_title")
        case .txt_skip_title : return MLocalized("txt_skip_title")
        case .copyright_by_tgc_format : return MLocalized("copyright_by_tgc_format")
        case .active_now : return MLocalized("active_now")
        case .you_re_free_member_now_it_s_time_to_upgrade : return MLocalized("you_re_free_member_now_it_s_time_to_upgrade")
        case .upgrade_for_more_benefits : return MLocalized("upgrade_for_more_benefits")
        case .what_you_will_receive : return MLocalized("what_you_will_receive")
        case .view_history_subscription : return MLocalized("view_history_subscription")
        case .don_t_want_to_subscribe_anymore : return MLocalized("don_t_want_to_subscribe_anymore")
        case .txt_deactivate_account : return MLocalized("txt_deactivate_account")
        case .cancel_subscription : return MLocalized("cancel_subscription")
        case .current_plan : return MLocalized("current_plan")
        case .txt_downgrade_title : return MLocalized("txt_downgrade_title")
        case .you_have_joined_ogogo : return MLocalized("you_have_joined_ogogo")
        case .txt_payment_methods : return MLocalized("txt_payment_methods")
        case .txt_card_title : return MLocalized("txt_card_title")
        case .txt_confirm_title : return MLocalized("txt_confirm_title")
        case .txt_pay_via : return MLocalized("txt_pay_via")
        case .your_account_have_been_updated_successfully : return MLocalized("your_account_have_been_updated_successfully")
        case .are_you_sure_you_want_to_downgrade_upgrade_member_format : return MLocalized("are_you_sure_you_want_to_downgrade_upgrade_member_format")
        case .are_you_sure_you_want_to_deactivate_account : return MLocalized("are_you_sure_you_want_to_deactivate_account")
        case .are_you_sure_you_want_to_cancel_subscription : return MLocalized("are_you_sure_you_want_to_cancel_subscription")
        case .no_i_don_t_have_one : return MLocalized("no_i_don_t_have_one")
        case .text_title_report_scratch_code : return MLocalized("text_title_report_scratch_code")
        case .txt_report_issue : return MLocalized("txt_report_issue")
        case .access_the_app_without_the_card : return MLocalized("access_the_app_without_the_card")
        case .txt_your_name : return MLocalized("txt_your_name")
        case .enter_your_name : return MLocalized("enter_your_name")
        case .where_you_bought_the_card_from : return MLocalized("where_you_bought_the_card_from")
        case .name_of_sales_person_vendor : return MLocalized("name_of_sales_person_vendor")
        case .additional_information : return MLocalized("additional_information")
        case .txt_send_report : return MLocalized("txt_send_report")
        case .it_seems_that_this_card : return MLocalized("it_seems_that_this_card")
        case .is_this_your_account : return MLocalized("is_this_your_account")
        case .yes_log_in : return MLocalized("yes_log_in")
        case .i_don_t_recognize_this_email : return MLocalized("i_don_t_recognize_this_email")
        case .txt_notice_title : return MLocalized("txt_notice_title")
        case .you_can_complete_your_registration : return MLocalized("you_can_complete_your_registration")
        case .txt_complete_registration : return MLocalized("txt_complete_registration")
        case .the_system_has_recorded_your_report : return MLocalized("the_system_has_recorded_your_report")
        case .place_the_qr_code_in_the_frame : return MLocalized("place_the_qr_code_in_the_frame")
        case .scan_qr_code : return MLocalized("scan_qr_code")
        case .text_continue : return MLocalized("text_continue")
        case .txt_or_title : return MLocalized("txt_or_title")
        case .skip_for_now : return MLocalized("skip_for_now")
        case .want_to_try_for_free : return MLocalized("want_to_try_for_free")
        case .hint_city_location : return MLocalized("hint_city_location")
        case .txt_email_address : return MLocalized("txt_email_address")
        case .success_exclamation : return MLocalized("success_exclamation")
        case .title_oops : return MLocalized("title_oops")
        case .msg_upgrade_failed : return MLocalized("msg_upgrade_failed")
        case .txt_exit_title : return MLocalized("txt_exit_title")
        case .txt_offer_detail : return MLocalized("txt_offer_detail")
        case .from_to_format : return MLocalized("from_to_format")
        case .txt_free_card : return MLocalized("txt_free_card")
        case .txt_no_win : return MLocalized("txt_no_win")
        case .value_10000 : return MLocalized("value_10000")
        case .value_1000 : return MLocalized("value_1000")
        case .value_100 : return MLocalized("value_100")
        case .value_50 : return MLocalized("value_50")
        case .txt_add_to_wallet : return MLocalized("txt_add_to_wallet")
        case .please_enter_the_vendor_name : return MLocalized("please_enter_the_vendor_name")
        case .enter_vendor_name : return MLocalized("enter_vendor_name")
        case .swipe_offer_everyday : return MLocalized("swipe_offer_everyday")
        case .click_to_undo : return MLocalized("click_to_undo")
        case .refusing_offer : return MLocalized("refusing_offer")
        case .blocking_offer : return MLocalized("blocking_offer")
        case .adding_to_favorites : return MLocalized("adding_to_favorites")
        case .adding_to_wallet : return MLocalized("adding_to_wallet")
        case .refused_offer : return MLocalized("refused_offer")
        case .added_to_wallet : return MLocalized("added_to_wallet")
        case .offer_blocked : return MLocalized("offer_blocked")
        case .added_to_favorites : return MLocalized("added_to_favorites")
        case .txt_tap_to_claim : return MLocalized("txt_tap_to_claim")
        case .please_input_vendor : return MLocalized("please_input_vendor")
        case .by_confirm_accept : return MLocalized("by_confirm_accept")
        case .enter_code_here : return MLocalized("enter_code_here")
        case .you_are_assigned : return MLocalized("you_are_assigned")
        case .do_you_accept_this_reward_or_not : return MLocalized("do_you_accept_this_reward_or_not")
        case .sure_i_accept_take_me_to_the_process : return MLocalized("sure_i_accept_take_me_to_the_process")
        case .no_i_dont_want : return MLocalized("no_i_dont_want")
        case .you_have_been_assigned_to_the_reward_format : return MLocalized("you_have_been_assigned_to_the_reward_format")
        case .you_were_assigned_to_the_reward_format : return MLocalized("you_were_assigned_to_the_reward_format")
        case .change_photo : return MLocalized("change_photo")
        case .your_photo_will_appear : return MLocalized("your_photo_will_appear")
        case .txt_message_title : return MLocalized("txt_message_title")
        case .invite_your_friends_via_email_address : return MLocalized("invite_your_friends_via_email_address")
        case .cannot_choose_image : return MLocalized("cannot_choose_image")
        case .email_format_is_existed : return MLocalized("email_format_is_existed")
        case .successfully_assigned_email : return MLocalized("successfully_assigned_email")
        case .expiry_date_format : return MLocalized("expiry_date_format")
        case .this_feature_will_be_available_soon : return MLocalized("this_feature_will_be_available_soon")
        case .txt_trips_title : return MLocalized("txt_trips_title")
        case .text_planning_your_trip : return MLocalized("text_planning_your_trip")
        case .plan_my_first_trip : return MLocalized("plan_my_first_trip")
        case .about_your_trip : return MLocalized("about_your_trip")
        case .what_is_the_name_of_this_trip : return MLocalized("what_is_the_name_of_this_trip")
        case .what_is_the_purpose_of_the_trip : return MLocalized("what_is_the_purpose_of_the_trip")
        case .where_will_you_plan_to_go : return MLocalized("where_will_you_plan_to_go")
        case .eg_tokyo_japan : return MLocalized("eg_tokyo_japan")
        case .txt_from_title : return MLocalized("txt_from_title")
        case .txt_to_title : return MLocalized("txt_to_title")
        case .extra_coming_soon : return MLocalized("extra_coming_soon")
        case .how_many_members : return MLocalized("how_many_members")
        case .tags_of_things_that_you_want_to_do : return MLocalized("tags_of_things_that_you_want_to_do")
        case .txt_add_trip : return MLocalized("txt_add_trip")
        case .txt_new_trip : return MLocalized("txt_new_trip")
        case .txt_planned_title : return MLocalized("txt_planned_title")
        case .txt_past_title : return MLocalized("txt_past_title")
        case .txt_edit_trip : return MLocalized("txt_edit_trip")
        case .this_trip_is_empty : return MLocalized("this_trip_is_empty")
        case .new_deals_offers_and_events : return MLocalized("new_deals_offers_and_events")
        case .explore_the_area : return MLocalized("explore_the_area")
        case .there_is_no_distributor_in : return MLocalized("there_is_no_distributor_in")
        case .tgc_distributor : return MLocalized("tgc_distributor")
        case .ask_me_later : return MLocalized("ask_me_later")
        case .ok_i_got_it : return MLocalized("ok_i_got_it")
        case .txt_get_the_card_scratch_the_code : return MLocalized("txt_get_the_card_scratch_the_code")
        case .type_a_message : return MLocalized("type_a_message")
        case .txt_type_to_search_in : return MLocalized("txt_type_to_search_in")
        case .txt_view : return MLocalized("txt_view")
        case .my_account : return MLocalized("my_account")
        case .about_us : return MLocalized("about_us")
        case .help_and_support : return MLocalized("help_and_support")
        case .sign_out : return MLocalized("sign_out")
        case .sign_out_message : return MLocalized("sign_out_message")
        case .list_view : return MLocalized("list_view")
        case .map_view : return MLocalized("map_view")
        case .txt_vendor_title : return MLocalized("txt_vendor_title")
        case .txt_newest_title : return MLocalized("txt_newest_title")
        case .most_liked : return MLocalized("most_liked")
        case .use_this_deal_now : return MLocalized("use_this_deal_now")
        case .delete_deal : return MLocalized("delete_deal")
        case .txt_wallet_title : return MLocalized("txt_wallet_title")
        case .txt_map_title : return MLocalized("txt_map_title")
        case .click_here_for : return MLocalized("click_here_for")
        case .txt_more_info : return MLocalized("txt_more_info")
        case .txt_from_format_to_format : return MLocalized("txt_from_format_to_format")
        case .vendor_name_format : return MLocalized("vendor_name_format")
        case .address_format : return MLocalized("address_format")
        case .please_check_again_your_network_connection : return MLocalized("please_check_again_your_network_connection")
        case .txt_flip_back : return MLocalized("txt_flip_back")
        case .txt_delete_deal : return MLocalized("txt_delete_deal")
        case .swipe_left_or_right_to_play_with_daily_deals : return MLocalized("swipe_left_or_right_to_play_with_daily_deals")
        case .txt_later_title : return MLocalized("txt_later_title")
        case .you_just_added_1_deal_into_wallet : return MLocalized("you_just_added_1_deal_into_wallet")
        case .you_just_refused_1_deal : return MLocalized("you_just_refused_1_deal")
        case .format_friends_invited : return MLocalized("format_friends_invited")
        case .sorry_no_result_found : return MLocalized("sorry_no_result_found")
        case .we_cannot_find_the_item : return MLocalized("we_cannot_find_the_item")
        case .move_to_trash : return MLocalized("move_to_trash")
        case .upgrade_to_get_more_benefits : return MLocalized("upgrade_to_get_more_benefits")
        case .input_scratch_code_on_OGOGO_activation_card : return MLocalized("input_scratch_code_on_OGOGO_activation_card")
        case .local : return MLocalized("local")
        case .global : return MLocalized("global")
        case .lifetime : return MLocalized("lifetime")
        case .congratulation : return MLocalized("congratulation")
        case .you_ve_successfully_upgraded_to : return MLocalized("you_ve_successfully_upgraded_to")
        case .error : return MLocalized("error")
        case .the_scratch_code_you_just_entered_isnt_available : return MLocalized("the_scratch_code_you_just_entered_isnt_available")
        case .are_you_sure_you_want_to_delete_the_deal : return MLocalized("are_you_sure_you_want_to_delete_the_deal")
        case .no_i_dont_want_to : return MLocalized("no_i_dont_want_to")
        case .yes_please_proceed : return MLocalized("yes_please_proceed")
        case .please_ask_for_their_OGOGO_code : return MLocalized("please_ask_for_their_OGOGO_code")
        case .ready_to_claim_your_deals_discounts_and_offers : return MLocalized("ready_to_claim_your_deals_discounts_and_offers")
        case .it_is_time_to_purchase_an_ogogo_activation_card : return MLocalized("it_is_time_to_purchase_an_ogogo_activation_card")
        case .your_tag_name_allows_you_to : return MLocalized("your_tag_name_allows_you_to")
        case .benefits_of_tag_name : return MLocalized("benefits_of_tag_name")
        case .click_here_to_see_vendor_locations : return MLocalized("click_here_to_see_vendor_locations")
        case .tag_name_is_defined_as_within_your_city_region : return MLocalized("tag_name_is_defined_as_within_your_city_region")
        case .tag_name_is_defined_as_anywhere_around_the_world : return MLocalized("tag_name_is_defined_as_anywhere_around_the_world")
        case .upgrade_to_tag_name : return MLocalized("upgrade_to_tag_name")
        case .downgrade_to_tag_name : return MLocalized("downgrade_to_tag_name")
        case .txt_notifications_title : return MLocalized("txt_notifications_title")
        case .expired_in_date : return MLocalized("expired_in_date")
        case .my_profile : return MLocalized("my_profile")
        case .txt_membership_title : return MLocalized("txt_membership_title")
        case .you_ve_successfully_used_deal : return MLocalized("you_ve_successfully_used_deal")
        case .your_deal_got_something_wrong : return MLocalized("your_deal_got_something_wrong")
        case .tag_name_more : return MLocalized("tag_name_more")
        case .txt_stored_title : return MLocalized("txt_stored_title")
        case .turn_on_location_services_to_allow_tag_name : return MLocalized("turn_on_location_services_to_allow_tag_name")
        case .txt_error_title : return MLocalized("txt_error_title")
        case .txt_chat_title : return MLocalized("txt_chat_title")
        case .add_your_trip : return MLocalized("add_your_trip")
        case .name_of_trip : return MLocalized("name_of_trip")
        case .txt_purpose_title : return MLocalized("txt_purpose_title")
        case .enter_name_of_this_trip : return MLocalized("enter_name_of_this_trip")
        case .enter_the_purpose_of_this_trip : return MLocalized("enter_the_purpose_of_this_trip")
        case .i_would_like_to_look_for : return MLocalized("i_would_like_to_look_for")
        case .sign_up_get_your_1st_deal : return MLocalized("sign_up_get_your_1st_deal")
        case .congratulations_exclamation : return MLocalized("congratulations_exclamation")
        case .you_are_now_eligible_for_lifetime_deals : return MLocalized("you_are_now_eligible_for_lifetime_deals")
        case .please_enter_your_name : return MLocalized("please_enter_your_name")
        case .remind_me_later : return MLocalized("remind_me_later")
        case .here_is_the_free_ogogo : return MLocalized("here_is_the_free_ogogo")
        case .enter_your_friends_email_address_to_gift : return MLocalized("enter_your_friends_email_address_to_gift")
        case .gift_1_code_to_a_friend : return MLocalized("gift_1_code_to_a_friend")
        case .take_a_look : return MLocalized("take_a_look")
        case .no_thanks : return MLocalized("no_thanks")
        case .text_version_format : return MLocalized("text_version_format")
        case .you_are_now_browsing_as_a_free_member : return MLocalized("you_are_now_browsing_as_a_free_member")
        case .upgrade_to_use_deals : return MLocalized("upgrade_to_use_deals")
        case .txt_login_title : return MLocalized("txt_login_title")
        case .tutorial_5_title : return MLocalized("tutorial_5_title")
        case .tutorial_5_message : return MLocalized("tutorial_5_message")
        case .tutorial_4_title : return MLocalized("tutorial_4_title")
        case .tutorial_4_message : return MLocalized("tutorial_4_message")
        case .tutorial_3_title : return MLocalized("tutorial_3_title")
        case .tutorial_3_message : return MLocalized("tutorial_3_message")
        case .tutorial_2_title : return MLocalized("tutorial_2_title")
        case .tutorial_2_message : return MLocalized("tutorial_2_message")
        case .tutorial_1_title : return MLocalized("tutorial_1_title")
        case .tutorial_1_message : return MLocalized("tutorial_1_message")
        case .click_here_to_view : return MLocalized("click_here_to_view")
        case .click_here : return MLocalized("click_here")
        case .enter_code_sign_up : return MLocalized("enter_code_sign_up")
        case .choose_your_plan : return MLocalized("choose_your_plan")
        case .free_member : return MLocalized("free_member")
        case .reset_password : return MLocalized("reset_password")
        case .message_reset_password : return MLocalized("message_reset_password")
        case .until_format_tag_date : return MLocalized("until_format_tag_date")
        case .all_deals: return MLocalized("all_deals")
        case .see_all: return MLocalized("see_all")
        case .there_are_no_deals_addded_in_your_trip: return MLocalized("there_are_no_deals_addded_in_your_trip")
        case .type_anything_here: return MLocalized("type_anything_here")
        case .search_tag_number_of_total: return MLocalized("search_tag_number_of_total")
        case .plan_your_trip: return MLocalized("plan_your_trip")
        case .trip_info_star: return MLocalized("trip_info_star")
        case .my_trip_name_star: return MLocalized("my_trip_name_star")
        case .please_enter_your_trip_name: return MLocalized("please_enter_your_trip_name")
        case .i_m_going_to_star: return MLocalized("i_m_going_to_star")
        case .enter_here: return MLocalized("enter_here")
        case .txt_from_star: return MLocalized("txt_from_star")
        case .txt_to_star: return MLocalized("txt_to_star")
        case .select_time: return MLocalized("select_time")
        case .extra_info_dot: return MLocalized("extra_info_dot")
        case .travelling_with: return MLocalized("travelling_with")
        case .im_interested_in: return MLocalized("im_interested_in")
        case .going_for: return MLocalized("going_for")
        case .txt_edit: return MLocalized("txt_edit")
        case .txt_name_title: return MLocalized("txt_name_title")
        case .txt_gender: return MLocalized("txt_gender")
        case .birth_year: return MLocalized("birth_year")
        case .marriage_status: return MLocalized("marriage_status")
        case .no_kid: return MLocalized("no_kid")
        case .education_level: return MLocalized("education_level")
        case .number_of_kids: return MLocalized("number_of_kids")
        case .interested_in: return MLocalized("interested_in")
        case .language_spoken: return MLocalized("language_spoken")
        case .txt_save: return MLocalized("txt_save")
        case .edit_profile: return MLocalized("edit_profile")
        case .my_wish_list: return MLocalized("my_wish_list")
        case .subscription_history: return MLocalized("subscription_history")
        case .activity_logs: return MLocalized("activity_logs")
        case .contact_tgc_for_help: return MLocalized("contact_tgc_for_help")
        case .delete_my_acount: return MLocalized("delete_my_acount")
        case .txt_age_range: return MLocalized("txt_age_range")
        case .label_interested: return MLocalized("label_interested")
        case .partner_name: return MLocalized("partner_name")
        case .you_are_already_a_tag_member: return MLocalized("you_are_already_a_tag_member")
        case .free: return MLocalized("free")
        case .child: return MLocalized("child")
        case .children: return MLocalized("children")
        case .delete: return MLocalized("delete")
        case .okay: return MLocalized("okay")
        case .wishlist_delete_message: return MLocalized("wishlist_delete_message")
        case .wishlist_info_message: return MLocalized("wishlist_info_message")
        case .wish_created_message: return MLocalized("wish_created_message")
        case .wish_created_title: return MLocalized("wish_created_title")
        case .faqs_title: return MLocalized("faqs_title")
        case .free_version_reminder_message: return MLocalized("free_version_reminder_message")
        case .new_location_shake_message: return MLocalized("new_location_shake_message")
        case .blue_banner_message: return MLocalized("blue_banner_message")
        case .please_use_deals_title: return MLocalized("please_use_deals_title")
        case .blue_banner_message_2: return MLocalized("blue_banner_message_2")
        case .setting_title: return MLocalized("setting_title")
        }
    }
}
