//
//  Constants.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static let IS_AR_DEMO: Bool = false
    
    static let DISTANCE_TO_REQUEST_DEAL: Double = 8.0
    static let WIDTH_MENU: CGFloat = 80 * (SCREEN_WIDTH / 100)
    static let OGOGO_CARD_LINK : String = "https://dev-gcc.s3-ap-southeast-1.amazonaws.com/assets/images/ogogo-card.png"
    static let GOOGLE_MAP_STYLE_URL : String = "https://dev-gcc.s3-ap-southeast-1.amazonaws.com/data/style-maps.json"
    static let BRANCH_CHANNEL : String = "member"
    static let DEFAULT_LOCATION: Float = 0.0
    static let TIME_INTERVAL_SPLASH_DEAL: TimeInterval =  0 //seconds
    static let TIME_INTERVAL_UNDO_CARD: TimeInterval =  0 //seconds
    static let TIME_INTERVAL_TOAST_CARD: TimeInterval =  1 //seconds
    static let TIME_INTERVAL_HOLDING_CARD: TimeInterval =  1 //seconds
    static let ALLOW_SWIPE_MENU: Bool = false
    static let BUTTON_SUBMIT_COLOR = "#625BED"
    static let DEFAULT_FONT = ROBOTO_FONT.LIGHT.rawValue
    static let CONTENT_WEB_FORMAT  = "<html><head><style>@font-face{font-family: \"\(DEFAULT_FONT)\"; src: url(\(DEFAULT_FONT).ttf);} body {font-family: \"\(DEFAULT_FONT)\"; font-size: 13; BODY_BACKGROUND_COLOR} img {max-width:100%%; height: auto !important;width:auto !important;}</style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0\"><title></title></head><body>%@</body></head></html>"
}

struct Storyboard {
    static let main = UIStoryboard(name: "Main", bundle: nil)
    static let account = UIStoryboard(name: "Account", bundle: nil)
    static let wallet = UIStoryboard(name: "Wallet", bundle: nil)
    static let wishlist = UIStoryboard(name: "Wishlist", bundle: nil)
    static let messages = UIStoryboard(name: "GroupMessages", bundle: nil)
}

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

// Google Place API
let placeAPIkey = "AIzaSyAZA75ob8AEq7QQ5cjx3Zim1fdzmDoyT-Y"
let tagDateString = "[%__TAG_DATE__%]"
let tagNameString = "[%__TAG_NAME__%]"

//AWS
let defAccessKey = "AKIAR2CLX2YSJB432IU5"
let defSecretKey = "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"
let defBucketName = "dev-gcc"

enum PaddingSide {
    case left(CGFloat)
    case right(CGFloat)
    case both(CGFloat)
}

struct Font {
    static let RobotoBlack = "Roboto-Black"
    static let RobotoRegular = "Roboto-Regular"
    static let RobotoBlackItalic = "Roboto-BlackItalic"
    static let RobotoItalic = "Roboto-Italic"
    static let RobotoThin = "Roboto-Thin"
    static let RobotoThinItalic = "Roboto-ThinItalic"
    static let RobotoLight = "Roboto-Light"
    static let RobotoLightItalic = "Roboto-LightItalic"
    static let RobotoMedium = "Roboto-Medium"
    static let RobotoMediumItalic = "Roboto-MediumItalic"
    static let RobotoBold = "Roboto-Bold"
    static let RobotoBoldItalic = "Roboto-BoldItalic"
}

// DealType
enum DealType: String, Codable {
    case basic
    case gold
    case silver
    case loyalty
    case black
    case favorite
    case unswiped
    case all
    case trip
    
    var iconValue: UIImage? {
        switch self {
        case .basic: return #imageLiteral(resourceName: "g_basic")
        case .black: return #imageLiteral(resourceName: "g_black")
        case .gold: return #imageLiteral(resourceName: "g_gold")
        case .loyalty: return #imageLiteral(resourceName: "g_loyalty")
        case .silver: return #imageLiteral(resourceName: "g_silver")
        case .favorite: return nil
        case .all, .unswiped, .trip: return nil
        }
    }
    
    var bgImageValue: UIImage? {
        switch self {
        case .all: return nil
        case .basic: return #imageLiteral(resourceName: "basic_deal_bg")
        case .black: return #imageLiteral(resourceName: "bg_black")
        case .gold: return #imageLiteral(resourceName: "gold_deal_bg")
        case .silver: return #imageLiteral(resourceName: "silver_deal_bg")
        case .loyalty: return #imageLiteral(resourceName: "rectangle_loyalty")
        case .favorite, .unswiped, .trip: return nil
        }
    }
    
    var pinImageValue: UIImage? {
        switch self {
        case .all: return nil
        case .basic: return #imageLiteral(resourceName: "gpin_basic")
        case .black: return #imageLiteral(resourceName: "gpin_black")
        case .favorite: return nil
        case .gold: return #imageLiteral(resourceName: "gpin_gold")
        case .loyalty: return #imageLiteral(resourceName: "gpin_red")
        case .silver: return #imageLiteral(resourceName: "gpin_silver")
        case .unswiped, .trip: return nil
        }
    }
    
    var badgeImageValue: UIImage? {
        switch self {
        case .all: return nil
        case .basic: return #imageLiteral(resourceName: "badge_basic")
        case .black: return #imageLiteral(resourceName: "badge_black")
        case .gold: return #imageLiteral(resourceName: "badge_gold")
        case .loyalty: return #imageLiteral(resourceName: "badge_red")
        case .silver: return #imageLiteral(resourceName: "badge_silver")
        case .favorite, .unswiped, .trip: return nil
        }
    }
    
    var selectedImageValue: UIImage? {
        switch self {
        case .all: return nil
        case .basic: return #imageLiteral(resourceName: "gpin_basic")
        case .black: return #imageLiteral(resourceName: "gpin_black")
        case .gold: return #imageLiteral(resourceName: "gpin_gold")
        case .loyalty: return #imageLiteral(resourceName: "gpin_red")
        case .silver: return #imageLiteral(resourceName: "gpin_silver")
        case .favorite, .unswiped, .trip: return nil
        }
    }
    
    var orderValue: Int {
        switch self {
        case .all: return 6
        case .basic: return 0
        case .black: return 3
        case .gold: return 5
        case .loyalty: return 2
        case .silver: return 4
        case .favorite, .unswiped, .trip: return 1
        }
    }
}

struct Device {
    static let iPhoneWithNotch = screenHeight >= 812
    static let iPhone5s = screenHeight == 568.0
    static let iPhone4s = screenHeight < 568.0
    static let iPhonePlus = screenHeight == 736.0
    static let iPhoneX = screenHeight == 812.0
    static let iPhoneMaxXr = screenHeight == 896.0
    static let iPhone5sAndLower = screenHeight <= 568.0
    static let iPhone6 = screenHeight <= 667
}

enum SalaryRange: CaseIterable {
    case a
    case b
    case c
    case d
    case e
    case f
    case g
    
    var stringValue: String {
        switch self {
        case .a: return "Less than $1,000"
        case .b: return "$1,000 - $4,999"
        case .c: return "$5,000 - $9,999"
        case .d: return "$10,000 - $14,999"
        case .e: return "$15,000 - $19,999"
        case .f: return "$20,000 - $29,999"
        case .g: return "$30,000 - $39,999"
        }
    }
}
