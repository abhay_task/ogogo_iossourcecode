//
//  Request.swift
//  TGCMember
//
//  Created by jonas on 12/2/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import Foundation

typealias KeyPair<T> = (key: T, value: T)

protocol JSONRequest {
    var parameters: [String: Any] { get }
}

struct LoginRequest: JSONRequest {
    var email: String
    var password: String
    var userID: String
    var pushingToken: String
    
    var parameters: [String : Any] {
        return ["email": email,
                "password": password,
                "user_id": userID,
                "pushing_token": pushingToken]
    }
}

struct UpdateUserProfileRequest: JSONRequest {
    var displayName: String
    var childrenCount: Int
    var gender: KeyPair<String>
    var birthYear: Int
    var marriageStatus: KeyPair<String>
    var educationLevel: KeyPair<String>
    var categories: [Wallet.Category]
    var nationality: String
    var homeAddress: String
    var languages: String
    
    var parameters: [String : Any] {
        return ["display_name": displayName,
                "gender": Int(gender.key) ?? 0,
                "birth_year": birthYear,
                "kid": childrenCount,
                "marriage": Int(marriageStatus.key) ?? 0,
                "education": Int(educationLevel.key) ?? 0,
                "interested_in": categories.compactMap { $0.id }.joined(separator: ","),
                "nationality": nationality,
                "home_address": homeAddress,
                "languages": languages]
    }
}

struct IDRequest: JSONRequest {
    var id: String
    
    var parameters: [String : Any] { ["instance_deal_id": id] }
}

struct SearchRequest: PageRequest {
    var type: DealType = .all
    var page: Int = 1
    var limit: Int = 999
    var activity: DealActivityType? = nil
    var keyword: String?  = nil
    var lat: Double? = nil
    var lng: Double? = nil
    var tripID: String? = nil
    
    var parameters: [String : Any] {
        var p: [String: Any] = {
            ["type": type.rawValue,
             "page": page,
             "limit": limit]
        }()
        
        if let keyword = keyword {
            p["keyword"] = keyword
        }
        
        if let activity = activity {
            p["activity"] = activity.rawValue
        }
        
        if let lat = lat, let lng = lng {
            p["lat"] = lat
            p["lng"] = lng
        }
        
        if let tripID = tripID {
            p["trip_id"] = tripID
        }
        
        return p
    }
}

struct SearchDealsMapRequest: JSONRequest {
    var keyword: String? = nil
    var limit: Int = 100
    let lat: Double
    let lng: Double
    let radius: Float
    let zoom: Float
    
    var parameters: [String : Any] {
        var p:[String: Any] = ["limit": limit,
                               "lat": lat,
                               "lng": lng,
                               "radius": radius,
                               "zoom": zoom]
        
        if let keyword = keyword {
            p["keyword"] = keyword
        }
        
        return p
    }
}

protocol PageRequest: JSONRequest {
    var page: Int { get set }
    var limit: Int { get set }
}


struct PlaceRequest: JSONRequest {
    var input: String? = nil
    var placeID: String? = nil
    
    var parameters: [String : Any] {
        var p = [String: Any]()
        
        p["key"] = placeAPIkey
        if let input = input {
            p["input"] = input
        }
        
        if let placeID = placeID {
            p["placeid"] = placeID
        }
        
        return p
    }
}

struct CreateTripRequest: JSONRequest {
    let id: String?
    let tripPref: TripPreferences

    var parameters: [String : Any] {
        var p: [String: Any] = [
            "name": tripPref.name,
            "address": tripPref.address,
            "lat": tripPref.lat ?? 0.0,
            "lng": tripPref.lng ?? 0.0,
            "from_date": tripPref.fromDate,
            "to_date": tripPref.toDate,
            "travelling_with": tripPref.travellingWith.id,
            "interested_in": tripPref.interestedIn.map { $0.id }.joined(separator: ","),
            "going_for": tripPref.goingFor.id,
            "wishes": tripPref.wishList.map { $0.parameters }
        ]
        
        if let id = id {
            p["id"] = id
        }
        
        return p
    }
}

struct FilterWalletRequest: JSONRequest {
    let categoryIDs: String
    
    var parameters: [String : Any] {
        return ["category_id": categoryIDs]
    }
}

struct UpdateAvatarRequest: JSONRequest {
    let urlString: String
    
    var parameters: [String : Any] { ["avatar_url": urlString] }
}

struct CreateWishRequest: JSONRequest, WishProtocol {
    var id: String? = nil
    let userId: String
    let category: Wallet.Category
    let name: String?
    var categoryName: String? { category.name ?? "" }
    
    var parameters: [String : Any] {
        ["user_id": userId,
         "category_id": category.id ?? "",
         "category_name": category.name ?? "",
         "name": name ?? ""]
    }
}

struct DeleteTripRequest: JSONRequest {
    let tripId: String
    
    var parameters: [String : Any] { ["trip_id": tripId] }
}
//{“name”:“india”,“address”:“India”,“lat”:20.593684,“lng”:78.96288,“from_date”:“2021-01-28",“to_date”:“2021-01-30",“travelling_with”:“72dd829e-bb68-4dbc-ba96-b473fe6392dc”,“interested_in”:“6d5dc2f4-a7a9-4081-8fed-eabed06d007d,4cfa4b7c-daca-4cc4-ad87-50d4de3c0a03,ecde8ecd-2a53-47c3-9d5f-a2e0d9328af5",“going_for”:“b1384a13-982d-4360-85cc-9c07f4ac6b71"}
//"display_name":"andrew durieux","kid":2,"gender":2,"birth_year":1995,"marriage":0,"education":7,"interested_in"
