//
// Color.swift
// Created on 2020-02-10

import Foundation
import UIKit

struct Color {
    static var customBackground: UIColor {
        if #available(iOS 13.0, *) {
            return .systemBackground
        }
        
        return .white
    }
    
    static var customLabel: UIColor {
        if #available(iOS 13.0, *) {
            return .label
        }
        
        return .black
    }
    
    // Dynamic

    //Reversed
    
    // Constant
    static let red = UIColor.create(red: 228, green: 109, blue: 87)
    static let darkBlue = UIColor.create(red: 47, green: 72, blue: 88)
    static let green = UIColor.create(red: 52, green: 166, blue: 95)
    static let yellow = UIColor.create(red: 255, green: 193, blue: 6)
    static let gray = UIColor.create(red: 138, green: 145, blue: 150)
    static let skyBlue = UIColor.create(red: 0, green: 202, blue: 234)
    
    // Gradient colors
    //Basic
    static let basicGradientStart = UIColor.create(red: 58, green: 90, blue: 111)
    static let basicGradientCenter = UIColor.create(red: 93, green: 133, blue: 160)
    static let basicGradientEnd = UIColor.create(red: 32, green: 48, blue: 58)
    
    static let silverGradientCenter = UIColor.create(red: 223, green: 225, blue: 226)
    static let silverGradientEnd = UIColor.create(red: 186, green: 186, blue: 186)
    static let silverGradientStart = UIColor.create(red: 140, green: 142, blue: 144)
    
    static let goldGradientStart = UIColor.create(red: 228, green: 210, blue: 72)
    static let goldGradientCenter = UIColor.create(red: 249, green: 236, blue: 102)
    static let goldGradientEnd = UIColor.create(red: 225, green: 203, blue: 77)
    
    static let blackGradientStart = UIColor.black
    static let blackGradientCenter = UIColor.create(red: 115, green: 115, blue: 115)
    static let blackGradientEnd = UIColor.black
}

extension UIColor {
    class func create(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor(displayP3Red: red.toColorValue, green: green.toColorValue, blue: blue.toColorValue, alpha: 1)
        }
        
        return UIColor(red: red.toColorValue, green: green.toColorValue, blue: blue.toColorValue, alpha: 1)
    }
    
    class func createDynamic(with lightValue: UIColor, darkValue: UIColor) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor(dynamicProvider: { trait -> UIColor in
                if trait.userInterfaceStyle == .dark {
                    return darkValue
                }
                
                return lightValue
            })
        }
        
        return lightValue
    }
}

protocol NumericType {
    var toCGFloat: CGFloat { get }
    var toColorValue: CGFloat { get }
}

extension Int: NumericType {
    var toCGFloat: CGFloat {
        return CGFloat(self)
    }
    
    var toColorValue: CGFloat {
        return CGFloat(self / 255)
    }
}

extension Double: NumericType {
    var toCGFloat: CGFloat {
        return CGFloat(self)
    }
    
    var toColorValue: CGFloat {
        return CGFloat(self / 255)
    }
}

extension CGFloat {
    var toColorValue: CGFloat {
        return self / 255
    }
}


