//
//  UIViewHelper.swift
 

import Foundation
import UIKit

extension UIView {
    func addTapToDismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyb))
        addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyb() {
        Utils.dismissKeyboard()
    }
    
    func addTapToRemoveFromSuperView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(removeFromView))
        addGestureRecognizer(tap)
    }
    
    @objc func removeFromView() {
        removeFromSuperview()
    }
    
    func addOverlayBackground(with color: UIColor = UIColor.black.withAlphaComponent(0.7)) {
        let vw = UIView(frame: UIScreen.main.bounds)
        vw.backgroundColor = color
        vw.tag = 7856
        addSubview(vw)
    }
    
    func removeOverlayBackgroud() {
        viewWithTag(7856)?.removeFromView()
    }
    
    func springAnimation() {
        transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.transform = .identity
            },
                       completion: nil)
    }
    
    func basicAnimate(_ duration: TimeInterval? = nil, method: @escaping () -> Void, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration ?? 0.3, animations: {
            method()
            self.layoutIfNeeded()
        }, completion: { _ in
            completion?()
        })
    }
    
    func addSubviewOnce(_ vw: UIView) {
        if !subviews.contains(vw) {
            addSubview(vw)
        }
    }
    
    func keyFrameAnimate(keyId: String,
                         animation: CAKeyframeAnimation,
                         duration: TimeInterval,
                         values: [Any]?) {
        
        let keyFrameAnimation = animation
        keyFrameAnimation.values = values
        keyFrameAnimation.duration = duration
        keyFrameAnimation.calculationMode = CAAnimationCalculationMode.cubic
        
        layer.add(keyFrameAnimation, forKey: keyId)
    }
    
    func getKeyWindow() -> UIWindow {
        if #available(iOS 13.0, *) {
//            let sceneDelegate = window?.windowScene?.delegate
            return UIApplication.shared.connectedScenes.filter({ $0.activationState == .foregroundActive })
                .map({ $0 as? UIWindowScene })
                .compactMap({ $0 }).first?.windows.filter({ $0.isKeyWindow }).first ?? UIWindow()
        }
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        return appDelegate?.window ?? UIWindow()
    }

    func zoomInAnimate(_ view: UIView? = nil) {
        if let view = view {
            view.addSubview(self)
        }
       
        transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.transform = .identity
        }, completion: nil)
    }
    
    func zoomOutAnimate(completion: (() -> Void)? = nil) {
        transform = .identity
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            self.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        }, completion: { _ in
            completion?()
            self.removeFromSuperview()
        })
    }
    
    func addDarkAlphaBgView(with alpha: CGFloat? = nil) {
        let darkView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        darkView.backgroundColor = UIColor.black.withAlphaComponent(alpha ?? 0.3)
        insertSubview(darkView, at: 0)
    }
    
    func rotationAnimation(duration: Double = 20) {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = duration
        rotation.isCumulative = true
        rotation.repeatCount = .greatestFiniteMagnitude
        rotation.isRemovedOnCompletion = false
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    func passingByAnimation(duration: Double = 10, identifier: String, fromPosition: [Double], toPosition: [Double]) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.fromValue = fromPosition
        animation.toValue = toPosition
        animation.duration = duration
        animation.repeatCount = .greatestFiniteMagnitude
        animation.isRemovedOnCompletion = false
        layer.add(animation, forKey: identifier)
    }
    
    func shake() {
        let midX = center.x
        let midY = center.y
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: midX - 10, y: midY)
        animation.toValue = CGPoint(x: midX + 10, y: midY)
        layer.add(animation, forKey: "position")
    }
    
    func rotate(withAngle: CGFloat) {
        basicAnimate(method: {
            self.transform = CGAffineTransform(rotationAngle: withAngle)
        }, completion: {
            
        })
    }
    
    func slideUpPresent(_ completion: (() -> Void)? = nil) {
        alpha = 0
        frame.origin.y = screenHeight
        basicAnimateWithSpring(0.5, method:  {
            self.frame.origin.y = screenHeight - (self.frame.height)
            self.alpha = 1
        }, completion: { completion?() })
    }
    
    func slideDownDismiss(duration: TimeInterval = 0.4, _ completion: (() -> Void)? = nil) {
        
        basicAnimateWithSpring(duration, method: {
            self.frame.origin.y = screenHeight
        }, completion: {
            completion?()
            self.removeFromSuperview()
        })
    }
    
    func basicAnimateWithSpring(_ duration: TimeInterval = 0.4, delay: Double = 0.0, springDamping: CGFloat = 0.6, initialSpringVelocity: CGFloat = 0.5, method: @escaping (() -> Void), completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            method()
        }, completion: { _ in
            completion?()
        })
    }
    
    func scaleUpAnimate() {
        transform = .identity
        basicAnimate(0.1, method: {
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.layoutIfNeeded()
        })
    }
    
    func scaleDownAnimate() {
        basicAnimateWithSpring(0.1, method: {
            self.transform = .init(scaleX: 0.9, y: 0.9)
        })
    }
    
    func alphaAnimate(to: CGFloat) {
        guard alpha != to else { return }
        basicAnimate(method: {
            self.alpha = to
        })
    }
    
    func fadeInPresent() {
        alpha = 0
         UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func fadeOutDismiss() {
        basicAnimate(0.3, method: {
            self.alpha = 0
        }, completion: {
            self.getKeyWindow().removeBlur()
            self.removeFromSuperview()
        })
    }
    
    func resetUi() {
        basicAnimateWithSpring(0.1, method: {
            self.transform = .identity
        })
    }
    
    func addParallaxEffect() {
        let min = CGFloat(-20)
        let max = CGFloat(20)
        
        let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = min
        xMotion.maximumRelativeValue = max
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = min
        yMotion.maximumRelativeValue = max
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion,yMotion]
        
        if motionEffects.isEmpty {
            addMotionEffect(motionEffectGroup)
        }
    }
    
    func removeAllMotionEffects() {
        motionEffects.forEach { mtnEffect in
            self.removeMotionEffect(mtnEffect)
        }
    }
    
    func removeShadow() {
        layer.shadowRadius = 0
        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOpacity = 0
    }
    
    func addBlurView() {
        let blurEffect = UIBlurEffect(style: .light)
        let vEffectView = UIVisualEffectView(frame: frame)
        vEffectView.effect = blurEffect
        vEffectView.tag = 0090
        
        addSubview(vEffectView)
    }
    
    func removeBlur() {
        viewWithTag(0090)?.removeFromSuperview()
    }
    
    static func loadFromNib() -> Self {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! Self
    }
}
