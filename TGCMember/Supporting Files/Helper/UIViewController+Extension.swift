//
//  UIViewController+Extension.swift
//  TGCMember
//
//  Created by jonas on 12/2/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentVc<T: UIViewController>(with presentation: UIModalPresentationStyle = .overFullScreen,
                                        transition: UIModalTransitionStyle = .coverVertical,
                                        storyboard: UIStoryboard = Storyboard.main,
                                        classType: T.Type,
                                        viewController: ((T) -> Void)? = nil,
                                        completion: (() -> Void)? = nil) {
        let vc = storyboard.instantiateViewController(withIdentifier: classType.className())
        vc.modalPresentationStyle = presentation
        vc.modalTransitionStyle = transition
        
        if let nav = vc as? UINavigationController,
            let vc = nav.viewControllers.first as? T {
            viewController?(vc)
        }
            
        if let vc = vc as? T {
            viewController?(vc)
        }
        
        present(vc, animated: true, completion: completion)
    }
    
    func presentWithPushAnim(vc: UIViewController,
                             viewController: ((UIViewController) -> Void)? = nil,
                             completion: (() -> Void)? = nil) {
        
        vc.modalPresentationStyle = .overFullScreen
        
        if let nav = vc as? UINavigationController,
           let firstVc = nav.viewControllers.first {
            viewController?(firstVc)
        } else {
            viewController?(vc)
        }
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(vc, animated: false, completion: nil)
    }
    
    func dismissWithPopAnim() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)

        if let nav = navigationController {
            nav.dismiss(animated: false)
        } else {
            dismiss(animated: false)
        }
    }
    
    func showVc<T: UIViewController>(with storyboard: UIStoryboard = Storyboard.main,
                                        classType: T.Type,
                                        viewController: ((T) -> Void)? = nil) {
        let vc = storyboard.instantiateViewController(withIdentifier: classType.className())
        
        if let nav = vc as? UINavigationController,
            let vc = nav.viewControllers.first as? T {
//            vc.modalPresentationStyle = .overFullScreen
            viewController?(vc)
        }
            
        if let vc = vc as? T {
//            vc.modalPresentationStyle = .overFullScreen
            viewController?(vc)
        }
        
        show(vc, sender: nil)
    }
    
    
    func showAlert(title: String? = nil, message: String, completion: (() -> Void)? = nil) {
        
        let alertVc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
        }
        
        alertVc.addAction(okAction)
        
        
        present(alertVc, animated: true, completion: completion)
    }
    
    func showConfirmAlert(_ title: String? = nil, message: String, style: UIAlertAction.Style = .default, completion: @escaping (Bool) -> Void) {
        
        let alertVc = UIAlertController(title: title ?? message, message: title != nil ? message : "", preferredStyle: .alert)
        
        let alertAction: UIAlertAction
        
        switch style {
        case .default:
            alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                completion(true)
            }
        case .destructive:
            alertAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
                completion(true)
            }
        default:
            alertAction = UIAlertAction(title: "Ok", style: .default) { _ in
                completion(true)
            }
        }
        
        
        let  cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            completion(false)
        }
        
        alertVc.addAction(alertAction)
        alertVc.addAction(cancelAction)
        
        present(alertVc, animated: true, completion: nil)
    }
}
