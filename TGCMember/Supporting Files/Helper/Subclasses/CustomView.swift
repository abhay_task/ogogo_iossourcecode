//
// CustomView.swift
// Created on 2020-02-13

import UIKit

@IBDesignable
class CustomView: UIView {
    // Radius
    @IBInspectable
    var topCorners: Bool = false
    @IBInspectable
    var bottomCorners: Bool = false
    @IBInspectable
    var leftCorners: Bool = false
    @IBInspectable
    var rightCorners: Bool = false
    
    @IBInspectable
    var cornerRadii: CGSize = .zero
    @IBInspectable
    var perfectRoundedRadius: Bool = false
    @IBInspectable
    var cornerRadius: CGFloat = 0.0
    
    // Border
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            applyBorder()
        }
    }
    @IBInspectable
    var borderColor: UIColor? {
        didSet {
            applyBorder()
        }
    }
    
    // Dark Mode dynamics
    @IBInspectable
    var hasShadowInDarkMode: Bool = true {
        didSet {
            updateUi()
        }
    }
    @IBInspectable
    var hasBorderInDarkMode: Bool = true {
        didSet {
            updateUi()
        }
    }
    
    // Shadow
    @IBInspectable
    var shadowRadius: CGFloat = 0.0
    @IBInspectable
    var shadowOpacity: Float = 0.0
    @IBInspectable
    var shadowOffset: CGSize = .zero
    @IBInspectable
    var shadowColor: UIColor = Color.customLabel
    
    // Blur
    @IBInspectable
    var isBlurView: Bool = false
    
    @IBInspectable
    var customGradColor1: String?
    
    @IBInspectable
    var customGradColor2: String?
    
    lazy var shadowView: UIView = {
        return UIView(frame: self.frame)
    }()
    
    override var isHidden: Bool {
        didSet {
            shadowView.isHidden = isHidden
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if isBlurView {
            let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.insertSubview(blurEffectView, at: 0)
        }
     
        var rectCorners: UIRectCorner = []
        
        if topCorners == true {
            rectCorners = [.topLeft, .topRight]
        } else if bottomCorners == true {
            rectCorners = [.bottomLeft, .bottomRight]
        } else if leftCorners == true {
            rectCorners = [.bottomLeft, .topLeft]
        } else if rightCorners == true {
            rectCorners = [.bottomRight, .topRight]
        }
        
        if !rectCorners.isEmpty {
            let maskPath = UIBezierPath(roundedRect: bounds,
                                        byRoundingCorners: rectCorners,
                                        cornerRadii: cornerRadii)
            let maskLayer = CAShapeLayer()
            maskLayer.frame = bounds
            maskLayer.path = maskPath.cgPath
            layer.mask = maskLayer
        }
        
        layer.masksToBounds = true
        
        let radius =  perfectRoundedRadius ? (frame.height < frame.width ? (frame.height / 2) : (frame.width / 2)) : cornerRadius
        layer.cornerRadius = radius
        
        applyShadow(rectCorners: rectCorners)
        applyBorder()
    }
    
    func applyShadow(rectCorners: UIRectCorner? = nil) {
        guard shadowOpacity > 0 else { shadowView.removeFromSuperview(); return }
        
        let radius = perfectRoundedRadius ? (frame.height < frame.width ? (frame.height / 2) : (frame.width / 2)) : cornerRadius
        
        shadowView.backgroundColor = .clear
        shadowView.layer.shadowColor = shadowColor.cgColor
        
        if let rectCorners = rectCorners, !rectCorners.isEmpty {
            shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: rectCorners, cornerRadii: cornerRadii).cgPath
        } else {
            shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        }
        
        shadowView.layer.shadowOffset = shadowOffset
        shadowView.layer.shadowOpacity = shadowOpacity
        shadowView.layer.shadowRadius = shadowRadius
        shadowView.layer.masksToBounds = true
        shadowView.clipsToBounds = false
        shadowView.tag = 333
        shadowView.frame.origin = frame.origin

//        self.superview?.addSubview(shadowView)

        self.superview?.subviews.first(where: { $0.tag == 333 })?.removeFromSuperview()
        self.superview?.insertSubview(shadowView, belowSubview: self)
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        updateUi()
    }
    
    func updateUi() {
        if #available(iOS 13, *) {
            if traitCollection.userInterfaceStyle == .dark {
                if !hasShadowInDarkMode {
                    shadowOpacity = 0
                }
                
                if !hasBorderInDarkMode {
                    borderWidth = 0
                }
            }
        }
    }
    
    func applyBorder() {
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
    }
}

extension UIView {
    
}
