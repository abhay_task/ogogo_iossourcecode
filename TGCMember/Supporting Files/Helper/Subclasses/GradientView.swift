//
//  GradientView.swift
//  TGCMember
//
//  Created by jonas on 7/30/21.
//  Copyright © 2021 Ogogo. All rights reserved.
//

import Foundation

class GradientButton: CustomButton {
    func applyGradient(_ type: DealType?) {
        guard let type = type else { return }
        
        layer.sublayers?.first(where: { $0.name == "GradientLayer" })?.removeFromSuperlayer()
        
        var colors = [CGColor]()
        
        setBackgroundImage(nil, for: .normal)
        
        switch type {
        case .all: return
        case .basic: colors = [Color.basicGradientStart, Color.basicGradientCenter, Color.basicGradientEnd].map { $0.cgColor }
        case .black: colors = [Color.blackGradientStart, Color.blackGradientCenter, Color.blackGradientEnd].map { $0.cgColor }
        case .favorite: return
        case .gold: colors = [Color.goldGradientStart, Color.goldGradientCenter, Color.goldGradientEnd].map { $0.cgColor }
        case .loyalty: setBackgroundImage(type.bgImageValue, for: .normal)
        case .silver: colors = [Color.silverGradientStart, Color.silverGradientCenter, Color.silverGradientEnd].map { $0.cgColor }
        case .unswiped, .trip: return
        }
    
        let gradLayer = CAGradientLayer()
        gradLayer.frame = bounds
        gradLayer.colors = colors
        gradLayer.startPoint = CGPoint(x: 0.0, y: 0.2)
        gradLayer.endPoint = CGPoint(x: 1.0, y: 0.2)
        gradLayer.name = "GradientLayer"
        
        layer.insertSublayer(gradLayer, at: 0)
    }
}
