//
// FloatingTextField.swift
// Created on 2020-02-13

import UIKit

@IBDesignable
class FloatingTextField: UITextField {
    
    @IBInspectable
    var borderColor: UIColor = Color.customLabel
    
    @IBInspectable
    var bottomBorderWidth: CGFloat = 1
    
    var border: CALayer!
    
    var isTyping: Bool = false
    
    var shouldHaveBorder: Bool = true
    
    override func awakeFromNib() {
        
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                   attributes: [NSAttributedString.Key.foregroundColor: textColor!.withAlphaComponent(0.5)])
        backgroundColor = .clear
        
        border = CALayer()
        
        border.name = "bottomBorder"
        let borderWidth: CGFloat = bottomBorderWidth
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width:  frame.size.width  + 30.0, height: frame.size.height)
        border.borderWidth = borderWidth

        if layer.sublayers?.contains (where: { $0.name == border.name }) == false, shouldHaveBorder {
            borderStyle = .none
            layer.addSublayer(border)
            layer.masksToBounds = true
        }
        
        // Set left side image
        //        leftViewMode = .always
        //        let imageView = UIImageView(frame: CGRect(x: -5.0, y: 0, width: 20.0, height: 20.0))
        //        let image = UIImage(named: "email_icon")
        //        imageView.image = image
        //        imageView.contentMode = .scaleAspectFit
        //        leftView = imageView
        
        // Set bottom border
        
        
        
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)

        guard border != nil else { return }
        
        layer.sublayers?.first(where: { $0.name == border.name })?.removeFromSuperlayer()
        layoutSubviews()
//        draw(frame)
    }
}
