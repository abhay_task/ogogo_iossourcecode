//
// CustomTextField.swift
// Created on 2020-02-13

import UIKit


enum TextFieldStatus {
    case valid
    case warning
    case invalid
    case notDetermined
}

@IBDesignable
class CustomTextField: UITextField {
    //Corner Radius
    @IBInspectable
    var topCorners: Bool = false
    @IBInspectable
    var bottomCorners: Bool = false
    @IBInspectable
    var leftCorners: Bool = false
    @IBInspectable
    var rightCorners: Bool = false
    
    @IBInspectable
    var cornerRadii: CGSize = .zero
    @IBInspectable
    var perfectRoundedRadius: Bool = false
    @IBInspectable
    var cornerRadius: CGFloat = 0.0
    
    // Border
    @IBInspectable
    var borderWidth: CGFloat = 0
    @IBInspectable
    var borderColor: UIColor?
    
    // Shadow
    @IBInspectable
    var shadowRadius: CGFloat = 0.0
    @IBInspectable
    var shadowOpacity: Float = 0.0
    @IBInspectable
    var shadowOffset: CGSize = .zero
    @IBInspectable
    var shadowColor: UIColor = Color.customLabel
    
    lazy var shadowView: UIView = {
        return UIView(frame: self.frame)
    }()
    
    private var status: TextFieldStatus = .notDetermined {
        didSet {
            updateStatus()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addPadding(.both(20))
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        var rectCorners: UIRectCorner = []
        
        if topCorners == true {
            rectCorners = [.topLeft, .topRight]
        } else if bottomCorners == true {
            rectCorners = [.bottomLeft, .bottomRight]
        } else if leftCorners == true {
            rectCorners = [.bottomLeft, .topLeft]
        } else if rightCorners == true {
            rectCorners = [.bottomRight, .topRight]
        }
        
        if !rectCorners.isEmpty {
            let maskPath = UIBezierPath(roundedRect: bounds,
                                        byRoundingCorners: rectCorners,
                                        cornerRadii: cornerRadii)
            let maskLayer = CAShapeLayer()
            maskLayer.frame = bounds
            maskLayer.path = maskPath.cgPath
            layer.mask = maskLayer
        }
        
        layer.masksToBounds = true
        
        let radius =  perfectRoundedRadius ? (frame.height < frame.width ? (frame.height / 2) : (frame.width / 2)) : cornerRadius
        layer.cornerRadius = radius
        
        applyShadow()
        applyBorder()
    }
    
    func applyShadow() {
        guard shadowOpacity > 0 else { shadowView.removeFromSuperview(); return }
        
        let radius = perfectRoundedRadius ? (frame.height < frame.width ? (frame.height / 2) : (frame.width / 2)) : cornerRadius
        
        shadowView.backgroundColor = .clear
        shadowView.layer.shadowColor = shadowColor.cgColor
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        shadowView.layer.shadowOffset = shadowOffset
        shadowView.layer.shadowOpacity = shadowOpacity
        shadowView.layer.shadowRadius = shadowRadius
        shadowView.layer.masksToBounds = true
        shadowView.clipsToBounds = false

        self.superview?.addSubview(shadowView)
        self.superview?.bringSubviewToFront(self)
    }
    
    func applyBorder() {
        guard borderWidth > 0 else { return }
        
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
    }
    
    func triggerInvalidField() {
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                   attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
    }
    
    private func updateStatus() {
        switch status {
        case .invalid:
            triggerInvalidField()
        case .notDetermined:
            break
        case .valid:
            break
        case .warning:
            break
        }
    }
    
    func setStatus(_ status: TextFieldStatus) {
        guard status != self.status else { return }
        
        self.status = status
    }
}
