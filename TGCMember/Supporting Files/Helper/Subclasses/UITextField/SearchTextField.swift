//
// SearchTextField.swift
// Created on 2020-02-13

import UIKit

@IBDesignable
class SearchTextField: UITextField {
    
    @IBInspectable
    var placeHolderTextColor: UIColor = .groupTableViewBackground {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "Search",
                                                       attributes: [NSAttributedString.Key.foregroundColor: placeHolderTextColor])
        }
    }
    @IBInspectable
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0.0
        }
    }
    
    func rightIcon(_ image: UIImage) {
        let iconContainerView: UIButton = UIButton(frame: CGRect(x: 15, y: 0, width: 42, height: 42))
        iconContainerView.setImage(image, for: .normal)
        iconContainerView.imageView?.contentMode = .scaleAspectFit
        
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func leftIcon(_ image: UIImage) {
        let iconContainerView: UIButton = UIButton(frame: CGRect(x: 15, y: 0, width: 42, height: 42))
        iconContainerView.setImage(image, for: .normal)
        iconContainerView.imageView?.contentMode = .scaleAspectFit
        
        leftView = iconContainerView
        leftViewMode = .always
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        borderStyle = .none
        
        
//        font = UIFont(name: Font.regular, size: 17)
        textColor = .black
        placeHolderTextColor = UIColor.black.withAlphaComponent(0.3)
        
        rightIcon(#imageLiteral(resourceName: "x-search-icon"))
        leftIcon(#imageLiteral(resourceName: "search-icon"))
    }
}
