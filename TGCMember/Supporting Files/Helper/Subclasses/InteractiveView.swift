//
// InteractiveView.swift
// Created on 2020-02-13

import UIKit

@IBDesignable
class InteractiveView: CustomView {
    
    var onTap: ((CustomView) -> Void)?
    
    @IBInspectable var shouldAnimateOnPress: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupGestures()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        guard shouldAnimateOnPress else { return }
        
        shadowView.scaleDownAnimate()
        scaleDownAnimate()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        guard shouldAnimateOnPress else { return }
        
        shadowView.resetUi()
        resetUi()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        guard shouldAnimateOnPress else { return }
        
        guard let touch = touches.first else { return }
        
        let yRange = 0...frame.height
        let xRange = 0...frame.width
        
        if !(yRange ~= touch.location(in: self).y) || !(xRange ~= touch.location(in: self).x) {
            shadowView.resetUi()
            resetUi()
            return
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        guard shouldAnimateOnPress else { return }
        shadowView.resetUi()
        resetUi()
    }
    
    func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func didTapView() {
        onTap?(self)
    }
}
