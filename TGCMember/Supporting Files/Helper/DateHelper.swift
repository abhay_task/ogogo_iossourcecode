//
//  DateHelper.swift/

import Foundation
import AFDateHelper

extension Date {
    
    func withoutTime() -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let myString = formatter.string(from: self)
        let yourDate = formatter.date(from: myString)
        
        return yourDate ?? Date()
    }
    
    func dayToInt() -> Int {
        let string = toString(format: "dd")
        return Int(string) ?? 0
    }
    
    func monthToInt() -> Int {
        let string = toString(format: "MM")
        return Int(string) ?? 0
    }
    
    func toString(format: String = "yyyy-MM-dd", forTime: Bool = false) -> String {
        let formatter = DateFormatter()
        if forTime {
            formatter.timeZone = TimeZone(identifier: "UTC")
        }
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let myString = formatter.string(from: self) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        
        formatter.locale = Locale(identifier: "en") // set locale to reliable US_POSIX
//        formatter.timeZone = TimeZone(abbreviation: "UTC")
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate ?? Date())
        
        return myStringafd
    }
    
    func toTimeStringWithTimeZone() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss a"
        let myString = formatter.string(from: self) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "HH:mm"
        // again convert your date to string
        let myStringafd = "\(formatter.string(from: yourDate ?? Date())) \(formatter.timeZone.abbreviation(for: yourDate ?? Date()) ?? "")"
        
        return myStringafd
    }
    
    func isInSameWeek(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .weekOfYear)
    }
    func isInSameMonth(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .month)
    }
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }
    func isInSameDay(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .day)
    }
    var isInThisWeek: Bool {
        return isInSameWeek(date: Date())
    }
    var isInToday: Bool {
        return Calendar.current.isDateInToday(self)
    }
    var isInTheFuture: Bool {
        return Date() < self
    }
    var isInThePast: Bool {
        return self < Date()
    }
    
    func getDaysInMonth() -> Int{
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        if let range = calendar.range(of: .day, in: .month, for: date) {
            let numDays = range.count
            
            return numDays
        }
        
        return 0
    }
    
    static func months() -> [String] {
        return Calendar.current.monthSymbols
    }

    static func getnumberOfDays(in month: Int, year: Int) -> Int {
        guard let date = Date(fromString: "\(year)-\(month)-01", format: .custom("yyy-mm-dd")) else { return 0 }
        return date.getDaysInMonth()
    }
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {

        let currentCalendar = Calendar.current

        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }

        return end - start
    }
    
    func isDaysPassed(number: Int) -> Bool {
        let nowDate = Date()
        let untilDate = nowDate.adjust(.day, offset: -number)
        
        return untilDate.compare(self).rawValue == compare(nowDate).rawValue
    }
}
