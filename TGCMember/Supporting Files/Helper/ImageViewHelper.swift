//
//  ImageViewHelper.swift

import Foundation
import UIKit//import Lottie
//
//struct LoadingImageIndicator: Indicator {
//    let view: UIView = UIView()
//    let lottie = LOTAnimationView(name: "loading-image")
//
//    func startAnimatingView() {
//        view.isHidden = false
//        lottie.play()
//    }
//    func stopAnimatingView() {
//        lottie.stop()
//        view.isHidden = true
//    }
//
//    init(frame: CGRect) {
//        lottie.loopAnimation = true
//        lottie.frame = frame
//        lottie.contentMode = .scaleAspectFill
//        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//        view.contentMode = .scaleAspectFill
//        view.backgroundColor = .clear
//        view.addSubview(lottie)
//    }
//}
//
//extension UIImageView {
//    func setKfImage(_ urlString: String) {
//        let urlString = urlString as NSString
//        guard let urlStringEcoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
//            let url = URL(string: urlStringEcoded) else { return }
//
//        contentMode = .scaleAspectFill
//        kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "loading-placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
//    }
//
//    func setFlagImage(_ shortAlpha: String) {
//        let flag = UIImage(named: shortAlpha)
//        image = flag
//    }
//}

extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func scaleImage(newHeight: CGFloat) -> CGSize {
        let scale = newHeight / size.height
        let newWidth = size.width * scale
    
        return CGSize(width: newWidth, height: newHeight)
    }
    
    func withColor(color: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()

        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(.normal)

        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        context.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()

        return newImage
    }
}
