//
//  VendorSummaryViewController.swift
//  TGCMember
//
//  Created by vang on 5/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class VendorSummaryViewController: UIViewController {

    @IBOutlet weak var btnVendorTitle: UIButton!

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var listSuggested:[[String: Any?]] = []
    private var data:[String: Any?]?
    private var summaryData:[String: Any?]?
    var onClickedAnchorCallback: DataCallback?
    
    private var beginScroll: Bool = false
    var currentEventIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
 
        self.collectionView.register(UINib(nibName: "SummaryItemCell", bundle: nil), forCellWithReuseIdentifier: "SummaryItemCell")
        
        self.collectionView.panGestureRecognizer.addTarget(self, action: #selector(_handleScrollViewGestureRecognizer(_:)))
        self.collectionView.addObserver(self, forKeyPath:"contentOffset", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    @objc private func _handleScrollViewGestureRecognizer(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            beginScroll = true
        case .ended:
            beginScroll = false
            
            setTimeout({
                self.collectionView.scrollToItem(at: IndexPath(row: self.currentEventIndex, section: 0), at: .left, animated: true)
            }, 200)
        default:
            break
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if !beginScroll {
            if let dictChange = change, let newPoint: CGPoint = dictChange[NSKeyValueChangeKey.newKey] as? CGPoint {
                //print("CHANGE OBSERVED: \(newPoint.x)")
                
                let index = newPoint.x / (UIScreen.main.bounds.width - 50)
                let roundIndex = round(index)
                
                if (currentEventIndex != Int(max(roundIndex, 0))) {
                    currentEventIndex = Int(max(roundIndex, 0))
                    
                    self.collectionView.scrollToItem(at: IndexPath(row: currentEventIndex, section: 0), at: .left, animated: true)
           
                }
                
            }
        }
        
    }

    @IBAction func onClickedAnchor(_ sender: Any) {
        if let callback = self.onClickedAnchorCallback {
            callback(self.data)
        }
    }
    
    @IBAction func onClickVendorTitle(_ sender: Any) {
        let vendorDetailVC = VendorDetailViewController()
        vendorDetailVC.summaryData = self.summaryData
        if let mData = self.data, let vendorId = mData["ID"] as? String {
            vendorDetailVC.vendorId = vendorId
        }
        
        vendorDetailVC.initTab = VENDOR_INIT_TAB.ABOUT
        self.navigationController?.pushViewController(vendorDetailVC, animated: true)
    }
    
    func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        print("Passing all touches to the next view (if any), in the view stack.")
        return false
    }
    
    private func getVendorSummaryById(_ summaryId: String) {
        APICommonServices.getVendorSummary(summaryId) { (resp) in
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?]  {
    
                    let summary = data["vendor_summary"] as? [String: Any?]
                    self.summaryData = summary
                    
                    let list = data["list_suggest"]  as? [[String : Any?]] ?? []
                    self.listSuggested = list
                  
                    self.collectionView.reloadData()
                    
                    if self.listSuggested.count > 0 {
                        self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: false)
                    }
                    
                }
            }
        }
    }
    
    
    public func updateData(_ data: [String: Any?]) {
        self.data = data
        
        if let mData = self.data, let vendorName = mData["Name"] as? String {
            self.btnVendorTitle.setTitle(vendorName, for: .normal)
        }
        
        if let mData = self.data, let summaryId = mData["SummaryID"] as? String {
            self.getVendorSummaryById(summaryId)
        }
    }

}


// MARK: - UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension VendorSummaryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listSuggested.count
    }
    
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let data = listSuggested[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SummaryItemCell", for: indexPath) as! SummaryItemCell
        
        cell.config(data, onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data, let type = tData["Type"] as? String {
                if type == SUGGEST_TYPE.EVENT.rawValue {
                    let eventDetailVC = EventDetailViewController()
                    eventDetailVC.eventInfo = tData
                    self.navigationController?.pushViewController(eventDetailVC, animated: true)
                } else if type == SUGGEST_TYPE.OFFER.rawValue {
                    let vendorDetailVC = VendorDetailViewController()
                    vendorDetailVC.summaryData = self.summaryData
                    vendorDetailVC.initTab = VENDOR_INIT_TAB.BASIC_DEAL
                    self.navigationController?.pushViewController(vendorDetailVC, animated: true)
                } else if type == SUGGEST_TYPE.DEAL.rawValue {
                    let dealDetailVC = DealDetailViewController()
                    dealDetailVC.dealInfo = tData
                    self.navigationController?.pushViewController(dealDetailVC, animated: true)
                }
            }
            
            setTimeout({
                self.collectionView.scrollToItem(at: IndexPath(row: self.currentEventIndex, section: 0), at: .left, animated: true)
            }, 1000)
            
            
        })
        
        
        return cell

    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: UIScreen.main.bounds.width - 50, height:  99)
    }
    

}
