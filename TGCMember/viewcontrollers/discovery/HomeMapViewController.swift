//
//  ViewController.swift
//  InteractiveAnimations
//
//  Created by Nathan Gitter on 9/4/17.
//  Copyright © 2017 Nathan Gitter. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import GoogleMaps
import SnapKit
import SDWebImage

// MARK: - State



// MARK: - View Controller


class HomeMapViewController: BaseViewController {
    
    @IBOutlet weak var coverCenterPicker: UIView!
    private let boundsNumber = 3
    
    var locationManager: CLLocationManager?
    var tappedMarker : GMSMarker?
    var viewMarkerSelected : GMarker?
    var pullQuickVC: PullQuickViewController?
    
    private var isViewDidAppear: Bool = false
    // MARK: - Constants
    private var latestPosition: Position = .bottom
    
    private var userLocation: CLLocationCoordinate2D?
    private var lastLocation: CLLocationCoordinate2D?
    private var queryLocation: CLLocationCoordinate2D?
    
    private var userMarker = GMSMarker()
    private var willZoomToLocation: Bool = true
    private var popupOffset: CGFloat = 0
    
    private var currentDealsList: [[String: Any?]] = []
    private var markersList: [GMSMarker] = []
    private var isDataReady: Bool = false
    private let selectedMarker = GMarker().loadSelectedMarker()
    
    private var splashDealView: SplashDealView?
    
    @IBOutlet weak var maskMapView: UIView!
    
    private var isShowSummary: Bool = false
    
    private var limit: Int = 10
    private var nextPage: Int = 1
    private var totalPage: Int = 1
    private var totalRecord: Int = 1
    private var canNotLoadMore: Bool = false
    
    private var cardOnScreen: CardOnScreenView?
    
    // MARK: - IBOutlet
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var pullMaskView: UIView!
    @IBOutlet weak var coverCurrentLocation: UIView!
    @IBOutlet weak var bottomLocationConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCategoryView: UIView!
    
    @IBOutlet weak var bottomCenterPickerConstraint: NSLayoutConstraint!
    
    var categoryView: CategoryView?
    var tabClickeCallback: TabClickedCallback?
    // MARK: - IBAction
    
    private var latestBoundsVisible: GMSCoordinateBounds?
    private let zompStep: Float = 15
    private var zoomMap: Float = 5
    private var modeDefault: Bool = true
    private var firstTimeGetList: Bool = true
    private var isFetchedLoyalty: Bool = false
    private var didMoveMapview: Bool = false
    private var latestRadius: Float = 20
    private var latestZoom: Float = 5
    //    private func onClickedMyLocation() {
    //        if let myLocation = self.userLocation {
    //            self.animateToPoint(myLocation)
    //        }
    //    }
    
    @IBAction func onClickedMyLocation(_ sender: Any) {
        if let myLocation = self.userLocation {
            self.animateToPoint(myLocation)
            
            setTimeout({
               self.handleFilterWithText(true, showLoading: true, needBounds: false, pickFromCenter: true)
            }, 800)
        }
    }
    
    @IBAction func onClickedListView(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.LIST_VIEW.rawValue)
    }
    
    @IBAction func onClickedMenu(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.MENU.rawValue)
        
    }
    
    @IBAction func onClickedWallet(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.WALLET_VIEW.rawValue)
    }
    
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        decorate()
        
        Utils.updatePushingToken()
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = false
        self.mapView.setMinZoom(5, maxZoom: 17)
        
        
        self.configMapStyle()
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectedDealOnCarousel(_:)), name: .ON_SELECTED_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onReloadPage(_:)), name: .REFRESH_HOME_PAGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onSelectedCategory(_:)), name: .ON_SELECTED_CATEGORY_SEARCH, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(unFocusAllDeals(_:)), name: .UN_FOCUS_DEAL_ON_MAP, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(filterWithText(_:)), name: .FILTER_WITH_TEXT_ON_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshListCarousel(_:)), name: .REFRESH_LIST_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshHomeMapAfterAuthorized(_:)), name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onClickedMoreStoredDeal(_:)), name: .ON_CLICKED_MORE_STORE_ON_CAROUSEL, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkLocationAgain(_:)), name: .FORCE_ALLOW_PERMISSION_LOCATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateCategoryViewData(_:)), name: .ON_UPDATE_CATEGORY_VIEW, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePositionLocation(_:)), name: .UPDATED_POSITION_LOCATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePositionPickerCenter(_:)), name: .UPDATED_POSITION_PICKER_CENTER, object: nil)
        
        
        //        Utils.generateSweeptakesLink { (data) in
        //            if let tData = data {
        //                let url = tData["deepLink"] as? String ?? ""
        //
        //                mPrint(".SWEEPTAKES -> ", url)
        //            }
        //        }
        //
        //
        //        Utils.checkLogin(self, requireLogin: false) { (status) in
        //            if status != .NOT_YET {
        //                Utils.getDeepLink(.MARKETING, completion: { (data) in
        //                    if let tData = data {
        //                        let url = tData["deepLink"] as? String ?? ""
        //
        //                        mPrint(".MARKETING", url)
        //                    }
        //                })
        //
        //                Utils.getDeepLink(.INVITE, completion: { (data) in
        //                    if let tData = data {
        //                        let url = tData["deepLink"] as? String ?? ""
        //
        //                        mPrint(".INVITE", url)
        //                    }
        //                })
        //
        //                Utils.getDeepLink(.SIGN_IN, completion: { (data) in
        //                    if let tData = data {
        //                        let url = tData["deepLink"] as? String ?? ""
        //
        //                        mPrint(".SIGN_IN", url)
        //                    }
        //                })
        //            }
        //        }
        //
        
        displayCategory()
    }
    
    @objc func onUpdateCategoryViewData(_ notification: Notification) {
        self.categoryView?.updateData()
    }
    
    
    
    private func configMapStyle() {
        APICommonServices.fetchContentFromJSON(Constants.GOOGLE_MAP_STYLE_URL) { (resp) in
            if let tResp = resp, let tData = tResp["data"] as? [[String: Any?]] {
                
                let jsonData = try! JSONSerialization.data(withJSONObject: tData, options: .prettyPrinted)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    //print("fetchContentFromJSON STRING --> ", jsonString)
                    
                    setTimeout({
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(jsonString: jsonString)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }, 0)
                } else {
                    if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }
                }
            }
        }
    }
    
    private func displayCategory() {
        self.categoryView = CategoryView().loadView()
        
        if let cateView = self.categoryView {
            self.coverCategoryView.addSubview(cateView)
            
            cateView.snp.makeConstraints { (maker) in
                maker.left.equalToSuperview()
                maker.top.equalToSuperview()
                maker.right.equalToSuperview()
                maker.bottom.equalToSuperview()
            }
        }
    }
    
    private func layoutTopViewIfNeed(_ stickyPoint: CGFloat, _ fullHeight: CGFloat) {
        let tSpace = fullHeight - stickyPoint
        let maxLeft: CGFloat = 75.0
        let anchor: CGFloat = 100
        
        if tSpace < anchor {
            let _space = anchor - tSpace
            let percent = min(_space, maxLeft) / maxLeft
            let value = percent * maxLeft
            
            //print("did drag to percent --->  \(percent)")
            self.pullMaskView?.backgroundColor = UIColor("rgba 255 255 255 \(percent)")
        } else {
            self.pullMaskView?.backgroundColor = .clear
        }
    }
    
    @objc func updatePositionLocation(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let stickyPoint = uData["stickyPoint"] as? CGFloat ?? 0.0
            let fullHeight = uData["fullHeight"] as? CGFloat ?? 0.0
            
            self.layoutTopViewIfNeed(stickyPoint, fullHeight)
            let tConstant = min(stickyPoint, SCREEN_HEIGHT/2)
            
            self.bottomLocationConstraint?.constant = tConstant
            
        }
    }
    
    @objc func updatePositionPickerCenter(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let stickyPoint = uData["stickyPoint"] as? CGFloat ?? 0.0
            let fullHeight = uData["fullHeight"] as? CGFloat ?? 0.0
            let position = uData["position"] as? Position ?? Position.bottom
            
            let tConstant = min(stickyPoint, SCREEN_HEIGHT/2)
            
            print("position --- >", position)
            
            //self.bottomCenterPickerConstraint?.constant = tConstant
        }
    }
    
    
    
    @objc func checkLocationAgain(_ notification: Notification) {
        fetchCurrentLocation()
    }
    
    @objc func refreshHomeMapAfterAuthorized(_ notification: Notification) {
        self.firstTimeGetList = true
        GLOBAL.GlobalVariables.selectedDeal = nil
        GLOBAL.GlobalVariables.selectedCategory = nil
        GLOBAL.GlobalVariables.selectedSearchText = ""
        self.tappedMarker = nil
        
        self.getListDealsInitMap()
    }
    
    @objc func onClickedMoreStoredDeal(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            self.handleShowMoreStoredDeal(uData)
        }
        
    }
    
    @objc func filterWithText(_ notification: Notification) {
        self.handleFilterWithText()
    }
    
    @objc func refreshListCarousel(_ notification: Notification) {
        var isNeedBounds: Bool = true
        var keepState: Bool = false
        var computeAgain: Bool = true
        if let uData = notification.object as? [String: Any] {
            isNeedBounds  = uData["IsNeedBounds"] as? Bool ?? true
            keepState = uData["KeepState"] as? Bool ?? false
            computeAgain = uData["ComputeAgain"] as? Bool ?? true
        }
        
        self.handleFilterWithText(false, showLoading: false, needBounds: isNeedBounds, pickFromCenter: keepState, computeBoundAgain: computeAgain)
    }
    
    
    private func handleFilterWithText(_ resetMarker: Bool = true, showLoading: Bool = true, needBounds: Bool = true, pickFromCenter: Bool = false, computeBoundAgain: Bool = true) {
        if resetMarker {
            self.tappedMarker = nil
        }
        
        if let category = GLOBAL.GlobalVariables.selectedCategory {
            let categoryId = category["ID"] as? String ?? ""
            self.getListDealsByCategory(categoryId: categoryId, showLoading: showLoading, needBouds: needBounds, pickFromCenter: pickFromCenter, computeBoundAgain: computeBoundAgain)
        } else {
            self.getListDealsInitMap(showLoading: showLoading, needBounds: needBounds, pickFromCenter: pickFromCenter, computeBoundAgain: computeBoundAgain)
        }
        
    }
    
    private func decorate() {
        self.coverCurrentLocation?.backgroundColor = .white
        self.coverCurrentLocation?.layer.cornerRadius = 10.0
        self.coverCurrentLocation?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.bottomCenterPickerConstraint?.constant = (SCREEN_HEIGHT - 50)/2
    }
    
    private var originalPullUpControllerViewSize: CGSize = .zero
    
    private func onTapFirstDealOnMap() {
        if self.currentDealsList.count > 0 {
            let deal = self.currentDealsList[0]
            GLOBAL.GlobalVariables.selectedDeal = deal
            self.handleHighlightDealOnMap(deal["ID"] as? String ?? "", isCallback: true)
        }
    }
    
    
    private func isNeedLoadMore(_ data: [String: Any?]) -> Bool {
        
        var result = false
        
        let index = self.currentDealsList.firstIndex(where: {$0["ID"] as? String == data["ID"] as? String }) ?? 0
        
        let checkIndex: Int = self.limit *  max(max(1, self.nextPage - 1) - 4, 1)
        
        if index >= (checkIndex - 1) {
            if self.nextPage <= self.totalPage {
                if self.currentDealsList.count < self.totalRecord {
                    result = true
                }
            }
        }
        
        
        return result
    }
    
    
    private func showClaimAlert(_ pData: [String: Any?]?, completion: SuccessedDataCallback? = nil) {
        if let cardData = pData {
            Utils.showClaimAlert(cardData, bgColor: UIColor(0, 0, 0, 0.1)) {  [weak self] (success, data) in
                guard let self = self else {return}
                completion?(success, data)
                
                if success {
                    Utils.closeCustomAlert()
                    
                    GLOBAL.GlobalVariables.selectedDeal = nil
                    self.tappedMarker = nil
                    
                    setTimeout({
                        NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                    }, 200)
                }
            }
        }
    }
    
    private func showTEO(_ data: [String: Any?]?) {
        let vc = InviteFriendsViewController()
        vc.data = data
        vc.callback = { [weak self] (success) in
            guard let self = self else {return}
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func gotoDailyDeals(_ data: [String: Any?]?) {
        if let tData = data, let deals = tData["UnswipedDeals"] as? [[String: Any?]] {
            self.openSwipeDeals(deals, false, true, false)
        }
    }
    
    private func doMoveToTrash(_ cardData: [String: Any?]?, completed: SuccessedCallback? = nil) {
        if let tData = cardData, let instanceDealID = tData["InstanceDealID"] as? String {
            Utils.showLoading()
            mPrint("doMoveToTrash -> ", tData)
            APICommonServices.removeDealFromStore(instanceDealID) { (resp) in
                mPrint("doMoveToTrash --> ", resp)
                if let resp = resp {
                    let status = resp["status"] as? Bool ?? false
                    completed?(status)
                    
                    if status {
                        GLOBAL.GlobalVariables.selectedDeal = nil
                        self.tappedMarker = nil
                        
                        NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                    } else {
                        Utils.showAlertView("", resp["message"] as? String ?? "")
                    }
                }
                
                Utils.dismissLoading()
            }
        }
    }
    
    private func handleShowMoreStoredDeal(_ data: [String: Any?]?) {
        let content = MoreDealStoreView().loadView()
        
        content.config(data, onClickedCloseCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            }, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                
                self.handleWhenClickedOnCard(data)
                
        })
        
        Utils.showAlertWithCustomView(content, bgColor: UIColor("rgba 47 72 88 0.8"), animation: false)
    }
    
    private func handleWhenClickedOnCard(_ data: [String: Any?]?, showPreview: Bool = false) {
        self.cardOnScreen = CardOnScreenView().loadView()
        self.cardOnScreen?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        
        let tData = [
            "cardData": data,
            "cardPoint": self.cardOnScreen?.frame.center
            ] as [String : Any?]
        
        if showPreview {
            self.cardOnScreen?.config(tData, onClickedCloseCallback: { [weak self] (data) in
                       guard let self = self else {return}
                       
                       GLOBAL.GlobalVariables.isShowLoyaltyFirst = false
                       Utils.closeCustomAlert()
                       
                       }, onClickedUseDealCallback: { [weak self] (pData) in
                           guard let self = self else {return}
                           
                           if let tData = pData, let claimCode = tData["ClaimCode"] as? String, claimCode == "" {
                               Utils.doUseDeal(tData) { [weak self] (success, data) in
                                   guard let self = self else {return}
                                   
                                   
                                   Utils.showResultUseDealAlert(success, tData) { [weak self] (success, pData) in
                                       guard let self = self else {return}
                                       
                                       if success {
                                           GLOBAL.GlobalVariables.isShowLoyaltyFirst = false
                                           Utils.closeCustomAlert()
                                           
                                           GLOBAL.GlobalVariables.selectedDeal = nil
                                           self.tappedMarker = nil
                                           
                                           setTimeout({
                                               NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                                           }, 200)
                                           
                                           if let tData = pData as? [String: Any?], let fromMoreStored = tData["fromMoreStored"] as? Bool, fromMoreStored == true {
                                               NotificationCenter.default.post(name: .UPDATE_MORE_STORED_AGAIN, object: tData)
                                           }
                                       }
                                   }
                               }
                           } else {
                               self.showClaimAlert(pData) { (success, data) in
                                   if success {
                                       GLOBAL.GlobalVariables.isShowLoyaltyFirst = false
                                       Utils.closeCustomAlert()
                                       
                                       if let tData = data as? [String: Any?], let fromMoreStored = tData["fromMoreStored"] as? Bool, fromMoreStored == true {
                                           NotificationCenter.default.post(name: .UPDATE_MORE_STORED_AGAIN, object: tData)
                                       }
                                   }
                               }
                           }
                       }, onClickedDeleteCallback: { [weak self] (data) in
                           guard let self = self else {return}
                           
                           self.doMoveToTrash(data) { (success) in
                               if success {
                                   GLOBAL.GlobalVariables.isShowLoyaltyFirst = false
                                   Utils.closeCustomAlert()
                               }
                               
                               if let tData = data, let fromMoreStored = tData["fromMoreStored"] as? Bool, fromMoreStored == true {
                                   NotificationCenter.default.post(name: .UPDATE_MORE_STORED_AGAIN, object: tData)
                               }
                           }
                           
                       }, onClickedChatCallback: { [weak self] (data) in
                           guard let self = self else {return}
                           
                           if let tData = data, let summary = tData["VendorSummary"] as? [String: Any?] {
                               self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
                           }
                       }, onClickedUpgradeCallback: { [weak self] (data) in
                           guard let self = self else {return}
                           
                           let upgradeVC = NewUpgradeViewController()
                           
                           if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
                               upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
                           }
                           
                           GLOBAL.GlobalVariables.discoveryInstance?.navigationController?.pushViewController(upgradeVC, animated: true)
                       }, onClickedCardCallback: { [weak self] (data) in
                           guard let self = self else {return}
                           
//                           if let topVC = GLOBAL.GlobalVariables.discoveryInstance {
//                               Utils.openDealDetail(topVC, data)
//                           }
                   })
                   
                   if let content = self.cardOnScreen {
                       Utils.showAlertWithCustomView(content, bgColor: UIColor(0, 0, 0, 0.8), animation: false)
                   }
        } else {
            if let topVC = GLOBAL.GlobalVariables.discoveryInstance {
                Utils.openDealDetail(topVC, data)
            }
        }
        
       
    }
    
    
    @objc func onSelectedDealOnCarousel(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            let dealId = uData["ID"] as? String ?? ""
            self.handleHighlightDealOnMap(dealId, isCallback: false)
        }
    }
    
    @objc func unFocusAllDeals(_ notification: Notification) {
        self.dismissSelectedDealOnMap()
    }
    
    @objc func onSelectedCategory(_ notification: Notification) {
        self.canNotLoadMore = false
        if let uData = notification.object as? [String: Any] {
            
            let categoryId = uData["ID"] as? String ?? ""
            
            self.getListDealsByCategory(categoryId: categoryId, showLoading: true, needBouds: false)
        } else {
            self.handleFilterWithText()
        }
    }
    
    
    func getListDealsInitMap(showLoading: Bool = true, needBounds: Bool = true, pickFromCenter: Bool = false, needCover: Bool = true, computeBoundAgain: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        self.isDataReady = false
        
        //        Utils.getPlace(for: CLLocation(latitude: self.queryLocation?.latitude ?? 0, longitude: self.queryLocation?.longitude ?? 0)) { placemark in
        //            guard let placemark = placemark else { return }
        //
        //            var output = "Our location is:"
        //            if let country = placemark.country {
        //                output = output + "\n\(country)"
        //            }
        //            if let state = placemark.administrativeArea {
        //                output = output + "\n\(state)"
        //            }
        //            if let town = placemark.locality {
        //                output = output + "\n\(town)"
        //            }
        //
        //            print("QUERY FROM --> \(output) <---")
        //        }
        
        
        let params = self.getRequestParams(needCover, computeBoundAgain)
        
        mPrint("PARAMS getListDealsInitMap -> ", params)
        
        APICommonServices.getListDealsOnMapFirst(params as [String : Any]) { (resp) in
            //mPrint("getListDealsOnMapFirst", resp)
            
            self.handleDealAfterFetch(resp, byCategory: false, needBounds: needBounds, pickFromCenter: pickFromCenter)
            
        }
    }
    
    private func getListDealsByCategory(categoryId: String, showLoading: Bool = true, needBouds: Bool = true, pickFromCenter: Bool = false, computeBoundAgain: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        let params = self.getRequestParams()
        
        //mPrint("Param getListDealsByCategory -> ", params)
        
        APICommonServices.getMoreDealsOnMapByCategory(params as [String : Any]) { (resp) in
            //mPrint("getListDealsByCategory --> ", resp)
            
            self.handleDealAfterFetch(resp, byCategory: true, needBounds: needBouds, pickFromCenter: pickFromCenter)
        }
        
    }
    
    private func getRequestParams(_ needCover: Bool = true, _ computeBoundAgain: Bool = true) -> [String: Any] {
        let radius = self.getMapVisibleRadius()
        
        var params: [String: Any] = [
            "keyword": GLOBAL.GlobalVariables.selectedSearchText,
            "page": 0,
            "limit": 10,
            
            "lat": self.queryLocation?.latitude,
            "lng": self.queryLocation?.longitude]
        
        if needCover {
            if (computeBoundAgain) {
                self.latestRadius = Float(radius/1000)
                self.latestZoom = self.zoomMap
            }
            
            params["radius"] = self.latestRadius
            params["zoom"] = self.latestZoom
        }
        
        if let category = GLOBAL.GlobalVariables.selectedCategory {
            let categoryId = category["ID"] as? String ?? ""
            params["category_id"] = categoryId
        }
        
        
        return params
    }
    
    private func handleDealAfterFetch(_ resp:[String: Any?]?, byCategory: Bool = false, needBounds: Bool = true, pickFromCenter: Bool = false) {
        GLOBAL.GlobalVariables.listCarouselsOnMap = []
        
        if let resp = resp, let status = resp["status"] as? Bool, status == true  {
            if let tData = resp["data"] as? [String: Any] {
                
                if !byCategory {
                    let listCates = tData["category"]  as? [[String : Any]] ?? []
                    GLOBAL.GlobalVariables.listCategoriesOnMap = listCates
                }
                
                let isShowNotFound = tData["IsNotFound"] as? Bool ?? false
                if isShowNotFound {
                    GLOBAL.GlobalVariables.listCarouselsOnMap.append(["IsNotFound": true])
                }
                
                if let list = tData["Carousel"] as? [[String : Any?]] {
                    GLOBAL.GlobalVariables.listCarouselsOnMap.append(contentsOf: list)
                }
                
                self.currentDealsList = Utils.getAllDealsFromSections(GLOBAL.GlobalVariables.listCarouselsOnMap)
                
                
                GLOBAL.GlobalVariables.listDealsOnMap = self.currentDealsList
                
                NotificationCenter.default.post(name: .ON_UPDATE_STORE_CAROUSEL_VIEW, object: nil)
                NotificationCenter.default.post(name: .ON_UPDATE_CATEGORY_VIEW, object: nil)
                
                self.displayDealsOnMap(needBouds: self.firstTimeGetList)
                
                if !self.firstTimeGetList && !pickFromCenter {
                    setTimeout({
                        self.onTapFirstDealOnMap()
                    }, 500)
                }
                
                self.firstTimeGetList = false
                
                if !self.isFetchedLoyalty {
                    self.isFetchedLoyalty = true
                    
                    Utils.getLoyaltyDeal { [weak self] (success, data) in
                        guard let self = self else {return}
                        if data != nil {
                            self.handleWhenClickedOnCard(data as? [String : Any?], showPreview: true)
                        } else {
                            GLOBAL.GlobalVariables.isShowLoyaltyFirst = false
                        }
                    }
                }
                
                
                self.isDataReady = true
            }
        }
        
        Utils.dismissLoading()
    }
    
    
    
    
    private func makeSuggestedControllerIfNeeded() -> PullQuickViewController {
        pullQuickVC = PullQuickViewController()
        
        pullQuickVC?.onClickedMyCurrentLocationCallback = { [weak self] data in
            guard let self = self else { return }
            
            //self.onClickedMyLocation()
        }
        
        
        pullQuickVC?.onDisplayCardOnScreenCallback = { [weak self] data in
            guard let self = self else { return }
            
            let dealType = Utils.getDealType(data)
            
            if dealType == DEAL_TYPE.LOYALTY {
                self.handleWhenClickedOnCard(data, showPreview: true)
            } else if dealType == DEAL_TYPE.BASIC ||
                dealType == DEAL_TYPE.BLACK ||
                dealType == DEAL_TYPE.SILVER ||
                dealType == DEAL_TYPE.GOLD ||
                dealType == DEAL_TYPE.SWEEPSTAKE {
                
                if let uDeal = GLOBAL.GlobalVariables.selectedDeal, let mData = data {
                    let mId = mData["ID"] as? String ?? ""
                    let uId = uDeal["ID"] as? String ?? ""
                    
                    if uId == mId && mId != "" {
                        self.handleWhenClickedOnCard(data)
                    }
                }
            } else if dealType == DEAL_TYPE.TEO {
                if let tData = data, let assignmentProgram = tData["AssignmentProgram"] as? [String: Any?] {
                    
                    let status = assignmentProgram["Status"] as? String
                    
                    if status == TEO_STATUS.COMPLETED.rawValue {
                        self.handleWhenClickedOnCard(data)
                    } else if status == TEO_STATUS.NEW.rawValue {
                        self.showTEO(data)
                    }
                }
                
            } else if dealType == DEAL_TYPE.UNSWIPED_DAILY {
                self.gotoDailyDeals(data)
            } else if dealType == DEAL_TYPE.SPECIAL_CARD {
                let newUpgradeVC = NewUpgradeViewController()
                newUpgradeVC.initPlanIndex = UPGRADE_PLAN.BASIC.viewIndex
                
                self.navigationController?.pushViewController(newUpgradeVC, animated: true)
            }
        }
        
        if let pullUpController = pullQuickVC {
            pullUpController.initialState = .contracted
            if originalPullUpControllerViewSize == .zero {
                originalPullUpControllerViewSize = pullUpController.view.bounds.size
            }
            
            return pullUpController
        }
        
        return PullQuickViewController()
    }
    
    private func addPullUpController() {
        let pullUpController = makeSuggestedControllerIfNeeded()
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        
        addPullUpController(pullUpController, initialStickyPointOffset: pullUpController.initialPointOffset, animated: true)
        
        bringActionToTop()
    }
    
    public func bringActionToTop() {
        //        topView.superview?.bringSubviewToFront(topView)
        //        coverBackView.superview?.bringSubviewToFront(coverBackView)
        //        coverProfileView.superview?.bringSubviewToFront(coverProfileView)
        //        coverWalletView.superview?.bringSubviewToFront(coverWalletView)
        //        coverNearMeView.superview?.bringSubviewToFront(coverNearMeView)
    }
    
    
    @objc func onReloadPage(_ notification: Notification) {
        self.reloadPage()
        
    }
    
    private func handleHighlightDealOnMap(_ dealId: String = "", isCallback: Bool = true) {
        
        let tDeal = self.checkDealsExist(dealId)
        
        if tDeal != nil {
            let lMarkers = self.markersList.filter { (marker) -> Bool in
                if let tInfo = marker.userData as? [String: Any?] {
                    if tInfo["ID"] as? String ?? "" == dealId {
                        return true
                    }
                }
                
                return false
            }
            
            if lMarkers.count > 0 {
                let needMarker = lMarkers[0]
                
                _ = self.focusToDealByMarker(needMarker, isCallback: isCallback)
            }
        } else {
            self.dismissSelectedDealOnMap()
        }
    }
    
    private func checkDealsExist(_ dealId: String) -> [String: Any?]? {
        let lDeals = self.currentDealsList.filter { (item) -> Bool in
            if item["ID"] as? String ?? "" == dealId {
                return true
            }
            
            return false
        }
        
        if lDeals.count > 0 {
            return lDeals[0]
        }
        
        return nil
    }
    
    private func reloadPage() {
        self.getListDealsInitMap()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .REFRESH_HOME_PAGE, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_SELECTED_CATEGORY_SEARCH, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UN_FOCUS_DEAL_ON_MAP, object: nil)
        NotificationCenter.default.removeObserver(self, name: .FILTER_WITH_TEXT_ON_CAROUSEL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .REFRESH_LIST_CAROUSEL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_CLICKED_MORE_STORE_ON_CAROUSEL, object: nil)
        NotificationCenter.default.removeObserver(self, name: .FORCE_ALLOW_PERMISSION_LOCATION, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GLOBAL.GlobalVariables.isHomeAppear = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        GLOBAL.GlobalVariables.isHomeAppear = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard (isViewDidAppear) else {
            isViewDidAppear = true
            
            self.addPullUpController()
            self.fetchCurrentLocation()
            
            GLOBAL.GlobalVariables.isHomeScreenReady = true
            
            return
        }
    }
    
    private func fetchCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func reBuildDealsListForMap() -> Void {
        self.currentDealsList = self.currentDealsList.map {
            var tDeal = $0
            
            if let tapped = tappedMarker, let selectedData = tapped.userData as? [String: Any], tDeal["ID"] as? String == selectedData["ID"] as? String {
                tDeal["selected"] = true
            } else {
                tDeal["selected"] = false
            }
            
            return tDeal
        }
        
    }
    
    func displayDealsOnMap(needBouds: Bool = true) {
        self.mapView.clear()
        
        self.displayMyLocation()
        self.reBuildDealsListForMap()
        
        let mPaths = GMSMutablePath()
        var mPositions: [CLLocationCoordinate2D] = []
        if let myLocation = self.userLocation  {
            mPaths.add(myLocation)
        }
        
        self.markersList = []
        
        for index in 0..<self.currentDealsList.count {
            let deal = self.currentDealsList[index]
            
            var lat: Double?
            var lng: Double?
            
            if let vendorSummary = deal["VendorSummary"] as? [String: Any?], let location = vendorSummary["Location"] as? [String: Any] {
                if location["lat"] is String {
                    lat = Double(location["lat"] as? String ?? "0")
                    lng = Double(location["lng"] as? String ?? "0")
                } else {
                    lat = location["lat"] as? Double
                    lng = location["lng"] as? Double
                }
            }
            
            let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
            let marker = self.createMarker(deal)
            
            self.markersList.append(marker)
            
            if index < self.boundsNumber {
                mPaths.add(position)
                mPositions.append(position) 
            }
        }
        
        if needBouds {
            if let myLocation = self.userLocation  {
                
                if let desPosition = mPositions.last {
                    let meterDistance = Utils.getDistanceInMeter(startLat: myLocation.latitude, startLong: myLocation.longitude, endLat: desPosition.latitude, endLong: desPosition.longitude)
                    
                    let bounds = Utils.getCoordinateBounds(centerLatLng: myLocation, radius: meterDistance)
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds))
                    
                } else {
                    self.animateToPoint(myLocation, zoom: 15)
                }
            }
            
            setTimeout({
                //print("---> latestBoundsVisible FIRST ---->")
                self.latestBoundsVisible = self.getCurrentBounds()
            }, 1500)
            
        }
        
        setTimeout({
            self.maskMapView.isHidden = true
            // self.coverCenterPicker.isHidden = false
        }, 1000)
        
    }
    
    func displayMyLocation(_ animateTo: Bool = false) {
        // Replace your locaiton marker in the center of the map.
        if let position = self.userLocation {
            
            userMarker.position = position
            userMarker.icon =  UIImage(named: "user_location")
            userMarker.userData = ["isUserLocation": true]
            userMarker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.5)
            userMarker.map = mapView
            
            
            if animateTo {
                self.animateToPoint(position, zoom: 15)
            }
        }
        
        
    }
    
    
    private func dismissSelectedDealOnMap() {
        viewMarkerSelected?.removeFromSuperview()
        
        if (tappedMarker != nil) {
            tappedMarker = nil
            self.displayDealsOnMap(needBouds: false)
        }
    }
    
    
    
    private func createMarker(_ deal: [String: Any?]) -> GMSMarker {
        var lat: Double?
        var lng: Double?
        
        if let vendorSummary = deal["VendorSummary"] as? [String: Any?], let location = vendorSummary["Location"] as? [String: Any] {
            if location["lat"] is String {
                lat = Double(location["lat"] as? String ?? "0")
                lng = Double(location["lng"] as? String ?? "0")
            } else {
                lat = location["lat"] as? Double
                lng = location["lng"] as? Double
            }
        }
        
        let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
        
        let marker = GMSMarker()
        marker.position = position
        marker.userData = deal
        marker.map = mapView
        
        let namePinImage = "g_\(deal["PingIcon"] as? String ?? "basic")"
        let nameBadgeImage = "badge_\(deal["BadgeColor"] as? String ?? "red")"
        let namePinSelectImage = "g_selected_\(deal["PingIcon"] as? String ?? "basic")"
        let shortBadge = (deal["ShortLabelTxt"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        
        if (deal["selected"] as? Bool == true) {
            selectedMarker.thumbnail.image = nil
            selectedMarker.gBigIcon?.image = UIImage(named: namePinSelectImage)
            selectedMarker.lblBadge?.text = shortBadge
            selectedMarker.badgeImage?.image = UIImage(named: nameBadgeImage)
            selectedMarker.dealName?.text = deal["Name"] as? String ?? ""
            
            if let summary = deal["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let tCategoryName = category["Name"] as? String {
                selectedMarker.categoryName?.text = tCategoryName
            } else {
                selectedMarker.categoryName?.text = ""
            }
            
            selectedMarker.coverBadge?.isHidden = shortBadge == ""
            
            marker.icon = Utils.imageWithView(view: selectedMarker)
            
            if let summary = deal["VendorSummary"] as? [String: Any?], let photoURL = summary["LogoUrl"] as? String {
                selectedMarker.thumbnail.sd_setImage(with:  URL(string: photoURL), completed: {  [weak self]  (_, _, _, _) in
                    guard let self = self else {return}
                    marker.iconView = nil
                    marker.icon = Utils.imageWithView(view: self.selectedMarker)
                })
            }
            
            
            marker.zIndex = 9999
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        } else {
            var pinMarkerView = GMarker().loadDefaultMarker()
            
            if self.zoomMap >=  self.zompStep {
                self.modeDefault  = false
                pinMarkerView = GMarker().loadCateMarker()
                pinMarkerView.gBigIcon?.image = UIImage(named: namePinSelectImage)
                if let summary = deal["VendorSummary"] as? [String: Any?], let category = summary["Category"] as? [String: Any?], let tCategoryImage = category["ImageUrl"] as? String {
                    pinMarkerView.cateImage?.sd_setImage(with:  URL(string: tCategoryImage), completed: {  [weak self]  (_, _, _, _) in
                        guard let self = self else {return}
                        marker.iconView = nil
                        marker.icon = Utils.imageWithView(view: pinMarkerView)
                    })
                } else {
                    pinMarkerView.cateImage?.image = nil
                }
            } else {
                self.modeDefault = true
            }
            
            pinMarkerView.gIcon?.image = UIImage(named: namePinImage)
            pinMarkerView.lblBadge?.text = shortBadge
            pinMarkerView.badgeImage?.image = UIImage(named: nameBadgeImage)
            pinMarkerView.coverBadge?.isHidden = shortBadge == ""
            
            marker.zIndex = 0
            marker.groundAnchor = CGPoint(x: 0.6, y: 1)
            //marker.icon = UIImage(named: "marker_ic")
            marker.icon = Utils.imageWithView(view: pinMarkerView)
        }
        
        return marker
    }
    
    
    
    private func focusToDealByMarker(_ marker: GMSMarker, isCallback: Bool = true) -> Bool {
        if isCallback {
            NotificationCenter.default.post(name: .ON_SELECTED_DEAL_ON_MAP, object: marker.userData as? [String : Any?])
        }
        
        if let tTapMarker = tappedMarker, let userInfo = tTapMarker.userData as? [String: Any?], let mInfo = marker.userData as? [String: Any?] {
            if userInfo["ID"] as? String == mInfo["ID"] as? String {
                return false
            }
        }
        
        if let userData = marker.userData as? [String: Any], let isUserLocation = userData["isUserLocation"] as? Bool {
            if (isUserLocation) {
                NotificationCenter.default.post(name: .ON_DISSMIS_STORE_CAROUSEL_VIEW, object: nil)
                
                return false
            }
        }
        
        tappedMarker = marker
        
        self.displayDealsOnMap(needBouds: false)
        
        let position = marker.position
        
        self.animateToPoint(position)
        
        return false
    }
    
    
    
    private func includeNewVisible() -> Bool {
        guard let latestVisible = self.latestBoundsVisible else { return false }
        
        var isInclude = true
        
        let visibleRegion = self.mapView.projection.visibleRegion()
        
        let currentFourPoints = [visibleRegion.nearLeft, visibleRegion.farLeft, visibleRegion.nearRight, visibleRegion.farRight]
        
        for cPoint in currentFourPoints {
            if !latestVisible.contains(cPoint) {
                isInclude = false
                break
            }
        }
        
        return isInclude
    }
    
    private func getCurrentBounds() -> GMSCoordinateBounds? {
        let visibleRegion = self.mapView.projection.visibleRegion()
        
        let bounds = GMSCoordinateBounds(region: visibleRegion)
        
        return bounds
        
    }
    
    private func getMapVisibleRadius() -> Double {
        let visibleRegion = self.mapView.projection.visibleRegion()
        var meterDistance: Double = 0
        let dLocation: CLLocationCoordinate2D = visibleRegion.nearRight
        
        let marker = self.createMarker(["VendorSummary": ["Location": ["lat": dLocation.latitude, "lng": dLocation.longitude]]])
        marker.map = mapView
        
        if let cLocation = self.queryLocation {
            meterDistance = Utils.getDistanceInMeter(startLat: cLocation.latitude, startLong: cLocation.longitude, endLat: dLocation.latitude, endLong: dLocation.longitude)
        }
        
        return meterDistance
    }
}


extension HomeMapViewController: GMSMapViewDelegate, CLLocationManagerDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.didMoveMapview = gesture
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.queryLocation = CLLocationCoordinate2D(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        
        if self.didMoveMapview {
            //print("JUST PICK AT --> (\(mapView.camera.target.latitude), \( mapView.camera.target.longitude))")
            let isInclude = self.includeNewVisible()
            
            if !isInclude {
                self.latestBoundsVisible = self.getCurrentBounds()
                
                //        let marker = self.createMarker(["VendorSummary": ["Location": ["lat": mapView.camera.target.latitude, "lng":]]])
                //
                //        marker.map = mapView
                GLOBAL.GlobalVariables.selectedDeal = nil
                self.handleFilterWithText(true, showLoading: true, needBounds: false, pickFromCenter: true)
            }
        }
    }
    
    public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return  nil
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        return self.focusToDealByMarker(marker)
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.dismissSelectedDealOnMap()
        
        GLOBAL.GlobalVariables.selectedDeal = nil
        
        NotificationCenter.default.post(name: .ON_DISABLE_HIGHLIGHT_DEAL_ON_CAROUSEL, object: nil)
        NotificationCenter.default.post(name: .ON_DISSMIS_STORE_CAROUSEL_VIEW, object: nil)
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.zoomMap = mapView.camera.zoom
      
        if self.modeDefault  {
            if self.zoomMap >= self.zompStep {
                self.displayDealsOnMap(needBouds: false)
            }
        } else {
            if self.zoomMap < self.zompStep {
                self.displayDealsOnMap(needBouds: false)
            }
        }
        
        
        let position = tappedMarker?.position
        
        if let tPosition = position {
            viewMarkerSelected?.center = mapView.projection.point(for: tPosition)
            viewMarkerSelected?.center.y -= 65
            viewMarkerSelected?.center.x += 40
        }
        
    }
    
    func animateToPoint(_ position: CLLocationCoordinate2D, zoom: Float = 0) {
        let spentHeight = self.pullQuickVC?.getCurrentHeight() ?? 0.0
        
        let point = mapView.projection.point(for: position)
        
        if zoom != 0 {
            let newPoint = mapView.projection.coordinate(for: point)
            let cameraZoom = GMSCameraUpdate.setTarget(newPoint, zoom: zoom)
            mapView.animate(with: cameraZoom)
        } else {
            let cameraTarget = GMSCameraUpdate.scrollBy(x: point.x - SCREEN_WIDTH/2, y: point.y - (SCREEN_HEIGHT - spentHeight)/2 - 100)
            mapView.animate(with: cameraTarget)
        }
        
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            // you're good to go!
            manager.startUpdatingLocation()
            
            //            self.userLocation = CLLocationCoordinate2D(latitude: 10.801370, longitude: 106.705790)
            //            GLOBAL.GlobalVariables.latestLat = self.userLocation?.latitude ?? 0
            //            GLOBAL.GlobalVariables.latestLng = self.userLocation?.longitude ?? 0
            //            self.getListDealsInitMap()
            
            GLOBAL.GlobalVariables.allowFetchLocation = true
        } else if status == .denied {
            GLOBAL.GlobalVariables.allowFetchLocation = false
            // do show ALERT -> SETTING -> LOCATION
            if !GLOBAL.GlobalVariables.isShowAlertRequestLocation {
                Utils.showAlertRequiredLocation()
            }
            
        }
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let _userLocation = locations.last {
                
            
            
            if self.lastLocation == nil {
                if GLOBAL.GlobalVariables.sysConfigModel.isHardCodeLocation {
                    self.lastLocation = CLLocationCoordinate2D(latitude: GLOBAL.GlobalVariables.sysConfigModel.hardLocationLat, longitude: GLOBAL.GlobalVariables.sysConfigModel.hardLocationLng)
                } else {
                    self.lastLocation = _userLocation.coordinate
                }
            }
            
            if (self.userLocation ==  nil ||
                self.userLocation!.latitude != _userLocation.coordinate.latitude ||
                self.userLocation!.longitude != _userLocation.coordinate.longitude) {
                
                if GLOBAL.GlobalVariables.sysConfigModel.isHardCodeLocation {
                    self.userLocation = CLLocationCoordinate2D(latitude: GLOBAL.GlobalVariables.sysConfigModel.hardLocationLat, longitude: GLOBAL.GlobalVariables.sysConfigModel.hardLocationLng)
                } else {
                    self.userLocation = _userLocation.coordinate
                }
                
                GLOBAL.GlobalVariables.latestLat = self.userLocation?.latitude ?? 0
                GLOBAL.GlobalVariables.latestLng = self.userLocation?.longitude ?? 0
                
                self.displayMyLocation(willZoomToLocation)
                
                if (willZoomToLocation) {
                    willZoomToLocation = false
                    
                    self.queryLocation = self.userLocation
                    
                    self.getListDealsInitMap(needCover: false)
                    
                    NotificationCenter.default.post(name: .BEGIN_CHECK_SWIPE_DEALS, object: nil)
                }
                
                
                
            }
            
            
            if let sourceLoc = self.lastLocation, let desLocation = self.userLocation {
                let distance = Utils.getDistanceInMeter(startLat: sourceLoc.latitude, startLong: sourceLoc.longitude, endLat: desLocation.latitude, endLong: desLocation.longitude)
                
                if distance/1000 > Constants.DISTANCE_TO_REQUEST_DEAL {
                    self.lastLocation = self.userLocation
                    self.getListDealsInitMap()
                }
            }
        }
        
    }
    
}


extension HomeMapViewController {
    func updateLanguages() {
        
    }
}

