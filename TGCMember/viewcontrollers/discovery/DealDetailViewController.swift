//
//  DealViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps
import WebKit

class DealDetailViewController: BaseViewController {
    
    var refreshControl: UIRefreshControl!
    
    
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblVendorName: UILabel!
    @IBOutlet weak var topLblAddressConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblChat: UILabel!
    @IBOutlet weak var coverKm: UIView!
    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet weak var coverShareLike: UIView!
    @IBOutlet weak var coverBack: UIView!
    @IBOutlet weak var btnUseDeal: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var loadingBanner: UIActivityIndicatorView!
    @IBOutlet weak var dealThumbnail: UIImageView!
    @IBOutlet weak var lblTitleLine1: UILabel!
    @IBOutlet weak var lblTitleLine2: UILabel!
    
    @IBOutlet weak var lblCateName: UILabel!
    @IBOutlet weak var lblTimeFromTo: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var vendorLogo: UIImageView!
    @IBOutlet weak var coverWartermask: UIView!
    @IBOutlet weak var lblSuggestUpgrade: UILabel!
    @IBOutlet weak var maskWarter: UIView!
    
    @IBOutlet weak var lblExpireDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var bottomAddressConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverFooter: UIView!
    @IBOutlet weak var bottomExpireConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightExpireConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverExpire: UIView!
    @IBOutlet weak var heightTimeConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverTime: UIView!
    @IBOutlet weak var coverChat: UIView!
    @IBOutlet weak var lblDayOfWeek: UILabel!
    
    private let selectedMarker = GMarker().loadSelectedMarker()
    var callback: FinishedCallback?
    var dealInfo: [String: Any?]?
    
    private var isRefresh: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        
        self.scrollView.delegate = self
        self.scrollView.addPullToRefresh { [weak self] in
            guard let self = self else {return}
            
            self.didPullToRefresh()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshDealDetail(_:)), name: .REFRESH_DEAL_DETAIL, object: nil)
        
        self.getDetails()
        
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self, name: .REFRESH_DEAL_DETAIL, object: nil)
    }
    
    @objc func refreshDealDetail(_ notification: Notification) {
        self.getDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           
           GLOBAL.GlobalVariables.dealDetailInstance = self
    }

    
    private func decorate() {
        self.coverWartermask?.layer.cornerRadius = 22
        self.maskWarter?.layer.cornerRadius = 22
        
        btnUseDeal.backgroundColor = UIColor(255, 193, 6)
        btnUseDeal.layer.borderWidth = 0.0
        btnUseDeal.layer.cornerRadius = 3.0
        btnUseDeal.layer.borderColor = UIColor.clear.cgColor
        btnUseDeal.setTitleColor(UIColor.white, for: .normal)
        btnUseDeal.layer.applySketchShadow(color: UIColor(203, 203, 203), alpha:1, x: 0, y: 8, blur: 15, spread: 0)
        
        coverBack?.layer.cornerRadius = 15.0
        
        coverChat?.layer.cornerRadius = 6.0
        coverChat?.layer.borderWidth = 0.7
        coverChat?.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        coverChat?.layer.masksToBounds = true
        
        vendorLogo?.layer.cornerRadius = 4.0
        vendorLogo?.layer.masksToBounds = true
        
        configMapStyle()
        
        bottomContentConstraint?.constant = Utils.isIPhoneNotch() ? 70 : 100
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
    
        let upgradeVC = NewUpgradeViewController()
        upgradeVC.backToDealDetail = true
        
        if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
            upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
        }
        
        GLOBAL.GlobalVariables.discoveryInstance?.navigationController?.pushViewController(upgradeVC, animated: true)
        
    }
    
    private func configMapStyle() {
        APICommonServices.fetchContentFromJSON(Constants.GOOGLE_MAP_STYLE_URL) { (resp) in
            if let tResp = resp, let tData = tResp["data"] as? [[String: Any?]] {
                
                let jsonData = try! JSONSerialization.data(withJSONObject: tData, options: .prettyPrinted)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                    // print("fetchContentFromJSON STRING --> ", jsonString)
                    
                    setTimeout({
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(jsonString: jsonString)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }, 0)
                } else {
                    if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }
                }
            }
        }
    }
    
    private func showPositonDeal() {
        var lat: Double?
        var lng: Double?
        
        if let tData = self.dealInfo, let tSummary = tData["VendorSummary"] as? [String: Any?], let location = tSummary["Location"] as? [String: Any] {
            if location["lat"] is String {
                lat = Double(location["lat"] as? String ?? "0")
                lng = Double(location["lng"] as? String ?? "0")
            } else {
                lat = location["lat"] as? Double
                lng = location["lng"] as? Double
            }
            
            let tDistance = Utils.getDistanceInMeter(startLat: GLOBAL.GlobalVariables.latestLat, startLong: GLOBAL.GlobalVariables.latestLng, endLat: lat!, endLong: lng!)
            
            self.lblKm?.text = "\(String(format: "%.1f", tDistance/1000))km"
        } else {
            self.lblKm?.text = "0km"
        }
        
        let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
        
        let dealType = Utils.getDealType(self.dealInfo).rawValue
        
        let marker = GMSMarker()
        marker.position = position
        
        marker.icon = UIImage(named: "g_\(dealType)")
        
        
        marker.zIndex = 9999
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        
        marker.map = mapView
        
        self.animateToPoint(position)
    }
    
    @IBAction func onClickedChat(_ sender: Any) {
        if let tData = self.dealInfo, let summary = tData["VendorSummary"] as? [String: Any?] {
            self.openChatDetail(["PartnerID":  summary["VendorID"] as? String ?? ""])
        }
    }
    
    private func didPullToRefresh() {
        self.getDetails()
    }
    
    
    @IBAction func onClickedFullMap(_ sender: Any) {
        let fullMapVC = FullMapViewController()
        fullMapVC.dealInfo = self.dealInfo
        self.addSubviewAsPresent(fullMapVC)
    }
    
    private func updateUseDealStyle(_ allowUseDeal: Bool = true) {
        self.coverFooter.isUserInteractionEnabled = allowUseDeal
        self.coverFooter.isHidden = !allowUseDeal
    }
    
    @IBAction func onClickeUseDeal(_ sender: Any) {
        Utils.checkLogin(self) {  [weak self]  (status) in
            guard let self = self else {return}
            
            if status == LOGIN_STATUS.SUCCESS_IMMEDIATELY {
                if let tData = self.dealInfo {
                    Utils.showClaimAlert(tData, bgColor: UIColor(0, 0, 0, 0.2)) {  [weak self] (success, data) in
                        guard let self = self else {return}
                        
                        if success {
                            Utils.closeCustomAlert()
                            
                            NotificationCenter.default.post(name: .UPDATE_USE_DEAL, object: data)
                            NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false])
                            
                            self.getDetails()
                        }
                    }
                }
            } else if status == LOGIN_STATUS.SUCCESS_AFTER {
                
            }
        }
    }
    
    
    private func fillData() {
        if let tData = self.dealInfo {
            
            self.dealThumbnail.sd_setImage(with:  URL(string: tData["Photo"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingBanner?.stopAnimating()
            })
            
            var isAllowUseDeal = tData["YouCanUseThisDeal"] as? Bool ?? false
            let dealType = Utils.getDealType(tData)
            
            if dealType == .LOYALTY {
                isAllowUseDeal = false
            }
            
            self.coverWartermask?.isHidden = isAllowUseDeal
            
            self.updateUseDealStyle(isAllowUseDeal)
            
            let tSummary = tData["VendorSummary"] as? [String: Any?] ?? [String: Any?]()
            let tCategory = tSummary["Category"] as? [String: Any?] ?? [String: Any?]()
            
            self.lblCateName?.text = (tCategory["Name"] as? String ?? "").capitalized
            
            self.vendorLogo.sd_setImage(with:  URL(string: tSummary["LogoUrl"] as? String ?? ""), completed: {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
            })
            
            self.lblVendorName?.text = (tSummary["Name"] as? String ?? "").capitalized
            self.lblTitleLine1?.text = tData["TitleLine1"] as? String ?? " "
            self.lblTitleLine2?.text = tData["TitleLine2"] as? String ?? " "
            //self.lblAddress?.text = Utils.getFullAddress(tSummary)
            
            let addressText = Utils.getFullAddress(tSummary)
            self.lblAddress?.text = addressText
            let widthAddress = addressText.width(withConstrainedHeight: 30, font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14) ?? UIFont.systemFont(ofSize: 14))
            // print("widthAddress ---> ", widthAddress)
            
            if widthAddress < (SCREEN_WIDTH - 84) {
                self.bottomAddressConstraint?.constant = 4
                self.topLblAddressConstraint?.constant = 8
            } else {
                self.bottomAddressConstraint?.constant = 0
                self.topLblAddressConstraint?.constant = 5
            }
            
            self.lblPhone?.text = "\(Utils.getPhoneNumber(tSummary))"
            
            
            if let fromAt = tData["FromAtVendor"] as? String, let toAt = tData["ToAtVendor"] as? String, fromAt != "", toAt != "" {      
                coverTime?.isHidden = false
                heightTimeConstraint?.constant = 30
                
                self.lblTimeFromTo?.text = "\(fromAt) \(LOCALIZED.txt_to_title.translate.lowercased()) \(toAt)"
            } else {
                coverTime?.isHidden = true
                heightTimeConstraint?.constant = 0
            }
            
            self.lblDayOfWeek?.text = tData["TxtDayOfWeek"] as? String ?? " "
            
            if dealType == .GOLD || dealType == .SILVER || dealType == .BLACK {
                heightExpireConstraint?.constant = 22
                if let expireDate = tData["ExpiryTime"] as? String, expireDate != "" {
                    let txtDate = Utils.stringFromStringDate(expireDate, toFormat: "dd MMM, yyyy")
                    var fDate = LOCALIZED.until_format_tag_date.translate;
                    fDate = fDate.replacingOccurrences(of: "[%__TAG_DATE__%]", with: txtDate)
                    
                    self.lblExpireDate?.text = fDate
                } else {
                    heightExpireConstraint?.constant = 0
                }
            } else {
                heightExpireConstraint?.constant = 0
            }
            
            bottomExpireConstraint?.constant = 20
            
            self.lblDescription?.setAttribute(tData["LongDescription"] as? String ?? "", color: UIColor("rgb 138 145 150"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 3.0)
            
            
            self.showPositonDeal()
            
        }
        
        self.contentView.isHidden = false
    }
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 17.0)
        mapView.animate(with: camera)
    }
    
    
    
    @IBAction func onClickedNumberPhone(_ sender: Any) {
        if let tData = self.dealInfo, let summary = tData["VendorSummary"] as? [String: Any?] {
            let number = Utils.getPhoneNumber(summary).replacingOccurrences(of: " ", with: "")
            Utils.callNumber(number)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        GLOBAL.GlobalVariables.dealDetailInstance = nil
        
        self.navigationController?.popViewController(animated: true)
        
        self.callback?()
    }
    
    
    private func getDetails() {
        if let tData = self.dealInfo {
            Utils.showLoading()
            
            let instanceDealID = tData["InstanceDealID"] as? String ?? ""
            
            APICommonServices.getDealDetail(instanceDealID) { (resp) in
                mPrint("getDealDetail --> ", resp)
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    if let detail = resp["deal"]  as? [String : Any?] {
                        self.dealInfo = detail
                        
                        self.fillData()
                    }
                } 
                
                Utils.dismissLoading()
                
                self.isRefresh = false
            }
        }
    }
    
    
    func updateLanguages() {
        
        self.btnUseDeal.setTitle(LOCALIZED.txt_use_title.translate.uppercased(), for: .normal)
        self.btnUseDeal.setTitleColor(UIColor("rgb 47 72 88"), for: .normal)
        
        var fullText = LOCALIZED.you_are_now_browsing_as_a_free_member.translate
        fullText = fullText.replacingOccurrences(of: "[%__TAG_UPGRADE_TO_USE_DEALS__%]", with: LOCALIZED.upgrade_to_use_deals.translate)
        self.lblSuggestUpgrade?.setTextWithUnderline(fullText, .white, UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), highlightText: [LOCALIZED.upgrade_to_use_deals.translate], highlightColor: [.white], highlightFont: [UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13)])
    }
    
    
}

extension DealDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !self.isRefresh {
            if scrollView.contentOffset.y < -60.0 {
                
                for subview in scrollView.subviews {
                    if subview is UIRefreshControl {
                        let rf = subview as! UIRefreshControl
                        
                        self.isRefresh = true
                        rf.beginRefreshing()
                        rf.sendActions(for: .valueChanged)
                        
                        break
                    }
                }
            }
        }
        
    }
}
