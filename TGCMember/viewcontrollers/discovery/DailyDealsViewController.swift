//
//  DailyDealsViewController.swift
//  TGCMember
//
//  Created by vang on 6/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass


private var numberOfCards: Int = 3

class DailyDealsViewController: BaseViewController {
    
    @IBOutlet weak var btnLater: UIButton!
    @IBOutlet weak var verticalSwipeView: VerticalSwipeView!
    @IBOutlet weak var topVerticalCardConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomLaterConstraint: NSLayoutConstraint!
    
    var selectedCard: [String: Any?]?
    var startLocation:CGPoint = CGPoint.zero
    var originalHeaderFrame: CGRect = .zero
    let disablePanVertical: Bool = true
    
    var draggableCardViews: [DraggableCardView] = []
    var swipeCardFrames: [CGRect] = []
    
    var isLoadFinished: Bool = false
    var tDirection : PanDirection = .None
    
    var fromPNS: Bool = true
    var fromCarousel: Bool = true
    var isStarter: Bool = false
    var haveAction: Bool = false
    
    var dealIds: String = "57d75ded-5081-44fa-9031-462258b3b3cb,c89c81bf-a978-4cb4-be07-5b2b646e8da5,3ef8c486-402b-4639-8269-38b32bd03e30"
    
    public var deals: [[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        
        self.getListDealsFromIds()
        
    }
    
    @IBAction func onClickedLater(_ sender: Any) {
        self.handleBeforeDismiss()
    }
    
    private func handleBeforeDismiss() {
        if fromCarousel && !self.haveAction {
            // do nothing
        } else {
            NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: ["IsNeedBounds": false, "KeepState": true, "ComputeAgain": false])
        }
        
        
        self.dismissSelf()
    }
    
    private func getListDealsFromIds() {
        if self.fromPNS {
            Utils.showLoading()
            
            if self.dealIds.suffix(1) == "," {
                self.dealIds = String(self.dealIds.dropLast())
            }
            
            APICommonServices.getListDealsFromPNS(self.dealIds) { [weak self] (resp) in
                guard let self = self else {return}
                
                var message = "";
                mPrint("getListDealsFromPNS", resp)
                if let resp = resp, let status = resp["status"] as? Bool {
                    if status == true {
                        if let data = resp["data"] as? [String: Any?], let listDeals = data["listDeals"] as? [[String : Any?]]  {
                            self.deals = listDeals
                        } else {
                            self.deals = []
                        }
                    }
                    
                    message = resp["message"] as? String ?? ""
                } else {
                    self.deals = []
                }
                
                if self.deals.count == 0  {
                    Utils.showConfirmAlertView("", message, LOCALIZED.txt_ok_title.translate, nil, confirmCallback: { (data) in
                        self.dismissSelf()
                    }, cancelCallback: nil)
                } else {
                    self.configSwipeView()
                }
                
                Utils.dismissLoading()
                
                
            }
        } else {
            self.configSwipeView()
        }
        
        
    }
    
    private func buildIds(_ deals: [[String: Any?]]) -> String {
        var IDs = ""
        
        for item in self.deals {
            let dealId = item["ID"] as? String ?? ""
            
            if dealId != "" {
                if IDs != "" {
                    IDs += ","
                }
                
                IDs += dealId
            }
        }
        
        return IDs
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func configSwipeView() {
        self.verticalSwipeView.animationDelegate = self
        let tDict = ["deals": self.deals]
        
        self.verticalSwipeView.config(tDict, onRemoveCardCallback: {  [weak self] (cardData) in
            guard let self = self else {return}
                if let tData = cardData {
                    mPrint("call api  REMOVE", tData["InstanceDealID"] as? String ?? "")
                    var params = [String: Any]()
                    params["instance_deal_id"] = tData["InstanceDealID"] as? String ?? ""
                    
                    self.doRemoveFromUnswiped(params)
                }
            
            }, onCheckCardCallback: {  [weak self] (cardData) in
                guard let self = self else {return}
                
                if let tData = cardData {
                    mPrint("call api  CHECK", tData["InstanceDealID"] as? String ?? "")
                    var params = [String: Any]()
                    params["instance_deal_id"] = tData["InstanceDealID"] as? String ?? ""
                  
                    self.doAddDealToStore(params)
                }
                
                
            }, onDislikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                
                if let tData = cardData {
                    mPrint("call api  DISLIKE", tData["InstanceDealID"] as? String ?? "")
                    var params = [String: Any]()
                    params["instance_deal_id"] = tData["InstanceDealID"] as? String ?? ""
                    params["very_hate"] = true
                    
                    self.doRemoveFromUnswiped(params)
                }
                
            }, onLikeCardCallback: { [weak self] (cardData) in
                guard let self = self else {return}
                if let tData = cardData {
                    mPrint("call api  LIKE", tData["InstanceDealID"] as? String ?? "")
                    var params = [String: Any]()
                    params["instance_deal_id"] = tData["InstanceDealID"] as? String ?? ""
                    params["very_like"] = true
                    
                    self.doAddDealToStore(params)
                }
            }, onRunOutOfCardsCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.handleBeforeDismiss()
                
            }, onReadyCallback: { [weak self] (data) in
                guard let self = self else {return}
                self.verticalSwipeView.startAnimation()
        })
        
    }
    
    private func doAddDealToStore(_ params: [String: Any]) {
        APICommonServices.addDealToStore(params) { (resp) in
            mPrint("doAddDealToStore --> ", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true {
                self.haveAction = true
            }
        }
    }
    
    private func doRemoveFromUnswiped(_ params: [String: Any]) {
        APICommonServices.removeDealFromUnswiped(params) { (resp) in
            mPrint("doRemoveFromUnswiped --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true {
                self.haveAction = true
            }
        }
    }
    
    private func dismissSelf() {
        self.dismissFromSuperview(true, completion: { [weak self] (finished) in
            guard let self = self else {return}
            
            GLOBAL.GlobalVariables.shouldShowDailyDeals = false
            
            self.removeFromParent()
            self.view.removeFromSuperview()
        })
        
    }
    
    private func decorate() {
        self.bottomLaterConstraint.constant = Utils.isIPhoneNotch() ? 35 : (SCREEN_HEIGHT <= 667 ? 10 : 20)
        self.btnLater.setTitleUnderline(" LATER", color: .white, font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 15))
    }
    
}

extension DailyDealsViewController: SwipeAppearanceDelegate {
    public func prepareAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int) {
        
    }
    
    public func beginAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int) {
        
    }
    
    public func completionAnimation(_ verticalSwipeView: VerticalSwipeView, _ atIndex: Int) {
        
    }
}





extension DailyDealsViewController {
    func updateLanguages() {
        
    }
}

