//
//  SearchViewController.swift
//  TGCMember
//
//  Created by vang on 5/3/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

let heightCategory: CGFloat = 105

class NearMeSearchViewController: BaseViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblAvailable: UILabel!
    private var searchCategories: [[String: Any?]] = []
    private var listResults: [[String: Any?]] = []
    
    @IBOutlet weak var coverSearch: UIView!
    @IBOutlet weak var coverCategoryView: UIView!
    
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet var availableButtons: [UIButton]!
    
    private var tempSelectedAvailable: AVAILABLE_TIME = GLOBAL.GlobalVariables.selectedAvailableFilterHome
    private var tempSelectedCategory: [String: Any?]? =  GLOBAL.GlobalVariables.selectedCategoryFilterHome
    private var tempKeywordFilter: String = GLOBAL.GlobalVariables.keywordFilterHome
    
    private var isFill: Bool = false
    
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !isFill {
            isFill = true
            
            reFillData()
            decorate()
            getListCatergories()
        }
        
    }

    private func getPlaceholderText() -> String {
        var tPlace = LOCALIZED.select_a_category_or_type_keyword.translate
        
        if let category = tempSelectedCategory {
            tPlace = "\(LOCALIZED.txt_type_to_search_in.translate)"
            
            let name = category["Name"] as! String
            
            tPlace = tPlace.replacingOccurrences(of: "[%__CATEGORY_NAME__%]", with: name)
        }
        
        return tPlace
    }
    
    private func decorate() {
        topConstraint.constant = Utils.isIPhoneNotch() ? 50 : 35
        
        coverSearch.layer.cornerRadius = 3.0
        coverSearch.layer.borderColor = UIColor(120, 121, 147, 0.08).cgColor
        coverSearch.layer.borderWidth = 1.0
        coverSearch.layer.masksToBounds = true
        
        
    }
    
    private func reFillData() {
        self.tfSearch.text = tempKeywordFilter
        self.tempSelectedCategory = GLOBAL.GlobalVariables.selectedCategoryFilterHome
        self.tempSelectedAvailable = GLOBAL.GlobalVariables.selectedAvailableFilterHome
        
        updatePlaceholder()
        renderAvailableTime()
    }
    
    private func updatePlaceholder() {
        var mMutableString = NSMutableAttributedString()
        
        let placeHolder = getPlaceholderText()
    
        mMutableString = NSMutableAttributedString(string: placeHolder, attributes: [NSAttributedString.Key.font:UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13.0)!])
    
        if let category = tempSelectedCategory {
            let name = category["Name"] as! String
            
            if let tRange = placeHolder.range(of: name) {
                let nsRange = placeHolder.nsRange(from: tRange)

                mMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(22, 22, 22), range:nsRange)
            }
        }
        
        self.tfSearch.attributedPlaceholder = mMutableString
    }
    
    private func dismissKeyboard() {
        self.tfSearch.resignFirstResponder()
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    @IBAction func onClickedLocation(_ sender: Any) {
        let locationVC = LocationViewController()
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    @IBAction func onClickedClearText(_ sender: Any) {
        self.tfSearch.text = ""
        
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.tfSearch.resignFirstResponder()
        self.dismissFromSuperview(true)
    }
    
    @IBAction func onClickedSubmit(_ sender: Any) {
        GLOBAL.GlobalVariables.selectedCategoryFilterHome = tempSelectedCategory
        GLOBAL.GlobalVariables.selectedAvailableFilterHome = tempSelectedAvailable
        GLOBAL.GlobalVariables.keywordFilterHome = self.tfSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        self.tfSearch.resignFirstResponder()
        
        NotificationCenter.default.post(name: .ON_HOME_APPLY_FILTER, object: nil)
        
        self.dismissFromSuperview(true)
        
    }
    
    
    @IBAction func onClickedAvailable(_ sender: Any) {
        let button = sender as! UIButton
        
        if tempSelectedAvailable == AVAILABLE_TIME(rawValue: button.tag) { // unselect
            tempSelectedAvailable = AVAILABLE_TIME.ALL
        } else { // new select
            let time = AVAILABLE_TIME(rawValue: button.tag)
            tempSelectedAvailable =  time ?? AVAILABLE_TIME.ALL
        }
        
        renderAvailableTime()
    }
    
    private func renderAvailableTime() {
        for btn in availableButtons {
            if btn.tag == tempSelectedAvailable.rawValue {
                btn.backgroundColor = UIColor(255, 193, 6)
                btn.layer.borderWidth = 0.0
                btn.layer.borderColor = UIColor.clear.cgColor
                btn.setTitleColor(UIColor.white, for: .normal)
               
                btn.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.24, x: 0, y: 1, blur: 4, spread: 0)
            } else {
                btn.backgroundColor = UIColor.white
                btn.layer.borderWidth = 1.0
                btn.layer.borderColor = UIColor(107, 107, 107).cgColor
                btn.setTitleColor(UIColor(107, 107, 107), for: .normal)
                btn.layer.applySketchShadow(color: UIColor.clear, alpha: 0, x: 0, y: 0, blur: 0, spread: 0)
            }
            
            btn.layer.cornerRadius = 19.0

        }
    }
    
    
    private func getListCatergories() {
        if GLOBAL.GlobalVariables.homeCategories.count > 0 {
            self.searchCategories = GLOBAL.GlobalVariables.homeCategories
            
            renderCategory()
        } else { // call API
            Utils.showLoading()
            
            APICommonServices.getListCategories { [weak self] (resp) in
                Utils.dismissLoading()
                
                guard let self = self else {return}
               
                if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                    if let data = resp["data"] as? [String: Any?] , let list = data["categories"]  as? [[String : Any?]] { 
                        GLOBAL.GlobalVariables.homeCategories = list
                        self.searchCategories = list
                        
                        self.renderCategory()
                    }
                }
            }

        }
    }
    
    
    private func isSelected(category: [String: Any?]?) -> Bool {
        if let tCategory = category , let tempCate = tempSelectedCategory {
            return tCategory["ID"] as! String == tempCate["ID"] as! String
        }
        
        return false
    }
    
    private func updateSelectedCategories(_ data: [String: Any?]?) {
        
        if let category = data {
            let isSelected = self.isSelected(category: category)
            
            if isSelected { // remove
                tempSelectedCategory = nil
            } else { // add
                tempSelectedCategory = category
            }
            
            renderCategory()
            
            updatePlaceholder()
        }
        
    }
    
    private func renderCategory() {
        for subview in self.coverCategoryView.subviews {
            subview.removeFromSuperview()
        }
        
        let widthItem = 70.0
        let heightItem = heightCategory
        var leftContraint = 25.0
        let spaceItem = 15.0
        
        for index in 0..<self.searchCategories.count {
            let category = self.searchCategories[index]
            
            let coverButton = Utils.buildBtnCategory(category, onClickedCallback: {  [weak self] (data) in
                guard let self = self else {return}
                
                self.updateSelectedCategories(category)
                }, isActive: self.isSelected(category: category))
            
            self.coverCategoryView.addSubview(coverButton)
            
            coverButton.snp.makeConstraints { (make) in
                make.top.equalTo(coverCategoryView)
                make.width.equalTo(widthItem)
                make.height.equalTo(heightItem)
//                make.bottom.equalTo(coverCategoryView)
                make.left.equalTo(coverCategoryView).offset(leftContraint)
                
                if index == (self.searchCategories.count - 1) {
                     make.right.equalTo(coverCategoryView).offset(-25)
                }
            }
            
            leftContraint += (widthItem + spaceItem)
     
        }
        
    }
}

extension NearMeSearchViewController {
    func updateLanguages() {
        updatePlaceholder()
        
        self.lblAvailable.text = LOCALIZED.txt_available_two_dots.translate
        self.btnSubmit.setTitle(LOCALIZED.txt_submit_title.translate, for: .normal)
        
        for button in self.availableButtons {
            if button.tag == AVAILABLE_TIME.TODAY.rawValue {
                button.setTitle("     \(LOCALIZED.txt_today_title.translate)     ", for: .normal)
            } else if button.tag == AVAILABLE_TIME.TOMORROW.rawValue {
                button.setTitle("     \(LOCALIZED.txt_tomorrow_title.translate)     ", for: .normal)
            } else if button.tag == AVAILABLE_TIME.THIS_WEEK.rawValue {
                button.setTitle("     \(LOCALIZED.txt_this_week.translate)     ", for: .normal)
            } else if button.tag == AVAILABLE_TIME.NEXT_WEEK.rawValue {
                button.setTitle("     \(LOCALIZED.txt_next_week.translate)     ", for: .normal)
            }
        }
    }
}
