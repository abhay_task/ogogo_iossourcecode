//
//  FullMapViewController.swift
//  TGCMember
//
//  Created by vang on 7/10/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps

class FullMapViewController: UIViewController {

    @IBOutlet weak var coverBack: UIView!
    var dealInfo: [String: Any?]?
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        decorate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showPositon()
    }

    @IBAction func onClickedClose(_ sender: Any) {
        self.dismissFromSuperview()
    }
    
    private func decorate() {
        coverBack?.layer.cornerRadius = 15.0
        
        configMapStyle()
    }
    
    private func configMapStyle() {
        APICommonServices.fetchContentFromJSON(Constants.GOOGLE_MAP_STYLE_URL) { (resp) in
            if let tResp = resp, let tData = tResp["data"] as? [[String: Any?]] {
                
                let jsonData = try! JSONSerialization.data(withJSONObject: tData, options: .prettyPrinted)
                
                if let jsonString = String(data: jsonData, encoding: .utf8) {
                   // print("fetchContentFromJSON STRING --> ", jsonString)
                    
                    setTimeout({
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(jsonString: jsonString)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }, 0)
                } else {
                    if let styleURL = Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                        do {
                            self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                        } catch  {
                            print("Cannot load map styles.")
                        }
                    }
                }
            }
        }
    }

    private func showPositon() {
        var lat: Double?
        var lng: Double?
        
        if let tData = self.dealInfo, let tSummary = tData["VendorSummary"] as? [String: Any?], let location = tSummary["Location"] as? [String: Any] {
            if location["lat"] is String {
                lat = Double(location["lat"] as? String ?? "0")
                lng = Double(location["lng"] as? String ?? "0")
            } else {
                lat = location["lat"] as? Double
                lng = location["lng"] as? Double
            }
        }
        
        let position = CLLocationCoordinate2D(latitude: lat ?? 0, longitude: lng ?? 0)
        
        let dealType = Utils.getDealType(self.dealInfo).rawValue
        
        let marker = GMSMarker()
        marker.position = position
        
        marker.icon = UIImage(named: "g_\(dealType)")
        
        
        marker.zIndex = 9999
        marker.groundAnchor = CGPoint(x: 0.5, y: 1)
        
        marker.map = mapView
        
        self.animateToPoint(position)
    }
    
    
    private func animateToPoint(_ position: CLLocationCoordinate2D) {
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint, zoom: 17.0)
        mapView.animate(with: camera)
    }

}
