//
//  EventListViewController.swift
//  TGCMember
//
//  Created by Vang Doan on 5/2/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class EventListViewController: BaseViewController {

    @IBOutlet weak var lblUpcomingEvents: UILabel!
    private var events:[String : [[String : Any]]] = [:]
    private var eventComponent = EventComponent()
    
    @IBOutlet weak var coverEventListView: UIView!
    var owner: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addEventComponent()
        NotificationCenter.default.addObserver(self, selector: #selector(onClickedBack(_:)), name: .ON_CLICK_BACK_HOME, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onApplyNewFilter(_:)), name: .ON_HOME_APPLY_FILTER, object: nil)
        
        self.getAllEvents()
    }
    
   
    
    
    func addEventComponent() {

        eventComponent.owner = self.owner
    
        self.coverEventListView.addSubview(eventComponent.view)
        self.addChild(eventComponent)
        
        eventComponent.view.snp.makeConstraints { (make) in
            make.left.equalTo(coverEventListView)
            make.right.equalTo(coverEventListView)
            make.top.equalTo(coverEventListView)
            make.bottom.equalTo(coverEventListView)
        }
    }
    
    private func mapDataBeforeReload(_ data: [[String : Any?]]?) {
        
        if let data = data {
           // mPrint("", data)
            var durations: [String] = []
            var dictEvents: [String: [[String: Any?]]] = [:]
            for index in 0..<data.count {
                let info = data[index] as [String: Any?]
            
                let tabKey = info["menuTabKey"] as? String ?? ""
                let tabName = info["menuTabName"] as? String ?? ""
                
                if GLOBAL.GlobalVariables.selectedAvailableFilterHome != AVAILABLE_TIME.ALL {
                    if GLOBAL.GlobalVariables.selectedAvailableFilterHome.timeSwitch == tabKey {
                        if let tData = info["data"] as? [String : Any?], let records = tData["Records"] as? [[String: Any?]]? ?? [] {
                            if records.count > 0 {
                                durations.append(tabName)
                                dictEvents[tabName] = records
                            }
                        }
                    }
                } else {
                    if let tData = info["data"] as? [String : Any?], let records = tData["Records"] as? [[String: Any?]]? ?? [] {
                        if records.count > 0 {
                            durations.append(tabName)
                            dictEvents[tabName] = records
                        }
                    }
                }
            }
            
            self.eventComponent.durations = durations
            self.eventComponent.selectedDuration = durations.count > 0 ? durations[0] : ""
            self.eventComponent.events = dictEvents
            
            self.eventComponent.reloadContent()
        }
      
    }
    
    private func getParamsRequest() -> [String: Any] {
        var params = [String: Any]()
        params["lat"] = GLOBAL.GlobalVariables.latestLat
        params["lng"] = GLOBAL.GlobalVariables.latestLng

        if let sCategory = GLOBAL.GlobalVariables.selectedCategoryFilterHome {
            params["category_id"] = sCategory["ID"] as? String ?? ""
        }
        
        return params
    }
    
    private func getAllEvents() {
        Utils.showLoading()
        
        let params = getParamsRequest()
        
        APICommonServices.getAllEvents(params) { [weak self] (resp) in
            Utils.dismissLoading()
            
            guard let self = self else {return}
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                
                if let data = resp["data"] as? [String: Any?] , let list = data["list"]  as? [[String : Any?]]?  {
                    self.mapDataBeforeReload(list)
                }
            }
        }
    }
    
    
    @objc func onClickedBack(_ notification: Notification) {
     
        self.dismissFromSuperview(true, completion: { [weak self] finished in
            guard let self = self else {return}
            
//            self.removeFromParent()
//            self.view.removeFromSuperview()
        })
        
    }
    
    @objc func onApplyNewFilter(_ notification: Notification) {
        self.getAllEvents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ON_HOME_APPLY_FILTER, object: nil)
    }
    
}

extension EventListViewController {
    func updateLanguages() {
        self.lblUpcomingEvents.text = LOCALIZED.upcoming_events_near_by.translate
    }
}
