//
//  LocationViewController.swift
//  TGCMember
//
//  Created by vang on 5/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class LocationViewController: BaseViewController {

    @IBOutlet weak var tfLocation: UITextField!
    
    @IBOutlet weak var btnNearMe: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        topConstraint.constant = Utils.isIPhoneNotch() ? 50 : 35
    }

    
    func updateLanguages() {
        self.btnNearMe.setTitle(" \(LOCALIZED.near_me.translate)", for: .normal)
        
        self.tfLocation.placeholder = LOCALIZED.enter_a_location.translate
        
    }

    @IBAction func onClickedBack(_ sender: Any) {
        self.tfLocation.resignFirstResponder()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedClear(_ sender: Any) {
        tfLocation.text = ""
    }
    
}
