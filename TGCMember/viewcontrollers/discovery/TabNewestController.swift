//
//  TabNewestController.swift
//  TGCMember
//
//  Created by vang on 11/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabNewestController: BaseViewController {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var tblDeals: UITableView!
    private var listDeals:[[String: Any?]]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblDeals.register(UINib(nibName: "NewDealWideCell", bundle: nil), forCellReuseIdentifier: "NewDealWideCell")
        
        decorate()
        
        getDealsNewest()
    }
    
    
    private func decorate() {
        
    }
    
    private func getDealsNewest() {
        Utils.showLoading()
        APICommonServices.getDealsNewest { [weak self] (resp) in
            guard let self = self else {return}
            mPrint("getDealsNewest", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] , let list = data["list"]  as? [String : Any?], let records = list["Records"] as? [[String : Any?]] {
                    self.listDeals = records
                    
                    self.tblDeals.reloadData()
                }
            }
            
            Utils.dismissLoading()
        }
        
    }
}

extension TabNewestController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDeals.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = listDeals[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewDealWideCell") as? NewDealWideCell
        
        cell?.config(data as? [String: Any?], onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data, let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController() {
                let dealDetailVC = DealDetailViewController()
                dealDetailVC.dealInfo = tData
                
                topVC.navigationController?.pushViewController(dealDetailVC, animated: true)
            }
        })
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return ((SCREEN_WIDTH - 40) * (154/335)) + 12
    }
    
    
}
