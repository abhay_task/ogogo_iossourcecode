//
//  MainViewController.swift
//  TGCMember
//
//  Created by vang on 5/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

enum DISCOVERY_SCREEN_ID: Int {
    case MENU = -1
    case MAP_VIEW = 0
    case LIST_VIEW = 1
    case WALLET_VIEW = 2
}

enum SWIPE_DEAL_TYPE: String {
    case STARTER = "starter"
    case DAILY = "daily"
}
class DiscoveryMainController: BaseViewController {
    
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var coverMenu: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var coverLogo: UIView!
    @IBOutlet weak var coverSearchInput: UIView!
    @IBOutlet weak var coverSearchButton: HighlightableView!
    @IBOutlet weak var widthSearchBoxConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var imageSearch: UIImageView!
    @IBOutlet weak var subCoverSearch: UIView!
    
    
    private var mTabBarController: UITabBarController = UITabBarController()
    private var tempTabIndex: Int =  DISCOVERY_SCREEN_ID.MAP_VIEW.rawValue
    
    var inputTimer: Timer?
    var isShowSearchInput: Bool = false
    var callbackMyWallet: DataCallback?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(getSwipeDeals(_:)), name: .BEGIN_CHECK_SWIPE_DEALS, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updatePositionLocation(_:)), name: .UPDATED_POSITION_LOCATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshViewAfterAuthorized), name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
        
        decorate()
        
        renderContent()
        onActive(fromIndex: 0, toIndex: tempTabIndex)
        
//        showLoyaltyDeals()
//        setupSwipeDeals()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .BEGIN_CHECK_SWIPE_DEALS, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATED_POSITION_LOCATION, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ON_UPDATE_CATEGORY_VIEW, object: nil)
    }
    
    @objc func refreshViewAfterAuthorized(_ notification: Notification) {
        self.tfSearch.text = ""
        self.btnClear.isHidden = true
    }
    
    
    @IBAction func onClickedSearch(_ sender: Any) {
        
        if self.isShowSearchInput {
            self.hideSearchInput()
            
            if GLOBAL.GlobalVariables.selectedSearchText != "" {
                self.tfSearch.text = ""
                GLOBAL.GlobalVariables.selectedSearchText = ""
                
                self.textFieldDidChange()
            }
        } else {
            self.showSearchInput()
        }
        
        self.isShowSearchInput = !self.isShowSearchInput
    }
    
    @IBAction func onClickedMenu(_ sender: Any) {
        self.toggleLeft()
    }
    
    @IBAction func onClickedClear(_ sender: Any) {
        self.tfSearch.text = ""
        GLOBAL.GlobalVariables.selectedSearchText = ""
        
        self.btnClear.alpha = 0
        
        self.textFieldDidChange()
    }
    
    
    
    private func handleFilter() {
        GLOBAL.GlobalVariables.selectedDeal = nil
        NotificationCenter.default.post(name: .FILTER_WITH_TEXT_ON_CAROUSEL, object: nil)
    }
    
    private func showSearchInput() {
        
        UIView.animate(withDuration: 0.4,
                       animations: {
                        self.imageSearch.image = UIImage(named: "close_white_ic")
                        self.tfSearch?.alpha = 1
                        self.btnClear?.alpha = GLOBAL.GlobalVariables.selectedSearchText == "" ? 0 : 1
                        
                        self.coverSearchInput?.layer.applySketchShadow(color: UIColor(120, 121, 147, 1), alpha: 0.2, x: 0, y: 2, blur: 20, spread: 0)
                        self.widthSearchBoxConstraint?.constant = SCREEN_WIDTH - 160
                        self.coverLogo?.alpha = 0
                        
                        self.view.layoutIfNeeded()
        },
                       completion: { [weak self] _ in
                        guard let self = self else {return}
                        self.imageSearch.image = UIImage(named: "close_white_ic")
                        
                        self.tfSearch?.alpha = 1
                        self.btnClear?.alpha = GLOBAL.GlobalVariables.selectedSearchText == "" ? 0 : 1
                        
                        self.coverSearchInput?.layer.applySketchShadow(color: UIColor(120, 121, 147, 1), alpha: 0.2, x: 0, y: 2, blur: 20, spread: 0)
                        self.coverLogo?.alpha = 0
                        self.widthSearchBoxConstraint.constant = SCREEN_WIDTH - 160
        })
    }
    
    private func hideSearchInput() {
        
        Utils.dismissKeyboard()
        
        UIView.animate(withDuration: 0.3,
                       animations: {
                        self.imageSearch.image = UIImage(named: "home_search_ic")
                        self.tfSearch?.alpha = 0
                        self.btnClear?.alpha = 0
                        self.coverSearchInput?.layer.removeSketchShadow()
                        self.coverLogo?.alpha = 1
                        self.widthSearchBoxConstraint?.constant = 0
                        
                        self.view.layoutIfNeeded()
        },
                       completion: { [weak self] _ in
                        guard let self = self else {return}
                        self.imageSearch.image = UIImage(named: "home_search_ic")
                        self.tfSearch?.alpha = 0
                        self.btnClear?.alpha = 0
                        self.coverSearchInput?.layer.removeSketchShadow()
                        self.coverLogo?.alpha = 1
                        self.widthSearchBoxConstraint.constant = 0
        })
    }
    
    
    private func layoutTopViewIfNeed(_ stickyPoint: CGFloat, _ fullHeight: CGFloat) {
        let tSpace = fullHeight - stickyPoint
        let maxLeft: CGFloat = 75.0
        let anchor: CGFloat = 100
        
        if tSpace < anchor {
            let _space = anchor - tSpace
            let percent = min(_space, maxLeft) / maxLeft
            
            
            //print("did drag to percent --->  \(percent)")
            self.maskView?.backgroundColor = UIColor("rgba 255 255 255 \(percent)")
            self.maskView?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: Float(min(0.24, percent)), x: 3, y: 4, blur: 13, spread: 0)
        } else {
            self.maskView?.backgroundColor = .clear
            self.maskView?.layer.removeSketchShadow()
        }
    }
    
    
    @objc func updatePositionLocation(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            
            let stickyPoint = uData["stickyPoint"] as? CGFloat ?? 0.0
            let fullHeight = uData["fullHeight"] as? CGFloat ?? 0.0
            
            self.layoutTopViewIfNeed(stickyPoint, fullHeight)
            
        }
    }
    
    @objc func getSwipeDeals(_ notification: Notification) {
        if GLOBAL.GlobalVariables.isFirstInitApp {
            setTimeout({
                self.checkToShowStarter()
            }, TimeInterval(GLOBAL.GlobalVariables.sysConfigModel.timePushStarter * 1000))
        } else {
            setTimeout({
                self.checkToShowDaily()
            }, 100)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        GLOBAL.GlobalVariables.discoveryInstance = self
        
    }
    
    private func checkToShowDaily() {
        setTimeInterval({ [weak self] (number, timer) in
            guard let self = self else {return}
            //print("checkToShowDaily - ING")
            if GLOBAL.GlobalVariables.isShowLoyaltyFirst == false {
                    timer.invalidate()
                    self.handleGetSwipeDeals(.DAILY)
                }
            }, 1000)
    }
    
    private func checkToShowStarter() {
        setTimeInterval({ [weak self] (number, timer) in
            guard let self = self else {return}
            print("checkToShowStarter - ING")
                if Utils.isValidToSwipeDeal() {
                    timer.invalidate()
                    self.handleGetSwipeDeals(.STARTER)
                }
            }, 1000)
    }
    
    private func handleGetSwipeDeals(_ type: SWIPE_DEAL_TYPE = .DAILY) {
        Utils.showLoading()
        
        APICommonServices.getSwipeDeals(type.rawValue) { [weak self] (resp) in
            guard let self = self else {return}
            
             mPrint("getSwipeDeal(\(type.rawValue.uppercased())) --> 😍😍😍", resp)
            
            if let resp = resp, let status = resp["status"] as? Bool, status == true, let deals = resp["data"] as? [[String: Any?]], deals.count > 0  {
                self.openSwipeDeals(deals, false, false, type == SWIPE_DEAL_TYPE.STARTER)
            }
            
            Utils.dismissLoading()
            
        }
    }
    
    
    private func decorate() {
        self.btnClear?.alpha = 0
        self.tfSearch?.alpha = 0
        
        self.coverMenu.backgroundColor = .white
        self.coverMenu.layer.cornerRadius = 20.0
        self.coverMenu.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverSearchButton?.backgroundColor = UIColor(228, 109, 87, 1)
        self.coverSearchButton?.layer.cornerRadius = 20.0
        self.coverSearchButton?.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverSearchInput?.backgroundColor = .white
        self.coverSearchInput?.layer.cornerRadius = 19.0
        
        self.subCoverSearch?.layer.cornerRadius = 19.0
        
        
        self.maskView?.backgroundColor = .clear
    }
    
    func updateLanguages() {
        
    }
    
    
    @IBAction func onClickedTabMapView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .THINGS_TO_DO
        //        self.navigate(toIndex: WALLET_SCREEN_ID.THINGS_TO_DO.rawValue)
        
    }
    
    @IBAction func onClickedTabListView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .TRIP_LIST
        //        self.navigate(toIndex: WALLET_SCREEN_ID.TRIP_LIST.rawValue)
    }
    
    @IBAction func onClickedTabWalletView(_ sender: Any) {
        //        GLOBAL.GlobalVariables.lastWalletScreen = .SAVED_EVENTS
        //        self.navigate(toIndex: WALLET_SCREEN_ID.SAVED_EVENTS.rawValue)
    }
    
    private func toggleMenu() {
        self.toggleLeft()
    }
    
    private func renderContent() {
        
        let tabMapView = HomeMapViewController()
        tabMapView.tabClickeCallback = { tabIndex in
            self.navigate(toIndex: tabIndex)
        }
        
        let tabListView = HomeListViewController()
        tabListView.tabClickeCallback = { tabIndex in
            self.navigate(toIndex: tabIndex)
        }
        
        let tabWalletView = HomeWalletViewController()
        tabWalletView.tabClickeCallback = { tabIndex in
            self.navigate(toIndex: tabIndex)
        }
        
        
        mTabBarController.delegate = self
        //        mTabBarController.viewControllers = [tabMapView, tabListView, tabWalletView]
        mTabBarController.viewControllers = [tabMapView]
        mTabBarController.tabBar.isHidden = true
        mTabBarController.selectedIndex = tempTabIndex
        
        self.contentView.addSubview(mTabBarController.view)
        self.addChild(mTabBarController)
        
        mTabBarController.view.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
    }
    
    private func onActive(fromIndex: Int, toIndex: Int) {
        
        
    }
    
    
    func navigate(toIndex: Int) -> Void {
        //  let controllerIndex = tabBarController.viewControllers?.firstIndex(of: viewController)
        if toIndex == DISCOVERY_SCREEN_ID.MENU.rawValue {
            self.toggleMenu()
            
            return
        }
        
        
        if mTabBarController.selectedIndex == toIndex || toIndex == self.tempTabIndex {
            return;
        }
        
        self.onActive(fromIndex: self.tempTabIndex, toIndex: toIndex)
        
        self.tempTabIndex = toIndex
        
        
        // Get the views.
        let fromView = mTabBarController.selectedViewController?.view
        let toView = mTabBarController.viewControllers![toIndex].view
        
        // Get the size of the view area.
        let viewSize: CGRect = fromView!.frame
        let scrollRight: Bool = toIndex > mTabBarController.selectedIndex
        
        // Add the to view to the tab bar view.
        fromView!.superview?.addSubview(toView!)
        
        // Position it off screen.
        let screenWidth: CGFloat = UIScreen.main.bounds.width
        toView!.frame = CGRect(x: (scrollRight ? screenWidth : -screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            fromView!.frame = CGRect(x: (scrollRight ? -screenWidth : screenWidth), y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
            toView!.frame = CGRect(x: 0, y: viewSize.origin.y, width: screenWidth, height: viewSize.size.height)
        }, completion: {  [weak self] (finished) in
            guard let self = self else {return}
            
            if finished {
                // Remove the old view from the tabbar view.
                fromView!.removeFromSuperview()
                self.mTabBarController.selectedIndex = toIndex
            }
        })
        
    }
    
    // MARK: LOYALTY DEALS SETUP
    func showLoyaltyDeals() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            let storyboard = UIStoryboard(name: "LoginLoyaltyDeals", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginLoyaltyDeals")
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false)
        })
    }
    
    // MARK: SWIPE DEALS SETUP
    func setupSwipeDeals() {
        let calendar = Calendar.current
        let now = Date()
        let date = calendar.date(
            bySettingHour: 18,
            minute: 30,
            second: 0,
            of: now)!

        let timer = Timer(fireAt: date, interval: 0, target: self, selector: #selector(showSwipeDeals), userInfo: nil, repeats: false)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
    
    @objc func showSwipeDeals() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            let storyboard = UIStoryboard(name: "LoginSwipeDeals", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginSwipeDeals")
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false)
        })
    }
    
}

extension DiscoveryMainController: UITextFieldDelegate {
    
    func textFieldDidChange() {
        if let timer =  self.inputTimer {
            timer.invalidate()
            self.inputTimer = nil;
        }
        
        self.inputTimer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false) { timer in
            self.handleFilter()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.handleFilter()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            GLOBAL.GlobalVariables.selectedSearchText = updatedText
            
            self.btnClear.alpha = updatedText == "" ? 0 : 1
            self.textFieldDidChange()
            //self.handleFilter()
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfSearch {
            if let _pull = GLOBAL.GlobalVariables.pullQuickInstance {
                _pull.navigateTo(PULL_FULL_HEIGHT, PULL_FULL_HEIGHT, animated: true)
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //_ = validateCode()
    }
}

extension DiscoveryMainController: UITabBarControllerDelegate {
    
    
}
