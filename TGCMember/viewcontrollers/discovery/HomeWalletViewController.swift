//
//  HomeWalletViewController.swift
//  TGCMember
//
//  Created by vang on 11/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

let NEW_HEIGHT_TITLE_CAROUSEL_WALLET: CGFloat = 60.0

public enum NEW_HEIGHT_CELL_GROW {
    static let CLOSE: CGFloat = NEW_HEIGHT_TITLE_CAROUSEL_WALLET + NEW_HEIGHT_GROW_VIEW.CLOSE
    static let OPEN: CGFloat = NEW_HEIGHT_TITLE_CAROUSEL_WALLET + NEW_HEIGHT_GROW_VIEW.OPEN
}



class HomeWalletViewController: BaseViewController {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var coverMenu: UIView!
    @IBOutlet weak var coverHome: UIView!
    @IBOutlet weak var tbDeals: UITableView!
    
    var tabClickeCallback: TabClickedCallback?
    
    var callback: DataCallback?
    
    private let heightWideDeal: CGFloat = 200
    private var listDeals:[[String: Any?]]  = []
    private var cardOnScreen: CardOnScreenView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tbDeals.register(UINib(nibName: "DealWideCell", bundle: nil), forCellReuseIdentifier: "DealWideCell")
        self.tbDeals.register(UINib(nibName: "NewGroupDealCell", bundle: nil), forCellReuseIdentifier: "NewGroupDealCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(downHeightForCarousel(_:)), name: .DOWN_HEIGHT_FOR_CAROUSEL_IN_WALLET, object: nil)
        
        
        decorate()
        
        self.getDealsInWallet()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .DOWN_HEIGHT_FOR_CAROUSEL_IN_WALLET, object: nil)
    }
    
    
    @objc func downHeightForCarousel(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            let tIndexTableCell = uData["indexTableCell"] as? Int ?? 0
            
            UIView.performWithoutAnimation {
                self.tbDeals.beginUpdates()
                
                var tInfo = [String: Any]()
                tInfo["indexTableCell"] = tIndexTableCell
                
                NotificationCenter.default.post(name: .RELOAD_GROUP_GROW_CARD, object: tInfo)
                
                self.tbDeals.endUpdates()
            }
        }
    }
    
    private func decorate() {
        self.topView.backgroundColor = UIColor(255, 193, 6)
        //self.topView.layer.cornerRadius = 19.0
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.topView.frame
        rectShape.position = self.topView.center
        rectShape.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: self.topView.bounds.height), byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 40, height: 40)).cgPath
        
        
        self.topView.layer.mask = rectShape
        
        self.topView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        self.coverMenu.backgroundColor = .white
        self.coverMenu.layer.cornerRadius = 20.0
        self.coverMenu.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        self.coverHome.backgroundColor = .white
        self.coverHome.layer.cornerRadius = 20.0
        self.coverHome.layer.applySketchShadow(color: UIColor(58, 56, 53, 1), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
    }
    
    @IBAction func onClickedMenu(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.MENU.rawValue)
    }
    
    @IBAction func onClickedHome(_ sender: Any) {
        self.tabClickeCallback?(DISCOVERY_SCREEN_ID.MAP_VIEW.rawValue)
    }
    
    private func getParamsRequest() -> [String: Any] {
        var params = [String: Any]()
        
        
        return params
    }
    
    func configHeightForEachCardOnCarousel(_ keepGrowedCell: Bool = false, _ withoutCellIndex: Int = -1) {
        var allCellHeights =  [Int: [CGFloat]]()
        
        for index in 0..<self.listDeals.count {
            if withoutCellIndex != index {
                let data = self.listDeals[index]
                if Utils.isCarouselDeal(data) {
                    let lDeals = data["List"] as? [[String: Any?]] ?? []
                    
                    let cellHeights = Array(repeating: NEW_HEIGHT_CELL_GROW.CLOSE, count: lDeals.count)
                    
                    allCellHeights[index] = cellHeights
                }
            } else {
                allCellHeights[index] = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[index]
                
            }
        }
        
        if !keepGrowedCell {
            GLOBAL.GlobalVariables.growedCell = nil
        }
        
        GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel = allCellHeights
        
        tbDeals.estimatedRowHeight = NEW_HEIGHT_CELL_GROW.CLOSE
        tbDeals.rowHeight = UITableView.automaticDimension
    }
    
    
    func resetPageCarousel(_ isReset: Bool = true) {
        var allCellPages = [Int]()
        
        allCellPages = Array(repeating: 1, count: self.listDeals.count)
        
        GLOBAL.GlobalVariables.pagesWalletCarousel = allCellPages
    }
    
    private func getDealsInWallet(_ showLoading: Bool = true, completed _completed: FinishedCallback? = nil) {
        if showLoading {
            Utils.showLoading()
        }
        
        let params = getParamsRequest()
        self.listDeals = []
        
        APICommonServices.getListDealInWallets(params) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("getListDealInWallets", resp)
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?] , status == true  {
                    
                    if let list = data["list"]  as? [String : Any?], let records = list["Records"]  as? [String : Any?] {
                        
                        let carouselDeals = records["Carousel"] as? [[String: Any?]] ?? []
                        let normalDeals = records["Normal"] as? [[String: Any?]] ?? []
                        
                        if carouselDeals.count > 0 {
                            self.listDeals.append(contentsOf: carouselDeals)
                        }
                        
                        if normalDeals.count > 0 {
                            self.listDeals.append(contentsOf: normalDeals)
                        }
                        
                    }
                    
                    self.configHeightForEachCardOnCarousel()
                    self.resetPageCarousel()
                    
                    self.tbDeals.reloadData(completion: {
                        _completed?()
                        self.callback?(nil)
                        self.callback = nil
                    })
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
            
        }
    }
    
    private func displayCardOnScreen(_ data: [String: Any?]?) {
        self.cardOnScreen = CardOnScreenView().loadView()
        self.cardOnScreen?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
        
        self.cardOnScreen?.config(data, onClickedCloseCallback: { [weak self] (data) in
            guard let self = self else {return}
            
                Utils.closeCustomAlert()
            
            }, onClickedUseDealCallback: { [weak self] (pData) in
                guard let self = self else {return}
                
                self.showClaimAlert(pData)
            }, onClickedDeleteCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
                
            })
        
        if let content = self.cardOnScreen {
            Utils.showAlertWithCustomView(content, bgColor: UIColor(0, 0, 0, 0.8))
        }
    }
    
    private func showClaimAlert(_ pData: [String: Any?]?) {
        
        if let tCard = pData, let cardData = tCard["cardData"] as? [String: Any?] {
            Utils.showClaimAlert(cardData, bgColor: UIColor(0, 0, 0, 0.1)) {  [weak self] (success, data) in
                guard let self = self else {return}
                if success {
                    setTimeout({
                        self.cardOnScreen?.addAnimationClaim({
                            Utils.closeCustomAlert()
                        })
                    }, 200)
                    
                    self.getDealsInWallet(false, completed: {
                        if let tData = data {
                            NotificationCenter.default.post(name: .SCROLL_TO_GROW_CARD_THEN_OPEN, object: tData)
                        }
                    })
                }
            }
            
        }
        
    }
    
    private func scrollToCarousel(_ data: [String: Any?]?) {
        if let tData = data {
            let indexRow = tData["indexTableCell"] as? Int ?? 0
            
            self.tbDeals.scrollToRow(at: IndexPath(row: indexRow, section: 0), at: .middle, animated: true)
            
            //print("scrollToCarousel --> ", indexRow)
        }
        
    }
    
}



extension HomeWalletViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDeals.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var data = listDeals[indexPath.row]
        
        if Utils.isCarouselDeal(data) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewGroupDealCell") as? NewGroupDealCell
            
            let backgroundView = UIView()
            backgroundView.frame = cell?.frame ?? .zero
            backgroundView.backgroundColor = .blue
            cell?.backgroundView = backgroundView
            cell?.backgroundColor = .clear
            
            cell?.parentHomeWallet = self
            data["indexTableCell"] = indexPath.row
            data["totalDeals"] = self.listDeals
            
            cell?.config(data, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                var isNeedUpdateHeightForCell = true
                
                if let tData = data {
                    isNeedUpdateHeightForCell = tData["isNeedUpdateHeightForCell"] as? Bool ?? true
                }
                
                if isNeedUpdateHeightForCell {
                    
                    UIView.performWithoutAnimation {
                        self.tbDeals.beginUpdates()
                        self.tbDeals.endUpdates()
                    }
                    
                }
                
                }, onClickedShowDetailCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.displayCardOnScreen(data)
                }, onScrollToCarouselCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.scrollToCarousel(data)
            })
            
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DealWideCell") as? DealWideCell
            
            cell?.config(data as? [String: Any?], onClickedCell: { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data, let type = tData["Type"] as? String {
                    if type == SUGGEST_TYPE.DEAL.rawValue {
                        let dealDetailVC = DealDetailViewController()
                        dealDetailVC.dealInfo = tData
                        self.navigationController?.pushViewController(dealDetailVC, animated: true)
                    } else if type == SUGGEST_TYPE.OFFER.rawValue {
                        let vendorDetailVC = VendorDetailViewController()
                        vendorDetailVC.summaryData = tData["VendorSummary"] as? [String: Any?]
                        vendorDetailVC.initTab = VENDOR_INIT_TAB.BASIC_DEAL
                        self.navigationController?.pushViewController(vendorDetailVC, animated: true)
                    }
                }
                
                }, onClickedMore: { [weak self] (data) in
                    guard let self = self else {return}
                    
            })
            
            return cell!
        }
        
        return UITableViewCell()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = listDeals[indexPath.row]
        
        if Utils.isCarouselDeal(data){
            return self.getHeightGrowCellFromIndex(indexPath.row)
        }
        
        return heightWideDeal
    }
    
    private func getHeightGrowCellFromIndex(_ index: Int) -> CGFloat {
        let collectionCellHeights = GLOBAL.GlobalVariables.cellHeightsHomeWalletCarousel[index]
        
        let maxHeight = collectionCellHeights?.max() ?? NEW_HEIGHT_CELL_GROW.CLOSE
        
        return maxHeight
    }
    
}
