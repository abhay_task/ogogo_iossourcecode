//
//  TabCategoryController.swift
//  TGCMember
//
//  Created by vang on 11/27/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class TabCategoryController: BaseViewController {
    func updateLanguages() {
        
    }
    
    @IBOutlet weak var tblCategory: UITableView!
    private var listCategories:[[String: Any?]]  = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblCategory.register(UINib(nibName: "CategoryWideCell", bundle: nil), forCellReuseIdentifier: "CategoryWideCell")
        
        getListCategories()
    }
    
    private func getListCategories() {
        Utils.showLoading()
        APICommonServices.getListCategories { [weak self] (resp) in
            guard let self = self else {return}
            //mPrint("getListCategories", resp)
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [String: Any?] , let list = data["categories"]  as? [[String : Any?]] {
                    self.listCategories = list

                    self.tblCategory.reloadData()
                }
            }

            Utils.dismissLoading()
        }

    }
}


extension TabCategoryController: UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var data = listCategories[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryWideCell") as? CategoryWideCell
        
        cell?.config(data as? [String: Any?], onClickedCellCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            
        })
        
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162.0
    }
    
    
}
