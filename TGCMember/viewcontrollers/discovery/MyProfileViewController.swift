//
//  MyProfileViewController.swift
//  TGCMember
//
//  Created by vang on 5/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class MyProfileViewController: BaseViewController {
    func updateLanguages() {
        self.lblUpgrade.text = LOCALIZED.txt_upgrade_title.translate.uppercased()
    }
    
    @IBOutlet weak var topInfoConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingAvatar: UIActivityIndicatorView!
    @IBOutlet weak var lblExpired: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var coverAvatar: UIView!
    @IBOutlet weak var coverBack: HighlightableView!
    
    @IBOutlet weak var imageUpgrade: UIImageView!
    @IBOutlet weak var coverTxt: UIView!
    @IBOutlet weak var coverUpgrade: UIView!
    @IBOutlet weak var lblUpgrade: UILabel!
    
  
    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedUpgrade(_ sender: Any) {
        
        let newUpgradeVC = NewUpgradeViewController()
        
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            newUpgradeVC.initPlanIndex = _upgradeModel.canUpToNextStep.viewIndex
        }
        
        self.navigationController?.pushViewController(newUpgradeVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.decorate()
        
        self.fillData()
    }
    

    private func decorate() {
        topInfoConstraint?.constant = SCREEN_HEIGHT > 667 ? 195 : 150
        
        coverBack?.layer.cornerRadius = 20.0
        coverBack?.layer.applySketchShadow(color: UIColor("rgb 58 56 53"), alpha: 0.24, x: 3, y: 4, blur: 13, spread: 0)
        
        coverAvatar?.layer.cornerRadius = 78.0
        coverAvatar?.layer.borderWidth = 2.1
        coverAvatar?.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        
        avatarImage?.layer.cornerRadius = 68.0
        
        coverTxt.layer.cornerRadius = 4.0
        coverTxt.backgroundColor = UIColor.white
        coverTxt.clipsToBounds = false
        coverTxt.layer.applySketchShadow(color: UIColor("rgba 120 121 147 1"), alpha: 0.2, x: 0, y: 4, blur: 30, spread: 0)
        
        if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
            coverUpgrade.isHidden = !_upgradeModel.canUpNextStep
        } else {
            coverUpgrade.isHidden = true
        }
    }

    private func fillData() {
        if let _userInfo = GLOBAL.GlobalVariables.userInfo {
            self.lblName.text = _userInfo["DisplayName"] as? String ?? ""
            self.lblRole.text = _userInfo["CurrentPlan"] as? String ?? ""
            
            if let subExpiredAt = _userInfo["SubscriptionExpiredAt"] as? String {
                self.lblExpired.isHidden = false
                let tDate = Utils.stringFromStringDate(subExpiredAt, toFormat: "MMM yyyy")

                var textExpired = LOCALIZED.expired_in_date.translate
                textExpired = textExpired.replacingOccurrences(of: "[%__TAG_DATE__%]", with: tDate)

                self.lblExpired.text = textExpired
            } else {
                self.lblExpired.isHidden = true
            }
           
            self.loadingAvatar.startAnimating()
            self.avatarImage.sd_setImage(with:  URL(string: _userInfo["AvatarURL"] as? String ?? ""), completed:  {  [weak self]  (_, _, _, _) in
                guard let self = self else {return}
                
                self.loadingAvatar.stopAnimating()
            })
            
            if let _upgradeModel = GLOBAL.GlobalVariables.upgradePackageModel {
                self.imageUpgrade.sd_setImage(with:  URL(string: _upgradeModel.iconUpgradeNextStep), completed:  {  [weak self]  (_, _, _, _) in
                    guard let self = self else {return}
                    
                })
            } else {
                self.imageUpgrade.image = nil
            }
            
            
            
        }
    }
}
