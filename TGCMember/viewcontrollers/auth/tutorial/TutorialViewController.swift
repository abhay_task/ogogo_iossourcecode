//
//  TutorialViewController.swift
//  TGCMember
//
//  Created by vang on 3/6/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

let BOTTOM_PAGING_TUTORIAL: CGFloat = SCREEN_HEIGHT <= 736 ? 10 : 50
let TOP_SKIP_TUTORIAL: CGFloat = Utils.isIPhoneNotch() ? 64 : (SCREEN_HEIGHT <= 736 ? 30: 40)

class TutorialViewController: BaseViewController {
    func updateLanguages() {
        btnSkip?.setTitle(LOCALIZED.txt_skip_title.translate, for: .normal)
    }
    
 
    @IBOutlet weak var topSkipConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomPagingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pagingView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnSkip: UIButton!
    
    private var mPageViewController: UIPageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    var currentIndex: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        decorate()
        renderContent()
        self.renderDot()
    }
    
    fileprivate lazy var pages: [UIViewController] = {
        var listPage: [UIViewController] = []
        
        for index in 0..<GLOBAL.GlobalVariables.tutorialConfig.count {
            var tData = GLOBAL.GlobalVariables.tutorialConfig[index]
            tData["currentIndex"] = index
            tData["totalCount"] = GLOBAL.GlobalVariables.tutorialConfig.count
            
            let subPage = SubTutorialPage()
            subPage.config(tData) { [weak self] (data) in
                guard let self = self else {return}
                
                self.onClickedNext()
            }
            
            listPage.append(subPage)
        }
        
        return listPage
    }()
    
    @IBAction func onClickedSkip(_ sender: Any) {
        self.navigationController?.pushViewController(NewWelcomeViewController(), animated: true)
    }
    
    private func onClickedNext() {
        self.navigationController?.pushViewController(NewWelcomeViewController(), animated: true)
    }
    
    private func decorate() {
        mPageViewController.view.backgroundColor = .clear
        self.topSkipConstraint?.constant = TOP_SKIP_TUTORIAL
        self.bottomPagingConstraint?.constant = BOTTOM_PAGING_TUTORIAL
        
    }
    
    private func renderContent() {
        for subview in self.mPageViewController.view.subviews {
            if subview is UIScrollView {
                let tScroll = subview as! UIScrollView
                tScroll.delegate = self
            }
        }
        
        self.mPageViewController.dataSource = self
        self.mPageViewController.delegate   = self
        
        
        if let firstVC = pages.first  {
            mPageViewController.setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
        }
        
        self.contentView.addSubview(mPageViewController.view)
        self.addChild(mPageViewController)
        
        mPageViewController.view.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView)
        }
    }
    
    
    private func renderDot() {
        btnSkip?.isHidden = self.currentIndex == (self.pages.count - 1)

        self.pagingView.removeSubviews()
        let spaceDot: CGFloat = 10
        let hieghtItem: CGFloat = 11
        
        let coverDot = UIView()
        coverDot.backgroundColor = .clear
        self.pagingView.addSubview(coverDot)
        
        coverDot.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.pagingView)
            make.centerY.equalTo(self.pagingView)
            make.height.equalTo(30)
        }
        
        var lastDot: UIView? = nil
        
        for index in 0..<self.pages.count {
            let tDot = UIView()
            tDot.backgroundColor = self.currentIndex == index ? .white : .clear
            tDot.layer.cornerRadius = hieghtItem/2
            
            if self.currentIndex != index {
                tDot.layer.borderWidth = 1.0
                tDot.layer.borderColor = UIColor.white.cgColor
            }
            
            coverDot.addSubview(tDot)
            
            if index == self.pages.count - 1 {
                if lastDot == nil {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalToSuperview()
                        make.right.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                } else {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalTo(lastDot!.snp.right).offset(spaceDot)
                        make.right.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                }
            } else {
                if lastDot == nil {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                } else {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalTo(lastDot!.snp.right).offset(spaceDot)
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                }
            }
            
            lastDot = tDot
        }
    }
    
}

extension TutorialViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentIndex == pages.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if currentIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentIndex == pages.count - 1 && scrollView.contentOffset.x > scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
}

extension TutorialViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return nil }
        
        guard pages.count > previousIndex else { return nil }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil }
        
        return pages[nextIndex]
    }
    
    // IMPORTANT: that's the key why it works, don't forget to add it
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex
    }
}

extension TutorialViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            if let currentViewController = pageViewController.viewControllers?.first,
                let index = pages.firstIndex(of: currentViewController) {
                self.currentIndex = index
            }
        }
        
        self.renderDot()
       // print("self.currentIndex --> ", self.currentIndex)
    }
}
