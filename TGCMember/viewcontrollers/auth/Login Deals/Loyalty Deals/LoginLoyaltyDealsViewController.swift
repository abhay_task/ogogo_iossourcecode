//
//  LoginLoyaltyDealsViewController.swift
//  TGCMember
//
//  Created by Stephen James Del Rosario on 2/1/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit

class LoginLoyaltyDealsViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cardViewContainer: UIView!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var exitBtn: UIButton!
    @IBOutlet weak var dealImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateEntry()
    }
    
    
    // MARK: - Button Actions
    @IBAction func didPressExitButton(_ sender: Any) {
        animateExit()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @IBAction func didPressChatButton(_ sender: Any) {
        // Proceed to Chat Interface
    }
    
    // MARK: - Initial UI Setup
    func setupUI() {
        cardViewContainer.layer.cornerRadius = 20
        dealImage.layer.cornerRadius = 15
        chatBtn.layer.cornerRadius = 5
        chatBtn.layer.borderWidth = 1
        chatBtn.layer.borderColor = UIColor(82, 197, 255, 1).cgColor
        backgroundView.backgroundColor = UIColor(0, 0, 0, 0.75)
    }
}

// MARK: - Animations
extension LoginLoyaltyDealsViewController {
    
    func animateEntry() {
        UIView.animate(withDuration: 0.0, delay: 0.0, options: .curveEaseIn, animations: {
            self.cardViewContainer.transform = CGAffineTransform.identity.scaledBy(x: 0.3, y: 0.3)
            })
        { (finished) in
            UIView.animate(withDuration: 0.5, animations: {
              self.cardViewContainer.transform = CGAffineTransform.identity
            })
        }
    }
    
    func animateExit() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            self.cardViewContainer.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.chatBtn.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.exitBtn.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
            self.backgroundView.backgroundColor = UIColor(0, 0, 0, 0.0)
        })
    }
}
