//
//  LoginSwipeDealsViewController.swift
//  TGCMember
//
//  Created by Stephen James Del Rosario on 1/28/21.
//  Copyright © 2021 gkim. All rights reserved.
//

import UIKit
import Shuffle_iOS

class LoginSwipeDealsViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cardViewContainer: UIView!
    @IBOutlet weak var cardViewContainerYConst: NSLayoutConstraint! {
        didSet {
            cardViewContainerYConst.constant = 1000
        }
    }
    
    let fadeInColor = UIColor(0, 0, 0, 0.6)
    let fadeOutColor = UIColor(0, 0, 0, 0.0)
    
    let cardBgImages = [UIImage(named: "bg_black"), UIImage(named: "bg_silver"), UIImage(named: "bg_gold")]
    
    let cardStack = SwipeCardStack()
    
    override func viewDidLoad() {
      super.viewDidLoad()
      
        cardStack.dataSource = self
        cardStack.delegate = self
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animateEntry()
    }
    
    // MARK: - Create Card
    func card(fromImage image: UIImage) -> SwipeCard {
        let card = SwipeCard()
        card.swipeDirections = [.left, .right]
        card.content = UIImageView(image: image)

        let leftOverlay = UIView()
        leftOverlay.backgroundColor = UIColor(100, 0, 0, 0.5)
        leftOverlay.layer.cornerRadius = 10

        let rightOverlay = UIView()
        rightOverlay.backgroundColor = UIColor(0, 100, 0, 0.5)
        rightOverlay.layer.cornerRadius = 10
        
        card.setOverlays([.left: leftOverlay, .right: rightOverlay])

        return card
    }
    
    // MARK: - Initial UI Setup
    func setupUI() {
        cardViewContainer.addSubview(cardStack)
        cardStack.frame = cardViewContainer.safeAreaLayoutGuide.layoutFrame
        
        cardViewContainer.backgroundColor = .clear
    }
}

// MARK: - Data Source
extension LoginSwipeDealsViewController: SwipeCardStackDataSource {
    
    func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
        return card(fromImage: cardBgImages[index]!)
    }
    
    func numberOfCards(in cardStack: SwipeCardStack) -> Int {
        cardBgImages.count
    }
}

// MARK: - Delegate
extension LoginSwipeDealsViewController: SwipeCardStackDelegate {
    
    func didSwipeAllCards(_ cardStack: SwipeCardStack) {

        animateExit()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
}

// MARK: - Animations
extension LoginSwipeDealsViewController {
    
    func animateEntry() {
        UIView.animate(withDuration: 1, delay: 1, options: .curveEaseIn, animations: {
            self.backgroundView.backgroundColor = self.fadeOutColor
            self.backgroundView.backgroundColor = self.fadeInColor
        })
        
        UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseIn, animations: {
            self.cardViewContainerYConst.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func animateExit() {
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: { [self] in
            self.backgroundView.backgroundColor = self.fadeInColor
            self.backgroundView.backgroundColor = self.fadeOutColor
        })
    }
}
