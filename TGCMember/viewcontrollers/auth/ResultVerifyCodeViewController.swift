//
//  ResultVerifyCodeViewController.swift
//  TGCMember
//
//  Created by vang on 7/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

public enum VERIFY_CODE_STATUS {
    case SUCCESS
    case FAILED
}

class ResultVerifyCodeViewController: BaseViewController {
    func updateLanguages() {
        self.btnReportIssue.setTitle(LOCALIZED.txt_report_issue.translate, for: .normal)
    self.btnAccessApp.setTitle(LOCALIZED.access_the_app_without_the_card.translate, for: .normal)
        
        self.btnYesLogin.setTitle(LOCALIZED.yes_log_in.translate, for: .normal)
        self.btnRecognize.setTitle(LOCALIZED.i_don_t_recognize_this_email.translate, for: .normal)
    }
    
 
    @IBOutlet weak var btnYesLogin: UIButton!
    @IBOutlet weak var btnRecognize: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblEmailSuggest: UILabel!
    @IBOutlet weak var lblPromptSuccess: UILabel!
    @IBOutlet weak var viewSuccess: UIView!
    @IBOutlet weak var viewError: UIView!
    @IBOutlet weak var heightTempFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var lblPrompt2: UILabel!
    @IBOutlet weak var lblPrompt1: UILabel!
    @IBOutlet weak var btnReportIssue: UIButton!
    @IBOutlet weak var btnAccessApp: UIButton!
    @IBOutlet weak var widthImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImageConstraint: NSLayoutConstraint!
    
    var verifyStatus: VERIFY_CODE_STATUS = .SUCCESS
    var data: [String: Any]?
    var scratchCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        decorate()
    }

    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedYesLogin(_ sender: Any) {
        Utils.logoutApp()
        
        let loginVC = LoginViewController()
        //loginVC.referralInfo = self.data
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func onClickedReport(_ sender: Any) {
        let reportIssueVC = ReportIssueViewController()
        reportIssueVC.scratchCode = self.scratchCode
        self.navigationController?.pushViewController(reportIssueVC, animated: true)
    }
    
    @IBAction func onClickedAccessApp(_ sender: Any) {
        if let discoveryIns = GLOBAL.GlobalVariables.discoveryInstance {
            self.navigationController?.popToViewController(discoveryIns, animated: true)
        } else {
            let _discoveryIns = DiscoveryMainController()
            self.navigationController?.pushViewController(_discoveryIns, animated: true)
        }
    }
    
    @IBAction func onClickedRecognize(_ sender: Any) {
        // show alert
        self.showConfirmRegistration()
    }
    
    private func showConfirmRegistration() {
        let content = NoticeCompleteRegistrationView().loadView()

        content.config(nil, onClickedCompleteCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            self.handleCompleteRegistration()
            
        }, onClickedCancelCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            if let freeVC = GLOBAL.GlobalVariables.freeAccountInstance  {
                 self.navigationController?.popToViewController(freeVC, animated: true)
            } else if let nextVC = GLOBAL.GlobalVariables.verifyCodeInstance {
                self.navigationController?.popToViewController(nextVC, animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
           
        }, onClickedCloseCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    private func handleCompleteRegistration() {
        let signUpVC = SignUpViewController()
        signUpVC.isSignUpFirst = true
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    private func decorate() {
        self.viewError.isHidden = verifyStatus == .SUCCESS ? true : false
        self.viewSuccess.isHidden = verifyStatus == .SUCCESS ? false : true
        
        if let tData = self.data {
            self.lblEmailSuggest.text = tData["Email"] as? String
        }
        
        self.bodyView.backgroundColor  = .white
        let heightScreen = UIScreen.main.bounds.height
        heightImageConstraint.constant = heightScreen <= 667 ? 200 : 270
        
        self.heightTempFooterConstraint.constant = defaultBottom
        self.btnAccessApp.layer.cornerRadius = 3.0
        self.btnAccessApp.layer.applySketchShadow()
        
        self.btnReportIssue.backgroundColor = UIColor.white
        self.btnReportIssue.layer.cornerRadius = 3.0
        self.btnReportIssue.layer.borderColor = UIColor.clear.cgColor
        self.btnReportIssue.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.4, x: 0, y: 0, blur: 3, spread: 0)
    
        self.btnYesLogin.layer.cornerRadius = 3.0
        self.btnYesLogin.layer.applySketchShadow()
        
        self.btnRecognize.backgroundColor = UIColor.white
        self.btnRecognize.layer.cornerRadius = 3.0
        self.btnRecognize.layer.borderColor = UIColor.clear.cgColor
        self.btnRecognize.layer.applySketchShadow(color: UIColor(0, 0, 0), alpha: 0.4, x: 0, y: 0, blur: 3, spread: 0)
        
    }
}
