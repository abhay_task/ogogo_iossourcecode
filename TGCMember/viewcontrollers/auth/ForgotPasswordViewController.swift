//
//  LoginViewController.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit



class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var heightLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightHeaderConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var coverEmailView: UIView!
    
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imageValid: UIImageView!
    @IBOutlet weak var coverEmailWarning: UIView!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    @IBOutlet weak var btnSend: HighlightableButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var coverBody: UIView!
    @IBOutlet weak var lblCopyright: UILabel!
    
    @IBOutlet weak var lblTitleReset: UILabel!
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.dismissKeyboard()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onClickedSend(_ sender: Any) {
        self.dismissKeyboard()
        self.doSend()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        decorate()
        
        
        _ = validateAllField()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    private func decorate() {
        
        coverBody?.layer.cornerRadius = CGFloat(40)
        coverBody?.layer.masksToBounds = true
        coverBody?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailView.layer.borderWidth = 0.5
        self.coverEmailView.layer.cornerRadius = 3.0
        self.coverEmailView.layer.masksToBounds = true
        
        self.heightFooterConstraint.constant = Utils.isIPhoneNotch() ? 90 : 70
        
        self.coverEmailWarning.isHidden = true
        
    }
    
    private func validateAllField(_ textCheck: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = true
        
        let isValidEmail = self.validateEmail(textCheck, withoutField)
        
        if !isValidEmail {
            isValid = false
        }
        
        self.btnSend.backgroundColor = isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSend.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnSend.layer.applySketchShadow()
        } else {
            self.btnSend.layer.removeSketchShadow()
        }
        
        
        return isValid
    }
    
    private func validateEmail(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
        
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverEmailWarning.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.imageValid.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverEmailView.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
        
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailWarning.isHidden  = true
        self.imageValid.isHidden  = true
    }
    
    
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let tBottom = keyboardSize.height - self.heightFooterConstraint.constant - 10
            self.bottomBodyConstraint?.constant = tBottom
            
            self.heightHeaderConstraint.constant = 150
            self.heightLogoConstraint.constant = 71
            self.centerLogoConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self = self else {return}
                self.headerView.superview?.layoutIfNeeded()
                self.coverBody.superview?.layoutIfNeeded()
                }, completion:  { finished in
                    self.heightHeaderConstraint.constant = 150
                    self.heightLogoConstraint.constant = 71
                    self.centerLogoConstraint.constant = 0
                    
                    self.bottomBodyConstraint?.constant = tBottom
            })
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint?.constant = 0
        
        self.heightHeaderConstraint.constant = 240
        self.heightLogoConstraint.constant = 87
        self.centerLogoConstraint.constant = 25
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let self = self else {return}
            self.headerView.superview?.layoutIfNeeded()
            self.coverBody.superview?.layoutIfNeeded()
            }, completion:  { finished in
                self.heightHeaderConstraint.constant = 240
                self.heightLogoConstraint.constant = 87
                self.centerLogoConstraint.constant = 25
                
                self.bottomBodyConstraint?.constant = 0
        })
    }
    
    private func doSend() {
        
        if !validateAllField() {
            return
        }
        
        Utils.showLoading()
        
        let email = self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        APICommonServices.sendEmailResetPassword(email) { (resp) in
            Utils.dismissLoading()
            
            if let tResp = resp  {
                mPrint("sendEmailResetPassword --> ", tResp)
                let status = tResp["status"] as? Bool ?? false
                let message = tResp["message"] as? String ?? ""
                
                Utils.showAlertStyle(message, status, "") { [weak self] in
                    guard let self = self else {return }
                    self.navigationController?.popViewController(animated: true)
                }

            }
        }
    }
    
    private func dismissKeyboard() {
        self.tfEmail.resignFirstResponder()
    }
    
    
    
}



extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        self.doSend()
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateAllField(updatedText, textField)
            
        }
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            }
        }, 100)
        
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateAllField()
    }
}

extension ForgotPasswordViewController {
    func updateLanguages() {
        self.btnSend.setTitle(LOCALIZED.txt_send_title.translate, for: .normal)
        self.lblEmail.text = LOCALIZED.txt_email_title.translate
        self.tfEmail.placeholder = LOCALIZED.enter_your_email_address.translate
        self.lblPrompt.setAttribute(LOCALIZED.message_reset_password.translate, color: UIColor("rgb 39 39 39"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 13), spacing: 3.5)
        self.lblTitleReset.text = LOCALIZED.reset_password.translate
        
        self.lblCopyright.text = Utils.getCopyright()
    }
}
