//
//  ReportIssueViewController.swift
//  TGCMember
//
//  Created by vang on 7/29/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ReportIssueViewController: BaseViewController {
    func updateLanguages() {
        self.lblEmailAddress.text = LOCALIZED.txt_email_address.translate
        self.tfEmail.placeholder = LOCALIZED.enter_your_email_address.translate
        self.lblYourName.text = LOCALIZED.txt_your_name.translate
        self.tfYourName.placeholder = LOCALIZED.enter_your_name.translate
        self.lblWhereYouBought.text = LOCALIZED.where_you_bought_the_card_from.translate
        self.tfBoughtFrom.placeholder = LOCALIZED.hint_city_location.translate
        self.lblNameOfSales.text = LOCALIZED.name_of_sales_person_vendor.translate
        self.lblAdditional.text = LOCALIZED.additional_information.translate
        
        self.btnSendReport.setTitle(LOCALIZED.txt_send_report.translate, for: .normal)
    }
    
    @IBOutlet weak var lblWhereYouBought: UILabel!
    
    @IBOutlet weak var lblAdditional: UILabel!
    @IBOutlet weak var lblNameOfSales: UILabel!
    @IBOutlet weak var lblYourName: UILabel!
    @IBOutlet weak var lblWarningEmail: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var coverWarningView: UIView!
    @IBOutlet weak var statusEmail: UIImageView!
    @IBOutlet weak var btnSendReport: HighlightableButton!
    @IBOutlet weak var coverInputEmail: UIView!
    @IBOutlet weak var coverInputName: UIView!
    @IBOutlet weak var coverInputBought: UIView!
    @IBOutlet weak var coverInputNameOfSale: UIView!
    @IBOutlet weak var coverInputAdditional: UIView!
    @IBOutlet weak var tvAdditional: UITextView!
    @IBOutlet weak var tfBoughtFrom: UITextField!
    @IBOutlet weak var tfYourName: UITextField!
    @IBOutlet weak var tfNameOfSale: UITextField!
    
    var scratchCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
        decorate()
        
        _ = validateAllField()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        self.dismissKeyboard()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let tBottom = keyboardSize.height + 10
            self.bottomScrollConstraint.constant = tBottom
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomScrollConstraint.constant = 0
    }
    
    private func decorate() {
        if let userInfo = GLOBAL.GlobalVariables.userInfo {
            self.tfEmail.text = userInfo["Email"] as? String ?? ""
            self.tfYourName.text = userInfo["DisplayName"] as? String ?? ""
        }
        
        self.coverInputEmail.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputEmail.layer.borderWidth = 0.5
        self.coverInputEmail.layer.cornerRadius = 3.0
        self.coverInputEmail.layer.masksToBounds = true
        
        self.coverInputName.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputName.layer.borderWidth = 0.5
        self.coverInputName.layer.cornerRadius = 3.0
        self.coverInputName.layer.masksToBounds = true
        
        self.coverInputBought.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputBought.layer.borderWidth = 0.5
        self.coverInputBought.layer.cornerRadius = 3.0
        self.coverInputBought.layer.masksToBounds = true
        
        self.coverInputNameOfSale.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputNameOfSale.layer.borderWidth = 0.5
        self.coverInputNameOfSale.layer.cornerRadius = 3.0
        self.coverInputNameOfSale.layer.masksToBounds = true
        
        self.coverInputAdditional.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverInputAdditional.layer.borderWidth = 0.5
        self.coverInputAdditional.layer.cornerRadius = 3.0
        self.coverInputAdditional.layer.masksToBounds = true
    }

    private func dismissKeyboard() {
        tfNameOfSale.resignFirstResponder()
        tfBoughtFrom.resignFirstResponder()
        tfYourName.resignFirstResponder()
        tfEmail.resignFirstResponder()
        tvAdditional.resignFirstResponder()
    }
    
    private func validateAllField(_ pText: String = "", _ withoutField: UIView? = nil) -> Bool {
        var isValid = true
        
        let isValidEmail = self.validateEmail(pText, withoutField)
        
        if !isValidEmail {
            isValid = false
        }
        
        self.btnSendReport.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSendReport.isUserInteractionEnabled = isValid
        
        if isValid {
            self.btnSendReport.layer.applySketchShadow()
        } else {
            self.btnSendReport.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    
    private func validateEmail(_ pText: String = "", _ withoutField: UIView? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
        
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverWarningView.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.statusEmail.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverInputEmail.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
        
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverInputEmail.layer.borderColor =  INPUT_COLOR.NORMAL.color.cgColor
        self.coverWarningView.isHidden  = true
        self.statusEmail.isHidden  = true
    }
    
    private func showReportSuccess() {
    
        let content = NoticeCompleteRegistrationView().loadReportSuccessView()
        
        content.config(nil, onClickedCompleteCallback: { [weak self] (data) in
            guard let self = self else {return}
            
                Utils.closeCustomAlert()
            
                if let discoveryIns = GLOBAL.GlobalVariables.discoveryInstance {
                    self.navigationController?.popToViewController(discoveryIns, animated: true)
                } else {
                    let _discoveryIns = DiscoveryMainController()
                    self.navigationController?.pushViewController(_discoveryIns, animated: true)
                }
            }, onClickedCloseCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
                
        })
        
        Utils.showAlertWithCustomView(content)
    }
    
    @IBAction func onClickedSendReport(_ sender: Any) {
        self.dismissKeyboard()

        Utils.showLoading()
        
        let params = [
            "event_type": REPORT_TYPE.ISSUE.rawValue,
            "screen": "onboarding_member",
            "value": scratchCode,
            "email": self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "full_name": tfYourName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "object": tfNameOfSale.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "reason": tfBoughtFrom.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "message": tvAdditional.text.trimmingCharacters(in: .whitespacesAndNewlines),
            "current_lat" : GLOBAL.GlobalVariables.latestLat,
            "current_lng": GLOBAL.GlobalVariables.latestLng
            ]  as [String : Any]
        
        APICommonServices.sendReport(params) { (resp) in
            mPrint("sendReport --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                if status {
                    setTimeout({
                        Utils.dismissLoading()
                        self.showReportSuccess()
                    }, 1000)
                } else {
                    Utils.dismissLoading()
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            } else {
                 Utils.dismissLoading()
            }
            
           
        }
    }
}

extension ReportIssueViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfEmail {
            tfYourName.becomeFirstResponder()
        } else if textField == tfYourName {
            tfBoughtFrom.becomeFirstResponder()
        } else if textField == tfBoughtFrom {
            tfNameOfSale.becomeFirstResponder()
        } else if textField == tfNameOfSale {
            tvAdditional.becomeFirstResponder()
        }

        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateAllField(updatedText, textField)

        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            }
        }, 100)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateAllField()
    }
}


extension ReportIssueViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        _ = validateAllField()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let tText = textView.text, let textRange = Range(range, in: tText) {
            let updatedText = tText.replacingCharacters(in: textRange, with: text)
            
            _ = self.validateAllField(updatedText, textView)
            
        }
        
        return true
    }
}
