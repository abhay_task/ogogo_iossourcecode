//
//  NewUpgradeViewController.swift
//  TGCMember
//
//  Created by vang on 1/7/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class NewUpgradeViewController: BaseViewController {
    func updateLanguages() {
        self.lblTItleScreen?.text = LOCALIZED.choose_your_plan.translate
    }
    
    private let topHeader: CGFloat = Utils.isIPhoneNotch() ? 40 : 20
    private let bottomFooter: CGFloat = Utils.isIPhoneNotch() ? 20 : 20
    private let paddingTopCard: CGFloat = 20.0
    
    private var heightCardUpgrade: CGFloat = 0.0
    
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCarouselConstraint: NSLayoutConstraint!
    @IBOutlet weak var coverCarousel: UIView!
    @IBOutlet weak var coverBack: HighlightableView!
    @IBOutlet weak var heightFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomCarouselConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lblTItleScreen: UILabel!
    @IBOutlet weak var contentFooter: UIView!
    private var isNeedPayViaWeb: Bool = false
    private var selectedPlan: [String: Any?]?
    
    var initPlanIndex: Int = 0
    private var selectedPlanIndex: Int = 0
    private var currentPlanIndex: Int = 0
    private var currentPlanType: UPGRADE_PLAN = .BASIC
    var fromCarousel: Bool = false
    
    var backToDealDetail: Bool = false
    
    private var items:[[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        decorate()
        NotificationCenter.default.addObserver(self, selector: #selector(updatedSubscription(_:)), name: .UPDATED_SUBSCRIPTION, object: nil)
        
        self.getPackages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @objc func updatedSubscription(_ notification: Notification) {
          
        if let uData = notification.object as? [String: Any] {
            let tType = UPGRADE_PLAN(rawValue: (uData["Type"] as? String ?? "")) ?? .FREE
            self.initPlanIndex = tType.viewIndex
            
            self.getPackages(false)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
        
        let remainSpace = keyboardSize.height - (bottomFooter + 40 + paddingTopCard)
        
        self.topCarouselConstraint.constant = -remainSpace
        self.bottomCarouselConstraint.constant = remainSpace
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.topCarouselConstraint.constant = 0
        self.bottomCarouselConstraint.constant = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UPDATED_SUBSCRIPTION, object: nil)
    }
    
    @IBAction func onClickedBg(_ sender: Any) {
        Utils.dismissKeyboard()
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.handleGoBack()
    }
    
    private func handleGoBack() {
        self.coverCarousel.alpha = 0
        
        if backToDealDetail {
            self.navigationController?.popViewController(animated: true)
        } else {
            if let discoveryVC = GLOBAL.GlobalVariables.discoveryInstance {
                self.navigationController?.popToViewController(discoveryVC, animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }
    
    private func addGestureToCarousel() {
        for subview in self.carousel.subviews {
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTouchCarousel(_:)))
            subview.addGestureRecognizer(tap)   
        }
        
    }
    
    @objc func handleTouchCarousel(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        Utils.dismissKeyboard()
    }
    
    private func decorate() {
        carousel.type = .coverFlow
        carousel.bounces = false
        carousel.perspective = -1/2000
        
        topHeaderConstraint.constant = topHeader
        bottomFooterConstraint?.constant = bottomFooter
        
        heightCardUpgrade = SCREEN_HEIGHT - (topHeader + 50 + bottomFooter + 50) - paddingTopCard * 2
        
//        coverBack?.layer.cornerRadius = 20.0
//        coverBack?.layer.applySketchShadow(color: UIColor("rgb 20 121 147"), alpha: 0.3, x: 3, y: 4, blur: 20, spread: 0)
        
    }
    
    private func renderDot() {
        self.contentFooter.removeSubviews()
        let spaceDot: CGFloat = 18
        let hieghtItem: CGFloat = 10
        
        let coverDot = UIView()
        coverDot.backgroundColor = .clear
        self.contentFooter.addSubview(coverDot)
        
        coverDot.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.contentFooter)
            make.centerY.equalTo(self.contentFooter)
            make.height.equalTo(30)
        }
        
        var lastDot: UIView? = nil
        
        for index in 0..<self.items.count {
            let tDot = UIView()
                        
            tDot.backgroundColor = self.selectedPlanIndex == index ?  .white : .clear
            tDot.layer.cornerRadius = hieghtItem/2
            
            if self.selectedPlanIndex != index {
                tDot.layer.borderWidth = 1.0
                tDot.layer.borderColor = UIColor.white.cgColor
            }
            
            coverDot.addSubview(tDot)
            
            if index == self.items.count - 1 {
                if lastDot == nil {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalToSuperview()
                        make.right.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                } else {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalTo(lastDot!.snp.right).offset(spaceDot)
                        make.right.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                }
            } else {
                if lastDot == nil {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalToSuperview()
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                } else {
                    tDot.snp.makeConstraints { (make) in
                        make.left.equalTo(lastDot!.snp.right).offset(spaceDot)
                        make.centerY.equalToSuperview()
                        make.width.equalTo(hieghtItem)
                        make.height.equalTo(hieghtItem)
                    }
                }
            }
            
            lastDot = tDot
        }
    }
    
    private func handleUpgradeCard(_ data: [String: Any?]?) {
        if let tData = data {
            
            let tType = tData["Type"] as? String ?? UPGRADE_PLAN.BASIC.rawValue
            let tName = (tData["Name"] as? String ?? "").upperCaseFirstLetter()
            
            if tType == UPGRADE_PLAN.BASIC.rawValue {
                var message = LOCALIZED.are_you_sure_you_want_to_downgrade_upgrade_member_format.translate
                message = message.replacingOccurrences(of: "[%__TAG_DOWNGRADE_UPGRADE__%]", with: LOCALIZED.txt_downgrade_title.translate.lowercased())
                message = message.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tName)
                
                Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { [weak self] (_) in
                    guard let self = self else {return}
                    
                    self.handleCancelSubscription()
                    }, cancelCallback: nil)
            } else { // to Silver/Gold
                let paymentVC = ConfirmPaymentViewController()
                paymentVC.data = tData
                
                var isUpgrade: Bool = true
                
                let cardIdx = tData["currentIndex"] as? Int ?? -1
                
                if cardIdx < self.currentPlanIndex {
                    isUpgrade = false
                    paymentVC.subscriptionType = .DOWNGRADE
                } else {
                    paymentVC.subscriptionType = .UPGRADE
                }
                
                if self.isNeedPayViaWeb {
                    self.navigationController?.pushViewController(paymentVC, animated: true)
                } else {
                    var message = LOCALIZED.are_you_sure_you_want_to_downgrade_upgrade_member_format.translate.replacingOccurrences(of: "[%__TAG_DOWNGRADE_UPGRADE__%]", with: paymentVC.subscriptionType.translate)
                    message = message.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tName)
                    let packagId = tData["ID"] as? String ?? ""
                    
                    Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { [weak self] (_) in
                        guard let self = self else {return}
                        
                        self.handleChangePlan(packagId, isUpgrade)
                        }, cancelCallback: nil)
                }
                
            }
        }
    }
    
    
    private func showSuccessChangeSubscription() {
        self.initPlanIndex = self.selectedPlanIndex
        
        let content = ResultAlertView().loadSuccessView()
        content.config(nil) { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            
            Utils.getMyProfile { (success) in
                NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                NotificationCenter.default.post(name: .REFRESH_LIST_CAROUSEL, object: nil)
                
                self.getPackages()
            }
        }
        
        Utils.showAlertWithCustomView(content)
    }
    
    private func handleCancelSubscription() {
        Utils.showLoading()
        
        APICommonServices.cancelSubscription() { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleCancelSubscription", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.showSuccessChangeSubscription()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func handleChangePlan(_ packageId: String, _ isUpgrade: Bool = true) {
        Utils.showLoading()
        
        APICommonServices.changePlan(packageId, isUpgrade) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleChangePlan", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.showSuccessChangeSubscription()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func getPackages(_ showLoading: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        APICommonServices.getAvailablePackages { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("getAvailablePackages", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], status == true  {
                    
                    self.isNeedPayViaWeb = data["IsNeedPayViaWeb"] as? Bool ?? false
                    
                    let packages = self.reBuildData(data)
                    
                    self.currentPlanIndex = self.getCurrentPlanIndex(packages)
                    self.buildCardUpgrade(packages)
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    
    private func getCurrentPlanIndex(_ packages: [[String: Any?]]) -> Int {
        var tIndex = 0
        
        for index in 0..<packages.count {
            let package = packages[index]
            
            let status = package["IsCurrentPlan"] as? Bool ?? false
            
            if status {
                tIndex = index
                break
            }
        }
        
        return tIndex
        
    }
    
    private func reBuildData(_ data: [String: Any?]) -> [[String: Any?]] {
        var totalPackages:[[String: Any?]] = []
        
        if var freePackage = data["free-member"] as? [String : Any?] {
            freePackage["Type"] = UPGRADE_PLAN.FREE.rawValue
            
            totalPackages.append(freePackage)
        }
        
        
        let packages = data["packages"] as? [[String: Any?]] ?? []
        
        for package in packages {
            var cPackage = package
            
            let type = cPackage["Type"] as? String ?? ""
            
            if type == "" {
                cPackage["Type"] = UPGRADE_PLAN.BASIC.rawValue
            }
            
            totalPackages.append(cPackage)
            
        }
        
        return totalPackages
    }
    
    private func buildCardUpgrade(_ plans: [[String: Any?]] = []) {
        self.items = plans
        self.renderDot()
        
        
        self.carousel.reloadData()
        
        self.carousel.scrollToItem(at: self.initPlanIndex, animated: false)
        
        addGestureToCarousel()
    }
}


extension NewUpgradeViewController: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let cardData = self.items[index]
        let typeUpgrade = cardData["Type"] as? String ?? UPGRADE_PLAN.FREE.rawValue
        let tRectCard = CGRect(x: 0, y: 0, width: SCREEN_WIDTH - 88, height: heightCardUpgrade)
        
        var cardUpgrade: NewCardUpgrade = FreeUpgrade().loadView()
        
        if typeUpgrade == UPGRADE_PLAN.BASIC.rawValue {
            cardUpgrade = BasicUpgrade().loadView()
        } else if typeUpgrade == UPGRADE_PLAN.SILVER.rawValue {
            cardUpgrade = SilverUpgrade().loadView()
            
        } else if typeUpgrade == UPGRADE_PLAN.GOLD.rawValue {
            cardUpgrade = GoldUpgrade().loadView()
        }
        
        cardUpgrade.frame = tRectCard
        cardUpgrade.currentIndex = index
        cardUpgrade.currentPlanIndex = self.currentPlanIndex
        
        cardUpgrade.config(cardData, onClickedUpgradeCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            if let tData = data, let type = tData["Type"] as? String {
                if type == UPGRADE_PLAN.FREE.rawValue {
                    self.carousel.scrollToItem(at: UPGRADE_PLAN.FREE.viewIndex + 1, animated: true)
                } else {
                    self.handleUpgradeCard(data)
                }
            }
            
            }, onActiveSuccessCallback: { [weak self] (success) in
                guard let self = self else {return}
                
                if success {
                    NotificationCenter.default.post(name: .REFRESH_DEAL_DETAIL, object: nil)
                    
                    if Utils.isRequireSignUpToGetDeal() {
                        let quickSignUp = QuickSignUpViewController()
                        quickSignUp.backToDealDetail = self.backToDealDetail
                        
                        self.navigationController?.pushViewController(quickSignUp, animated: true)
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                
        })
        
        return cardUpgrade
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        if option == .spacing {
            return 1.1
        } else if option == .tilt {
            return 0
        } 
        
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        Utils.dismissKeyboard()
        self.selectedPlanIndex = carousel.currentItemIndex
        
        self.renderDot()
    }
    
}

