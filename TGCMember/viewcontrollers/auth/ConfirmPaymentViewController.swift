//
//  ConfirmPaymentViewController.swift
//  TGCMember
//
//  Created by vang on 7/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

public enum SUBSCRIPTION_TYPE: String {
    case UPGRADE = "upgrade"
    case DOWNGRADE = "downgrade"
    case CANCEL = "cancel"
    
    var translate : String {
        switch self {
        case .UPGRADE : return LOCALIZED.txt_upgrade_title.translate.lowercased()
        case .DOWNGRADE : return LOCALIZED.txt_downgrade_title.translate.lowercased()
        case .CANCEL : return LOCALIZED.txt_cancel_title.translate.lowercased()
        }
    }
}

class ConfirmPaymentViewController: BaseViewController {
    var data: [String: Any?]?
    
    @IBOutlet weak var coverCardView: UIView!
    @IBOutlet weak var coverMethodsView: UIView!
    @IBOutlet weak var heightCardConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightMethodsConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblPrompt: UILabel!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint!
    
    private var paymentMethods: [[String: Any?]] = []
    private var selectedMethod: [String: Any?]?
    private var selectedCardPayment: [String: Any?]?
    
    private var paymentTransaction: [String: Any?]?
    private var isShowAlert: Bool = false
    
    var subscriptionType: SUBSCRIPTION_TYPE = .UPGRADE
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
   
        
        decorate()
        checkEnableConfirm()
        getPaymentMethods()
    }

    private func decorate() {
        topHeaderConstraint.constant = Utils.isIPhoneNotch() ? 30 : 10
        bottomFooterConstraint.constant = defaultBottom
        
        btnConfirm.backgroundColor = UIColor(hexFromString: "#FFC106")
        btnConfirm.clipsToBounds = false
        btnConfirm.layer.cornerRadius = 3.0

        btnConfirm.layer.applySketchShadow()
        
        btnCancel.setTitleUnderline(LOCALIZED.txt_cancel_title.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 12.0))
        
        if let tData = self.data {
            let tType = tData["Type"] as? String ?? UPGRADE_PLAN.BASIC.rawValue
            
            var fText = LOCALIZED.you_have_joined_ogogo.translate
            fText = fText.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tType)
        }
 
        self.heightCardConstraint.constant = 0
    }
    
    private func showSuccessTransaction() {
        if !self.isShowAlert {
            let content = ResultAlertView().loadSuccessView()
            content.config(nil) { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
                
                self.isShowAlert = false
                self.navigationController?.popViewController(animated: true)
            }
            
            Utils.showAlertWithCustomView(content)
        }
        
        self.isShowAlert = true
    }
    
    private func checkEnableConfirm() {
        self.btnConfirm.backgroundColor = self.selectedMethod != nil ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnConfirm.isUserInteractionEnabled = self.selectedMethod != nil
        
        if self.selectedMethod != nil {
            self.btnConfirm.layer.applySketchShadow()
        } else {
            self.btnConfirm.layer.removeSketchShadow()
        }
        
    }
    
    private func updatePaymentTransaction() {
        NotificationCenter.default.post(name: .UPDATED_SUBSCRIPTION, object: self.data)
        setTimeout( { [weak self]  in
            guard let self = self else {return}
            
            self.showSuccessTransaction()
        }, 200)
        
    }
    
    @IBAction func onClickedConfirm(_ sender: Any) {
        self.generateTransaction { [weak self] (success) in
            guard let self = self else {return}
            
            if success {
                self.confirmPayementTransaction(false)
            } else {
                Utils.dismissLoading()
            }
        }
        
    }
    
    private func handlePayWithSavedCard() {
        if let tPayementTransaction = self.paymentTransaction, let config = tPayementTransaction["Config"] as? [String: Any?], let purchase = tPayementTransaction["PurchaseItem"] as? [String: Any?] {
        
            let fullName = GLOBAL.GlobalVariables.userInfo!["DisplayName"] as? String ?? ""
            
            var cardId = ""
            if let sCard = self.selectedCardPayment {
                cardId = sCard["ID"] as? String ?? ""
            }
            
            let params = [
                "payment_kind": KIND_PAYMENT_METHOD.SUBSCRIPTION.rawValue,
                "tgc_transaction_id": config["TransactionID"] as? String ?? "",
                "tgc_item_id": config["ItemID"] as? String ?? "",
                "item_id": purchase["ID"] as? String ?? "",
                "item_name": purchase["Name"] as? String ?? "",
                "total_amount": purchase["Amount"] as? Int ?? 0,
                "full_name": fullName,
                "gateway_id": tPayementTransaction["ID"] as? String ?? "",
                "currency_symbol": config["CurrencySymbol"] as? String ?? "",
                "card_id": cardId
            ] as [String : Any]
        
            APICommonServices.payWithCard(params) { [weak self] (resp) in
                guard let self = self else {return}

                mPrint("payWithCard", resp)
                
                if let resp = resp, let status = resp["status"] as? Bool {
                    if status == true  {
                        self.submitFinishTransaction()
                    } else {
                        Utils.showAlert(self, "", resp["message"] as? String ?? "")
                    }
                }
                
                Utils.dismissLoading()
            }
        } else {
            Utils.dismissLoading()
        }

    
    }
    
    private func generateTransaction(_ callback: SuccessedCallback?) {
        if let sMethod = self.selectedMethod {
            Utils.showLoading()
            
            let method = sMethod["Key"] as? String ?? ""
            
            var packageId = ""
            
            if let tData = self.data {
                packageId = tData["ID"] as? String ?? ""
            }
            
            APICommonServices.generateTransaction(packageId, paymemtMethod: method) { [weak self] (resp) in
                guard let self = self else {return}
                
                mPrint("generateTransaction", (resp))
                var isSuccess = false
                
                if let resp = resp, let status = resp["status"] as? Bool {
                    
                    if let data = resp["data"] as? [String: Any?], status == true  {
                        isSuccess = true
                        
                        self.paymentTransaction = data["paymentTransaction"] as? [String: Any?]
                    } else {
                        Utils.showAlert(self, "", resp["message"] as? String ?? "")
                    }
                }
                
                if let tCallback = callback {
                    tCallback(isSuccess)
                } else {
                    Utils.dismissLoading()
                }
            }
        }
    }
    
    private func confirmPayementTransaction(_ showLoading: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        var transactionId = ""
        if let tPayementTransaction = self.paymentTransaction, let config = tPayementTransaction["Config"] as? [String: Any?] {
            transactionId = config["TransactionID"] as? String ?? ""
        }
        
        APICommonServices.confirmTransaction(transactionId) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("confirmTransaction", (resp))
           
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    if let _ = self.selectedCardPayment {
                        self.handlePayWithSavedCard()
                    } else { // Open webview
                        Utils.dismissLoading()
                        self.openWebPayment()
                    }
                    
                } else {
                    Utils.dismissLoading()
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            } else {
                Utils.dismissLoading()
            }
            
        }
    }
    
    
    private func openWebPayment() {
        let webPaymentVC = WebPaymentViewController()
        var paymentMethod = ""
        var packageId = ""
        
        if var pData = self.paymentTransaction {
            if let tData = self.data {
               
                packageId = tData["ID"] as? String ?? ""
                
                if let sMethod = self.selectedMethod {
                    paymentMethod = sMethod["Key"] as? String ?? ""
                    
                }
            }
            
            pData["payment_method"] = paymentMethod
            pData["package_id"] = packageId
            webPaymentVC.data = pData
        }

        webPaymentVC.successCallback = { [weak self] (success) in
            guard let self = self else {return}
            
            self.updatePaymentTransaction()
        }
        
        self.addSubviewAsPresent(webPaymentVC)
    }
    
    private func submitFinishTransaction() {
        if let tData = self.data {
            Utils.showLoading()
            
            var paymentMethod = ""
            var transactionId = ""
            let packageId = tData["ID"] as? String ?? ""
            
            if let sMethod = self.selectedMethod {
                paymentMethod = sMethod["Key"] as? String ?? ""
            }
            
            if let tPayementTransaction = self.paymentTransaction, let config = tPayementTransaction["Config"] as? [String: Any?] {
                transactionId = config["TransactionID"] as? String ?? ""
            }
            
            let params = [
                            "package_id": packageId,
                            "transaction_id": transactionId,
                            "payment_method": paymentMethod
                         ]
            
            if subscriptionType == .UPGRADE {
                self.handleUpgrade(params)
            } else {
                self.handleUpgrade(params)
                //self.handleDowngrade(params)
            }
            
        }
    }
    
    private func handleUpgrade(_ params: [String: Any]) {
        Utils.showLoading()
        
        APICommonServices.upgradeSubscription(params) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleUpgrade", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.updatePaymentTransaction()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func handleDowngrade(_ params: [String: Any]) {
        Utils.showLoading()
        
        APICommonServices.downgradeSubscription(params) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleDowngrade", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.updatePaymentTransaction()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    
    @IBAction func onClickedCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func onSelectedPaymentMethod(_ method: [String: Any?]) {
        self.selectedMethod = method
        self.selectedCardPayment = nil
        
        self.checkEnableConfirm()
        self.buildPaymentMethodsView()
    }
    
    private func handleClickedCard(_ data: [String: Any?]?) {
        NotificationCenter.default.post(name: .SELECTED_CARD_PAYMENT, object: data)
        
        if let tData = data, let isSelected = tData["isSelected"] as? Bool, isSelected == true {
            self.selectedCardPayment = tData
        } else {
            self.selectedCardPayment = nil
        }
    }
    
    
    private func buildPaymentCardsView() {
        for subview in self.coverCardView.subviews {
            subview.removeFromSuperview()
        }
        
        if let sMethod = self.selectedMethod, let listCards = sMethod["Cards"] as? [[String: Any?]] {
            let contentView = UIView()
            contentView.backgroundColor = .clear
            
            let lblTitle = UILabel()
            lblTitle.text = LOCALIZED.txt_card_title.translate
            lblTitle.textColor = UIColor(30, 38, 71)
            lblTitle.font = UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 14)
            
            contentView.addSubview(lblTitle)
            
            lblTitle.snp.makeConstraints { (make) in
                make.top.equalTo(contentView)
                make.left.equalTo(contentView)
            }
            
            
            let heightTitle: CGFloat = 30.0
            let heightItem: CGFloat = 60.0
            let widthItem: CGFloat = UIScreen.main.bounds.width - 80.0
            
            var spaceItem: CGFloat = 20.0
            var topConstraint: CGFloat = heightTitle
            
            for index in 0..<listCards.count {
                let info = listCards[index] as [String: Any?]
                
                let tCard = PaymentCardView().loadView()
                tCard.config(info, onClickedCardCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.handleClickedCard(data)
                })
                
                contentView.addSubview(tCard)
                
                tCard.snp.makeConstraints { (make) in
                    make.top.equalTo(contentView).offset(topConstraint)
                    make.left.equalTo(contentView)
                    make.height.equalTo(heightItem)
                    make.width.equalTo(widthItem)
                }
                
                topConstraint += (heightItem + spaceItem)
                
            }
            
            let heightCover = CGFloat(listCards.count)*heightItem + CGFloat(listCards.count - 1)*spaceItem + heightTitle
            self.heightCardConstraint.constant = heightCover
            
            
            self.coverCardView.addSubview(contentView)
            
            contentView.snp.makeConstraints { (make) in
                make.top.equalTo(coverCardView)
                make.left.equalTo(coverCardView)
                make.right.equalTo(coverCardView)
                make.bottom.equalTo(coverCardView)
            }
        } else {
            self.heightCardConstraint.constant = 0.0
        }
    }
    
    private func buildPaymentMethodsView() {
        
        for subview in self.coverMethodsView.subviews {
            subview.removeFromSuperview()
        }
    
        let contentView = UIView()
        contentView.backgroundColor = .clear
        
        let lblTitle = UILabel()
        lblTitle.text = LOCALIZED.txt_payment_methods.translate
        lblTitle.textColor = UIColor(30, 38, 71)
        lblTitle.font = UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 14)
        
        contentView.addSubview(lblTitle)
        
        lblTitle.snp.makeConstraints { (make) in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView)
        }
        
        
        let heightTitle: CGFloat = 30
        let heightItem: CGFloat = 30
        
        var topConstraint: CGFloat = heightTitle
        
        for index in 0..<self.paymentMethods.count {
            
            let info = self.paymentMethods[index] as [String: Any?]
            
            let tNode = self.buildItemMethod(info)
            
            contentView.addSubview(tNode)
            
            tNode.snp.makeConstraints { (make) in
                make.top.equalTo(contentView).offset(topConstraint)
                make.left.equalTo(contentView)
                make.height.equalTo(heightItem)
            }
            
            topConstraint += heightItem
            
        }
        
        let heightCover = CGFloat(self.paymentMethods.count)*heightItem + heightTitle
        self.heightMethodsConstraint.constant = heightCover
        
       
        self.coverMethodsView.addSubview(contentView)
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(coverMethodsView)
            make.left.equalTo(coverMethodsView)
            make.right.equalTo(coverMethodsView)
            make.bottom.equalTo(coverMethodsView)
        }
        
        self.buildPaymentCardsView()
    }
    
    private func buildItemMethod(_ data:[String: Any?]? = nil) -> UIView {
        
        if let tData = data {
            let enable = tData["Enable"] as? Bool ?? false
            
            var selected = false
            
            if self.selectedMethod != nil {
                let tKey = tData["Key"] as? String
                let uKey = self.selectedMethod!["Key"] as? String
                
                if tKey == uKey {
                     selected = true
                }
            }
        
            let cover = HighlightableView()
            cover.isUserInteractionEnabled = enable
            
            let dot = UIImageView()
            dot.image = selected ? UIImage(named: "check_circle_yellow") : UIImage(named: "check_circle_empty")
          
            cover.addSubview(dot)
            
            let lbName = UILabel()
            
            lbName.text = tData["Name"] as? String ?? ""
            lbName.textColor = enable ? UIColor(30, 38, 71) : UIColor(203, 203, 203)
            lbName.font = UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14)
           
            cover.addSubview(lbName)
            
            dot.snp.makeConstraints { (make) in
                make.centerY.equalTo(cover)
                make.left.equalTo(cover)
                make.width.equalTo(20.0)
                make.height.equalTo(20.0)
            }
            
            lbName.snp.makeConstraints { (make) in
                make.centerY.equalTo(cover)
                make.left.equalTo(cover).offset(35)
                make.right.equalTo(cover)
            }
            
            let button = UIButton()
            button.actionHandler(controlEvents: .touchUpInside) {
                self.onSelectedPaymentMethod(tData)
            }
            
            cover.addSubview(button)
            button.snp.makeConstraints { (make) in
                make.top.equalTo(cover)
                make.left.equalTo(cover)
                make.right.equalTo(cover)
                make.bottom.equalTo(cover)
            }
            
            return cover
        }
        
        
        return UIView()
    }
    
    
    private func getPaymentMethods() {
        Utils.showLoading()
        
        var packageId = ""
        
        if let tData = self.data {
            packageId = tData["ID"] as? String ?? ""
        }
        
        APICommonServices.getPaymentMethods(packageId) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("getPaymentMethods", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], status == true  {
                    var methods = data["paymentOptions"]  as? [[String: Any?]] ?? []
                    
                    self.paymentMethods = methods
                    self.buildPaymentMethodsView()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }


}

extension ConfirmPaymentViewController {
    func updateLanguages() {
        
    }
}
