//
//  NewWelcomeViewController.swift
//  TGCMember
//
//  Created by vang on 3/6/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit
import Nantes

class NewWelcomeViewController: BaseViewController {
    func updateLanguages() {
        lblCodeSignUp?.text = LOCALIZED.enter_code_sign_up.translate
        lblSignIn?.text = LOCALIZED.txt_sign_in.translate
    }
    
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var coverBtnCodeSignUp: HighlightableView!
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var lblCodeSignUp: UILabel!
    @IBOutlet weak var coverBtnSignIn: HighlightableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        decorate()
        
        setupFooterLabel()
    }
    
    
    private func decorate() {
        self.footerView?.backgroundColor = .clear
        coverBtnSignIn?.backgroundColor = UIColor("rgb 255 193 6")
        coverBtnSignIn?.layer.cornerRadius = 5.0
        coverBtnSignIn?.layer.masksToBounds = true
        
        coverBtnCodeSignUp?.backgroundColor = .clear
        coverBtnCodeSignUp?.layer.cornerRadius = 5.0
        coverBtnCodeSignUp?.layer.borderWidth = 1.0
        coverBtnCodeSignUp?.layer.borderColor = UIColor("rgb 255 193 6").cgColor
        coverBtnCodeSignUp?.layer.masksToBounds = true
    }
    
    private func setupFooterLabel() {
        self.footerView.removeSubviews()
        
        var fullText = LOCALIZED.click_here_to_view.translate
        fullText = fullText.replacingOccurrences(of: "[%__TAG_CLICK_HERE__%]", with: LOCALIZED.click_here.translate)
        fullText = fullText.replacingOccurrences(of: "[%__TAG_FREE_MEMBER__%]", with: LOCALIZED.free_member.translate)
                
        let labelTerm: NantesLabel = .init(frame: .zero)
        labelTerm.delegate = self
        labelTerm.numberOfLines = 0
        labelTerm.textAlignment = .center

        var mMutableString = NSMutableAttributedString()
        
        mMutableString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ])
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        mMutableString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, mMutableString.length))
        
        if let tRange = fullText.range(of: LOCALIZED.free_member.translate) {
            let nsRange = fullText.nsRange(from: tRange)

            mMutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: ROBOTO_FONT.BOLD.rawValue, size: 14) ?? UIFont.systemFont(ofSize: 14), range:nsRange)
        }
        
        labelTerm.attributedText = mMutableString
        
        labelTerm.linkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor("rgb 255 193 6"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 18.0) ?? UIFont.systemFont(ofSize: 18),
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        
        labelTerm.activeLinkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor("rgba 255 193 6 0.5"),
            NSAttributedString.Key.font: UIFont(name: ROBOTO_FONT.MEDIUM.rawValue, size: 18.0) ?? UIFont.systemFont(ofSize: 18),
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        
        labelTerm.addLink(to: URL(string: "https://clickhere.com")!, withRange: (fullText as NSString).range(of: LOCALIZED.click_here.translate))
        
        self.footerView.addSubview(labelTerm)
        
        labelTerm.snp.makeConstraints { (make) in
            make.left.equalTo(self.footerView).offset(5)
            make.centerY.equalTo(self.footerView)
            make.right.equalTo(self.footerView).offset(-5)
        }
        
    }
    
    @IBAction func onClickedSignIn(_ sender: Any) {
        Utils.setIsClickedOnWelcome()
        
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    @IBAction func onClickedEnterCode(_ sender: Any) {
        Utils.setIsClickedOnWelcome()
        
        let upgradeVC = NewUpgradeViewController()
        
        if let _upgradeInfo = GLOBAL.GlobalVariables.upgradePackageModel {
            upgradeVC.initPlanIndex = _upgradeInfo.canUpToNextStep.viewIndex
        }
        
        self.navigationController?.pushViewController(upgradeVC, animated: true)
    }
}

extension NewWelcomeViewController: NantesLabelDelegate {
    func attributedLabel(_ label: NantesLabel, didSelectLink link: URL) {
        Utils.setIsClickedOnWelcome()
        self.navigationController?.pushViewController(DiscoveryMainController(), animated: true)
    }
}
