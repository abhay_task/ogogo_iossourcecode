//
//  CountryListViewController.swift
//  TGCMember
//
//  Created by vang on 6/25/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CountryListViewController: BaseViewController {
  

    @IBOutlet weak var lblChooseCountry: UILabel!
    @IBOutlet weak var topTitleConstraint: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var coverSearchView: UIView!
    @IBOutlet weak var coverCountryView: UIView!
    @IBOutlet weak var tbCountry: UITableView!
    
    var selectedCountryCallback: DataCallback?
    var selectedCountry: CountryModel = CountryModel()
    private var countries: [[String: Any?]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tbCountry.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        
        decorate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (GLOBAL.GlobalVariables.countriesSystem.count == 0) {
            self.getCountries();
        } else {
            self.countries = GLOBAL.GlobalVariables.countriesSystem
            self.tbCountry.reloadData()
        }
    }
    
    private func dismissKeyboard() {
        self.tfSearch.resignFirstResponder()
    }
    
    private func filterCountry(_ keyword: String = "") -> [[String: Any?]] {
        if keyword == "" {
            return GLOBAL.GlobalVariables.countriesSystem
        }
        
        let tCountries = GLOBAL.GlobalVariables.countriesSystem.filter { (item) -> Bool in
            if let name = item["name"] as? String {
                if name.lowercased().contains(keyword.lowercased()) {
                    return true
                }
                
                return false
            }
            
            return false
        }
        
        return tCountries
    }
    
    @IBAction func onClickedClose(_ sender: Any) {
        self.dismissFromSuperview()
    }
    
    private func getCountries() {
        Utils.showLoading()
        
        APICommonServices.getCountryList() { (resp) in
           
            if let resp = resp, let status = resp["status"] as? Bool, status == true  {
                if let data = resp["data"] as? [[String: Any?]] {
                    GLOBAL.GlobalVariables.countriesSystem = data
                   
                    self.countries = data
                    self.tbCountry.reloadData()
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func decorate() {
        self.topTitleConstraint.constant = Utils.isIPhoneNotch() ? 50 : 40
        self.coverCountryView.layer.cornerRadius = 8.0
        self.coverCountryView.layer.masksToBounds = true
    }
    
}

extension CountryListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var data = self.countries[indexPath.row]
        data["selectedCountryCode"] = self.selectedCountry.countryCode ?? ""
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell") as? CountryCell
        cell?.config(data, onClickedCellCallback: {  [weak self] (data) in
            guard let self = self else {return}
            
            if let callback = self.selectedCountryCallback {
                callback(data)
                self.dismissKeyboard()
                self.dismissFromSuperview()
            }
            
        })
        
        return cell!
     
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
}

extension CountryListViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeyboard()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            self.countries = self.filterCountry(updatedText)
            
            self.tbCountry.reloadData()
        }
        
        return true
    }

}

extension CountryListViewController {
    func updateLanguages() {
        self.lblChooseCountry.text = LOCALIZED.txt_choose_country.translate.uppercased()
        self.tfSearch.placeholder = LOCALIZED.txt_search_country.translate
    }
    
}
