//
//  UpgradeViewController.swift
//  TGCMember
//
//  Created by vang on 7/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class UpgradeViewController: BaseViewController {

    @IBOutlet weak var scrollViewCard: UIScrollView!
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomFooterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightBenefitConstraint: NSLayoutConstraint!
    @IBOutlet weak var benefitView: UIView!
    @IBOutlet weak var btnFooter: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblTitleScreen: UILabel!
    @IBOutlet weak var lblTitleFooter: UILabel!
    @IBOutlet weak var lblUpgradeForMore: UILabel!
    
    private var selectedPlan: [String: Any?]?
    private var currentPlanIndex: Int = -1
    private var currentPlanType: UPGRADE_PLAN = .BASIC
    private var isNeedPayViaWeb: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(updatedSubscription(_:)), name: .UPDATED_SUBSCRIPTION, object: nil)
        
        decorate()
        
        self.getPackages()
    }
    
    
    @objc func updatedSubscription(_ notification: Notification) {
        self.getPackages(false)
    }
    
    @IBAction func onClickedBtnFooter(_ sender: Any) {
        let message = self.currentPlanType == .BASIC ? LOCALIZED.are_you_sure_you_want_to_deactivate_account.translate : LOCALIZED.are_you_sure_you_want_to_cancel_subscription.translate
        
        Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { [weak self] (_) in
            guard let self = self else {return}
            
            if self.currentPlanType == .BASIC {
                //self.handleDeactiveAccount()
            } else {
                self.handleCancelSubscription()
            }
            
        }, cancelCallback: nil)
    }
    

    @IBAction func onClickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func decorate() {
        topHeaderConstraint.constant = Utils.isIPhoneNotch() ? 30 : 10
        self.bottomFooterConstraint.constant = defaultBottom
        
       
    }
    
    private func getCurrentPlanIndex(_ packages: [[String: Any?]]) -> Int {
        var tIndex = -1
        
        for index in 0..<packages.count {
            let package = packages[index]
            
            let status = package["IsCurrentPlan"] as? Bool ?? false
            
            if status {
                let tType = package["Type"] as? String ?? ""
                self.currentPlanType = UPGRADE_PLAN(rawValue: tType) ?? .BASIC
                
                tIndex = index
                break
            }
        }
        
        return tIndex
        
    }
    
    private func getSelectedPlan(_ packages: [[String: Any?]]) {
        let cPlans = packages.filter { (item) -> Bool in
            let status = item["IsCurrentPlan"] as? Bool ?? false
            
            return status
        }
        
        if cPlans.count > 0 {
            self.footerView.isHidden = false
            
            self.selectedPlan = cPlans[0]
            
            self.updateFooterByType()
            
            self.renderBenefitView()
        } else {
            self.footerView.isHidden = true
        }
    }
    
    private func updateFooterByType() {
        if let tData = self.selectedPlan {
            self.btnFooter.isHidden = false
            
            let tType = tData["Type"] as? String ?? UPGRADE_PLAN.BASIC.rawValue
            
            if tType == UPGRADE_PLAN.BASIC.rawValue {
                self.btnFooter.setTitleUnderline(LOCALIZED.txt_deactivate_account.translate, color: UIColor(176, 176, 176) , font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 10) ?? UIFont.systemFont(ofSize: 10) )
            } else {
                self.btnFooter.setTitleUnderline(LOCALIZED.cancel_subscription.translate, color: UIColor(176, 176, 176), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 10) ?? UIFont.systemFont(ofSize: 10) )
            }
        }
    }
    
    
    private func getPackages(_ showLoading: Bool = true) {
        if showLoading {
            Utils.showLoading()
        }
        
        APICommonServices.getAvailablePackages { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("getAvailablePackages", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                
                if let data = resp["data"] as? [String: Any?], status == true  {
                    
                    self.isNeedPayViaWeb = data["IsNeedPayViaWeb"] as? Bool ?? false
                    
                    let packages = data["packages"] as? [[String: Any?]] ?? []
                    
                    self.currentPlanIndex = self.getCurrentPlanIndex(packages)
                    self.getSelectedPlan(packages)
                    self.buildCardUpgrade(packages)
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func renderBenefitView() {
        self.benefitView.backgroundColor = .clear
        self.benefitView.removeSubviews()
        
        if let tSelected = self.selectedPlan {
            let data = tSelected["Description"] as? [[String: Any?]] ?? []
            
            Utils.buildBenefitDescription(data) { [weak self] (info) in
                guard let self = self else {return}
                
                if let tInfo = info {
                    let contentView = tInfo["contentView"] as? UIView ?? UIView()
                    let contentHeight = tInfo["contentHeight"] as? CGFloat ?? 0.0
                    self.heightBenefitConstraint.constant = contentHeight
                    self.benefitView.addSubview(contentView)
                    
                    contentView.snp.makeConstraints({ (make) in
                        make.top.equalTo(self.benefitView)
                        make.left.equalTo(self.benefitView)
                        make.right.equalTo(self.benefitView)
                        make.bottom.equalTo(self.benefitView)
                    })
                }
            }
        } else {
             self.heightBenefitConstraint.constant = 10
        }
    }
    
    private func handleClickedCard(_ data: [String: Any?]?) {
        NotificationCenter.default.post(name: .SELECTED_CARD_UPGRADE, object: data)
        self.selectedPlan = data
        self.footerView.isHidden = false
        
        self.renderBenefitView()
    }
    
    private func handleUpgradeCard(_ data: [String: Any?]?) {
        if let tData = data {

            let tType = tData["Type"] as? String ?? UPGRADE_PLAN.BASIC.rawValue
            let tName = (tData["Name"] as? String ?? "").upperCaseFirstLetter()
            
            if tType == UPGRADE_PLAN.BASIC.rawValue {
                var message = LOCALIZED.are_you_sure_you_want_to_downgrade_upgrade_member_format.translate
                message = message.replacingOccurrences(of: "[%__TAG_DOWNGRADE_UPGRADE__%]", with: LOCALIZED.txt_downgrade_title.translate.lowercased())
                message = message.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tName)
                
                Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { [weak self] (_) in
                    guard let self = self else {return}
                    
                    self.handleCancelSubscription()
                }, cancelCallback: nil)
            } else { // to Silver/Gold
                let paymentVC = ConfirmPaymentViewController()
                paymentVC.data = tData
                
                var isUpgrade: Bool = true
                
                let cardIdx = tData["currentIndex"] as? Int ?? -1
                
                if cardIdx < self.currentPlanIndex {
                    isUpgrade = false
                    paymentVC.subscriptionType = .DOWNGRADE
                } else {
                    paymentVC.subscriptionType = .UPGRADE
                }
                
                if self.isNeedPayViaWeb {
                    self.navigationController?.pushViewController(paymentVC, animated: true)
                } else {
                    var message = LOCALIZED.are_you_sure_you_want_to_downgrade_upgrade_member_format.translate.replacingOccurrences(of: "[%__TAG_DOWNGRADE_UPGRADE__%]", with: paymentVC.subscriptionType.translate)
                    message = message.replacingOccurrences(of: "[%__TAG_NAME__%]", with: tName)
                    let packagId = tData["ID"] as? String ?? ""
                    
                    Utils.showConfirmAlert(self, "", message, LOCALIZED.txt_ok_title.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { [weak self] (_) in
                        guard let self = self else {return}
                        
                        self.handleChangePlan(packagId, isUpgrade)
                        }, cancelCallback: nil)
                }
                
            }
        }
    }
    
    private func handleChangePlan(_ packageId: String, _ isUpgrade: Bool = true) {
        Utils.showLoading()
        
        APICommonServices.changePlan(packageId, isUpgrade) { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleChangePlan", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.showSuccessChangeSubscription()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func handleCancelSubscription() {
        Utils.showLoading()
        
        APICommonServices.cancelSubscription() { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleCancelSubscription", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.showSuccessChangeSubscription()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func handleDeactiveAccount() {
        Utils.showLoading()
        
        APICommonServices.deactiveAccount() { [weak self] (resp) in
            guard let self = self else {return}
            
            mPrint("handleDeactiveAccount", (resp))
            
            if let resp = resp, let status = resp["status"] as? Bool {
                if status {
                    self.showSuccessChangeSubscription()
                } else {
                    Utils.showAlert(self, "", resp["message"] as? String ?? "")
                }
            }
            
            Utils.dismissLoading()
        }
    }
    
    private func showSuccessChangeSubscription() {
        let content = ResultAlertView().loadSuccessView()
        content.config(nil) { [weak self] (data) in
            guard let self = self else {return}
            
            Utils.closeCustomAlert()
            
            self.getPackages()
        }
        
        Utils.showAlertWithCustomView(content)
    }
    
    private func buildCardUpgrade(_ plans: [[String: Any?]] = []) {
        for subview in self.scrollViewCard.subviews {
            subview.removeFromSuperview()
        }
        
        let heightParent: CGFloat = 180.0
        
        let contentView = UIView()
        let widthCard: CGFloat = 135.0
        let heightCard: CGFloat = heightParent - 20.0
        let spaceCard: CGFloat = 20.0
        self.scrollViewCard.addSubview(contentView)
        
        contentView.snp.makeConstraints { (make) in
            make.left.equalTo(scrollViewCard)
            make.top.equalTo(scrollViewCard)
            make.right.equalTo(scrollViewCard)
            make.bottom.equalTo(scrollViewCard)
        }
        
        for index in 0..<plans.count {
            let cardUpgrade = CardUpgrade().loadView()
            cardUpgrade.currentPlanIndex = self.currentPlanIndex
            cardUpgrade.currentIndex = index
            
            let data = plans[index]
            
            cardUpgrade.config(data, onClickedCardCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.handleClickedCard(data)
            }, onClickedUpgradeCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.handleUpgradeCard(data)
            })
            
            contentView.addSubview(cardUpgrade)
            
            let leftConstraint: CGFloat = (CGFloat(index) * (widthCard + spaceCard))
            
            cardUpgrade.snp.makeConstraints { (make) in
                make.left.equalTo(contentView).offset(leftConstraint)
                make.top.equalTo(contentView).offset(10)
                make.bottom.equalTo(contentView).offset(-10)
                make.width.equalTo(widthCard)
                make.height.equalTo(heightCard)
                
                if index == (plans.count - 1) {
                     make.right.equalTo(contentView).offset(-spaceCard)
                }
                
            }
            
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .UPDATED_SUBSCRIPTION, object: nil)
    }
    
}

extension UpgradeViewController {
    func updateLanguages() {
        self.lblUpgradeForMore.text = LOCALIZED.upgrade_for_more_benefits.translate
        self.lblTitleScreen.text = LOCALIZED.txt_upgrade_title.translate
        self.lblTitleFooter.text = LOCALIZED.don_t_want_to_subscribe_anymore.translate
    }
}
