//
//  QuickSignUpViewController.swift
//  TGCMember
//
//  Created by vang on 3/3/20.
//  Copyright © 2020 gkim. All rights reserved.
//

import UIKit

class QuickSignUpViewController: BaseViewController {
    func updateLanguages() {
        //self.lblSignUp.text = LOCALIZED.txt_sign_up.translate
        self.tfEmail.placeholder = LOCALIZED.enter_your_email_address.translate
        self.lblWarningEmail.text = LOCALIZED.please_enter_a_valid_email.translate
        
        self.lblPassword.text = LOCALIZED.txt_password_title.translate
        self.tfPassword.placeholder = LOCALIZED.enter_password.translate
        self.lblWarningPassword.text = LOCALIZED.please_enter_at_least_6_characters.translate
        
        self.lblConfirmPassword.text = LOCALIZED.txt_confirm_password.translate
        self.tfConfirmPassword.placeholder = LOCALIZED.enter_password.translate
        self.lblWarningConfirmPassword.text = LOCALIZED.password_and_confirm_password_does_not_match.translate
        
        
        //        self.btnSkipNow.setTitleUnderline(LOCALIZED.skip_for_now.translate, color: UIColor(hexFromString: "#E46D57"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 15))
        //
        self.btnSubmit.setTitle(LOCALIZED.txt_submit_title.translate, for: .normal)
        
        self.lblCongratulations?.setAttribute("You are now eligible for lifetime Deals & Discounts at the best places all around the world.\n\nSign Up now to receive your 1st deal. A free OGOGO membership activation code to share with a friend!", color: UIColor("rgb 39 39 39"), font: UIFont(name: ROBOTO_FONT.REGULAR.rawValue, size: 14), spacing: 3.5)
        
        self.lblCongratulations?.textAlignment = .center
    }
    
    @IBOutlet weak var coverBody: UIView!
    @IBOutlet weak var lblWarningConfirmPassword: UILabel!
    @IBOutlet weak var lblWarningPassword: UILabel!
    @IBOutlet weak var lblWarningEmail: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var coverEmailWarning: UIView!
    @IBOutlet weak var coverPasswordWarning: UIView!
    @IBOutlet weak var coverConfirmPasswordWarning: UIView!
    
    @IBOutlet weak var btnSubmit: HighlightableButton!
    @IBOutlet weak var coverYourNameView: UIView!
    @IBOutlet weak var coverEmailView: UIView!
    @IBOutlet weak var coverConfirmPasswordView: UIView!
    @IBOutlet weak var coverPasswordView: UIView!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet weak var btnShowConfirmPassword: UIButton!
    @IBOutlet weak var btnShowPassword: UIButton!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var imageValid: UIImageView!
    @IBOutlet weak var tfYourName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var lblEmail: UILabel!
    
    
    @IBOutlet weak var lblCongratulations: UILabel!
    
    @IBOutlet weak var bottomBodyConstraint: NSLayoutConstraint!
    private var isShowPass: Bool = true
    private var isShowConfirmPass: Bool = true
     var backToDealDetail: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        decorate()
        refillIfHave()
        
        _ = validateAllField()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    @IBAction func onClickedSubmit(_ sender: Any) {
        self.doSubmit()
    }
    
    
    private func handleGoBack(_ toSignIn: Bool = false) {
        if let dealDetailIns = GLOBAL.GlobalVariables.dealDetailInstance, self.backToDealDetail == true {
            self.navigationController?.popToViewController(dealDetailIns, animated: true)
        } else {
            if let discoveryIns = GLOBAL.GlobalVariables.discoveryInstance {
               self.navigationController?.popToViewController(discoveryIns, animated: true)
            } else if let appDelegate = Utils.getAppDelegate() {
                appDelegate.initViewController(DiscoveryMainController())
            }
        }

    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            let tBottom = keyboardSize.height - 10
            
            self.bottomBodyConstraint?.constant = tBottom
            
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                guard let self = self else {return}
                
                self.coverBody.superview?.layoutIfNeeded()
                }, completion:  { finished in
                    self.bottomBodyConstraint?.constant = tBottom
            })
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.bottomBodyConstraint?.constant = 0
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let self = self else {return}
            
            self.coverBody.superview?.layoutIfNeeded()
            }, completion:  { finished in
                self.bottomBodyConstraint?.constant = 0
        })
    }
    
    private func dismissKeyboard() {
        self.tfYourName.resignFirstResponder()
        self.tfEmail.resignFirstResponder()
        self.tfPassword.resignFirstResponder()
        self.tfConfirmPassword.resignFirstResponder()
    }
    
    private func refillIfHave() {
        let quickSignUpModel = Utils.getCachedSignUpInfo()
        
        self.tfYourName?.text = quickSignUpModel.name
        self.tfEmail?.text = quickSignUpModel.email
    }
    
    private func decorate() {
        coverBody?.layer.cornerRadius = CGFloat(40)
        coverBody?.layer.masksToBounds = true
        coverBody?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.scrollView.contentInsetAdjustmentBehavior = .never
        
        self.coverYourNameView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverYourNameView.layer.borderWidth = 0.5
        self.coverYourNameView.layer.cornerRadius = 3.0
        self.coverYourNameView.layer.masksToBounds = true
        
        self.coverEmailView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailView.layer.borderWidth = 0.5
        self.coverEmailView.layer.cornerRadius = 3.0
        self.coverEmailView.layer.masksToBounds = true
        
        self.coverPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPasswordView.layer.borderWidth = 0.5
        self.coverPasswordView.layer.cornerRadius = 3.0
        self.coverPasswordView.layer.masksToBounds = true
        
        self.coverConfirmPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverConfirmPasswordView.layer.borderWidth = 0.5
        self.coverConfirmPasswordView.layer.cornerRadius = 3.0
        self.coverConfirmPasswordView.layer.masksToBounds = true
        
        tooglePassword()
        toogleConfirmPassword()
        
        self.coverEmailWarning.isHidden = true
        self.coverPasswordWarning.isHidden = true
        self.coverConfirmPasswordWarning.isHidden = true
        
    }
    
    @IBAction func onClickedBG(_ sender: Any) {
        dismissKeyboard()
    }
    
    @IBAction func onClickedShowPass(_ sender: Any) {
        tooglePassword()
    }
    
    @IBAction func onClickedShowConfirmPass(_ sender: Any) {
        toogleConfirmPassword()
    }
    
    private func tooglePassword() {
        isShowPass = !isShowPass
        
        self.tfPassword.isSecureTextEntry = !isShowPass
        
        self.btnShowPassword.setImage(isShowPass ? UIImage(named: "show_pass") : UIImage(named: "hide_pass"), for: .normal)
    }
    
    private func toogleConfirmPassword() {
        isShowConfirmPass = !isShowConfirmPass
        
        self.tfConfirmPassword.isSecureTextEntry = !isShowConfirmPass
        
        self.btnShowConfirmPassword.setImage(isShowConfirmPass ? UIImage(named: "show_pass") : UIImage(named: "hide_pass"), for: .normal)
    }
    
    private func doSubmit() {
        
        self.dismissKeyboard()
        
        if !validateAllField() {
            return
        }
        
        Utils.showLoading()
        
        let params = [
            "user_id": Utils.getUserId(),
            "name": self.tfYourName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "email": self.tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
            "password": self.tfPassword.text ?? ""
            ]  as [String : Any]
        
        mPrint("BASE UPDATE --> ", params)
        
        APICommonServices.baseUpdate(params) { (resp) in
            mPrint("BASE UPDATE RESP --> ", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                let message = resp["message"] as? String ?? ""

                if status {
                    Utils.getMyProfile { [weak self] (success) in
                        guard let self = self else {return}
                        
                        Utils.dismissLoading()
                        
                        NotificationCenter.default.post(name: .REFRESH_MENU_VIEW, object: nil)
                        if GLOBAL.GlobalVariables.isHomeScreenReady {
                            NotificationCenter.default.post(name: .REFRESH_HOME_MAP_AFTER_AUTHORIZED, object: nil)
                        }
                        
                        if Utils.isSuggestGiftCode() {
                            let freeVC = SendFreeCodeViewController()
                            freeVC.backToDealDetail = self.backToDealDetail
                            
                            self.navigationController?.pushViewController(freeVC, animated: true)
                        } else {
                            self.handleGoBack()
                        }
                        
                    }
                } else {
                    Utils.dismissLoading()
                    Utils.showAlertStyle(message, status)
                }
                
            } else {
                Utils.dismissLoading()
            }
            
        }
    }
    
    private func validateAllField(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = true
        
        let isValidYourName = self.validateYourName(pText, withoutField)
        let isValidEmail = self.validateEmail(pText, withoutField)
        let isValidPassword = self.validatePassword(pText, withoutField)
        let isValidConfirmPassword = self.validateConfirmPassword(pText, withoutField == self.tfPassword ? pText : self.tfPassword.text ?? "", withoutField)
        
        if !isValidEmail || !isValidYourName || !isValidPassword || !isValidConfirmPassword {
            isValid = false
        }
        
        self.btnSubmit?.backgroundColor =  isValid ? UIColor(255, 193, 6, 1) : UIColor(142, 142, 147, 1)
        self.btnSubmit?.isUserInteractionEnabled = isValid
        
        if  isValid {
            self.btnSubmit?.layer.applySketchShadow()
        } else {
            self.btnSubmit?.layer.removeSketchShadow()
        }
        
        return isValid
    }
    
    
    private func validateYourName(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfYourName ? pText : self.tfYourName.text
        
        var isValid = false
        
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = true
        }
        
        return isValid
        
    }
    
    private func validateEmail(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        let checkText = withoutField == self.tfEmail ? pText : self.tfEmail.text
        
        var isValid = false
        if checkText?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            isValid = Utils.isValidEmail(tfEmail.text)
            
            self.coverEmailWarning.isHidden  = withoutField == self.tfEmail ? true : isValid
            self.imageValid.isHidden  = withoutField == self.tfEmail ? true : !isValid
            self.coverEmailView.layer.borderColor = withoutField == self.tfEmail ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
        } else {
            self.resetViewEmail()
        }
        
        
        return isValid
    }
    
    private func resetViewEmail() {
        self.coverEmailView.layer.borderColor =  INPUT_COLOR.NORMAL.color.cgColor
        self.coverEmailWarning.isHidden  = true
        self.imageValid.isHidden  = true
    }
    
    
    private func validatePassword(_ pText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        
        let checkText = withoutField == self.tfPassword ? pText : self.tfPassword.text
        if checkText != "" {
            isValid = false
            if let text = checkText, text.count > 5 {
                isValid = true
            }
            
            self.coverPasswordWarning.isHidden  = withoutField == self.tfPassword ? true : isValid
            self.coverPasswordView.layer.borderColor = withoutField == self.tfPassword ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
            
        } else {
            self.resetViewPassword()
        }
        
        return isValid
    }
    
    
    private func resetViewPassword() {
        self.coverPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverPasswordWarning.isHidden  = true
    }
    
    
    private func validateConfirmPassword(_ pText: String = "", _ passText: String = "", _ withoutField: UITextField? = nil) -> Bool {
        var isValid = false
        let checkText = withoutField == self.tfConfirmPassword ? pText : self.tfConfirmPassword.text
        
        if checkText != "" {
            isValid =  passText == checkText
            
            self.coverConfirmPasswordWarning.isHidden  = withoutField == self.tfConfirmPassword ? true : isValid
            self.coverConfirmPasswordView.layer.borderColor = withoutField == self.tfConfirmPassword ? INPUT_COLOR.NORMAL.color.cgColor : (isValid ? INPUT_COLOR.NORMAL.color.cgColor : INPUT_COLOR.ERROR.color.cgColor)
            
        } else {
            self.resetViewConfirmPassword()
        }
        
        return isValid
    }
    
    private func resetViewConfirmPassword() {
        self.coverConfirmPasswordView.layer.borderColor = INPUT_COLOR.NORMAL.color.cgColor
        self.coverConfirmPasswordWarning.isHidden  = true
    }
}


extension QuickSignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfYourName {
            tfEmail.becomeFirstResponder()
        } else if textField == tfEmail {
            tfPassword.becomeFirstResponder()
        } else if textField == tfPassword {
            tfConfirmPassword.becomeFirstResponder()
        } else if textField == tfConfirmPassword {
            self.doSubmit()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            _ = self.validateAllField(updatedText, textField)
            
            if textField == self.tfYourName {
                GLOBAL.GlobalVariables.quickSignUpModel.name = updatedText
            } else if textField == self.tfEmail {
                GLOBAL.GlobalVariables.quickSignUpModel.email = updatedText
            }
            
            Utils.cachedSignUpInfo()
            
            if textField == self.tfPassword || textField == self.tfConfirmPassword {
                textField.text = updatedText
                
                return false
            }
            
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        setTimeout({
            if textField == self.tfEmail {
                self.resetViewEmail()
            } else if textField == self.tfPassword {
                self.resetViewPassword()
            } else if textField == self.tfConfirmPassword {
                self.resetViewConfirmPassword()
            }
        }, 100)
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        _ = validateAllField()
    }
}
