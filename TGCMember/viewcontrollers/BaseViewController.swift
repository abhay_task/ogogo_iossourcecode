//
//  BaseViewController.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import UserNotificationsUI
import UserNotifications

typealias BaseViewController = MViewController & LanguageProtocol

open class MViewController: UIViewController {
    var imagePicker : UIImagePickerController?
    var photoCallback: DataCallback?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLanguagesImmediately(_:)), name: .UPDATE_LANGUAGES_IMMEDIATELY, object: nil)
        
    }
    
    @objc func updateLanguagesImmediately(_ notification: Notification) {
        guard let controller = self as? BaseViewController else {
            return
        }
        
        controller.updateLanguages()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setup()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setup() {
        guard let controller = self as? BaseViewController else {
            return
        }
        
        controller.updateLanguages()
    }
    
    func prepareLocalPushAndSend(timeInterval: TimeInterval){
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let content = UNMutableNotificationContent()
        
        content.categoryIdentifier = "deal"
        content.title = "THE GOLD CARD - DAILY DEALS"
        content.subtitle = ""
        content.body = "Hey Andrew, here are your daily deals for 2 Feb 2019. Open now for great offers."
        
        content.userInfo = ["attachment-video" : "https://www.w3schools.com/html/mov_bbb.mp4", "type": "video"
        ]
        
        let request = UNNotificationRequest(identifier: "tgcmember", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
 
    

    func openChatDetail(_ data : [String: Any?]?) {
        let chatVC = ChatDetailViewController()
        chatVC.data = data
        
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    
    func openSwipeDeals(_ deals: [[String: Any?]], _ fromPNS: Bool = false, _ fromCarousel: Bool = false, _ isStarter: Bool = false) {
        if let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController(), deals.count > 0 {
            let dailyVC = DailyDealsViewController()
            dailyVC.deals = deals
            dailyVC.fromPNS = fromPNS
            dailyVC.fromCarousel = fromCarousel
            dailyVC.isStarter = isStarter
            
            topVC.addSubviewAsPresent(dailyVC)
        }
    }
    
    
    open func initBarWith(title: String?, leftClicked: DataCallback?) -> Void {
        let navBarVC = NavigationBarViewController()
        navBarVC.initBarWith(title: title, leftClicked: leftClicked)
       
        navBarVC.view.frame = CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: Utils.isIPhoneNotch() ? 84 : 64)
        
        self.view.addSubview(navBarVC.view)
        self.addChild(navBarVC)
    }
}


protocol LanguageProtocol {
    func updateLanguages()
}


extension MViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    
    public func showPickerPhoto() {
        if let _ = imagePicker {
            
        } else {
            imagePicker = UIImagePickerController()
        }
        
        guard let imagePicker = imagePicker else {return}
        
        imagePicker.delegate = self
        let alert = UIAlertController(title: "Select a Photo", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo...", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Choose from Library...", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openCamera() {
        guard let imagePicker = imagePicker else {return}
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func openGallary() {
        guard let imagePicker = imagePicker else {return}
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let data = pickedImage.jpegData(compressionQuality: 0.7) {
                //self.handleSendFileMessage(data)
             
                self.photoCallback?(["imageData": data])
            } else {
                self.photoCallback?(["imageData": nil])
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}

