//
//  BaseTripDetailViewController.swift
//  TGCMember
//
//  Created by vang on 10/16/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class BaseTripDetailViewController: BaseViewController {
    open func updateLanguages() {
        
    }
    
    var data: [String: Any?]?
    @IBOutlet weak var coverContent: UIView!
    private var emptyTripView: TripEmptyView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sDecorate()
    }
    
    private func sDecorate() {
        coverContent.alpha = 0
    }
    
    open func onClickedExplore() {
        if let app = GLOBAL.GlobalVariables.appDelegate, let vc = app.getTopViewController()  {
            let exploreVC = ExploreViewController()
            exploreVC.data = self.data
            
            vc.addSubviewAsPresent(exploreVC)
        }
    }
    
    func willDisplayEmptyView(_ willDisplay: Bool = true) {
           if willDisplay {
               if self.emptyTripView == nil {
                    self.emptyTripView = TripEmptyView().loadView()
                    self.emptyTripView?.config(nil, onClickedExploreCallback: { [weak self] (data) in
                        guard let self = self else {return}
                        
                        self.onClickedExplore()
                    })
                
                    if let subview = self.emptyTripView {
                       self.view.addSubview(subview)
                       
                       subview.snp.makeConstraints { (make) in
                           make.left.equalToSuperview()
                           make.top.equalToSuperview()
                           make.right.equalToSuperview()
                           make.bottom.equalToSuperview()
                       }
                    }
                }
               
                if let subview = self.emptyTripView {
                    subview.alpha = 1
                }
            
                self.coverContent?.alpha = 0
           } else {
                if let subview = self.emptyTripView {
                    subview.alpha = 0
                }
               
                self.coverContent?.alpha = 1
           }
       }

}
