//
//  ExSlideMenuController.swift
//  TGCMember
//
//  Created by vang on 5/20/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class ExSlideMenuController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.changeLeftViewWidth(Constants.WIDTH_MENU)
    }
    
    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is DiscoveryMainController {
           
                return true
            }
        }
        
        return false
    }
    

}
