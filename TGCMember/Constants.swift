//
//  Constants.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit


class Constants: NSObject {
    static let IS_AR_DEMO: Bool = false
    
    static let DISTANCE_TO_REQUEST_DEAL: Double = 8.0
    static let WIDTH_MENU: CGFloat = 80 * (SCREEN_WIDTH / 100)
    static let OGOGO_CARD_LINK : String = "https://dev-gcc.s3-ap-southeast-1.amazonaws.com/assets/images/ogogo-card.png"
    static let GOOGLE_MAP_STYLE_URL : String = "https://dev-gcc.s3-ap-southeast-1.amazonaws.com/data/style-maps.json"
    static let BRANCH_CHANNEL : String = "member"
    static let DEFAULT_LOCATION: Float = 0.0
    static let TIME_INTERVAL_SPLASH_DEAL: TimeInterval =  0 //seconds
    static let TIME_INTERVAL_UNDO_CARD: TimeInterval =  0 //seconds
    static let TIME_INTERVAL_TOAST_CARD: TimeInterval =  1 //seconds
    static let TIME_INTERVAL_HOLDING_CARD: TimeInterval =  1 //seconds
    static let ALLOW_SWIPE_MENU: Bool = false
    static let BUTTON_SUBMIT_COLOR = "#625BED"
    static let DEFAULT_FONT = ROBOTO_FONT.LIGHT.rawValue
    static let CONTENT_WEB_FORMAT  = "<html><head><style>@font-face{font-family: \"\(DEFAULT_FONT)\"; src: url(\(DEFAULT_FONT).ttf);} body {font-family: \"\(DEFAULT_FONT)\"; font-size: 13; BODY_BACKGROUND_COLOR} img {max-width:100%%; height: auto !important;width:auto !important;}</style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0\"><title></title></head><body>%@</body></head></html>"
    
}
