//
//  CustomAlertView.swift
//  TGCMember
//
//  Created by vang on 5/17/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit

class CustomAlertView: UIView {
    
    var useMotionEffects: Bool = true
    var motionEffectExtent: Int = 10
    
    private var alertView: UIView!
    var containerView: UIView!
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0,width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        setObservers()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setObservers()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setObservers()
    }
    
    // Create the dialog view and animate its opening
    internal func show(_ bgColor: UIColor = UIColor(white: 0, alpha: 0.4), animation: Bool = true, forceTop: Bool = false) {
        show(bgColor: bgColor, animation: animation, forceTop: forceTop, completion: nil)
    }
    
    internal func show(bgColor: UIColor = UIColor(white: 0, alpha: 0.4), animation: Bool = true, forceTop: Bool = false, completion: ((Bool) -> Void)?) {
        alertView = createAlertView(animation)
        
        if animation {
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
        
        self.backgroundColor = UIColor(white: 0, alpha: 0)
      
        self.addSubview(alertView)
        
        if animation {
        // Attach to the top most window
            switch (UIApplication.shared.statusBarOrientation) {
            case UIInterfaceOrientation.landscapeLeft:
                self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 270 / 180))
    
            case UIInterfaceOrientation.landscapeRight:
                self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 90 / 180))
    
            case UIInterfaceOrientation.portraitUpsideDown:
                self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * 180 / 180))
    
            default:
                break
            }
        }
        
        self.frame = CGRect(x:0, y:0, width: self.frame.width, height: self.frame.height)
        //        UIApplication.shared.windows.last?.addSubview(self)
        

        
        if forceTop {
            if let topVC = GLOBAL.GlobalVariables.rootVC {
                topVC.view.addSubview(self)
            }
        } else {
            if let appDelegate = Utils.getAppDelegate(), let topVC = appDelegate.getTopViewController() {
                topVC.view.addSubview(self)
            }
        }
        
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.backgroundColor = bgColor
            self.alertView.layer.opacity = 1
            
            if animation {
                self.alertView.layer.transform = CATransform3DMakeScale(1, 1, 1)
            }
        }, completion: completion)
        
        
        
    }
    
    // Close the alertView, remove views
    internal func close() {
        close(completion: nil)
    }
    
    internal func close(completion: ((Bool) -> Void)?) {
        let currentTransform = alertView.layer.transform
        
        let startRotation = alertView.value(forKeyPath: "layer.transform.rotation.z") as! Float
        let rotation = CATransform3DMakeRotation(CGFloat(-startRotation) + CGFloat(Double.pi * 270 / 180), 0, 0, 0)
        
        alertView.layer.transform = CATransform3DConcat(rotation, CATransform3DMakeScale(1, 1, 1))
        alertView.layer.opacity = 1
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions() , animations: {
            self.backgroundColor = UIColor(white: 0, alpha: 0)
            self.alertView.layer.transform = CATransform3DConcat(currentTransform, CATransform3DMakeScale(0.6, 0.6, 1))
            self.alertView.layer.opacity = 0
        }, completion: { (finished: Bool) in
            for view in self.subviews as [UIView] {
                view.removeFromSuperview()
            }
            
            self.removeFromSuperview()
            completion?(finished)
        })
    }
    
    // Enables or disables the specified button
    // Should be used after the alert view is displayed
    internal func setButtonEnabled(enabled: Bool, buttonName: String) {
        for subview in alertView.subviews as [UIView] {
            if subview is UIButton {
                let button = subview as! UIButton
                
                if button.currentTitle == buttonName {
                    button.isEnabled = enabled
                    break
                }
            }
        }
    }
    
    // Observe orientation and keyboard changes
    private func setObservers() {
        NotificationCenter.default.addObserver(self, selector:#selector(deviceOrientationDidChange(notification:)) , name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    // Create the containerView
    private func createAlertView(_ animation: Bool = true) -> UIView {
        if containerView == nil {
            containerView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 150))
        }
        
        let screenSize = self.calculateScreenSize()
        let dialogSize = self.calculateDialogSize()
        
        let view = UIView(frame: CGRect(
            x: (screenSize.width - dialogSize.width) / 2,
            y: (screenSize.height - dialogSize.height) / 2,
            width: dialogSize.width,
            height: dialogSize.height ))
        
        
        view.layer.opacity = 0.5
        
        if animation {
            view.layer.transform = CATransform3DMakeScale(1.3, 1.3, 1)
            view.layer.shouldRasterize = true
            view.layer.rasterizationScale = UIScreen.main.scale
        }
        
        // Apply motion effects
        if useMotionEffects {
            applyMotionEffects(view: view)
        }
        
        view.addSubview(containerView)
        
        return view
    }
    
    
    
    // Generate the gradient layer of the alertView
    private func generateGradient(bounds: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        
        gradient.frame = bounds
        //gradient.cornerRadius = cornerRadius
        
        gradient.colors = [
            UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha:1).cgColor,
            UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha:1).cgColor,
            UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha:1).cgColor
        ]
        
        return gradient
    }
    
    
    // Calculate the size of the dialog
    private func calculateDialogSize() -> CGSize {
        return CGSize(
            width: containerView.frame.size.width,
            height: containerView.frame.size.height
        )
    }
    
    // Calculate the size of the screen
    private func calculateScreenSize() -> CGSize {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        if orientationIsLandscape() {
            return CGSize(width: height, height: width)
        } else {
            return CGSize(width: width, height: height)
        }
    }
    
    // Add motion effects
    private func applyMotionEffects(view: UIView) {
        let horizontalEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontalEffect.minimumRelativeValue = -motionEffectExtent
        horizontalEffect.maximumRelativeValue = +motionEffectExtent
        
        let verticalEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        verticalEffect.minimumRelativeValue = -motionEffectExtent
        verticalEffect.maximumRelativeValue = +motionEffectExtent
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [horizontalEffect, verticalEffect]
        
        view.addMotionEffect(motionEffectGroup)
    }
    
    // Whether the UI is in landscape mode
    private func orientationIsLandscape() -> Bool {
        return UIDevice.current.orientation.isLandscape
    }
    
    
    // Handle device orientation changes
    @objc func deviceOrientationDidChange(notification: NSNotification) {
        let interfaceOrientation = UIApplication.shared.statusBarOrientation
        let startRotation = self.value(forKeyPath: "layer.transform.rotation.z") as! Float
        
        var rotation: CGAffineTransform
        
        switch (interfaceOrientation) {
        case UIInterfaceOrientation.landscapeLeft:
            rotation = CGAffineTransform(rotationAngle: CGFloat(-startRotation) + CGFloat(Double.pi * 270 / 180))
            break
            
        case UIInterfaceOrientation.landscapeRight:
            rotation = CGAffineTransform(rotationAngle: CGFloat(-startRotation) + CGFloat(Double.pi * 90 / 180))
            break
            
        case UIInterfaceOrientation.portraitUpsideDown:
            rotation = CGAffineTransform(rotationAngle: CGFloat(-startRotation) + CGFloat(Double.pi * 180 / 180))
            break
            
        default:
            rotation = CGAffineTransform(rotationAngle: CGFloat(-startRotation) + 0)
            break
        }
        
        //        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
        ////            self.alertView.transform = rotation
        //        }, completion: nil)
        
        // Fix errors caused by being rotated one too many times
        //        dispatch_after(dispatch_time(dispatch_time_t(DispatchTime.now()), Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
        //            let endInterfaceOrientation = UIApplication.sharedApplication.statusBarOrientation
        //            if interfaceOrientation != endInterfaceOrientation {
        //                // TODO: user moved phone again before than animation ended: rotation animation can introduce errors here
        //            }
        //        })
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //            let endInterfaceOrientation = UIApplication.shared.statusBarOrientation
        //            if interfaceOrientation != endInterfaceOrientation {
        //                // TODO: user moved phone again before than animation ended: rotation animation can introduce errors here
        //            }
        //
        //        }
        
    }
    
    // Handle keyboard show changes
    @objc func keyboardWillShow(notification: NSNotification) {
        let screenSize = self.calculateScreenSize()
        let dialogSize = self.calculateDialogSize()
        //[[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        var keyboardSize = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as AnyObject).cgRectValue.size
        
        if orientationIsLandscape() {
            keyboardSize = CGSize(width: keyboardSize.height, height: keyboardSize.width)
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions(), animations: {
            self.alertView.frame = CGRect(
                x: (screenSize.width - dialogSize.width) / 2,
                y: (screenSize.height - keyboardSize.height - dialogSize.height) / 2,
                width: dialogSize.width,
                height: dialogSize.height
            )
        }, completion: nil)
    }
    
    // Handle keyboard hide changes
    @objc func keyboardWillHide(notification: NSNotification) {
        let screenSize = self.calculateScreenSize()
        let dialogSize = self.calculateDialogSize()
        
        UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions(), animations: {
            self.alertView.frame = CGRect(
                x: (screenSize.width - dialogSize.width) / 2,
                y: (screenSize.height - dialogSize.height) / 2,
                width: dialogSize.width,
                height: dialogSize.height
            )
        }, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
}
