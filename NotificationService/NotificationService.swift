//
//  NotificationService.swift
//  NotificationService
//
//  Created by Sang Huynh on 6/3/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UserNotifications
import UIKit
import Alamofire

final class NotificationService: UNNotificationServiceExtension {
    
    private var contentHandler: ((UNNotificationContent) -> Void)?
    private var bestAttemptContent: UNMutableNotificationContent?
    
    override internal func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void){
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        func failEarly() {
            contentHandler(request.content)
        }
        
        guard let content = (request.content.mutableCopy() as? UNMutableNotificationContent) else {
            return failEarly()
        }
        
        var attachmentURL  : String?
        var identifier = "image";
        if let attachmentImageURL = content.userInfo["attachment-image"] as? String {
            attachmentURL = attachmentImageURL
            identifier = "image.png"
        }
        if let attachmentImageURL = content.userInfo["attachment-image-gif"] as? String {
            attachmentURL = attachmentImageURL
            identifier = "image.gif"
        }
        if let attachmentVideoURL = content.userInfo["attachment-audio"] as? String {
            attachmentURL = attachmentVideoURL
            identifier = "audio.mp3"
        }
        if let attachmentVideoURL = content.userInfo["attachment-video"] as? String {
            attachmentURL = attachmentVideoURL
             identifier = "video.mp4"
        }
        
        guard let _ = attachmentURL else {
             return failEarly()
        }
       
        let url = URL(string: attachmentURL!)
        
        downloadWithURL(identifier ,  url: url!, completion: { [weak self] (complete) in
            guard let self = self else { return }
            
            if complete {
                contentHandler(self.bestAttemptContent ?? content.copy() as! UNNotificationContent)
            }
        })
        
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent = bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    private func downloadWithURL(_ identifier : String, url: URL, completion: @escaping (Bool) -> Void) {
        AF.request(url.absoluteString).downloadProgress(closure : { (progress) in
            
        }).responseData{ (response) in
            if let data = response.value {
                
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let attachmentURL = documentsURL.appendingPathComponent(identifier)
                
                do {
                    try data.write(to: attachmentURL)
                } catch {
                    print("Something went wrong!")
                }
                
                do {
                    let attachment = try UNNotificationAttachment(identifier: identifier, url: attachmentURL, options: nil)
                    self.bestAttemptContent?.attachments = [attachment]
                    completion(true)
                } catch {
                    completion(true)
    
                }
                
              
            }
        }
        
    }
    

}

