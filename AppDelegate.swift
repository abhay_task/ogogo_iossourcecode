//
//  AppDelegate.swift
//  TGCMember
//
//  Created by vang on 4/22/19.
//  Copyright © 2019 gkim. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import Branch
import UserNotifications
import Fabric
import Crashlytics
import Alamofire
import IQKeyboardManagerSwift
//import Kingfisher

enum DEAL_NOTIFCATION_ACTION: String {
    case TAKE_A_LOCK = "deal.takeALook"
    case NO_THANKS = "deal.noThanks"
    case REMIND_ME_LATER = "deal.redmindMeLater"
}

enum NETWORK_STATUS {
    case CONNECTED
    case DISCONNECTED
    case UNDEFINED
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    private let rControl = RMController()
    private let mLocationManager = MLocationManager()
    private var isStartNew: Bool = true
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GLOBAL.GlobalVariables.appDelegate = self
//        CacheManager.shared.dailyShowDatePerUser = [:]
//        CacheManager.shared.starterShowDatePerUser = [:]
//        CacheManager.shared.termsAndConditionsAccepted = nil
        self.listenerNetwork()
        //Keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        if #available(iOS 13.0, *) {
            self.window?.overrideUserInterfaceStyle = .light
        }
        
//        KingfisherManager.shared.cache.clearMemoryCache()
//        KingfisherManager.shared.cache.clearCache()
//        KingfisherManager.shared.cache.clearDiskCache()
        
        // Override point for customization after application launch.
        NotificationCenter.default.addObserver(self, selector: #selector(forceSignIn(_:)), name: .FORCE_SIGN_IN, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(finishedSweeptakes(_:)), name: .FINISHED_SWEEPTAKES, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAppDidBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(deviceLanguageChanged), name: NSLocale.currentLocaleDidChangeNotification, object: nil)
        
        self.initLanguagesSupport()
        
        Fabric.with([Crashlytics.self])
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        registerNotification(application)
        
        // Intercom
        IntercomManager.shared.setup()
        
        self.checkLoggedIn()
        
        // if you are using the TEST key
        Branch.setUseTestBranchKey(false)
        
        // listener for Branch Deep Link data
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            let data = params as? [String: Any]
            
            self.handleBranch(data)
        }
        
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        
        //        setTimeout({
        //            //self.processPushHighlightVendor(["vendorID" : "d7e50d8e-8bb6-4cd8-9817-5fa5bb9a4ea0"], userInteraction: false)
        //            setTimeInterval({ [weak self] (number, timer) in
        //                guard let self = self else {return}
        //
        //                if GLOBAL.GlobalVariables.isHomeScreenReady {
        //                    timer.invalidate()
        //                     self.processSplash(["deal_show_id" : "7c146c2c-0c9d-4b09-999a-347a83563ac8"], userInteraction: false)
        //                }
        //            }, 500)
        //
        //        }, 10000)
        //
        
        
        return true
    }
    
    @objc private func deviceLanguageChanged() {
        let languageCode = Locale.current.languageCode ?? LANGUAGE.ENGLISH.rawValue
        let language = LANGUAGE(rawValue: languageCode) ?? .ENGLISH
        Utils.setLanguage(language)
    }
    
    private func listenerNetwork () {
        // Allocate a reachability object
        let connection = Connection.reachability(withHostName: "www.google.com")
        
        // Set the blocks
        connection!.reachableBlock = { (connection: Connection?) -> Void in
            
            if GLOBAL.GlobalVariables.connectionStatus == .DISCONNECTED {
                GLOBAL.GlobalVariables.connectionStatus = .CONNECTED
                DispatchQueue.main.async {
                    print(" ---> REACHABLE!")
                    if GLOBAL.GlobalVariables.isShowNetworkAlert {
                        Utils.closeCustomAlert(forceTop: true)
                        GLOBAL.GlobalVariables.isShowNetworkAlert = false
                    }
                    
//                    for (_, key_value) in GLOBAL.GlobalVariables.dictionaryRequestStatus.enumerated() {
//                       print("\(key_value)")
//                        if let tRequest = key_value.value {
//                            tRequest.request
//                        }
//                    }
                }
            }
        }
        
        connection!.unreachableBlock = { (connection: Connection?) -> Void in
            
            if GLOBAL.GlobalVariables.connectionStatus != .DISCONNECTED {
                GLOBAL.GlobalVariables.connectionStatus = .DISCONNECTED
                DispatchQueue.main.async {
                    print("----> UNREACHABLE!")
                    Utils.dismissLoading()
                    Utils.showNetworkError()
                }
            }
        }
        
        connection!.startNotifier()
    }
    private func initLanguagesSupport() {
        UserDefaults.standard.set([LANGUAGE.ENGLISH.rawValue, LANGUAGE.VIETNAM.rawValue, LANGUAGE.THAI.rawValue], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
    }
    
    private func checkLoggedIn() {
        _ = Utils.getUserInfo()
        
        self.initViewController(CheckViewController())
        
        if Utils.getAppToken() == nil {
            IntercomManager.shared.registerUser(with: GLOBAL.GlobalVariables.userInfo?["ID"] as? String)
            
            Utils.initApp { [weak self] (success) in
                guard let self = self else {return}
                
//                GLOBAL.GlobalVariables.isFirstInitApp = true
                
                Utils.getSystemConfig { [weak self] (success) in
                    guard let self = self else {return}
                    
                    Utils.getTutorialConfig { [weak self] (success) in
                           guard let self = self else {return}
                           
                           self.initViewController(TutorialViewController())
                    }
                }
                
            }
        } else {
            GLOBAL.GlobalVariables.isFirstInitApp = false
            
            let dispatchGroup = DispatchGroup()
            
            dispatchGroup.enter()
            Utils.getMyProfile { (success) in
                dispatchGroup.leave()
            }
            
            dispatchGroup.enter()
            Utils.getSystemConfig { _ in
                dispatchGroup.leave()
            }
            
            dispatchGroup.enter()
            Repository.shared.getInfoTarget(completionHandler: { result in
                switch result {
                case .success(let infoTarget):
                    CacheManager.shared.infoTarget = infoTarget
                case .failure(let error):
                    print(print(error.message))
                }
                
                dispatchGroup.leave()
            })
            
            dispatchGroup.enter()
            Repository.shared.getPackages(completionHandler: { response in
                switch response {
                case .success(let data):
                    let currentPlan = data.data?.packages?.first(where: { $0.isCurrentPlan == true })
                    CacheManager.shared.currentPlan = currentPlan
                case .failure(let error):
                    print(error.localizedDescription)
                }
                
                dispatchGroup.leave()
            })
            
            dispatchGroup.notify(queue: .main, execute: { [weak self] in
                guard let self = self else { return }
                
                let isClickedWelcome = Utils.getIsClickedOnWelcome()
                
                if !isClickedWelcome {
                    Utils.getTutorialConfig { [weak self] (success) in
                           guard let self = self else {return}
                           
                           self.initViewController(TutorialViewController())
                    }
                } else if Utils.isRequireSignUpToGetDeal() {
                    self.initViewController(QuickSignUpViewController())
                } else {
                    let vc = Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className())
                    self.initViewController(vc)
                }
            })
        }
        

    }
    
    private func openSplashFromTEO(_ data: [String: Any?]) {
        
        if GLOBAL.GlobalVariables.isCheckScreenReady {
            let email = data["email"] as? String ?? ""
            
            APICommonServices.submitTEO(email) { (resp) in
                mPrint("submitTEO", resp)
            }
            
            let dealId = data["deal_id"] as? String ?? ""
            self.getTEOSplashDetail(dealId)
        }
    }
    
    private func handleBranch(_ data: [String: Any]?) {
        // do stuff with deep link data (nav to page, display content, etc)
        
        Utils.checkLogin(nil, requireLogin: false) { [weak self] (status) in
            guard let self = self else {return}
            
            if status == .NOT_YET {
                
                if let tData = data {
                    
                    let strType = tData["kind"] as? String ?? ""
                    
                    if strType != "" {
                        mPrint("handleBranch", tData)
                        
                        let fromApp = tData["from_app"] as? String ?? ""
                        let linkType = LINK_TYPE(rawValue: strType)
                        
                        if fromApp == Constants.BRANCH_CHANNEL {
                            
                            if linkType == LINK_TYPE.TELL_EVERYONE {
                                self.openSplashFromTEO(tData)
                            } else {
                                if let refCode = tData["ref_code"] as? String {
                                    self.handleGetReferral(refCode)
                                } else if GLOBAL.GlobalVariables.isFirstCheckBranch {
                                    self.openNextScreenAfterChecked()
                                }
                            }
                        } else {
                            if linkType == .SWEEPTAKES {
                                self.openSweeptakesScreen()
                            } else {
                                self.openNextScreenAfterChecked()
                            }
                        }
                    } else if GLOBAL.GlobalVariables.isFirstCheckBranch {
                        //self.openNextScreenAfterChecked()
                    }
                    
                } else if GLOBAL.GlobalVariables.isFirstCheckBranch {
                    self.openNextScreenAfterChecked()
                }
            }
            
            GLOBAL.GlobalVariables.isFirstCheckBranch = false
        }
    }
    
    private func getTEOSplashDetail(_ dealId: String) {
        Utils.showLoading()
        APICommonServices.getTEOSplashDetail(dealId) { (resp) in
            mPrint("getTEOSplashDetail", resp)
            
            if let resp = resp, let data = resp["data"] as? [String: Any?], let detail = data["deal"] as? [String: Any?], let status = resp["status"] as? Bool {
                if status {
                    self.handleShowTEOSplashDeal(detail)
                } else {
                    self.openNextScreenAfterChecked()
                }
            } else {
                self.openNextScreenAfterChecked()
            }
            
            Utils.dismissLoading()
            
        }
    }
    
    private func handleShowTEOSplashDeal(_ data: [String: Any?]?) {
        let tTopVc = self.getTopViewController()
        
        if let tData = data, let tTemplate = tData["Template"] as? [String: Any?], let topVc = tTopVc {
            
            
            let typeTpl = Utils.getTemplateSplash(tTemplate)
            
            let splashDealView = SplashDealView().loadTheme(typeTpl)
            
            splashDealView.config(tData, onClickedDetailCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                if let tData = data {
                    
                    let backCallback = { [weak self] () in
                        guard let self = self else {return}
                        
                        Utils.checkLogin(nil, requireLogin: false, { [weak self] (status) in
                            guard let self = self else {return}
                            
                            if status == .NOT_YET {
                                self.openNextScreenAfterChecked()
                            } else {
                                self.initViewController(Storyboard.main.instantiateViewController(withIdentifier: DiscoveryMainController.className()))
                            }
                        })
                    }
                    
                    let dealDetailVC = DealDetailViewController()
                    dealDetailVC.dealInfo = tData
                    dealDetailVC.callback = backCallback
                    
                    topVc.navigationController?.pushViewController(dealDetailVC, animated: true)
                }
                }, onClickedCloseCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    self.openNextScreenAfterChecked()
            })
            
            
            splashDealView.showInView(topVc)
            
        } else {
            self.openNextScreenAfterChecked()
        }
        
    }
    
    private func handleGetReferral(_ refCode: String) {
        Utils.showLoading()
        
        APICommonServices.getReferralInfo(refCode) { (resp) in
            mPrint("getReferralInfo", resp)
            if let resp = resp, let status = resp["status"] as? Bool  {
                
                if status {
                    if let data = resp["data"] as? [String: Any?], let referralInfo = data["user"] as? [String: Any?] {
                        self.openNextScreenAfterChecked(referralInfo)
                    } else {
                        self.openNextScreenAfterChecked()
                    }
                } else {
                    self.openNextScreenAfterChecked()
                }
            } else {
                self.openNextScreenAfterChecked()
            }
            
            Utils.dismissLoading()
        }
    }
    
    public func openNextScreenAfterChecked(_ referral: [String: Any?]? = nil) {
        GLOBAL.GlobalVariables.isCheckScreenReady = false
        
        GLOBAL.GlobalVariables.referralInfo = referral
        
        let welcomeVC = NewWelcomeViewController()
        self.initViewController(welcomeVC)
    }
    
    private func openSweeptakesScreen(_ data: [String: Any?]? = nil) {
        let sweeptakesVC = SweepstakeViewController()
        
        let vc = self.getTopViewController()
        
        vc?.addSubviewAsPresent(sweeptakesVC)
    }
    
    
    func registerNotification(_ application: UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { (authorized, error)  in
                    if authorized {
                        
                        let uTakeALook = UNNotificationAction(identifier: DEAL_NOTIFCATION_ACTION.TAKE_A_LOCK.rawValue, title: LOCALIZED.take_a_look.translate, options: [.foreground])
                        let uNoThanks = UNNotificationAction(identifier: DEAL_NOTIFCATION_ACTION.NO_THANKS.rawValue , title: LOCALIZED.no_thanks.translate, options: [])
                        let uRemindLater = UNNotificationAction(identifier: DEAL_NOTIFCATION_ACTION.REMIND_ME_LATER.rawValue , title: LOCALIZED.remind_me_later.translate, options: [])
                        
                        
                        let dealCategory = UNNotificationCategory(identifier: "deal", actions: [uTakeALook, uNoThanks, uRemindLater], intentIdentifiers: [], options: [])
                        
                        let deal2Category = UNNotificationCategory(identifier: "deal2", actions: [uTakeALook, uNoThanks, uRemindLater], intentIdentifiers: [], options: [])
                        
                        UNUserNotificationCenter.current().setNotificationCategories([dealCategory, deal2Category])
                    }
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    public func initViewController(_ mainVC: UIViewController) {
        //window = UIWindow(frame: UIScreen.main.bounds)
        let mainVC = mainVC
        let navigationController = UINavigationController(rootViewController: mainVC)
        navigationController.navigationBar.isTranslucent = false
        
        let slideMenu = MenuViewController()
        
        let slideMenuController = ExSlideMenuController(mainViewController: navigationController, leftMenuViewController: slideMenu)
        
        GLOBAL.GlobalVariables.rootVC = slideMenuController
        guard let window = self.window else {
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
            return
        }
        
        window.rootViewController = slideMenuController
        window.makeKeyAndVisible()
        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
    
    @objc func forceSignIn(_ notification: Notification) {
        let loginVC = LoginViewController()
        loginVC.isForcedLogin = true
        
        self.initViewController(loginVC)
    }
    
    @objc func handleAppDidBecomeActive(_ notification: Notification) {
        if !self.isStartNew {
            NotificationCenter.default.post(name: .FORCE_ALLOW_PERMISSION_LOCATION, object: ["fromDidBecomeActive": true])
            
            if GLOBAL.GlobalVariables.allowFetchLocation {
                self.mLocationManager.getCurrentLocation { (data) in
                    print("new location -> (\(GLOBAL.GlobalVariables.latestLat), \(GLOBAL.GlobalVariables.latestLng))")
                    
                    APICommonServices.trackNewLocation { (resp) in
                        //mPrint("trackNewLocation -> ", resp)
                    }
                }
            }
            
        } else {
            self.isStartNew = false
        }
        
    }
    
    @objc func finishedSweeptakes(_ notification: Notification) {
        if let uData = notification.object as? [String: Any] {
            let isWin = uData["isWin"] as? Bool ?? false
            let isCancel = uData["isCancel"] as? Bool ?? false
            
            if isCancel {
                if GLOBAL.GlobalVariables.isCheckScreenReady {
                    self.openNextScreenAfterChecked()
                }
            } else if isWin {
                let signUpVC = SignUpViewController()
                signUpVC.isFromSweepstake = true
                
                self.initViewController(signUpVC)
            } else {
                let vVC = VerifyCodeViewController()
                vVC.isFromSweepstake = true
                
                self.initViewController(vVC)
            }
            
        }
        
    }
    
    private func showNewChat(_ data: [String: Any?]?) {
        
        let vc = self.getTopViewController()
        
        let chatView = NewChatView().loadView()
        chatView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50.0)
        chatView.config(data)
        chatView.isUserInteractionEnabled = false
        
        rControl.dismissOnScreenMessage(withCompletion: ({
            self.rControl.showMessage(
                withCustom: chatView,
                atPosition: .navBarOverlay,
                viewController: vc?.navigationController,
                tapCompletion: {
                    self.gotoChatDetails(data)
            }
            )
        }))
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        
        mPrint("APNS token -> ", token)
        
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                mPrint("FCM token -> ", result.token)
                
                GLOBAL.GlobalVariables.fcmToken = result.token
                
                Utils.updatePushingToken()
            }
        })
        
    }
    
    public func getTopViewController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
//            if topController is ExSlideMenuController {
//                let rootView = topController as! ExSlideMenuController
//                let mainVC = rootView.mainViewController as? UINavigationController
//
//                if let nav = mainVC {
//                    let rootVC = nav.topViewController
//
//                    return rootVC
//                }
//
//                return nil
//            }
            
            return topController
        }
        
        return nil
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        Branch.getInstance().application(app, open: url, options: options)
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        // handler for Universal Links
        Branch.getInstance().continue(userActivity)
        return true
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    private func gotoTilacDeals(_ deals: [[String: Any?]] = []) {
        let tilacVC = TilacDealsViewController()
        //tilacVC.deals = deals
        if let rootVC = self.getTopViewController() {
            rootVC.addSubviewAsPresent(tilacVC)
        }
    }
    
    private func gotoDailyDeals(_ dealIds: String) {
        if let topVC = self.getTopViewController() {
            let dailyVC = DailyDealsViewController()
            dailyVC.dealIds = dealIds
            
            topVC.addSubviewAsPresent(dailyVC)
        }
    }
    
    private func gotoChatDetails(_ data:  [String: Any?]?) {
        if let tData = data {
            if let rootVC = self.getTopViewController() {
                
                let chatVC = ChatDetailViewController()
                let cData = ["PartnerID": tData["ownerId"] as? String]
                chatVC.data = cData
                
                rootVC.navigationController?.pushViewController(chatVC, animated: true)
                
            }
        }
    }
    
    private func showTEODeal(_ data: [String: Any?]?) {
        Utils.dismissLoading()
        
        setTimeout({
            let content = TEOAlertView().loadView()
            
            content.config(data, onClickedAcceptCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
                
                if let rootVC = self.getTopViewController() {
                    let inviteVC = InviteFriendsViewController()
                    inviteVC.data = data
                    
                    rootVC.navigationController?.pushViewController(inviteVC, animated: true)
                }
                
                }, onClickedCancelCallback: { [weak self] (data) in
                    guard let self = self else {return}
                    
                    Utils.closeCustomAlert()
            })
            
            Utils.showAlertWithCustomView(content)
            
        }, 300)
    }
    
    private func showSuggestUpgradeSalesForce(_ data: [String: Any?]? = nil) {
        let content = AlertSuggestUpgradeSalesforce().loadView()
        
        content.config(data, onClickedYesCallback: { [weak self] (data) in
            guard let self = self else {return}
            
            self.submitConfirmUpgradeSalesforce(.YES, data)
            
            Utils.closeCustomAlert()
            }, onClickedNoCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.submitConfirmUpgradeSalesforce(.NO, data)
                Utils.closeCustomAlert()
            }, onClickedTGCDistributorCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.submitConfirmUpgradeSalesforce(.TGC_DISTRIBUTOR, data)
                Utils.closeCustomAlert()
            }, onClickedLaterCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                self.submitConfirmUpgradeSalesforce(.ASK_ME_LATER, data)
                Utils.closeCustomAlert()
            }, onClickedCloseCallback: { [weak self] (data) in
                guard let self = self else {return}
                
                Utils.closeCustomAlert()
        })
        
        
        Utils.showAlertWithCustomView(content)
    }
    
    private func submitConfirmUpgradeSalesforce(_ answer: CONFIRM_UPGRADE_SALESFORCE = .NO, _ data: [String: Any?]?) {
        var region = ""
        var trackingId = ""
        if let tData = data {
            region = tData["in_where"] as? String ?? ""
            trackingId = tData["member_tracking_pns_id"] as? String ?? ""
        }
        
        APICommonServices.confirmUpgradeSalesforce(answer.rawValue, region, trackingId) { (resp) in
            mPrint("confirmUpgradeSalesforce -> ", resp)
        }
    }
    
    private func handleWhenInviteePNSCancelView(_ pushInfo: [String: Any?]) {
        let inviteStatus = pushInfo["invite_status"] as? String ?? ""
        
        if inviteStatus == TEO_STATUS.COMPLETED.rawValue {
            setTimeout({
                NotificationCenter.default.post(name: .REFRESH_TEO_IN_LIST, object: nil)
            }, 500)
        }
        
    }
    
    private func handleWhenInviteeCompleted(_ pushInfo: [String: Any?]) {
        //        if let topVC = self.getTopViewController() {
        //            let todoID = pushInfo["assignment_id"] as? String ?? ""
        //            let inviteStatus = pushInfo["invite_status"] as? String ?? ""
        //
        //            if inviteStatus == TEO_STATUS.COMPLETED.rawValue {
        //                if let mainTabVc = GLOBAL.GlobalVariables.walletMainInstance {
        //                    topVC.navigationController?.popToViewController(mainTabVc, animated: true)
        //                    mainTabVc.navigate(toIndex: WALLET_SCREEN_ID.MY_WALLET.rawValue)
        //
        //                    setTimeout({
        //                        NotificationCenter.default.post(name: .REFRESH_TEO_IN_LIST, object: nil)
        //                    }, 700)
        //                } else {
        //                    GLOBAL.GlobalVariables.lastWalletScreen = .MY_WALLET
        //
        //                    let waletMainVC = WalletMainViewController()
        //                    topVC.navigationController?.pushViewController(waletMainVC, animated: true)
        //                }
        //            } else {
        //                GLOBAL.GlobalVariables.todoWaitingId = todoID
        //
        //                if let mainTabVc = GLOBAL.GlobalVariables.walletMainInstance {
        //                    topVC.navigationController?.popToViewController(mainTabVc, animated: true)
        //                    mainTabVc.navigate(toIndex: WALLET_SCREEN_ID.THINGS_TO_DO.rawValue)
        //                } else {
        //                    GLOBAL.GlobalVariables.lastWalletScreen = .THINGS_TO_DO
        //
        //                    let waletMainVC = WalletMainViewController()
        //                    topVC.navigationController?.pushViewController(waletMainVC, animated: true)
        //                }
        //
        //
        //                setTimeout({
        //                    NotificationCenter.default.post(name: .REFRESH_THEN_SCROLL_TO_ACTION_AND_EXPAND, object: nil)
        //                }, 700)
        //            }
        //        }
        
    }
    
    private func processPush(_ userInfo: [AnyHashable : Any], userInteraction: Bool = false, action: String? = nil) {
        if let pushInfo = userInfo as? [String: Any?], let pushType = pushInfo["push_type"] as? String {
            mPrint("pushInfo", pushInfo)
            
            Utils.checkLogin(nil, requireLogin: false) { (status) in
                if status == .SUCCESS_IMMEDIATELY {
                    if pushType == PUSH_TYPE.DEAL_DAILY_NOTIFICATION.rawValue {
                        self.processDailySwipes(pushInfo, userInteraction: userInteraction, action: action)
                    } else if pushType == PUSH_TYPE.NEW_CHAT_MESSAGE.rawValue {
                        if userInteraction  { // FROM BACKGROUN
                            if GLOBAL.GlobalVariables.numberChatReady > 0 { // check Room exist
                                NotificationCenter.default.post(name: .CHECK_CHAT_EXIST, object: pushInfo)
                            } else {
                                self.gotoChatDetails(pushInfo)
                            }
                        } else { // IN APP
                            if GLOBAL.GlobalVariables.numberChatReady > 0 { // check Room exist
                                NotificationCenter.default.post(name: .CHECK_CHAT_EXIST, object: pushInfo)
                            } else { // show top RMessage
                                self.showNewChat(pushInfo)
                            }
                        }
                        
                    } else if pushType == PUSH_TYPE.TELL_EVERYONE_ASSIGN_TO_MEMBER.rawValue {
                        var teoData = [String: Any?]()
                        teoData["dealId"] =  pushInfo["deal_id"] as? String ?? ""
                        teoData["totalAssignee"] =  pushInfo["total_of_required_assignee"] as? Int ?? 5
                        
                        self.showTEODeal(teoData)
                        
                    } else if pushType == PUSH_TYPE.TELL_EVERYONE_INVITEE_COMPLETED.rawValue {
                        if userInteraction  { // FROM BACKGROUN
                            self.handleWhenInviteeCompleted(pushInfo)
                        } else { // IN APP
                            self.showAlert(pushInfo, confirmCallback: { [weak self] (data) in
                                guard let self = self else {return}
                                self.handleWhenInviteeCompleted(pushInfo)
                                }, cancelCallback: { [weak self] (data) in
                                    guard let self = self else {return}
                                    self.handleWhenInviteePNSCancelView(pushInfo)
                            })
                        }
                    }
                    //                    else if pushType == PUSH_TYPE.SUGGEST_UPGRADE_SALESFORCE.rawValue {
                    //                        self.showSuggestUpgradeSalesForce(pushInfo)
                    //                    } else if pushType == PUSH_TYPE.DAILY_SWIPES_TOOL.rawValue {
                    //                        self.processDailySwipes(pushInfo, userInteraction: userInteraction, action: action)
                    //                    } else if pushType == PUSH_TYPE.SPLASH_PNS_TOOL.rawValue {
                    //                       self.processSplash(pushInfo, userInteraction: userInteraction)
                    //                    } else if pushType == PUSH_TYPE.WALLET_CAROUSEL_PNS_TOOL.rawValue {
                    //                       self.processHighlightCarousel(pushInfo, userInteraction: userInteraction)
                    //                    } else if pushType == PUSH_TYPE.WALLET_DEAL_PNS_TOOL.rawValue {
                    //                        self.processOpenDealInCarousel(pushInfo, userInteraction: userInteraction)
                    //                    } else if pushType == PUSH_TYPE.DEAL_DETAIL_PNS_TOOL.rawValue {
                    //                        self.processOpenDealDetail(pushInfo, userInteraction: userInteraction)
                    //                    } else if pushType == PUSH_TYPE.REMINDER_TO_PAY_TOOL.rawValue {
                    //                        self.processRedminderToPay(pushInfo, userInteraction: userInteraction)
                    //                    } else if pushType == PUSH_TYPE.NEW_LOCATION_SHAKE_PNS_TOOL.rawValue {
                    //                        self.showSuggestUpgradeSalesForce(pushInfo)
                    //                    }
                } else {
                    if pushType == PUSH_TYPE.DEAL_DETAIL_PNS_TOOL.rawValue {
                        self.processOpenDealDetail(pushInfo, userInteraction: userInteraction)
                    }
                }
            }
        }
    }
    
    private func showAlert(_ pushInfo: [String: Any?], confirmCallback: DataCallback? = nil, cancelCallback:  DataCallback? = nil) {
        if let topVc = self.getTopViewController() {
            Utils.showConfirmAlert(topVc, pushInfo["title"] as? String ?? "", pushInfo["body"] as? String ?? "", LOCALIZED.txt_view.translate, LOCALIZED.txt_cancel_title.translate, confirmCallback: { (_) in
                confirmCallback?(pushInfo)
            }, cancelCallback: { (_) in
                cancelCallback?(pushInfo)
                
            })
        }
    }
    
    private func processDailySwipes(_ pushInfo: [String: Any?], userInteraction: Bool = false, action: String? = nil) {
        if userInteraction {
            if action == DEAL_NOTIFCATION_ACTION.TAKE_A_LOCK.rawValue || action == UNNotificationDefaultActionIdentifier {
                self.handleShowDailySwipes(pushInfo)
            }
        } else {
            self.showAlert(pushInfo, confirmCallback: { (data) in
                self.handleShowDailySwipes(pushInfo)
            })
        }
        
    }
    
    private func handleShowDailySwipes(_ pushInfo: [String: Any?]) {
        let tIds = pushInfo["list-deal-id"] as? String ?? ""
        
        self.gotoDailyDeals(tIds)
    }
    
    private func processSplash(_ pushInfo: [String: Any?], userInteraction: Bool = false) {
        if userInteraction {
            self.handleShowFlashDeal(pushInfo)
        } else {
            self.showAlert(pushInfo, confirmCallback: { (data) in
                self.handleShowFlashDeal(pushInfo)
            })
        }
    }
    
    private func handleShowFlashDeal(_ pushInfo: [String: Any?]) {
        if let topVc = self.getTopViewController() {
            let dealId = pushInfo["deal_show_id"] as? String ?? ""
            
            APICommonServices.getInfoSplashScreen(dealId) { [weak self] (resp) in
                guard let self = self else {return}
                mPrint("getInfoSplashScreen", resp)
                
                if let resp = resp, let data = resp["data"] as? [String: Any?], let status = resp["status"] as? Bool {
                    if status {
                        Utils.handleShowSplashDeal(topVc, data)
                    }
                }
            }
        }
    }
    
    private func processRedminderToPay(_ pushInfo: [String: Any?], userInteraction: Bool = false) {
        if userInteraction {
            
        } else {
            if let topVc = self.getTopViewController() {
                Utils.showAlert(topVc, pushInfo["title"] as? String ?? "", pushInfo["body"] as? String ?? "")
            }
        }
    }
    
    private func processHighlightCarousel(_ pushInfo: [String: Any?], userInteraction: Bool = false) {
        if userInteraction {
            self.handleHighlightCarousel(pushInfo)
        } else {
            self.showAlert(pushInfo, confirmCallback: { (data) in
                self.handleHighlightCarousel(pushInfo)
            })
        }
    }
    
    private func handleHighlightCarousel(_ pushInfo: [String: Any?]) {
        
        //        if let topVC = self.getTopViewController() {
        //            if let mainTabVc = GLOBAL.GlobalVariables.walletMainInstance {
        //                topVC.navigationController?.popToViewController(mainTabVc, animated: true)
        //                mainTabVc.navigate(toIndex: WALLET_SCREEN_ID.MY_WALLET.rawValue)
        //
        //                setTimeout({
        //                    NotificationCenter.default.post(name: .HIGHLIGH_CAROUSEL, object: pushInfo)
        //                }, 700)
        //            } else {
        //                GLOBAL.GlobalVariables.lastWalletScreen = .MY_WALLET
        //
        //                let waletMainVC = WalletMainViewController()
        //                waletMainVC.callbackMyWallet = { (data) in
        //                    NotificationCenter.default.post(name: .HIGHLIGH_CAROUSEL, object: pushInfo)
        //                }
        //                topVC.navigationController?.pushViewController(waletMainVC, animated: true)
        //            }
        //        }
    }
    
    private func processOpenDealDetail(_ pushInfo: [String: Any?], userInteraction: Bool = false) {
        if userInteraction {
            self.handleOpenDealDetail(pushInfo)
        } else {
            self.showAlert(pushInfo, confirmCallback: { (data) in
                self.handleOpenDealDetail(pushInfo)
            })
        }
    }
    
    private func handleOpenDealDetail(_ pushInfo: [String: Any?]) {
        if let topVC = self.getTopViewController(), let dealId = pushInfo["deal_id"] as? String {
            Utils.handleGetDealDetail(atView: nil, dealId) { (data) in
                //mPrint("handleOpenDealDetail --> ", data)
                
                if data != nil {
                    let dealDetailVC = DealDetailViewController()
                    dealDetailVC.dealInfo = data
                    topVC.navigationController?.pushViewController(dealDetailVC, animated: true)
                }
            }
        }
    }
    
    
    private func processOpenDealInCarousel(_ pushInfo: [String: Any?], userInteraction: Bool = false) {
        if userInteraction {
            self.handleOpenDealInCarousel(pushInfo)
        } else {
            self.showAlert(pushInfo, confirmCallback: { (data) in
                self.handleOpenDealInCarousel(pushInfo)
            })
        }
        
    }
    
    private func handleOpenDealInCarousel(_ pushInfo: [String: Any?]) {
        //        let tDict = ["DealID": pushInfo["id_deal_in_wallet"] as? String ?? "", "fromTool": true] as [String : Any]
        //
        //        if let topVC = self.getTopViewController() {
        //            if let mainTabVc = GLOBAL.GlobalVariables.walletMainInstance {
        //                topVC.navigationController?.popToViewController(mainTabVc, animated: true)
        //                mainTabVc.navigate(toIndex: WALLET_SCREEN_ID.MY_WALLET.rawValue)
        //
        //                setTimeout({
        //                    NotificationCenter.default.post(name: .SCROLL_TO_GROW_CARD_THEN_OPEN, object: tDict)
        //                }, 700)
        //            } else {
        //                GLOBAL.GlobalVariables.lastWalletScreen = .MY_WALLET
        //
        //                let waletMainVC = WalletMainViewController()
        //                waletMainVC.callbackMyWallet = { (data) in
        //                    NotificationCenter.default.post(name: .SCROLL_TO_GROW_CARD_THEN_OPEN, object: tDict)
        //                }
        //                topVC.navigationController?.pushViewController(waletMainVC, animated: true)
        //            }
        //        }
    }
    
    
    
    
    private func isShowDailyDeals(_ userInfo:  [AnyHashable : Any], userInteraction: Bool = false, action: String? = nil) -> Bool {
        var isShow = false
        if let pushInfo = userInfo as? [String: Any?], let pushType = pushInfo["push_type"] as? String {
            if pushType == PUSH_TYPE.DEAL_DAILY_NOTIFICATION.rawValue {
                if userInteraction  { // FROM BACKGROUND
                    if action == DEAL_NOTIFCATION_ACTION.TAKE_A_LOCK.rawValue || action == UNNotificationDefaultActionIdentifier {
                        isShow = true
                    }
                } else {
                    isShow = true
                }
            }
        }
        
        return isShow
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        GLOBAL.GlobalVariables.shouldShowDailyDeals = self.isShowDailyDeals(userInfo)
        self.processPush(userInfo)
        
        completionHandler([])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        GLOBAL.GlobalVariables.shouldShowDailyDeals = self.isShowDailyDeals(userInfo, userInteraction: true, action: response.actionIdentifier)
        
        setTimeInterval({ [weak self] (number, timer) in
            guard let self = self else {return}
            
            if GLOBAL.GlobalVariables.isHomeScreenReady {
                timer.invalidate()
                self.processPush(userInfo, userInteraction: true, action: response.actionIdentifier)
            }
            }, 500)
        
        completionHandler()
    }
    
}

