
var Excel = require('exceljs');
var fs = require('fs');
var output_folder = __dirname + "/../TGCMember";
var path_excel = process.argv[2] ? process.argv[2] : __dirname + "/xls-files/member_ogogo.xlsx";
var LocalizationTabNames = ["TGC Member Native"];

var workbook = new Excel.Workbook();

var data_languages = [];

var languages = [
    { code: 'en', numCol: 2 },
    { code: 'vi', numCol: 3 },
    { code: 'zh-Hans', numCol: 4 },
    { code: 'zh-Hant', numCol: 4 }
];


workbook.xlsx.readFile(path_excel).then(function () {
    for (var i = 0; i < languages.length; i++) {
        let lang = languages[i];
        console.log('Progressing ', lang);
        // build Localizable.strings
        workbook.eachSheet(function (worksheet, sheetId) {
            let sheet_name = worksheet.name;
            if (LocalizationTabNames.indexOf(sheet_name) >= 0) {
                data_languages = []
                worksheet.eachRow(function (row, rowNumber) {
                    if (rowNumber == 1) {
                        return;
                    }

                    data_languages.push({ key: row.getCell(1).value, [lang.code]: row.getCell(lang.numCol).value });
                });
                
                // require(__dirname + '/android.js')(output_folder, data_languages, lang)
                require(__dirname + '/iOS.js')(output_folder, data_languages, lang)
            }
        });
    }

})


workbook.xlsx.readFile(path_excel).then(function () {
   
    console.log('Progressing create Localized.swift --> ');
    // build Localizable.strings
    workbook.eachSheet(function (worksheet, sheetId) {
        let sheet_name = worksheet.name;
        if (LocalizationTabNames.indexOf(sheet_name) >= 0) {
            key_languages = []
            worksheet.eachRow(function (row, rowNumber) {
                if (rowNumber == 1) {
                    return;
                }

                key_languages.push({ key: row.getCell(1).value });
            });
            
            // require(__dirname + '/android.js')(output_folder, data_languages, lang)
            require(__dirname + '/Swift.js')(output_folder, key_languages)
        }
    });
    

})

