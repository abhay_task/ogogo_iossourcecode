var fs = require('fs');
var utils = require('./utils.js');

module.exports = function (output_folder, data, languages) {
    try {
        fs.unlinkSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    output_folder = output_folder + "/android"
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }

    output_folder = output_folder + "/res"
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    output_folder = output_folder + "/values-" + languages.code
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    console.log("data.output_folder android: ", output_folder);
    function create_folder_language(language) {
        var content_file = "<resources xmlns:tools=\"http://schemas.android.com/tools\">";
        for (var j = 0; j < data.length; ++j) {
            var item = data[j];
            if (item[language.code] && item.key) {
                var text = item[language.code];
                if (text.hasOwnProperty("result")) {
                    text = text.result
                }
                // console.log('text', text);
                text = utils.str_replace("'", "\\'", text);
                text = utils.str_replace("\\\'", "\\'", text);
                text = utils.str_replace(" & ", " &amp; ", text);
                text = utils.str_replace('_X0008_', '', text);
                content_file += '\n <string name="' + item.key + '">' + text + '</string>';
            }
        }
        content_file += "\n</resources>";
        fs.writeFileSync(output_folder + "/strings.xml", content_file, { encoding: "utf8" })
        console.log("Save ok Android languages: ", languages.code);
    }
    
    create_folder_language(languages);
}