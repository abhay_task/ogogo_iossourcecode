var fs = require('fs');
var utils = require('./utils.js');

module.exports = function (output_folder, datas) {
    try {
        fs.unlinkSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    // output_folder = output_folder + "/ios"

    console.log("create_file_localized --> : ", output_folder);

    function create_file_localized() {
        console.log('datas --> ', datas);
        var content_file = "import UIKit\n\npublic enum LOCALIZED {\n";
        for (var j = 0; j < datas.length; ++j) {
            var item = datas[j];
            console.log('item --> ', item);
            var text = item.key;

            if (text != null && text != "") {
                if (text.hasOwnProperty("result")) {
                    text = text.result
                }
    
                // console.log('text', text);
    
                text = utils.str_replace("&amp;", "&", text);
                text = utils.str_replace("'", "\\'", text);
                text = utils.str_replace('"', '\\"', text);
                text = utils.str_replace('_X0008_', '', text);

                content_file += '\n     case ' + item.key;
                
            }
        }

        content_file += "\n\n     var translate : String {\n          switch self {";
        for (var j = 0; j < datas.length; ++j) {
            var item = datas[j];
            console.log('item --> ', item);
            var text = item.key;

            if (text != null && text != "") {
                if (text.hasOwnProperty("result")) {
                    text = text.result
                }
    
                // console.log('text', text);
    
                text = utils.str_replace("&amp;", "&", text);
                text = utils.str_replace("'", "\\'", text);
                text = utils.str_replace('"', '\\"', text);
                text = utils.str_replace('_X0008_', '', text);

                content_file += '\n             case .' + item.key + ' : return MLocalized("' + item.key + '")';
                
            }
        }

        content_file += "\n             }";
        content_file += "\n     }";
        content_file += "\n}";

        fs.writeFileSync(output_folder + "/Localized.swift", content_file, { encoding: "utf8" })
        console.log("Save ok IOS LOCALIZED KEYS: ");
    }

    create_file_localized();



}