var fs = require('fs');
var utils = require('./utils.js');

module.exports = function (output_folder, datas, languages) {
    try {
        fs.unlinkSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    try {
        fs.mkdirSync(output_folder);
    } catch (e) {
        // console.log(e);
    }
    // output_folder = output_folder + "/ios"

    console.log("data.output_folder ios: ", output_folder);
    function create_folder_language_ios(language) {

        var content_file = "";
        for (var j = 0; j < datas.length; ++j) {
            var item = datas[j];
            if (item[language.code] && item.key) {
                var text = item[language.code];
                if (text.hasOwnProperty("result")) {
                    text = text.result
                }
                // console.log('text', text);

                text = utils.str_replace("&amp;", "&", text);
                text = utils.str_replace("'", "\\'", text);
                text = utils.str_replace('"', '\\"', text);
                text = utils.str_replace('_X0008_', '', text);
                content_file += '\n "' + item.key + '" = "' + text + '";';
            }
        }

        output_folder = output_folder + "/" + languages.code + ".lproj"
        try {
            fs.mkdirSync(output_folder);
        } catch (e) {
            // console.log(e);
        }

        fs.writeFileSync(output_folder + "/Languages.strings", content_file, { encoding: "utf8" })
        console.log("Save ok IOS languages: ", languages.code);
    }

    create_folder_language_ios(languages);



}